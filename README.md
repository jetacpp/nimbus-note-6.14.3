## How prepare project before start application:
You need to have `https://gl.nimbusweb.co/web/webnotes` 
project should be switched to `electron-release` branch with `installed node libs`.
 
Then in `nimbus-electron` folder run:
```
yarn build:webnotes
```

### How to start application:
```
yarn
yarn start
```

## How to control api sync server & webnotes dev/prod configs
1. Copy `dev/config.env.example.js` as `dev/config.env.js`
2. Change content of `dev/config.env.js`
3. If no file found in `dev/config.env.js` then dev sync server & webnotes configs will be used during project build

### How to make release app version on OSX/WIN (output will be at dist/mas folder):
```
yarn release:osx / yarn release:win
```

### Get OSX Account APP specific password
`https://www.imore.com/how-generate-app-specific-passwords-iphone-ipad-mac`

### App notarize & upload process for OSX release:
- App notarize done with electron-notarize lib & `src/afterSign.js` script
- If notarize process finish success you will get email into your apple id account email with UUID
- With UUID you can check notarize details in console:
```
xcrun altool --notarization-info 91e6a145-xxxx-457d-8a70-xxxxxxx -u "test@email.com" -p "xxx-yhhv-xxxx-kirf"
```
If you did not get email with UUID, you can also check notarize history and get last history item (optional)
```
xcrun altool --notarization-history 0 -u "test@email.com" -p "xxx-yhhv-xxxx-kirf"
```
- For upload to app store use console function:
```
xcrun altool --upload-app --file "/Users/oleksandrlisunov/PhpstormProjects/nimbus-electron/dist/mas/Nimbus Note-X.X.XX.pkg" --username "test@email.com" --password "xxx-yhhv-xxxx-kirf"
``` 

### How to enable TS types for local development
```
yarn ts:types
```

### Develop sync database app version
Set `config.USE_DEV_SYNC = true;` in the `config.js` file

### Generate icons for project: 
- win: 512x512 ico
- mac: icns https://elliotekj.com/2014/05/27/how-to-create-high-resolution-icns-files/

### Publish WIN build
```
yarn release:win
```

- backup locally latest.yml file from S3
```
https://electron-data.s3.amazonaws.com/prod/latest.yml
```

then use aws console tools:
- go to `dist` folder
- run console command:
```
aws s3 sync . s3://electron-data/prod
```
- check sync results:
```
aws s3 ls s3://electron-data/prod/
```

### Which local dirs / projects / branches should be for development
- webnotes / https://gl.nimbusweb.co/webnotes/webnotes / electron-release (*required)
- web-editor / https://gl.nimbusweb.co/webnotes/web-editor / electron-release (if you need to work with editor)
- nimbus-editor / https://gl.nimbusweb.co/lib/editor / electron-release (if you need to work with editor)

### Which config files electron use to build webnotes
```
./config/env/...
```

### Build project with updated web-editor or nimbus-editor projects
1. Get project `web-editor` from git
2. Get project `nimbus-editor` from git
3. Go to `web-editor` project, switch to `electron-release` branch, make `npm install`
4. Go to nimbus-editor project, switch to `electron-release` branch, make `npm install`, check project name in package.json should be `nimbus-editor`, then make `npm link`
5. Go to web-editor project, make `npm link nimbus-editor`, now you can check `node_modules/nimbus-editor` dir it should follow to local nimbus-editor project
6. Now you can make changes to editor code, go to `nimbus-electron` project and run command: 
```
yarn build:webnotes:editor
```