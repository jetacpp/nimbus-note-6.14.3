'use strict';

let NotesApp = angular.module("NotesApp");
const ELECTRON_HOST = 'localhost';
const ELECTRON_PORT = 9921;
const ELECTRON_PROTOCOL = '{{protocol}}';
const ELECTRON_URL = `http://${ELECTRON_HOST}:${ELECTRON_PORT}`;

const FILE_SERVER_URL = ELECTRON_URL;
const HOST = 'nimbusweb.me';

Object.defineProperty(window, 'NIMBUS_CONFIG', {
  value: {
    FILE_SERVER_URL
  }
})

NotesApp.service('config', [function () {
  this.platform = '{{platform}}';
  this.editorClient = '{{editorClient}}';
  this.baseUrl = `https://${HOST}`;

  this.gaId = '';
  this.gtagSiteId = '';
  this.gtagAppId = '';
  this.hotjarId = '';
  this.hotjarSv = '';

  this.sentryUrl = 'sentry.nimbusweb.co';
  this.sentryKey = '7e9d53f6899d4c4288fb48cafb66d5e9';
  this.sentryProject = '9';

  this.boxUrl = '/box';
  this.statsdUrl = '';

  this.intercomAppId = 'pv2smqms';
  this.zohoWidgetId = '53bb1c5e8ef732a0cf6b012048891c736858ee622e156415299c993aad3fcd04';

  this.editorHelpUrl = {
    en: 'https://s.nimbusweb.me/share/3547021/d95s5pwex7lel0o2zssx',
    ru: 'https://s.nimbusweb.me/share/3547388/6p1il3z7xf767uldat0b'
  };
  this.embedsHelpUrl = {
    en: 'https://s.nimbusweb.me/share/3757280/oy6e6dazytqdnfwmr9e3',
    ru: 'https://s.nimbusweb.me/share/3757268/kmh6hum785m0pcnpqp09',
  };
  this.upgradeUrl = "http://nimbus.everhelper.me/pricing.php";
  this.supportFormUrl = "https://nimbusweb.me/contact-us.php";
  this.contactUrl = {
    en: 'https://nimbusweb.me/contact-us.php',
    ru: 'https://nimbusweb.me/contact-us.php'
  };
  this.appNoteUrl = {
    en: 'https://nimbusweb.me/note.php',
    ru: 'https://nimbusweb.me/ru/note.php',
  };
  this.appClipperUrl = {
    en: 'https://nimbusweb.me/clipper.php',
    ru: 'https://nimbusweb.me/ru/clipper.php',
  };
  this.appScreenshotUrl = {
    en: 'https://nimbusweb.me/screenshot.php',
    ru: 'https://nimbusweb.me/ru/screenshot.php',
  };
  this.helpUrl = {
    en: 'https://s.nimbusweb.me/share/3552427/znds47abuxzypf005298',
    ru: 'https://s.nimbusweb.me/share/3552429/vlrhr2qh828tt2i3izgb',
  };
  this.deleteAccountUrl = "https://nimbus.everhelper.me/remove-account/";
  this.shareUrl = `https://${HOST}/s`;
  this.adminUrl = `https://teams.nimbusweb.me`;
  this.cookieConsentUrl = 'https://nimbusweb.me/privacy.php';
  this.fileServerUrl = FILE_SERVER_URL;
  this.emailDomain = "m.nimbusweb.me";
  this.server = ELECTRON_URL;
  this.serverSecure = false;
  this.shortUrlService = "http://nimb.ws/dantist_api.php";
  this.billingUrl = "https://nimbusweb.me/user-billing/";
  this.authLogout = `${ELECTRON_URL}/logout`;
  this.host = HOST;
  this.getSocketUrlFunction = ({
    nodeId,
    workspaceId,
    textId,
    cid,
    baseUrl
  }) => (token) =>
    !token ? `` :
      `${baseUrl}?token=${token}&cid=${cid}`
  this.featureFlags = [
    'electron',
    'betaEditor',
    'smilesToolbar',
    'twoStepsAuth',
  ];

  this.getElectronUrl = (url) => {
    let baseUrl = this.baseUrl
    if(window.APP_HYDRATED_STATE) {
      const { electronOrganization: org } = window.APP_HYDRATED_STATE
      if(org && org.electronHost) {
        baseUrl = `${ELECTRON_PROTOCOL}${org.electronHost}`
      }
    }
    return url.replace(ELECTRON_URL, baseUrl)
  }

  this.isElectron = true;

  this.init = function () {
  };

  this.androidNoteUrl = {
    en: 'https://play.google.com/store/apps/details?id=com.bvblogic.nimbusnote&hl=en',
    ru: 'https://play.google.com/store/apps/details?id=com.bvblogic.nimbusnote&hl=ru',
  };
  this.iphoneNoteUrl = {
    en: 'https://apps.apple.com/us/app/nimbus-note/id828918459',
    ru: 'https://apps.apple.com/ru/app/nimbus-note/id828918459',
  };
  this.webNoteUrl = {
    en: 'https://nimbusweb.me/client',
    ru: 'https://nimbusweb.me/client',
  };
  this.macNoteUrl = {
    en: 'https://apps.apple.com/us/app/nimbus-note-app/id1431085284?ls=1&mt=12',
    ru: 'https://apps.apple.com/ru/app/nimbus-note-app/id1431085284?ls=1&mt=12',
  };
  this.windowsNoteUrl = {
    en: 'https://nimbusweb.me/nimbusnote.exe',
    ru: 'https://nimbusweb.me/nimbusnote.exe',
  };
  this.chromeClipperUrl = {
    en: 'https://chrome.google.com/webstore/detail/web-clipper-nimbus/kiokdhlcmjagacmcgoikapbjmmhfchbi?hl=en',
    ru: 'https://chrome.google.com/webstore/detail/web-clipper-nimbus/kiokdhlcmjagacmcgoikapbjmmhfchbi?hl=ru',
  };
  this.operaClipperUrl = {
    en: 'https://addons.opera.com/en/extensions/details/nimbus-clipper/',
    ru: 'https://addons.opera.com/en/extensions/details/nimbus-clipper/',
  };
  this.firefoxClipperUrl = {
    en: 'http://nimbusweb.me/firefox-clipper/clipper.xpi',
    ru: 'http://nimbusweb.me/firefox-clipper/clipper.xpi',
  };
  this.chromeScreenShotUrl = {
    en: 'https://chrome.google.com/webstore/detail/nimbus-screenshot-screen/bpconcjcammlapcogcnnelfmaeghhagj?hl=en',
    ru: 'https://chrome.google.com/webstore/detail/nimbus-screenshot-screen/bpconcjcammlapcogcnnelfmaeghhagj?hl=ru',
  };
  this.operaScreenShotUrl = {
    en: 'https://addons.opera.com/en/extensions/details/nimbus-screen-capture/',
    ru: 'https://addons.opera.com/en/extensions/details/nimbus-screen-capture/',
  };
  this.firefoxScreenShotUrl = {
    en: 'https://addons.mozilla.org/en-US/firefox/addon/nimbus-screenshot/',
    ru: 'https://addons.mozilla.org/en-US/firefox/addon/nimbus-screenshot/',
  };
  this.markdownRulesUrl = {
    en: 'https://nimbusweb.me/s/share/4174588/z7mnun06goa8werpw1mc',
    ru: 'https://nimbusweb.me/s/share/4174594/q86d9ek046lyfk0ywyi9',
  };
  this.winHotKeysUrl = {
    en: 'https://nimbusweb.me/s/share/3121950/w3etnkxwaepds4qtv8tw',
    ru: 'https://nimbusweb.me/s/share/4174594/q86d9ek046lyfk0ywyi9',
  };
  this.macHotKeysUrl = {
    en: 'https://nimbusweb.me/s/share/3746491/9fk5g12b8tdt5hw0u04w',
    ru: 'https://nimbusweb.me/s/share/4174596/aulihae4uenq60gc8kwy',
  };
}]);