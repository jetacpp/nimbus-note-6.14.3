'use strict';

let NotesApp = angular.module("NotesApp");
const ELECTRON_HOST = 'localhost';
const ELECTRON_PORT = 9921;
const ELECTRON_PROTOCOL = 'http://';
const ELECTRON_URL = `http://${ELECTRON_HOST}:${ELECTRON_PORT}`;

const FILE_SERVER_URL = ELECTRON_URL;
const HOST = 'develop.nimbustest.com';

Object.defineProperty(window, 'NIMBUS_CONFIG', {
  value: {
    FILE_SERVER_URL: FILE_SERVER_URL
  }
});

NotesApp.service('config', [function () {
  this.platform = 'win32';
  this.editorClient = 'win_notes';
  this.baseUrl = `http://${HOST}`;

  this.gaId = '';
  this.gtagSiteId = '';
  this.gtagAppId = '';
  this.hotjarId = '';
  this.hotjarSv = '';
  this.sentryUrl = '';
  this.sentryKey = '';
  this.sentryProject = '';
  this.statsdUrl = '';
  this.intercomAppId = 'pv2smqms';

  this.editorHelpUrl = {
    en: 'https://s.nimbusweb.me/share/3547021/d95s5pwex7lel0o2zssx',
    ru: 'https://s.nimbusweb.me/share/3547388/6p1il3z7xf767uldat0b'
  };
  this.upgradeUrl = "http://nimbus.everhelper.me/pricing.php";
  this.supportFormUrl = "https://nimbusweb.me/contact-us.php";
  this.contactUrl = {
    en: 'https://nimbusweb.co/contact.php',
    ru: 'https://nimbusweb.me/ru/contact-us.php'
  };
  this.appNoteUrl = {
    en: 'https://nimbusweb.me/note.php',
    ru: 'https://nimbusweb.me/ru/note.php',
  };
  this.appClipperUrl = {
    en: 'https://nimbusweb.me/clipper.php',
    ru: 'https://nimbusweb.me/ru/clipper.php',
  };
  this.appScreenshotUrl = {
    en: 'https://nimbusweb.me/screenshot.php',
    ru: 'https://nimbusweb.me/ru/screenshot.php',
  };
  this.helpUrl = {
    en: 'https://s.nimbusweb.me/share/3552427/znds47abuxzypf005298',
    ru: 'https://s.nimbusweb.me/share/3552429/vlrhr2qh828tt2i3izgb',
  };
  this.deleteAccountUrl = "http://nimbustest.com/remove-account/";
  this.shareUrl = `https://${HOST}/s`;
  this.adminUrl = `http://notes-admin.develop.nimbustest.com`;
  this.cookieConsentUrl = 'https://nimbusweb.me/privacy.php';
  this.fileServerUrl = FILE_SERVER_URL;
  this.emailDomain = "m.nimbusweb.me";
  this.server = ELECTRON_URL;
  this.serverSecure = false;
  this.shortUrlService = "http://develop.nimbustest.com/short-url/";
  this.billingUrl = "https://nimbusweb.me/user-billing/";
  this.authLogout = `${ELECTRON_URL}/logout`;
  this.host = HOST;
  this.getSocketUrlFunction = ({
    nodeId,
    workspaceId,
    textId,
    cid,
    baseUrl
  }) => (token) =>
    !token ? `` :
      `${baseUrl}?token=${token}&cid=${cid}`
  this.featureFlags = [
    'electron',
    'betaEditor',
    'smilesToolbar',
    'twoStepsAuth',
  ];

  this.getElectronUrl = (url) => {
    let baseUrl = this.baseUrl
    if(window.ELECTRON_ORGANIZATION) {
      const org = window.ELECTRON_ORGANIZATION
      if(org && org.electronHost) {
        baseUrl = `${ELECTRON_PROTOCOL}${org.electronHost}`
      }
    }
    return url.replace(ELECTRON_URL, baseUrl)
  }

  this.isElectron = true;

  this.init = function () {
  };

  this.androidNoteUrl = {
    en: 'https://play.google.com/store/apps/details?id=com.bvblogic.nimbusnote&hl=en',
    ru: 'https://play.google.com/store/apps/details?id=com.bvblogic.nimbusnote&hl=ru',
  };
  this.iphoneNoteUrl = {
    en: 'https://apps.apple.com/us/app/nimbus-note/id828918459',
    ru: 'https://apps.apple.com/ru/app/nimbus-note/id828918459',
  };
  this.webNoteUrl = {
    en: 'https://nimbusweb.me/client',
    ru: 'https://nimbusweb.me/client',
  };
  this.macNoteUrl = {
    en: 'https://apps.apple.com/us/app/nimbus-note-app/id1431085284?ls=1&mt=12',
    ru: 'https://apps.apple.com/ru/app/nimbus-note-app/id1431085284?ls=1&mt=12',
  };
  this.windowsNoteUrl = {
    en: 'https://nimbusweb.me/nimbusnote.exe',
    ru: 'https://nimbusweb.me/nimbusnote.exe',
  };
  this.chromeClipperUrl = {
    en: 'https://chrome.google.com/webstore/detail/web-clipper-nimbus/kiokdhlcmjagacmcgoikapbjmmhfchbi?hl=en',
    ru: 'https://chrome.google.com/webstore/detail/web-clipper-nimbus/kiokdhlcmjagacmcgoikapbjmmhfchbi?hl=ru',
  };
  this.operaClipperUrl = {
    en: 'https://addons.opera.com/en/extensions/details/nimbus-clipper/',
    ru: 'https://addons.opera.com/en/extensions/details/nimbus-clipper/',
  };
  this.firefoxClipperUrl = {
    en: 'http://nimbusweb.me/firefox-clipper/clipper.xpi',
    ru: 'http://nimbusweb.me/firefox-clipper/clipper.xpi',
  };
  this.chromeScreenShotUrl = {
    en: 'https://chrome.google.com/webstore/detail/nimbus-screenshot-screen/bpconcjcammlapcogcnnelfmaeghhagj?hl=en',
    ru: 'https://chrome.google.com/webstore/detail/nimbus-screenshot-screen/bpconcjcammlapcogcnnelfmaeghhagj?hl=ru',
  };
  this.operaScreenShotUrl = {
    en: 'https://addons.opera.com/en/extensions/details/nimbus-screen-capture/',
    ru: 'https://addons.opera.com/en/extensions/details/nimbus-screen-capture/',
  };
  this.firefoxScreenShotUrl = {
    en: 'https://addons.mozilla.org/en-US/firefox/addon/nimbus-screenshot/',
    ru: 'https://addons.mozilla.org/en-US/firefox/addon/nimbus-screenshot/',
  };
}]);