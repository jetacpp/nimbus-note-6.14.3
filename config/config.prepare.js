const fs = require('fs-extra');
const path = require('path');

const envFilePath = path.join(__dirname, '../dev/config.env.js');
let envConfig;
try {
  if (fs.existsSync(envFilePath)) {
    envConfig = require(envFilePath);
  }
} catch(err) {console.error(err)}

const destConfigPath = __dirname;
const commonConfigPath = path.join(destConfigPath, 'env', 'common');
const isProd = envConfig && envConfig.env && envConfig.env === 'prod';
const envConfigPath = isProd ?
  path.join(destConfigPath, 'env', 'prod') :
  path.join(destConfigPath, 'env', 'dev');

const SERVICE_FILE_NAME = 'config.service.electron.js';
const LOCAL_FILE_NAME = 'config.local.electron.js';

let platform = process.platform;
// platform = 'win32';

const editorClient = platform === 'win32' ? 'win_notes' : 'mac_notes';
const baseProtocol = isProd ? 'https://' : 'http://';

const contents = fs.readFileSync(path.join(envConfigPath, SERVICE_FILE_NAME), 'utf8');
const config = contents.replace(/{{protocol}}/gi, `${baseProtocol}`)
                      .replace(/{{platform}}/gi, `${platform}`)
                      .replace(/{{editorClient}}/gi, editorClient);

fs.writeFileSync(path.join(destConfigPath, SERVICE_FILE_NAME), config);
fs.copyFileSync(path.join(commonConfigPath, LOCAL_FILE_NAME), path.join(destConfigPath, LOCAL_FILE_NAME));