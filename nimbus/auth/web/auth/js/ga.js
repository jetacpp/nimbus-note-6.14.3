function getCurrentUrlSafe() {
  var currentUrlClean = document.location.href
  currentUrlClean = currentUrlClean.replace(/[&\?]email=[^&]+/, '')
  return currentUrlClean
}

$(document).ready(function() {
  if (window.gtag) {
    if (window.location.href.indexOf("int_source=top_button") > -1) {
      gtag('event', 'Top clicked', {
        eventCategory: 'Sign Up Path',
        eventAction: 'Top clicked',
      })
    } else if (window.location.href.indexOf("int_source=top_button_no_email") > -1) {
      gtag('event', 'Top (without email) clicked', {
        eventCategory: 'Sign Up Path',
        eventAction: 'Top (without email) clicked',
      })
    } else if (window.location.href.indexOf("int_source=page_center_button") > -1) {
      gtag('event', 'Page center clicked', {
        eventCategory: 'Sign Up Path',
        eventAction: 'Page center clicked',
      })
    } else if (window.location.href.indexOf("int_source=page_bottom_button") > -1) {
      gtag('event', 'Page bottom clicked', {
        eventCategory: 'Sign Up Path',
        eventAction: 'Page bottom clicked',
      })
    }
    $(".sign_up").click(function() {
      gtag('event', 'Clicked', {
        eventCategory: 'Sign Up',
        eventAction: 'Clicked',
        transport_type: 'beacon'
      })
    });

    $(window).on('sign-in', function(event, params) {
      gtag("event", "Sign in", {
        event_category: params.method,
        transport_type: 'beacon',
        "event_label": getCurrentUrlSafe()
      });
    })

    $(window).on('sign-up', function(event, params) {
      gtag("event", "Sign up", {
        event_category: params.method,
        transport_type: 'beacon',
        "event_label": getCurrentUrlSafe()
      });
    })

    $(window).on('password-reset', function(event) {
      gtag("event", "Password recovered", {
        transport_type: 'beacon'
      });
    })

    // error tracking
    $(window).on('sign-in-error', function(event, params) {
      gtag("event", "error", {
        event_category: "sign in: " + params.description,
        transport_type: 'beacon',
        event_label: getCurrentUrlSafe(),
        fatal: false
      });
    })
    $(window).on('sign-up-error', function(event, params) {
      gtag("event", "error", {
        event_category: "sign up: " + params.description,
        transport_type: 'beacon',
        event_label: getCurrentUrlSafe(),
        fatal: false
      });
    })
    $(window).on('otp-error', function(event, params) {
      gtag("event", "error", {
        event_category: "otp: " + params.description,
        transport_type: 'beacon',
        event_label: getCurrentUrlSafe(),
        fatal: false
      });
    })
    $(window).on('captcha-error', function(event, params) {
      gtag("event", "error", {
        event_category: "captcha: " + params.description,
        transport_type: 'beacon',
        event_label: getCurrentUrlSafe(),
        fatal: false
      });
    })
  }
});