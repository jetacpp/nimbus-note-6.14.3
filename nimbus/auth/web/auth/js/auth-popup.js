$(function() {
  if(window.top != window) {
    // disable scrolling  for iframe
    $("body").css("overflow", "hidden");
    $(".form_container").resize(function() {
      if(parent && parent.postMessage) {
        parent.postMessage({
          action: 'everhelper:authform:resized',
          body: {
            // one pixel for top border
            height: $(".form_container").height() + 1
          }
        }, '*');
      }
    });
  }
});