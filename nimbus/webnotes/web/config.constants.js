"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const notesList_actions_1 = require("./client/app/notesList/notesList.actions");
var EMOJI_CATEGORIES;
(function (EMOJI_CATEGORIES) {
    EMOJI_CATEGORIES["ACTIVITY"] = "activity";
    EMOJI_CATEGORIES["ANIMALS"] = "animals_and_nature";
    EMOJI_CATEGORIES["FLAGS"] = "flags";
    EMOJI_CATEGORIES["FOOD"] = "food_and_drink";
    EMOJI_CATEGORIES["OBJECTS"] = "objects";
    EMOJI_CATEGORIES["PEOPLES"] = "people";
    EMOJI_CATEGORIES["TRAVEL"] = "travel_and_places";
    EMOJI_CATEGORIES["SYMBOLS"] = "symbols";
})(EMOJI_CATEGORIES = exports.EMOJI_CATEGORIES || (exports.EMOJI_CATEGORIES = {}));
exports.EMOJI_CATEGORIES_ORDER = [
    EMOJI_CATEGORIES.PEOPLES,
    EMOJI_CATEGORIES.ANIMALS,
    EMOJI_CATEGORIES.FOOD,
    EMOJI_CATEGORIES.ACTIVITY,
    EMOJI_CATEGORIES.TRAVEL,
    EMOJI_CATEGORIES.FLAGS,
    EMOJI_CATEGORIES.OBJECTS,
    EMOJI_CATEGORIES.SYMBOLS,
];
exports.EMOJI_DEFAULT_CATEGORY = EMOJI_CATEGORIES.PEOPLES;
exports.EMOJI_POPUP_IN_ROW_COUNT = 7;
exports.NOTE_TITLE_LENGTH = 32;
exports.NOTE_SHORT_TEXT_LENGTH = 128;
exports.BACKEND_ITEMS_UPDATE_LIMIT = 100;
exports.CURRENT_RELEASE_DATE = '2020-07-14 00:00:00';
exports.CURRENT_RELEASE_POPUP_ID = 21;
exports.EMAIL_CONFIRMATION_DELAY = 2 * 24 * 60 * 60;
exports.USER_VAR_IGNORE_INTRO = 'ignoreIntro';
exports.USER_VAR_NOTE_APPEARANCE = 'noteAppearance';
exports.USER_VAR_NOTE_FULL_WIDTH = 'noteFullWidth';
exports.USER_VAR_IGNORE_SHARES_INFO = 'ignoreSharesInfo';
exports.USER_VAR_IGNORE_START_TOOLTIPS = 'ignoreStartTooltips';
exports.USER_VAR_COLOR_THEME = 'colorTheme';
exports.USER_VAR_HELP_TOOLTIPS_START_SHOWN = 'helpTooltipsStartShown';
exports.USER_VAR_HELP_TOOLTIPS_NOTE_SHOWN = 'helpTooltipsNoteShown';
exports.USER_VAR_DESKTOP_APPS_INFORMER_SHOWN = 'desktopAppsInformerShown';
exports.USER_VAR_LAST_OPENED_WORKSPACES = 'lastOpenedWorkspaces';
exports.USER_VAR_IS_OCR_ADVERT_SHOWN = 'isOcrAdvertShown';
exports.USER_VAR_PROFESSION_ASK_SHOWN = 'professionAskShown';
exports.USER_VAR_LAST_SEEN_RELEASE_POPUP_ID = 'lastSeenReleasePopupId';
exports.USER_VAR_COLUMNS_PROPERTIES = 'columnsProperties';
exports.USER_VAR_IS_PINNED_LEFT_BLOCK = 'isPinnedLeftBlock';
exports.USER_VAR_IS_SEARCH_FEATURE_VIEWED = 'isSearchFeatureViewed';
exports.USER_VAR_NOTES_LIST_VIEW_TYPE = 'notesListViewType';
exports.USER_VAR_NOTES_LIST_SORT_TYPE = 'notesListSortType';
exports.USER_VAR_NOTES_LIST_SORT_DESCENDING = 'notesListSortDescending';
exports.USER_VAR_EDITOR_MARKDOWN_ENABLED = 'markdownEnabled';
exports.USER_VAR_EMAIL_CONFIRMATION_TIMESTAMP = 'emailConfirmationTimestamp';
exports.storageToVariablesMap = {
    'current-theme': exports.USER_VAR_COLOR_THEME,
    'help-tooltips-start-shown': exports.USER_VAR_HELP_TOOLTIPS_START_SHOWN,
    'help-tooltips-note-shown': exports.USER_VAR_HELP_TOOLTIPS_NOTE_SHOWN,
    'start-tooltips-ignore-key': exports.USER_VAR_HELP_TOOLTIPS_START_SHOWN,
    'is-information-shown': exports.USER_VAR_DESKTOP_APPS_INFORMER_SHOWN,
    'is_ocr_advert_shown': exports.USER_VAR_IS_OCR_ADVERT_SHOWN,
    'nimbus-editor-profession-ask-shown': exports.USER_VAR_PROFESSION_ASK_SHOWN,
    'last-seen-release-popup-id': exports.USER_VAR_LAST_SEEN_RELEASE_POPUP_ID,
    'ngStorage-columns': exports.USER_VAR_COLUMNS_PROPERTIES,
    'ngStorage-isPinnedLeftBlock': exports.USER_VAR_IS_PINNED_LEFT_BLOCK,
    'search-feature-viewed': exports.USER_VAR_IS_SEARCH_FEATURE_VIEWED,
    'notes-list-view-type': exports.USER_VAR_NOTES_LIST_VIEW_TYPE,
    'notes-list-sort-type': exports.USER_VAR_NOTES_LIST_SORT_TYPE,
    'notes-list-sort-is-descending-type': exports.USER_VAR_NOTES_LIST_SORT_DESCENDING,
};
var COLOR_THEMES;
(function (COLOR_THEMES) {
    COLOR_THEMES[COLOR_THEMES["LIGHT"] = 0] = "LIGHT";
    COLOR_THEMES[COLOR_THEMES["DARK"] = 1] = "DARK";
})(COLOR_THEMES = exports.COLOR_THEMES || (exports.COLOR_THEMES = {}));
exports.DEFAULT_NOTE_APPEARANCE = { style: 'normal', size: 'normal' };
exports.DEFAULT_COLOR_THEME = COLOR_THEMES.LIGHT;
exports.DEFAULT_WORKSPACE_COLOR = 'C8C8C8';
exports.DEFAULT_USER_VARIABLES = {
    [exports.USER_VAR_IGNORE_INTRO]: false,
    [exports.USER_VAR_IGNORE_SHARES_INFO]: false,
    [exports.USER_VAR_IGNORE_START_TOOLTIPS]: false,
    [exports.USER_VAR_NOTE_APPEARANCE]: exports.DEFAULT_NOTE_APPEARANCE,
    [exports.USER_VAR_COLOR_THEME]: exports.DEFAULT_COLOR_THEME,
    [exports.USER_VAR_HELP_TOOLTIPS_START_SHOWN]: false,
    [exports.USER_VAR_HELP_TOOLTIPS_NOTE_SHOWN]: false,
    [exports.USER_VAR_DESKTOP_APPS_INFORMER_SHOWN]: false,
    [exports.USER_VAR_LAST_OPENED_WORKSPACES]: {},
    [exports.USER_VAR_IS_OCR_ADVERT_SHOWN]: false,
    [exports.USER_VAR_PROFESSION_ASK_SHOWN]: false,
    [exports.USER_VAR_LAST_SEEN_RELEASE_POPUP_ID]: -1,
    [exports.USER_VAR_NOTE_FULL_WIDTH]: false,
    [exports.USER_VAR_COLUMNS_PROPERTIES]: {
        sidebar: {
            width: 310,
        },
        notesList: {
            width: 336,
        },
        todosPanel: {
            width: 234,
        },
    },
    [exports.USER_VAR_IS_PINNED_LEFT_BLOCK]: true,
    [exports.USER_VAR_IS_SEARCH_FEATURE_VIEWED]: 0,
    [exports.USER_VAR_NOTES_LIST_VIEW_TYPE]: notesList_actions_1.NOTES_LIST_VIEW_TYPE.COMFORTABLE,
    [exports.USER_VAR_NOTES_LIST_SORT_TYPE]: 'dateUpdated',
    [exports.USER_VAR_NOTES_LIST_SORT_DESCENDING]: true,
    [exports.USER_VAR_EDITOR_MARKDOWN_ENABLED]: true,
    [exports.USER_VAR_EMAIL_CONFIRMATION_TIMESTAMP]: null,
};
exports.DAY_PARTS = {
    am: 'am',
    pm: 'pm',
};
var NOTE_ROLES;
(function (NOTE_ROLES) {
    NOTE_ROLES["TODO"] = "todo";
    NOTE_ROLES["SCREENCAST"] = "screencast";
    NOTE_ROLES["VIDEO"] = "video";
    NOTE_ROLES["STREAM"] = "streaming_video";
    NOTE_ROLES["WELCOME_GUIDE"] = "welcome_guide";
})(NOTE_ROLES = exports.NOTE_ROLES || (exports.NOTE_ROLES = {}));
exports.EMBED_VIDEO_SIZE = {
    width: 948,
    height: 515,
};
//# sourceMappingURL=config.constants.js.map