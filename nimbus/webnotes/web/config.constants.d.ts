import { NOTES_LIST_VIEW_TYPE } from "./client/app/notesList/notesList.actions";
export declare enum EMOJI_CATEGORIES {
    ACTIVITY = "activity",
    ANIMALS = "animals_and_nature",
    FLAGS = "flags",
    FOOD = "food_and_drink",
    OBJECTS = "objects",
    PEOPLES = "people",
    TRAVEL = "travel_and_places",
    SYMBOLS = "symbols"
}
export declare const EMOJI_CATEGORIES_ORDER: EMOJI_CATEGORIES[];
export declare const EMOJI_DEFAULT_CATEGORY: EMOJI_CATEGORIES;
export declare const EMOJI_POPUP_IN_ROW_COUNT = 7;
export declare const NOTE_TITLE_LENGTH = 32;
export declare const NOTE_SHORT_TEXT_LENGTH = 128;
export declare const BACKEND_ITEMS_UPDATE_LIMIT = 100;
export declare const CURRENT_RELEASE_DATE = "2020-07-14 00:00:00";
export declare const CURRENT_RELEASE_POPUP_ID = 21;
export declare const EMAIL_CONFIRMATION_DELAY: number;
export declare const USER_VAR_IGNORE_INTRO = "ignoreIntro";
export declare const USER_VAR_NOTE_APPEARANCE = "noteAppearance";
export declare const USER_VAR_NOTE_FULL_WIDTH = "noteFullWidth";
export declare const USER_VAR_IGNORE_SHARES_INFO = "ignoreSharesInfo";
export declare const USER_VAR_IGNORE_START_TOOLTIPS = "ignoreStartTooltips";
export declare const USER_VAR_COLOR_THEME = "colorTheme";
export declare const USER_VAR_HELP_TOOLTIPS_START_SHOWN = "helpTooltipsStartShown";
export declare const USER_VAR_HELP_TOOLTIPS_NOTE_SHOWN = "helpTooltipsNoteShown";
export declare const USER_VAR_DESKTOP_APPS_INFORMER_SHOWN = "desktopAppsInformerShown";
export declare const USER_VAR_LAST_OPENED_WORKSPACES = "lastOpenedWorkspaces";
export declare const USER_VAR_IS_OCR_ADVERT_SHOWN = "isOcrAdvertShown";
export declare const USER_VAR_PROFESSION_ASK_SHOWN = "professionAskShown";
export declare const USER_VAR_LAST_SEEN_RELEASE_POPUP_ID = "lastSeenReleasePopupId";
export declare const USER_VAR_COLUMNS_PROPERTIES = "columnsProperties";
export declare const USER_VAR_IS_PINNED_LEFT_BLOCK = "isPinnedLeftBlock";
export declare const USER_VAR_IS_SEARCH_FEATURE_VIEWED = "isSearchFeatureViewed";
export declare const USER_VAR_NOTES_LIST_VIEW_TYPE = "notesListViewType";
export declare const USER_VAR_NOTES_LIST_SORT_TYPE = "notesListSortType";
export declare const USER_VAR_NOTES_LIST_SORT_DESCENDING = "notesListSortDescending";
export declare const USER_VAR_EDITOR_MARKDOWN_ENABLED = "markdownEnabled";
export declare const USER_VAR_EMAIL_CONFIRMATION_TIMESTAMP = "emailConfirmationTimestamp";
export declare const storageToVariablesMap: {
    'current-theme': string;
    'help-tooltips-start-shown': string;
    'help-tooltips-note-shown': string;
    'start-tooltips-ignore-key': string;
    'is-information-shown': string;
    'is_ocr_advert_shown': string;
    'nimbus-editor-profession-ask-shown': string;
    'last-seen-release-popup-id': string;
    'ngStorage-columns': string;
    'ngStorage-isPinnedLeftBlock': string;
    'search-feature-viewed': string;
    'notes-list-view-type': string;
    'notes-list-sort-type': string;
    'notes-list-sort-is-descending-type': string;
};
export declare enum COLOR_THEMES {
    LIGHT = 0,
    DARK = 1
}
export declare const DEFAULT_NOTE_APPEARANCE: {
    style: string;
    size: string;
};
export declare const DEFAULT_COLOR_THEME: COLOR_THEMES;
export declare const DEFAULT_WORKSPACE_COLOR = "C8C8C8";
export declare const DEFAULT_USER_VARIABLES: {
    [USER_VAR_IGNORE_INTRO]: boolean;
    [USER_VAR_IGNORE_SHARES_INFO]: boolean;
    [USER_VAR_IGNORE_START_TOOLTIPS]: boolean;
    [USER_VAR_NOTE_APPEARANCE]: {
        style: string;
        size: string;
    };
    [USER_VAR_COLOR_THEME]: COLOR_THEMES;
    [USER_VAR_HELP_TOOLTIPS_START_SHOWN]: boolean;
    [USER_VAR_HELP_TOOLTIPS_NOTE_SHOWN]: boolean;
    [USER_VAR_DESKTOP_APPS_INFORMER_SHOWN]: boolean;
    [USER_VAR_LAST_OPENED_WORKSPACES]: {};
    [USER_VAR_IS_OCR_ADVERT_SHOWN]: boolean;
    [USER_VAR_PROFESSION_ASK_SHOWN]: boolean;
    [USER_VAR_LAST_SEEN_RELEASE_POPUP_ID]: number;
    [USER_VAR_NOTE_FULL_WIDTH]: boolean;
    [USER_VAR_COLUMNS_PROPERTIES]: {
        sidebar: {
            width: number;
        };
        notesList: {
            width: number;
        };
        todosPanel: {
            width: number;
        };
    };
    [USER_VAR_IS_PINNED_LEFT_BLOCK]: boolean;
    [USER_VAR_IS_SEARCH_FEATURE_VIEWED]: number;
    [USER_VAR_NOTES_LIST_VIEW_TYPE]: NOTES_LIST_VIEW_TYPE;
    [USER_VAR_NOTES_LIST_SORT_TYPE]: string;
    [USER_VAR_NOTES_LIST_SORT_DESCENDING]: boolean;
    [USER_VAR_EDITOR_MARKDOWN_ENABLED]: boolean;
    [USER_VAR_EMAIL_CONFIRMATION_TIMESTAMP]: any;
};
interface IDayParts {
    am: string;
    pm: string;
}
export declare const DAY_PARTS: IDayParts;
export declare enum NOTE_ROLES {
    TODO = "todo",
    SCREENCAST = "screencast",
    VIDEO = "video",
    STREAM = "streaming_video",
    WELCOME_GUIDE = "welcome_guide"
}
export declare const EMBED_VIDEO_SIZE: {
    width: number;
    height: number;
};
export {};
