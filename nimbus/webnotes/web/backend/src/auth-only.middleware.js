"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
let AuthOnlyMiddleware = class AuthOnlyMiddleware {
    resolve() {
        return (req, res, next) => {
            if (!req.session || !req.session.userId) {
                return res.status(404).json({});
            }
            next();
        };
    }
};
AuthOnlyMiddleware = tslib_1.__decorate([
    common_1.Injectable()
], AuthOnlyMiddleware);
exports.AuthOnlyMiddleware = AuthOnlyMiddleware;
//# sourceMappingURL=auth-only.middleware.js.map