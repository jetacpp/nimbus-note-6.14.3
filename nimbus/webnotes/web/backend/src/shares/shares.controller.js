"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
let SharesController = class SharesController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    getTrialRequest(req, shareId, nodeId, workspaceId, folderId, securityKey) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const noteClient = this.apiServiceService.getNoteClient(userId, sessionId);
            const bodyRequest = {
                workspaceId,
                parentId: folderId,
            };
            try {
                return yield noteClient.post(`/shares/${shareId}/copy?securityKey=${securityKey}&nodeId=${nodeId}`, bodyRequest);
            }
            catch (_a) {
                return null;
            }
        });
    }
};
tslib_1.__decorate([
    common_1.Post('/:shareId/copy'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Body('shareId')),
    tslib_1.__param(2, common_1.Body('nodeId')),
    tslib_1.__param(3, common_1.Body('workspaceId')),
    tslib_1.__param(4, common_1.Body('folderId')),
    tslib_1.__param(5, common_1.Body('securityKey')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], SharesController.prototype, "getTrialRequest", null);
SharesController = tslib_1.__decorate([
    common_1.Controller('/shares'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], SharesController);
exports.SharesController = SharesController;
//# sourceMappingURL=shares.controller.js.map