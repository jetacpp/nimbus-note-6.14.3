import { ApiServiceService } from '../services/api-service/api-service.service';
export declare class SharesController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    getTrialRequest(req: any, shareId: any, nodeId: any, workspaceId: any, folderId: any, securityKey: any): Promise<any>;
}
