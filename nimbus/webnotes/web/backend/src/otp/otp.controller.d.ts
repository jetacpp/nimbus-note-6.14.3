import { ApiServiceService } from '../services/api-service/api-service.service';
export declare class OtpController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    getIssue(req: any): Promise<any>;
    isOtpSetup(req: any): Promise<any>;
    setOtpTurnOn(req: any, issueToken: any, issueState: any): Promise<any>;
    setOtpTurnOff(req: any): Promise<any>;
}
