"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
const swagger_1 = require("@nestjs/swagger");
let OtpController = class OtpController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    getIssue(req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId: sessionUserId, sessionId } = req.session;
                const otpClient = this.apiServiceService.getOtpClient(sessionUserId, sessionId);
                return yield otpClient.post('/issue', { userId: sessionUserId });
            }
            catch (err) {
                console.error(err);
                throw new Error('UNKNOWN_ERROR');
            }
        });
    }
    isOtpSetup(req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId: sessionUserId, sessionId } = req.session;
                const otpClient = this.apiServiceService.getOtpClient(sessionUserId, sessionId);
                const { createdAt } = yield otpClient.get(`/setup/${sessionUserId}`);
                return createdAt !== undefined;
            }
            catch (err) {
                return null;
            }
        });
    }
    setOtpTurnOn(req, issueToken, issueState) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId: sessionUserId, sessionId } = req.session;
                const otpClient = this.apiServiceService.getOtpClient(sessionUserId, sessionId);
                return yield otpClient.post(`/setup/${sessionUserId}`, {
                    state: issueState,
                    token: issueToken,
                });
            }
            catch (error) {
                throw Object.assign({}, error, { name: 'OtpSetupCodeError' });
            }
        });
    }
    setOtpTurnOff(req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId: sessionUserId, sessionId } = req.session;
                const otpClient = this.apiServiceService.getOtpClient(sessionUserId, sessionId);
                return yield otpClient.delete(`/setup/${sessionUserId}`);
            }
            catch (err) {
                console.error(err);
                throw new Error('UNKNOWN_ERROR');
            }
        });
    }
};
tslib_1.__decorate([
    common_1.Post('/issue'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], OtpController.prototype, "getIssue", null);
tslib_1.__decorate([
    common_1.Get('/setup'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], OtpController.prototype, "isOtpSetup", null);
tslib_1.__decorate([
    common_1.Post('/setup'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Body('issueToken')),
    tslib_1.__param(2, common_1.Body('issueState')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], OtpController.prototype, "setOtpTurnOn", null);
tslib_1.__decorate([
    common_1.Delete('/setup'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], OtpController.prototype, "setOtpTurnOff", null);
OtpController = tslib_1.__decorate([
    swagger_1.ApiUseTags('OTP'),
    common_1.Controller('/otp'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], OtpController);
exports.OtpController = OtpController;
//# sourceMappingURL=otp.controller.js.map