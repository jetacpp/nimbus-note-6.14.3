"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const common_2 = require("@nestjs/common");
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const config = require("../../server/config");
let ErrorsInterceptor = class ErrorsInterceptor {
    intercept(context, call$) {
        return call$.pipe(operators_1.catchError((err) => {
            console.error(err);
            if (err.name && err.httpStatus ||
                (err.response && err.response.name && err.response.httpStatus)) {
                err = Object.assign({}, err, err.response);
                if (config.isDebug) {
                    return rxjs_1.throwError(new common_2.HttpException(err, err.httpStatus));
                }
                else {
                    const targetError = err.errors ? err.errors[0] : err;
                    return rxjs_1.throwError(new common_2.HttpException(targetError, err.httpStatus));
                }
            }
            return rxjs_1.throwError(new common_2.HttpException('Error', common_1.HttpStatus.INTERNAL_SERVER_ERROR));
        }));
    }
};
ErrorsInterceptor = tslib_1.__decorate([
    common_1.Injectable()
], ErrorsInterceptor);
exports.ErrorsInterceptor = ErrorsInterceptor;
//# sourceMappingURL=errors.interceptor.js.map