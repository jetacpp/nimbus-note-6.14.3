import { ApiServiceService } from '../services/api-service/api-service.service';
export declare class NotesController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    changeWorkspaceRequest(workspaceId: any, nodesIds: any, newWorkspaceId: any, parentId: any, isMove: any, nodeType: any, req: any): Promise<any[]>;
    duplicaterRequest(workspaceId: any, nodesIds: any, parentId: any, suffix: any, nodeType: any, req: any): Promise<any[]>;
    changeWorkspace({ workspaceId, nodesIds, newWorkspaceId, parentId, isMove, nodeType, noteClient, }: {
        workspaceId: any;
        nodesIds: any;
        newWorkspaceId: any;
        parentId: any;
        isMove: any;
        nodeType: any;
        noteClient: any;
    }): Promise<any[]>;
    private checkEncrypted;
    static getNotesCountForWs(noteClient: any, wsId: any): Promise<any>;
}
