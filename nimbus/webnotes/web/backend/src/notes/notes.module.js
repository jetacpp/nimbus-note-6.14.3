"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const services_module_1 = require("../services/services.module");
const notes_controller_1 = require("./notes.controller");
let NotesModule = class NotesModule {
};
NotesModule = tslib_1.__decorate([
    common_1.Module({
        imports: [services_module_1.ServicesModule],
        controllers: [notes_controller_1.NotesController],
    })
], NotesModule);
exports.NotesModule = NotesModule;
//# sourceMappingURL=notes.module.js.map