"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
let NotesController = class NotesController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    changeWorkspaceRequest(workspaceId, nodesIds, newWorkspaceId, parentId, isMove, nodeType, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const noteClient = this.apiServiceService.getNoteClient(userId, sessionId);
            return this.changeWorkspace({
                workspaceId,
                nodesIds,
                newWorkspaceId,
                parentId,
                isMove,
                nodeType,
                noteClient,
            });
        });
    }
    duplicaterRequest(workspaceId, nodesIds, parentId, suffix, nodeType, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const noteClient = this.apiServiceService.getNoteClient(userId, sessionId);
            const updatedNodes = [];
            for (const nodeId of nodesIds) {
                const node = yield noteClient.get([`/workspaces/??/notes/??`, workspaceId, nodeId]);
                const changes = { title: `${node.title} ${suffix}`, parentId };
                const copies = yield this.changeWorkspace({
                    workspaceId,
                    newWorkspaceId: workspaceId,
                    nodesIds: [node.globalId],
                    parentId: node.parentId,
                    isMove: false,
                    nodeType,
                    noteClient,
                });
                for (const copy of copies) {
                    const updated = yield noteClient.post([`/workspaces/??/notes/??`, workspaceId, copy.globalId], changes);
                    updatedNodes.push(updated);
                }
            }
            return updatedNodes;
        });
    }
    changeWorkspace({ workspaceId, nodesIds, newWorkspaceId, parentId, isMove, nodeType, noteClient, }) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const updatedNodes = [];
            const isHaveEncryptedNote = yield this.checkEncrypted(workspaceId, nodesIds, noteClient);
            if (isHaveEncryptedNote) {
                throw new common_1.HttpException({
                    name: 'HAVE_ENCRYPTED_NOTE',
                    httpStatus: common_1.HttpStatus.BAD_REQUEST,
                }, common_1.HttpStatus.BAD_REQUEST);
            }
            try {
                for (const nodeId of nodesIds) {
                    const data = yield noteClient.post([`/workspaces/??/notes/??/workspace`, workspaceId, nodeId], {
                        workspaceId: newWorkspaceId,
                        parentId,
                        remove: isMove,
                    });
                    updatedNodes.push(data);
                }
                return updatedNodes;
            }
            catch (error) {
                const newError = Object.assign({}, error, { nodeType, isMove });
                throw new common_1.HttpException(newError, common_1.HttpStatus.FORBIDDEN);
            }
        });
    }
    checkEncrypted(workspaceId, nodeIds, noteClient) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return Promise.all([
                noteClient.get([`/workspaces/??/notes`, workspaceId], {
                    filter: {
                        globalId: nodeIds,
                        type: 'note',
                        isEncrypted: true,
                    },
                })
                    .then(notes => notes.length > 0)
                    .catch(error => true),
                ...nodeIds.map((nodeId) => {
                    return noteClient.get([`/workspaces/??/notes`, workspaceId], {
                        filter: {
                            type: 'note',
                            isEncrypted: true,
                        },
                        qs: {
                            rootId: nodeId,
                        },
                    })
                        .then(notes => notes.length > 0)
                        .catch(error => true);
                }),
            ]).then(isEncryptedArray => isEncryptedArray.find(x => x === true));
        });
    }
    static getNotesCountForWs(noteClient, wsId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const notes = yield noteClient.get([`/workspaces/??/notes`, wsId], {
                onlyId: true,
                filter: {
                    type: 'note',
                    role: { $not: ['welcome_guide'] },
                    isImported: false,
                },
            });
            return notes.length;
        });
    }
};
tslib_1.__decorate([
    common_1.Post('/changeWorkspaceRequest'),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Body('nodesIds')),
    tslib_1.__param(2, common_1.Body('workspaceId')),
    tslib_1.__param(3, common_1.Body('parentId')),
    tslib_1.__param(4, common_1.Body('isMove')),
    tslib_1.__param(5, common_1.Body('nodeType')),
    tslib_1.__param(6, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object, Object, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], NotesController.prototype, "changeWorkspaceRequest", null);
tslib_1.__decorate([
    common_1.Post('/duplicateRequest'),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Body('nodesIds')),
    tslib_1.__param(2, common_1.Body('parentId')),
    tslib_1.__param(3, common_1.Body('suffix')),
    tslib_1.__param(4, common_1.Body('nodeType')),
    tslib_1.__param(5, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], NotesController.prototype, "duplicaterRequest", null);
NotesController = tslib_1.__decorate([
    common_1.Controller('/workspaces/:workspaceId'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], NotesController);
exports.NotesController = NotesController;
//# sourceMappingURL=notes.controller.js.map