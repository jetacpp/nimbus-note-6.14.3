"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const previews_controller_1 = require("./previews.controller");
const services_module_1 = require("../services/services.module");
let PreviewsModule = class PreviewsModule {
};
PreviewsModule = tslib_1.__decorate([
    common_1.Module({
        imports: [
            services_module_1.ServicesModule,
        ],
        controllers: [
            previews_controller_1.PreviewsController,
        ],
    })
], PreviewsModule);
exports.PreviewsModule = PreviewsModule;
//# sourceMappingURL=previews.module.js.map