import { ApiServiceService } from '../services/api-service/api-service.service';
import { PreviewCreateDTO } from './preview-create.dto';
import { PreviewDTO } from './preview.dto';
export declare class PreviewsController {
    private apiService;
    constructor(apiService: ApiServiceService);
    findAll(req: any, workspaceId: string, filter: string): Promise<PreviewDTO[]>;
    upsert(req: any, workspaceId: string, previewId: string, previewCreateDto: PreviewCreateDTO): Promise<PreviewDTO>;
    remove(req: any, workspaceId: string, previewId: string): Promise<any>;
}
