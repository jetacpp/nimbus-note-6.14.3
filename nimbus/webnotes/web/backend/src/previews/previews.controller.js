"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
const preview_create_dto_1 = require("./preview-create.dto");
const swagger_1 = require("@nestjs/swagger");
const preview_dto_1 = require("./preview.dto");
let PreviewsController = class PreviewsController {
    constructor(apiService) {
        this.apiService = apiService;
    }
    findAll(req, workspaceId, filter) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const noteClient = this.apiService.getNoteClient(userId, sessionId);
            return noteClient.get(['/workspaces/??/previews', workspaceId], {
                qs: {
                    filter,
                },
            });
        });
    }
    upsert(req, workspaceId, previewId, previewCreateDto) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const noteClient = this.apiService.getNoteClient(userId, sessionId);
            return noteClient.post(['/workspaces/??/previews/??', workspaceId, previewId], previewCreateDto);
        });
    }
    remove(req, workspaceId, previewId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const noteClient = this.apiService.getNoteClient(userId, sessionId);
            return noteClient.delete(['/workspaces/??/previews/??', workspaceId, previewId]);
        });
    }
};
tslib_1.__decorate([
    common_1.Get(),
    swagger_1.ApiOkResponse({
        description: 'previews list',
        type: preview_dto_1.PreviewDTO,
        isArray: true,
    }),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('workspaceId')),
    tslib_1.__param(2, common_1.Query('filter')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], PreviewsController.prototype, "findAll", null);
tslib_1.__decorate([
    common_1.Post(':previewId'),
    swagger_1.ApiCreatedResponse({
        description: 'upsert preview',
        type: preview_dto_1.PreviewDTO,
    }),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('workspaceId')),
    tslib_1.__param(2, common_1.Param('previewId')),
    tslib_1.__param(3, common_1.Body()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, String, String, preview_create_dto_1.PreviewCreateDTO]),
    tslib_1.__metadata("design:returntype", Promise)
], PreviewsController.prototype, "upsert", null);
tslib_1.__decorate([
    common_1.Delete(':previewId'),
    swagger_1.ApiOkResponse({
        description: 'remove preview',
    }),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('workspaceId')),
    tslib_1.__param(2, common_1.Param('previewId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], PreviewsController.prototype, "remove", null);
PreviewsController = tslib_1.__decorate([
    common_1.Controller('/workspaces/:workspaceId/previews'),
    swagger_1.ApiUseTags('previews'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], PreviewsController);
exports.PreviewsController = PreviewsController;
//# sourceMappingURL=previews.controller.js.map