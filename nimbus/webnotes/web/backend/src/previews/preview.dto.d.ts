export declare class PreviewDTO {
    noteGlobalId: string;
    attachmentGlobalId: string;
    updatedAt: number;
    createdAt: number;
}
