"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const swagger_1 = require("@nestjs/swagger");
class PreviewCreateDTO {
}
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", String)
], PreviewCreateDTO.prototype, "attachmentGlobalId", void 0);
exports.PreviewCreateDTO = PreviewCreateDTO;
//# sourceMappingURL=preview-create.dto.js.map