import { ApiServiceService } from '../services/api-service/api-service.service';
export declare class MentionsController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    recentRequest(workspaceId: any, req: any): Promise<any>;
    workspaceInfo(workspaceId: any, req: any): Promise<any>;
}
