"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
let MentionsController = class MentionsController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    recentRequest(workspaceId, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const notificationClient = this.apiServiceService.getNotificationClient(userId, sessionId);
            const recentMentions = yield notificationClient.get(['/workspaces/??/recentMentions', workspaceId]);
            return recentMentions;
        });
    }
    workspaceInfo(workspaceId, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId, sessionId } = req.session;
                const workspaceClient = this.apiServiceService.getWorkspaceClient(userId, sessionId);
                return yield workspaceClient.get(['/workspaces/??/members', workspaceId]);
            }
            catch (error) {
                throw Object.assign({}, error, { name: 'MentionWorkspaceAccessDenied' });
            }
        });
    }
};
tslib_1.__decorate([
    common_1.Get('/recentMentions'),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MentionsController.prototype, "recentRequest", null);
tslib_1.__decorate([
    common_1.Get('/workspaceInfo'),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MentionsController.prototype, "workspaceInfo", null);
MentionsController = tslib_1.__decorate([
    common_1.Controller('/workspaces/:workspaceId/mentions/'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], MentionsController);
exports.MentionsController = MentionsController;
//# sourceMappingURL=mentions.controller.js.map