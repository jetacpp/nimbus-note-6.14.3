"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const services_module_1 = require("../services/services.module");
const mentions_controller_1 = require("./mentions.controller");
let MentionsModule = class MentionsModule {
};
MentionsModule = tslib_1.__decorate([
    common_1.Module({
        imports: [services_module_1.ServicesModule],
        controllers: [mentions_controller_1.MentionsController],
    })
], MentionsModule);
exports.MentionsModule = MentionsModule;
//# sourceMappingURL=mentions.module.js.map