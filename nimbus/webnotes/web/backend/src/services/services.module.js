"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("./api-service/api-service.service");
let ServicesModule = class ServicesModule {
};
ServicesModule = tslib_1.__decorate([
    common_1.Module({
        providers: [api_service_service_1.ApiServiceService],
        exports: [api_service_service_1.ApiServiceService],
    })
], ServicesModule);
exports.ServicesModule = ServicesModule;
//# sourceMappingURL=services.module.js.map