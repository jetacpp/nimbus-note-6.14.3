import APIFactory = require('eh-api-client');
export declare class ApiServiceService {
    userClientFactory: APIFactory;
    noteClientFactory: APIFactory;
    fileClientFactory: APIFactory;
    workspaceClientFactory: APIFactory;
    organizationClientFactory: APIFactory;
    trialClientFactory: APIFactory;
    annotationClientFactory: APIFactory;
    notificationClientFactory: APIFactory;
    surveyClientFactory: APIFactory;
    mailClientFactory: APIFactory;
    otpClientFactory: APIFactory;
    getUserClient(userId: number, sessionId: string, app?: string): any;
    getWorkspaceClient(userId: number, sessionId: string, app?: string): any;
    getFileClient(userId: number, sessionId: string, app?: string): any;
    getNoteClient(userId: number, sessionId: string, app?: string): any;
    getOrganizationClient(userId: number, sessionId: string, app?: string): any;
    getTrialClient(userId: number, sessionId: string, app?: string): any;
    getAnnotationClient(userId: number, sessionId: string, app?: string): any;
    getNotificationClient(userId: number, sessionId: string, app?: string): any;
    getSurveyClient(userId: number, sessionId: string, app?: string): any;
    getMailClient(userId: number, sessionId: string, app?: string): any;
    getOtpClient(userId: number, sessionId: string, app?: string): any;
}
