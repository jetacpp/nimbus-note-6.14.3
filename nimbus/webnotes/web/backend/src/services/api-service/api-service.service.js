"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const APIFactory = require("eh-api-client");
const config = require("../../../../server/config");
let ApiServiceService = class ApiServiceService {
    constructor() {
        this.userClientFactory = new APIFactory(config.services.user);
        this.noteClientFactory = new APIFactory(config.services.note);
        this.fileClientFactory = new APIFactory(config.services.file);
        this.workspaceClientFactory = new APIFactory(config.services.workspace);
        this.organizationClientFactory = new APIFactory(config.services.org);
        this.trialClientFactory = new APIFactory(config.services.trial);
        this.annotationClientFactory = new APIFactory(config.services.annotation);
        this.notificationClientFactory = new APIFactory(config.services.notification);
        this.surveyClientFactory = new APIFactory(config.services.survey);
        this.mailClientFactory = new APIFactory(config.services.mail);
        this.otpClientFactory = new APIFactory(config.services.otp);
    }
    getUserClient(userId, sessionId, app = 'web') {
        const client = this.userClientFactory.getClient(userId, app);
        client.setSessionId(sessionId);
        return client;
    }
    getWorkspaceClient(userId, sessionId, app = 'web') {
        const client = this.workspaceClientFactory.getClient(userId, app);
        client.setSessionId(sessionId);
        return client;
    }
    getFileClient(userId, sessionId, app = 'web') {
        const client = this.fileClientFactory.getClient(userId, app);
        client.setSessionId(sessionId);
        return client;
    }
    getNoteClient(userId, sessionId, app = 'web') {
        const client = this.noteClientFactory.getClient(userId, app);
        client.setSessionId(sessionId);
        return client;
    }
    getOrganizationClient(userId, sessionId, app = 'web') {
        const client = this.organizationClientFactory.getClient(userId, app);
        client.setSessionId(sessionId);
        return client;
    }
    getTrialClient(userId, sessionId, app = 'web') {
        const client = this.trialClientFactory.getClient(userId, app);
        client.setSessionId(sessionId);
        return client;
    }
    getAnnotationClient(userId, sessionId, app = 'web') {
        const client = this.annotationClientFactory.getClient(userId, app);
        client.setSessionId(sessionId);
        return client;
    }
    getNotificationClient(userId, sessionId, app = 'web') {
        const client = this.notificationClientFactory.getClient(userId, app);
        client.setSessionId(sessionId);
        return client;
    }
    getSurveyClient(userId, sessionId, app = 'web') {
        const client = this.surveyClientFactory.getClient(userId, app);
        client.setSessionId(sessionId);
        return client;
    }
    getMailClient(userId, sessionId, app = 'web') {
        const client = this.mailClientFactory.getClient(userId, app);
        client.setSessionId(sessionId);
        return client;
    }
    getOtpClient(userId, sessionId, app = 'web') {
        const client = this.otpClientFactory.getClient(userId, app);
        client.setSessionId(sessionId);
        return client;
    }
};
ApiServiceService = tslib_1.__decorate([
    common_1.Injectable()
], ApiServiceService);
exports.ApiServiceService = ApiServiceService;
//# sourceMappingURL=api-service.service.js.map