export declare class SurveyResponseDTO {
    userId: number;
    date: number;
    value: string;
}
