"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const services_module_1 = require("../services/services.module");
const survey_controller_1 = require("./survey.controller");
let SurveyModule = class SurveyModule {
};
SurveyModule = tslib_1.__decorate([
    common_1.Module({
        imports: [services_module_1.ServicesModule],
        controllers: [survey_controller_1.SurveyController],
    })
], SurveyModule);
exports.SurveyModule = SurveyModule;
//# sourceMappingURL=survey.module.js.map