import { ApiServiceService } from '../services/api-service/api-service.service';
export declare class SurveyController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    getProfessionAnswer(req: any): Promise<any>;
    sendProfessionAnswer(req: any, answer: any): Promise<any>;
}
