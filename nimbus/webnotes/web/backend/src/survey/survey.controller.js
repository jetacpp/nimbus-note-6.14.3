"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
const swagger_1 = require("@nestjs/swagger");
const survey_response_dto_1 = require("./dto/survey.response.dto");
let SurveyController = class SurveyController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    getProfessionAnswer(req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId, sessionId } = req.session;
                const surveyClient = this.apiServiceService.getSurveyClient(userId, sessionId);
                return yield surveyClient.get('/surveys/professions/answer');
            }
            catch (_a) {
                return null;
            }
        });
    }
    sendProfessionAnswer(req, answer) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId, sessionId } = req.session;
                const surveyClient = this.apiServiceService.getSurveyClient(userId, sessionId);
                return yield surveyClient.post('/surveys/professions/answer', {
                    value: answer.answer,
                });
            }
            catch (err) {
                console.error(err);
                throw new Error('UNKNOWN_ERROR');
            }
        });
    }
};
tslib_1.__decorate([
    common_1.Get('/profession'),
    swagger_1.ApiOkResponse({
        description: 'Get user answer about profession question',
        type: survey_response_dto_1.SurveyResponseDTO,
    }),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], SurveyController.prototype, "getProfessionAnswer", null);
tslib_1.__decorate([
    common_1.Post('/profession'),
    swagger_1.ApiOkResponse({
        description: 'Send answer about profession question',
        type: survey_response_dto_1.SurveyResponseDTO,
    }),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Body()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], SurveyController.prototype, "sendProfessionAnswer", null);
SurveyController = tslib_1.__decorate([
    swagger_1.ApiUseTags('Survey'),
    common_1.Controller('/survey'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], SurveyController);
exports.SurveyController = SurveyController;
//# sourceMappingURL=survey.controller.js.map