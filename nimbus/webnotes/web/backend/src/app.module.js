"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mails_controller_1 = require("./mails/mails.controller");
const common_1 = require("@nestjs/common");
const auth_only_middleware_1 = require("./auth-only.middleware");
const annotations_controller_1 = require("./annotations/annotations.controller");
const annotations_module_1 = require("./annotations/annotations.module");
const backups_controller_1 = require("./backups/backups.controller");
const backups_module_1 = require("./backups/backups.module");
const members_controller_1 = require("./workspaces/members/members.controller");
const mentions_controller_1 = require("./mentions/mentions.controller");
const mentions_module_1 = require("./mentions/mentions.module");
const notes_controller_1 = require("./notes/notes.controller");
const notes_module_1 = require("./notes/notes.module");
const organizations_controller_1 = require("./organizations/organizations.controller");
const organizations_module_1 = require("./organizations/organizations.module");
const otp_controller_1 = require("./otp/otp.controller");
const otp_module_1 = require("./otp/otp.module");
const previews_controller_1 = require("./previews/previews.controller");
const previews_module_1 = require("./previews/previews.module");
const shares_controller_1 = require("./shares/shares.controller");
const shares_module_1 = require("./shares/shares.module");
const survey_controller_1 = require("./survey/survey.controller");
const survey_module_1 = require("./survey/survey.module");
const trial_controller_1 = require("./trial/trial.controller");
const trial_module_1 = require("./trial/trial.module");
const users_controller_1 = require("./users/users.controller");
const users_module_1 = require("./users/users.module");
const workspaces_controller_1 = require("./workspaces/workspaces/workspaces.controller");
const workspaces_module_1 = require("./workspaces/workspaces.module");
const mails_module_1 = require("./mails/mails.module");
let AppModule = class AppModule {
    configure(consumer) {
        const authOnlyControllers = [
            annotations_controller_1.AnnotationsController,
            backups_controller_1.BackupsController,
            mentions_controller_1.MentionsController,
            members_controller_1.MembersController,
            notes_controller_1.NotesController,
            organizations_controller_1.OrganizationsController,
            otp_controller_1.OtpController,
            previews_controller_1.PreviewsController,
            shares_controller_1.SharesController,
            survey_controller_1.SurveyController,
            trial_controller_1.TrialController,
            users_controller_1.UsersController,
            workspaces_controller_1.WorkspacesController,
            mails_controller_1.MailsController,
        ];
        for (const controller of authOnlyControllers) {
            consumer.apply(auth_only_middleware_1.AuthOnlyMiddleware).forRoutes(controller);
        }
    }
};
AppModule = tslib_1.__decorate([
    common_1.Module({
        imports: [
            annotations_module_1.AnnotationsModule,
            backups_module_1.BackupsModule,
            mentions_module_1.MentionsModule,
            notes_module_1.NotesModule,
            organizations_module_1.OrganizationsModule,
            otp_module_1.OtpModule,
            previews_module_1.PreviewsModule,
            shares_module_1.SharesModule,
            survey_module_1.SurveyModule,
            trial_module_1.TrialModule,
            users_module_1.UsersModule,
            workspaces_module_1.WorkspacesModule,
            mails_module_1.MailsModule,
        ],
        controllers: [],
        providers: [],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map