"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
var WorkspacesController_1;
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../../services/api-service/api-service.service");
let WorkspacesController = WorkspacesController_1 = class WorkspacesController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    findAll(req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const workspaceClient = this.apiServiceService.getWorkspaceClient(userId, sessionId);
            const userClient = this.apiServiceService.getUserClient(userId, sessionId);
            return WorkspacesController_1.getWorkspaces(workspaceClient, userClient, userId);
        });
    }
    getUserPremium(req, workspaceId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const emptyPremium = { status: null, dateEnd: null };
            try {
                const { userId, sessionId } = req.session;
                const workspaceClient = this.apiServiceService.getWorkspaceClient(userId, sessionId);
                const userClient = this.apiServiceService.getUserClient(userId, sessionId);
                const workspace = yield workspaceClient.get([`/workspaces/??`, workspaceId]);
                const premiums = yield userClient.get(['/users/??/premiums', workspace.userId]);
                const activeCapture = premiums.find(p => p.status === 'active' && p.type === 'capture');
                const activeEverhelper = premiums.find(p => p.status === 'active' && p.type === 'everhelper');
                return activeEverhelper || activeCapture || emptyPremium;
            }
            catch (error) {
                return emptyPremium;
            }
        });
    }
    getWorkspaceEmails(req, workspaceId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId, sessionId } = req.session;
                const noteClient = this.apiServiceService.getNoteClient(userId, sessionId);
                return yield noteClient.get([`/workspaceEmails/??`, workspaceId]);
            }
            catch (error) {
                return { status: null, dateEnd: null };
            }
        });
    }
    upsertWorkspace(req, workspaceId, upsertWorkspace) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const workspaceClient = this.apiServiceService.getWorkspaceClient(userId, sessionId);
            const userClient = this.apiServiceService.getUserClient(userId, sessionId);
            const workspace = yield workspaceClient.post([`/workspaces/??`, workspaceId], upsertWorkspace);
            const [result] = yield WorkspacesController_1.addUserAndUsageInfo([workspace], workspaceClient, userClient, userId);
            return result;
        });
    }
    createWorkspace(req, createWorkspace) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const workspaceClient = this.apiServiceService.getWorkspaceClient(userId, sessionId);
            const userClient = this.apiServiceService.getUserClient(userId, sessionId);
            const workspace = yield workspaceClient.post([`/workspaces`], createWorkspace);
            const [result] = yield WorkspacesController_1.addUserAndUsageInfo([workspace], workspaceClient, userClient, userId);
            return result;
        });
    }
    deleteWorkspace(req, workspaceId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const workspaceClient = this.apiServiceService.getWorkspaceClient(userId, sessionId);
            return workspaceClient.delete([`/workspaces/??`, workspaceId]);
        });
    }
    static getWorkspaces(workspaceClient, userClient, userId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const result = yield workspaceClient.get([`/workspaces`]);
            return WorkspacesController_1.addUserAndUsageInfo(result, workspaceClient, userClient, userId);
        });
    }
    static getWorkspacesAccess(workspaceClient) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield workspaceClient.get([`/workspacesAccess`]);
        });
    }
    static addUserAndUsageInfo(result, workspaceClient, userClient, currentUserId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!result || !result.length) {
                return [];
            }
            try {
                const workspaceIds = result.map((workspace) => workspace.globalId);
                const ownerIds = result.map((workspace) => workspace.userId);
                const workspaceUsagePromises = workspaceIds.map((workspaceId) => {
                    return workspaceClient.get([`/workspaces/??/usage`, workspaceId]);
                });
                const workspaceAvatarsPromises = workspaceIds.map((workspaceId) => {
                    return workspaceClient.get([`/workspaces/??/avatar`, workspaceId]).catch((error) => {
                        if (error.name === 'NotFound' && error.httpStatus === 404) {
                            return { storedFileUUID: null };
                        }
                        throw error;
                    });
                });
                const ownerPromise = userClient.get([`/users/`], {
                    filter: { id: ownerIds },
                });
                const workspaceAvatars = yield Promise.all(workspaceAvatarsPromises);
                const [owners, ...workspaceUsages] = yield Promise.all([
                    ownerPromise,
                    ...workspaceUsagePromises,
                ]);
                return result.map((workspace, index) => {
                    return Object.assign({}, workspace, { avatar: workspaceAvatars[index], usage: workspaceUsages[index], owner: owners.find((user) => user.id === workspace.userId), isDefaultForCurrentUser: workspace.isDefault && workspace.userId === currentUserId });
                });
            }
            catch (err) {
                console.error(err);
                return result;
            }
        });
    }
};
tslib_1.__decorate([
    common_1.Get(),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], WorkspacesController.prototype, "findAll", null);
tslib_1.__decorate([
    common_1.Get('/:workspaceId/premium'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('workspaceId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], WorkspacesController.prototype, "getUserPremium", null);
tslib_1.__decorate([
    common_1.Get('/:workspaceId/emails'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('workspaceId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], WorkspacesController.prototype, "getWorkspaceEmails", null);
tslib_1.__decorate([
    common_1.Post('/:workspaceId'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('workspaceId')),
    tslib_1.__param(2, common_1.Body()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], WorkspacesController.prototype, "upsertWorkspace", null);
tslib_1.__decorate([
    common_1.Post(''),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Body()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], WorkspacesController.prototype, "createWorkspace", null);
tslib_1.__decorate([
    common_1.Delete('/:workspaceId'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('workspaceId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], WorkspacesController.prototype, "deleteWorkspace", null);
WorkspacesController = WorkspacesController_1 = tslib_1.__decorate([
    common_1.Controller('workspaces'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], WorkspacesController);
exports.WorkspacesController = WorkspacesController;
//# sourceMappingURL=workspaces.controller.js.map