import { ApiServiceService } from '../../services/api-service/api-service.service';
export declare class WorkspacesController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    findAll(req: any): Promise<any[]>;
    getUserPremium(req: any, workspaceId: any): Promise<any>;
    getWorkspaceEmails(req: any, workspaceId: any): Promise<any>;
    upsertWorkspace(req: any, workspaceId: any, upsertWorkspace: any): Promise<any>;
    createWorkspace(req: any, createWorkspace: any): Promise<any>;
    deleteWorkspace(req: any, workspaceId: any): Promise<any>;
    static getWorkspaces(workspaceClient: any, userClient: any, userId: any): Promise<any>;
    static getWorkspacesAccess(workspaceClient: any): Promise<any>;
    static addUserAndUsageInfo(result: any, workspaceClient: any, userClient: any, currentUserId: any): Promise<any>;
}
