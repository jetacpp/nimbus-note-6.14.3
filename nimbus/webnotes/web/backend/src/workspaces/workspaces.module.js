"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const members_controller_1 = require("./members/members.controller");
const services_module_1 = require("../services/services.module");
const workspaces_controller_1 = require("./workspaces/workspaces.controller");
let WorkspacesModule = class WorkspacesModule {
};
WorkspacesModule = tslib_1.__decorate([
    common_1.Module({
        imports: [services_module_1.ServicesModule],
        controllers: [members_controller_1.MembersController, workspaces_controller_1.WorkspacesController],
    })
], WorkspacesModule);
exports.WorkspacesModule = WorkspacesModule;
//# sourceMappingURL=workspaces.module.js.map