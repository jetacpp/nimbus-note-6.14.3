"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
var MembersController_1;
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../../services/api-service/api-service.service");
let MembersController = MembersController_1 = class MembersController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    findAllForWorkspace(workspaceId, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const workspaceClient = this.apiServiceService.getWorkspaceClient(userId, sessionId);
            const userClient = this.apiServiceService.getUserClient(userId, sessionId);
            const result = yield workspaceClient.get([`/workspaces/??/members`, workspaceId]);
            return MembersController_1.addUserInfoToMembers(result, userClient);
        });
    }
    updateWorkspaceMember(memberId, role, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const workspaceClient = this.apiServiceService.getWorkspaceClient(userId, sessionId);
            const userClient = this.apiServiceService.getUserClient(userId, sessionId);
            const member = yield workspaceClient.post([`/members/??`, memberId], {
                role,
            });
            return MembersController_1.addUserInfoToMember(member, userClient);
        });
    }
    removeMemberFromWorkspace(memberId, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const workspaceClient = this.apiServiceService.getWorkspaceClient(userId, sessionId);
            return yield workspaceClient.delete([`/members/??`, memberId]);
        });
    }
    static addUserInfoToMember(member, userClient) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const userInfo = yield userClient.get([`/users/??`, member.userId]);
            return Object.assign({}, member, {
                firstname: userInfo.firstname,
                lastname: userInfo.lastname,
                email: userInfo.email,
                username: userInfo.username,
            });
        });
    }
    static addUserInfoToMembers(members = [], userClient) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const usersIds = members.map((member) => member.userId);
            const users = yield userClient.get({
                url: `/users/`,
                filter: {
                    id: usersIds,
                },
            });
            const avatars = yield Promise.all(members.map(member => {
                return userClient.get([`/avatars/??`, member.userId]).catch((error) => {
                    if (error.name === 'NotFound' && error.httpStatus === 404) {
                        return { storedFileUUID: null };
                    }
                    throw error;
                });
            }));
            return members.map((member, idx) => {
                const userFound = users.find((user) => user.id === member.userId);
                if (!userFound) {
                    return Object.assign({}, member, { avatar: avatars[idx] });
                }
                return Object.assign({}, member, {
                    firstname: userFound.firstname,
                    lastname: userFound.lastname,
                    email: userFound.email,
                    username: userFound.username,
                    avatar: avatars[idx],
                });
            });
        });
    }
};
tslib_1.__decorate([
    common_1.Get(),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MembersController.prototype, "findAllForWorkspace", null);
tslib_1.__decorate([
    common_1.Post(),
    tslib_1.__param(0, common_1.Param('memberId')),
    tslib_1.__param(1, common_1.Body('role')),
    tslib_1.__param(2, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MembersController.prototype, "updateWorkspaceMember", null);
tslib_1.__decorate([
    common_1.Delete(),
    tslib_1.__param(0, common_1.Param('memberId')),
    tslib_1.__param(1, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MembersController.prototype, "removeMemberFromWorkspace", null);
MembersController = MembersController_1 = tslib_1.__decorate([
    common_1.Controller('/workspaces/:workspaceId/members/:memberId?'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], MembersController);
exports.MembersController = MembersController;
//# sourceMappingURL=members.controller.js.map