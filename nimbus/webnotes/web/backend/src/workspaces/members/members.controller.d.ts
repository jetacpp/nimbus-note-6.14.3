import { ApiServiceService } from '../../services/api-service/api-service.service';
export declare class MembersController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    findAllForWorkspace(workspaceId: any, req: any): Promise<any[]>;
    updateWorkspaceMember(memberId: any, role: any, req: any): Promise<any[]>;
    removeMemberFromWorkspace(memberId: any, req: any): Promise<any[]>;
    static addUserInfoToMember(member: any, userClient: any): Promise<any>;
    static addUserInfoToMembers(members: any[], userClient: any): Promise<any[]>;
}
