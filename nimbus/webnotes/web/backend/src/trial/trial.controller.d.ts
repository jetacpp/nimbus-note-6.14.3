import { ApiServiceService } from '../services/api-service/api-service.service';
export declare class TrialController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    getTrialRequest(req: any): Promise<any>;
    setTrialRequest(req: any): Promise<any>;
    static getTrial(trialClient: any): Promise<any>;
}
