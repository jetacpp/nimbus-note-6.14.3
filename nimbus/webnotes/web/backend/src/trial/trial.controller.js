"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
var TrialController_1;
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
let TrialController = TrialController_1 = class TrialController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    getTrialRequest(req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const trialClient = this.apiServiceService.getTrialClient(userId, sessionId);
            const result = yield TrialController_1.getTrial(trialClient);
            return result;
        });
    }
    setTrialRequest(req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const trialClient = this.apiServiceService.getTrialClient(userId, sessionId);
            const currentTrial = yield this.getTrialRequest(req);
            if (currentTrial.state === 'active') {
                return currentTrial;
            }
            else {
                const newTrialPayload = {
                    service: 'web',
                    duration: 0,
                };
                const newTrial = yield trialClient.post(['/proTrial'], newTrialPayload);
                return newTrial;
            }
        });
    }
    static getTrial(trialClient) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const trial = yield trialClient.get(['/proTrial']);
                return trial;
            }
            catch (_a) {
                return {
                    state: null,
                    dateStart: 0,
                    dateUntil: 0,
                    daysRemain: 0,
                    requestorService: null
                };
            }
        });
    }
};
tslib_1.__decorate([
    common_1.Get('/proTrial'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], TrialController.prototype, "getTrialRequest", null);
tslib_1.__decorate([
    common_1.Post('/proTrial'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], TrialController.prototype, "setTrialRequest", null);
TrialController = TrialController_1 = tslib_1.__decorate([
    common_1.Controller('/trial'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], TrialController);
exports.TrialController = TrialController;
//# sourceMappingURL=trial.controller.js.map