"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const services_module_1 = require("../services/services.module");
const trial_controller_1 = require("./trial.controller");
let TrialModule = class TrialModule {
};
TrialModule = tslib_1.__decorate([
    common_1.Module({
        imports: [services_module_1.ServicesModule],
        controllers: [trial_controller_1.TrialController],
    })
], TrialModule);
exports.TrialModule = TrialModule;
//# sourceMappingURL=trial.module.js.map