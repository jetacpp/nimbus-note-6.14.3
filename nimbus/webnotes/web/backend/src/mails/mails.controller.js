"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
const swagger_1 = require("@nestjs/swagger");
let MailsController = class MailsController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    sendMailAdmin(req, body) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const mailClient = this.apiServiceService.getMailClient(userId, sessionId);
            const { title, html } = body.mailOptions;
            return yield mailClient.post(`/admin`, { title, html });
        });
    }
    sendMailConfirm(req, body) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const userClient = this.apiServiceService.getUserClient(userId, sessionId);
            return yield userClient.post(`/resendEmailConfirmation`, {});
        });
    }
};
tslib_1.__decorate([
    common_1.Post('/admin'),
    swagger_1.ApiResponse({
        status: 200,
        description: 'Send mail to admin',
        isArray: true,
    }),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Body()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MailsController.prototype, "sendMailAdmin", null);
tslib_1.__decorate([
    common_1.Post('/confirm'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Body()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MailsController.prototype, "sendMailConfirm", null);
MailsController = tslib_1.__decorate([
    common_1.Controller('mails'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], MailsController);
exports.MailsController = MailsController;
//# sourceMappingURL=mails.controller.js.map