"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mails_controller_1 = require("./mails.controller");
const common_1 = require("@nestjs/common");
const services_module_1 = require("../services/services.module");
let MailsModule = class MailsModule {
};
MailsModule = tslib_1.__decorate([
    common_1.Module({
        imports: [services_module_1.ServicesModule],
        controllers: [mails_controller_1.MailsController],
    })
], MailsModule);
exports.MailsModule = MailsModule;
//# sourceMappingURL=mails.module.js.map