import { ApiServiceService } from '../services/api-service/api-service.service';
export declare class MailsController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    sendMailAdmin(req: any, body: any): Promise<any>;
    sendMailConfirm(req: any, body: any): Promise<any>;
}
