import { MiddlewareFunction, NestMiddleware } from '@nestjs/common';
export declare class AuthOnlyMiddleware implements NestMiddleware {
    resolve(): MiddlewareFunction;
}
