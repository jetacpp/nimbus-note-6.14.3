"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
const swagger_1 = require("@nestjs/swagger");
const annotation_attachment_dto_1 = require("./dto/annotation.attachment.dto");
let AnnotationsController = class AnnotationsController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    findAll(workspaceId, filter, query, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const annotationClient = this.apiServiceService.getAnnotationClient(userId, sessionId);
            const noteClient = this.apiServiceService.getNoteClient(userId, sessionId);
            const { items } = yield annotationClient.get(['/workspaces/??/search', workspaceId], {
                qs: {
                    query,
                    filter,
                },
            });
            const attachmentsIds = items.map(item => item.annotation.attachmentGlobalId);
            const attachmentsFilter = JSON.stringify({
                globalId: { $in: attachmentsIds },
            });
            const attachments = yield noteClient.get(['/workspaces/??/attachments', workspaceId], {
                qs: {
                    filter: attachmentsFilter,
                },
            });
            const attachmentsMap = this.prepareMap(attachments, 'globalId');
            const excerpts = yield Promise.all([
                ...attachmentsIds.map((attachmentId) => {
                    return annotationClient.get(['/workspaces/??/annotations/??/excerpt', workspaceId, attachmentId], {
                        qs: {
                            query,
                        },
                    });
                }),
            ]);
            const excerptsMap = excerpts.map((item, index) => {
                const excerpt = {};
                excerpt[attachmentsIds[index]] = item.excerpt;
                return excerpt;
            }).reduce((map, item) => {
                map = Object.assign({}, map, item);
                return map;
            }, {});
            return items.map((item) => {
                return Object.assign({}, item.annotation, { attachment: attachmentsMap[item.annotation.attachmentGlobalId], excerpt: excerptsMap[item.annotation.attachmentGlobalId] });
            });
        });
    }
    getAttachmentAnnotation(workspaceId, attachmentId, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            try {
                const annotationClient = this.apiServiceService.getAnnotationClient(userId, sessionId);
                return yield annotationClient.get([`/workspaces/??/annotations/??`, workspaceId, attachmentId]);
            }
            catch (error) {
                console.error(error);
            }
            return [];
        });
    }
    updateAttachmentAnnotation(workspaceId, attachmentId, annotation, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            try {
                const annotationClient = this.apiServiceService.getAnnotationClient(userId, sessionId);
                return yield annotationClient.post([`/workspaces/??/annotations/??`, workspaceId, attachmentId], annotation);
            }
            catch (error) {
                console.error(error);
            }
            return [];
        });
    }
    prepareMap(array, field) {
        return array.reduce((map, item) => {
            const id = item[field];
            if (!id) {
                return map;
            }
            return Object.assign({ [id]: item }, map);
        }, {});
    }
};
tslib_1.__decorate([
    common_1.Get(),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Query('filter')),
    tslib_1.__param(2, common_1.Query('query')),
    tslib_1.__param(3, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, String, String, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AnnotationsController.prototype, "findAll", null);
tslib_1.__decorate([
    common_1.Get(':attachmentId'),
    swagger_1.ApiOkResponse({
        description: 'get attachment annotation',
        type: annotation_attachment_dto_1.AnnotationAttachmentDTO,
        isArray: true,
    }),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Param('attachmentId')),
    tslib_1.__param(2, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AnnotationsController.prototype, "getAttachmentAnnotation", null);
tslib_1.__decorate([
    common_1.Post(':attachmentId'),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Param('attachmentId')),
    tslib_1.__param(2, common_1.Body()),
    tslib_1.__param(3, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String, annotation_attachment_dto_1.AnnotationAttachmentDTO, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AnnotationsController.prototype, "updateAttachmentAnnotation", null);
AnnotationsController = tslib_1.__decorate([
    common_1.Controller('/workspaces/:workspaceId/annotations/'),
    swagger_1.ApiUseTags('annotations'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], AnnotationsController);
exports.AnnotationsController = AnnotationsController;
//# sourceMappingURL=annotations.controller.js.map