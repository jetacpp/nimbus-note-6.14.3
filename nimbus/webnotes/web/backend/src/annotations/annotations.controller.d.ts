import { ApiServiceService } from '../services/api-service/api-service.service';
import { AnnotationAttachmentDTO } from './dto/annotation.attachment.dto';
export declare class AnnotationsController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    findAll(workspaceId: any, filter: string, query: string, req: any): Promise<any[]>;
    getAttachmentAnnotation(workspaceId: string, attachmentId: string, req: any): Promise<any[]>;
    updateAttachmentAnnotation(workspaceId: string, attachmentId: string, annotation: AnnotationAttachmentDTO, req: any): Promise<any[]>;
    private prepareMap;
}
