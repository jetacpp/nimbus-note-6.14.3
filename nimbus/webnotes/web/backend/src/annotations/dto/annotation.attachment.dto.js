"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const swagger_1 = require("@nestjs/swagger");
const annotation_extraction_dto_1 = require("./annotation.extraction.dto");
class AnnotationAttachmentDTO {
}
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", String)
], AnnotationAttachmentDTO.prototype, "attachmentGlobalId", void 0);
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", Number)
], AnnotationAttachmentDTO.prototype, "createdAt", void 0);
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", annotation_extraction_dto_1.AnnotationExtractionDTO)
], AnnotationAttachmentDTO.prototype, "extractions", void 0);
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", String)
], AnnotationAttachmentDTO.prototype, "label", void 0);
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", String)
], AnnotationAttachmentDTO.prototype, "locale", void 0);
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", String)
], AnnotationAttachmentDTO.prototype, "noteGlobalId", void 0);
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", String)
], AnnotationAttachmentDTO.prototype, "text", void 0);
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", Array)
], AnnotationAttachmentDTO.prototype, "textDetailed", void 0);
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", String)
], AnnotationAttachmentDTO.prototype, "type", void 0);
exports.AnnotationAttachmentDTO = AnnotationAttachmentDTO;
//# sourceMappingURL=annotation.attachment.dto.js.map