"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const swagger_1 = require("@nestjs/swagger");
class AnnotationExtractionDTO {
}
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", Array)
], AnnotationExtractionDTO.prototype, "entities", void 0);
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", String)
], AnnotationExtractionDTO.prototype, "type", void 0);
exports.AnnotationExtractionDTO = AnnotationExtractionDTO;
//# sourceMappingURL=annotation.extraction.dto.js.map