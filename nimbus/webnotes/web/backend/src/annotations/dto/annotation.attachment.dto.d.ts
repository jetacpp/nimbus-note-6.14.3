import { AnnotationDetailedDTO } from './annotation.detailed.dto';
import { AnnotationExtractionDTO } from './annotation.extraction.dto';
export declare class AnnotationAttachmentDTO {
    attachmentGlobalId: string;
    createdAt: number;
    extractions: AnnotationExtractionDTO;
    label: string;
    locale: string;
    noteGlobalId: string;
    text: string;
    textDetailed: AnnotationDetailedDTO[];
    type: string;
}
