import { AnnotationDetailedPolyDTO } from './annotation.detailed.poly.dto';
export declare class AnnotationDetailedDTO {
    description: string;
    poly: AnnotationDetailedPolyDTO[];
}
