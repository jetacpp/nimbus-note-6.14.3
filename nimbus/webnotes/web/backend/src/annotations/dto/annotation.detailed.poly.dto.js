"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const swagger_1 = require("@nestjs/swagger");
class AnnotationDetailedPolyDTO {
}
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", Number)
], AnnotationDetailedPolyDTO.prototype, "x", void 0);
tslib_1.__decorate([
    swagger_1.ApiModelProperty(),
    tslib_1.__metadata("design:type", Number)
], AnnotationDetailedPolyDTO.prototype, "y", void 0);
exports.AnnotationDetailedPolyDTO = AnnotationDetailedPolyDTO;
//# sourceMappingURL=annotation.detailed.poly.dto.js.map