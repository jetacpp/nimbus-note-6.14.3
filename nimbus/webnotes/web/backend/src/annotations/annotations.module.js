"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const services_module_1 = require("../services/services.module");
const annotations_controller_1 = require("./annotations.controller");
let AnnotationsModule = class AnnotationsModule {
};
AnnotationsModule = tslib_1.__decorate([
    common_1.Module({
        imports: [services_module_1.ServicesModule],
        controllers: [annotations_controller_1.AnnotationsController],
    })
], AnnotationsModule);
exports.AnnotationsModule = AnnotationsModule;
//# sourceMappingURL=annotations.module.js.map