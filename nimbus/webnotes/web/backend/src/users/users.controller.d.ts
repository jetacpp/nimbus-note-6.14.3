import { ApiServiceService } from '../services/api-service/api-service.service';
export declare class UsersController {
    private apiService;
    constructor(apiService: ApiServiceService);
    getUserProfile(req: any, userId: any): Promise<any>;
    getUserProfileAvatar(req: any, userId: any): Promise<any>;
    setUserVariable(req: any, key: any, value: any): Promise<any>;
    getUserVariable(req: any, key: any): Promise<any>;
    static getUserVariables(userClient: any, userId: any): Promise<any>;
}
