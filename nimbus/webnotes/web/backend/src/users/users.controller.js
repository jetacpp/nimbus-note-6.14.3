"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
const config_constants_1 = require("../../../config.constants");
let UsersController = class UsersController {
    constructor(apiService) {
        this.apiService = apiService;
    }
    getUserProfile(req, userId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId: sessionUserId, sessionId } = req.session;
                const userClient = this.apiService.getUserClient(sessionUserId, sessionId);
                const userProfilePromise = userClient.get([`/users/??`, userId]);
                const userAvatarPromise = userClient.get([`/avatars/??`, userId]).catch((error) => {
                    if (error.name === 'NotFound' && error.httpStatus === 404) {
                        return { storedFileUUID: null };
                    }
                    throw error;
                });
                const [userProfile, userAvatar] = yield Promise.all([userProfilePromise, userAvatarPromise]);
                return Object.assign({}, userProfile, { avatar: userAvatar });
            }
            catch (error) {
                return { status: null, dateEnd: null };
            }
        });
    }
    getUserProfileAvatar(req, userId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId: sessionUserId, sessionId } = req.session;
                const userClient = this.apiService.getUserClient(sessionUserId, sessionId);
                return yield userClient.get([`/avatars/??`, userId]);
            }
            catch (error) {
                return { status: null, dateEnd: null };
            }
        });
    }
    setUserVariable(req, key, value) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            try {
                const userClient = this.apiService.getUserClient(userId, sessionId);
                const { value: newValue } = yield userClient.post([`/users/??/vars/??`, userId, key], { value });
                return newValue;
            }
            catch (error) {
                return { status: null, dateEnd: null };
            }
        });
    }
    getUserVariable(req, key) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            try {
                const userClient = this.apiService.getUserClient(userId, sessionId);
                const { value } = yield userClient.get([`/users/??/vars/??`, userId, key]);
                return value;
            }
            catch (error) {
                return null;
            }
        });
    }
    static getUserVariables(userClient, userId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const currentVariablesList = yield userClient.get(['/users/??/vars', userId]);
            const currentVariables = currentVariablesList.reduce((map, { key, value }) => {
                map[key] = value;
                return map;
            }, {});
            return Object.assign({}, config_constants_1.DEFAULT_USER_VARIABLES, currentVariables);
        });
    }
};
tslib_1.__decorate([
    common_1.Get('/:userId'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('userId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UsersController.prototype, "getUserProfile", null);
tslib_1.__decorate([
    common_1.Get('/:userId/avatar'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('userId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UsersController.prototype, "getUserProfileAvatar", null);
tslib_1.__decorate([
    common_1.Post('/vars/:key'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('key')),
    tslib_1.__param(2, common_1.Body('value')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UsersController.prototype, "setUserVariable", null);
tslib_1.__decorate([
    common_1.Get('/vars/:key'),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('key')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UsersController.prototype, "getUserVariable", null);
UsersController = tslib_1.__decorate([
    common_1.Controller('/users'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], UsersController);
exports.UsersController = UsersController;
//# sourceMappingURL=users.controller.js.map