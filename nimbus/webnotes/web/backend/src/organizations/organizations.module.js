"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const organizations_controller_1 = require("./organizations.controller");
const services_module_1 = require("../services/services.module");
let OrganizationsModule = class OrganizationsModule {
};
OrganizationsModule = tslib_1.__decorate([
    common_1.Module({
        controllers: [organizations_controller_1.OrganizationsController],
        imports: [
            services_module_1.ServicesModule,
        ],
    })
], OrganizationsModule);
exports.OrganizationsModule = OrganizationsModule;
//# sourceMappingURL=organizations.module.js.map