import { ApiServiceService } from '../services/api-service/api-service.service';
export declare class OrganizationsController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    findAll(req: any): Promise<any>;
    getInformation(req: any, filter: string): Promise<any>;
    getOrganizationLimits(req: any, orgId: any): Promise<any>;
    static getOrganizations(orgClient: any, userClient: any): Promise<any>;
    static getSubdomainInfo(organizationClient: any, subdomain: string): Promise<any>;
}
