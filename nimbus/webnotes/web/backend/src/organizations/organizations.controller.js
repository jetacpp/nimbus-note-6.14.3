"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
var OrganizationsController_1;
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
const organizationDTO_1 = require("./dto/organizationDTO");
const swagger_1 = require("@nestjs/swagger");
let OrganizationsController = OrganizationsController_1 = class OrganizationsController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    findAll(req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId, sessionId } = req.session;
                const orgClient = this.apiServiceService.getOrganizationClient(userId, sessionId);
                const userClient = this.apiServiceService.getUserClient(userId, sessionId);
                return OrganizationsController_1.getOrganizations(orgClient, userClient);
            }
            catch (err) {
                console.error(err);
                throw new Error('UNKNOWN_ERROR');
            }
        });
    }
    getInformation(req, filter) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId, sessionId } = req.session;
                const organizationClient = this.apiServiceService.getOrganizationClient(userId, sessionId);
                return yield organizationClient.get([`/orgInfo`], {
                    qs: { filter },
                });
            }
            catch (err) {
                console.error(err);
                throw new Error('UNKNOWN_ERROR');
            }
        });
    }
    getOrganizationLimits(req, orgId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const { userId, sessionId } = req.session;
                const orgClient = this.apiServiceService.getOrganizationClient(userId, sessionId);
                return yield orgClient.get(`/orgs/${orgId}/limits`);
            }
            catch (err) {
                console.error(err);
                throw new Error('UNKNOWN_ERROR');
            }
        });
    }
    static getOrganizations(orgClient, userClient) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const fetchRole = orgId => orgClient.get(`/orgs/${orgId}/userRole`);
            const orgs = yield orgClient.get([`/orgs`]);
            const orgsRoles = yield Promise.all(orgs.map(({ globalId }) => fetchRole(globalId)));
            const ownersIds = [...new Set(orgs.map(({ userId }) => userId))];
            const owners = yield userClient.get([`/users`], { filter: { id: ownersIds } });
            return orgs.map((org, idx) => (Object.assign({}, org, { owner: owners.find(owner => owner.id === org.userId), access: orgsRoles[idx] })));
        });
    }
    static getSubdomainInfo(organizationClient, subdomain) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const subdomainInfo = yield organizationClient.get([`/orgInfo`], {
                    filter: {
                        sub: subdomain,
                    },
                });
                return Object.assign({}, subdomainInfo);
            }
            catch (e) {
                return {};
            }
        });
    }
};
tslib_1.__decorate([
    common_1.Get(),
    swagger_1.ApiResponse({
        status: 200,
        description: 'Organization lists with user member',
        type: organizationDTO_1.OrganizationDTO,
        isArray: true,
    }),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], OrganizationsController.prototype, "findAll", null);
tslib_1.__decorate([
    common_1.Get('/info'),
    swagger_1.ApiResponse({
        status: 200,
        description: 'Organization information for domain',
    }),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Query('filter')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, String]),
    tslib_1.__metadata("design:returntype", Promise)
], OrganizationsController.prototype, "getInformation", null);
tslib_1.__decorate([
    common_1.Get('/:orgId/limits'),
    swagger_1.ApiResponse({
        status: 200,
        description: 'Organization limits',
        isArray: true,
    }),
    tslib_1.__param(0, common_1.Req()),
    tslib_1.__param(1, common_1.Param('orgId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], OrganizationsController.prototype, "getOrganizationLimits", null);
OrganizationsController = OrganizationsController_1 = tslib_1.__decorate([
    swagger_1.ApiUseTags('organizations'),
    common_1.Controller('organizations'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], OrganizationsController);
exports.OrganizationsController = OrganizationsController;
//# sourceMappingURL=organizations.controller.js.map