export declare class UserDTO {
    id: number;
    firstname: string;
    lastname: string;
    username: string;
    email: string;
}
