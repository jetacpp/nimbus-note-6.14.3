import { UserDTO } from './userDTO';
export declare class OrganizationDTO {
    globalId: string;
    userId: number;
    createdAt: number;
    updatedAt: number;
    title: string;
    sub: string;
    suspended: boolean;
    user: UserDTO;
}
