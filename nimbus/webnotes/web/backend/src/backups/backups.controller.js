"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const api_service_service_1 = require("../services/api-service/api-service.service");
let BackupsController = class BackupsController {
    constructor(apiServiceService) {
        this.apiServiceService = apiServiceService;
    }
    getBackups(workspaceId, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const noteClient = this.apiServiceService.getNoteClient(userId, sessionId);
            return yield noteClient.get(['/workspaces/??/backups', workspaceId]);
        });
    }
    deleteBackup(workspaceId, backupId, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const noteClient = this.apiServiceService.getNoteClient(userId, sessionId);
            return yield noteClient.delete(['/workspaces/??/backups/??', workspaceId, backupId]);
        });
    }
    getBackupStructure(workspaceId, backupId, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const noteClient = this.apiServiceService.getNoteClient(userId, sessionId);
            return yield noteClient.get(['/workspaces/??/backups/??/structure', workspaceId, backupId]);
        });
    }
    postBackupRestorer(workspaceId, backupId, overwrite, nodesGlobalIds, req) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { userId, sessionId } = req.session;
            const noteClient = this.apiServiceService.getNoteClient(userId, sessionId);
            return yield noteClient.post(['/workspaces/??/backups/??/restorer', workspaceId, backupId], {
                overwrite,
                noteGlobalId: nodesGlobalIds,
            });
        });
    }
};
tslib_1.__decorate([
    common_1.Get('/backups'),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], BackupsController.prototype, "getBackups", null);
tslib_1.__decorate([
    common_1.Delete('/backups/:backupId'),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Param('backupId')),
    tslib_1.__param(2, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], BackupsController.prototype, "deleteBackup", null);
tslib_1.__decorate([
    common_1.Get('/backups/:backupId/structure'),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Param('backupId')),
    tslib_1.__param(2, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], BackupsController.prototype, "getBackupStructure", null);
tslib_1.__decorate([
    common_1.Post('/backups/:backupId/restorer'),
    tslib_1.__param(0, common_1.Param('workspaceId')),
    tslib_1.__param(1, common_1.Param('backupId')),
    tslib_1.__param(2, common_1.Body('overwrite')),
    tslib_1.__param(3, common_1.Body('noteGlobalId')),
    tslib_1.__param(4, common_1.Req()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], BackupsController.prototype, "postBackupRestorer", null);
BackupsController = tslib_1.__decorate([
    common_1.Controller('/workspaces/:workspaceId'),
    tslib_1.__metadata("design:paramtypes", [api_service_service_1.ApiServiceService])
], BackupsController);
exports.BackupsController = BackupsController;
//# sourceMappingURL=backups.controller.js.map