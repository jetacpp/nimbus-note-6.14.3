import { ApiServiceService } from '../services/api-service/api-service.service';
export declare class BackupsController {
    private apiServiceService;
    constructor(apiServiceService: ApiServiceService);
    getBackups(workspaceId: any, req: any): Promise<any>;
    deleteBackup(workspaceId: any, backupId: any, req: any): Promise<any>;
    getBackupStructure(workspaceId: any, backupId: any, req: any): Promise<any>;
    postBackupRestorer(workspaceId: any, backupId: any, overwrite: any, nodesGlobalIds: any, req: any): Promise<any>;
}
