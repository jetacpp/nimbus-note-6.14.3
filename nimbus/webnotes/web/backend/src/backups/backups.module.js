"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const services_module_1 = require("../services/services.module");
const backups_controller_1 = require("./backups.controller");
let BackupsModule = class BackupsModule {
};
BackupsModule = tslib_1.__decorate([
    common_1.Module({
        imports: [services_module_1.ServicesModule],
        controllers: [backups_controller_1.BackupsController],
    })
], BackupsModule);
exports.BackupsModule = BackupsModule;
//# sourceMappingURL=backups.module.js.map