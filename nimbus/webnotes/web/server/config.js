"use strict";
const fs = require("fs");
let config;
if (fs.existsSync(__dirname + "/config.local.js")) {
    config = require("./config.local.js");
}
else {
    config = require("./config.test.js");
}
module.exports = config;
//# sourceMappingURL=config.js.map