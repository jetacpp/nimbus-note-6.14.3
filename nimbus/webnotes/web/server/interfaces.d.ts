import * as Express from "express";
export interface ISession {
    userId: number;
    sessionId: string;
}
export interface INimbusRequest extends Express.Request {
    session: ISession;
}
