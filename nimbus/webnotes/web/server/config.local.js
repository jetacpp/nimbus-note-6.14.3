module.exports = {
    port: 3011,
    services: {
        user: {
            url: 'http://api.develop.nimbustest.com/user-api-service',
            basePath: '/v1'
        },
        note: {
            url: 'http://api.develop.nimbustest.com/note-api-service',
            basePath: '/v1'
        },
        file: {
            url: 'http://api.develop.nimbustest.com/file-api-service',
            basePath: '/v1'
        }
    },
    redis: {
        host: "develop.nimbustest.com",
        port: 6379
    },
    amqp: "amqp://amqp.nimbus-internal:5672",
    authLogin: '/auth/?f=login',
    uploadDir: "uploads/",
    domain: "everhelper.me"
};