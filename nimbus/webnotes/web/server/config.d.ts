interface IProjectConfig {
    port: number;
    services: {
        auth: string;
        user: string;
        note: string;
        file: string;
        workspace: string;
        annotation: string;
        notification: string;
        org: string;
        trial: string;
        survey: string;
        mail: string;
        otp: string;
    };
    mongo: any;
    amqp: string;
    authLogin: string;
    uploadDir: string;
    domain: string;
    debug: boolean;
    useLocalhostProxy: boolean;
    isDebug: boolean;
    statsd: any;
    publicPath: string;
    boxUrl: string;
}
declare let config: IProjectConfig;
export = config;
