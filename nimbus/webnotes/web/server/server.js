"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const config = require("./config");
const metrics_1 = require("./common/metrics");
metrics_1.initMetrics({
    config: config.statsd,
});
const packageJson = require('./package.json');
const APIFactory = require('eh-api-client');
const browserslist_useragent_1 = require("browserslist-useragent");
const path = require("path");
const express = require("express");
const engines = require("consolidate");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const multer = require("multer");
const proxy = require("http-proxy-middleware");
const core_1 = require("@nestjs/core");
const swagger_1 = require("@nestjs/swagger");
const app_module_1 = require("../backend/src/app.module");
const errors_interceptor_1 = require("../backend/src/errors.interceptor");
const me_1 = require("./routes/me");
const noteTags_1 = require("./routes/noteTags");
const attachments_1 = require("./routes/attachments");
const notesApi_1 = require("./routes/notesApi");
const orgs_1 = require("./routes/orgs");
const userSettings_1 = require("./routes/userSettings");
const middlewares_1 = require("./common/middlewares");
const workspacesApi_1 = require("./routes/workspacesApi");
const workspaces_controller_1 = require("../backend/src/workspaces/workspaces/workspaces.controller");
const organizations_controller_1 = require("../backend/src/organizations/organizations.controller");
const users_controller_1 = require("../backend/src/users/users.controller");
const trial_controller_1 = require("../backend/src/trial/trial.controller");
const notes_controller_1 = require("../backend/src/notes/notes.controller");
const routeOptions = {
    noteService: new APIFactory(config.services.note),
    userService: new APIFactory(config.services.user),
    fileService: new APIFactory(config.services.file),
    orgService: new APIFactory(config.services.org),
    trialService: new APIFactory(config.services.trial),
    workspaceService: new APIFactory(config.services.workspace),
    isDebug: config.debug,
};
exports.app = express();
metrics_1.initAppMetrics({
    config: config.statsd,
    app: exports.app,
});
const server = require('http').Server(exports.app);
if (process.env.AUTO_STOP_CONTAINER) {
    let timeout;
    const autoStopContainerTimeout = parseInt(process.env.AUTO_STOP_CONTAINER, 10);
    const setAutoStopTimeout = () => {
        if (timeout) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(() => {
            server.close(() => {
                process.exit(0);
                console.log('Server will be stopped after timeout');
            });
        }, autoStopContainerTimeout);
    };
    exports.app.use((req, res, next) => {
        setAutoStopTimeout();
        next();
    });
    setAutoStopTimeout();
    console.log('Server will be stopped after last request in ' + autoStopContainerTimeout / 1000 + ' seconds');
}
exports.app.use(bodyParser.json({ limit: '8mb' }));
exports.app.set('views', path.join(__dirname, '..', 'templates'));
exports.app.engine('html', engines.mustache);
exports.app.set('view engine', 'mustache');
exports.app.use(cookieParser());
const upload = multer({ dest: 'uploads/' });
const storage = multer.memoryStorage();
const uploadEvernote = multer({ storage });
exports.app.use('/static', middlewares_1.allowCrossDomainMiddleware);
exports.app.use('/static', express.static(path.join(__dirname, '..', 'static')));
exports.app.post('/api/me/changePassword', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, me_1.postPasswordMiddleware(routeOptions));
exports.app.post('/api/me/profile', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, me_1.postProfileName(routeOptions));
exports.app.post('/api/me/avatar', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, upload.fields([{ name: 'avatar' }]), me_1.postProfileAvatar(routeOptions));
exports.app.delete('/api/me/avatar', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, upload.fields([{ name: 'avatar' }]), me_1.deleteProfileAvatar(routeOptions));
exports.app.post('/api/workspaces/:workspaceId/notesParentId', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, notesApi_1.postNotesParentIdMiddleware(routeOptions));
exports.app.post('/api/workspaces/:workspaceId/notesTags', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, noteTags_1.postNotesTagsMiddleware(routeOptions));
exports.app.post('/api/workspaces/:workspaceId/tags', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, noteTags_1.postTagMiddleware(routeOptions));
exports.app.delete('/api/workspaces/:workspaceId/tags', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, noteTags_1.deleteTagMiddleware(routeOptions));
exports.app.get('/api/workspaces/:workspaceId/usage', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, orgs_1.getUsageMiddleware(routeOptions));
exports.app.get('/api/workspaces/:workspaceId/info', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, orgs_1.getInfoMiddleware(routeOptions));
exports.app.get('/api/user-settings', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, userSettings_1.getUserSettingsMiddleware());
exports.app.post('/api/user-settings', middlewares_1.nimbusSessionMiddleware, middlewares_1.authOnly, userSettings_1.postUserSettingsMiddleware());
exports.app.put('/api/workspaces/:workspaceId/attachments/:attachmentId', middlewares_1.nimbusSessionMiddleware, upload.fields([{ name: 'attachment' }]), attachments_1.postAttachmentsMiddleware(routeOptions));
exports.app.post('/api/workspaces/:workspaceId/evernoteAttachments/:attachmentId', middlewares_1.nimbusSessionMiddleware, uploadEvernote.single('base64Text'), attachments_1.postEvernoteAttachmentsMiddleware(routeOptions));
notesApi_1.setNotesApiRoutes(exports.app, middlewares_1.nimbusSessionMiddleware, routeOptions);
workspacesApi_1.setWorkspacesApiRoutes(exports.app, middlewares_1.nimbusSessionMiddleware, routeOptions);
function getConfigUrl({ version, publicPath = '/' }) {
    return `${publicPath}static/js/config.service.js?version=${version}`;
}
function isMobileBrowser(userAgent) {
    const mobileRegExp = new RegExp('Mobile');
    return mobileRegExp.test(userAgent);
}
function getStaticFileUrl(UUID) {
    const { boxUrl } = config;
    return UUID ? `${boxUrl}/file/${UUID}` : undefined;
}
var mobilePlatforms;
(function (mobilePlatforms) {
    mobilePlatforms["WINDOWS_PHONE"] = "Windows Phone";
    mobilePlatforms["ANDROID"] = "Android";
    mobilePlatforms["IOS"] = "iOS";
})(mobilePlatforms || (mobilePlatforms = {}));
function getMobileOS(userAgent) {
    console.log(userAgent);
    if (/windows phone/i.test(userAgent)) {
        return mobilePlatforms.WINDOWS_PHONE;
    }
    if (/android/i.test(userAgent)) {
        return mobilePlatforms.ANDROID;
    }
    if (/iPad|iPhone|iPod/.test(userAgent)) {
        return mobilePlatforms.IOS;
    }
    return null;
}
function getApplication(req, res) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session, headers, cookies, startTime } = req;
        const { sessionId, userId } = session;
        const { publicPath, authLogin } = config;
        const { version } = packageJson;
        const userAgent = headers['user-agent'];
        const isMobileUserAgent = isMobileBrowser(userAgent);
        const isUseAppAnyway = cookies.useAppAnyway == 'yes';
        if (!isMobileUserAgent &&
            (userAgent && browserslist_useragent_1.matchesUA(userAgent, {
                allowHigherVersions: true,
                ignoreMinor: true,
                ignorePatch: true,
            }) || isUseAppAnyway)) {
            if (session.userId) {
                const { headers } = req;
                const hydratedState = yield getHydratedState({ userId, sessionId, routeOptions, headers });
                const { logo, defaultLogo } = hydratedState.subdomain;
                res.render('index.html', {
                    version,
                    loadTime: Date.now() - startTime,
                    configUrl: getConfigUrl({ version, publicPath }),
                    hydratedState: JSON.stringify(hydratedState),
                    favicon: hydratedState.subdomain.favicon,
                    logo: (logo) ? logo : defaultLogo,
                });
            }
            else {
                res.status(401).render('redirect.html', { authUrl: authLogin });
            }
        }
        else if (isMobileUserAgent) {
            const mobileOS = getMobileOS(userAgent);
            res.render('mobile.html', { mobileOS });
        }
        else {
            res.render('bad-browser.html');
        }
    });
}
const getHydratedState = ({ userId, sessionId, routeOptions, headers }) => tslib_1.__awaiter(this, void 0, void 0, function* () {
    if (!userId || !sessionId) {
        return {};
    }
    const workspaceClient = routeOptions.workspaceService.getClient(userId, 'web');
    workspaceClient.setSessionId(sessionId);
    const userClient = routeOptions.userService.getClient(userId, 'web');
    userClient.setSessionId(sessionId);
    const noteClient = routeOptions.noteService.getClient(userId, 'web');
    noteClient.setSessionId(sessionId);
    const orgClient = routeOptions.orgService.getClient(userId, 'web');
    orgClient.setSessionId(sessionId);
    const trialClient = routeOptions.trialService.getClient(userId, 'web');
    trialClient.setSessionId(sessionId);
    const { host } = headers;
    const subdomain = host.split('.')[0];
    const [workspaces, workspacesAccess, organizations, userVariables, trial, me, myInfo, subdomainInfo,] = yield Promise.all([
        workspaces_controller_1.WorkspacesController.getWorkspaces(workspaceClient, userClient, userId).catch(() => []),
        workspaces_controller_1.WorkspacesController.getWorkspacesAccess(workspaceClient).catch(() => []),
        organizations_controller_1.OrganizationsController.getOrganizations(orgClient, userClient).catch(() => []),
        users_controller_1.UsersController.getUserVariables(userClient, userId).catch(() => []),
        trial_controller_1.TrialController.getTrial(trialClient),
        noteClient.get(['/me']),
        notesApi_1.getUserInfo({ userId }, { userClient }),
        organizations_controller_1.OrganizationsController.getSubdomainInfo(orgClient, subdomain).catch(() => []),
    ]);
    const personalWorkspaces = workspaces.filter((ws) => ws.orgId.startsWith('u'));
    const personalNotesCounts = yield Promise.all(personalWorkspaces.map((ws) => {
        return notes_controller_1.NotesController.getNotesCountForWs(noteClient, ws.globalId);
    }));
    const personalNotesTotal = personalNotesCounts.reduce((mem, sum) => mem + sum, 0);
    const workspacesAccessMap = workspacesAccess.reduce((map, access) => {
        if (!access.workspaceId || !access.access) {
            return map;
        }
        map[access.workspaceId] = access.access;
        return map;
    }, {});
    let favicon = '/static/assets/images/everhelper.ico';
    if (subdomainInfo.smallLogoStoredFileUUID) {
        favicon = getStaticFileUrl(subdomainInfo.smallLogoStoredFileUUID + '?height=16');
    }
    const defaultLogo = '/static/assets/images/logo.svg';
    let logo = null;
    if (subdomainInfo.bigLogoStoredFileUUID) {
        logo = getStaticFileUrl(subdomainInfo.bigLogoStoredFileUUID);
    }
    return {
        organizations,
        userVariables,
        workspaces,
        workspacesAccessMap,
        trial,
        subdomain: { favicon, logo, defaultLogo },
        profile: Object.assign({}, me, myInfo, { hasPersonalNotes: !!personalNotesTotal }),
    };
});
exports.app.get('/ws*', middlewares_1.metricsInitMiddleware('startTime'), middlewares_1.nimbusSessionMiddleware, getApplication);
exports.app.get('/client*', middlewares_1.metricsInitMiddleware('startTime'), middlewares_1.nimbusSessionMiddleware, getApplication);
function bootstrap() {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        try {
            const nestApp = yield core_1.NestFactory.create(app_module_1.AppModule, server);
            const options = new swagger_1.DocumentBuilder()
                .setTitle('Webnotes API')
                .setVersion('1.0')
                .setBasePath('/v1')
                .build();
            const document = swagger_1.SwaggerModule.createDocument(nestApp, options);
            swagger_1.SwaggerModule.setup('/api', nestApp, document);
            nestApp.use(middlewares_1.nimbusSessionMiddleware);
            nestApp.useGlobalInterceptors(new errors_interceptor_1.ErrorsInterceptor());
            yield nestApp.init();
            exports.app.use('/v1', nestApp.getHttpAdapter().getInstance());
        }
        catch (error) {
            console.error(error);
        }
        if (config.useLocalhostProxy) {
            exports.app.use('/', proxy({
                target: 'http://auth-form-localhost.develop.nimbustest.com',
                autoRewrite: true,
                changeOrigin: true,
                cookieDomainRewrite: '.localhost.ru',
            }));
        }
        else {
            exports.app.get('/', middlewares_1.nimbusSessionMiddleware, getApplication);
        }
        server.listen(config.port, () => {
            console.log('Listening on port ' + config.port + '...');
            if (config.useLocalhostProxy) {
                const wsProxy = proxy({
                    target: 'http://develop.nimbustest.com',
                    autoRewrite: true,
                    changeOrigin: true,
                    cookieDomainRewrite: '.localhost.ru',
                    ws: true,
                });
                server.on('upgrade', wsProxy.upgrade);
            }
        });
        server.keepAliveTimeout = 301 * 1000;
        server.headersTimeout = 305 * 1000;
    });
}
bootstrap();
//# sourceMappingURL=server.js.map