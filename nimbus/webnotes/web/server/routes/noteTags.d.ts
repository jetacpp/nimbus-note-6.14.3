import * as Express from 'express';
import { INimbusRequest } from '../interfaces';
export declare function deleteTagMiddleware({ noteService, isDebug, }: {
    noteService: any;
    isDebug: any;
}): (req: INimbusRequest, res: Express.Response) => Promise<void | Express.Response>;
export declare function postTagMiddleware({ noteService, isDebug, }: {
    noteService: any;
    isDebug: any;
}): (req: INimbusRequest, res: Express.Response) => Promise<void | Express.Response>;
export declare function postNotesTagsMiddleware({ noteService, isDebug, }: {
    noteService: any;
    isDebug: any;
}): (req: INimbusRequest, res: Express.Response) => Promise<Express.Response>;
