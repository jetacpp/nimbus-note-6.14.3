export declare function postAttachmentsMiddleware({ fileService, noteService, isDebug, }: {
    fileService: any;
    noteService: any;
    isDebug: any;
}): (req: any, res: any) => Promise<void>;
export declare function postEvernoteAttachmentsMiddleware({ fileService, noteService, isDebug, }: {
    fileService: any;
    noteService: any;
    isDebug: any;
}): (req: any, res: any) => void;
