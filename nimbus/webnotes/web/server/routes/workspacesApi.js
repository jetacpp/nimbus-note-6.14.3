"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const middlewares_1 = require("../common/middlewares");
const METHODS = {
    'GET': 'get',
    'POST': 'post',
    'DELETE': 'delete'
};
const REQUESTS = [
    { httpMethod: 'GET', url: '/api/workspaces/[0-9a-z]{16}' },
    { httpMethod: 'POST', url: '/api/workspaces/[0-9a-z]{16}/members' },
    { httpMethod: 'POST', url: '/api/members/:memberId', postReceive: postMemberPostReceive },
    { httpMethod: 'DELETE', url: '/api/members/:memberId' },
    { httpMethod: 'GET', url: '/api/invites' },
    { httpMethod: 'POST', url: '/api/invites' },
    { httpMethod: 'POST', url: '/api/invitesById/:inviteId' },
    { httpMethod: 'POST', url: '/api/invitesById/:inviteId/send' },
    { httpMethod: 'DELETE', url: '/api/invitesById/:inviteId' },
];
function setWorkspacesApiRoutes(app, nimbusSessionMiddleware, routeOptions) {
    REQUESTS.forEach((request) => {
        const requestMethod = METHODS[request.httpMethod];
        if (!requestMethod) {
            console.error('Wrong method', request.httpMethod);
            return;
        }
        app[requestMethod](request.url, nimbusSessionMiddleware, middlewares_1.authOnly, requestToInternalService(request, routeOptions));
    });
    app.post('/api/workspaces/:workspaceId/transferToOwner/:ownerId', nimbusSessionMiddleware, middlewares_1.authOnly, postWorkspaceTransferOwner(routeOptions));
}
exports.setWorkspacesApiRoutes = setWorkspacesApiRoutes;
function requestToInternalService(request, { workspaceService, userService, isDebug, }) {
    return (req, res) => {
        const { userId, sessionId } = req.session;
        const client = workspaceService.getClient(userId, 'web');
        const targetUrl = req.path.replace('/api/', '/');
        client.setSessionId(sessionId);
        client.request(request.httpMethod, {
            url: targetUrl,
            qs: req.query,
        }, req.body, (err, result) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (err) {
                if (!isDebug) {
                    delete err.message;
                }
                return res.status(err.httpStatus ? err.httpStatus : 500).json(err);
            }
            if (request.postReceive) {
                result = yield request.postReceive(result, {
                    workspaceClient: client,
                    userClient: userService.getClient(userId, 'web'),
                });
            }
            result = result ? result : {};
            res.status(200).json(result);
        }));
    };
}
function getMembersPostReceive(result, { userClient, }) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        if (!result || !result.length) {
            return [];
        }
        const usersIds = result.map((member) => member.userId);
        const users = yield userClient.get({
            url: `/users/`,
            filter: {
                id: usersIds
            },
        });
        return result.map((member) => {
            const user = users.find((user) => user.id == member.userId);
            if (!user) {
                return member;
            }
            return Object.assign({}, member, {
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email,
                username: user.username,
            });
        });
    });
}
function postMemberPostReceive(result, { userClient, }) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const [newResult] = yield getMembersPostReceive([result], {
            userClient,
        });
        return newResult;
    });
}
function postWorkspaceTransferOwner({ workspaceService, orgService, isDebug, }) {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session, params } = req;
        const { ownerId, workspaceId } = params;
        if (!session.userId || !ownerId) {
            return res.status(404).json({});
        }
        try {
            const orgClient = orgService.getClient(ownerId, 'web');
            const orgInfo = yield orgClient.get(['/orgs/default/info']);
            const { orgId } = orgInfo;
            const workspaceClient = workspaceService.getClient(req.session.userId, 'web');
            yield workspaceClient.post(['/workspaces/??/org', workspaceId], {
                orgId,
            });
            return res.status(200).json({});
        }
        catch (err) {
            if (err.httpStatus) {
                res.status(err.httpStatus).json({ message: err.message });
            }
            else if (err.response) {
                if (!isDebug) {
                    delete err.body.message;
                }
                res.status(err.response.statusCode).json(err.body);
            }
            else {
                throw err;
            }
        }
    });
}
exports.postWorkspaceTransferOwner = postWorkspaceTransferOwner;
//# sourceMappingURL=workspacesApi.js.map