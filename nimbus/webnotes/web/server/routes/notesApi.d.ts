import * as Express from 'express';
import { INimbusRequest } from '../interfaces';
export declare function setNotesApiRoutes(app: any, nimbusSessionMiddleware: any, requestOptions: any): void;
export declare function getUserInfo(result: any, clients: any): Promise<any>;
export declare function postNotesParentIdMiddleware({ noteService, isDebug, }: {
    noteService: any;
    isDebug: any;
}): (req: INimbusRequest, res: Express.Response) => Promise<Express.Response>;
