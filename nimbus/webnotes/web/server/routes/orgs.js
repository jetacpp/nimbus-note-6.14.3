"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
function getUsageMiddleware({ workspaceService, noteService, isDebug, }) {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session } = req;
        if (!session.userId) {
            return res.status(404).json({});
        }
        try {
            const { workspaceId } = req.params;
            const workspaceClient = workspaceService.getClient(req.session.userId, 'web');
            const noteClient = noteService.getClient(req.session.userId, 'web');
            const workspace = yield workspaceClient.get(['/workspaces/??', workspaceId]);
            const result = yield noteClient.get(['/orgs/??/usage', workspace.orgId]);
            return res.status(200).json(result);
        }
        catch (err) {
            if (err.httpStatus) {
                res.status(err.httpStatus).json({ message: err.message });
            }
            else if (err.response) {
                if (!isDebug) {
                    delete err.body.message;
                }
                res.status(err.response.statusCode).json(err.body);
            }
            else {
                throw err;
            }
        }
    });
}
exports.getUsageMiddleware = getUsageMiddleware;
function getInfoMiddleware({ workspaceService, noteService, isDebug, }) {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session } = req;
        if (!session.userId) {
            return res.status(404).json({});
        }
        try {
            const { workspaceId } = req.params;
            const workspaceClient = workspaceService.getClient(req.session.userId, 'web');
            const noteClient = noteService.getClient(req.session.userId, 'web');
            const workspace = yield workspaceClient.get(['/workspaces/??', workspaceId]);
            const result = yield noteClient.get(['/orgs/??/info', workspace.orgId]);
            return res.status(200).json(result);
        }
        catch (err) {
            if (err.httpStatus) {
                res.status(err.httpStatus).json({ message: err.message });
            }
            else if (err.response) {
                if (!isDebug) {
                    delete err.body.message;
                }
                res.status(err.response.statusCode).json(err.body);
            }
            else {
                throw err;
            }
        }
    });
}
exports.getInfoMiddleware = getInfoMiddleware;
//# sourceMappingURL=orgs.js.map