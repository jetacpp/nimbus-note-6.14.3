"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const METHODS = {
    'GET': 'get',
    'POST': 'post',
    'DELETE': 'delete',
};
const REQUESTS = [
    { httpMethod: 'GET', url: '/api/me', postReceive: getUserInfo },
    { httpMethod: 'GET', url: '/api/workspaces/:workspaceId/search/notes*' },
    { httpMethod: 'GET', url: '/api/workspaces/:workspaceId/notes*' },
    { httpMethod: 'GET', url: '/api/workspaces/:workspaceId/attachments*' },
    { httpMethod: 'GET', url: '/api/workspaces/:workspaceId/todos*' },
    { httpMethod: 'GET', url: '/api/workspaces/:workspaceId/tags' },
    { httpMethod: 'GET', url: '/api/workspaces/:workspaceId/tags*' },
    { httpMethod: 'GET', url: '/api/workspaces/:workspaceId/texts*' },
    { httpMethod: 'GET', url: '/api/workspaces/:workspaceId/reminders*' },
    { httpMethod: 'GET', url: '/api/workspaces/:workspaceId/reminder/:noteId*' },
    { httpMethod: 'GET', url: '/api/workspaces/:workspaceId/backups' },
    { httpMethod: 'GET', url: '/api/workspaceEmails*' },
    { httpMethod: 'GET', url: '/api/workspaceInfos*' },
    { httpMethod: 'POST', url: '/api/me' },
    { httpMethod: 'POST', url: '/api/workspaces/:workspaceId/notes*' },
    { httpMethod: 'POST', url: '/api/workspaces/:workspaceId/attachments*' },
    { httpMethod: 'POST', url: '/api/workspaces/:workspaceId/todos*' },
    { httpMethod: 'POST', url: '/api/workspaces/:workspaceId/limit-checker*' },
    { httpMethod: 'POST', url: '/api/workspaces/:workspaceId/shares*' },
    { httpMethod: 'POST', url: '/api/workspaces/:workspaceId/reminder/:noteId*' },
    { httpMethod: 'POST', url: '/api/workspaces/:workspaceId/texts/:textId/tokens' },
    { httpMethod: 'POST', url: '/api/workspaces/:workspaceId/texts/:notesId' },
    { httpMethod: 'DELETE', url: '/api/workspaces/:workspaceId/notes*' },
    { httpMethod: 'DELETE', url: '/api/workspaces/:workspaceId/todos*' },
    { httpMethod: 'DELETE', url: '/api/workspaces/:workspaceId/attachments*' },
    { httpMethod: 'DELETE', url: '/api/workspaces/:workspaceId/reminder/:noteId*' },
];
function setNotesApiRoutes(app, nimbusSessionMiddleware, requestOptions) {
    REQUESTS.forEach((request) => {
        const { noteService, userService, isDebug, } = requestOptions;
        const requestMethod = METHODS[request.httpMethod];
        if (!requestMethod) {
            console.error('Wrong method', request.httpMethod);
            return;
        }
        app[requestMethod](request.url, nimbusSessionMiddleware, (req, res) => {
            const { session } = req;
            if (!session.userId) {
                return res.status(404).json({});
            }
            const noteClient = noteService.getClient(session.userId, 'web');
            const targetUrl = req.path.replace('/api/', '/');
            noteClient.setSessionId(session.sessionId);
            noteClient.request(request.httpMethod, { url: targetUrl, qs: req.query }, req.body, (err, result, response) => tslib_1.__awaiter(this, void 0, void 0, function* () {
                if (err) {
                    if (!isDebug) {
                        delete err.message;
                    }
                    return res.status(err.httpStatus ? err.httpStatus : 500).json(err);
                }
                const totalCount = response.headers['x-total-count'];
                if (totalCount) {
                    res.set({ "x-total-count": totalCount });
                }
                if ((/workspaces\/[a-zA-z0-9]{16}\/notes/.test(targetUrl)) && req.query && req.query.onlyId && result && result.length) {
                    result = result.map((x) => x.globalId);
                }
                if ((/workspaces\/default\/notes/.test(targetUrl)) && req.query && req.query.onlyId && result && result.length) {
                    result = result.map((x) => x.globalId);
                }
                if (request.postReceive) {
                    result = yield request.postReceive(result, {
                        noteClient,
                        userClient: userService.getClient(session.userId, 'web')
                    });
                }
                result = result ? result : {};
                res.status(200).json(result);
            }));
        });
    });
}
exports.setNotesApiRoutes = setNotesApiRoutes;
function getUserInfo(result, clients) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        if (!result || !result.userId) {
            return result;
        }
        try {
            const { userClient } = clients;
            const { userId } = result;
            const avatarPromise = userClient.get({
                url: `/avatars/${userId}`,
            }).catch((error) => {
                if (error.name === 'NotFound' && error.httpStatus === 404) {
                    return { storedFileUUID: null };
                }
                throw error;
            });
            const userPromise = userClient.get({
                url: `/users/${userId}`,
            });
            const [avatar, user] = yield Promise.all([avatarPromise, userPromise]);
            if (!avatar || !user) {
                return result;
            }
            const { storedFileUUID } = avatar;
            const { firstname, lastname } = user;
            return Object.assign({}, result, {
                firstname,
                lastname,
                avatarFileUUID: storedFileUUID
            });
        }
        catch (error) {
            console.error(error);
            return result;
        }
    });
}
exports.getUserInfo = getUserInfo;
function postNotesParentIdMiddleware({ noteService, isDebug, }) {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session, params, body } = req;
        const { parentId } = body;
        if (!session.userId || !parentId) {
            return res.status(404).json({});
        }
        const client = noteService.getClient(session.userId, 'web');
        client.setSessionId(session.sessionId);
        const workspaceId = params.workspaceId || 'default';
        for (const noteId of body.notesIds) {
            const targetUrl = `/workspaces/${workspaceId}/notes/${noteId}`;
            try {
                yield client.request('post', { url: [targetUrl], qs: req.query }, {
                    globalId: noteId,
                    parentId,
                });
            }
            catch (error) {
                if (error) {
                    if (!isDebug) {
                        delete error.message;
                    }
                    return res.status(error.httpStatus ? error.httpStatus : 500).json(error);
                }
            }
        }
        res.status(200).json({});
    });
}
exports.postNotesParentIdMiddleware = postNotesParentIdMiddleware;
//# sourceMappingURL=notesApi.js.map