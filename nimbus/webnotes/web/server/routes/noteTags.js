"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
function handleError(err, res, isDebug = false) {
    if (err.httpStatus) {
        res.status(err.httpStatus).json({ message: err.message });
    }
    else if (err.response) {
        if (!isDebug) {
            delete err.body.message;
        }
        res.status(err.response.statusCode).json(err.body);
    }
    else {
        throw err;
    }
}
function deleteTagMiddleware({ noteService, isDebug, }) {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session, query, params } = req;
        if (!query || !query.tag) {
            return res.status(404).json({});
        }
        const workspaceId = params.workspaceId || 'default';
        const tag = query.tag;
        const noteClient = noteService.getClient(session.userId, 'web');
        try {
            const result = yield noteClient.delete([`/workspaces/${workspaceId}/tags/??`, tag]);
            return res.status(200).json(result);
        }
        catch (err) {
            return handleError(err, res, isDebug);
        }
    });
}
exports.deleteTagMiddleware = deleteTagMiddleware;
function postTagMiddleware({ noteService, isDebug, }) {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session, query, body, params } = req;
        if (!query || !query.tag) {
            return res.status(404).json({});
        }
        const workspaceId = params.workspaceId || 'default';
        const tag = query.tag;
        const noteClient = noteService.getClient(session.userId, 'web');
        try {
            const result = yield noteClient.post([`/workspaces/${workspaceId}/tags/??`, tag], body);
            return res.status(200).json(result);
        }
        catch (err) {
            return handleError(err, res, isDebug);
        }
    });
}
exports.postTagMiddleware = postTagMiddleware;
function postNotesTagsMiddleware({ noteService, isDebug, }) {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session, params, body } = req;
        if (!session.userId) {
            return res.status(404).json({});
        }
        const client = noteService.getClient(session.userId, 'web');
        client.setSessionId(session.sessionId);
        const workspaceId = params.workspaceId || 'default';
        for (const noteId of body.notesIds) {
            const targetUrl = `/workspaces/${workspaceId}/notes/${noteId}/tags/??`;
            const tagsToAdd = body.tagsToAdd || [];
            const tagsToRemove = body.tagsToRemove || [];
            const addPromises = tagsToAdd
                .map((tag) => client.request('post', { url: [targetUrl, tag], qs: req.query }, {}).catch((error) => {
            }));
            const removePromises = tagsToRemove
                .map((tag) => client.request('delete', { url: [targetUrl, tag], qs: req.query }, {}).catch((error) => {
            }));
            try {
                yield Promise.all([...addPromises, ...removePromises]);
            }
            catch (error) {
                if (error) {
                    if (!isDebug) {
                        delete error.message;
                    }
                    return res.status(error.httpStatus ? error.httpStatus : 500).json(error);
                }
            }
        }
        res.status(200).json({});
    });
}
exports.postNotesTagsMiddleware = postNotesTagsMiddleware;
//# sourceMappingURL=noteTags.js.map