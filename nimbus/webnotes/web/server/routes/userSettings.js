"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongodb_1 = require("mongodb");
const config = require("../config");
let mongoDb;
mongodb_1.MongoClient.connect(config.mongo.url, {
    auth: config.mongo.auth,
}).then((client) => {
    mongoDb = client.db(config.mongo.database);
    mongoDb.createCollection("user-settings", { validator: { $or: [
                { dateTimeLocale: { $type: "string" } },
                { userId: { $type: "string" } }
            ]
        } });
}).catch((err) => {
    console.error(err);
});
function getUserSettingsMiddleware() {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        if (!mongoDb) {
            return res.status(404).json({});
        }
        const userSettingsCollection = mongoDb.collection('user-settings');
        const settings = yield userSettingsCollection.findOne({ userId: req.session.userId });
        if (settings) {
            delete settings._id;
            delete settings.userId;
            return res.status(200).json(settings);
        }
        else {
            return res.status(200).json({});
        }
    });
}
exports.getUserSettingsMiddleware = getUserSettingsMiddleware;
function postUserSettingsMiddleware() {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        if (!mongoDb) {
            return res.status(404).json({});
        }
        const userSettingsCollection = mongoDb.collection('user-settings');
        const updateResult = yield userSettingsCollection.updateOne({ userId: req.session.userId }, {
            $set: Object.assign({}, req.body, { userId: req.session.userId })
        }, {
            upsert: true
        });
        if (updateResult.result.ok) {
            if (updateResult.upsertedId) {
                return res.status(201).json({});
            }
            else {
                return res.status(200).json({});
            }
        }
        else {
            return res.status(404).json({});
        }
    });
}
exports.postUserSettingsMiddleware = postUserSettingsMiddleware;
//# sourceMappingURL=userSettings.js.map