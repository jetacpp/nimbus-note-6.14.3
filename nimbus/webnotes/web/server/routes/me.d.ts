export declare function postProfileName({ userService, isDebug, }: {
    userService: any;
    isDebug: any;
}): (req: any, res: any) => Promise<any>;
export declare function postProfileAvatar({ fileService, userService, isDebug, }: {
    fileService: any;
    userService: any;
    isDebug: any;
}): (req: any, res: any) => Promise<any>;
export declare function deleteProfileAvatar({ userService, isDebug, }: {
    userService: any;
    isDebug: any;
}): (req: any, res: any) => Promise<any>;
export declare function postPasswordMiddleware({ userService, isDebug, }: {
    userService: any;
    isDebug: any;
}): (req: any, res: any) => any;
