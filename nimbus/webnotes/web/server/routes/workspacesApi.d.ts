export declare function setWorkspacesApiRoutes(app: any, nimbusSessionMiddleware: any, routeOptions: any): void;
export declare function postWorkspaceTransferOwner({ workspaceService, orgService, isDebug, }: {
    workspaceService: any;
    orgService: any;
    isDebug: any;
}): (req: any, res: any) => Promise<any>;
