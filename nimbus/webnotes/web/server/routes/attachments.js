"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const fs = require("fs");
const md5 = require("md5");
const nodeDns = require("dns");
const nodeUrl = require("url");
const nodeHttp = require("http");
const nodeHttps = require("https");
const nodePath = require("path");
const request = require("request");
const imageType = require("image-type");
const checkIp = require("check-ip");
const randomString = (length, chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') => {
    let result = '';
    while (result.length < length) {
        result += chars[Math.round(Math.random() * (chars.length - 1))];
    }
    return result;
};
function uploadFileName(tempFilename, res, attachmentOptions, apiData, stream) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { fileService, noteService, isDebug, userId, sessionId, } = apiData;
        const { workspaceId, noteId, attachmentId, inList, displayName, type, mime, hash, isImport, size, } = attachmentOptions;
        const fileClient = fileService.getClient(userId, 'web');
        fileClient.setSessionId(sessionId);
        let fileSize;
        let fileStream;
        if (!stream) {
            fileSize = fs.statSync(tempFilename).size;
            fileStream = fs.createReadStream(tempFilename);
        }
        else {
            fileSize = size;
            fileStream = stream;
        }
        const cleanup = () => fs.unlink(tempFilename, (err) => {
            if (err) {
                console.log('Unlink file failed:', tempFilename);
            }
        });
        try {
            const { name: tempFileName, type: resultType } = yield fileClient.request('POST', {
                url: '/tempfiles/stream',
                headers: {
                    'Content-Type': mime,
                    'Content-Length': fileSize,
                },
            }, fileStream);
            const client = noteService.getClient(userId, 'web');
            client.setSessionId(sessionId);
            const createAttachmentObject = {
                noteGlobalId: noteId,
                source: {
                    tempFileName,
                },
                displayName,
                mime: resultType,
                type,
                inList,
            };
            const url = `/workspaces/${workspaceId}/attachments/${attachmentId}?isImport=${!!isImport}`;
            return client.put({ url }, createAttachmentObject).then((result) => {
                result = result ? result : {};
                result = Object.assign({}, result, { hash });
                cleanup();
                res.status(200).json(result);
            });
        }
        catch (error) {
            if (!isDebug) {
                delete error.message;
            }
            cleanup();
            res.status(error.httpStatus ? error.httpStatus : 500).json(error);
        }
    });
}
function downloadExternalFile(url, res, attachmentOptions, apiData) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        try {
            const stats = yield getUrlStats(url);
            console.log('Download file:', url);
            console.log('Stats:', stats.statusCode, stats.type, stats.length);
            if (stats.statusCode !== 200) {
                throw new Error('ERR_URL_NOT_AVAILABLE ' + stats.statusCode);
            }
            if (!stats.type.startsWith('image/')) {
                throw new Error('ERR_BAD_IMAGE_TYPE');
            }
            const displayName = nodePath.basename(stats.pathname);
            const newPath = 'uploads/' + randomString(20);
            const newFileStream = fs.createWriteStream(newPath);
            const sourceStream = request(url);
            Object.assign(attachmentOptions, {
                displayName,
                mime: stats.type,
                size: stats.length,
            });
            sourceStream.pipe(newFileStream);
            newFileStream.on('finish', () => {
                uploadFileName(newPath, res, attachmentOptions, apiData);
            });
        }
        catch (err) {
            console.log('Download error:', err);
        }
    });
}
function getUrlStats(url) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        return new Promise((mainResolve, mainReject) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { hostname, pathname, protocol } = nodeUrl.parse(url);
            if (!hostname) {
                return mainReject(new Error('ERR_BAD_URL'));
            }
            const ipAddress = yield new Promise((resolve, reject) => {
                nodeDns.lookup(hostname, (err, address) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(address);
                });
            });
            const ipStats = checkIp(ipAddress);
            if (ipStats.isBogon) {
                return mainReject(new Error('ERR_PRIVATE_ADDRESS'));
            }
            const stats = yield new Promise((resolve, reject) => {
                request.head(url, (err, res) => {
                    if (err) {
                        return reject(err);
                    }
                    const { statusCode, headers } = res;
                    const { 'content-type': type, 'content-length': length } = headers;
                    resolve({
                        statusCode,
                        type,
                        length,
                        hostname,
                        pathname,
                    });
                });
            });
            if (stats.type && stats.statusCode !== 405) {
                return mainResolve(stats);
            }
            const typeFromBytes = yield new Promise((resolve, reject) => {
                const httpClient = {
                    'http:': nodeHttp,
                    'https:': nodeHttps,
                }[protocol];
                httpClient.get(url, (res) => {
                    if (res.statusCode !== 200) {
                        throw new Error('ERR_URL_NOT_AVAILABLE ' + res.statusCode);
                    }
                    else {
                        stats.statusCode = res.statusCode;
                        res.on('readable', () => {
                            const chunk = res.read(imageType.minimumBytes);
                            res.destroy();
                            resolve(imageType(chunk));
                        });
                    }
                });
            });
            stats.type = typeFromBytes.mime;
            mainResolve(stats);
        }));
    });
}
function copyAttachment(res, attachmentOptions, apiData) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { noteService, userId, sessionId, } = apiData;
        const { workspaceId, noteId, attachmentId, sourceShareId, sourceShareSecret, sourceWorkspaceId, sourceAttachmentId, } = attachmentOptions;
        const client = noteService.getClient(userId, 'web');
        client.setSessionId(sessionId);
        console.log('Copy attach:', {
            sourceShareId,
            sourceShareSecret,
            sourceWorkspaceId,
            sourceAttachmentId,
        });
        const createAttachmentSource = {
            attachmentGlobalId: sourceAttachmentId,
        };
        try {
            if (sourceShareId) {
                createAttachmentSource.shareId = +sourceShareId;
                createAttachmentSource.shareSecurityKey = sourceShareSecret;
            }
            else {
                createAttachmentSource.workspaceId = sourceWorkspaceId;
            }
            const createAttachmentObject = {
                noteGlobalId: noteId,
                source: createAttachmentSource,
            };
            const url = `/workspaces/${workspaceId}/attachments/${attachmentId}?isImport=false`;
            return client.put({ url }, createAttachmentObject)
                .then((result) => {
                res.status(200).json(result);
            })
                .catch((err) => {
                throw err;
            });
        }
        catch (err) {
            console.log('Copy error:', err);
        }
    });
}
function postAttachmentsMiddleware({ fileService, noteService, isDebug, }) {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session, params, body } = req;
        const { userId, sessionId } = session;
        const { workspaceId, attachmentId } = params;
        const { noteGlobalId: noteId, inList } = body;
        const apiData = {
            fileService,
            noteService,
            isDebug,
            userId,
            sessionId,
        };
        const attachmentOptions = {
            workspaceId,
            noteId,
            attachmentId,
            inList: inList === 'true',
        };
        if (req.files.attachment) {
            const uploadFile = req.files.attachment[0];
            const { type, displayName } = body;
            const { path: tempFilename, mimetype: mime } = uploadFile;
            Object.assign(attachmentOptions, {
                displayName,
                type,
                mime,
            });
            uploadFileName(tempFilename, res, attachmentOptions, apiData);
        }
        else {
            const { sourceShareId, sourceShareSecret, sourceWorkspaceId, sourceAttachmentId, sourceExternalUrl, } = body;
            if (sourceExternalUrl) {
                downloadExternalFile(sourceExternalUrl, res, attachmentOptions, apiData);
            }
            else {
                Object.assign(attachmentOptions, {
                    sourceShareId,
                    sourceShareSecret,
                    sourceWorkspaceId,
                    sourceAttachmentId,
                });
                copyAttachment(res, attachmentOptions, apiData);
            }
        }
    });
}
exports.postAttachmentsMiddleware = postAttachmentsMiddleware;
function postEvernoteAttachmentsMiddleware({ fileService, noteService, isDebug, }) {
    return (req, res) => {
        const { session, params, body } = req;
        const { mime, filename, noteId } = body;
        if (!mime) {
            return;
        }
        const raw = new Buffer(req.file.buffer.toString(), 'base64');
        const newPath = 'uploads/' + randomString(20) + '-raw';
        fs.writeFile(newPath, raw, (err) => {
            fs.readFile(newPath, (err, buf) => {
                const { userId, sessionId } = session;
                const { workspaceId, attachmentId } = params;
                const isImage = (/\/(gif|jpg|jpeg|bmp|png)$/i).test(mime);
                const hash = md5(buf);
                uploadFileName(newPath, res, {
                    workspaceId,
                    noteId,
                    attachmentId,
                    displayName: filename,
                    type: isImage ? 'image' : 'file',
                    mime,
                    inList: !isImage,
                    hash,
                    isImport: true,
                }, {
                    fileService,
                    noteService,
                    isDebug,
                    userId,
                    sessionId,
                });
            });
        });
    };
}
exports.postEvernoteAttachmentsMiddleware = postEvernoteAttachmentsMiddleware;
//# sourceMappingURL=attachments.js.map