export declare function getUsageMiddleware({ workspaceService, noteService, isDebug, }: {
    workspaceService: any;
    noteService: any;
    isDebug: any;
}): (req: any, res: any) => Promise<any>;
export declare function getInfoMiddleware({ workspaceService, noteService, isDebug, }: {
    workspaceService: any;
    noteService: any;
    isDebug: any;
}): (req: any, res: any) => Promise<any>;
