export declare function getUserSettingsMiddleware(): (req: any, res: any) => Promise<any>;
export declare function postUserSettingsMiddleware(): (req: any, res: any) => Promise<any>;
