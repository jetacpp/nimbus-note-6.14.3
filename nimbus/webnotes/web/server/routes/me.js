"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const fs = require("fs");
function postProfileName({ userService, isDebug, }) {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session } = req;
        if (!session.userId) {
            return res.status(404).json({});
        }
        try {
            const userClient = userService.getClient(req.session.userId, 'web');
            const firstname = (req.body.firstname) ? req.body.firstname : '';
            const lastname = (req.body.lastname) ? req.body.lastname : '';
            const username = (req.body.username) ? req.body.username : '';
            yield userClient.post(`/users/${req.session.userId}`, { firstname, lastname, username });
            res.status(200).json({});
        }
        catch (error) {
            if (!isDebug) {
                delete error.message;
            }
            return res.status(error.httpStatus ? error.httpStatus : 500).json(error);
        }
    });
}
exports.postProfileName = postProfileName;
function postProfileAvatar({ fileService, userService, isDebug, }) {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session } = req;
        if (!session.userId) {
            return res.status(404).json({});
        }
        const { userId, sessionId } = session;
        try {
            const userClient = userService.getClient(userId, 'web');
            const fileClient = fileService.getClient(userId, 'web');
            fileClient.setSessionId(sessionId);
            userClient.setSessionId(sessionId);
            const uploadFile = req.files.avatar[0];
            const { path: tempFilename, originalname: filename, mimetype: mime } = uploadFile;
            const file = {
                value: fs.createReadStream(tempFilename),
                options: {
                    filename,
                    contentType: mime
                }
            };
            const { name: tempFileName } = yield fileClient.request('POST', {
                url: '/tempfiles',
                formData: { file },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8'
                }
            });
            const result = yield userClient.post(`/avatars/${userId}`, { tempFileName });
            res.status(200).json(result);
        }
        catch (error) {
            console.log(error);
            if (!isDebug) {
                delete error.message;
            }
            return res.status(error.httpStatus ? error.httpStatus : 500).json(error);
        }
    });
}
exports.postProfileAvatar = postProfileAvatar;
function deleteProfileAvatar({ userService, isDebug, }) {
    return (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
        const { session } = req;
        if (!session.userId) {
            return res.status(404).json({});
        }
        const { userId, sessionId } = session;
        try {
            const userClient = userService.getClient(userId, 'web');
            userClient.setSessionId(sessionId);
            const result = userClient.delete(`/avatars/${userId}`);
            res.status(200).json(result);
        }
        catch (error) {
            if (!isDebug) {
                delete error.message;
            }
            return res.status(error.httpStatus ? error.httpStatus : 500).json(error);
        }
    });
}
exports.deleteProfileAvatar = deleteProfileAvatar;
function postPasswordMiddleware({ userService, isDebug, }) {
    return (req, res) => {
        const { userId, sessionId } = req.session;
        if (!userId) {
            return res.status(404).json({});
        }
        const userClient = userService.getClient(userId, 'web');
        const oldPassword = (req.body.oldPassword) ? req.body.oldPassword : '';
        const newPassword = (req.body.newPassword) ? req.body.newPassword : '';
        const comparePayload = { password: oldPassword, prehash: false };
        const changesPayload = { oldPassword, newPassword, saveSessionId: sessionId };
        userClient.post(`/users/${userId}/comparePassword`, comparePayload).then(() => {
            return userClient.post(`/users/${userId}/changePassword`, changesPayload);
        }).then(() => {
            res.status(200).json({});
        }).catch((err) => {
            if (!isDebug) {
                delete err.message;
            }
            if (err.httpStatus) {
                res.status(err.httpStatus);
            }
            else if (err.statusCode) {
                res.status(err.statusCode);
            }
            else {
                res.status(400);
            }
            if (typeof err.body == "string") {
                res.send({ name: "Internal Error", message: err.body });
            }
            if (err.body && err.body.message && err.body.name) {
                if (!isDebug) {
                    delete err.body.message;
                }
                res.send({ name: err.body.name, message: err.body.message });
            }
            else {
                res.json(err);
            }
        });
    };
}
exports.postPasswordMiddleware = postPasswordMiddleware;
//# sourceMappingURL=me.js.map