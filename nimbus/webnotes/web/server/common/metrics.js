"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appmetrics = require("appmetrics");
const StatsD = require("node-statsd");
const mb = (bytes) => (bytes / Math.pow(2, 20)).toFixed(0);
const percent = (val) => (val * 100).toFixed(3);
const msFromTime = (time) => {
    const SEC = 1e9;
    const MS = 1e6;
    const diff = process.hrtime(time);
    const diffInNano = diff[0] * SEC + diff[1];
    return diffInNano / MS;
};
const getRequestKind = (url) => {
    const firstPath = url.split('/')[1];
    if (firstPath === 'ws' || firstPath === 'client') {
        return 'app';
    }
    else if (firstPath === 'api' || firstPath === 'v1') {
        return 'api';
    }
    else if (firstPath === 'asset' || firstPath === 'static') {
        return 'asset';
    }
    else if (firstPath === 'attachment') {
        return 'attachment';
    }
    else {
        return 'other';
    }
};
const cpuHandler = ({ client }) => ({ process }) => {
    client.gauge('webnotes.cpu.usage', percent(process));
};
const ramHandler = ({ client }) => ({ physical }) => {
    client.gauge('webnotes.ram.usage', mb(physical));
};
const reqDurationHandler = ({ client }) => ({ req, startTime }) => {
    const requestKind = getRequestKind(req.originalUrl);
    const duration = msFromTime(startTime);
    client.timing('webnotes.req.' + requestKind + '.duration', duration);
    client.timing('webnotes.req.duration', duration);
};
const reqCountHandler = ({ client }) => ({ count }) => {
    client.gauge('webnotes.req.count', count);
};
function initMetrics({ config }) {
    if (!config || config.mock) {
        return;
    }
    const client = new StatsD(config);
    const monitoring = appmetrics.monitor();
    monitoring.on('cpu', cpuHandler({ client }));
    monitoring.on('memory', ramHandler({ client }));
}
exports.initMetrics = initMetrics;
function initAppMetrics({ config, app }) {
    if (!config || config.mock) {
        return;
    }
    const client = new StatsD(config);
    const interval = 10e3;
    let requestsCount = 0;
    setInterval(() => {
        reqCountHandler({ client })({
            count: requestsCount,
        });
        requestsCount = 0;
    }, interval);
    app.use((req, res, next) => {
        const start = process.hrtime();
        res.on('finish', () => {
            reqDurationHandler({ client })({
                req,
                startTime: start,
            });
        });
        res.on('close', () => {
            requestsCount++;
        });
        next();
    });
}
exports.initAppMetrics = initAppMetrics;
//# sourceMappingURL=metrics.js.map