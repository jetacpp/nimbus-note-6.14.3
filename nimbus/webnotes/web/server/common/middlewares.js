"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nimbus_session_1 = require("nimbus-session");
const config = require("../config");
exports.authOnly = (req, res, next) => {
    if (!req.session || !req.session.userId) {
        return res.status(404).json({});
    }
    next();
};
exports.nimbusSessionMiddleware = (req, res, next) => {
    const host = req.header('X-Orig-Host') || req.hostname;
    const domain = host.includes(config.domain) ? `.${config.domain}` : host;
    return nimbus_session_1.getSessionMiddleware({
        domain,
        cookieId: 'eversessionid',
        store: new nimbus_session_1.UserAPIStore(config.services.user, 'web'),
    })(req, res, next);
};
exports.metricsInitMiddleware = (label) => (req, res, next) => {
    req[label] = Date.now();
    next();
};
exports.allowCrossDomainMiddleware = (req, res, next) => {
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Origin', '*');
    next();
};
//# sourceMappingURL=middlewares.js.map