export declare function initMetrics({ config }: {
    config: any;
}): void;
export declare function initAppMetrics({ config, app }: {
    config: any;
    app: any;
}): void;
