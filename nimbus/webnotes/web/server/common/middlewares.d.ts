export declare const authOnly: (req: any, res: any, next: any) => any;
export declare const nimbusSessionMiddleware: (req: any, res: any, next: any) => Promise<void>;
export declare const metricsInitMiddleware: (label: any) => (req: any, res: any, next: any) => void;
export declare const allowCrossDomainMiddleware: (req: any, res: any, next: any) => void;
