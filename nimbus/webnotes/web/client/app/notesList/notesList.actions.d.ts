export declare enum NOTES_LIST_VIEW_TYPE {
    COMPACT = 1,
    COMFORTABLE = 2,
    DETAILED = 3
}
export declare const NotesListActions: {
    showSearchFilters: (() => {
        type: "SHOW_SEARCH_FILTERS";
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"SHOW_SEARCH_FILTERS">;
    hideSearchFilters: (() => {
        type: "HIDE_SEARCH_FILTERS";
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"HIDE_SEARCH_FILTERS">;
    setFilterNotes: ((folderId?: any, tag?: any, isFavorites?: any, isTasks?: any, isReminders?: any, search?: any) => {
        type: "SET_FILTER_NOTES";
        folderId: any;
        tag: any;
        isFavorites: any;
        isTasks: any;
        isReminders: any;
        search: any;
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"SET_FILTER_NOTES">;
    invalidateNotesList: ((queryId?: any) => {
        type: "INVALIDATE_NOTES_LIST";
        queryId: any;
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"INVALIDATE_NOTES_LIST">;
    requestNotesList: ((queryId?: any) => {
        type: "REQUEST_NOTES_LIST";
        queryId: any;
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"REQUEST_NOTES_LIST">;
    receiveNotesList: ((notes: any) => {
        type: "RECEIVE_NOTES_LIST";
        notes: any;
        receivedAt: number;
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"RECEIVE_NOTES_LIST">;
    setNotesListSort: ((sortField: any, sortIsDescending?: any) => {
        type: "SET_NOTES_LIST_SORT";
        sortField: any;
        sortIsDescending: any;
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"SET_NOTES_LIST_SORT">;
    removeNoteFromNotesList: ((noteId: any) => {
        type: "REMOVE_NOTE_FROM_NOTES_LIST";
        noteId: any;
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"REMOVE_NOTE_FROM_NOTES_LIST">;
    clearNotesListFilter: (() => {
        type: "CLEAR_NOTES_LIST_FILTER";
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"CLEAR_NOTES_LIST_FILTER">;
    receiveSearchExcerpts: ((searchQuery: any, searchExcerpts: any) => {
        type: "RECEIVE_SEARCH_EXCERPTS";
        searchQuery: any;
        searchExcerpts: any;
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"RECEIVE_SEARCH_EXCERPTS">;
    setNotesListViewType: ((viewType: NOTES_LIST_VIEW_TYPE) => {
        type: "SET_NOTES_LIST_VIEW_TYPE";
        viewType: NOTES_LIST_VIEW_TYPE;
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"SET_NOTES_LIST_VIEW_TYPE">;
    forceDetailedViewType: ((isForceDetailedViewType: boolean) => {
        type: "FORCE_DETAILED_VIEW_TYPE";
        isForceDetailedViewType: boolean;
    }) & import("../../../../../../../Users/oleksandrlisunov/PhpstormProjects/webnotes/node_modules/typesafe-actions/es5-commonjs/get-type").TypeGetter<"FORCE_DETAILED_VIEW_TYPE">;
};
