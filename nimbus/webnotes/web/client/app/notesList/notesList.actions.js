"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typesafe_actions_1 = require("typesafe-actions");
var NOTES_LIST_VIEW_TYPE;
(function (NOTES_LIST_VIEW_TYPE) {
    NOTES_LIST_VIEW_TYPE[NOTES_LIST_VIEW_TYPE["COMPACT"] = 1] = "COMPACT";
    NOTES_LIST_VIEW_TYPE[NOTES_LIST_VIEW_TYPE["COMFORTABLE"] = 2] = "COMFORTABLE";
    NOTES_LIST_VIEW_TYPE[NOTES_LIST_VIEW_TYPE["DETAILED"] = 3] = "DETAILED";
})(NOTES_LIST_VIEW_TYPE = exports.NOTES_LIST_VIEW_TYPE || (exports.NOTES_LIST_VIEW_TYPE = {}));
exports.NotesListActions = {
    showSearchFilters: typesafe_actions_1.createAction('SHOW_SEARCH_FILTERS'),
    hideSearchFilters: typesafe_actions_1.createAction('HIDE_SEARCH_FILTERS'),
    setFilterNotes: typesafe_actions_1.createAction('SET_FILTER_NOTES', (folderId = null, tag = null, isFavorites = null, isTasks = null, isReminders = null, search = {}) => {
        return {
            type: 'SET_FILTER_NOTES',
            folderId,
            tag,
            isFavorites,
            isTasks,
            isReminders,
            search,
        };
    }),
    invalidateNotesList: typesafe_actions_1.createAction('INVALIDATE_NOTES_LIST', (queryId = "") => {
        return {
            type: 'INVALIDATE_NOTES_LIST',
            queryId,
        };
    }),
    requestNotesList: typesafe_actions_1.createAction('REQUEST_NOTES_LIST', (queryId = "") => {
        return {
            type: 'REQUEST_NOTES_LIST',
            queryId,
        };
    }),
    receiveNotesList: typesafe_actions_1.createAction('RECEIVE_NOTES_LIST', (notes) => {
        return {
            type: 'RECEIVE_NOTES_LIST',
            notes,
            receivedAt: Date.now(),
        };
    }),
    setNotesListSort: typesafe_actions_1.createAction('SET_NOTES_LIST_SORT', (sortField, sortIsDescending = true) => {
        return {
            type: 'SET_NOTES_LIST_SORT',
            sortField,
            sortIsDescending,
        };
    }),
    removeNoteFromNotesList: typesafe_actions_1.createAction('REMOVE_NOTE_FROM_NOTES_LIST', (noteId) => {
        return {
            type: 'REMOVE_NOTE_FROM_NOTES_LIST',
            noteId,
        };
    }),
    clearNotesListFilter: typesafe_actions_1.createAction('CLEAR_NOTES_LIST_FILTER'),
    receiveSearchExcerpts: typesafe_actions_1.createAction('RECEIVE_SEARCH_EXCERPTS', (searchQuery, searchExcerpts) => {
        return {
            type: 'RECEIVE_SEARCH_EXCERPTS',
            searchQuery,
            searchExcerpts,
        };
    }),
    setNotesListViewType: typesafe_actions_1.createAction('SET_NOTES_LIST_VIEW_TYPE', (viewType) => {
        return {
            type: 'SET_NOTES_LIST_VIEW_TYPE',
            viewType,
        };
    }),
    forceDetailedViewType: typesafe_actions_1.createAction('FORCE_DETAILED_VIEW_TYPE', (isForceDetailedViewType) => {
        return {
            type: 'FORCE_DETAILED_VIEW_TYPE',
            isForceDetailedViewType,
        };
    }),
};
//# sourceMappingURL=notesList.actions.js.map