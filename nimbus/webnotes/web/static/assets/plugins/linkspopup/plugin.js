tinymce.PluginManager.add('linkspopup', function(editor) {
    var $toolbar = $("#nimbusEditorToolbar"), menu, selectedLink;

    var clearMenu = function() {
        if(menu) {
            menu.hideItem();
            menu.remove();
            menu = null;
        }
    };

    var hideMenu = function() {
        if(menu && menu.visibleItem)
            menu.hideItem();
    };

    var initMenu = function(isInnerAnchor) {
        var items = [];

        if (!isInnerAnchor) {
            items.push({
                text: "Open",
                onclick: function () {
                    if (selectedLink) {
                        var targetHref = selectedLink.getAttribute("href");
                        if (targetHref)
                            window.open(targetHref, '_blank');
                        menu.hideItem();
                    }
                }
            });
        }

        items.push({
            text: "Change",
            onclick: function () {
                tinyMCE.execCommand('mceLink');
                menu.hideItem();
            }
        });

        menu = new tinymce.ui.Menu({
            items: items,
            classes: 'linkspopup'
        });
        menu.visibleItem = false;

        menu.showItem = function() {
            this.$el.show();
            this.visibleItem = true;
        };

        menu.hideItem = function() {
            this.$el.hide();
            this.visibleItem = false;
        };

        menu.renderTo();

        editor.off('remove', clearMenu).on('remove', clearMenu);
        editor.off('blur', clearMenu).on('blur', clearMenu);
        editor.off('keydown', hideMenu).on('keydown', hideMenu);

        $toolbar.off("click", hideMenu).on("click", hideMenu);
    };

    editor.on('click', function(e) {
        hideMenu();
        if (e.target.nodeName == "A") {
            var selectedText = editor.selection.getContent({format: 'text'});
            var isInnerAnchor = e.target.hasAttribute('name');
            if(!selectedText.length) {
                selectedLink = e.target;

                initMenu(isInnerAnchor);

                setTimeout(function() {
                    menu.showItem();
                }, 1);

                // Position menu
                var pos = {x: e.pageX, y: e.pageY};

                if (!editor.inline) {
                    pos = tinymce.DOM.getPos(editor.getContentAreaContainer());
                    pos.x += e.clientX;
                    pos.y += e.clientY;
                }

                menu.moveTo(pos.x, pos.y + 10);
            }
        }
    });
});