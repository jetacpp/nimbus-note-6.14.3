tinymce.PluginManager.add('uploader', function(editor) {
    // Add a button that opens a window
    editor.addButton('uploader', {
        title: 'Insert image',
        classes: 'widget',
        onclick: function() {
            $("#editor-image-upload").trigger("addAttach");
        }
    });
});