tinymce.PluginManager.add('nimbusimagetools', function(editor) {
  function isEditableImage(img) {
    return editor.dom.is(img, 'img[data-attachment-id],[data-note-id]:not([data-mce-object],[data-mce-placeholder])');
  }

  function getSelectedImage() {
    return editor.selection.getNode();
  }

  function addButtons() {
    editor.addButton('setnotepreview', {
      title: 'Set as preview',
      cmd: 'mcennSetImagePreview'
    });

    editor.addButton('downloadattachment', {
      title: 'Download',
      cmd: 'mcennDownloadAttachment'
    });

    /*editor.addButton('showocr', {
      title: 'Get text from image',
      cmd: 'mcennShowOCR'
    });*/
  }

  function setPreview() {
    var image = getSelectedImage();
    var attachmentId = image.getAttribute('data-attachment-id');
    var noteId = image.getAttribute('data-note-id');

    $(document).trigger('nimbus-editor-set-preview', {attachmentId: attachmentId, noteId: noteId})
  }

  function download() {
    var image = getSelectedImage();
    var attachmentId = image.getAttribute('data-attachment-id');
    var noteId = image.getAttribute('data-note-id');

    $(document).trigger('nimbus-editor-download-attachment', {attachmentId: attachmentId, noteId: noteId})
  }

  function showOCR() {
    var image = getSelectedImage();
    var attachmentId = image.getAttribute('data-attachment-id');
    var noteId = image.getAttribute('data-note-id');

    $(document).trigger('nimbus-editor-show-ocr', { attachmentId: attachmentId, noteId: noteId })
  }

  tinymce.util.Tools.each({
    mcennSetImagePreview: setPreview,
    mcennDownloadAttachment: download,
    mcennShowOCR: showOCR,
  }, function(fn, cmd) {
    editor.addCommand(cmd, fn);
  });

  addButtons();

  editor.addContextToolbar(
    isEditableImage,
    'setnotepreview downloadattachment showocr'
  );
});
