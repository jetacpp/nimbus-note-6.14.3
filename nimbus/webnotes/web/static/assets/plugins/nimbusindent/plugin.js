tinymce.PluginManager.add('nimbusindent', function (editor) {
  editor.on('keydown', function(e) {
    if (e.keyCode == 9) {
      e.preventDefault();
      e.stopPropagation();

      if (!e.shiftKey) {
        editor.execCommand('Indent');
      } else {
        editor.execCommand('Outdent');
      }

      return false;
    }
  });
});