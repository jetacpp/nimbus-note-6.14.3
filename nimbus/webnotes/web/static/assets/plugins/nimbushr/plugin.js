tinymce.PluginManager.add('nimbushr', function(editor) {
    editor.addCommand('InsertHorizontalRule', function() {
        editor.execCommand('mceInsertContent', false, '<hr /><br />');
    });

    editor.addButton('hr', {
        icon: 'hr',
        tooltip: 'Horizontal line',
        cmd: 'InsertHorizontalRule'
    });

    editor.addMenuItem('hr', {
        icon: 'hr',
        text: 'Horizontal line',
        cmd: 'InsertHorizontalRule',
        context: 'insert'
    });
});
