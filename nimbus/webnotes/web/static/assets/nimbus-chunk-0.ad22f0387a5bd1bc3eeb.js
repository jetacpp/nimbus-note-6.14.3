(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./client/assets/plugins/nimbusimagetools/plugin.js":
/*!**********************************************************!*\
  !*** ./client/assets/plugins/nimbusimagetools/plugin.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/* WEBPACK VAR INJECTION */(function($) {/*** IMPORTS FROM imports-loader ***/\n(function() {\n\ntinymce.PluginManager.add('nimbusimagetools', function(editor) {\n  function isEditableImage(img) {\n    return editor.dom.is(img, 'img[data-attachment-id],[data-note-id]:not([data-mce-object],[data-mce-placeholder])');\n  }\n\n  function getSelectedImage() {\n    return editor.selection.getNode();\n  }\n\n  function addButtons() {\n    editor.addButton('setnotepreview', {\n      title: 'Set as preview',\n      cmd: 'mcennSetImagePreview'\n    });\n\n    editor.addButton('downloadattachment', {\n      title: 'Download',\n      cmd: 'mcennDownloadAttachment'\n    });\n\n    /*editor.addButton('showocr', {\n      title: 'Get text from image',\n      cmd: 'mcennShowOCR'\n    });*/\n  }\n\n  function setPreview() {\n    var image = getSelectedImage();\n    var attachmentId = image.getAttribute('data-attachment-id');\n    var noteId = image.getAttribute('data-note-id');\n\n    $(document).trigger('nimbus-editor-set-preview', {attachmentId: attachmentId, noteId: noteId})\n  }\n\n  function download() {\n    var image = getSelectedImage();\n    var attachmentId = image.getAttribute('data-attachment-id');\n    var noteId = image.getAttribute('data-note-id');\n\n    $(document).trigger('nimbus-editor-download-attachment', {attachmentId: attachmentId, noteId: noteId})\n  }\n\n  function showOCR() {\n    var image = getSelectedImage();\n    var attachmentId = image.getAttribute('data-attachment-id');\n    var noteId = image.getAttribute('data-note-id');\n\n    $(document).trigger('nimbus-editor-show-ocr', { attachmentId: attachmentId, noteId: noteId })\n  }\n\n  tinymce.util.Tools.each({\n    mcennSetImagePreview: setPreview,\n    mcennDownloadAttachment: download,\n    mcennShowOCR: showOCR,\n  }, function(fn, cmd) {\n    editor.addCommand(cmd, fn);\n  });\n\n  addButtons();\n\n  editor.addContextToolbar(\n    isEditableImage,\n    'setnotepreview downloadattachment showocr'\n  );\n});\n\n}.call(window));\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\")))\n\n//# sourceURL=webpack:///./client/assets/plugins/nimbusimagetools/plugin.js?");

/***/ }),

/***/ "./client/tinymce.app.ts":
/*!*******************************!*\
  !*** ./client/tinymce.app.ts ***!
  \*******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var tinymce_tinymce__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tinymce/tinymce */ \"./node_modules/tinymce/tinymce.js\");\n/* harmony import */ var tinymce_tinymce__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(tinymce_tinymce__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _tinymce_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tinymce.common */ \"./client/tinymce.common.ts\");\n/* harmony import */ var _assets_plugins_nimbusimagetools_plugin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./assets/plugins/nimbusimagetools/plugin */ \"./client/assets/plugins/nimbusimagetools/plugin.js\");\n/* harmony import */ var _assets_plugins_nimbusimagetools_plugin__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_assets_plugins_nimbusimagetools_plugin__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\n\n\n//# sourceURL=webpack:///./client/tinymce.app.ts?");

/***/ })

}]);