#!/usr/bin/env bash
yarn build:webnotes:ci
yarn install --pure-lockfile
yarn build:cert
yarn cert:osx
yarn compile
yarn build:osx
yarn sign:osx
yarn pack:osx
