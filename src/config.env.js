"use strict";
var ENV_PROD = 'prod';
var ENV_DEV = 'dev';
module.exports = {
    ENV_PROD: ENV_PROD,
    ENV_DEV: ENV_DEV,
    "env": ENV_PROD,
};
