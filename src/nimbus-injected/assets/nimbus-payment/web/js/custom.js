"use strict";
(function (window) {
    console.log("custom.js script load for communicate with nimbus-note-payment project");
    var $ = window.$;
    var inAppPurchase = window.nodeRequire('electron').remote.inAppPurchase;
    var MESSAGE_PAGE_KEY_PAYMENT = "nimbus-purchase";
    var ERROR_PAYMENT_FAILED = -1;
    var ERROR_SUBSCRIPTION_NOT_UNIQUE = -2;
    var toastMessage;
    var products;
    var lang = 'en';
    var trans = {};
    trans["get_nimbus_pro"] = {
        'en': 'Get Nimbus Pro',
        'ru': 'Получить Nimbus Pro',
        'de': 'Holen Sie sich Nimbus Pro'
    };
    trans["more_monthly_uploads"] = {
        'en': 'More monthly uploads',
        'ru': 'Увеличенный лимит ежемесячных загрузок',
        'de': 'Mehr monatliche uploads'
    };
    trans["send_and_synchronize"] = {
        'en': 'Send and synchronize up to 5 GB of notes to Nimbus Note every month',
        'ru': 'Отправлять и синхронизовывать заметки до 5 ГБ в Nimbus Note каждый месяц',
        'de': 'Senden und synchronisieren Sie jeden Monat bis zu 5 GB Notizen an Nimbus Note'
    };
    trans["large_attachments"] = {
        'en': 'Large attachments',
        'ru': 'Загрузка больших файлов',
        'de': 'Große Anhänge'
    };
    trans["currently_upload_files"] = {
        'en': 'Currently you can upload files up to 10 MB',
        'ru': 'В настоящее время вы можете загружать файлы размером до 10 МБ',
        'de': 'Derzeit können Sie Dateien bis zu 10 MB hochladen'
    };
    trans["you_can_attach_files"] = {
        'en': 'With Nimbus Pro, you can attach files up to 1 GB!',
        'ru': 'С Nimbus Pro вы можете прикрепить файлы размером до 1 ГБ!',
        'de': 'Mit Nimbus Pro können Sie Dateien bis zu 1 GB anhängen!'
    };
    trans["nimbus_capture_benefits"] = {
        'en': 'Nimbus Capture benefits',
        'ru': 'Преимущества Nimbus Capture',
        'de': 'Nimbus Capture-Vorteile'
    };
    trans["you_can_get_pro_for"] = {
        'en': 'You also get a PRO account for ',
        'ru': 'Вы также получаете учетную запись PRO ',
        'de': 'Sie erhalten auch einen PRO-Account '
    };
    trans["premium_support"] = {
        'en': 'Premium Support',
        'ru': 'Премиум-поддержка',
        'de': 'Premium-Unterstützung'
    };
    trans["get_quick_answers"] = {
        'en': 'Get quick answers to your questions with priority support from our help team',
        'ru': 'Получайте быстрые ответы на свои вопросы с приоритетной поддержкой нашей команды помощи',
        'de': 'Erhalten Sie schnelle Antworten auf Ihre Fragen mit Priorität Unterstützung von unserem Hilfe-Team'
    };
    trans["price_or"] = {
        'en': 'or',
        'ru': 'или',
        'de': 'oder'
    };
    trans["price_terms_and_privacy"] = {
        'en': "Subscriptions will be charged to your credit card through your iTunes account. Subscription automatically \n    renews unless auto-renew is turned off at least 24-hours before the end of the current period. Account will be \n    charged for renewal within 24-hours prior to the end of the current period, and identify the cost of the renewal. \n    You may manage your subscriptions in Account Settings after purchase. By continuing, you accept our \n    <a href=\"https://nimbusweb.me/terms-and-conditions.php\" target=\"_blank\">Terms of Service</a> and \n    <a href=\"http://nimbus.everhelper.me/privacy.php\" target=\"_blank\">Privacy Policy</a>",
        'ru': "\u0421\u0443\u043C\u043C\u0430 \u0431\u0443\u0434\u0435\u0442 \u0441\u043F\u0438\u0441\u0430\u043D\u0430 \u0441 \u043A\u0440\u0435\u0434\u0438\u0442\u043D\u043E\u0439 \u043A\u0430\u0440\u0442\u044B, \u043F\u0440\u0438\u0432\u044F\u0437\u0430\u043D\u043D\u043E\u0439 \u043A \u0443\u0447\u0451\u0442\u043D\u043E\u0439 \u0437\u0430\u043F\u0438\u0441\u0438 iTunes. \u041F\u043E\u0434\u043F\u0438\u0441\u043A\u0430 \u043E\u0431\u043D\u043E\u0432\u043B\u044F\u0435\u0442\u0441\u044F \n    \u0430\u0432\u0442\u043E\u043C\u0430\u0442\u0438\u0447\u0435\u0441\u043A\u0438, \u0435\u0441\u043B\u0438 \u0430\u0432\u0442\u043E\u043E\u0431\u043D\u043E\u0432\u043B\u0435\u043D\u0438\u0435 \u043D\u0435 \u0432\u044B\u043A\u043B\u044E\u0447\u0435\u043D\u043E \u0437\u0430 24 \u0447\u0430\u0441\u0430 \u0434\u043E \u043E\u043A\u043E\u043D\u0447\u0430\u043D\u0438\u044F \u0442\u0435\u043A\u0443\u0449\u0435\u0433\u043E \u043F\u0435\u0440\u0438\u043E\u0434\u0430. \u0421\u0447\u0435\u0442 \u0431\u0443\u0434\u0435\u0442 \u0432\u044B\u0441\u0442\u0430\u0432\u043B\u0435\u043D \n    \u0432 \u0442\u0435\u0447\u0435\u043D\u0438\u0438 24 \u0447\u0430\u0441\u043E\u0432 \u0434\u043E \u043E\u043A\u043E\u043D\u0447\u0430\u043D\u0438\u044F \u0442\u0435\u043A\u0443\u0449\u0435\u0433\u043E \u043F\u0435\u0440\u0438\u043E\u0434\u0430 \u0438 \u043E\u043F\u0440\u0435\u0434\u0435\u043B\u0438\u0442 \u0441\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C \u043E\u0431\u043D\u043E\u0432\u043B\u0435\u043D\u0438\u044F. \u0412\u044B \u043C\u043E\u0436\u0435\u0442\u0435 \u0438\u0437\u043C\u0435\u043D\u0438\u0442\u044C \u0441\u0432\u043E\u044E \u043F\u043E\u0434\u043F\u0438\u0441\u043A\u0443 \n    \u0432 \u043D\u0430\u0441\u0442\u0440\u043E\u0439\u043A\u0430\u0445 \u0430\u043A\u043A\u0430\u0443\u043D\u0442\u0430 \u043F\u043E\u0441\u043B\u0435 \u043F\u043E\u043A\u0443\u043F\u043A\u0438. \u041F\u0440\u043E\u0434\u043E\u043B\u0436\u0430\u044F, \u0412\u044B \u043F\u0440\u0438\u043D\u0438\u043C\u0430\u0435\u0442\u0435 \n    <a href=\"https://nimbusweb.me/terms-and-conditions.php\" target=\"_blank\">\u0423\u0441\u043B\u043E\u0432\u0438\u044F \u0441\u043E\u0433\u043B\u0430\u0448\u0435\u043D\u0438\u044F</a> \u0438 \n    <a href=\"http://nimbus.everhelper.me/privacy.php\" target=\"_blank\">\u041F\u043E\u043B\u0438\u0442\u0438\u043A\u0443 \u043A\u043E\u043D\u0444\u0438\u0434\u0435\u043D\u0446\u0438\u0430\u043B\u044C\u043D\u043E\u0441\u0442\u0438</a>",
        'de': "Abonnements werden Ihrer Kreditkarte \u00FCber Ihr iTunes-Konto belastet. Das Abonnement wird automatisch \n    verl\u00E4ngert, es sei denn, die automatische Verl\u00E4ngerung ist mindestens 24 Stunden vor dem Ende des aktuellen \n    Zeitraums deaktiviert. Das Konto wird f\u00FCr die Verl\u00E4ngerung innerhalb von 24 Stunden vor dem Ende des laufenden \n    Zeitraums in Rechnung gestellt und ermittelt die Kosten f\u00FCr die Verl\u00E4ngerung. Sie k\u00F6nnen Ihre Abonnements nach dem \n    Kauf in den Kontoeinstellungen verwalten. Wenn Sie fortfahren, akzeptieren Sie unsere \n    <a href=\"https://nimbusweb.me/terms-and-conditions.php\" target=\"_blank\">Nutzungsbedingungen</a> und \n    <a href=\"http://nimbus.everhelper.me/privacy.php\" target=\"_blank\">Datenschutzbestimmungen</a>"
    };
    trans["purchase_close"] = {
        'en': 'Close window',
        'ru': 'Закрыть окно',
        'de': 'Fenster schließen '
    };
    trans["discount_save"] = {
        'en': 'Save ',
        'ru': 'Экономия ',
        'de': 'Sparen '
    };
    trans["restore_purchase"] = {
        'en': 'Restore purchase',
        'ru': 'Восстановить покупку',
        'de': 'Kauf wiederherstellen'
    };
    trans["month"] = {
        'en': 'month',
        'ru': 'месяц',
        'de': 'monat'
    };
    trans["year"] = {
        'en': 'year',
        'ru': 'год',
        'de': 'jahr'
    };
    trans["title_in_app_purchase"] = {
        'en': 'Subscription purchase:',
        'ru': 'Покупка подписки:',
        'de': 'Abonnement kaufen:'
    };
    trans["products_list_unable_to_get"] = {
        'en': 'Unable to display purchase products, please try close then open payment page.',
        'ru': 'Невозможно отобразить продукты для покупки, попробуйте закрыть, а затем откройте страницу оплаты.',
        'de': 'Die Kaufprodukte können nicht angezeigt werden. Bitte versuchen Sie, die Zahlungsseite zu schließen.'
    };
    trans["user_not_allowed_to_make_in_app_purchase"] = {
        'en': 'Sorry, you are not allowed to make in-app purchase.',
        'ru': 'Извините, нет возможности делать покупки в приложении.',
        'de': 'Entschuldigung, Sie dürfen keinen In-App-Kauf tätigen.'
    };
    trans["purchase_of_product_is_not_valid"] = {
        'en': 'Trying to buy not valid product',
        'ru': 'Попытка купить недействительный продукт',
        'de': 'Versuch, ein ungültiges produkt zu kaufen'
    };
    trans["purchase_of_product_added_to_the_queue"] = {
        'en': 'The payment has been added to the payment queue',
        'ru': 'Платеж был добавлен в очередь оплаты',
        'de': 'Die Zahlung wurde zur Zahlungswarteschlange hinzugefügt'
    };
    trans["purchase_of_product_in_progress"] = {
        'en': 'Purchasing of product',
        'ru': 'Приобретение продукта',
        'de': 'Einkauf von Produkten'
    };
    trans["purchase_of_product_has_been_done"] = {
        'en': 'Congratulations, success purchase of product',
        'ru': 'Поздравляем с успешная покупкой продукта',
        'de': 'Herzlichen Glückwunsch, Erfolg Kauf des produkts'
    };
    trans["purchase_of_product_has_been_failed"] = {
        'en': 'Failed to purchase product',
        'ru': 'Не удалось приобрести продукт',
        'de': 'Fehler beim Kauf des produkts'
    };
    trans["purchase_of_product_has_been_restored"] = {
        'en': 'The purchase of product has been restored',
        'ru': 'Покупка продукта была восстановлена',
        'de': 'Der Kauf des produkts wurde wiederhergestellt'
    };
    trans["purchase_of_product_has_been_deferred"] = {
        'en': 'The purchase has been deferred for product',
        'ru': 'Покупка была отложена для продукта',
        'de': 'Der Kauf wurde für das produkt zurückgestellt'
    };
    trans["purchase_of_product_has_been_failed_could_not_verify"] = {
        'en': 'Purchase failed, could not verify purchase for product',
        'ru': 'Покупка завершилась неудачно, не удалось проверить покупку для продукта',
        'de': 'Der Kauf ist fehlgeschlagen. Der Kauf des Produkts konnte nicht bestätigt werden'
    };
    trans["purchase_of_product_has_been_failed_detect_duplication"] = {
        'en': 'Purchase failed, detect duplication for purchase of product',
        'ru': 'Ошибка покупки, обнаруженина повтороная покупка продукта',
        'de': 'Kauf fehlgeschlagen, Duplikation beim Kauf eines Produkts erkennen'
    };
    trans["purchase_of_product_has_been_failed_internal_problem"] = {
        'en': 'Purchase failed, can not finish purchase for product',
        'ru': 'Ошибка покупки, невозможно завершить покупку продукта',
        'de': 'Der Kauf ist fehlgeschlagen, der Kauf für das Produkt kann nicht abgeschlossen werden'
    };
    var getTranslationByKey = function (transKey) {
        return typeof (trans[transKey][lang]) === "undefined" ? transKey : trans[transKey][lang];
    };
    window.elGetIpcRequest('event:initApp:request', function (content) {
        lang = content.language;
        $("[trans]").each(function () {
            var $self = $(this);
            var transKey = $self.attr("trans");
            $self.html(getTranslationByKey(transKey));
        });
    });
    window.elGetIpcRequest('event:upload:products:recipe:requests', function (content) {
        var translationKey = '';
        if (!content.productInfo) {
            translationKey = 'purchase_of_product_has_been_failed_internal_problem';
            console.log("Upload product recipe has a problems with productInfo: ", content.productInfo);
        }
        if (!content.response) {
            translationKey = 'purchase_of_product_has_been_failed_internal_problem';
            console.log("Upload product recipe has a problems with api response: ", content.response);
        }
        if (translationKey) {
            window.elSendIpcRequest('event:client:error:log', {
                err: null, response: null,
                description: "Purchase problem => upload:products:recipe:requests => BadData",
                data: {
                    content: content
                }
            });
            window.elSendIpcRequest('event:purchase:products:dialog:requests', {
                dialogOptions: {
                    type: "error",
                    message: getTranslationByKey(translationKey) + ": " + content.productInfo + "."
                }
            });
            window.elSendIpcRequest('event:close:notification:requests', {});
            return;
        }
        var productInfo = content.productInfo;
        var response = content.response;
        if (response.errorCode) {
            window.elSendIpcRequest('event:client:error:log', {
                err: null, response: null,
                description: "Purchase problem => upload:products:recipe:requests => BadData",
                data: {
                    response: response,
                    productInfo: productInfo
                }
            });
            translationKey = 'purchase_of_product_has_been_failed_internal_problem';
            switch (response.errorCode) {
                case ERROR_PAYMENT_FAILED: {
                    translationKey = 'purchase_of_product_has_been_failed_could_not_verify';
                    break;
                }
                case ERROR_SUBSCRIPTION_NOT_UNIQUE: {
                    translationKey = 'purchase_of_product_has_been_failed_detect_duplication';
                    break;
                }
            }
            window.elSendIpcRequest('event:close:notification:requests', {});
            window.elSendIpcRequest('event:purchase:products:dialog:requests', {
                dialogOptions: {
                    type: "error",
                    message: getTranslationByKey(translationKey) + ": " + productInfo + "."
                }
            });
        }
        else {
            translationKey = 'purchase_of_product_has_been_done';
            if (content.restore) {
                translationKey = 'purchase_of_product_has_been_restored';
            }
            window.elSendIpcRequest('event:purchase:products:dialog:requests', {
                dialogOptions: {
                    type: "info",
                    message: getTranslationByKey(translationKey) + ": " + productInfo + "."
                },
                closeWindow: true
            });
        }
    });
    window.elGetIpcRequest('event:display:productsId:list:requests', function (content) {
        var productsIdList = content.productsIdList;
        if (!Array.isArray(productsIdList) || productsIdList.length <= 0) {
            console.log('Unable to get the products id list: ', content);
        }
        if (window.getPurchaiseProducts) {
            window.getPurchaiseProducts(productsIdList, renderPurchaiseProducts);
        }
    });
    window.elGetIpcRequest('event:display:products:requests', function (content) {
        products = content.products;
        var displayError = content.displayError;
        if (!Array.isArray(products) || products.length <= 0) {
            if (displayError) {
                showMessagePopup("" + getTranslationByKey('products_list_unable_to_get'));
            }
        }
        renderPurchaiseProducts(products);
    });
    var getProductInfoByIndentifier = function (productIndentifier, key) {
        if (!products) {
            window.elSendIpcRequest('event:client:error:log', {
                err: null, response: null,
                description: "Purchase problem => getProductInfoByIndentifier",
                data: {
                    productIndentifier: productIndentifier,
                    key: key,
                    products: products
                }
            });
            return '';
        }
        var product;
        for (var i = 0; i < products.length; i++) {
            if (products[i].productIdentifier === productIndentifier) {
                product = products[i];
                break;
            }
        }
        if (!product) {
            window.elSendIpcRequest('event:client:error:log', {
                err: null, response: null,
                description: "Purchase problem => getProductInfoByIndentifier",
                data: {
                    productIndentifier: productIndentifier,
                    key: key,
                    product: product
                }
            });
            return '';
        }
        return typeof (product[key]) !== 'undefined' ? product[key] : '';
    };
    var renderPurchaiseProducts = function (products) {
        var $productItems = $('.nimbus_payment__purchase_items');
        var $productItemTemplate = $('#nimbus_payment_purchase_item_template .nimbus_payment__purchase_items__item');
        var $productOrTemplate = $('#nimbus_payment_purchase_or_template .nimbus_payment__purchase_items__or');
        products.sort(function (a, b) {
            var priceA = a.price;
            var priceB = b.price;
            var comparison = 0;
            if (priceA > priceB) {
                comparison = 1;
            }
            else if (priceA < priceB) {
                comparison = -1;
            }
            return comparison;
        });
        $productItems.html("");
        var lessPriceProductMonthPrice = 0;
        for (var index = 0; index < products.length; index++) {
            var product = products[index];
            var $productItem = $productItemTemplate.clone();
            $productItem.attr('data-product-indetifier', product.productIdentifier);
            var $productItemTitle = $productItem.find('.nimbus_payment__purchase_items__button');
            product.period = { 'title': "", 'monthCount': "" };
            product.discount = { 'percentage': 0 };
            if (product.productIdentifier === 'mac.nimbusnote.subs.monthly') {
                product.period.title = "/" + getTranslationByKey('month');
                product.period.monthCount = 1;
                product.discount.percentage = 0;
                lessPriceProductMonthPrice = product.price;
            }
            else if (product.productIdentifier === 'mac.nimbusnote.subs.yearly') {
                product.period.title = "/" + getTranslationByKey('year');
                product.period.monthCount = 12;
                product.discount.percentage = 0;
            }
            if (product.period.monthCount > 1 && lessPriceProductMonthPrice > 0) {
                var productFullPrice = product.period.monthCount * lessPriceProductMonthPrice;
                if (productFullPrice && product.price) {
                    var discount = parseInt(((productFullPrice / product.price) - 1) * 100);
                    if (discount > 0) {
                        product.discount.percentage = discount;
                    }
                }
            }
            if ($productItemTitle.length) {
                $productItemTitle.html("" + product.formattedPrice + product.period.title);
            }
            var $productDiscount = $productItem.find('.nimbus_payment__purchase_items__discount');
            if (product.discount.percentage > 0) {
                var $productDiscountValue = $productItem.find('.nimbus_payment__purchase_items__discount_value');
                $productDiscountValue.html(product.discount.percentage + "%");
            }
            else {
                $productDiscount.remove();
            }
            $productItems.append($productItem);
            if (index !== (products.length - 1)) {
                var $productOr = $productOrTemplate.clone();
                $productItems.append($productOr);
            }
        }
        if (products.length) {
            renderRestorePurchaseButton();
        }
    };
    var renderRestorePurchaseButton = function () {
        var $purchaseBtn = $('.nimbus_payment__restore_block__button');
        if ($purchaseBtn.length) {
            $purchaseBtn.show(0);
        }
    };
    var purchaseProductClick = function (productIndetifier, productQuantity) {
        if (productQuantity === void 0) { productQuantity = 1; }
        var isProductValid = inAppPurchase.purchaseProduct(productIndetifier, productQuantity);
        if (!isProductValid) {
            window.elSendIpcRequest('event:client:error:log', {
                err: null, response: isProductValid,
                description: "Purchase problem => inAppPurchase.purchaseProduct",
                data: {
                    productIndetifier: productIndetifier,
                    productQuantity: productQuantity
                }
            });
            return showMessagePopup(getTranslationByKey('purchase_of_product_is_not_valid') + ": " + productIndetifier + ".", { type: window.MESSAGE_TYPE_ERROR });
        }
    };
    var listenPaymentItemsClick = function () {
        var events = 'click touchend';
        var paymentItemClass = '.nimbus_payment__purchase_items__item';
        $(document).off(events, paymentItemClass, paymentItemClickHandler)
            .on(events, paymentItemClass, paymentItemClickHandler);
    };
    var paymentItemClickHandler = function (event) {
        event.preventDefault();
        var productIndetifier = $(this).attr('data-product-indetifier');
        if (productIndetifier) {
            purchaseProductClick(productIndetifier);
        }
    };
    var listenPaymentTransactionsUpdate = function () {
        inAppPurchase.off('transactions-updated', transactionUpdateHandler)
            .on('transactions-updated', transactionUpdateHandler);
    };
    var listenPaymentRestoreClick = function () {
        $(document).on('click', '.nimbus_payment__restore_block__button', function () {
            console.log('start restore purchase');
            purchaseProductClick('mac.nimbusnote.subs.monthly');
        });
    };
    var closeWindowClickHandler = function () {
        window.elSendIpcRequest('event:close:payment:window:requests', {});
    };
    var listenWindowCloseClick = function () {
        var events = 'click';
        var closePopupClass = '.nimbus_payment__close > span';
        $(document).off(events, closePopupClass, closeWindowClickHandler)
            .on(events, closePopupClass, closeWindowClickHandler);
    };
    var transactionUpdateHandler = function (event, transactions) {
        if (!Array.isArray(transactions)) {
            return;
        }
        transactions.forEach(function (transaction) {
            var payment = transaction.payment;
            var paymentTitle = getProductInfoByIndentifier(payment.productIdentifier, 'localizedTitle');
            var paymentPrice = getProductInfoByIndentifier(payment.productIdentifier, 'formattedPrice');
            var paymentPeriod = getProductInfoByIndentifier(payment.productIdentifier, 'period');
            var productInfo = paymentTitle + " " + paymentPrice + paymentPeriod.title;
            var receiptURL;
            console.log("payment transaction: ", transaction);
            switch (transaction.transactionState) {
                case 'purchasing':
                    showMessagePopup(getTranslationByKey('purchase_of_product_in_progress') + ": " + productInfo + " ...", {
                        type: window.MESSAGE_TYPE_INFO,
                        hideAfter: false
                    });
                    break;
                case 'purchased':
                    receiptURL = decodeURI(inAppPurchase.getReceiptURL());
                    console.log("productInfo: " + productInfo);
                    console.log("Purchased Product receipt URL: " + receiptURL);
                    window.elSendIpcRequest('event:upload:products:recipe:requests', {
                        url: receiptURL,
                        productInfo: productInfo,
                        transaction: transaction
                    });
                    inAppPurchase.finishTransactionByDate(transaction.transactionDate);
                    break;
                case 'failed':
                    showMessagePopup(getTranslationByKey('purchase_of_product_has_been_failed') + ": " + productInfo + ".", {
                        type: window.MESSAGE_TYPE_ERROR,
                        hideAfter: 15000
                    });
                    window.elSendIpcRequest('event:client:error:log', {
                        productInfo: productInfo,
                        transaction: transaction
                    });
                    window.elSendIpcRequest('event:update:user:usage:requests', {
                        productInfo: productInfo
                    });
                    inAppPurchase.finishTransactionByDate(transaction.transactionDate);
                    break;
                case 'restored':
                    receiptURL = decodeURI(inAppPurchase.getReceiptURL());
                    console.log("Restored Product receipt URL: " + receiptURL);
                    window.elSendIpcRequest('event:upload:products:recipe:requests', {
                        url: receiptURL,
                        productInfo: productInfo,
                        restore: true
                    });
                    inAppPurchase.finishTransactionByDate(transaction.transactionDate);
                    break;
                case 'deferred':
                    showMessagePopup(getTranslationByKey('purchase_of_product_has_been_deferred') + ": " + productInfo + ".", {
                        type: window.MESSAGE_TYPE_WARNING,
                        hideAfter: 15000
                    });
                    window.elSendIpcRequest('event:client:error:log', {
                        productInfo: productInfo,
                        transaction: transaction
                    });
                    break;
                default:
                    break;
            }
        });
    };
    window.elGetIpcRequest('event:display:notification:requests', function (content) {
        if (toastMessage) {
            toastMessage.reset();
        }
        toastMessage = $.toast(window.getToastMessageConfig(content));
    });
    window.elGetIpcRequest('event:close:notification:requests', function () {
        if (toastMessage) {
            toastMessage.reset();
        }
    });
    var showMessagePopup = function (data, settings) {
        if (settings === void 0) { settings = {}; }
        if (typeof (data) === 'string') {
            data = { text: data };
        }
        data.title = data.title || getTranslationByKey("title_in_app_purchase");
        window.elSendIpcRequest('event:display:notification:requests', {
            data: data,
            id: MESSAGE_PAGE_KEY_PAYMENT,
            settings: settings
        });
        console.log("Notification data: ", data);
    };
    listenWindowCloseClick();
    if (!window.canMakePayments()) {
        showMessagePopup(getTranslationByKey('user_not_allowed_to_make_in_app_purchase'), { type: window.MESSAGE_TYPE_WARNING });
    }
    else {
        listenPaymentItemsClick();
        listenPaymentTransactionsUpdate();
        listenPaymentRestoreClick();
        window.elSendIpcRequest('event:display:products:requests', { displayError: false });
        window.elSendIpcRequest('event:display:productsId:list:requests', {});
    }
})(window);
