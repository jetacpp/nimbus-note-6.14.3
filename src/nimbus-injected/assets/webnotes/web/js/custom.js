"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
(function (window) {
    console.log("custom.js script load for communicate with webnotes project");
    var $ = window.$;
    var toastMessage;
    function getTranslation(texts) {
        if (typeof (texts[getCustomerLanguage()]) !== 'undefined') {
            return texts[getCustomerLanguage()];
        }
        else if (typeof (texts['en']) !== 'undefined') {
            return texts['en'];
        }
        return '';
    }
    var LAST_SYNC_TITLE = {
        'en': 'Last sync: ',
        'ru': 'Последний синк: ',
        'de': 'Letzte sync: '
    };
    var SYNC_STATUS_AUTHORIZED = {
        code: "AUTHORIZED",
        text: {
            'en': 'Authenticate...',
            'ru': 'Идентификация...',
            'de': 'Authentifizieren...'
        }
    };
    var SYNC_DOWNLOADING_META_INFO = {
        code: "DOWNLOADING_META_INFO",
        text: {
            'en': 'Downloading meta info...',
            'ru': 'Загрузка мета-данных...',
            'de': 'Meta info herunterladen...'
        }
    };
    var SYNC_REMOVE_FOLDERS = {
        code: "REMOVE_FOLDERS",
        text: {
            'en': 'Remove folder...',
            'ru': 'Удаление папок...',
            'de': 'Entfernen sie ordner...'
        }
    };
    var SYNC_REMOVE_NOTES = {
        code: "REMOVE_NOTES",
        text: {
            'en': 'Remove notes...',
            'ru': 'Удаление заметок...',
            'de': 'Notizen entfernen'
        }
    };
    var SYNC_REMOVE_ATTACHMENTS = {
        code: "REMOVE_ATTACHMENTS",
        text: {
            'en': 'Remove attachments...',
            'ru': 'Удаление аттачей...',
            'de': 'Anhänge entfernen...'
        }
    };
    var SYNC_UPDATE_CONTENT = {
        code: "UPDATE_CONTENT",
        text: {
            'en': 'Updating content...',
            'ru': 'Обновление контента...',
            'de': 'Inhalt aktualisieren...'
        }
    };
    var SYNC_DOWNLOAD_NOTES = {
        code: "DOWNLOAD_NOTES",
        text: {
            'en': 'Updating notes ',
            'ru': 'Обновление заметок ',
            'de': 'Notizen aktualisieren '
        }
    };
    var SYNC_DOWNLOAD_ATTACHMENT = {
        code: "DOWNLOAD_ATTACHMENT",
        text: {
            'en': 'Downloading attachments ',
            'ru': 'Скачивание файлов ',
            'de': 'Anhang herunterladen '
        }
    };
    var SYNC_UPLOAD_FOLDERS = {
        code: "UPLOAD_FOLDERS",
        text: {
            'en': 'Uploading folders...',
            'ru': 'Передача папок...',
            'de': 'Ordner hochladen...'
        }
    };
    var SYNC_UPLOAD_TAGS = {
        code: "UPLOAD_TAGS",
        text: {
            'en': 'Uploading tags...',
            'ru': 'Передача тегов...',
            'de': 'Tags hochladen...'
        }
    };
    var SYNC_UPLOAD_NOTES = {
        code: "UPLOAD_NOTES",
        text: {
            'en': 'Uploading notes...',
            'ru': 'Передача заметок...',
            'de': 'Notizen hochladen...'
        }
    };
    var SYNC_UPLOAD_ATTACHMENTS = {
        code: "UPLOAD_ATTACHMENTS",
        text: {
            'en': 'Uploading attachments...',
            'ru': 'Передача файлов...',
            'de': 'Anhänge hochladen...'
        }
    };
    var SYNC_TRAFFIC_LIMIT = {
        code: "TRAFFIC_LIMIT",
        text: {
            'en': 'Traffic limit. Please, get Pro Account',
            'ru': 'Лимит траффика. Пожалуйста, приобретите Pro Аккаунт',
            'de': 'Traffic-Limit. Bitte erhalten Pro Account'
        }
    };
    var SYNC_STATUS_NOT_AVAILABLE = {
        code: "NOT_AVAILABLE",
        text: {
            'en': 'Not available',
            'ru': 'Не доступно',
            'de': 'Nicht verfügbar'
        }
    };
    var SYNC_STATUS_PAUSED = {
        code: "PAUSED",
        text: {
            'en': 'Sync paused',
            'ru': 'Пауза синхронизации',
            'de': 'Sync pausiert'
        }
    };
    var SYNC_STATUS_CANCELED = {
        code: "CANCELED",
        text: {
            'en': 'Sync failed',
            'ru': 'Ошибка синхронизации',
            'de': 'Zeitfehler'
        }
    };
    var SYNC_STATUS_READY = {
        code: "READY",
        text: {
            'en': 'Start Sync',
            'ru': 'Запустить синхронизацию',
            'de': 'Starten Sie die Synchronisierung'
        },
        cursor: 'pointer'
    };
    var SYNC_STATUS_DONE = {
        code: "DONE",
        text: {
            'en': 'Sync complete',
            'ru': 'Синхронизация завершена',
            'de': 'Synch abgeschlossen'
        },
    };
    var NEED_SYNC_BEFORE_SHARE = {
        code: "NEED_SYNC_BEFORE_SHARE",
        text: {
            'en': 'Please, make Sync before Share',
            'ru': 'Сделайте Синхронизацию перед Шарингом',
            'de': 'Bitte machen Sie Synch vor Freigabe'
        },
        cursor: 'pointer'
    };
    var syncTimerForDone = null;
    function setCustomerLanguage(language) {
        window.customerLanguage = language;
    }
    function setEventOriginURL(originURL) {
        window.originURL = originURL;
    }
    function getCustomerLanguage() {
        return window.customerLanguage || 'en';
    }
    function getEventOriginURL() {
        return window.originURL || '';
    }
    window.elGetIpcRequest('event:initApp:request', function (content) {
        if (content.contextMenu) {
            window.setAppContextMenu(content.contextMenu);
        }
        if (content.language) {
            setCustomerLanguage(content.language);
        }
        if (content.originURL) {
            setEventOriginURL(content.originURL);
        }
        elInitContent(content);
    });
    window.elGetIpcRequest('event:client:autosync:request', function (content) {
        var workspaceId = content.workspaceId;
        elSendAutoSyncRequest({ workspaceId: workspaceId });
    });
    window.elGetIpcRequest('event:change:language:response', function (content) {
        if (content.contextMenu) {
            window.setAppContextMenu(content.contextMenu);
        }
        if (content.language) {
            setCustomerLanguage(content.language);
        }
        requestSyncPanelReload();
    });
    window.elGetIpcRequest('event:logout:request', function () {
        elProcessClientLogout();
    });
    window.elGetIpcRequest('event:client:display:settings:response', function () {
        elDisplaySettings();
    });
    window.elGetIpcRequest('event:client:traffic:info:response', function () {
        elTrafficInfo();
    });
    window.elGetIpcRequest('event:client:update:inline:attach:response', function (content) {
        elUpdateInlineAttach(content);
    });
    window.elGetIpcRequest('event:client:set:active:workspace:request', function (content) {
        var workspaceId = content.workspaceId, onlineStatus = content.onlineStatus;
        toggleClientSync({ workspaceId: workspaceId, show: onlineStatus });
        var workspaceSelector = '';
        if (typeof (workspaceId) !== 'undefined') {
            workspaceSelector = "[data-workspace-id=" + workspaceId + "]";
            if (!workspaceId) {
                workspaceSelector = "[data-workspace-is-default=true]";
            }
        }
        var $syncBar = $('.workspace-sync');
        var $syncEl = $syncBar.find(".workspace-sync--action" + workspaceSelector);
        if ($syncEl.length) {
            if ($syncBar.hasClass('hidden')) {
                $syncBar.removeClass('hidden');
            }
        }
    });
    window.elGetIpcRequest('event:client:add:note:response', function () {
        elAddNote();
    });
    window.elGetIpcRequest('event:client:find:notes:response', function () {
        elFindNotes();
    });
    window.elGetIpcRequest('event:client:send:notification:response', function (content) {
        window.elSendNotification(content);
    });
    window.elGetIpcRequest('event:client:remove:workspace:response', function (content) {
        elRemoveWorkspaceForAccount(content);
    });
    window.elGetIpcRequest('event:client:remove:organization:response', function (content) {
        elRemoveOrgForAccount(content);
    });
    window.elGetIpcRequest('event:client:update:workspace:members:response', function (content) {
        elUpdateWorkspaceMembersForAccount(content);
    });
    window.elGetIpcRequest('event:client:update:workspace:invites:response', function (content) {
        elUpdateWorkspaceInvitesForAccount(content);
    });
    window.elGetIpcRequest('event:client:update:workspaces:access:response', function (content) {
        elUpdateWorkspacesAccessForAccount(content);
    });
    window.elGetIpcRequest('event:client:update:workspaces:premium:response', function (content) {
        elUpdateWorkspacesPremiumForAccount(content);
    });
    window.elGetIpcRequest('event:client:update:workspace:response', function (content) {
        elUpdateWorkspaceForAccount(content);
    });
    window.elGetIpcRequest('event:client:update:organization:response', function (content) {
        elUpdateOrgForAccount(content);
    });
    window.elGetIpcRequest('event:client:workspace:message:response', function (content) {
        elDisplayMessageForAccount(content);
    });
    window.elGetIpcRequest('event:client:me:name:update:response', function (content) {
        elUpdateUserName(content);
    });
    window.elGetIpcRequest('event:client:me:avatar:remove:response', function (content) {
        elRemoveUserAvatar(content);
    });
    window.elGetIpcRequest('event:client:popup:too_many_notes:response', function (content) {
        elDisplayNotesCountLimitPopup(content);
    });
    window.elGetIpcRequest('event:client:note:text:update:response', function (content) {
        updateNoteTextForAccount(content);
    });
    window.elGetIpcRequest('event:client:popup:limits:show:response', function (content) {
        elDisplayLimitNoticeForAccount(content);
    });
    window.elGetIpcRequest('event:client:popup:rate:app:show:response', function (content) {
        elDisplayRateNoticeForAccount(content);
    });
    window.elGetIpcRequest('event:client:login:response', function (content) {
        elDisplayClientLogin(content.login);
    });
    window.elGetIpcRequest('event:client:autosync:response', function (content) {
        var workspaceId = content.workspaceId;
        elClientSyncHandler({ workspaceId: workspaceId });
    });
    window.elGetIpcRequest('event:client:sync:response', function (content) {
        elDisplayClientSync(content);
    });
    window.elGetIpcRequest('event:display:angular:notification:requests', function (content) {
        if (toastMessage) {
            toastMessage.reset();
        }
        content.settings.transparent = 0.5;
        toastMessage = $.toast(window.getToastMessageConfig(content));
    });
    window.elGetIpcRequest('event:display:productsId:list:requests', function (content) {
        var productsIdList = content.productsIdList;
        if (!Array.isArray(productsIdList) || productsIdList.length <= 0) {
            console.log('Unable to get the products id list: ', content);
        }
        if (window.getPurchaiseProducts) {
            window.getPurchaiseProducts(productsIdList, function (productsList) {
                productsList = [];
                if (productsList && !productsList.length) {
                    window.getPurchaiseProducts(productsIdList);
                }
            });
        }
    });
    window.elGetIpcRequest('event:editor:update:synced-status:request', function (content) {
        handleEditorUpdateSyncedStatusRequest(content);
    });
    window.elGetIpcRequest('event:editor:bg:note-sync:request', function (content) {
        handleBgNoteSync(content);
    });
    window.elGetIpcRequest('event:editor:update:attachment:request', function (content) {
        handleEditorUpdateAttachment(content);
    });
    window.elGetIpcRequest('event:client:update:editor:downloaded:attach:response', function (content) {
        handleEditorUpdateDownloadedAttachment(content);
    });
    window.elGetIpcRequest('event:navigate:to:internal:note:requests', function (content) {
        handleEditorNavigateToInternalNote(content);
    });
    window.elGetIpcRequest('event:export:set:status:requests', function (content) {
        elSetExportStatus(content);
    });
    window.elGetIpcRequest('event:export:set:progress:requests', function (content) {
        elSetExportProgress(content);
    });
    window.elGetIpcRequest('event:client:export:workspace:notes:response', function () {
        elExportWorkspaceNotes();
    });
    window.elSendIpcRequest('event:display:productsId:list:requests', {});
    if (window) {
        window.addEventListener('message', function (event) {
            if (event.origin !== getEventOriginURL())
                return;
            var eventName = event.data.eventName;
            switch (eventName) {
                case 'event:web:app:reload:request': {
                    requestSyncPanelReload();
                    break;
                }
                case 'event:web:app:print:request': {
                    var htmlContents = event.data.htmlContents;
                    requestPrintNote(htmlContents);
                    break;
                }
                case 'event:web:app:playVideoTab:request': {
                    var _a = event.data, htmlContents = _a.htmlContents, attachment = _a.attachment;
                    requestPlayVideoTab(htmlContents, attachment);
                    break;
                }
                case 'event:web:app:closeVideoTab:request': {
                    var globalId = event.data.globalId;
                    requestCloseVideoTab(globalId);
                    break;
                }
                case 'event:web:app:handleSaveNote:request': {
                    var note = event.data.note;
                    handleSaveNote(note);
                    break;
                }
                case 'event:web:app:handleSelectNote:request': {
                    var _b = event.data, workspaceId = _b.workspaceId, globalId = _b.globalId;
                    handleSelectNote({ workspaceId: workspaceId, globalId: globalId });
                    break;
                }
                case 'event:client:clear:sync:data:request': {
                    handleClearSyncData();
                    break;
                }
                case 'event:web:app:start-sync-attach:request': {
                    var attachment = event.data.attachment;
                    handleStartSingleAttachSync(attachment);
                    break;
                }
                case 'event:web:app:stop-sync-attach:request': {
                    var attachment = event.data.attachment;
                    handleStopSingleAttachSync(attachment);
                    break;
                }
                case 'event:web:app:set:active:workspace:request': {
                    var workspaceId = event.data.workspaceId;
                    handleSetActiveWorkspace(workspaceId);
                    break;
                }
                case 'event:web:app:set:active:organization:request': {
                    var org = event.data.org;
                    handleSetActiveOrganization(org);
                    break;
                }
                case 'event:web:app:print:attachment:request': {
                    var attachment = event.data.attachment;
                    handlePrintAttachment(attachment);
                    break;
                }
                case 'event:web:app:copy:text:to:clipboard:request': {
                    var text = event.data.text;
                    handleCopyTextToClipBoard(text);
                    break;
                }
                case 'event:web:editor:note:update:request': {
                    handleEditorUpdateNote(event.data);
                    break;
                }
                case 'event:web:app:handleEditorSyncDownload:request': {
                    handleEditorSyncDownload(event.data);
                    break;
                }
                case 'event:web:app:handleOpenUrl:request': {
                    var url = event.data.url;
                    handleOpenUrl(url);
                    break;
                }
                case 'event:web:app:handleOpenHelp:request': {
                    handleOpenHelp();
                    break;
                }
                case 'event:web:app:handleOpenEditorHelp:request': {
                    handleOpenEditorHelp();
                    break;
                }
                case 'event:web:app:handleOpenSubmitBug:request': {
                    handleOpenSubmitBug();
                    break;
                }
                case 'event:web:app:remove:attachments:request': {
                    handleRemoveAttachments(event.data);
                    break;
                }
                case 'event:web:app:export:active:note:request': {
                    handleExportNote(event.data);
                    break;
                }
                case 'event:web:app:open:export:path': {
                    handleOpenExportPath(event.data);
                    break;
                }
                case 'event:web:app:cancel:export': {
                    handleCancelExport(event.data);
                    break;
                }
            }
        }, false);
    }
    function elInitContent(content) {
        if ($) {
            elInitTemplateEventsHandlers();
            elChangeOnlineStatusHandlers();
        }
    }
    function elChangeOnlineStatusHandlers() {
        elProcessChangeOnlineStatus();
        window.removeEventListener('online', elProcessChangeOnlineStatus);
        window.addEventListener('online', elProcessChangeOnlineStatus);
        window.removeEventListener('offline', elProcessChangeOnlineStatus);
        window.addEventListener('offline', elProcessChangeOnlineStatus);
    }
    function elProcessChangeOnlineStatus(event) {
        if (event === void 0) { event = null; }
        var onlineStatus;
        if (event) {
            onlineStatus = (event.type === "online");
        }
        else {
            onlineStatus = window.elAppIsOnline();
        }
        window.elSendIpcRequest('event:online:changed', { "online": onlineStatus });
        toggleClientSync({ show: onlineStatus });
    }
    function syncAcceleratorHandler(event) {
        if ((event.ctrlKey || event.metaKey) && event.which === 83) {
            event.preventDefault();
            elSendAutoSyncRequest({});
            return false;
        }
    }
    function elInitTemplateEventsHandlers() {
        $(document).off("keydown", syncAcceleratorHandler).on("keydown", syncAcceleratorHandler);
    }
    function elProcessClientLogout() {
        window.elSendIpcRequest('event:logout:response', { "logout": true });
    }
    function elDisplayClientLogin(login) {
        var $syncBar = $('.workspace-sync');
        if ($syncBar) {
            if (login) {
                elDisplayClientSync({
                    status: SYNC_STATUS_READY.code
                });
            }
            else {
                elDisplayClientSync({
                    status: SYNC_STATUS_NOT_AVAILABLE.code
                });
            }
        }
    }
    function elDisplayClientSync(syncData) {
        if (syncData) {
            var workspaceId = syncData.workspaceId, status_1 = syncData.status;
            var syncDisplayData = {};
            if (syncTimerForDone && status_1 !== SYNC_STATUS_DONE.code) {
                clearTimeout(syncTimerForDone);
                syncTimerForDone = null;
            }
            var workspaceSelector = '';
            if (typeof (workspaceId) !== 'undefined') {
                workspaceSelector = "[data-workspace-id=" + workspaceId + "]";
                if (!workspaceId) {
                    workspaceSelector = "[data-workspace-is-default=true]";
                }
            }
            var $syncBar = $('.workspace-sync');
            var $syncEl_1 = $syncBar.find(".workspace-sync--action" + workspaceSelector);
            if ($syncEl_1.length) {
                if ($syncBar.hasClass('hidden')) {
                    $syncBar.removeClass('hidden');
                }
            }
            if (status_1) {
                syncDisplayData.status = status_1;
                switch (status_1) {
                    case SYNC_STATUS_READY.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_STATUS_READY.text);
                        syncDisplayData.cursor = SYNC_STATUS_READY.cursor;
                        break;
                    }
                    case SYNC_STATUS_AUTHORIZED.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_STATUS_AUTHORIZED.text);
                        break;
                    }
                    case SYNC_DOWNLOADING_META_INFO.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_DOWNLOADING_META_INFO.text);
                        break;
                    }
                    case SYNC_REMOVE_FOLDERS.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_REMOVE_FOLDERS.text);
                        break;
                    }
                    case SYNC_REMOVE_NOTES.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_REMOVE_NOTES.text);
                        break;
                    }
                    case SYNC_REMOVE_ATTACHMENTS.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_REMOVE_ATTACHMENTS.text);
                        break;
                    }
                    case SYNC_UPDATE_CONTENT.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_UPDATE_CONTENT.text);
                        break;
                    }
                    case SYNC_DOWNLOAD_NOTES.code: {
                        var statusText = getTranslation(SYNC_DOWNLOAD_NOTES.text);
                        if (typeof (syncData.current) !== "undefined" && syncData.current) {
                            if (typeof (syncData.total) !== "undefined" && syncData.total) {
                                statusText += (syncData.current + "/" + syncData.total);
                            }
                        }
                        syncDisplayData.statusText = statusText;
                        break;
                    }
                    case SYNC_DOWNLOAD_ATTACHMENT.code: {
                        var statusText = getTranslation(SYNC_DOWNLOAD_ATTACHMENT.text);
                        if (typeof (syncData.current) !== "undefined" && syncData.current) {
                            if (typeof (syncData.total) !== "undefined" && syncData.total) {
                                statusText += (syncData.current + "/" + syncData.total);
                            }
                        }
                        syncDisplayData.statusText = statusText;
                        break;
                    }
                    case SYNC_UPLOAD_FOLDERS.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_UPLOAD_FOLDERS.text);
                        break;
                    }
                    case SYNC_UPLOAD_TAGS.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_UPLOAD_TAGS.text);
                        break;
                    }
                    case SYNC_UPLOAD_NOTES.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_UPLOAD_NOTES.text);
                        break;
                    }
                    case SYNC_UPLOAD_ATTACHMENTS.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_UPLOAD_ATTACHMENTS.text);
                        break;
                    }
                    case SYNC_TRAFFIC_LIMIT.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_TRAFFIC_LIMIT.text);
                        break;
                    }
                    case NEED_SYNC_BEFORE_SHARE.code: {
                        syncDisplayData.statusText = getTranslation(NEED_SYNC_BEFORE_SHARE.text);
                        syncDisplayData.cursor = NEED_SYNC_BEFORE_SHARE.cursor;
                        syncTimerForDone = setTimeout(function () {
                            if ($syncEl_1.length) {
                                var status_2 = $syncEl_1.attr("data-sync-state");
                                if (status_2 === NEED_SYNC_BEFORE_SHARE.code) {
                                }
                            }
                        }, 500);
                        break;
                    }
                    case SYNC_STATUS_CANCELED.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_STATUS_CANCELED.text);
                        syncTimerForDone = setTimeout(function () {
                            if ($syncEl_1.length) {
                                var status_3 = $syncEl_1.attr("data-sync-state");
                                if (status_3 === SYNC_STATUS_CANCELED.code) {
                                }
                            }
                        }, 1000);
                        break;
                    }
                    case SYNC_STATUS_PAUSED.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_STATUS_PAUSED.text);
                        syncTimerForDone = setTimeout(function () {
                            if ($syncEl_1.length) {
                                var status_4 = $syncEl_1.attr("data-sync-state");
                                if (status_4 === SYNC_STATUS_DONE.code) {
                                }
                            }
                        }, 1000);
                        break;
                    }
                    case SYNC_STATUS_DONE.code: {
                        syncDisplayData.statusText = getTranslation(SYNC_STATUS_DONE.text);
                        if ($syncEl_1.length) {
                            var status_5 = $syncEl_1.attr("data-sync-state");
                            if (status_5 === SYNC_STATUS_DONE.code) {
                                elDisplayClientSync({ status: SYNC_STATUS_READY.code });
                            }
                        }
                        break;
                    }
                    default: {
                        syncDisplayData.status = SYNC_STATUS_NOT_AVAILABLE.code;
                        syncDisplayData.statusText = getTranslation(SYNC_STATUS_NOT_AVAILABLE.text);
                    }
                }
            }
            if ($syncEl_1.length) {
                var $statusIcon = $syncEl_1.find('.workspace-sync--action__icon[icon=workspace-sync]');
                var $errorIcon = $syncBar.find(".workspace-sync--action__icon[icon=workspace-sync-problem]");
                if ($statusIcon.length && $errorIcon.length) {
                    var statusText = syncDisplayData.statusText;
                    $syncEl_1.attr("data-sync-state", syncDisplayData.status);
                    $syncEl_1.attr("data-cursor", syncDisplayData.cursor ? syncDisplayData.cursor : 'default');
                    var errorLastSyncStatusList = [
                        SYNC_STATUS_PAUSED.code,
                        SYNC_STATUS_CANCELED.code,
                        SYNC_TRAFFIC_LIMIT.code
                    ];
                    if (errorLastSyncStatusList.indexOf(syncDisplayData.status) >= 0) {
                        if ($errorIcon.hasClass('hidden')) {
                            $errorIcon.removeClass('hidden');
                        }
                    }
                    else {
                        if (!$errorIcon.hasClass('hidden')) {
                            $errorIcon.addClass('hidden');
                        }
                    }
                    var updateLastSyncStatusList = [
                        SYNC_STATUS_DONE.code,
                        SYNC_STATUS_PAUSED.code,
                        SYNC_STATUS_CANCELED.code,
                        SYNC_TRAFFIC_LIMIT.code,
                        SYNC_STATUS_NOT_AVAILABLE.code
                    ];
                    if (updateLastSyncStatusList.indexOf(syncDisplayData.status) >= 0) {
                        var today = new Date();
                        var month = (today.getMonth() + 1);
                        if (month < 10) {
                            month = "0" + month;
                        }
                        var day = (today.getDate());
                        if (day < 10) {
                            day = "0" + day;
                        }
                        var minutes = (today.getMinutes());
                        if (minutes < 10) {
                            minutes = "0" + minutes;
                        }
                        var seconds = (today.getSeconds());
                        if (seconds < 10) {
                            seconds = "0" + seconds;
                        }
                        var date = today.getFullYear() + '-' + month + '-' + day;
                        var time = today.getHours() + ":" + minutes + ":" + seconds;
                        var dateTime = date + " " + time;
                        $syncEl_1.attr('title', getTranslation(LAST_SYNC_TITLE) + dateTime + ' (' + statusText + ')');
                        if ($statusIcon.hasClass('rotating')) {
                            $statusIcon.removeClass('rotating');
                        }
                    }
                    else if (syncDisplayData.status !== SYNC_STATUS_READY.code) {
                        $syncEl_1.attr('title', statusText);
                        if (!$statusIcon.hasClass('rotating')) {
                            $statusIcon.addClass('rotating');
                        }
                    }
                    else {
                        $syncEl_1.attr('title', statusText);
                        if ($statusIcon.hasClass('rotating')) {
                            $statusIcon.removeClass('rotating');
                        }
                    }
                }
            }
        }
    }
    function toggleClientSync(inputData) {
        var workspaceId = inputData.workspaceId, show = inputData.show;
        show = show || false;
        var $syncBar = $('.workspace-sync');
        if ($syncBar.length) {
            var workspaceSelector = '';
            if (typeof (workspaceId) !== 'undefined') {
                workspaceSelector = "[data-workspace-id=" + workspaceId + "]";
                if (!workspaceId) {
                    workspaceSelector = "[data-workspace-is-default=true]";
                }
            }
            var $syncEl_2 = $syncBar.find(".workspace-sync--action" + workspaceSelector + " > [icon=workspace-sync]");
            var $syncErrorEl_1 = $syncBar.find(".workspace-sync--action" + workspaceSelector + " > [icon=workspace-sync-error]");
            if ($syncEl_2.length && $syncErrorEl_1.length) {
                if (show) {
                    elDisplayClientSync({
                        workspaceId: workspaceId,
                        status: SYNC_STATUS_READY.code
                    });
                    $syncErrorEl_1.hide(0, function () {
                        $syncEl_2.show(0);
                    });
                }
                else {
                    $syncEl_2.hide(0, function () {
                        $syncErrorEl_1.show(0);
                    });
                    elDisplayClientSync({
                        workspaceId: workspaceId,
                        status: SYNC_STATUS_NOT_AVAILABLE.code
                    });
                }
            }
        }
    }
    $(function () {
        $(document).off("click", ".attach_single_name a")
            .on("click", ".attach_single_name a", function () {
            var $self = $(this);
            if ($self.attr('attachment-id') && $self.data('href')) {
                window.location = $self.data('href');
            }
        });
        $(document).off("click", ".workspace-sync--action > [icon=workspace-sync]")
            .on("click", ".workspace-sync--action > [icon=workspace-sync]", function (event) {
            event.preventDefault();
            var workspaceId = $(this).closest('.workspace-sync--action').attr('data-workspace-id');
            elClientSyncHandler({ workspaceId: workspaceId, manualSync: true });
        });
    });
    function elSendAutoSyncRequest(inputData) {
        var workspaceId = inputData.workspaceId, timer = inputData.timer;
        timer = timer || 100;
        setTimeout(function () {
            window.elSendIpcRequest('event:client:autosync:request', { workspaceId: workspaceId });
        }, timer);
    }
    function elClientSyncHandler(inputData) {
        var workspaceId = inputData.workspaceId, manualSync = inputData.manualSync;
        var $syncStatus = $(".workspace-sync--action");
        if ($syncStatus.length) {
            var status_6 = $syncStatus.attr("data-sync-state") ? $syncStatus.attr("data-sync-state").trim() : '';
            if (!status_6) {
                status_6 = SYNC_STATUS_READY.code;
                $syncStatus.attr("data-sync-state", status_6);
            }
            var syncAvailableStatuses = [
                SYNC_STATUS_READY.code,
                NEED_SYNC_BEFORE_SHARE.code,
                SYNC_STATUS_PAUSED.code,
                SYNC_STATUS_CANCELED.code,
                SYNC_TRAFFIC_LIMIT.code,
                SYNC_STATUS_DONE.code
            ];
            if (status_6 === SYNC_TRAFFIC_LIMIT.code) {
            }
            if (syncAvailableStatuses.indexOf(status_6) >= 0) {
                return window.elSendIpcRequest('event:sync:start:request', { workspaceId: workspaceId, status: status_6, manualSync: manualSync });
            }
            window.elSendIpcRequest('event:sync:queue:request', { workspaceId: workspaceId, status: status_6 });
        }
    }
    function requestSyncPanelReload() {
        setTimeout(function () {
            elInitContent({
                language: getCustomerLanguage()
            });
        }, 350);
    }
    function requestPrintNote(htmlContents) {
        if (!htmlContents)
            return;
        window.elSendIpcRequest('event:print', { "template": htmlContents });
    }
    function requestPlayVideoTab(htmlContents, attachment) {
        if (!htmlContents)
            return;
        window.elSendIpcRequest('event:play:video:request', { "html": htmlContents, "attach": attachment });
    }
    function requestCloseVideoTab(globalId) {
        if (!globalId)
            return;
        window.elSendIpcRequest('event:close:video:request', { "globalId": globalId });
    }
    function handleSaveNote(note) {
        if (note && !note.isOfflineOnly) {
            elSendAutoSyncRequest({});
        }
    }
    function handleSelectNote(inputData) {
        window.elSendIpcRequest('event:select:note:request', inputData);
    }
    function handleClearSyncData() {
        window.elSendIpcRequest('event:client:clear:sync:data:request');
    }
    function handleStartSingleAttachSync(attachment) {
        if (attachment) {
            window.elSendIpcRequest('event:client:start-single-attach-sync:request', { "globalId": attachment.globalId });
        }
    }
    function handleStopSingleAttachSync(attachment) {
        if (attachment) {
            window.elSendIpcRequest('event:client:stop-single-attach-sync:request', { "globalId": attachment.globalId });
        }
    }
    function handleOpenUrl(url) {
        window.elSendIpcRequest('event:client:open:url:request', { url: url });
    }
    function handleOpenHelp() {
        window.elSendIpcRequest('event:client:open:help:request', {});
    }
    function handleOpenEditorHelp() {
        window.elSendIpcRequest('event:client:open:editor:help:request', {});
    }
    function handleOpenSubmitBug() {
        window.elSendIpcRequest('event:client:open:submit:bug:request', {});
    }
    function handleRemoveAttachments(_a) {
        var workspaceId = _a.workspaceId, noteId = _a.noteId, attachments = _a.attachments;
        window.elSendIpcRequest('event:web:app:remove:attachments:request', { workspaceId: workspaceId, noteId: noteId, attachments: attachments });
    }
    function handleExportNote(data) {
        window.elSendIpcRequest('event:web:app:export:active:note:request', data);
    }
    function handleOpenExportPath(data) {
        window.elSendIpcRequest('event:web:app:export:active:open:request', data);
    }
    function handleCancelExport(data) {
        window.elSendIpcRequest('event:web:app:export:active:cancel:request', data);
    }
    function handleSetActiveWorkspace(workspaceId) {
        window.elSendIpcRequest('event:client:set:active:workspace:request', { workspaceId: workspaceId });
    }
    function handleSetActiveOrganization(org) {
        window.elSendIpcRequest('event:client:set:active:organization:request', { org: org });
    }
    function handlePrintAttachment(attachment) {
        window.elSendIpcRequest('event:client:print:attachment:request', { attachment: attachment });
    }
    function handleCopyTextToClipBoard(text) {
        window.writeTextToClipboard(text);
    }
    function handleEditorUpdateNote(inputData) {
        window.elSendIpcRequest('event:editor:note:update:request', inputData);
    }
    function handleEditorSyncDownload(inputData) {
        window.elSendIpcRequest('event:editor:attachment:sync:download:request', inputData);
    }
    function handleEditorPutDbRequest(data) {
        return new Promise(function (resolve) {
            window.elSendIpcRequest('event:editor:put:db:data:request', data);
            window.elGetOnceIpcRequest('event:editor:put:db:data:response', function (data) {
                return resolve(data);
            });
        });
    }
    function handleEditorPutArrayDbRequest(data) {
        return new Promise(function (resolve) {
            window.elSendIpcRequest('event:editor:put:db:array:data:request', data);
            window.elGetOnceIpcRequest('event:editor:put:db:array:data:response', function (data) {
                return resolve(data);
            });
        });
    }
    function handleEditorGetDbRequest(data) {
        return new Promise(function (resolve) {
            window.elSendIpcRequest('event:editor:get:db:data:request', data);
            window.elGetOnceIpcRequest('event:editor:get:db:data:response', function (data) {
                return resolve(data);
            });
        });
    }
    function handleEditorGetFromRangeDbRequest(data) {
        return new Promise(function (resolve) {
            window.elSendIpcRequest('event:editor:getFromRange:db:data:request', data);
            window.elGetOnceIpcRequest('event:editor:getFromRange:db:data:response', function (data) {
                return resolve(data);
            });
        });
    }
    function handleEditorGetKeysFromRangeRequest(data) {
        return new Promise(function (resolve) {
            window.elSendIpcRequest('event:editor:getKeysFromRange:db:data:request', data);
            window.elGetOnceIpcRequest('event:editor:getKeysFromRange:db:data:response', function (data) {
                return resolve(data);
            });
        });
    }
    function handleEditorDeleteRequest(data) {
        return new Promise(function (resolve) {
            window.elSendIpcRequest('event:editor:delete:db:data:request', data);
            window.elGetOnceIpcRequest('event:editor:delete:db:data:response', function (data) {
                return resolve(data);
            });
        });
    }
    function handleEditorSyncCallbackRequest(data) {
        window.elSendIpcRequest('event:editor:sync-callback:data:request', data);
    }
    function handleEditorSyncTextCallbackRequest(data) {
        window.elSendIpcRequest('event:editor:sync-text-callback:data:request', data);
    }
    function handleEditorOpenAttachment(data) {
        window.elSendIpcRequest('event:editor:open:attach:request', data);
    }
    function handleEditorShowError(data) {
        var message = data.message;
        if (!message) {
            return;
        }
        window.electron.showError(__assign(__assign({}, data), { electron: true }));
    }
    function handleEditorUpdateSyncedStatusRequest(data) {
        if (window.textEditor && window.textEditor.setNoteSynced) {
            window.textEditor.setNoteSynced(data, true);
        }
        if (window.textEditor && window.textEditor.setBgNoteSynced) {
            window.textEditor.setBgNoteSynced(data, true);
        }
    }
    function handleBgNoteSync(data) {
        if (window.electron) {
            if (window.electron.makeBgNoteSync) {
                window.electron.makeBgNoteSync(data);
            }
        }
    }
    function handleEditorUpdateAttachment(data) {
        if (window.electron) {
            if (window.electron.updateEditorAttachment) {
                window.electron.updateEditorAttachment(data);
            }
        }
    }
    function handleEditorNavigateToInternalNote(data) {
        if (window.electron) {
            if (window.electron.processNavigation) {
                window.electron.processNavigation(data);
            }
        }
    }
    function handleEditorUpdateDownloadedAttachment(data) {
        if (window.electron) {
            if (window.electron.updateEditorDownloadedAttach) {
                window.electron.updateEditorDownloadedAttach(data);
            }
        }
    }
    if (!window.textEditor) {
        window.textEditor = {};
    }
    if (!window.textEditor.put) {
        window.textEditor.put = handleEditorPutDbRequest;
    }
    if (!window.textEditor.putArray) {
        window.textEditor.putArray = handleEditorPutArrayDbRequest;
    }
    if (!window.textEditor.get) {
        window.textEditor.get = handleEditorGetDbRequest;
    }
    if (!window.textEditor.getFromRange) {
        window.textEditor.getFromRange = handleEditorGetFromRangeDbRequest;
    }
    if (!window.textEditor.getKeysFromRange) {
        window.textEditor.getKeysFromRange = handleEditorGetKeysFromRangeRequest;
    }
    if (!window.textEditor.delete) {
        window.textEditor.delete = handleEditorDeleteRequest;
    }
    if (!window.textEditor.syncCb) {
        window.textEditor.syncCb = handleEditorSyncCallbackRequest;
    }
    if (!window.textEditor.syncTextCb) {
        window.textEditor.syncTextCb = handleEditorSyncTextCallbackRequest;
    }
    if (!window.textEditor.openAttachment) {
        window.textEditor.openAttachment = handleEditorOpenAttachment;
    }
    if (!window.textEditor.showError) {
        window.textEditor.showError = handleEditorShowError;
    }
    function elDisplaySettings() {
        if (typeof (window.electron) !== "undefined" && window.electron.showSettings) {
            window.electron.showSettings();
        }
    }
    function elTrafficInfo() {
        if (typeof (window.electron) !== "undefined" && window.electron.showTrafficInfo) {
            window.electron.showTrafficInfo();
        }
    }
    function elUpdateInlineAttach(attachData) {
        var $attach = $("img[data-attachment-id=" + attachData.globalId + "][data-note-id=" + attachData.noteGlobalId + "]");
        if ($attach.length) {
            $attach.attr('src', $attach.attr('src') + '&' + new Date().getTime());
        }
    }
    function elDisplayLimitNoticeForAccount(errorData) {
        if (typeof (window.electron) !== "undefined" && window.electron.showLimitNoticeForAccount) {
            window.electron.showLimitNoticeForAccount(errorData);
        }
    }
    function elDisplayRateNoticeForAccount(storeData) {
        if (typeof (window.electron) !== "undefined" && window.electron.showRateNoticeForAccount) {
            window.electron.showRateNoticeForAccount(storeData);
        }
    }
    function elRemoveWorkspaceForAccount(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.removeWorkspaceForAccount) {
            window.electron.removeWorkspaceForAccount(data);
        }
    }
    function elRemoveOrgForAccount(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.removeOrgForAccount) {
            window.electron.removeOrgForAccount(data);
        }
    }
    function elUpdateWorkspaceForAccount(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.updateWorkspaceForAccount) {
            window.electron.updateWorkspaceForAccount(data);
        }
    }
    function elUpdateOrgForAccount(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.updateOrgForAccount) {
            window.electron.updateOrgForAccount(data);
        }
    }
    function elUpdateWorkspaceMembersForAccount(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.updateWorkspaceMembersForAccount) {
            window.electron.updateWorkspaceMembersForAccount(data);
        }
    }
    function elUpdateWorkspaceInvitesForAccount(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.updateWorkspaceInvitesForAccount) {
            window.electron.updateWorkspaceInvitesForAccount(data);
        }
    }
    function elUpdateWorkspacesAccessForAccount(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.updateWorkspacesAccessForAccount) {
            window.electron.updateWorkspacesAccessForAccount(data);
        }
    }
    function elUpdateWorkspacesPremiumForAccount(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.fetchWorkspacePremiumForAccount) {
            window.electron.fetchWorkspacePremiumForAccount(data);
        }
    }
    function elDisplayMessageForAccount(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.displayMessageForAccount) {
            window.electron.displayMessageForAccount(data);
        }
    }
    function elUpdateUserName(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.updateUserNameForAccount) {
            window.electron.updateUserNameForAccount(data);
        }
    }
    function elRemoveUserAvatar(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.removeUserAvatarForAccount) {
            window.electron.removeUserAvatarForAccount(data);
        }
    }
    function elDisplayNotesCountLimitPopup(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.displayNotesCountLimitPopup) {
            window.electron.displayNotesCountLimitPopup(data);
        }
    }
    function elSetExportStatus(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.setExportStatus) {
            window.electron.setExportStatus(data);
        }
    }
    function elSetExportProgress(data) {
        if (typeof (window.electron) !== "undefined" && window.electron.setExportProgress) {
            window.electron.setExportProgress(data);
        }
    }
    function elExportWorkspaceNotes() {
        if (typeof (window.electron) !== "undefined" && window.electron.exportWorkspaceNotes) {
            window.electron.exportWorkspaceNotes();
        }
    }
    function updateNoteTextForAccount(data) {
        var globalId = data.globalId;
        if (!globalId) {
            return;
        }
        var $inlineImg = $("img[data-attachment-id=" + globalId + "]");
        if (!$inlineImg.length) {
            $inlineImg = $(".note-text-editor img[src*='/" + globalId + "/']");
        }
        if (!$inlineImg.length) {
            return;
        }
        $inlineImg.attr('src', $inlineImg.attr('src') + "?t=" + new Date().getTime());
    }
    function elFindNotes() {
        $(".dialog-input--search").focus();
    }
    function elAddNote() {
        $(".button-add-circle").get(0).click();
    }
})(window);
