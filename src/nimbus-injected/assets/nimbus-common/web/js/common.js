"use strict";
(function (window) {
    console.log("common.js script load for communicate with auth/webnotes/update project");
    var $ = window.$;
    var ipcRenderer = window.nodeRequire('electron').ipcRenderer;
    var remote = window.nodeRequire('electron').remote;
    var clipboard = remote.clipboard, Menu = remote.Menu;
    var menu;
    var inAppPurchase;
    if (remote.process.platform === 'darwin') {
        inAppPurchase = remote.inAppPurchase;
    }
    function elSendIpcRequest(key, val) {
        if (val === void 0) { val = null; }
        if (ipcRenderer) {
            ipcRenderer.send(key, val);
        }
    }
    function elGetIpcRequest(key, callback) {
        if (callback === void 0) { callback = function (res) {
        }; }
        if (ipcRenderer)
            ipcRenderer.on(key, function (event, arg) {
                callback(arg);
            });
    }
    function elGetOnceIpcRequest(key, callback) {
        if (callback === void 0) { callback = function (res) {
        }; }
        if (ipcRenderer)
            ipcRenderer.once(key, function (event, arg) {
                callback(arg);
            });
    }
    function elSendNotification(content) {
        var title = content.title, body = content.body;
        body.body = body.subtitle;
        var notification = new window.Notification(title, body);
        if (body.reminderData && notification) {
            notification.addEventListener('click', function () {
                var noteUrl = body.reminderData.noteUrl;
                if (noteUrl) {
                    window.elSendIpcRequest('event:open:note:url:handler', { noteUrl: noteUrl });
                }
            });
        }
    }
    function elLogContent(content) {
        console.log("Log info: ", content);
    }
    function elAppIsOnline() {
        return navigator.onLine;
    }
    if ($) {
        $.fn.serializeObject = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                }
                else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    }
    var getPurchaiseProducts = function (productIdList, callback) {
        callback = callback || function () {
        };
        if (!window.canMakePayments()) {
            return callback([]);
        }
        if (!inAppPurchase) {
            return callback([]);
        }
        inAppPurchase.getProducts(productIdList).then(function (products) {
            if (!Array.isArray(products) || products.length <= 0) {
                console.log('Unable to retrieve the product information.', products);
            }
            window.elSendIpcRequest('event:display:products:save:requests', {
                'products': products
            });
            if (callback) {
                return callback(products);
            }
        });
    };
    var canMakePayments = function () {
        if (!inAppPurchase) {
            return false;
        }
        return inAppPurchase.canMakePayments();
    };
    var writeTextToClipboard = function (text) {
        clipboard.writeText(text);
    };
    var getToastMessageConfig = function (content) {
        var data = content.data, settings = content.settings;
        var toastMessage = {
            text: data.text,
            showHideTransition: 'fade',
            textColor: '#fff',
            allowToastClose: true,
            hideAfter: typeof (settings.hideAfter) !== "undefined" ? settings.hideAfter : 10000,
            stack: 5,
            textAlign: 'left',
            position: typeof (settings.position) !== "undefined" ? settings.position : 'top-right',
            transparent: typeof (settings.transparent) !== "undefined" ? settings.transparent : 0.3
        };
        switch (settings.type) {
            case window.MESSAGE_TYPE_INFO: {
                toastMessage.bgColor = "rgba(0, 0, 255, " + toastMessage.transparent + ")";
                break;
            }
            case window.MESSAGE_TYPE_WARNING: {
                toastMessage.bgColor = "rgba(255, 255, 0, " + toastMessage.transparent + ")";
                break;
            }
            case window.MESSAGE_TYPE_ERROR: {
                toastMessage.bgColor = "rgba(255, 0, 0, " + toastMessage.transparent + ")";
                break;
            }
            case window.MESSAGE_TYPE_SUCCESS: {
                toastMessage.bgColor = "rgba(0, 255, 0, " + toastMessage.transparent + ")";
                break;
            }
            default: {
                toastMessage.bgColor = "rgba(0, 0, 0, " + toastMessage.transparent + ")";
            }
        }
        return toastMessage;
    };
    var reloadPageHandler = function () {
        elSendIpcRequest('event:client:reload:page:request', {});
    };
    var contextMenuHandler = function (event) {
        event.preventDefault();
        var tagNameCorrect = [
            'input',
            'textarea',
        ].indexOf(event.target.tagName.toLowerCase()) >= 0;
        var parentCorrect = event.target.closest([
            ".note-body-panel",
        ].join(','));
        var parentTable = parentCorrect ? event.target.closest('.table-wrapper') : null;
        var parentMenu = parentCorrect ? event.target.closest('.popup-menu') : null;
        var editorBlockCorrect = parentCorrect && !parentTable && !parentMenu;
        if ((tagNameCorrect || editorBlockCorrect) && menu) {
            menu.popup();
        }
    };
    var setAppContextMenu = function (items) {
        if (items === void 0) { items = []; }
    };
    window.elSendIpcRequest = elSendIpcRequest;
    window.elGetIpcRequest = elGetIpcRequest;
    window.elGetOnceIpcRequest = elGetOnceIpcRequest;
    window.elAppIsOnline = elAppIsOnline;
    window.elLogContent = elLogContent;
    window.getPurchaiseProducts = getPurchaiseProducts;
    window.canMakePayments = canMakePayments;
    window.getToastMessageConfig = getToastMessageConfig;
    window.writeTextToClipboard = writeTextToClipboard;
    window.elSendNotification = elSendNotification;
    window.setAppContextMenu = setAppContextMenu;
    window.MESSAGE_TYPE_INFO = 'info';
    window.MESSAGE_TYPE_ERROR = 'error';
    window.MESSAGE_TYPE_WARNING = 'warning';
    window.MESSAGE_TYPE_SUCCESS = 'success';
})(window);
