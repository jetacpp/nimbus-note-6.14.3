"use strict";
(function (window) {
    console.log("custom.js script load for communicate with nimbus-note-auth project");
    function elInitContent() {
        elChangeOnlineStatusHandlers();
    }
    function elChangeOnlineStatusHandlers() {
        elProcessChangeOnlineStatus();
        window.removeEventListener('online', elProcessChangeOnlineStatus);
        window.addEventListener('online', elProcessChangeOnlineStatus);
        window.removeEventListener('offline', elProcessChangeOnlineStatus);
        window.addEventListener('offline', elProcessChangeOnlineStatus);
    }
    function elProcessChangeOnlineStatus() {
        var onlineStatus = window.elAppIsOnline();
        window.elSendIpcRequest('event:online:changed', { "online": onlineStatus });
    }
    window.elGetIpcRequest('event:logout:request', function () {
        elProcessClientLogout();
    });
    function elProcessClientLogout() {
        window.elSendIpcRequest('event:logout:response', { "logout": true });
    }
    elInitContent();
})(window);
