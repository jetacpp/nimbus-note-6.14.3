"use strict";
(function (window) {
    console.log("custom.js script load for communicate with nimbus-note-update project");
    var $ = window.$;
    var lang = 'en';
    var trans = {};
    trans["update_available__title"] = {
        'en': "Found Updates",
        'ru': "\u041D\u0430\u0439\u0434\u0435\u043D\u043E \u043E\u0431\u043D\u043E\u0432\u043B\u0435\u043D\u0438\u0435",
        'de': "Gefundene Updates"
    };
    trans["update_available__message"] = {
        'en': "Found updates, do you want update now?",
        'ru': " \u0414\u043E\u0441\u0442\u0443\u043F\u043D\u043E \u043E\u0431\u043D\u043E\u0432\u043B\u0435\u043D\u0438\u0435, \u0445\u043E\u0442\u0438\u0442\u0435 \u043E\u0431\u043D\u043E\u0432\u0438\u0442\u044C \u0441\u0435\u0439\u0447\u0430\u0441?",
        'de': "Gefundene Updates, m\u00F6chtest du jetzt ein Update?"
    };
    trans["update_available__progress"] = {
        'en': "Download progress",
        'ru': "\u041F\u0440\u043E\u0433\u0440\u0435\u0441\u0441 \u0437\u0430\u0433\u0440\u0443\u0437\u043A\u0438",
        'de': "Voortgang downloaden"
    };
    trans["update_available__yes"] = {
        'en': "Install new version",
        'ru': "\u0423\u0441\u0442\u0430\u043D\u043E\u0432\u0438\u0442\u044C \u043D\u043E\u0432\u0443\u044E \u0432\u0435\u0440\u0441\u0438\u044E",
        'de': "Installeer een nieuwe versie"
    };
    trans["update_available__no"] = {
        'en': "No",
        'ru': "\u041D\u0435\u0442",
        'de': "Nein"
    };
    trans["update_available__later"] = {
        'en': "Remind later",
        'ru': "\u041D\u0430\u043F\u043E\u043C\u043D\u0438\u0442\u044C \u043F\u043E\u0437\u0436\u0435",
        'de': "Herinner later"
    };
    trans["update_available__hide"] = {
        'en': "Hide",
        'ru': "\u0421\u043A\u0440\u044B\u0442\u044C",
        'de': "Verbergen"
    };
    trans["update_available__skip_version"] = {
        'en': "Skip new version",
        'ru': "\u041F\u0440\u043E\u043F\u0443\u0441\u0442\u0438\u0442\u044C \u043D\u043E\u0432\u0443\u044E \u0432\u0435\u0440\u0441\u0438\u044E",
        'de': "Sla nieuwe versie over"
    };
    trans["update_in_progress"] = {
        'en': "Download in-progress...",
        'ru': "\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430 \u043F\u0440\u0438\u043B\u043E\u0436\u0435\u043D\u0438\u044F...",
        'de': "Download l\u00E4uft..."
    };
    trans["update_in_progress__message"] = {
        'en': "Downloading new application version ",
        'ru': "\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430 \u043D\u043E\u0432\u043E\u0439 \u0432\u0435\u0440\u0441\u0438\u0438 \u043F\u0440\u0438\u043B\u043E\u0436\u0435\u043D\u0438\u044F ",
        'de': "Nieuwe toepassingsversie downloaden "
    };
    trans["update_description__message"] = {
        'en': "Available application version ",
        'ru': "\u0414\u043E\u0441\u0442\u0443\u043F\u043D\u0430 \u0432\u0435\u0440\u0441\u0438\u044F \u043F\u0440\u0438\u043B\u043E\u0436\u0435\u043D\u0438\u044F ",
        'de': "Beschikbare applicatieversie "
    };
    var getTranslationByKey = function (transKey) {
        if (typeof (trans[transKey]) === "undefined") {
            return transKey;
        }
        if (typeof (trans[transKey][lang]) === "undefined") {
            return transKey;
        }
        return trans[transKey][lang] ? trans[transKey][lang] : transKey;
    };
    window.elGetIpcRequest('event:initApp:request', function (content) {
        lang = content.language;
        $("[trans]").each(function () {
            var $self = $(this);
            var transKey = $self.attr("trans");
            $self.html(getTranslationByKey(transKey));
        });
    });
    window.elGetIpcRequest('event:latest:update:version:response', function (content) {
        var version = content ? content.version : '';
        $('.update_available_version').text(version);
    });
    window.elGetIpcRequest('event:latest:update:progress:response', function (content) {
        var progress = content ? content.progress : '';
        $('#update_available__progress').text(progress);
    });
    var closeWindowClickHandler = function () {
        window.elSendIpcRequest('event:close:update:window:requests', {});
    };
    var updateWindowClickHandler = function () {
        var className = "#nimbus_update__title_found, #nimbus_update__description, #nimbus_update__description, \n                       .nimbus_update__button_later, .nimbus_update__button_confirm";
        $(className).hide(0, function () {
            var $buttons = $('.nimbus_update__buttons');
            if (!$buttons.hasClass('nimbus_update__buttons_center')) {
                $buttons.addClass('nimbus_update__buttons_center');
            }
            $('#nimbus_update__title_download, #nimbus_update__progress, .nimbus_update__button_hide').show(0);
        });
        window.elSendIpcRequest('event:make:update:window:requests', {});
    };
    var listenUpdateNoClick = function () {
        var events = 'click';
        var closePopupClass = '#nimbus_update__buttons__hide, #nimbus_update__buttons__no';
        $(document).off(events, closePopupClass, closeWindowClickHandler)
            .on(events, closePopupClass, closeWindowClickHandler);
    };
    var listenUpdateInstallClick = function () {
        var events = 'click';
        var closePopupClass = '#nimbus_update__buttons__yes';
        $(document).off(events, closePopupClass, updateWindowClickHandler)
            .on(events, closePopupClass, updateWindowClickHandler);
    };
    listenUpdateNoClick();
    listenUpdateInstallClick();
    window.elSendIpcRequest('event:latest:update:version:requests', {});
    $('#nimbus_update__buttons__yes').focus();
})(window);
module.exports = 0;
