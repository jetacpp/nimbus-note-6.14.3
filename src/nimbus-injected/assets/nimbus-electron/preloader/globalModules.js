"use strict";
console.log("Init Injected global");
var loadNimbusGlobalModules = function () {
    if (window) {
        if (!window.$) {
            window.nodeRequire = require;
            delete window.require;
            window.$ = window.jQuery = window.nodeRequire('jquery');
        }
        if (!window.EventEmitter) {
            window.EventEmitter = window.nodeRequire('wolfy87-eventemitter');
        }
        if (!window.async) {
            window.async = window.nodeRequire('async');
        }
    }
};
document.addEventListener("DOMNodeInserted", loadNimbusGlobalModules);
window.nodeRequire = window.nodeRequire || require;
var ipcRenderer = window.nodeRequire('electron').ipcRenderer;
ipcRenderer.on('nimbus:electron:log', function (event, data) {
    console.log(data);
});
if (process.env.NODE_ENV === "development") {
    window.__devtron = { require: require, process: process };
    var devtron = window.nodeRequire('devtron');
    if (devtron) {
        devtron.install();
    }
}
