"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = __importDefault(require("path"));
var fs_extra_1 = __importDefault(require("fs-extra"));
var electron_1 = require("electron");
var electron_util_1 = __importDefault(require("electron-util"));
var IS_DEV_MODE = require('electron-is-dev');
var SHOW_WEB_CONSOLE = IS_DEV_MODE;
process.env.NODE_ENV = IS_DEV_MODE ? 'development' : 'production';
var APP_NAME = 'Nimbus Note';
var DEFAULT_CHARSET = 'utf8';
var APPSTORE_APP_ID = '1431085284';
var APPBUNDLE_ID = 'co.nimbusweb.nimbusnoteapp';
var SENTRY_KEY = 'https://7e9d53f6899d4c4288fb48cafb66d5e9:4a8ba3b9a9d54d249b63808ceb012a31@sentry.nimbusweb.co/9';
var APP_DIR = path_1.default.resolve(__dirname + "/..");
var ASAR_APP_DIR = electron_util_1.default.fixPathForAsarUnpack(APP_DIR);
var SRC_DIR = APP_DIR + "/src";
var PAGE_AUTH_NAME = 'auth';
var PAGE_NOTE_NAME = 'note';
var PURCHASE_PRODUCTS = ['mac.nimbusnote.subs.monthly', 'mac.nimbusnote.subs.yearly'];
var SYNC_URL_PROD = 'sync.everhelper.me';
var SYNC_URL_DEV = 'sync.develop.nimbustest.com';
var TRIAL_URL_PROD = 'https://trial-api.everhelper.me/v1';
var TRIAL_URL_DEV = 'http://trial-server.develop.nimbustest.com/v1';
var AUTH_URL_PROD = 'nimbusweb.me';
var AUTH_URL_DEV = 'develop.nimbustest.com';
var envFilePath = './config.env.js';
var envFileAbsolutePath = path_1.default.join(__dirname, envFilePath);
var envConfig;
try {
    if (fs_extra_1.default.existsSync(envFileAbsolutePath)) {
        envConfig = require(envFilePath);
    }
}
catch (err) {
    console.error(err);
}
var USE_DEV_SYNC = !(envConfig && envConfig.env && envConfig.env === 'prod');
var syncApiServiceDomain = USE_DEV_SYNC ? SYNC_URL_DEV : SYNC_URL_PROD;
var authApiServiceDomain = USE_DEV_SYNC ? AUTH_URL_DEV : AUTH_URL_PROD;
var trialApiService = USE_DEV_SYNC ? TRIAL_URL_DEV : TRIAL_URL_PROD;
var authApiServicePort = USE_DEV_SYNC ? 80 : 443;
var API_AUTH_LOGIN_PATH = USE_DEV_SYNC ? '/auth_form/api/auth' : '/auth/api/auth';
var API_AUTH_REGISTER_PATH = USE_DEV_SYNC ? '/auth_form/api/register' : '/auth/api/register';
var API_AUTH_CHALLENGE_PATH = USE_DEV_SYNC ? '/auth_form/api/challenge' : '/auth/api/challenge';
var API_AUTH_REMIND_PATH = USE_DEV_SYNC ? '/auth_form/api/remind' : '/auth/api/remind';
var DOWNLOAD_NOTE_COUNT_PER_STEP = 100;
var logsReportName = 'data6aa6-0c32-a83a-afe9-712daf073565';
var nimbusPaymentExternalUrl = 'nimbus.everhelper.me/pricing.php';
var nimbusPaymentUrlPath = '/custom/templates/payment.html';
var nimbusMessageUrlPath = '/custom/templates/message.html';
var nimbusUpdateUrlPath = '/custom/templates/update.html';
var NIMBUS_AUTH_PROJ = 'nimbus/auth';
var NIMBUS_ANGULAR_PROJ = 'nimbus/webnotes';
var NIMBUS_INJECTED_DIR_NAME = 'nimbus-injected';
var NIMBUS_ANGULAR_WEB_DIR_NAME = 'web';
var NIMBUS_CUSTOM_DIR_NAME = 'custom';
var NIMBUS_ANGULAR_TEMPLATE_STATIC_DIR_NAME = 'static';
var NIMBUS_ANGULAR_TEMPLATE_DIR_NAME = 'templates';
var NIMBUS_AUTH_TEMPLATE_DIR_NAME = 'auth';
var APP_WINDOW_DEFAULT_WIDTH = 1200;
var APP_WINDOW_DEFAULT_HEIGHT = 890;
var SETTINGS_APP_WINDOW_STATE_NAME = 'nimbus-note-window-state';
var DATABASE_SUFFIX = USE_DEV_SYNC ? 'dev' : '';
var DATABASE_CLIENT_VERSION = "3" + DATABASE_SUFFIX;
var DATABASE_DATA_VERSION = "3" + DATABASE_SUFFIX;
var DB_USER_MODEL_NAME = "nimbusNoteDbUsers" + DATABASE_CLIENT_VERSION;
var DB_SETTINGS_MODEL_NAME = "nimbusNoteDbSettings" + DATABASE_CLIENT_VERSION;
var DB_TODO_MODEL_NAME = "nimbusNoteDbTodo" + DATABASE_DATA_VERSION;
var DB_ITEM_MODEL_NAME = "nimbusNoteDbItems" + DATABASE_DATA_VERSION;
var DB_TEXT_MODEL_NAME = "nimbusNoteDbTexts" + DATABASE_DATA_VERSION;
var DB_TEXT_EDITOR_MODEL_NAME = "nimbusNoteDbTextEditor" + DATABASE_DATA_VERSION;
var DB_TAG_MODEL_NAME = "nimbusNoteDbTags" + DATABASE_DATA_VERSION;
var DB_NOTE_TAGS_MODEL_NAME = "nimbusNoteDbNoteTags" + DATABASE_DATA_VERSION;
var DB_ATTACH_MODEL_NAME = "nimbusNoteDbAttaches" + DATABASE_DATA_VERSION;
var DB_WORKSPACE_MODEL_NAME = "nimbusNoteDbWorkspaces" + DATABASE_DATA_VERSION;
var DB_WORKSPACE_MEMBERS_MODEL_NAME = "nimbusNoteDbWorkspaceMembers" + DATABASE_DATA_VERSION;
var DB_WORKSPACE_INVITES_MODEL_NAME = "nimbusNoteDbWorkspaceInvites" + DATABASE_DATA_VERSION;
var DB_ORG_MODEL_NAME = "nimbusNoteDbOrgs" + DATABASE_DATA_VERSION;
var DB_ORG_MEMBERS_MODEL_NAME = "nimbusNoteDbOrgsMembers" + DATABASE_DATA_VERSION;
var DB_USER_SETTINGS_MODEL_NAME = "nimbusNoteDbUserSettings" + DATABASE_DATA_VERSION;
var DEFAULT_WORKSPACE_ID = 'default';
var CLIENT_AUTO_LOGIN = true;
var SETTINGS_APP_AUTH_NAME = 'nimbus-note-user-auth';
var LOCAL_SERVER_HOST = 'localhost';
var LOCAL_URL_PROTOCOL = 'nimbus-note://';
var LOCAL_SERVER_PORT = 9921;
var LOCAL_SERVER_HTTP = "http://" + LOCAL_SERVER_HOST + ":" + LOCAL_SERVER_PORT;
var LOCAL_SERVER_HTTP_HOST = "http://" + LOCAL_SERVER_HOST + ":" + LOCAL_SERVER_PORT;
var LOCAL_SERVER_HTTPS_HOST = "https://" + LOCAL_SERVER_HOST + ":" + LOCAL_SERVER_PORT;
var WEBNOTES_DOMAIN = USE_DEV_SYNC ? 'develop.nimbustest.com' : 'nimbusweb.me';
var WEBNOTES_PROTOCOL = USE_DEV_SYNC ? 'http://' : 'https://';
var WEBNOTES_BASE_URL = "" + WEBNOTES_PROTOCOL + WEBNOTES_DOMAIN;
var nimbusAttachmentUrl = LOCAL_SERVER_HTTP_HOST + "/attachment";
var nimbusPaymentUrl = LOCAL_SERVER_HTTP_HOST + "/" + NIMBUS_ANGULAR_PROJ + nimbusPaymentUrlPath;
var nimbusMessageUrl = LOCAL_SERVER_HTTP_HOST + "/" + NIMBUS_ANGULAR_PROJ + nimbusMessageUrlPath;
var nimbusUpdateUrl = LOCAL_SERVER_HTTP_HOST + "/" + NIMBUS_ANGULAR_PROJ + nimbusUpdateUrlPath;
var nimbusModulesPath = APP_DIR + "/node_modules";
var nimbusBuildAssetsPath = APP_DIR + "/build";
var platformIconPath = nimbusBuildAssetsPath + "/" + (process.platform === 'darwin' ? 'icon.png' : 'icon.ico');
var applicationIconPath = nimbusBuildAssetsPath + "/" + (process.platform === 'darwin' ? 'icon.png' : 'icon.window.png');
var trayIconPath = nimbusBuildAssetsPath + "/" + 'icon.tray.png';
var aboutIconPath = platformIconPath;
var nimbusTemplatesPath = SRC_DIR + "/" + NIMBUS_INJECTED_DIR_NAME + "/template";
var nimbusAngularTemplatesPath = nimbusTemplatesPath + "/webnotes/web";
var nimbusCommonAssetsPath = SRC_DIR + "/" + NIMBUS_INJECTED_DIR_NAME + "/assets/nimbus-common/web";
var nimbusAngularAssetsPath = SRC_DIR + "/" + NIMBUS_INJECTED_DIR_NAME + "/assets/webnotes/web";
var nimbusAuthAssetsPath = SRC_DIR + "/" + NIMBUS_INJECTED_DIR_NAME + "/assets/auth/web";
var nimbusPaymentAssetsPath = SRC_DIR + "/" + NIMBUS_INJECTED_DIR_NAME + "/assets/nimbus-payment/web";
var nimbusUpdateAssetsPath = SRC_DIR + "/" + NIMBUS_INJECTED_DIR_NAME + "/assets/nimbus-update/web";
var nimbusPreloadAssetsFile = SRC_DIR + "/" + NIMBUS_INJECTED_DIR_NAME + "/assets/nimbus-electron/preloader/globalModules.js";
var nimbusAuthIndexFile = "/" + NIMBUS_AUTH_PROJ + "/" + NIMBUS_ANGULAR_WEB_DIR_NAME + "/auth/auth.html";
var nimbusAngularIndexFile = "/" + NIMBUS_ANGULAR_PROJ + "/" + NIMBUS_ANGULAR_WEB_DIR_NAME + "/templates/index.html";
var nimbusAngularFolderPath = APP_DIR + "/" + NIMBUS_ANGULAR_PROJ + "/" + NIMBUS_ANGULAR_WEB_DIR_NAME;
var nimbusAuthFolderPath = APP_DIR + "/" + NIMBUS_AUTH_PROJ + "/" + NIMBUS_ANGULAR_WEB_DIR_NAME;
var nimbusAngularWebFolderPath = APP_DIR + "/" + NIMBUS_ANGULAR_PROJ + "/" + NIMBUS_ANGULAR_WEB_DIR_NAME;
var nimbusAuthWebFolderPath = APP_DIR + "/" + NIMBUS_AUTH_PROJ + "/" + NIMBUS_ANGULAR_WEB_DIR_NAME;
var nimbusCustomFolderPath = APP_DIR + "/" + NIMBUS_ANGULAR_PROJ + "/" + NIMBUS_CUSTOM_DIR_NAME;
var nimbusAngularStaticFolderPath = nimbusAngularWebFolderPath + "/" + NIMBUS_ANGULAR_TEMPLATE_STATIC_DIR_NAME;
var nimbusCustomStaticFolderPath = nimbusCustomFolderPath + "/" + NIMBUS_ANGULAR_TEMPLATE_STATIC_DIR_NAME;
var nimbusAuthTemplatesFolderPath = nimbusAuthWebFolderPath + "/" + NIMBUS_AUTH_TEMPLATE_DIR_NAME;
var nimbusAngularTemplatesFolderPath = nimbusAngularWebFolderPath + "/" + NIMBUS_ANGULAR_TEMPLATE_DIR_NAME;
var nimbusCustomTemplatesFolderPath = nimbusCustomFolderPath + "/" + NIMBUS_ANGULAR_TEMPLATE_DIR_NAME;
var APP_VERSION = electron_1.app.getVersion();
var WEBNOTES_VERSION = fs_extra_1.default.existsSync(nimbusAngularWebFolderPath + "/package.json") ? require('../nimbus/webnotes/web/package.json').version : '';
var clientDataBasePath = '';
var clientAttachmentBasePath = '';
var clientAttachmentBaseUrl = '';
var clientDataPath = '';
var dataBasePath = '';
var nimbusAttachmentPath = '';
var everhelperAttachmentBoxUrl = 'https://box.everhelper.me';
var nimbusAttachmentBoxUrl = 'https://nimbusweb.me/box';
var NOTE_PREVIEW_SCALE_SIZE = 90;
var CLIENT_ID_GOOGLE = '922895159049-h7lpm45sedlhibpoa78hj0u0j7pksenc.apps.googleusercontent.com';
var CLIENT_SECRET_GOOGLE = '5y0f-dbDrD6eGZw35bU6uH4b';
var CLIENT_URL_GOOGLE = LOCAL_SERVER_HTTP_HOST + "/ouath/success/google";
var CLIENT_ID_FACEBOOK = '906231639828983';
var CLIENT_SECRET_FACEBOOK = 'ceeca50b6d7c66b6825e15358ebce25b';
var CLIENT_URL_FACEBOOK = LOCAL_SERVER_HTTP_HOST + "/ouath/success/facebook";
var CLIENT_NAME = process.platform === 'win32' ? 'win_notes' : 'mac_notes';
exports.default = {
    APP_NAME: APP_NAME,
    DEFAULT_CHARSET: DEFAULT_CHARSET,
    APPSTORE_APP_ID: APPSTORE_APP_ID,
    APPBUNDLE_ID: APPBUNDLE_ID,
    SENTRY_KEY: SENTRY_KEY,
    PAGE_AUTH_NAME: PAGE_AUTH_NAME,
    PAGE_NOTE_NAME: PAGE_NOTE_NAME,
    IS_DEV_MODE: IS_DEV_MODE,
    USE_DEV_SYNC: USE_DEV_SYNC,
    SYNC_URL_PROD: SYNC_URL_PROD,
    SYNC_URL_DEV: SYNC_URL_DEV,
    API_AUTH_LOGIN_PATH: API_AUTH_LOGIN_PATH,
    API_AUTH_REGISTER_PATH: API_AUTH_REGISTER_PATH,
    API_AUTH_CHALLENGE_PATH: API_AUTH_CHALLENGE_PATH,
    API_AUTH_REMIND_PATH: API_AUTH_REMIND_PATH,
    SHOW_WEB_CONSOLE: SHOW_WEB_CONSOLE,
    PURCHASE_PRODUCTS: PURCHASE_PRODUCTS,
    DOWNLOAD_NOTE_COUNT_PER_STEP: DOWNLOAD_NOTE_COUNT_PER_STEP,
    syncApiServiceDomain: syncApiServiceDomain,
    authApiServiceDomain: authApiServiceDomain,
    authApiServicePort: authApiServicePort,
    trialApiService: trialApiService,
    logsReportName: logsReportName,
    nimbusPaymentExternalUrl: nimbusPaymentExternalUrl,
    nimbusPaymentUrlPath: nimbusPaymentUrlPath,
    nimbusMessageUrlPath: nimbusMessageUrlPath,
    nimbusUpdateUrlPath: nimbusUpdateUrlPath,
    NIMBUS_AUTH_PROJ: NIMBUS_AUTH_PROJ,
    NIMBUS_ANGULAR_PROJ: NIMBUS_ANGULAR_PROJ,
    NIMBUS_INJECTED_DIR_NAME: NIMBUS_INJECTED_DIR_NAME,
    NIMBUS_ANGULAR_WEB_DIR_NAME: NIMBUS_ANGULAR_WEB_DIR_NAME,
    NIMBUS_CUSTOM_DIR_NAME: NIMBUS_CUSTOM_DIR_NAME,
    NIMBUS_ANGULAR_TEMPLATE_STATIC_DIR_NAME: NIMBUS_ANGULAR_TEMPLATE_STATIC_DIR_NAME,
    NIMBUS_ANGULAR_TEMPLATE_DIR_NAME: NIMBUS_ANGULAR_TEMPLATE_DIR_NAME,
    NIMBUS_AUTH_TEMPLATE_DIR_NAME: NIMBUS_AUTH_TEMPLATE_DIR_NAME,
    APP_WINDOW_DEFAULT_WIDTH: APP_WINDOW_DEFAULT_WIDTH,
    APP_WINDOW_DEFAULT_HEIGHT: APP_WINDOW_DEFAULT_HEIGHT,
    SETTINGS_APP_WINDOW_STATE_NAME: SETTINGS_APP_WINDOW_STATE_NAME,
    DATABASE_SUFFIX: DATABASE_SUFFIX,
    DEFAULT_WORKSPACE_ID: DEFAULT_WORKSPACE_ID,
    DATABASE_CLIENT_VERSION: DATABASE_CLIENT_VERSION,
    DATABASE_DATA_VERSION: DATABASE_DATA_VERSION,
    DB_USER_MODEL_NAME: DB_USER_MODEL_NAME,
    DB_SETTINGS_MODEL_NAME: DB_SETTINGS_MODEL_NAME,
    DB_TODO_MODEL_NAME: DB_TODO_MODEL_NAME,
    DB_ITEM_MODEL_NAME: DB_ITEM_MODEL_NAME,
    DB_TEXT_MODEL_NAME: DB_TEXT_MODEL_NAME,
    DB_TEXT_EDITOR_MODEL_NAME: DB_TEXT_EDITOR_MODEL_NAME,
    DB_TAG_MODEL_NAME: DB_TAG_MODEL_NAME,
    DB_NOTE_TAGS_MODEL_NAME: DB_NOTE_TAGS_MODEL_NAME,
    DB_ATTACH_MODEL_NAME: DB_ATTACH_MODEL_NAME,
    DB_WORKSPACE_MODEL_NAME: DB_WORKSPACE_MODEL_NAME,
    DB_WORKSPACE_MEMBERS_MODEL_NAME: DB_WORKSPACE_MEMBERS_MODEL_NAME,
    DB_WORKSPACE_INVITES_MODEL_NAME: DB_WORKSPACE_INVITES_MODEL_NAME,
    DB_ORG_MODEL_NAME: DB_ORG_MODEL_NAME,
    DB_ORG_MEMBERS_MODEL_NAME: DB_ORG_MEMBERS_MODEL_NAME,
    DB_USER_SETTINGS_MODEL_NAME: DB_USER_SETTINGS_MODEL_NAME,
    CLIENT_AUTO_LOGIN: CLIENT_AUTO_LOGIN,
    SETTINGS_APP_AUTH_NAME: SETTINGS_APP_AUTH_NAME,
    LOCAL_SERVER_PORT: LOCAL_SERVER_PORT,
    LOCAL_SERVER_HOST: LOCAL_SERVER_HOST,
    LOCAL_URL_PROTOCOL: LOCAL_URL_PROTOCOL,
    LOCAL_SERVER_HTTP_HOST: LOCAL_SERVER_HTTP_HOST,
    LOCAL_SERVER_HTTPS_HOST: LOCAL_SERVER_HTTPS_HOST,
    WEBNOTES_DOMAIN: WEBNOTES_DOMAIN,
    WEBNOTES_PROTOCOL: WEBNOTES_PROTOCOL,
    WEBNOTES_BASE_URL: WEBNOTES_BASE_URL,
    nimbusAttachmentUrl: nimbusAttachmentUrl,
    nimbusPaymentUrl: nimbusPaymentUrl,
    nimbusMessageUrl: nimbusMessageUrl,
    nimbusUpdateUrl: nimbusUpdateUrl,
    APP_DIR: APP_DIR,
    ASAR_APP_DIR: ASAR_APP_DIR,
    SRC_DIR: SRC_DIR,
    nimbusModulesPath: nimbusModulesPath,
    nimbusBuildAssetsPath: nimbusBuildAssetsPath,
    applicationIconPath: applicationIconPath,
    trayIconPath: trayIconPath,
    aboutIconPath: aboutIconPath,
    nimbusCommonAssetsPath: nimbusCommonAssetsPath,
    nimbusTemplatesPath: nimbusTemplatesPath,
    nimbusAngularTemplatesPath: nimbusAngularTemplatesPath,
    nimbusAngularAssetsPath: nimbusAngularAssetsPath,
    nimbusAuthAssetsPath: nimbusAuthAssetsPath,
    nimbusPaymentAssetsPath: nimbusPaymentAssetsPath,
    nimbusUpdateAssetsPath: nimbusUpdateAssetsPath,
    nimbusPreloadAssetsFile: nimbusPreloadAssetsFile,
    nimbusAuthIndexFile: nimbusAuthIndexFile,
    nimbusAngularIndexFile: nimbusAngularIndexFile,
    nimbusAngularFolderPath: nimbusAngularFolderPath,
    nimbusAuthFolderPath: nimbusAuthFolderPath,
    nimbusAngularWebFolderPath: nimbusAngularWebFolderPath,
    nimbusAuthWebFolderPath: nimbusAuthWebFolderPath,
    nimbusCustomFolderPath: nimbusCustomFolderPath,
    nimbusAngularStaticFolderPath: nimbusAngularStaticFolderPath,
    nimbusCustomStaticFolderPath: nimbusCustomStaticFolderPath,
    nimbusAuthTemplatesFolderPath: nimbusAuthTemplatesFolderPath,
    nimbusAngularTemplatesFolderPath: nimbusAngularTemplatesFolderPath,
    nimbusCustomTemplatesFolderPath: nimbusCustomTemplatesFolderPath,
    APP_VERSION: APP_VERSION,
    WEBNOTES_VERSION: WEBNOTES_VERSION,
    clientDataBasePath: clientDataBasePath,
    clientAttachmentBasePath: clientAttachmentBasePath,
    clientAttachmentBaseUrl: clientAttachmentBaseUrl,
    clientDataPath: clientDataPath,
    dataBasePath: dataBasePath,
    nimbusAttachmentPath: nimbusAttachmentPath,
    everhelperAttachmentBoxUrl: everhelperAttachmentBoxUrl,
    nimbusAttachmentBoxUrl: nimbusAttachmentBoxUrl,
    NOTE_PREVIEW_SCALE_SIZE: NOTE_PREVIEW_SCALE_SIZE,
    CLIENT_ID_GOOGLE: CLIENT_ID_GOOGLE,
    CLIENT_SECRET_GOOGLE: CLIENT_SECRET_GOOGLE,
    CLIENT_URL_GOOGLE: CLIENT_URL_GOOGLE,
    CLIENT_ID_FACEBOOK: CLIENT_ID_FACEBOOK,
    CLIENT_SECRET_FACEBOOK: CLIENT_SECRET_FACEBOOK,
    CLIENT_URL_FACEBOOK: CLIENT_URL_FACEBOOK,
    CLIENT_NAME: CLIENT_NAME,
};
