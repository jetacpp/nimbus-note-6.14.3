"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_extra_1 = __importDefault(require("fs-extra"));
var path_1 = __importDefault(require("path"));
var electron_pdf_window_1 = __importDefault(require("electron-pdf-window"));
var Sentry = __importStar(require("@sentry/electron"));
var electron_1 = require("electron");
var config_runtime_1 = __importDefault(require("./config.runtime"));
var errorHandler_1 = __importDefault(require("./nimbus-electron/utilities/errorHandler"));
var instance_1 = __importDefault(require("./nimbus-electron/window/instance"));
var selectedItem_1 = __importDefault(require("./nimbus-electron/window/selectedItem"));
var LocalCache_1 = __importDefault(require("./nimbus-electron/sync/cache/LocalCache"));
var attachmentHandler_1 = __importDefault(require("./nimbus-electron/utilities/attachmentHandler"));
var Translations_1 = __importDefault(require("./nimbus-electron/translations/Translations"));
var pdb_1 = __importDefault(require("./pdb"));
var server_1 = __importDefault(require("./nimbus-electron/sync/socket/server"));
var state_1 = __importDefault(require("./nimbus-electron/window/state"));
var menu_1 = __importDefault(require("./nimbus-electron/menu/menu"));
var state_2 = __importDefault(require("./nimbus-electron/online/state"));
var print_1 = __importDefault(require("./nimbus-electron/print/print"));
var child_1 = __importDefault(require("./nimbus-electron/window/child"));
var products_1 = __importDefault(require("./nimbus-electron/online/products"));
var App_1 = __importDefault(require("./nimbus-electron/sync/process/application/App"));
var NimbusSDK_1 = __importDefault(require("./nimbus-electron/sync/nimbussdk/net/NimbusSDK"));
var page_1 = __importDefault(require("./nimbus-electron/request/page"));
var SyncManager_1 = __importDefault(require("./nimbus-electron/sync/process/SyncManager"));
var AutoSyncManager_1 = __importDefault(require("./nimbus-electron/sync/nimbussdk/manager/AutoSyncManager"));
var AttachmentSingleDownloader_1 = __importDefault(require("./nimbus-electron/sync/downlaoder/AttachmentSingleDownloader"));
var urlParser_1 = __importDefault(require("./nimbus-electron/utilities/urlParser"));
var workspace_1 = __importDefault(require("./nimbus-electron/db/models/workspace"));
var attach_1 = __importDefault(require("./nimbus-electron/db/models/attach"));
var settings_1 = __importDefault(require("./nimbus-electron/db/models/settings"));
var orgs_1 = __importDefault(require("./nimbus-electron/db/models/orgs"));
var apiAttachExist_1 = __importDefault(require("./nimbus-electron/request/interceptors/jsonInterceptors/attach/apiAttachExist"));
var login_1 = __importDefault(require("./nimbus-electron/request/auth/login"));
var logout_1 = __importDefault(require("./nimbus-electron/request/auth/logout"));
var updateUserUsage_1 = __importDefault(require("./nimbus-electron/sync/process/rx/handlers/updateUserUsage"));
var PopupRateApp_1 = __importDefault(require("./nimbus-electron/popup/PopupRateApp"));
var SelectedWorkspace_1 = __importDefault(require("./nimbus-electron/workspace/SelectedWorkspace"));
var TextEditor_1 = __importDefault(require("./nimbus-electron/db/TextEditor"));
var autoUpdater_1 = require("./nimbus-electron/utilities/autoUpdater");
var helpHandler_1 = require("./nimbus-electron/utilities/helpHandler");
var SelectedOrganization_1 = __importDefault(require("./nimbus-electron/organization/SelectedOrganization"));
var navigationHandler_1 = require("./nimbus-electron/utilities/navigationHandler");
var google_1 = __importDefault(require("./nimbus-electron/request/auth/oauth/google"));
var facebook_1 = __importDefault(require("./nimbus-electron/request/auth/oauth/facebook"));
var authHandler_1 = require("./nimbus-electron/utilities/authHandler");
var exportHandler_1 = require("./nimbus-electron/utilities/exportHandler");
var forceQuit = false;
var tray = null;
var gotTheLock = true;
if (process.platform !== 'darwin') {
    gotTheLock = electron_1.app.requestSingleInstanceLock();
}
if (!gotTheLock) {
    electron_1.app.quit();
}
else {
    electron_1.app.on('second-instance', function () {
        if (instance_1.default.get()) {
            if (instance_1.default.get()) {
                if (instance_1.default.get().isMinimized()) {
                    instance_1.default.get().restore();
                    if (!instance_1.default.get().isVisible()) {
                        instance_1.default.get().show();
                    }
                    return;
                }
                if (!instance_1.default.get().isVisible()) {
                    instance_1.default.get().show();
                }
            }
        }
    });
    try {
        Sentry.init({ dsn: config_runtime_1.default.SENTRY_KEY });
    }
    catch (e) {
        console.log("Could not init Sentry");
    }
    if (process.platform !== 'darwin') {
        electron_1.app.setAppUserModelId(config_runtime_1.default.APPBUNDLE_ID);
    }
    var showDialogOnWindowError_1 = function (event) {
        var options = {
            type: 'info',
            title: "" + Translations_1.default.get('popup_crash__title'),
            message: "" + Translations_1.default.get('popup_crash__message'),
            defaultId: 0,
            cancelId: 1,
            buttons: [
                "" + Translations_1.default.get('popup_crash__refresh_app'),
                "" + Translations_1.default.get('popup_crash__close_app')
            ]
        };
        electron_1.dialog.showMessageBox(options).then(function (_a) {
            var response = _a.response;
            if (response === 0) {
                page_1.default.reload();
            }
            else {
                forceQuit = true;
                electron_1.app.quit();
            }
        });
    };
    if (!fs_extra_1.default.existsSync(config_runtime_1.default.clientDataPath)) {
        fs_extra_1.default.mkdirSync(config_runtime_1.default.clientDataPath);
    }
    if (!fs_extra_1.default.existsSync(config_runtime_1.default.dataBasePath)) {
        fs_extra_1.default.mkdirSync(config_runtime_1.default.dataBasePath);
    }
    if (process.env.NODE_ENV === "development") {
        require('electron-debug')({
            enabled: true,
            showDevTools: true
        });
    }
    if (!fs_extra_1.default.existsSync(config_runtime_1.default.nimbusAngularWebFolderPath)) {
        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
            console.error("Folder webnotes doesn't exist");
        }
    }
    var server_2 = require("./express-server/bin/www");
    var initAutoUpdater_1 = function () {
        var initHandlers = require('./nimbus-electron/utilities/autoUpdater').initHandlers;
        initHandlers();
    };
    var initAutoStart_1 = function () { return __awaiter(void 0, void 0, void 0, function () {
        var needSetAutoLoad, AutoLaunch, autoLauncher_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, settings_1.default.needSetAutoLoad()];
                case 1:
                    needSetAutoLoad = _a.sent();
                    if (needSetAutoLoad) {
                        AutoLaunch = require('auto-launch');
                        autoLauncher_1 = new AutoLaunch({ name: 'Nimbus Note', isHidden: true });
                        autoLauncher_1.isEnabled()
                            .then(function (isEnabled) {
                            if (isEnabled) {
                                return;
                            }
                            autoLauncher_1.enable();
                        })
                            .catch(function (error) {
                            if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                errorHandler_1.default.log("AutoLauncher err: " + (error == null ? "unknown" : (error.stack || error).toString()));
                            }
                        });
                    }
                    return [2];
            }
        });
    }); };
    var initCustomProtocol_1 = function () {
        if (process.defaultApp) {
            if (process.argv.length >= 2) {
                electron_1.app.setAsDefaultProtocolClient('nimbus-note', process.execPath, [path_1.default.resolve(process.argv[1])]);
            }
        }
        else {
            electron_1.app.setAsDefaultProtocolClient('nimbus-note');
        }
    };
    electron_1.app.on('ready', function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!(process.platform !== 'darwin' && !config_runtime_1.default.IS_DEV_MODE)) return [3, 2];
                    initAutoUpdater_1();
                    return [4, initAutoStart_1()];
                case 1:
                    _a.sent();
                    _a.label = 2;
                case 2:
                    initCustomProtocol_1();
                    if (server_2) {
                        server_1.default.start(server_2);
                    }
                    return [4, initAppWindow_1(createAppWindow_1)];
                case 3:
                    _a.sent();
                    return [2];
            }
        });
    }); });
    var openNoteUrlHandler_1 = function (event, arg) {
        var noteUrl = arg.noteUrl;
        if (noteUrl) {
            if (instance_1.default.get()) {
                instance_1.default.get().loadURL(noteUrl);
                instance_1.default.get().show();
            }
        }
    };
    electron_1.app.on('open-url', function (event, url) {
        var webNotesHost = config_runtime_1.default.WEBNOTES_BASE_URL;
        if (url.indexOf(webNotesHost + "/ws/") === 0) {
            var noteUrl = url.replace(webNotesHost, config_runtime_1.default.LOCAL_SERVER_HTTP_HOST);
            openNoteUrlHandler_1(event, { noteUrl: noteUrl });
        }
    });
    electron_1.app.on('window-all-closed', function () {
        if (process.platform !== 'darwin')
            electron_1.app.quit();
    });
    electron_1.app.on('activate', function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, initAppWindow_1(createAppWindow_1)];
                case 1:
                    _a.sent();
                    return [2];
            }
        });
    }); });
    pdb_1.default.prepareClientDbPath(function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, App_1.default.initSyncState()];
                case 1:
                    _a.sent();
                    return [2];
            }
        });
    }); });
    var createAppWindow_1 = function (windowState) { return __awaiter(void 0, void 0, void 0, function () {
        function closePaymentWindow() {
            if (!appWindow || !child_1.default.get(child_1.default.TYPE_PURCHASE)) {
                return;
            }
            child_1.default.get(child_1.default.TYPE_PURCHASE).close();
        }
        function openPaymentWindow() {
            if (!appWindow || child_1.default.get(child_1.default.TYPE_PURCHASE)) {
                return;
            }
            var purchaseWindow = new electron_1.BrowserWindow({
                modal: false,
                parent: appWindow,
                show: false,
                alwaysOnTop: true,
                backgroundColor: "#3B3B3B",
                width: 925,
                height: 790,
                minWidth: 710,
                maxHeight: 790,
                center: true,
                title: "Get Nimbus Pro",
                fullscreen: false,
                minimizable: false,
                webPreferences: {
                    devTools: true,
                    nodeIntegration: true,
                    allowRunningInsecureContent: false,
                    plugins: false,
                    preload: config_runtime_1.default.nimbusPreloadAssetsFile
                }
            });
            purchaseWindow.once('close', function () {
                child_1.default.set(child_1.default.TYPE_PURCHASE, null);
            });
            child_1.default.set(child_1.default.TYPE_PURCHASE, purchaseWindow);
            if (child_1.default.get(child_1.default.TYPE_PURCHASE)) {
                child_1.default.get(child_1.default.TYPE_PURCHASE).webContents.on('new-window', function (event, url) {
                    setTimeout(function () {
                        child_1.default.get(child_1.default.TYPE_PURCHASE).setAlwaysOnTop(false);
                    }, 1000);
                    event.preventDefault();
                    electron_1.shell.openExternal(url);
                });
                child_1.default.get(child_1.default.TYPE_PURCHASE).once('ready-to-show', function () {
                    page_1.default.loadNimbusPaymentAssets(child_1.default.get(child_1.default.TYPE_PURCHASE));
                    setTimeout(function () {
                        child_1.default.get(child_1.default.TYPE_PURCHASE).show();
                    }, 250);
                });
                child_1.default.get(child_1.default.TYPE_PURCHASE).on('enter-full-screen', function (event) {
                    event.preventDefault();
                    child_1.default.get(child_1.default.TYPE_PURCHASE).maximize();
                });
                child_1.default.get(child_1.default.TYPE_PURCHASE).on('leave-full-screen', function (event) {
                    event.preventDefault();
                    child_1.default.get(child_1.default.TYPE_PURCHASE).unmaximize();
                });
                child_1.default.get(child_1.default.TYPE_PURCHASE).on('minimize', function (event) {
                    event.preventDefault();
                    appWindow.minimize();
                });
                child_1.default.get(child_1.default.TYPE_PURCHASE).once('closed', function () {
                    child_1.default.set(child_1.default.TYPE_PURCHASE, null);
                });
                return child_1.default.get(child_1.default.TYPE_PURCHASE).loadURL(config_runtime_1.default.nimbusPaymentUrl);
            }
        }
        var spellCheck, appWindow, uploadProductRecipeHandler, appWindowAlreadyShown, showAppWindow;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, settings_1.default.hasSpellChecker()];
                case 1:
                    spellCheck = _a.sent();
                    appWindow = new electron_1.BrowserWindow({
                        x: (windowState.bounds ? windowState.bounds.x : undefined),
                        y: (windowState.bounds ? windowState.bounds.y : undefined),
                        width: (windowState.bounds ? windowState.bounds.width : state_1.default.defaultWidth()),
                        height: (windowState.bounds ? windowState.bounds.height : state_1.default.defaultHeight()),
                        show: false,
                        center: !!windowState.bounds,
                        title: config_runtime_1.default.APP_NAME,
                        icon: config_runtime_1.default.applicationIconPath,
                        webPreferences: {
                            nodeIntegration: true,
                            allowRunningInsecureContent: false,
                            plugins: false,
                            preload: config_runtime_1.default.nimbusPreloadAssetsFile,
                            spellcheck: !!spellCheck,
                        },
                    });
                    if (process.platform !== 'darwin') {
                        appWindow.webContents.session.setSpellCheckerLanguages(['en-US', 'ru']);
                    }
                    instance_1.default.set(appWindow);
                    if (process.platform !== 'darwin') {
                        tray = new electron_1.Tray("" + config_runtime_1.default.applicationIconPath);
                        tray.setToolTip('Nimbus Note');
                        tray.on('click', function () {
                            if (instance_1.default.get()) {
                                if (instance_1.default.get().isMinimized()) {
                                    instance_1.default.get().restore();
                                    if (!instance_1.default.get().isVisible()) {
                                        instance_1.default.get().show();
                                    }
                                    return;
                                }
                                instance_1.default.get().show();
                            }
                        });
                        tray.setContextMenu(menu_1.default.getTrayMenu());
                    }
                    state_1.default.listenChangeInfo();
                    state_2.default.listenChangeStatus();
                    print_1.default.listenPrint();
                    logout_1.default.listenRequest();
                    appWindow.webContents.on('crashed', showDialogOnWindowError_1);
                    appWindow.webContents.on('unresponsive', showDialogOnWindowError_1);
                    appWindow.webContents.on('new-window', function (event, url) { return __awaiter(void 0, void 0, void 0, function () {
                        var _a, notePageHost, reloadPage, localNavUrl;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    event.preventDefault();
                                    if (url.indexOf(config_runtime_1.default.nimbusAttachmentUrl) >= 0) {
                                        return [2, (function () { return __awaiter(void 0, void 0, void 0, function () {
                                                var nimbusAttachmentExist;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0: return [4, apiAttachExist_1.default.interceptRequestAsync({ url: url })];
                                                        case 1:
                                                            nimbusAttachmentExist = _a.sent();
                                                            if (nimbusAttachmentExist) {
                                                                return [2, appWindow.loadURL(url)];
                                                            }
                                                            else {
                                                                attachmentHandler_1.default.displayAttachSyncInProgressNotification();
                                                            }
                                                            return [2];
                                                    }
                                                });
                                            }); })()];
                                    }
                                    else if (process.platform === 'darwin' && url.indexOf(config_runtime_1.default.nimbusPaymentExternalUrl) >= 0) {
                                        openPaymentWindow();
                                        return [2];
                                    }
                                    return [4, navigationHandler_1.getNavigationUrlDetails(url)];
                                case 1:
                                    _a = _b.sent(), notePageHost = _a.notePageHost, reloadPage = _a.reloadPage;
                                    if (!notePageHost) {
                                        return [2, electron_1.shell.openExternal(url)];
                                    }
                                    localNavUrl = url.replace(notePageHost, config_runtime_1.default.LOCAL_SERVER_HTTP_HOST);
                                    if (!reloadPage) {
                                        return [2, navigationHandler_1.requestInternalNoteNavigation(localNavUrl)];
                                    }
                                    return [4, page_1.default.reload(false, localNavUrl)];
                                case 2:
                                    _b.sent();
                                    return [2];
                            }
                        });
                    }); });
                    appWindow.webContents.on('will-navigate', function (event, url) { return __awaiter(void 0, void 0, void 0, function () {
                        var routeParams, workspaceId, globalId, attachmentPath;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!(url.indexOf('/client/recent') >= 0)) return [3, 1];
                                    event.preventDefault();
                                    return [3, 4];
                                case 1:
                                    if (!(url.indexOf(config_runtime_1.default.nimbusAttachmentUrl) >= 0)) return [3, 4];
                                    event.preventDefault();
                                    routeParams = urlParser_1.default.getPathParams(url);
                                    return [4, SelectedWorkspace_1.default.getGlobalId()];
                                case 2:
                                    workspaceId = _a.sent();
                                    globalId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;
                                    return [4, attach_1.default.getAttachmentPath({ workspaceId: workspaceId, globalId: globalId })];
                                case 3:
                                    attachmentPath = _a.sent();
                                    if (attachmentPath && instance_1.default.get()) {
                                        instance_1.default.get().loadURL(url);
                                    }
                                    _a.label = 4;
                                case 4: return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:client:app:lang', function (event) {
                        event.sender.send('event:client:app:lang');
                    });
                    electron_1.ipcMain.on('event:client:reload:page:request', function () {
                        page_1.default.reload();
                    });
                    electron_1.ipcMain.on('event:client:error:log', function (event, arg) {
                        errorHandler_1.default.log(arg);
                    });
                    electron_1.ipcMain.on('event:open:note:url:handler', openNoteUrlHandler_1);
                    electron_1.ipcMain.on('event:play:video:request', function (event, arg) {
                        if (arg && arg.html && arg.attach) {
                            return (function () { return __awaiter(void 0, void 0, void 0, function () {
                                var attach, workspaceId, nimbusAttachmentExist, videoWindowInstance_1;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            attach = arg.attach;
                                            return [4, SelectedWorkspace_1.default.getGlobalId()];
                                        case 1:
                                            workspaceId = _a.sent();
                                            return [4, apiAttachExist_1.default.interceptRequestByGlobalIdAsync({
                                                    workspaceId: workspaceId,
                                                    globalId: attach.globalId
                                                })];
                                        case 2:
                                            nimbusAttachmentExist = _a.sent();
                                            if (nimbusAttachmentExist) {
                                                videoWindowInstance_1 = new electron_1.BrowserWindow({
                                                    modal: false,
                                                    width: 640,
                                                    height: 480,
                                                    show: false,
                                                    center: true,
                                                    parent: appWindow,
                                                    icon: "" + config_runtime_1.default.applicationIconPath,
                                                    backgroundColor: '#000000',
                                                    useContentSize: true,
                                                    fullscreen: false,
                                                    minimizable: false,
                                                    webPreferences: {
                                                        devTools: false
                                                    }
                                                });
                                                videoWindowInstance_1.globalId = attach.globalId;
                                                videoWindowInstance_1.workspaceId = workspaceId;
                                                videoWindowInstance_1.on('enter-full-screen', function (event) {
                                                    event.preventDefault();
                                                    videoWindowInstance_1.maximize();
                                                });
                                                videoWindowInstance_1.on('leave-full-screen', function (event) {
                                                    event.preventDefault();
                                                    videoWindowInstance_1.unmaximize();
                                                });
                                                videoWindowInstance_1.on('minimize', function (event) {
                                                    event.preventDefault();
                                                    appWindow.minimize();
                                                });
                                                videoWindowInstance_1.on('close', function (event) {
                                                    if (event && event.sender) {
                                                        var globalId = event.sender.globalId;
                                                        if (globalId) {
                                                            child_1.default.set(child_1.default.TYPE_ATTACH + globalId, null);
                                                        }
                                                    }
                                                });
                                                instance_1.default.createAttachVideoWindow(videoWindowInstance_1, arg.html);
                                                child_1.default.set(child_1.default.TYPE_ATTACH + attach.globalId, videoWindowInstance_1);
                                            }
                                            else {
                                                attachmentHandler_1.default.displayAttachSyncInProgressNotification();
                                            }
                                            return [2];
                                    }
                                });
                            }); })();
                        }
                    });
                    electron_1.ipcMain.on('event:editor:open:attach:request', function (event, arg) {
                        if (!arg) {
                            return;
                        }
                        if (!arg.attach) {
                            return;
                        }
                        attachmentHandler_1.default.playAudioAttachment(arg);
                    });
                    electron_1.ipcMain.on('event:close:video:request', function (event, arg) {
                        if (arg && arg.globalId) {
                            var globalId = arg.globalId;
                            var childWindowId = child_1.default.TYPE_ATTACH + globalId;
                            if (child_1.default.get(childWindowId)) {
                                child_1.default.get(childWindowId).close();
                            }
                        }
                    });
                    electron_1.ipcMain.on('event:close:payment:window:requests', function () {
                        closePaymentWindow();
                    });
                    electron_1.ipcMain.on('event:open:payment:window:request', function () {
                        openPaymentWindow();
                    });
                    electron_1.ipcMain.on('event:sync:start:request', function (event, arg) { return __awaiter(void 0, void 0, void 0, function () {
                        var workspaceId, manualSync;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = arg.workspaceId, manualSync = arg.manualSync;
                                    return [4, workspace_1.default.getLocalId(workspaceId)];
                                case 1:
                                    workspaceId = _a.sent();
                                    SyncManager_1.default.startSyncOnline({ workspaceId: workspaceId, manualSync: manualSync });
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:web:app:export:active:note:request', function (event, arg) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, exportHandler_1.exportHandler(appWindow, arg)];
                                case 1:
                                    _a.sent();
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:web:app:export:active:open:request', function (event, arg) { return __awaiter(void 0, void 0, void 0, function () {
                        var path;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    path = arg.path;
                                    return [4, exportHandler_1.exportOpenPath(path)];
                                case 1:
                                    _a.sent();
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:web:app:export:active:cancel:request', function (event, arg) { return __awaiter(void 0, void 0, void 0, function () {
                        var globalId;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    globalId = arg.globalId;
                                    return [4, exportHandler_1.exportCancel(globalId)];
                                case 1:
                                    _a.sent();
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:sync:queue:request', function (event, arg) { return __awaiter(void 0, void 0, void 0, function () {
                        var workspaceId;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = arg.workspaceId;
                                    return [4, workspace_1.default.getLocalId(workspaceId)];
                                case 1:
                                    workspaceId = _a.sent();
                                    SyncManager_1.default.addSyncQueue({ workspaceId: workspaceId });
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:client:autosync:request', function (event, arg) { return __awaiter(void 0, void 0, void 0, function () {
                        var workspaceId, autoSyncEnable;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = arg.workspaceId;
                                    if (!(typeof (workspaceId) === 'undefined')) return [3, 2];
                                    return [4, SelectedWorkspace_1.default.getGlobalId()];
                                case 1:
                                    workspaceId = _a.sent();
                                    return [3, 4];
                                case 2: return [4, workspace_1.default.getLocalId(workspaceId)];
                                case 3:
                                    workspaceId = _a.sent();
                                    _a.label = 4;
                                case 4: return [4, AutoSyncManager_1.default.checkAutoSyncEnable({ workspaceId: workspaceId })];
                                case 5:
                                    autoSyncEnable = _a.sent();
                                    if (autoSyncEnable) {
                                        event.sender.send('event:client:autosync:response', { workspaceId: workspaceId });
                                    }
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:display:productsId:list:requests', function (event) {
                        event.sender.send('event:display:productsId:list:requests', {
                            "productsIdList": config_runtime_1.default.PURCHASE_PRODUCTS
                        });
                    });
                    electron_1.ipcMain.on('event:display:products:requests', function (event, args) {
                        var displayError = args.displayError;
                        if (typeof (displayError) === "undefined") {
                            displayError = true;
                        }
                        event.sender.send('event:display:products:requests', {
                            products: products_1.default.get(),
                            displayError: displayError
                        });
                    });
                    uploadProductRecipeHandler = function (event, data) {
                        var url = data.url, productInfo = data.productInfo, restore = data.restore;
                        if (url) {
                            NimbusSDK_1.default.getApi().productRecipeUpload(url, function (err, response) { return __awaiter(void 0, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (err) {
                                                errorHandler_1.default.log({
                                                    err: err, response: response,
                                                    description: "Purchase => uploadProductRecipeHandler => productRecipeUpload",
                                                    data: data
                                                });
                                            }
                                            return [4, updateUserUsage_1.default({ useForSync: false })];
                                        case 1:
                                            _a.sent();
                                            return [4, orgs_1.default.syncUserOrganizationsWorkspaces()];
                                        case 2:
                                            _a.sent();
                                            event.sender.send('event:upload:products:recipe:requests', {
                                                response: response,
                                                productInfo: productInfo,
                                                restore: restore
                                            });
                                            return [2];
                                    }
                                });
                            }); });
                        }
                        else {
                            errorHandler_1.default.log({
                                err: null, response: null,
                                description: "Purchase => uploadProductRecipeHandler",
                                data: data
                            });
                        }
                    };
                    electron_1.ipcMain.on('event:upload:products:recipe:requests', uploadProductRecipeHandler);
                    electron_1.ipcMain.on('event:update:user:usage:requests', function () { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, updateUserUsage_1.default({ useForSync: false })];
                                case 1:
                                    _a.sent();
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:purchase:products:dialog:requests', function (event, args) {
                        var dialogOptions = args.dialogOptions, closeWindow = args.closeWindow;
                        closeWindow = closeWindow || false;
                        if (typeof (dialogOptions.title) === "undefined") {
                            dialogOptions.title = "Nimbus Pro";
                        }
                        if (typeof (dialogOptions.detail) === "undefined") {
                            dialogOptions.detail = "In-App Purchase";
                        }
                        if (typeof (dialogOptions.callback) === "undefined") {
                            dialogOptions.callback = function () {
                            };
                        }
                        if (child_1.default.get(child_1.default.TYPE_PURCHASE) && dialogOptions) {
                            if (closeWindow) {
                                return electron_1.dialog.showMessageBox(child_1.default.get(child_1.default.TYPE_PURCHASE), dialogOptions).then(function () {
                                    setTimeout(function () {
                                        child_1.default.get(child_1.default.TYPE_PURCHASE).close();
                                        dialogOptions.callback();
                                        page_1.default.reload();
                                    }, 250);
                                });
                            }
                            electron_1.dialog.showMessageBox(child_1.default.get(child_1.default.TYPE_PURCHASE), dialogOptions).then(function () {
                                dialogOptions.callback();
                            });
                        }
                    });
                    electron_1.ipcMain.on('event:latest:update:version:requests', function (event) {
                        event.sender.send('event:latest:update:version:response', autoUpdater_1.getUpdateInfo());
                    });
                    electron_1.ipcMain.on('event:close:update:window:requests', function (event) {
                        autoUpdater_1.closeUpdateWindow();
                    });
                    electron_1.ipcMain.on('event:make:update:window:requests', function (event) {
                        autoUpdater_1.confirmUpdateWindow();
                    });
                    electron_1.ipcMain.on('event:client:clear:sync:data:request', function () {
                        LocalCache_1.default.requestClear();
                    });
                    electron_1.ipcMain.on('event:web:app:remove:attachments:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, attachmentHandler_1.default.eraseList(args)];
                                case 1:
                                    _a.sent();
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:client:start-single-attach-sync:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var workspaceId, globalId;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = args.workspaceId, globalId = args.globalId;
                                    if (!!workspaceId) return [3, 2];
                                    return [4, SelectedWorkspace_1.default.getGlobalId()];
                                case 1:
                                    workspaceId = _a.sent();
                                    _a.label = 2;
                                case 2:
                                    if (!globalId) return [3, 4];
                                    return [4, AttachmentSingleDownloader_1.default.download({ workspaceId: workspaceId, globalId: globalId })];
                                case 3:
                                    _a.sent();
                                    _a.label = 4;
                                case 4: return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:client:stop-single-attach-sync:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var workspaceId, globalId;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = args.workspaceId, globalId = args.globalId;
                                    if (!globalId) return [3, 2];
                                    return [4, AttachmentSingleDownloader_1.default.cancel({ workspaceId: workspaceId, globalId: globalId })];
                                case 1:
                                    _a.sent();
                                    _a.label = 2;
                                case 2: return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:client:set:active:workspace:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var workspaceId, prevWorkspaceId, _a, _b, _c, _d, _e;
                        return __generator(this, function (_f) {
                            switch (_f.label) {
                                case 0:
                                    workspaceId = args.workspaceId;
                                    if (!(typeof (workspaceId) !== 'undefined')) return [3, 6];
                                    return [4, SelectedWorkspace_1.default.getGlobalId()];
                                case 1:
                                    prevWorkspaceId = _f.sent();
                                    return [4, SelectedWorkspace_1.default.set(workspaceId)];
                                case 2:
                                    _f.sent();
                                    _a = prevWorkspaceId;
                                    return [4, SelectedWorkspace_1.default.getGlobalId()];
                                case 3:
                                    if (_a === (_f.sent())) {
                                        return [2];
                                    }
                                    return [4, SyncManager_1.default.stopSync(prevWorkspaceId)];
                                case 4:
                                    _f.sent();
                                    _c = (_b = event.sender).send;
                                    _d = ['event:client:set:active:workspace:request'];
                                    _e = {};
                                    return [4, SelectedWorkspace_1.default.getGlobalId()];
                                case 5:
                                    _c.apply(_b, _d.concat([(_e.workspaceId = _f.sent(),
                                            _e.onlineStatus = state_2.default.get(),
                                            _e)]));
                                    _f.label = 6;
                                case 6: return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:client:set:active:organization:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var orgData, globalId, prevWorkspaceId, reloadPage;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    orgData = args.org;
                                    globalId = orgData ? orgData.globalId : null;
                                    return [4, SelectedWorkspace_1.default.getGlobalId()];
                                case 1:
                                    prevWorkspaceId = _a.sent();
                                    return [4, SelectedOrganization_1.default.set(globalId)];
                                case 2:
                                    reloadPage = _a.sent();
                                    if (!reloadPage) {
                                        return [2];
                                    }
                                    return [4, SyncManager_1.default.stopSync(prevWorkspaceId)];
                                case 3:
                                    _a.sent();
                                    return [4, page_1.default.reload(false)];
                                case 4:
                                    _a.sent();
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:client:print:attachment:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var attachment, targetPath, exists, win_1;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    attachment = args.attachment;
                                    if (!(typeof (attachment) !== 'undefined' && typeof (attachment['file_uuid']) !== 'undefined')) return [3, 2];
                                    targetPath = pdb_1.default.getClientAttachmentPath() + "/" + attachment['file_uuid'];
                                    return [4, fs_extra_1.default.exists(targetPath)];
                                case 1:
                                    exists = _a.sent();
                                    if (exists) {
                                        if (!appWindow) {
                                            return [2];
                                        }
                                        win_1 = new electron_1.BrowserWindow({
                                            width: 800,
                                            height: 600,
                                            show: true,
                                            parent: appWindow,
                                        });
                                        win_1.webContents.on('dom-ready', function () {
                                            win_1.webContents.executeJavaScript("\n             window.PDFViewerApplication.eventBus.on('pagesloaded', () => {\n              setTimeout(() => {\n                window.PDFViewerApplication.eventBus.dispatch('print');\n              }, 500);\n             });\n          ");
                                        });
                                        electron_pdf_window_1.default.addSupport(win_1);
                                        win_1.loadURL("file://" + targetPath);
                                    }
                                    _a.label = 2;
                                case 2: return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:oauth:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var provider;
                        return __generator(this, function (_a) {
                            provider = args.provider;
                            if (provider === 'google') {
                                google_1.default.auth();
                            }
                            else if (provider === 'facebook') {
                                facebook_1.default.auth();
                            }
                            return [2];
                        });
                    }); });
                    electron_1.ipcMain.on('event:open:business:registration:request', function () {
                        authHandler_1.openBusinessAuthLink();
                    });
                    electron_1.ipcMain.on('event:client:open:url:request', function (event, args) {
                        var url = args.url;
                        navigationHandler_1.openLink(url);
                    });
                    electron_1.ipcMain.on('event:client:open:help:request', function () {
                        helpHandler_1.openHelpLink();
                    });
                    electron_1.ipcMain.on('event:client:open:editor:help:request', function () {
                        helpHandler_1.openEditorHelpLink();
                    });
                    electron_1.ipcMain.on('event:client:open:submit:bug:request', function () {
                        helpHandler_1.openSubmitBugLink();
                    });
                    electron_1.ipcMain.on('event:editor:note:update:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            TextEditor_1.default.updateNote(args);
                            return [2];
                        });
                    }); });
                    electron_1.ipcMain.on('event:editor:attachment:sync:download:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            TextEditor_1.default.downloadAttach(args);
                            return [2];
                        });
                    }); });
                    electron_1.ipcMain.on('event:editor:sync-text-callback:data:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            TextEditor_1.default.updateNoteText(args);
                            return [2];
                        });
                    }); });
                    electron_1.ipcMain.on('event:editor:put:db:data:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var res;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, TextEditor_1.default.put(args)];
                                case 1:
                                    res = _a.sent();
                                    event.sender.send('event:editor:put:db:data:response', res);
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:editor:put:db:array:data:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var res;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, TextEditor_1.default.putArray(args)];
                                case 1:
                                    res = _a.sent();
                                    event.sender.send('event:editor:put:db:array:data:response', res);
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:editor:get:db:data:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var res;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, TextEditor_1.default.getFromRange(args)];
                                case 1:
                                    res = _a.sent();
                                    event.sender.send('event:editor:get:db:data:response', res);
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:editor:getFromRange:db:data:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var res;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, TextEditor_1.default.getFromRange(args)];
                                case 1:
                                    res = _a.sent();
                                    event.sender.send('event:editor:getFromRange:db:data:response', res);
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:editor:getKeysFromRange:db:data:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var res;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, TextEditor_1.default.getKeysFromRange(args)];
                                case 1:
                                    res = _a.sent();
                                    event.sender.send('event:editor:getKeysFromRange:db:data:response', res);
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:editor:delete:db:data:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var res;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, TextEditor_1.default.delete(args)];
                                case 1:
                                    res = _a.sent();
                                    event.sender.send('event:editor:delete:db:data:response', res);
                                    return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:display:products:save:requests', function (event, args) {
                        if (args.products && (args.products.length)) {
                            products_1.default.set(args.products);
                        }
                    });
                    electron_1.ipcMain.on('event:select:note:request', function (event, args) { return __awaiter(void 0, void 0, void 0, function () {
                        var workspaceId, globalId, _a, _b, _c;
                        return __generator(this, function (_d) {
                            switch (_d.label) {
                                case 0:
                                    workspaceId = args.workspaceId, globalId = args.globalId;
                                    if (!globalId) return [3, 2];
                                    _b = (_a = selectedItem_1.default).set;
                                    _c = {};
                                    return [4, workspace_1.default.getLocalId(workspaceId)];
                                case 1:
                                    _b.apply(_a, [(_c.workspaceId = _d.sent(),
                                            _c.itemType = selectedItem_1.default.TYPE_NOTE,
                                            _c.item = globalId,
                                            _c)]);
                                    _d.label = 2;
                                case 2: return [2];
                            }
                        });
                    }); });
                    electron_1.ipcMain.on('event:display:notification:requests', function (event, args) {
                        event.sender.send('event:display:notification:requests', {
                            data: args.data,
                            id: args.id,
                            icon: "" + config_runtime_1.default.applicationIconPath,
                            settings: args.settings,
                            macNotification: process.platform === 'darwin'
                        });
                    });
                    electron_1.ipcMain.on('event:close:notification:requests', function (event) {
                        event.sender.send('event:close:notification:requests', {});
                    });
                    appWindow.on('closed', function () {
                        setTimeout(function () {
                            if (server_2) {
                                server_2.close();
                            }
                            appWindow = null;
                        }, 1);
                    });
                    appWindowAlreadyShown = false;
                    showAppWindow = function () {
                        setTimeout(function () {
                            PopupRateApp_1.default.check();
                            if (!appWindowAlreadyShown) {
                                appWindowAlreadyShown = true;
                            }
                        }, appWindowAlreadyShown ? 0 : 4000);
                    };
                    appWindow.on('show', showAppWindow);
                    appWindow.once('ready-to-show', function () {
                        if (process.platform !== 'darwin') {
                            var showMinimized = process.argv.indexOf('--hidden') >= 0;
                            if (showMinimized) {
                                return;
                            }
                        }
                        appWindow.show();
                    });
                    electron_1.app.on('before-quit', function () {
                        forceQuit = true;
                    });
                    if (process.platform === 'darwin') {
                        electron_1.app.on('activate', function () {
                            if (appWindow) {
                                if (!appWindow.isVisible()) {
                                    appWindow.show();
                                }
                            }
                        });
                        appWindow.on('close', function (event) {
                            if (!forceQuit) {
                                event.preventDefault();
                                if (!appWindow.isFullScreen()) {
                                    return appWindow.hide();
                                }
                                appWindow.setFullScreen(false);
                                setTimeout(function () {
                                    appWindow.hide();
                                }, 750);
                            }
                            else {
                                instance_1.default.set(null);
                            }
                        });
                    }
                    else {
                        appWindow.on('close', function (event) {
                            if (!forceQuit) {
                                event.preventDefault();
                                appWindow.hide();
                            }
                            else {
                                instance_1.default.set(null);
                            }
                        });
                    }
                    menu_1.default.add();
                    login_1.default.tryLoginClient();
                    return [2];
            }
        });
    }); };
    var initAppWindow_1 = function (callback) { return __awaiter(void 0, void 0, void 0, function () {
        var appWindow;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    appWindow = instance_1.default.get();
                    if (!!appWindow) return [3, 2];
                    return [4, Translations_1.default.initLang()];
                case 1:
                    _a.sent();
                    state_1.default.get(function (err, windowState) {
                        callback(windowState);
                    });
                    _a.label = 2;
                case 2: return [2];
            }
        });
    }); };
}
