"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var config_1 = __importDefault(require("./config"));
var runtimeConfig = null;
(function () {
    if (!runtimeConfig && config_1.default) {
        var appDataPath = electron_1.app.getPath('userData');
        if (appDataPath) {
            var clientDataPath = appDataPath;
            var dataBasePath = clientDataPath + "/databases";
            var nimbusAttachmentPath = clientDataPath + "/attachments";
            var nimbusTempAttachmentPath = clientDataPath + "/temp";
            var nimbusExportPath = clientDataPath + "/export-items";
            runtimeConfig = __assign(__assign({}, config_1.default), { clientDataPath: clientDataPath,
                dataBasePath: dataBasePath,
                nimbusAttachmentPath: nimbusAttachmentPath,
                nimbusTempAttachmentPath: nimbusTempAttachmentPath,
                nimbusExportPath: nimbusExportPath });
        }
    }
    return runtimeConfig;
})();
if (!runtimeConfig) {
    runtimeConfig = config_1.default;
}
exports.default = runtimeConfig;
