"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_extra_1 = __importDefault(require("fs-extra"));
var nedb_1 = __importDefault(require("nedb"));
var config_runtime_1 = __importDefault(require("./config.runtime"));
var auth_1 = __importDefault(require("./nimbus-electron/auth/auth"));
var customError_1 = __importDefault(require("./nimbus-electron/utilities/customError"));
var workspace_1 = __importDefault(require("./nimbus-electron/db/models/workspace"));
var dbConnections = {};
var dbClientId = null;
var AppDatabase = (function () {
    function AppDatabase() {
    }
    AppDatabase.prepareClientDbPath = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        auth_1.default.getUser(function (err, authInfo) {
            if (authInfo && Object.keys(authInfo).length) {
                if (AppDatabase.getDbClientId() !== authInfo.userId) {
                    AppDatabase.setDbClientId(authInfo.userId);
                }
                callback(null, authInfo);
            }
            else {
                callback(null, null);
            }
        });
    };
    AppDatabase.setDbClientId = function (clientId) {
        dbClientId = clientId;
        AppDatabase.setClientDbPath(clientId);
        AppDatabase.setWorkspaceDbPath(config_runtime_1.default.DEFAULT_WORKSPACE_ID);
        AppDatabase.setClientAttachmentPath(clientId);
        AppDatabase.setClientTempAttachmentPath(clientId);
        AppDatabase.setClientExportPath(clientId);
        dbConnections = {};
    };
    AppDatabase.getDbClientId = function () {
        return dbClientId;
    };
    AppDatabase.clearDbConnections = function () {
        for (var i in dbConnections) {
            if (dbConnections.hasOwnProperty(i)) {
                delete dbConnections[i];
            }
        }
    };
    AppDatabase.setClientDbPath = function (clientId) {
        if (clientId) {
            config_runtime_1.default.clientDataBasePath = config_runtime_1.default.dataBasePath + "/" + clientId.toString();
            if (!fs_extra_1.default.existsSync(config_runtime_1.default.clientDataBasePath)) {
                fs_extra_1.default.mkdirSync(config_runtime_1.default.clientDataBasePath);
            }
        }
    };
    AppDatabase.setWorkspaceDbPath = function (workspaceId) {
        if (!workspaceId) {
            if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                console.log("Client workspaceId is empty", workspaceId);
            }
        }
        if (!config_runtime_1.default.clientDataBasePath) {
            if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                console.log("Client database path is empty", config_runtime_1.default.clientDataBasePath, workspaceId);
            }
            return;
        }
        var workspaceDbPath = config_runtime_1.default.clientDataBasePath + "/" + workspaceId;
        if (!fs_extra_1.default.existsSync(workspaceDbPath)) {
            fs_extra_1.default.mkdirSync(workspaceDbPath);
        }
    };
    AppDatabase.getClientDbPath = function () {
        return config_runtime_1.default.clientDataBasePath || "";
    };
    AppDatabase.setClientAttachmentPath = function (clientId) {
        if (clientId) {
            config_runtime_1.default.clientAttachmentBasePath = config_runtime_1.default.nimbusAttachmentPath + "/" + clientId.toString();
            if (!fs_extra_1.default.existsSync(config_runtime_1.default.clientAttachmentBasePath)) {
                fs_extra_1.default.mkdirSync(config_runtime_1.default.clientAttachmentBasePath);
            }
        }
    };
    AppDatabase.setClientTempAttachmentPath = function (clientId) {
        if (clientId) {
            if (!fs_extra_1.default.existsSync(config_runtime_1.default.nimbusTempAttachmentPath)) {
                fs_extra_1.default.mkdirSync(config_runtime_1.default.nimbusTempAttachmentPath);
            }
            config_runtime_1.default.clientTempAttachmentBasePath = config_runtime_1.default.nimbusTempAttachmentPath + "/" + clientId.toString();
            if (!fs_extra_1.default.existsSync(config_runtime_1.default.clientTempAttachmentBasePath)) {
                fs_extra_1.default.mkdirSync(config_runtime_1.default.clientTempAttachmentBasePath);
            }
        }
    };
    AppDatabase.setClientExportPath = function (clientId) {
        if (clientId) {
            if (!fs_extra_1.default.existsSync(config_runtime_1.default.nimbusExportPath)) {
                fs_extra_1.default.mkdirSync(config_runtime_1.default.nimbusExportPath);
            }
            config_runtime_1.default.clientExportBasePath = config_runtime_1.default.nimbusExportPath + "/" + clientId.toString();
            if (!fs_extra_1.default.existsSync(config_runtime_1.default.clientExportBasePath)) {
                fs_extra_1.default.mkdirSync(config_runtime_1.default.clientExportBasePath);
            }
        }
    };
    AppDatabase.getClientAttachmentPath = function () {
        return config_runtime_1.default.clientAttachmentBasePath || "";
    };
    AppDatabase.getClientTempAttachmentPath = function () {
        return config_runtime_1.default.clientTempAttachmentBasePath || "";
    };
    AppDatabase.getClientExportPath = function () {
        return config_runtime_1.default.clientExportBasePath || "";
    };
    AppDatabase.setClientAttachmentUrl = function (clientId) {
        if (clientId) {
            config_runtime_1.default.clientAttachmentBaseUrl = config_runtime_1.default.nimbusAttachmentUrl + "/" + clientId.toString();
        }
    };
    AppDatabase.getInstance = function (inputData) {
        var dbName = inputData.dbName;
        var workspaceId = inputData.workspaceId;
        if (!dbName) {
            return null;
        }
        if (!config_runtime_1.default) {
            return null;
        }
        if (!workspaceId) {
            workspaceId = workspace_1.default.DEFAULT_NAME;
        }
        var dbFullName;
        if (dbName === config_runtime_1.default.DB_USER_MODEL_NAME || dbName === config_runtime_1.default.DB_SETTINGS_MODEL_NAME) {
            dbFullName = dbName;
        }
        else if (dbName === config_runtime_1.default.DB_WORKSPACE_MODEL_NAME || dbName === config_runtime_1.default.DB_USER_SETTINGS_MODEL_NAME) {
            dbFullName = dbName;
        }
        else if (config_runtime_1.default.clientDataBasePath && workspaceId) {
            dbFullName = workspaceId + "/" + dbName;
        }
        if (!dbFullName) {
            if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                console.log("Client db full name is empty", config_runtime_1.default.clientDataBasePath, workspaceId, dbName);
            }
        }
        if (typeof (dbConnections[dbFullName]) === "undefined") {
            var dbPath = config_runtime_1.default.dataBasePath + "/" + dbName;
            if (dbName === config_runtime_1.default.DB_USER_MODEL_NAME || dbName === config_runtime_1.default.DB_SETTINGS_MODEL_NAME) {
                dbPath = config_runtime_1.default.dataBasePath + "/" + dbName;
            }
            else if (config_runtime_1.default.clientDataBasePath) {
                if (dbName === config_runtime_1.default.DB_WORKSPACE_MODEL_NAME || dbName === config_runtime_1.default.DB_USER_SETTINGS_MODEL_NAME) {
                    dbPath = config_runtime_1.default.clientDataBasePath + "/" + dbName;
                }
                else {
                    dbPath = config_runtime_1.default.clientDataBasePath + "/" + workspaceId + "/" + dbName;
                }
            }
            else {
                if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                    console.log("Client db path is empty", config_runtime_1.default.clientDataBasePath, workspaceId, dbName);
                }
                return null;
            }
            dbConnections[dbFullName] = new nedb_1.default({
                filename: dbPath,
                autoload: true
            });
        }
        return dbConnections[dbFullName];
    };
    AppDatabase.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = options.workspaceId;
        var dbName = options.dbName;
        var pdb = AppDatabase.getInstance({ workspaceId: workspaceId, dbName: dbName });
        if (!pdb) {
            console.log("No database connection", dbName);
            return callback(null, null);
        }
        data = data.findParams ? data.findParams : data;
        try {
            pdb.findOne(AppDatabase.getFindQuery(data, options), function (err, doc) {
                if (err) {
                    if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                        console.log("Problem to find objects doc & err: ", doc, err);
                    }
                    return callback(customError_1.default.itemFindError(), null);
                }
                callback(null, doc);
            });
        }
        catch (err) {
            return callback(customError_1.default.itemFindError(), null);
        }
    };
    AppDatabase.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = options.workspaceId;
        var dbName = options.dbName;
        var pdb = AppDatabase.getInstance({ workspaceId: workspaceId, dbName: dbName });
        if (!pdb) {
            console.log("No database connection", dbName);
            return callback(null, []);
        }
        var findQuery = options.findParams ? options.findParams : { selector: data };
        var query = pdb.find(AppDatabase.getFindQuery(findQuery.selector, options));
        var sortQuery = {};
        if (typeof (findQuery.sort) !== "undefined") {
            sortQuery = {};
            for (var i in findQuery.sort) {
                if (findQuery.sort.hasOwnProperty(i)) {
                    for (var j in findQuery.sort[i]) {
                        if (findQuery.sort[i].hasOwnProperty(j)) {
                            sortQuery[j] = findQuery.sort[i][j] === "desc" ? -1 : 1;
                        }
                    }
                }
            }
            if (Object.keys(sortQuery)) {
                query.sort(sortQuery);
            }
        }
        if (typeof (findQuery.skip) !== "undefined") {
            query.skip(findQuery.skip);
        }
        if (typeof (findQuery.limit) !== "undefined") {
            query.limit(findQuery.limit);
        }
        try {
            query.exec(function (err, docs) {
                if (err) {
                    if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                        console.log("Problem to findAll object", data, err);
                    }
                    return callback(customError_1.default.itemFindAllError(), []);
                }
                callback(null, docs);
            });
        }
        catch (err) {
            return callback(customError_1.default.itemFindAllError(), []);
        }
    };
    AppDatabase.create = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = options.workspaceId;
        var dbName = options.dbName;
        var pdb = AppDatabase.getInstance({ workspaceId: workspaceId, dbName: dbName });
        if (!pdb) {
            return callback(customError_1.default.noDbConnection(options.dbName), null);
        }
        var newData = options.prepareModelData(data);
        try {
            pdb.insert(newData, function (err, newDoc) {
                if (err || !newDoc) {
                    if (err.errorType === 'uniqueViolated' && err.key === 'default') {
                        return callback(null, newDoc);
                    }
                    return callback(customError_1.default.itemSaveError(), null);
                }
                callback(null, newDoc);
            });
        }
        catch (err) {
            return callback(customError_1.default.itemSaveError(), null);
        }
    };
    AppDatabase.update = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = options.workspaceId;
        var dbName = options.dbName;
        var pdb = AppDatabase.getInstance({ workspaceId: workspaceId, dbName: dbName });
        if (!pdb) {
            return callback(customError_1.default.noDbConnection(dbName), 0);
        }
        AppDatabase.find(data, options, function (err, doc) {
            if (err || !doc) {
                if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                    console.log("No db objects for update: ", data, options);
                }
                return callback(null, 0);
            }
            for (var prop in options.updateFields) {
                if (options.updateFields.hasOwnProperty(prop)) {
                    doc[prop] = options.updateFields[prop];
                }
            }
            try {
                pdb.update({ _id: doc._id }, doc, { upsert: true }, function (err, numReplaced, upsert) {
                    if (err) {
                        return callback(customError_1.default.itemUpdateError(), 0);
                    }
                    callback(null, numReplaced);
                });
            }
            catch (err) {
                return callback(customError_1.default.itemUpdateError(), 0);
            }
        });
    };
    AppDatabase.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = options.workspaceId;
        var dbName = options.dbName;
        var pdb = AppDatabase.getInstance({ workspaceId: workspaceId, dbName: dbName });
        if (!pdb) {
            return callback(customError_1.default.noDbConnection(options.dbName), 0);
        }
        if (typeof (data.erised) === "undefined") {
            options.ignoreErised = true;
        }
        var findQuery = options.findParams ? options.findParams : { selector: data };
        try {
            pdb.remove(AppDatabase.getFindQuery(findQuery.selector, options), { multi: true }, function (err, numRemoved) {
                if (err) {
                    return callback(err, 0);
                }
                callback(null, numRemoved);
            });
        }
        catch (err) {
            return callback(err, 0);
        }
    };
    AppDatabase.count = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = options.workspaceId;
        var dbName = options.dbName;
        var pdb = AppDatabase.getInstance({ workspaceId: workspaceId, dbName: dbName });
        if (!pdb) {
            console.log("No database connection", dbName);
            return callback(null, 0);
        }
        var findQuery = options.findParams ? options.findParams : { selector: data };
        try {
            pdb.count(AppDatabase.getFindQuery(findQuery.selector, options), function (err, count) {
                if (err) {
                    if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                        console.log("Problem to get db count err & count: ", err, count);
                    }
                }
                callback(null, count);
            });
        }
        catch (err) {
            if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                console.log("Problem to get db count err & count: ", err, 0);
            }
            callback(null, 0);
        }
    };
    AppDatabase.getFindQuery = function (selector, options) {
        if (selector) {
            var queryMissErisedValue = typeof (selector.erised) === "undefined";
            var queryMissErisedIgnoreValue = typeof (options.ignoreErised) === "undefined";
            if (queryMissErisedValue && queryMissErisedIgnoreValue) {
                selector.erised = { '$ne': true };
            }
        }
        return selector;
    };
    AppDatabase.prepareItemDbProperties = function (item, data) {
        if (data._id) {
            item._id = data._id;
        }
        if (data._rev) {
            item._rev = data._rev;
        }
        return item;
    };
    AppDatabase.getErasedUpdateFields = function () {
        return {
            erised: true,
            needSync: true
        };
    };
    return AppDatabase;
}());
exports.default = AppDatabase;
