"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs-extra");
var mimeTypes = require("mime-types");
var attach_1 = __importDefault(require("../db/models/attach"));
var pdb_1 = __importDefault(require("../../pdb"));
var workspace_1 = __importDefault(require("../db/models/workspace"));
var syncHandler_1 = __importDefault(require("./syncHandler"));
var SelectedWorkspace_1 = __importDefault(require("../workspace/SelectedWorkspace"));
var apiAttachExist_1 = __importDefault(require("../request/interceptors/jsonInterceptors/attach/apiAttachExist"));
var filenamify_1 = __importDefault(require("filenamify"));
var errorHandler_1 = __importDefault(require("./errorHandler"));
var electron_1 = require("electron");
var path_1 = __importDefault(require("path"));
var instance_1 = __importDefault(require("../window/instance"));
var Translations_1 = __importDefault(require("../translations/Translations"));
var ALLOWED_AUDIO_EXTENSIONS = [
    ".3gp", ".aa", ".aac", ".aax", ".act", ".aiff", ".alac", ".amr", ".ape", ".au", ".awb", ".dct", ".dss", ".dvf", ".flac",
    ".gsm", ".iklax", ".ivs", ".m4a", ".m4b", ".m4p", ".mmf", ".mp3", ".mpc", ".msv", ".nmf", ".ogg", ".oga", ".mogg",
    ".opus", ".ra, .rm", ".raw", ".rf64", ".sln", ".tta", ".voc", ".vox", ".wav", ".wma", ".wv", ".webm", ".8svx", ".cda",
];
var AttachmentHandler = (function () {
    function AttachmentHandler() {
    }
    AttachmentHandler.eraseList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, noteId, attachments, _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, noteId = inputData.noteId, attachments = inputData.attachments;
                        if (typeof (workspaceId) === 'undefined') {
                            return [2];
                        }
                        if (!noteId) {
                            return [2];
                        }
                        if (!attachments) {
                            return [2];
                        }
                        if (!attachments.length) {
                            return [2];
                        }
                        if (!workspaceId) return [3, 2];
                        return [4, workspace_1.default.getLocalId(workspaceId)];
                    case 1:
                        _a = _b.sent();
                        return [3, 3];
                    case 2:
                        _a = null;
                        _b.label = 3;
                    case 3:
                        workspaceId = _a;
                        attach_1.default.findAll({
                            globalId: { $in: attachments },
                            noteGlobalId: noteId,
                        }, { workspaceId: workspaceId }, function (err, attachItems) { return __awaiter(_this, void 0, void 0, function () {
                            var _loop_1, _i, attachItems_1, attachItem;
                            var _this = this;
                            return __generator(this, function (_a) {
                                if (err || !attachItems) {
                                    return [2];
                                }
                                _loop_1 = function (attachItem) {
                                    attach_1.default.count({ globalId: attachItem.globalId }, { workspaceId: workspaceId }, function (err, count) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            if (count === 1) {
                                                attach_1.default.erase({ globalId: attachItem.globalId }, { workspaceId: workspaceId }, function () { return __awaiter(_this, void 0, void 0, function () {
                                                    var targetPath, exists, previewPath, previewExist;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                if (!pdb_1.default.getClientAttachmentPath()) {
                                                                    return [2];
                                                                }
                                                                targetPath = pdb_1.default.getClientAttachmentPath() + "/" + attachItem.storedFileUUID;
                                                                syncHandler_1.default.log('=> remove attach targetPath?', targetPath);
                                                                return [4, fs.exists(targetPath)];
                                                            case 1:
                                                                exists = _a.sent();
                                                                if (exists) {
                                                                    try {
                                                                        fs.unlink(targetPath, function () {
                                                                        });
                                                                    }
                                                                    catch (e) { }
                                                                }
                                                                previewPath = targetPath + "-preview";
                                                                return [4, fs.exists(previewPath)];
                                                            case 2:
                                                                previewExist = _a.sent();
                                                                if (previewExist) {
                                                                    try {
                                                                        fs.unlink(previewPath, function () {
                                                                        });
                                                                    }
                                                                    catch (e) { }
                                                                }
                                                                return [2];
                                                        }
                                                    });
                                                }); });
                                            }
                                            return [2];
                                        });
                                    }); });
                                };
                                for (_i = 0, attachItems_1 = attachItems; _i < attachItems_1.length; _i++) {
                                    attachItem = attachItems_1[_i];
                                    _loop_1(attachItem);
                                }
                                return [2];
                            });
                        }); });
                        return [2];
                }
            });
        });
    };
    AttachmentHandler.playAudioAttachment = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var attach, workspaceId, nimbusAttachmentExist, mime, ext, displayName, nameExtParts, nameExt, nameContainAllowedExt, findExtByMime, mimeContainExt, nameContainExtAsMime, targetPath, destPath, exists, filenames, err_1, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        attach = inputData.attach;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        workspaceId = _a.sent();
                        return [4, apiAttachExist_1.default.interceptRequestByGlobalIdAsync({
                                workspaceId: workspaceId,
                                globalId: attach.globalId
                            })];
                    case 2:
                        nimbusAttachmentExist = _a.sent();
                        if (!nimbusAttachmentExist) {
                            return [2, AttachmentHandler.displayAttachSyncInProgressNotification()];
                        }
                        if (!pdb_1.default.getClientAttachmentPath()) {
                            return [2];
                        }
                        if (!pdb_1.default.getClientTempAttachmentPath()) {
                            return [2];
                        }
                        mime = attach.mime.split('/');
                        ext = mime.length >= 2 ? mime[1] : '';
                        displayName = filenamify_1.default(attach.displayName);
                        nameExtParts = displayName ? displayName.split('.') : [];
                        nameExt = nameExtParts.length >= 2 ? "." + nameExtParts[nameExtParts.length - 1] : '';
                        nameContainAllowedExt = nameExt && ALLOWED_AUDIO_EXTENSIONS.indexOf(nameExt) >= 0;
                        if (nameContainAllowedExt) {
                            displayName = "" + displayName;
                        }
                        findExtByMime = mimeTypes.extension(ext) ? "." + mimeTypes.extension(ext) : '';
                        mimeContainExt = findExtByMime && ALLOWED_AUDIO_EXTENSIONS.indexOf(findExtByMime) >= 0;
                        if (!nameContainAllowedExt && mimeContainExt) {
                            displayName = displayName + "." + findExtByMime;
                        }
                        nameContainExtAsMime = displayName.lastIndexOf("." + ext) === (displayName.length - (ext.length + 1));
                        if (!nameContainAllowedExt && !mimeContainExt && !nameContainExtAsMime) {
                            displayName = displayName + "." + ext;
                        }
                        targetPath = pdb_1.default.getClientAttachmentPath() + "/" + attach.storedFileUUID;
                        destPath = displayName && ext ? pdb_1.default.getClientTempAttachmentPath() + "/" + displayName : null;
                        return [4, fs.exists(targetPath)];
                    case 3:
                        exists = _a.sent();
                        if (!exists) {
                            errorHandler_1.default.log("event:editor:open:attach:request => file not exist by path: " + targetPath);
                            return [2];
                        }
                        if (!destPath) {
                            errorHandler_1.default.log("event:editor:open:attach:request => file has problem with name: '" + displayName + "' or mime type: " + attach.mime + " and can not be open by path " + targetPath);
                            return [2];
                        }
                        filenames = [];
                        _a.label = 4;
                    case 4:
                        _a.trys.push([4, 6, , 7]);
                        return [4, fs.readdir(pdb_1.default.getClientTempAttachmentPath())];
                    case 5:
                        filenames = _a.sent();
                        return [3, 7];
                    case 6:
                        err_1 = _a.sent();
                        errorHandler_1.default.log(err_1);
                        return [3, 7];
                    case 7:
                        _a.trys.push([7, 9, , 10]);
                        return [4, fs.copy(targetPath, destPath)];
                    case 8:
                        _a.sent();
                        electron_1.shell.openItem(destPath);
                        setTimeout(function () {
                            filenames.map(function (filename) {
                                if (filename.indexOf('.') > 0 && filename !== displayName) {
                                    fs.unlink(path_1.default.join(pdb_1.default.getClientTempAttachmentPath(), filename));
                                }
                            });
                        }, 1000);
                        return [3, 10];
                    case 9:
                        err_2 = _a.sent();
                        errorHandler_1.default.log(err_2);
                        return [3, 10];
                    case 10: return [2];
                }
            });
        });
    };
    AttachmentHandler.displayAttachSyncInProgressNotification = function () {
        if (instance_1.default.get()) {
            instance_1.default.get().webContents.send('event:display:angular:notification:requests', {
                data: {
                    text: Translations_1.default.get('notice_sync_is_not_finished')
                },
                id: 'nimbus_angular_notice',
                settings: {
                    position: 'bottom-right'
                }
            });
        }
    };
    ;
    return AttachmentHandler;
}());
exports.default = AttachmentHandler;
