"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var $ = require("cheerio");
var TextHandler = (function () {
    function TextHandler() {
    }
    TextHandler.stripTags = function (str) {
        if (str) {
            str = $.load(str).text().trim();
        }
        return str ? str : '';
    };
    return TextHandler;
}());
exports.default = TextHandler;
