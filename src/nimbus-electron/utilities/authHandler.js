"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../config"));
function openBusinessAuthLink() {
    require('electron').shell.openExternal("https://nimbusweb.me/auth/?f=register&rt=business&client=" + config_1.default.CLIENT_NAME);
}
exports.openBusinessAuthLink = openBusinessAuthLink;
