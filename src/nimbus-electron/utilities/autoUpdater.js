"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var electron_updater_1 = require("electron-updater");
var errorHandler_1 = __importDefault(require("./errorHandler"));
var Translations_1 = __importDefault(require("../translations/Translations"));
var state_1 = __importDefault(require("../online/state"));
var config_runtime_1 = __importDefault(require("../../config.runtime"));
var instance_1 = __importDefault(require("../window/instance"));
var child_1 = __importDefault(require("../window/child"));
var page_1 = __importDefault(require("../request/page"));
var updater;
var updateInfo;
var showError;
electron_updater_1.autoUpdater.autoDownload = false;
function showUpdatePopup(data) {
    var updateWindow = new electron_1.BrowserWindow({
        parent: instance_1.default.get(),
        modal: true,
        show: false,
        alwaysOnTop: true,
        width: 400,
        height: 175,
        icon: "" + config_runtime_1.default.applicationIconPath,
        resizable: false,
        minimizable: false,
        skipTaskbar: true,
        webPreferences: {
            devTools: process.env.NODE_ENV === "development",
            nodeIntegration: true,
            allowRunningInsecureContent: false,
            plugins: false,
            preload: config_runtime_1.default.nimbusPreloadAssetsFile
        }
    });
    updateWindow.setMenu(null);
    child_1.default.set(child_1.default.TYPE_UPDATE, updateWindow);
    updateWindow.once('close', function () {
        child_1.default.set(child_1.default.TYPE_UPDATE, null);
        if (updater) {
            updater.enabled = true;
        }
    });
    child_1.default.get(child_1.default.TYPE_UPDATE).once('ready-to-show', function () {
        page_1.default.loadNimbusUpdateAssets(child_1.default.get(child_1.default.TYPE_UPDATE));
        setTimeout(function () {
            child_1.default.get(child_1.default.TYPE_UPDATE).show();
        }, 250);
    });
    updateWindow.loadURL(config_runtime_1.default.nimbusUpdateUrl);
}
function sendStatusToWindow(data) {
    var event = data.event, error = data.error, text = data.text, info = data.info, progress = data.progress;
    if (text) {
        errorHandler_1.default.log(text);
    }
    if (info) {
        updateInfo = info;
        errorHandler_1.default.log(info);
    }
    if (error) {
        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
            console.log(error);
        }
        errorHandler_1.default.log(error);
        if (updater) {
            updater.enabled = true;
            updater = null;
        }
        return electron_1.dialog.showErrorBox(Translations_1.default.get('update_error__title'), Translations_1.default.get('update_error__message'));
    }
    switch (event) {
        case 'update-available': {
            showUpdatePopup(data);
            break;
        }
        case 'download-progress': {
            if (child_1.default.get(child_1.default.TYPE_UPDATE)) {
                child_1.default.get(child_1.default.TYPE_UPDATE).webContents.send('event:latest:update:progress:response', {
                    progress: Math.round(progress.percent + 0.0001) + "%"
                });
            }
            break;
        }
        case 'update-not-available': {
            closeUpdateWindow();
            if (showError) {
                electron_1.dialog.showMessageBox({
                    title: Translations_1.default.get('update_not_available__title'),
                    message: Translations_1.default.get('update_not_available__message'),
                    icon: config_runtime_1.default.applicationIconPath,
                }).then(function () {
                    if (updater) {
                        updater.enabled = true;
                        updater = null;
                    }
                    showError = false;
                });
            }
            break;
        }
        case 'update-downloaded': {
            closeUpdateWindow();
            electron_1.dialog.showMessageBox({
                title: Translations_1.default.get('update_downloaded__title'),
                message: Translations_1.default.get('update_downloaded__message'),
                icon: config_runtime_1.default.applicationIconPath,
            }).then(function () {
                if (updater) {
                    updater.enabled = true;
                    updater = null;
                }
                setImmediate(function () { return electron_updater_1.autoUpdater.quitAndInstall(); });
            });
            break;
        }
        case 'error': {
            if (updater) {
                updater.enabled = true;
                updater = null;
            }
            closeUpdateWindow();
            if (showError) {
                sendStatusToWindow({ error: error });
            }
            break;
        }
    }
}
function closeUpdateWindow() {
    if (child_1.default.get(child_1.default.TYPE_UPDATE)) {
        child_1.default.get(child_1.default.TYPE_UPDATE).close();
    }
    if (updater) {
        updater.enabled = true;
        updater = null;
    }
}
exports.closeUpdateWindow = closeUpdateWindow;
function confirmUpdateWindow() {
    try {
        electron_updater_1.autoUpdater.downloadUpdate().catch(function (error) {
            if (isNetworkError(error)) {
                errorHandler_1.default.log('Network Error');
            }
            else {
                errorHandler_1.default.log('Unknown Error');
                errorHandler_1.default.log(error == null ? "unknown" : (error.stack || error).toString());
            }
        });
    }
    catch (error) {
        sendStatusToWindow({ text: 'downloadUpdate error', error: error });
    }
}
exports.confirmUpdateWindow = confirmUpdateWindow;
function isNetworkError(errorObject) {
    return errorObject.message === "net::ERR_INTERNET_DISCONNECTED" ||
        errorObject.message === "net::ERR_PROXY_CONNECTION_FAILED" ||
        errorObject.message === "net::ERR_CONNECTION_RESET" ||
        errorObject.message === "net::ERR_CONNECTION_CLOSE" ||
        errorObject.message === "net::ERR_NAME_NOT_RESOLVED" ||
        errorObject.message === "net::ERR_CONNECTION_TIMED_OUT";
}
function getUpdateInfo() {
    return updateInfo;
}
exports.getUpdateInfo = getUpdateInfo;
function autoCheck() {
    if (updater) {
        updater.enabled = true;
        updater = null;
    }
    showError = false;
    setTimeout(function () {
        if (state_1.default.get()) {
            try {
                if (!config_runtime_1.default.USE_DEV_SYNC) {
                    electron_updater_1.autoUpdater.checkForUpdates();
                }
            }
            catch (error) {
                if (showError) {
                    sendStatusToWindow({ text: 'autoCheck => checkForUpdates => error', error: error });
                }
                updater.enabled = true;
            }
        }
    }, 5000);
}
exports.autoCheck = autoCheck;
function menuCheck(menuItem) {
    if (!state_1.default.get()) {
        if (instance_1.default.get()) {
            instance_1.default.get().webContents.send('event:client:workspace:message:response', {
                message: Translations_1.default.get('toast__can_not_update_application__offline'),
            });
        }
        return;
    }
    updater = menuItem;
    updater.enabled = false;
    showError = true;
    try {
        if (!config_runtime_1.default.USE_DEV_SYNC) {
            electron_updater_1.autoUpdater.checkForUpdates();
        }
    }
    catch (error) {
        if (showError) {
            sendStatusToWindow({ text: 'menuCheck => checkForUpdates => error', error: error });
        }
        updater.enabled = true;
    }
}
exports.menuCheck = menuCheck;
function initHandlers() {
    electron_updater_1.autoUpdater.on('checking-for-update', function () {
        sendStatusToWindow({ event: 'checking-for-update', text: 'Checking for update...' });
    });
    electron_updater_1.autoUpdater.on('update-available', function (info) {
        sendStatusToWindow({ event: 'update-available', text: 'Update available.', info: info });
    });
    electron_updater_1.autoUpdater.on('update-not-available', function (info) {
        sendStatusToWindow({ event: 'update-not-available', text: 'Update not available.', info: info });
    });
    electron_updater_1.autoUpdater.on('error', function (error) {
        if (showError) {
            sendStatusToWindow({ event: 'update-not-available', text: 'Error in auto-updater.', error: error });
        }
    });
    electron_updater_1.autoUpdater.on('download-progress', function (progressObj) {
        showError = true;
        var text = "Download speed: " + progressObj.bytesPerSecond;
        text = text + ' - Downloaded ' + progressObj.percent + '%';
        text = text + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
        sendStatusToWindow({ event: 'download-progress', text: text, progress: progressObj });
    });
    electron_updater_1.autoUpdater.on('update-downloaded', function (info) {
        sendStatusToWindow({ event: 'update-downloaded', text: 'Update downloaded', info: info });
    });
    setInterval(function () {
        autoCheck();
    }, 86400000);
    setTimeout(function () {
        autoCheck();
    }, 1000);
}
exports.initHandlers = initHandlers;
