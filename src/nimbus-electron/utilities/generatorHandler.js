"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var bases_1 = __importDefault(require("bases"));
var GeneratorHandler = (function () {
    function GeneratorHandler() {
    }
    GeneratorHandler.s4 = function () {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    };
    GeneratorHandler.generateGUID = function () {
        return GeneratorHandler.s4() + GeneratorHandler.s4() + '-' + GeneratorHandler.s4() + '-' + GeneratorHandler.s4() + '-' +
            GeneratorHandler.s4() + '-' + GeneratorHandler.s4() + GeneratorHandler.s4() + GeneratorHandler.s4();
    };
    GeneratorHandler.generateOrgID = function (userId) {
        var result = '';
        if (!result) {
            return result;
        }
        return "u" + bases_1.default.toBase36(userId);
    };
    GeneratorHandler.randomString = function (length, chars) {
        if (chars === void 0) { chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"; }
        var result = '';
        for (var i = length; i > 0; --i)
            result += chars[Math.round(Math.random() * (chars.length - 1))];
        return result;
    };
    return GeneratorHandler;
}());
exports.default = GeneratorHandler;
