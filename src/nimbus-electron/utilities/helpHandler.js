"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Translations_1 = __importDefault(require("../translations/Translations"));
function openHelpLink() {
    if (process.platform === 'darwin') {
        if (Translations_1.default.getLang() === 'ru') {
            require('electron').shell.openExternal('https://s.nimbusweb.me/share/3552429/vlrhr2qh828tt2i3izgb');
        }
        else {
            require('electron').shell.openExternal('https://s.nimbusweb.me/share/3552427/znds47abuxzypf005298');
        }
    }
    else {
        if (Translations_1.default.getLang() === 'ru') {
            require('electron').shell.openExternal('https://s.nimbusweb.me/share/3552430/mfepmzvcmx63mw134emw');
        }
        else {
            require('electron').shell.openExternal('https://s.nimbusweb.me/share/3552435/w7afdnq9x9dprpfayd9u');
        }
    }
}
exports.openHelpLink = openHelpLink;
function openEditorHelpLink() {
    if (Translations_1.default.getLang() === 'ru') {
        require('electron').shell.openExternal('https://s.nimbusweb.me/share/3547388/6p1il3z7xf767uldat0b');
    }
    else {
        require('electron').shell.openExternal('https://s.nimbusweb.me/share/3547021/d95s5pwex7lel0o2zssx');
    }
}
exports.openEditorHelpLink = openEditorHelpLink;
function openContactUsLink() {
    if (Translations_1.default.getLang() === 'ru') {
        require('electron').shell.openExternal('https://nimbusweb.me/ru/contact-us.php');
    }
    else {
        require('electron').shell.openExternal('https://nimbusweb.co/contact.php');
    }
}
exports.openContactUsLink = openContactUsLink;
function openSubmitBugLink() {
    if (Translations_1.default.getLang() === 'ru') {
        require('electron').shell.openExternal('https://nimbus.feedy.app/feedback/');
    }
    else {
        require('electron').shell.openExternal('https://support.nimbusweb.co/portal/community/nimbus-web-inc');
    }
}
exports.openSubmitBugLink = openSubmitBugLink;
