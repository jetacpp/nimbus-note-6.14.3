"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function setUpdateProps(saveProps, curDate) {
    for (var saveKey in saveProps) {
        if (!saveProps.hasOwnProperty(saveKey)) {
            continue;
        }
        if (saveKey && saveKey.indexOf('UpdateTime') > 0) {
            continue;
        }
        saveProps[saveKey + "UpdateTime"] = curDate;
    }
    return saveProps;
}
exports.setUpdateProps = setUpdateProps;
function propOutDate(props, existObj, fieldsUpdateDates) {
    var existNoteObjProp = props.existNoteObjProp, serverNoteObjProp = props.serverNoteObjProp;
    if (!existObj || !fieldsUpdateDates || !existNoteObjProp || !serverNoteObjProp) {
        return true;
    }
    var propName = existNoteObjProp + "UpdateTime";
    if (typeof (existObj[propName]) === 'undefined') {
        var noteFieldsUpdateDatesPropExist = true;
        if (!fieldsUpdateDates) {
            noteFieldsUpdateDatesPropExist = false;
        }
        if (noteFieldsUpdateDatesPropExist && typeof (fieldsUpdateDates[serverNoteObjProp]) === 'undefined') {
            noteFieldsUpdateDatesPropExist = false;
        }
        if (noteFieldsUpdateDatesPropExist && !fieldsUpdateDates[serverNoteObjProp]) {
            noteFieldsUpdateDatesPropExist = false;
        }
        if (noteFieldsUpdateDatesPropExist && typeof (fieldsUpdateDates[serverNoteObjProp]['updatedAt']) === 'undefined') {
            noteFieldsUpdateDatesPropExist = false;
        }
        if (noteFieldsUpdateDatesPropExist && !fieldsUpdateDates[serverNoteObjProp]['updatedAt']) {
            noteFieldsUpdateDatesPropExist = false;
        }
        return noteFieldsUpdateDatesPropExist;
    }
    var existPropDate = existObj[propName];
    if (!existPropDate) {
        return true;
    }
    if (typeof (fieldsUpdateDates[serverNoteObjProp]) === 'undefined') {
        return false;
    }
    if (typeof (fieldsUpdateDates[serverNoteObjProp]['updatedAt']) === 'undefined') {
        return false;
    }
    var serverPropDate = fieldsUpdateDates[serverNoteObjProp]['updatedAt'];
    if (!serverPropDate) {
        return false;
    }
    return existPropDate < serverPropDate;
}
exports.propOutDate = propOutDate;
function propPreviewOutDate(props, existObj, note) {
    var existNoteObjProp = props.existNoteObjProp;
    if (!existObj || !note || !existNoteObjProp) {
        return true;
    }
    var propName = existNoteObjProp + "UpdateTime";
    if (typeof (existObj[propName]) === 'undefined') {
        return true;
    }
    var existPropDate = existObj[propName];
    if (!existPropDate) {
        return true;
    }
    var serverPropDate = note.preview && note.preview.date_updated ? note.preview.date_updated : note.date_updated;
    if (!serverPropDate) {
        return true;
    }
    return existPropDate < serverPropDate;
}
exports.propPreviewOutDate = propPreviewOutDate;
function propReminderOutDate(props, existObj, note) {
    var existNoteObjProp = props.existNoteObjProp;
    if (!existObj || !note || !existNoteObjProp) {
        return true;
    }
    var propName = existNoteObjProp + "UpdateTime";
    if (typeof (existObj[propName]) === 'undefined') {
        return true;
    }
    var existPropDate = existObj[propName];
    if (!existPropDate) {
        return true;
    }
    var serverPropDate = note.date_updated;
    if (!serverPropDate) {
        return true;
    }
    return existPropDate < serverPropDate;
}
exports.propReminderOutDate = propReminderOutDate;
