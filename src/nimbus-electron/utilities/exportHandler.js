"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var workspace_1 = __importDefault(require("../db/models/workspace"));
var exportNote = __importStar(require("./export/exportNote"));
var compressNote = __importStar(require("./export/compressNote"));
var syncHandler_1 = __importDefault(require("./syncHandler"));
var Translations_1 = __importDefault(require("../translations/Translations"));
var path = __importStar(require("path"));
var find_1 = require("./export/utils/find");
var process_1 = require("./export/note/process");
var filenamify_1 = __importDefault(require("filenamify"));
var file_1 = require("./export/utils/file");
var instance_1 = __importDefault(require("../window/instance"));
var pdb_1 = __importDefault(require("../../pdb"));
var fs = require("fs-extra");
var EXPORT_TYPE;
(function (EXPORT_TYPE) {
    EXPORT_TYPE[EXPORT_TYPE["NOTE"] = 0] = "NOTE";
    EXPORT_TYPE[EXPORT_TYPE["FOLDER"] = 1] = "FOLDER";
})(EXPORT_TYPE = exports.EXPORT_TYPE || (exports.EXPORT_TYPE = {}));
var EXPORT_STATUS;
(function (EXPORT_STATUS) {
    EXPORT_STATUS[EXPORT_STATUS["START"] = 0] = "START";
    EXPORT_STATUS[EXPORT_STATUS["PROGRESS"] = 1] = "PROGRESS";
    EXPORT_STATUS[EXPORT_STATUS["FINISH"] = 2] = "FINISH";
    EXPORT_STATUS[EXPORT_STATUS["CANCEL"] = 3] = "CANCEL";
    EXPORT_STATUS[EXPORT_STATUS["ERROR"] = 4] = "ERROR";
})(EXPORT_STATUS = exports.EXPORT_STATUS || (exports.EXPORT_STATUS = {}));
var DEFAULT_TITLE = {
    'folder': 'New folder',
    'note': 'New note',
};
var activeExport = {};
exports.exportHandler = function (appWindow, payload) { return __awaiter(void 0, void 0, void 0, function () {
    var id, noteId, folderId, format, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = payload.id;
                noteId = payload.noteGlobalId;
                folderId = payload.folderGlobalId;
                format = payload.format;
                payload.extension = format === 'html' ? 'zip' : 'pdf';
                _a.label = 1;
            case 1:
                _a.trys.push([1, 6, , 7]);
                if (!noteId) return [3, 3];
                return [4, exportNoteHandler(appWindow, payload)];
            case 2:
                _a.sent();
                return [3, 5];
            case 3:
                if (!folderId) return [3, 5];
                return [4, exportFolderHandler(appWindow, payload)];
            case 4:
                _a.sent();
                _a.label = 5;
            case 5: return [3, 7];
            case 6:
                error_1 = _a.sent();
                syncHandler_1.default.sendLog(error_1.message);
                clearActiveExport(folderId);
                exports.setExportStatus(folderId, EXPORT_STATUS.ERROR);
                return [3, 7];
            case 7: return [2, id];
        }
    });
}); };
function exportFolderHandler(appWindow, payload) {
    return __awaiter(this, void 0, void 0, function () {
        var id, format, workspaceId, workspaceGlobalId, extension, folderId, folders, userTempPath, folder, title, _a, canceled, userSelectedPath, language, size, style, timezone, totalNotes, progress, exportDir, folderTitles, folderPaths, baseDirName, doneNotes, _i, folders_1, folder_1, folderName, folderPath, _b, folderDirName, folderDir, notes, noteTitles, _c, notes_1, note, noteName, noteId, textVersion, dir, dirName, outputPath, outputExist, error_2, error_3;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0:
                    id = payload.id;
                    format = payload.format;
                    workspaceId = payload.workspaceId;
                    return [4, workspace_1.default.getLocalId(payload.workspaceId)];
                case 1:
                    workspaceGlobalId = _d.sent();
                    extension = payload.extension;
                    folderId = payload.folderGlobalId;
                    folders = payload.folders ? Object.values(payload.folders) : [];
                    if (!payload.format || !payload.folderGlobalId) {
                        throw new Error("Problem to make export: bad payload provided");
                    }
                    userTempPath = pdb_1.default.getClientExportPath();
                    if (!(folderId === 'root')) return [3, 2];
                    folder = {
                        globalId: 'root',
                        parentId: null,
                        title: 'All Notes',
                        childrenNotesCount: 0,
                    };
                    return [3, 4];
                case 2: return [4, find_1.getFolder(workspaceGlobalId, folderId)];
                case 3:
                    folder = _d.sent();
                    _d.label = 4;
                case 4:
                    if (!folder) {
                        throw new Error("Problem to get export folder with folderId: " + folderId + " and workspaceId: " + workspaceGlobalId);
                    }
                    title = folder.title;
                    title = filenamify_1.default(title);
                    return [4, exportFolderSaveDialog(appWindow, { title: title })];
                case 5:
                    _a = _d.sent(), canceled = _a.canceled, userSelectedPath = _a.userSelectedPath;
                    if (canceled || !userSelectedPath) {
                        return [2, null];
                    }
                    language = payload.language;
                    size = payload.size;
                    style = payload.style;
                    timezone = payload.timezone;
                    totalNotes = folders.reduce(function (a, b) {
                        return a + (b.childrenNotesCount || 0);
                    }, 0);
                    progress = 0;
                    exports.setExportStatus(folderId, EXPORT_STATUS.PROGRESS);
                    exports.setExportProgress(folderId, totalNotes, progress);
                    return [4, process_1.makeTempDir(userTempPath, true)];
                case 6:
                    exportDir = _d.sent();
                    if (!exportDir) {
                        throw new Error("Problem: can not create export temp dir for path: " + userSelectedPath);
                    }
                    if (typeof activeExport[folderId] !== "undefined") {
                        clearActiveExport(folderId);
                    }
                    activeExport[folderId] = {
                        status: EXPORT_STATUS.PROGRESS,
                        interval: setInterval(function () {
                            exports.setExportProgress(folderId, totalNotes, progress);
                        }, 250),
                        path: exportDir,
                        readStream: null,
                        writeStream: null,
                    };
                    folderTitles = file_1.getUniqueTitles(folders, DEFAULT_TITLE.folder, extension);
                    folderPaths = file_1.getItemsPath(folders, folderTitles);
                    baseDirName = '';
                    doneNotes = 0;
                    _i = 0, folders_1 = folders;
                    _d.label = 7;
                case 7:
                    if (!(_i < folders_1.length)) return [3, 14];
                    folder_1 = folders_1[_i];
                    if (!exports.isExportActive(folderId)) {
                        return [2, null];
                    }
                    folderName = file_1.getUniqueTitle(folderTitles, folder_1);
                    if (!folderName) {
                        throw new Error("Problem: can not find export dir name: " + folderName + " for folder: " + folder_1.globalId + " with title: " + folder_1.title);
                    }
                    folderPath = file_1.getItemPath(folderPaths, folder_1);
                    if (!exports.isExportActive(folderId)) {
                        return [2, null];
                    }
                    return [4, process_1.makeDir(folderPath, exportDir)];
                case 8:
                    _b = _d.sent(), folderDirName = _b.destinationName, folderDir = _b.destinationPath;
                    if (folder_1.globalId === folderId) {
                        baseDirName = folderDirName;
                    }
                    if (!folderDir) {
                        throw new Error("Problem: can not create export dir: " + folderName + " for folder: " + folder_1.globalId + " with title: " + folder_1.title);
                    }
                    if (!exports.isExportActive(folderId)) {
                        return [2, null];
                    }
                    return [4, find_1.getFolderNotes(workspaceGlobalId, folder_1.globalId)];
                case 9:
                    notes = _d.sent();
                    noteTitles = file_1.getUniqueTitles(notes, DEFAULT_TITLE.note);
                    _c = 0, notes_1 = notes;
                    _d.label = 10;
                case 10:
                    if (!(_c < notes_1.length)) return [3, 13];
                    note = notes_1[_c];
                    if (!exports.isExportActive(folderId)) {
                        return [2, null];
                    }
                    noteName = file_1.getUniqueTitle(noteTitles, note);
                    if (!folderName) {
                        throw new Error("Problem: can not find export note name: " + noteName + " for note: " + note.globalId + " with title: " + note.title);
                    }
                    noteId = note.globalId;
                    textVersion = note.text_version;
                    if (!exports.isExportActive(folderId)) {
                        return [2, null];
                    }
                    return [4, processNote(appWindow, {
                            id: id,
                            saveFilePath: folderDir,
                            saveFileName: noteName + "." + extension,
                            workspaceId: workspaceId,
                            noteId: noteId,
                            format: format,
                            language: language,
                            size: size,
                            style: style,
                            timezone: timezone,
                            globalId: folderId,
                            exportType: EXPORT_TYPE.FOLDER,
                            textVersion: textVersion,
                            workspaceGlobalId: workspaceGlobalId,
                            folderExportDir: exportDir,
                        })];
                case 11:
                    dir = _d.sent();
                    if (!exports.isExportActive(folderId)) {
                        return [2, null];
                    }
                    if (!dir) {
                        throw new Error("Problem: processNote return empty dir for note name: " + noteName + " for note globalId: " + note.globalId);
                    }
                    doneNotes++;
                    progress = totalNotes ? Math.floor(doneNotes * 100 / totalNotes) : 100;
                    _d.label = 12;
                case 12:
                    _c++;
                    return [3, 10];
                case 13:
                    _i++;
                    return [3, 7];
                case 14:
                    if (!exports.isExportActive(folderId)) {
                        return [2, null];
                    }
                    clearActiveExport(folderId);
                    dirName = exportDir.split(path.sep).pop();
                    outputPath = path.join(userSelectedPath, dirName);
                    _d.label = 15;
                case 15:
                    _d.trys.push([15, 19, , 20]);
                    return [4, fs.exists(outputPath)];
                case 16:
                    outputExist = _d.sent();
                    if (!outputExist) return [3, 18];
                    syncHandler_1.default.sendLog("Clear exist " + outputPath);
                    return [4, exports.clearExportDir(outputPath)];
                case 17:
                    _d.sent();
                    _d.label = 18;
                case 18: return [3, 20];
                case 19:
                    error_2 = _d.sent();
                    syncHandler_1.default.sendLog("Problem: to clear " + outputPath + " - " + error_2.message);
                    syncHandler_1.default.sendLog(error_2);
                    return [3, 20];
                case 20:
                    syncHandler_1.default.sendLog("Move " + exportDir + " into " + outputPath);
                    _d.label = 21;
                case 21:
                    _d.trys.push([21, 23, , 24]);
                    return [4, fs.move(exportDir, outputPath)];
                case 22:
                    _d.sent();
                    return [3, 24];
                case 23:
                    error_3 = _d.sent();
                    syncHandler_1.default.sendLog("Problem: to move " + exportDir + " into " + outputPath + " - " + error_3.message);
                    exports.clearExportDir(exportDir);
                    return [3, 24];
                case 24:
                    exports.setExportProgress(folderId, totalNotes, 100, outputPath);
                    exports.setExportStatus(folderId, EXPORT_STATUS.FINISH);
                    return [2, exportDir];
            }
        });
    });
}
function exportNoteHandler(appWindow, payload) {
    return __awaiter(this, void 0, void 0, function () {
        var id, format, workspaceId, workspaceGlobalId, noteId, extension, note, _a, globalId, title, textVersion, _b, canceled, userSelectedPath, userTempPath, language, size, style, timezone, totalNotes, progress, noteName, saveFileName, exportPath, outputExist, error_4, error_5;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    id = payload.id;
                    format = payload.format;
                    workspaceId = payload.workspaceId;
                    return [4, workspace_1.default.getLocalId(payload.workspaceId)];
                case 1:
                    workspaceGlobalId = _c.sent();
                    noteId = payload.noteGlobalId;
                    extension = payload.extension;
                    if (!payload.format || !payload.noteGlobalId) {
                        throw new Error("Problem to make export: bad payload provided");
                    }
                    return [4, find_1.getNote(workspaceGlobalId, noteId)];
                case 2:
                    note = _c.sent();
                    if (!note) {
                        throw new Error("Problem to get export note for noteId: " + noteId + " and workspaceId: " + workspaceGlobalId);
                    }
                    _a = note, globalId = _a.globalId, title = _a.title, textVersion = _a.text_version;
                    title = file_1.clearTitle(title, DEFAULT_TITLE.note);
                    return [4, exportNoteSaveDialog(appWindow, { title: title, extension: extension })];
                case 3:
                    _b = _c.sent(), canceled = _b.canceled, userSelectedPath = _b.userSelectedPath;
                    if (canceled || !userSelectedPath) {
                        return [2, null];
                    }
                    userTempPath = pdb_1.default.getClientExportPath();
                    language = payload.language;
                    size = payload.size;
                    style = payload.style;
                    timezone = payload.timezone;
                    totalNotes = 1;
                    progress = 0;
                    exports.setExportStatus(noteId, EXPORT_STATUS.PROGRESS);
                    exports.setExportProgress(noteId, totalNotes, 0, '');
                    if (typeof activeExport[noteId] !== "undefined") {
                        clearActiveExport(noteId);
                    }
                    activeExport[noteId] = {
                        status: EXPORT_STATUS.PROGRESS,
                        interval: setInterval(function () {
                            exports.setExportProgress(noteId, totalNotes, progress);
                        }, 250),
                        readStream: null,
                        writeStream: null,
                    };
                    if (!exports.isExportActive(noteId)) {
                        return [2, null];
                    }
                    noteName = userSelectedPath.split(path.sep).pop();
                    if (!noteName) {
                        noteName = file_1.getExportFileName(title);
                    }
                    saveFileName = noteName + "." + extension;
                    return [4, processNote(appWindow, {
                            id: id,
                            saveFilePath: userTempPath,
                            saveFileName: saveFileName,
                            workspaceId: workspaceId,
                            noteId: noteId,
                            format: format,
                            language: language,
                            size: size,
                            style: style,
                            timezone: timezone,
                            globalId: noteId,
                            exportType: EXPORT_TYPE.NOTE,
                            textVersion: textVersion,
                            workspaceGlobalId: workspaceGlobalId,
                        })];
                case 4:
                    exportPath = _c.sent();
                    if (!exports.isExportActive(noteId, exportPath)) {
                        return [2, null];
                    }
                    if (!exportPath) {
                        exports.clearExportDir(exportPath);
                        throw new Error("Problem: processNote return empty dir for for note name: " + title + " with note globalId: " + globalId);
                    }
                    clearActiveExport(noteId);
                    _c.label = 5;
                case 5:
                    _c.trys.push([5, 9, , 10]);
                    return [4, fs.exists(userSelectedPath)];
                case 6:
                    outputExist = _c.sent();
                    if (!outputExist) return [3, 8];
                    syncHandler_1.default.sendLog("Clear exist " + userSelectedPath);
                    return [4, fs.unlink(userSelectedPath)];
                case 7:
                    _c.sent();
                    _c.label = 8;
                case 8: return [3, 10];
                case 9:
                    error_4 = _c.sent();
                    syncHandler_1.default.sendLog("Problem: to clear " + userSelectedPath + " - " + error_4.message);
                    syncHandler_1.default.sendLog(error_4);
                    return [3, 10];
                case 10:
                    syncHandler_1.default.sendLog("Move " + exportPath + " into " + userSelectedPath);
                    _c.label = 11;
                case 11:
                    _c.trys.push([11, 13, , 14]);
                    return [4, fs.move(exportPath, userSelectedPath)];
                case 12:
                    _c.sent();
                    return [3, 14];
                case 13:
                    error_5 = _c.sent();
                    syncHandler_1.default.sendLog("Problem: to move " + exportPath + " into " + userSelectedPath + " - " + error_5.message);
                    syncHandler_1.default.sendLog(error_5);
                    fs.unlink(exportPath);
                    return [3, 14];
                case 14:
                    exports.setExportProgress(noteId, totalNotes, 100, userSelectedPath);
                    exports.setExportStatus(noteId, EXPORT_STATUS.FINISH);
                    return [2, exportPath];
            }
        });
    });
}
function processNote(appWindow, payload) {
    return __awaiter(this, void 0, void 0, function () {
        var id, saveFilePath, saveFileName, workspaceId, noteId, format, language, size, style, timezone, globalId, exportType, textVersion, workspaceGlobalId, folderExportDir, exportDir, exportBaseDir, workerData, dir, compressData, outputFile;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    id = payload.id, saveFilePath = payload.saveFilePath, saveFileName = payload.saveFileName, workspaceId = payload.workspaceId, noteId = payload.noteId, format = payload.format, language = payload.language, size = payload.size, style = payload.style, timezone = payload.timezone, globalId = payload.globalId, exportType = payload.exportType, textVersion = payload.textVersion, workspaceGlobalId = payload.workspaceGlobalId, folderExportDir = payload.folderExportDir;
                    return [4, process_1.makeTempDir(saveFilePath)];
                case 1:
                    exportDir = _a.sent();
                    if (!exportDir) {
                        throw new Error("Problem: can not create export temp dir for path: " + saveFilePath);
                    }
                    exportBaseDir = exportType === EXPORT_TYPE.NOTE ? exportDir : '';
                    if (!exports.isExportActive(globalId, exportBaseDir)) {
                        return [2, null];
                    }
                    return [4, exportNote.run({
                            id: id,
                            dir: exportDir,
                            workspaceId: workspaceId,
                            noteId: noteId,
                            format: format,
                            language: language,
                            size: size,
                            style: style,
                            timezone: timezone,
                            globalId: globalId,
                            exportType: exportType,
                            textVersion: textVersion,
                            workspaceGlobalId: workspaceGlobalId,
                        })];
                case 2:
                    workerData = _a.sent();
                    if (!exports.isExportActive(globalId, exportBaseDir)) {
                        return [2, null];
                    }
                    if (!workerData) {
                        throw new Error("Problem to generate html container for export with workspaceId: " + workspaceId + " and noteId: " + noteId + " and format: " + format);
                    }
                    dir = workerData.dir;
                    if (!dir) {
                        throw new Error("Problem to get dir from workerData for noteId: " + noteId);
                    }
                    if (exportType === EXPORT_TYPE.NOTE) {
                        syncHandler_1.default.sendLog("Export dir: " + dir);
                    }
                    return [4, compressNote.run(__assign(__assign({}, workerData), { saveFilePath: saveFilePath,
                            saveFileName: saveFileName,
                            format: format,
                            globalId: globalId, folderExportDir: folderExportDir ? folderExportDir : exportDir }))];
                case 3:
                    compressData = _a.sent();
                    if (!exports.isExportActive(globalId)) {
                        return [2, null];
                    }
                    if (!compressData) {
                        exports.clearExportDir(dir);
                        throw new Error("Problem to compress container for export with workspaceId: " + workspaceId + " and noteId: " + noteId + " and format: " + format);
                    }
                    outputFile = compressData.outputFile;
                    return [4, exports.clearExportDir(dir)];
                case 4:
                    _a.sent();
                    return [2, outputFile];
            }
        });
    });
}
var exportNoteSaveDialog = function (appWindow, options) { return __awaiter(void 0, void 0, void 0, function () {
    var title, extension, filePath, saveFilePath, _a, canceled, userSelectedPath;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                title = options.title, extension = options.extension;
                filePath = extension ? filenamify_1.default(title) + "." + extension : filenamify_1.default(title);
                saveFilePath = electron_1.app.getPath('documents') + "/" + filePath;
                return [4, electron_1.dialog.showSaveDialog(appWindow, {
                        title: "" + Translations_1.default.get('export_notes_dialog'),
                        defaultPath: saveFilePath,
                        filters: [
                            { name: extension, extensions: [extension] },
                            { name: 'All Files', extensions: ['*'] },
                        ],
                        showsTagField: false,
                        properties: ['createDirectory']
                    })];
            case 1:
                _a = _b.sent(), canceled = _a.canceled, userSelectedPath = _a.filePath;
                return [2, { canceled: canceled, userSelectedPath: userSelectedPath }];
        }
    });
}); };
var exportFolderSaveDialog = function (appWindow, options) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, canceled, filePaths, userSelectedPath;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0: return [4, electron_1.dialog.showOpenDialog(appWindow, {
                    title: "" + Translations_1.default.get('export_folder_dialog'),
                    properties: ['openDirectory', 'createDirectory']
                })];
            case 1:
                _a = _b.sent(), canceled = _a.canceled, filePaths = _a.filePaths;
                userSelectedPath = filePaths.length ? filePaths[0] : '';
                return [2, { canceled: canceled, userSelectedPath: userSelectedPath }];
        }
    });
}); };
exports.setExportStatus = function (globalId, status) {
    if (instance_1.default.get()) {
        instance_1.default.get().webContents.send('event:export:set:status:requests', { globalId: globalId, status: status });
    }
};
exports.setExportProgress = function (globalId, total, progress, path) {
    if (path === void 0) { path = ''; }
    if (instance_1.default.get()) {
        instance_1.default.get().webContents.send('event:export:set:progress:requests', {
            globalId: globalId,
            total: total,
            progress: progress,
            path: path,
        });
    }
};
var clearActiveExport = function (globalId) {
    if (typeof activeExport[globalId] === 'undefined') {
        return;
    }
    clearInterval(activeExport[globalId].interval);
    if (activeExport[globalId].readStream) {
        syncHandler_1.default.sendLog('=> close export read stream');
        activeExport[globalId].readStream.close();
    }
    if (activeExport[globalId].writeStream) {
        syncHandler_1.default.sendLog('=> close export write stream');
        activeExport[globalId].writeStream.close();
    }
    if (activeExport[globalId].compressReadStream) {
        syncHandler_1.default.sendLog('=> close export compress read stream');
        var compressReadStreamPath = activeExport[globalId].compressReadStreamPath;
        activeExport[globalId].compressReadStream.abort();
        if (compressReadStreamPath) {
            try {
                exports.clearExportDir(compressReadStreamPath);
            }
            catch (e) { }
        }
    }
    if (activeExport[globalId].compressWriteStream) {
        syncHandler_1.default.sendLog('=> close export compress write stream');
        var compressWritePath = activeExport[globalId].compressWriteStreamPath;
        activeExport[globalId].compressWriteStream.close();
        if (compressWritePath) {
            try {
                exports.clearExportDir(compressWritePath);
            }
            catch (e) { }
        }
        var folderExportDir = activeExport[globalId].compressFolderExportDir;
        if (folderExportDir) {
            try {
                exports.clearExportDir(folderExportDir);
            }
            catch (e) { }
        }
    }
    exports.clearActiveExportStream(globalId);
    exports.clearActiveCompressStream(globalId);
    delete activeExport[globalId];
};
exports.setActiveExportStream = function (globalId, readStream, writeStream) {
    if (typeof activeExport[globalId] === 'undefined') {
        return;
    }
    activeExport[globalId].readStream = readStream;
    activeExport[globalId].writeStream = writeStream;
};
exports.clearActiveExportStream = function (globalId) {
    if (typeof activeExport[globalId] === 'undefined') {
        return;
    }
    activeExport[globalId].readStream = null;
    activeExport[globalId].writeStream = null;
};
exports.setActiveCompressStream = function (globalId, readStream, readStreamPath, writeStream, writeStreamPath, folderExportDir) {
    if (typeof activeExport[globalId] === 'undefined') {
        return;
    }
    activeExport[globalId].compressReadStream = readStream;
    activeExport[globalId].compressReadStreamPath = readStreamPath;
    activeExport[globalId].compressWriteStream = writeStream;
    activeExport[globalId].compressWriteStreamPath = writeStreamPath;
    if (folderExportDir) {
        activeExport[globalId].compressFolderExportDir = folderExportDir;
    }
};
exports.clearActiveCompressStream = function (globalId) {
    if (typeof activeExport[globalId] === 'undefined') {
        return;
    }
    activeExport[globalId].compressReadStream = null;
    activeExport[globalId].compressWriteStream = null;
    activeExport[globalId].compressReadStreamPath = null;
    activeExport[globalId].compressWriteStreamPath = null;
    activeExport[globalId].compressFolderExportDir = null;
};
exports.isExportActive = function (globalId, dir) {
    if (dir === void 0) { dir = ''; }
    if (typeof activeExport[globalId] === 'undefined') {
        return false;
    }
    var _a = activeExport[globalId], cancel = _a.cancel, path = _a.path;
    if (cancel) {
        clearActiveExport(globalId);
        var exportDir_1 = dir || path;
        if (exportDir_1) {
            setTimeout(function () { return exports.clearExportDir(exportDir_1); }, 300);
        }
    }
    return !cancel;
};
exports.exportCancel = function (globalId) {
    if (!globalId) {
        return;
    }
    if (typeof activeExport[globalId] === 'undefined') {
        return;
    }
    activeExport[globalId].cancel = true;
};
exports.exportOpenPath = function (path) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4, electron_1.shell.showItemInFolder(path)];
            case 1:
                _a.sent();
                return [2];
        }
    });
}); };
exports.clearExportDir = function (dir) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4, file_1.removePath(dir)];
            case 1:
                _a.sent();
                return [2];
        }
    });
}); };
