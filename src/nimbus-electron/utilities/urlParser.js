"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var parsed = require("url-parse");
var UrlParser = (function () {
    function UrlParser() {
    }
    UrlParser.getDetails = function (url) {
        return parsed(url.trim().replace(/\+/gi, " ").replace(/%2B/gi, "+"), true, null);
    };
    UrlParser.getAttachmentRemoteUri = function (url) {
        if (!url) {
            return "";
        }
        var lastSlashIndex = url.lastIndexOf("/");
        var path = url.slice(0, lastSlashIndex + 1);
        if (!path) {
            return "";
        }
        var fileName = url.slice(lastSlashIndex + 1);
        if (!fileName) {
            return "";
        }
        return path + encodeURIComponent(fileName);
    };
    UrlParser.escapeRegExp = function (str) {
        return str.trim().replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    };
    UrlParser.getQueryParams = function (url) {
        var urlData = UrlParser.getDetails(url);
        return urlData && urlData.query ? urlData.query : {};
    };
    UrlParser.getFilterParams = function (url) {
        var result = {};
        var query = UrlParser.getQueryParams(url);
        if (query && Object.keys(query).length && query.filter) {
            var filterQuery = JSON.parse(query.filter);
            if (filterQuery && Object.keys(filterQuery).length)
                result = filterQuery;
        }
        return result;
    };
    UrlParser.getLimitParams = function (url) {
        var result = {};
        var query = UrlParser.getQueryParams(url);
        if (query && Object.keys(query).length && query.range) {
            var rangeQuery = JSON.parse(query.range);
            if (rangeQuery && Object.keys(rangeQuery).length)
                result = rangeQuery;
        }
        return result;
    };
    UrlParser.getOrderParams = function (url) {
        var result = {};
        var query = UrlParser.getQueryParams(url);
        if (!query) {
            return result;
        }
        if (!Object.keys(query).length) {
            return result;
        }
        if (!query.order) {
            return result;
        }
        var orderQuery = JSON.parse(query.order);
        if (!orderQuery) {
            return result;
        }
        if ((orderQuery) instanceof Array) {
            if (!orderQuery.length) {
                return result;
            }
            var orderParams = orderQuery[0];
            if (!orderParams.length) {
                return result;
            }
            return orderParams;
        }
        else if ((orderQuery) instanceof Object) {
            if (!Object.keys(orderQuery).length) {
                return result;
            }
            result = orderQuery;
        }
        return result;
    };
    UrlParser.getPathParams = function (url) {
        var urlData = UrlParser.getDetails(url);
        return urlData.pathname.split('/');
    };
    UrlParser.getSortAndLimitParams = function (options) {
        options = options || {};
        var orderField = "";
        var orderDirection = 1;
        var offsetValue = 0;
        var limitValue = 0;
        if (options.order instanceof Array) {
            if (typeof (options.order[0]) !== "undefined" && options.order[0])
                orderField = options.order[0];
            if (typeof (options.order[1]) !== "undefined" && options.order[1] === "DESC")
                orderDirection = -1;
        }
        if (options.range) {
            if (typeof (options.range.offset) !== "undefined" && options.range.offset > 0)
                offsetValue = options.range.offset;
            if (typeof (options.range.limit) !== "undefined" && options.range.limit > 0)
                limitValue = options.range.limit;
        }
        var sortQuery = {};
        if (orderField && orderDirection)
            sortQuery[orderField] = orderDirection;
        return {
            'orderField': orderField,
            'orderDirection': orderDirection,
            'offsetValue': offsetValue,
            'limitValue': limitValue,
            'sortQuery': sortQuery
        };
    };
    UrlParser.getWorkspaceIdFromUrl = function (url) {
        if (!url) {
            return null;
        }
        var data = url.split('/');
        return typeof (data[5]) !== 'undefined' && data[5] ? data[5] : null;
    };
    return UrlParser;
}());
exports.default = UrlParser;
