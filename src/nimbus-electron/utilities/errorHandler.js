"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var fs = require("fs-extra");
var util = require("util");
var Sentry = __importStar(require("@sentry/electron"));
var readLastLines = __importStar(require("read-last-lines"));
var config_1 = __importDefault(require("../../config"));
var NimbusSDK_1 = __importDefault(require("../sync/nimbussdk/net/NimbusSDK"));
var instance_1 = __importDefault(require("../window/instance"));
var dateHandler_1 = __importDefault(require("./dateHandler"));
var Translations_1 = __importDefault(require("../translations/Translations"));
var instance_2 = __importDefault(require("../window/instance"));
var pdb_1 = __importDefault(require("../../pdb"));
var API_1 = __importDefault(require("../sync/nimbussdk/net/API"));
var logStream;
var stopSaveLog;
var ErrorHandler = (function () {
    function ErrorHandler() {
    }
    ErrorHandler.log = function (err) {
        if (err) {
            if (config_1.default.SHOW_WEB_CONSOLE) {
                console.log(err);
            }
        }
        var appWindow = instance_1.default.get();
        if (appWindow && appWindow.webContents) {
            appWindow.webContents.send('nimbus:electron:log', err);
        }
        if (stopSaveLog) {
            return;
        }
        ErrorHandler.writeSessionLogsToFile(err);
    };
    ErrorHandler.writeSessionLogsToFile = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var targetPath, dateTime, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!data) {
                            return [2];
                        }
                        if (typeof (data) === "object") {
                            data = JSON.stringify(data);
                        }
                        if (!pdb_1.default.getClientAttachmentPath()) {
                            return [2];
                        }
                        targetPath = pdb_1.default.getClientAttachmentPath() + "/" + config_1.default.logsReportName;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, fs.exists(targetPath)];
                    case 2:
                        if (!(_a.sent())) {
                            if (logStream) {
                                logStream.end();
                            }
                            logStream = fs.createWriteStream(targetPath, { flags: 'a' });
                        }
                        if (!logStream) {
                            logStream = fs.createWriteStream(targetPath, { flags: 'a' });
                        }
                        dateTime = dateHandler_1.default.dateYmdHis();
                        logStream.write(dateTime + '  ' + util.format(data) + '\n');
                        return [3, 4];
                    case 3:
                        e_1 = _a.sent();
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("Problem to write into log file", e_1);
                        }
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    ErrorHandler.clearSessionLogsStream = function () {
        return __awaiter(this, void 0, void 0, function () {
            var targetPath, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ErrorHandler.stopSyncLog();
                        targetPath = pdb_1.default.getClientAttachmentPath() + "/" + config_1.default.logsReportName;
                        return [4, fs.exists(targetPath)];
                    case 1:
                        if (!_a.sent()) return [3, 5];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4, fs.unlink(targetPath)];
                    case 3:
                        _a.sent();
                        return [3, 5];
                    case 4:
                        e_2 = _a.sent();
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("Error => clearSessionLogsStream => remove file: ", targetPath, e_2);
                        }
                        return [3, 5];
                    case 5: return [2];
                }
            });
        });
    };
    ErrorHandler.stopSyncLog = function () {
        stopSaveLog = true;
        setTimeout(function () { return stopSaveLog = false; }, 5000);
    };
    ErrorHandler.sendSessionLogsToNimbus = function () {
        var _this = this;
        NimbusSDK_1.default.getApi().userLogin(function (err, login) { return __awaiter(_this, void 0, void 0, function () {
            var appWindow, targetPath, targetExist, email;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!pdb_1.default.getClientAttachmentPath()) {
                            return [2];
                        }
                        appWindow = instance_1.default.get();
                        targetPath = pdb_1.default.getClientAttachmentPath() + "/" + config_1.default.logsReportName;
                        return [4, fs.exists(targetPath)];
                    case 1:
                        targetExist = _a.sent();
                        if (appWindow && appWindow.webContents) {
                            appWindow.webContents.send('nimbus:electron:log', {
                                "logs:path": targetPath,
                                "exist": targetExist
                            });
                        }
                        if (!targetExist) {
                            return [2];
                        }
                        email = login ? login : "not authorized";
                        try {
                            readLastLines.read(targetPath, 100)
                                .then(function (lines) {
                                Sentry.configureScope(function (scope) {
                                    scope.clear();
                                    scope.setTag('event_id', 'customer:log');
                                    Sentry.captureMessage("Electron logs from customer: " + email);
                                    var linesData = lines.split('\n');
                                    for (var _i = 0, linesData_1 = linesData; _i < linesData_1.length; _i++) {
                                        var line = linesData_1[_i];
                                        Sentry.addBreadcrumb({
                                            category: 'log',
                                            message: line,
                                            level: Sentry.Severity.Info
                                        });
                                    }
                                });
                            })
                                .catch(function (e) {
                                if (config_1.default.SHOW_WEB_CONSOLE) {
                                    console.log("Error => sendSessionLogsToNimbus => read logs file: ", targetPath, e);
                                }
                            });
                        }
                        catch (e) {
                            if (config_1.default.SHOW_WEB_CONSOLE) {
                                console.log("Error => sendSessionLogsToNimbus => read logs file: ", targetPath, e);
                            }
                        }
                        return [2];
                }
            });
        }); });
    };
    ErrorHandler.requestSendSessionLogs = function () {
        var dialogOptions = {
            type: "question",
            title: electron_1.app.name,
            message: "" + Translations_1.default.get('send_logs__confirm_message'),
            detail: "" + Translations_1.default.get('send_logs__confirm_details'),
            buttons: [
                "" + Translations_1.default.get('send_logs__confirm_btn_cancel'),
                "" + Translations_1.default.get('send_logs__confirm_btn_submit')
            ],
            defaultId: 1,
            cancelId: 0
        };
        if (instance_2.default.get()) {
            electron_1.dialog.showMessageBox(instance_2.default.get(), dialogOptions).then(function (_a) {
                var response = _a.response;
                if (response) {
                    ErrorHandler.sendSessionLogsToNimbus();
                }
            });
        }
    };
    ErrorHandler.displayErrorPopup = function (inputData) {
        var err = inputData.err, response = inputData.response;
        if (err === API_1.default.ERROR_ACCESS_DENIED) {
            if (instance_2.default.get()) {
                instance_2.default.get().webContents.send('event:client:workspace:message:response', {
                    message: Translations_1.default.get('toast__can_not_sync_access_denied'),
                });
            }
        }
        else if (response && err === API_1.default.ERROR_QUOTA_EXCEED) {
            var translationKey = '';
            var body = response.body;
            if (!body) {
                return;
            }
            var _errorDesc = body._errorDesc;
            if (_errorDesc) {
                switch (_errorDesc) {
                    case API_1.default.DESC_TOO_MANY_NOTES: {
                        translationKey = API_1.default.DESC_TOO_MANY_NOTES;
                        break;
                    }
                }
            }
            if (translationKey) {
                instance_2.default.get().webContents.send('event:client:popup:too_many_notes:response', {});
            }
        }
    };
    return ErrorHandler;
}());
exports.default = ErrorHandler;
