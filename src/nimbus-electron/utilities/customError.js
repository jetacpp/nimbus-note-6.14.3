"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_runtime_1 = __importDefault(require("../../config.runtime"));
var CustomError = (function () {
    function CustomError() {
    }
    CustomError.noAuth = function (errorMessage) {
        if (errorMessage === void 0) { errorMessage = ""; }
        return CustomError.getError("User not authorized", errorMessage);
    };
    CustomError.wrongInput = function (errorMessage) {
        if (errorMessage === void 0) { errorMessage = ""; }
        return CustomError.getError("Wrong input data", errorMessage);
    };
    CustomError.itemNotFound = function (errorMessage) {
        if (errorMessage === void 0) { errorMessage = ""; }
        return CustomError.getError("Model item not found", errorMessage);
    };
    CustomError.itemFindError = function (errorMessage) {
        if (errorMessage === void 0) { errorMessage = ""; }
        return CustomError.getError("Model item find problem", errorMessage);
    };
    CustomError.itemFindAllError = function (errorMessage) {
        if (errorMessage === void 0) { errorMessage = ""; }
        return CustomError.getError("Model item find all problem", errorMessage);
    };
    CustomError.noDbConnection = function (errorMessage) {
        if (errorMessage === void 0) { errorMessage = ""; }
        return CustomError.getError("No database connection", errorMessage);
    };
    CustomError.itemSaveError = function (errorMessage) {
        if (errorMessage === void 0) { errorMessage = ""; }
        return CustomError.getError("Model item save problem", errorMessage);
    };
    CustomError.itemUpdateError = function (errorMessage) {
        if (errorMessage === void 0) { errorMessage = ""; }
        return CustomError.getError("Model item update problem", errorMessage);
    };
    CustomError.itemRemoveError = function (errorMessage) {
        if (errorMessage === void 0) { errorMessage = ""; }
        return CustomError.getError("Model item remove problem", errorMessage);
    };
    CustomError.wrongAttachPath = function (errorMessage) {
        if (errorMessage === void 0) { errorMessage = ""; }
        return CustomError.getError("Wrong attach path", errorMessage);
    };
    CustomError.noError = function () {
        return null;
    };
    CustomError.getError = function (errorPrefix, errorMessage) {
        if (errorPrefix === void 0) { errorPrefix = ""; }
        if (errorMessage === void 0) { errorMessage = ""; }
        var text = errorMessage ? errorPrefix + ": " + errorMessage : errorPrefix;
        var error = new Error(text);
        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
            console.log(error);
        }
        return error;
    };
    return CustomError;
}());
exports.default = CustomError;
