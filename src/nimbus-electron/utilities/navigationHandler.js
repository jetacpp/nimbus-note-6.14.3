"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var auth_1 = __importDefault(require("../auth/auth"));
var config_runtime_1 = __importDefault(require("../../config.runtime"));
var workspace_1 = __importDefault(require("../db/models/workspace"));
var urlParser_1 = __importDefault(require("./urlParser"));
var SelectedOrganization_1 = __importDefault(require("../organization/SelectedOrganization"));
var instance_1 = __importDefault(require("../window/instance"));
function openLink(url) {
    if (!url) {
        return;
    }
    electron_1.shell.openExternal(url);
}
exports.openLink = openLink;
function getNavigationUrlDetails(url) {
    return __awaiter(this, void 0, void 0, function () {
        var authInfo, defaultNavigation, personalOrgNavigation, workspacesList, organizations, _i, organizations_1, organization, globalId, domain, sub, electronHost, businessNavigation;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, auth_1.default.getUserAsync()];
                case 1:
                    authInfo = _a.sent();
                    defaultNavigation = {
                        notePageHost: '',
                        organizationId: null,
                        workspaceId: null,
                        reloadPage: false,
                    };
                    return [4, parseNavigationUrlByOrgHost({
                            url: url,
                            host: config_runtime_1.default.WEBNOTES_DOMAIN,
                            orgId: null,
                            defaultNavigation: defaultNavigation,
                        })];
                case 2:
                    personalOrgNavigation = _a.sent();
                    if (personalOrgNavigation.notePageHost) {
                        return [2, personalOrgNavigation];
                    }
                    return [4, workspace_1.default.findUserWorkspaces()];
                case 3:
                    workspacesList = _a.sent();
                    return [4, SelectedOrganization_1.default.getUserOrganizationsDetailsByWorkspaces(authInfo, workspacesList)];
                case 4:
                    organizations = (_a.sent()).organizations;
                    _i = 0, organizations_1 = organizations;
                    _a.label = 5;
                case 5:
                    if (!(_i < organizations_1.length)) return [3, 11];
                    organization = organizations_1[_i];
                    globalId = organization.globalId, domain = organization.domain, sub = organization.sub, electronHost = organization.electronHost;
                    if (!electronHost || electronHost === config_runtime_1.default.WEBNOTES_DOMAIN) {
                        return [3, 10];
                    }
                    businessNavigation = defaultNavigation;
                    if (!domain) return [3, 7];
                    return [4, parseNavigationUrlByOrgHost({
                            url: url,
                            host: domain,
                            orgId: globalId,
                            defaultNavigation: defaultNavigation,
                        })];
                case 6:
                    businessNavigation = _a.sent();
                    _a.label = 7;
                case 7:
                    if (!sub) return [3, 9];
                    return [4, parseNavigationUrlByOrgHost({
                            url: url,
                            host: organization.sub + "." + config_runtime_1.default.authApiServiceDomain,
                            orgId: globalId,
                            defaultNavigation: defaultNavigation,
                        })];
                case 8:
                    businessNavigation = _a.sent();
                    _a.label = 9;
                case 9:
                    if (businessNavigation.notePageHost) {
                        return [2, businessNavigation];
                    }
                    _a.label = 10;
                case 10:
                    _i++;
                    return [3, 5];
                case 11: return [2, defaultNavigation];
            }
        });
    });
}
exports.getNavigationUrlDetails = getNavigationUrlDetails;
function parseNavigationUrlByOrgHost(_a) {
    var url = _a.url, host = _a.host, orgId = _a.orgId, defaultNavigation = _a.defaultNavigation;
    return __awaiter(this, void 0, void 0, function () {
        var notePageHost, organizationId, workspaceId, reloadPage, searchHostHttp, searchHostHttps;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    notePageHost = defaultNavigation.notePageHost, organizationId = defaultNavigation.organizationId, workspaceId = defaultNavigation.workspaceId, reloadPage = defaultNavigation.reloadPage;
                    if (!url || !host) {
                        return [2, defaultNavigation];
                    }
                    if (typeof (notePageHost) === 'undefined') {
                        notePageHost = '';
                    }
                    if (typeof (organizationId) === 'undefined') {
                        organizationId = null;
                    }
                    if (typeof (workspaceId) === 'undefined') {
                        workspaceId = null;
                    }
                    if (typeof (workspaceId) === 'undefined') {
                        reloadPage = false;
                    }
                    searchHostHttp = "http://" + host;
                    searchHostHttps = "https://" + host;
                    if (url.indexOf(searchHostHttp + "/ws/") >= 0) {
                        notePageHost = searchHostHttp;
                    }
                    if (url.indexOf(searchHostHttps + "/ws/") >= 0) {
                        notePageHost = searchHostHttps;
                    }
                    if (!notePageHost) return [3, 3];
                    organizationId = orgId;
                    return [4, workspace_1.default.getLocalId(urlParser_1.default.getWorkspaceIdFromUrl(url))];
                case 1:
                    workspaceId = _b.sent();
                    return [4, SelectedOrganization_1.default.set(organizationId, workspaceId)];
                case 2:
                    reloadPage = _b.sent();
                    _b.label = 3;
                case 3: return [2, {
                        notePageHost: notePageHost,
                        organizationId: organizationId,
                        workspaceId: workspaceId,
                        reloadPage: reloadPage,
                    }];
            }
        });
    });
}
exports.parseNavigationUrlByOrgHost = parseNavigationUrlByOrgHost;
function requestInternalNoteNavigation(url) {
    if (instance_1.default.get()) {
        return instance_1.default.get().webContents.send('event:navigate:to:internal:note:requests', {
            url: url
        });
    }
}
exports.requestInternalNoteNavigation = requestInternalNoteNavigation;
