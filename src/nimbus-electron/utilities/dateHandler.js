"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DateHandler = (function () {
    function DateHandler() {
    }
    DateHandler.now = function (date) {
        if (date === void 0) { date = null; }
        return Math.floor((date ? new Date(date) : new Date()).getTime() / 1000);
    };
    DateHandler.msec = function (date) {
        if (date === void 0) { date = null; }
        return Math.floor((date ? new Date(date) : new Date()).getTime());
    };
    DateHandler.dateYmdHis = function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        var hh = today.getHours();
        var mn = today.getMinutes();
        var ss = today.getSeconds();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        if (hh < 10) {
            hh = '0' + hh;
        }
        if (mn < 10) {
            mn = '0' + mn;
        }
        if (ss < 10) {
            ss = '0' + ss;
        }
        return yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + mn + ':' + ss;
    };
    DateHandler.setBigTimeout = function (callback, delay) {
        var maxDelay = Math.pow(2, 31) - 1;
        if (delay > maxDelay) {
            var args_1 = arguments;
            args_1[1] -= maxDelay;
            return setTimeout(function () {
                DateHandler.setBigTimeout.apply(undefined, args_1);
            }, maxDelay);
        }
        return setTimeout.apply(undefined, arguments);
    };
    return DateHandler;
}());
exports.default = DateHandler;
