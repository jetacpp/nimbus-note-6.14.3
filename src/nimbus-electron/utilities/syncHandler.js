"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../config"));
var instance_1 = __importDefault(require("../window/instance"));
var logs = [];
var syncHandler = (function () {
    function syncHandler() {
    }
    syncHandler.init = function () {
        logs = [];
    };
    syncHandler.getLogs = function () {
        return logs;
    };
    syncHandler.setLog = function (log) {
        logs.push({
            date: new Date().toISOString(),
            log: log,
        });
    };
    syncHandler.showLogs = function () {
        var appWindow = instance_1.default.get();
        if (appWindow && appWindow.webContents) {
            appWindow.webContents.send('nimbus:electron:log', 'Last sync logs:');
            appWindow.webContents.send('nimbus:electron:log', syncHandler.getLogs());
        }
    };
    syncHandler.log = function (log, payload) {
        if (payload === void 0) { payload = {}; }
        if (!log) {
            return;
        }
        if (config_1.default.SHOW_WEB_CONSOLE) {
            console.log(log);
        }
        syncHandler.setLog(log);
    };
    syncHandler.sendLog = function (log) {
        var appWindow = instance_1.default.get();
        if (appWindow && appWindow.webContents) {
            appWindow.webContents.send('nimbus:electron:log', log);
        }
        if (config_1.default.SHOW_WEB_CONSOLE) {
            console.log(log);
        }
    };
    return syncHandler;
}());
exports.default = syncHandler;
