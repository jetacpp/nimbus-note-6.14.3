"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var workspace_1 = __importDefault(require("../../../db/models/workspace"));
var orgs_1 = __importDefault(require("../../../db/models/orgs"));
var item_1 = __importDefault(require("../../../db/models/item"));
var attach_1 = __importDefault(require("../../../db/models/attach"));
function getWorkspace(workspaceId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!workspaceId) return [3, 2];
                    return [4, workspace_1.default.getById(workspaceId)];
                case 1: return [2, _a.sent()];
                case 2: return [4, workspace_1.default.getDefault()];
                case 3: return [2, _a.sent()];
            }
        });
    });
}
exports.getWorkspace = getWorkspace;
function getOrg(orgId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, orgs_1.default.getById(orgId)];
                case 1: return [2, _a.sent()];
            }
        });
    });
}
exports.getOrg = getOrg;
function getNote(workspaceId, globalId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2, new Promise(function (resolve) {
                    item_1.default.find({ globalId: globalId, type: 'note' }, { workspaceId: workspaceId }, function (err, itemInstance) {
                        if (err || !itemInstance) {
                            return resolve(null);
                        }
                        return resolve(itemInstance);
                    });
                })];
        });
    });
}
exports.getNote = getNote;
function getFolder(workspaceId, globalId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2, new Promise(function (resolve) {
                    item_1.default.find({ globalId: globalId, type: 'folder' }, { workspaceId: workspaceId }, function (err, itemInstance) {
                        if (err || !itemInstance) {
                            return resolve(null);
                        }
                        return resolve(itemInstance);
                    });
                })];
        });
    });
}
exports.getFolder = getFolder;
function getFolderNotes(workspaceId, folderId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2, new Promise(function (resolve) {
                    item_1.default.findAll({ parentId: folderId, type: 'note' }, { workspaceId: workspaceId }, function (err, itemList) {
                        if (err || !itemList) {
                            return resolve([]);
                        }
                        return resolve(itemList);
                    });
                })];
        });
    });
}
exports.getFolderNotes = getFolderNotes;
function getAttachments(workspaceId, noteGlobalId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2, new Promise(function (resolve) {
                    attach_1.default.findAll({ noteGlobalId: noteGlobalId }, { workspaceId: workspaceId }, function (err, attachments) {
                        if (err || !attachments) {
                            return resolve([]);
                        }
                        return resolve(attachments);
                    });
                })];
        });
    });
}
exports.getAttachments = getAttachments;
