"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFormatDate = function () {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + month;
    }
    if (minutes < 10) {
        minutes = "0" + month;
    }
    if (seconds < 10) {
        seconds = "0" + month;
    }
    return year + "-" + month + "-" + day + "_" + hours + "-" + minutes + "-" + seconds;
};
