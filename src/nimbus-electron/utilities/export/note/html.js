"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Y = __importStar(require("yjs"));
var jsdom = require("jsdom");
var Storage = require("dom-storage");
var mime = require("mime");
var editor_node_1 = require("@nimbus/editor-electron/dist/editor-node");
var text_1 = __importDefault(require("../../../db/models/text"));
var googleFonts = ['IBMPlexSans-Roboto', 'RobotoSlab', 'Caveat', 'AnonymousPro', 'Inconsolata'];
var ResizeObserver = (function () {
    function ResizeObserver() {
    }
    ResizeObserver.prototype.observe = function () { };
    ResizeObserver.prototype.disconnect = function () { };
    return ResizeObserver;
}());
exports.getNoteHtml = function (settings, doc, attachmentsByGlobalId) { return __awaiter(void 0, void 0, void 0, function () {
    var textVersion;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                textVersion = settings.textVersion;
                if (!(textVersion > 1)) return [3, 2];
                return [4, getNewEditorHtml(settings, doc, attachmentsByGlobalId)];
            case 1: return [2, _a.sent()];
            case 2: return [4, getOldEditorHtml(settings, attachmentsByGlobalId)];
            case 3: return [2, _a.sent()];
        }
    });
}); };
var getHost = function (url) { return url.replace(/https?:\/\//i, '').split('/')[0]; };
var httpGetRequest = function (url) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.onloadend = function (e) {
            try {
                var response = JSON.parse(xhr.response);
                resolve(response.body);
            }
            catch (error) {
                reject(error);
            }
        };
        xhr.onerror = function (error) { reject(error); };
        xhr.send(null);
    });
};
var getNewEditorHtml = function (settings, doc, attachmentsByGlobalId) { return __awaiter(void 0, void 0, void 0, function () {
    var language, timezone, style, size, pdf, noteUrlTemplate, folderUrlTemplate, workspaceUrlTemplate, title, domain, workspaceId, JSDOM, dom, window, setToGlobal, _i, setToGlobal_1, k, editor, mentionUrlTemplates, context, html;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                language = settings.language, timezone = settings.timezone, style = settings.style, size = settings.size, pdf = settings.pdf, noteUrlTemplate = settings.noteUrlTemplate, folderUrlTemplate = settings.folderUrlTemplate, workspaceUrlTemplate = settings.workspaceUrlTemplate, title = settings.title, domain = settings.domain, workspaceId = settings.workspaceId;
                JSDOM = jsdom.JSDOM;
                dom = new JSDOM("<!DOCTYPE html><div id=\"note-editor\"></div>");
                window = dom.window;
                global['navigator'] = {
                    userAgent: 'Chrome'
                };
                global['document'] = dom.window.document;
                global['window'] = dom.window;
                setToGlobal = [
                    'Node', 'Text', 'HTMLElement', 'MutationObserver', 'Image'
                ];
                for (_i = 0, setToGlobal_1 = setToGlobal; _i < setToGlobal_1.length; _i++) {
                    k = setToGlobal_1[_i];
                    global[k] = window[k];
                }
                global['localStorage'] = new Storage(null, { strict: false });
                global['ResizeObserver'] = ResizeObserver;
                return [4, editor_node_1.initEditor({
                        y: doc,
                        Y: Y,
                        mode: 'export',
                        plugins: {
                            i18n: {
                                language: language
                            },
                            blockMenu: {
                                enabled: false
                            },
                            embeddedMenu: {
                                enabled: false
                            },
                            attachmentManager: {
                                resolve: function (attachmentGlobalId) {
                                    console.info('Resolving attach %s', attachmentGlobalId);
                                    if (!attachmentsByGlobalId[attachmentGlobalId]) {
                                        return Promise.reject(new editor_node_1.AttachmentNotFoundError('attachment ' + attachmentGlobalId + ' not found'));
                                    }
                                    return Promise.resolve(attachmentsByGlobalId[attachmentGlobalId]);
                                }
                            },
                            bookmarkManager: {
                                getPreviews: function (url) {
                                    return httpGetRequest("https://everhelper.me/sdpreviews/listing.php?p=0&order=best&host=" + getHost(url) + "&ag=1").catch(function (err) {
                                        return 'bookmark.error.unknown_error';
                                    });
                                },
                                getFavicon: function (url) {
                                    return httpGetRequest("https://www.google.com/s2/favicons?domain=" + url).catch(function (err) {
                                        return 'bookmark.error.unknown_error';
                                    });
                                },
                                iframelyHost: 'https://iframely.nimbusweb.me',
                                iframelyPort: null,
                                extendedMetaRequest: true
                            }
                        },
                        blots: {
                            date: {
                                timezone: timezone,
                            }
                        },
                        editorStyle: { style: style, size: size }
                    })];
            case 1:
                editor = _a.sent();
                mentionUrlTemplates = {
                    note: noteUrlTemplate({
                        domain: domain,
                        workspaceId: workspaceId,
                    }),
                    folder: folderUrlTemplate({
                        domain: domain,
                        workspaceId: workspaceId,
                    }),
                    workspace: workspaceUrlTemplate({
                        domain: domain,
                    }),
                };
                context = new editor_node_1.ToHTMLContext({ mentionUrlTemplates: mentionUrlTemplates, pdf: pdf });
                html = editor.toHTML(context);
                editor.destroy();
                if (typeof global['document'] !== 'undefined') {
                    delete global['document'];
                }
                if (typeof global['window'] !== 'undefined') {
                    delete global['window'];
                }
                if (typeof global['navigator'] !== 'undefined') {
                    delete global['navigator'];
                }
                if (typeof global['localStorage'] !== 'undefined') {
                    delete global['localStorage'];
                }
                if (typeof global['ResizeObserver'] !== 'undefined') {
                    delete global['ResizeObserver'];
                }
                if (pdf) {
                    html = html.replace(new RegExp("<iframe[^>]+>.*?</iframe>", "g"), "");
                }
                html = "<!doctype html>\n    <html lang=\"en\">\n        <head>\n            <title>" + title + "</title>\n            <meta charset=\"utf-8\">\n            <link rel=\"stylesheet\" type=\"text/css\" href=\"./assets/theme.css\">\n            <link rel=\"stylesheet\" type=\"text/css\" href=\"./assets/fonts/fonts.css\">\n            " + googleFonts.map(function (font) { return "<link rel=\"stylesheet\" href=\"./assets/fonts/google-fonts/" + font + ".css\">"; }).join('\n') + "\n        </head>\n        <body>\n            " + html + "\n        </body>\n    </html>";
                return [2, {
                        html: html,
                        context: context,
                    }];
        }
    });
}); };
var getOldEditorHtml = function (settings, attachmentsByGlobalId) { return __awaiter(void 0, void 0, void 0, function () {
    var noteId, title, workspaceGlobalId, html, regexp, attachIdList, attachments, _i, attachIdList_1, attachId, globalId, attach, extension, attachPath;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                noteId = settings.noteId, title = settings.title, workspaceGlobalId = settings.workspaceGlobalId;
                return [4, text_1.default.getTextAsync(noteId, workspaceGlobalId)];
            case 1:
                html = _a.sent();
                regexp = new RegExp('#attacheloc:(.*)#', 'ig');
                attachIdList = html.match(regexp);
                if (!attachIdList) {
                    attachIdList = [];
                }
                attachments = {};
                for (_i = 0, attachIdList_1 = attachIdList; _i < attachIdList_1.length; _i++) {
                    attachId = attachIdList_1[_i];
                    globalId = attachId.substring(12, attachId.length - 1);
                    if (typeof attachmentsByGlobalId[globalId] === 'undefined') {
                        continue;
                    }
                    attach = attachmentsByGlobalId[globalId];
                    extension = mime.getExtension(attach.mime);
                    extension = extension ? "." + extension : '';
                    attachPath = "assets/" + attach.globalId + extension;
                    html = html.replace(attachId, attachPath);
                    attachments[globalId] = globalId;
                }
                html = ("" + html).replace(/<a\s+href=/gi, '<a target="_blank" href=');
                html = "<!doctype html>\n    <html lang=\"en\">\n        <head>\n            <title>" + title + "</title>\n            <meta charset=\"utf-8\">\n        </head>\n        <body>\n            " + html + "\n        </body>\n    </html>";
                return [2, {
                        html: html,
                        context: {
                            attachments: attachments,
                        },
                    }];
        }
    });
}); };
