"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var fs = require("fs-extra");
var mime = require("mime");
var find_1 = require("../utils/find");
var pdb_1 = __importDefault(require("../../../../pdb"));
var config_1 = __importDefault(require("../../../../config"));
var workspace_1 = __importDefault(require("../../../db/models/workspace"));
var html_1 = require("./html");
var date_1 = require("../utils/date");
var syncHandler_1 = __importDefault(require("../../syncHandler"));
var exportHandler_1 = require("../../exportHandler");
var generatorHandler_1 = __importDefault(require("../../generatorHandler"));
var file_1 = require("../utils/file");
var fs_extra_1 = require("fs-extra");
function processDoc(doc, settings) {
    return __awaiter(this, void 0, void 0, function () {
        var dir, workspaceId, noteId, textVersion, globalId, workspaceGlobalId, workspaceInstance, orgInfo, note, attachments, domain, title, attachmentsByGlobalId, _i, attachments_1, attach, _a, html, context, distPath, err_1, _b, attachments_2, attachment, _c, _d, attachmentGlobalId, attach, storedFileUUID, extension, attachDestPath;
        return __generator(this, function (_e) {
            switch (_e.label) {
                case 0:
                    dir = settings.dir, workspaceId = settings.workspaceId, noteId = settings.noteId, textVersion = settings.textVersion, globalId = settings.globalId;
                    return [4, workspace_1.default.getLocalId(workspaceId)];
                case 1:
                    workspaceGlobalId = _e.sent();
                    return [4, find_1.getWorkspace(workspaceGlobalId)];
                case 2:
                    workspaceInstance = _e.sent();
                    return [4, find_1.getOrg(workspaceInstance.orgId)];
                case 3:
                    orgInfo = _e.sent();
                    return [4, find_1.getNote(workspaceGlobalId, noteId)];
                case 4:
                    note = _e.sent();
                    return [4, find_1.getAttachments(workspaceGlobalId, noteId)];
                case 5:
                    attachments = _e.sent();
                    domain = orgInfo && orgInfo.domain ? orgInfo.domain : config_1.default.WEBNOTES_DOMAIN;
                    title = note.title;
                    attachmentsByGlobalId = {};
                    for (_i = 0, attachments_1 = attachments; _i < attachments_1.length; _i++) {
                        attach = attachments_1[_i];
                        attach.global_id = attach.globalId;
                        attach.display_name = attach.displayName;
                        attachmentsByGlobalId[attach.globalId] = attach;
                    }
                    return [4, html_1.getNoteHtml(__assign(__assign({}, settings), { title: title,
                            domain: domain,
                            workspaceId: workspaceId }), doc, attachmentsByGlobalId)];
                case 6:
                    _a = _e.sent(), html = _a.html, context = _a.context;
                    distPath = path.join(config_1.default.ASAR_APP_DIR, '/node_modules/@nimbus/editor-electron/dist');
                    return [4, exports.writeExportNoteContent(globalId, path.join(dir, '/note.html'), html)];
                case 7:
                    _e.sent();
                    if (!(textVersion > 1)) return [3, 13];
                    _e.label = 8;
                case 8:
                    _e.trys.push([8, 11, , 12]);
                    return [4, fs.copy(path.join(distPath, '/fonts'), path.join(dir, '/assets/fonts'))];
                case 9:
                    _e.sent();
                    return [4, fs.copy(path.join(distPath, '/export_theme.css'), path.join(dir, '/assets/theme.css'))];
                case 10:
                    _e.sent();
                    return [3, 12];
                case 11:
                    err_1 = _e.sent();
                    syncHandler_1.default.sendLog("Problem: export assets fonts + theme copy error: " + err_1.message);
                    return [3, 12];
                case 12: return [3, 15];
                case 13:
                    if (!Object.keys(context.attachments).length) return [3, 15];
                    return [4, fs_extra_1.mkdir(path.join(dir, '/assets'))];
                case 14:
                    _e.sent();
                    _e.label = 15;
                case 15:
                    console.info('Count editor attachments to copy %d', Object.keys(context.attachments).length);
                    if (!context.attachments) {
                        context.attachments = {};
                    }
                    for (_b = 0, attachments_2 = attachments; _b < attachments_2.length; _b++) {
                        attachment = attachments_2[_b];
                        if (!attachment.inList) {
                            continue;
                        }
                        context.attachments[attachment.globalId] = attachment.globalId;
                    }
                    _c = 0, _d = Object.keys(context.attachments);
                    _e.label = 16;
                case 16:
                    if (!(_c < _d.length)) return [3, 19];
                    attachmentGlobalId = _d[_c];
                    if (!exportHandler_1.isExportActive(globalId)) {
                        return [2, {
                                dir: dir,
                                title: title,
                            }];
                    }
                    if (typeof attachmentsByGlobalId[attachmentGlobalId] === 'undefined') {
                        return [3, 18];
                    }
                    console.info('Copy attach %s', attachmentGlobalId);
                    attach = attachmentsByGlobalId[attachmentGlobalId];
                    if (!attach) return [3, 18];
                    storedFileUUID = attach.storedFileUUID;
                    extension = mime.getExtension(attach.mime);
                    extension = extension ? "." + extension : '';
                    attachDestPath = dir + "/assets/" + attach.global_id + extension;
                    return [4, copyAttach(globalId, storedFileUUID, attachDestPath)];
                case 17:
                    _e.sent();
                    _e.label = 18;
                case 18:
                    _c++;
                    return [3, 16];
                case 19: return [2, {
                        dir: dir,
                        title: title,
                    }];
            }
        });
    });
}
exports.processDoc = processDoc;
var writeStreamResolveFn = function (globalId, resolve) {
    exportHandler_1.clearActiveExportStream(globalId);
    resolve();
};
var copyAttach = function (globalId, storedFileUUID, toPath) { return __awaiter(void 0, void 0, void 0, function () {
    var fromPath;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                fromPath = pdb_1.default.getClientAttachmentPath() + "/" + storedFileUUID;
                return [4, copyExportNoteFile(globalId, fromPath, toPath)];
            case 1: return [2, _a.sent()];
        }
    });
}); };
var copyExportNoteFile = function (globalId, fromPath, toPath) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2, new Promise(function (resolve) {
                var input = fs.createReadStream(fromPath);
                var output = fs.createWriteStream(toPath);
                input.on('error', function () { return writeStreamResolveFn(globalId, resolve); });
                output.on('error', function () { return writeStreamResolveFn(globalId, resolve); });
                output.on('close', function () { return writeStreamResolveFn(globalId, resolve); });
                exportHandler_1.setActiveExportStream(globalId, input, output);
                input.pipe(output);
            })];
    });
}); };
exports.writeExportNoteContent = function (globalId, toPath, content) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2, new Promise(function (resolve) {
                var output = fs.createWriteStream(toPath);
                output.on('error', function () { return writeStreamResolveFn(globalId, resolve); });
                output.on('close', function () { return writeStreamResolveFn(globalId, resolve); });
                exportHandler_1.setActiveExportStream(globalId, null, output);
                output.write(content);
                output.end();
            })];
    });
}); };
exports.makeDir = function (title, exportDir) {
    if (exportDir === void 0) { exportDir = null; }
    return __awaiter(void 0, void 0, void 0, function () {
        var clientPath, dirPath, destinationName, destinationPath, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    clientPath = pdb_1.default.getClientExportPath();
                    dirPath = exportDir ? exportDir : clientPath;
                    destinationName = title;
                    destinationPath = path.join(dirPath, destinationName);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4, fs.mkdir(destinationPath)];
                case 2:
                    _a.sent();
                    return [2, {
                            destinationName: destinationName,
                            destinationPath: destinationPath,
                        }];
                case 3:
                    error_1 = _a.sent();
                    syncHandler_1.default.sendLog(error_1);
                    return [2, {
                            destinationName: '',
                            destinationPath: null,
                        }];
                case 4: return [2];
            }
        });
    });
};
exports.makeTempDir = function (basePath, folder) {
    if (basePath === void 0) { basePath = ''; }
    if (folder === void 0) { folder = false; }
    return __awaiter(void 0, void 0, void 0, function () {
        var clientPath, isWriteable, name_1, destinationPath_1, name_2, destinationPath, error_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    basePath = folder ? basePath : path.dirname(basePath);
                    clientPath = basePath ? basePath : pdb_1.default.getClientExportPath();
                    return [4, file_1.ensureDirExistsAndWritable(clientPath)];
                case 1:
                    isWriteable = _a.sent();
                    if (!isWriteable) {
                        return [2, null];
                    }
                    _a.label = 2;
                case 2:
                    _a.trys.push([2, 6, , 7]);
                    if (!folder) return [3, 4];
                    name_1 = "export-" + date_1.getFormatDate();
                    destinationPath_1 = path.join(clientPath, name_1);
                    return [4, fs.mkdir(destinationPath_1)];
                case 3:
                    _a.sent();
                    return [2, destinationPath_1];
                case 4:
                    name_2 = "export-" + generatorHandler_1.default.randomString(8);
                    destinationPath = path.join(clientPath, name_2);
                    return [4, fs.mkdir(destinationPath)];
                case 5:
                    _a.sent();
                    return [2, destinationPath];
                case 6:
                    error_2 = _a.sent();
                    syncHandler_1.default.sendLog(error_2);
                    return [2, null];
                case 7: return [2];
            }
        });
    });
};
