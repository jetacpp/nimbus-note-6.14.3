"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var purchaseProducts;
var PurchaseProduct = (function () {
    function PurchaseProduct() {
    }
    PurchaseProduct.set = function (products) {
        purchaseProducts = products;
    };
    PurchaseProduct.get = function () {
        return purchaseProducts || [];
    };
    return PurchaseProduct;
}());
exports.default = PurchaseProduct;
