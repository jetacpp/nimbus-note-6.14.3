"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var SyncManager_1 = __importDefault(require("../sync/process/SyncManager"));
var appOnline;
function set(state) {
    appOnline = state;
}
function get() {
    return appOnline;
}
function listenChangeStatus() {
    electron_1.ipcMain.on('event:online:changed', function (event, arg) {
        set(arg.online);
        if (!arg.online) {
            SyncManager_1.default.stopAllSync();
        }
    });
    electron_1.powerMonitor.on('suspend', function () {
        SyncManager_1.default.stopAllSync();
    });
}
exports.default = {
    set: set,
    get: get,
    listenChangeStatus: listenChangeStatus
};
