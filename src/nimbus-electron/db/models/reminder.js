"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ReminderObj_1 = __importDefault(require("../../sync/process/db/ReminderObj"));
var ModelReminder = (function () {
    function ModelReminder() {
    }
    ModelReminder.getResponseJson = function (data) {
        if (!data) {
            return null;
        }
        if (!data.reminder) {
            return null;
        }
        return {
            date: data.reminder.date,
            interval: data.reminder.interval,
            dateUpdated: data.dateUpdated,
            noteGlobalId: data.globalId,
            priority: data.reminder.priority,
            phone: data.reminder.phone
        };
    };
    ModelReminder.getResponseListJson = function (list) {
        return list.map(function (data) {
            return ModelReminder.getResponseJson(data);
        });
    };
    ModelReminder.prepareModelData = function (data) {
        var item = new ReminderObj_1.default();
        item = ModelReminder.prepareCommonProperties(item, data);
        return item;
    };
    ModelReminder.prepareCommonProperties = function (item, data) {
        item.date = data.date || null;
        item.interval = data.interval || null;
        return item;
    };
    ModelReminder.INTERVAL_0 = 0;
    ModelReminder.INTERVAL_30_MIN = 1800;
    ModelReminder.INTERVAL_HOUR = 3600;
    ModelReminder.INTERVAL_DAY = 86400;
    ModelReminder.INTERVAL_WEEK = 604800;
    ModelReminder.INTERVAL_MONTH = 2592000;
    ModelReminder.INTERVAL_YEAR = 31536000;
    return ModelReminder;
}());
exports.default = ModelReminder;
