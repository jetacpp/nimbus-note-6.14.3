"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customError_1 = __importDefault(require("../../utilities/customError"));
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var dbName = config_runtime_1.default.DB_USER_SETTINGS_MODEL_NAME;
var ModelUserSettings = (function () {
    function ModelUserSettings() {
    }
    ModelUserSettings.find = function (data, options, callback) {
        if (options === void 0) { options = {}; }
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, function (err, item) {
            callback(err, ModelUserSettings.getPublicData(item));
        });
    };
    ModelUserSettings.count = function (data, options, callback) {
        if (options === void 0) { options = {}; }
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.count(data, options, callback);
    };
    ModelUserSettings.create = function (data, updateFields, callback) {
        if (data === void 0) { data = {}; }
        if (callback === void 0) { callback = function (err, res) {
        }; }
        if (!data.property) {
            return callback(customError_1.default.wrongInput(), null);
        }
        ModelUserSettings.count(data, {}, function (err, findCount) {
            if (err) {
                callback(err, null);
            }
            updateFields.property = data.property;
            var options = { dbName: dbName };
            if (findCount) {
                options.updateFields = updateFields;
                pdb_1.default.update(data, options, callback);
            }
            else {
                options.prepareModelData = ModelUserSettings.prepareModelData;
                pdb_1.default.create(updateFields, options, callback);
            }
        });
    };
    ModelUserSettings.delete = function (data, callback) {
        if (data === void 0) { data = {}; }
        if (callback === void 0) { callback = function (err, res) {
        }; }
        if (!data.property) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        var options = { dbName: dbName };
        pdb_1.default.remove(data, options, callback);
    };
    ModelUserSettings.get = function (key, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        ModelUserSettings.find({ property: key }, {}, callback);
    };
    ModelUserSettings.set = function (key, value, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        ModelUserSettings.create({ property: key }, { value: value }, callback);
    };
    ModelUserSettings.has = function (key, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        ModelUserSettings.count({ property: key }, {}, function (err, itemCount) {
            callback(err, !!itemCount);
        });
    };
    ModelUserSettings.remove = function (key, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        ModelUserSettings.delete({ property: key }, function (err, itemCount) {
            callback(err, !!itemCount);
        });
    };
    ModelUserSettings.getPublicData = function (doc) {
        var item = {};
        if (doc) {
            return doc.value;
        }
        return item;
    };
    ModelUserSettings.prepareModelData = function (data) {
        var item = {};
        item._id = data._id || generatorHandler_1.default.randomString(16);
        item.property = data.property;
        item.value = data.value;
        if (typeof (data.value) !== "undefined") {
            item.value = data.value;
        }
        return item;
    };
    ModelUserSettings.getDefaultSyncStatus = function () {
        return ModelUserSettings.SYNC_STATUS_DEFAULT;
    };
    ModelUserSettings.getDefaultSyncTimer = function () {
        return ModelUserSettings.SYNC_TIMER_MIN_DEFAULT;
    };
    ModelUserSettings.getSyncInfo = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId;
                        var syncInfo = {};
                        ModelUserSettings.get("syncStatus:" + workspaceId, function (err, syncStatus) {
                            if (typeof (syncStatus) === "object" && Object.keys(syncStatus).length === 0) {
                                syncInfo.syncStatus = ModelUserSettings.getDefaultSyncStatus();
                            }
                            else {
                                syncInfo.syncStatus = parseInt(syncStatus);
                            }
                            ModelUserSettings.get("syncTimer:" + workspaceId, function (err, syncTimer) {
                                if (typeof (syncTimer) === "object" && Object.keys(syncTimer).length === 0) {
                                    syncInfo.syncTimer = ModelUserSettings.SYNC_TIMER_MIN_DEFAULT;
                                }
                                else {
                                    syncInfo.syncTimer = parseInt(syncTimer);
                                }
                                resolve(syncInfo);
                            });
                        });
                    })];
            });
        });
    };
    ModelUserSettings.getIndexList = function () {
        return ["property", "value"];
    };
    ModelUserSettings.SYNC_STATUS_DEFAULT = 1;
    ModelUserSettings.SYNC_TIMER_MIN_DEFAULT = 10;
    return ModelUserSettings;
}());
exports.default = ModelUserSettings;
