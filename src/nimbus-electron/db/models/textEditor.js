"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var dbName = config_runtime_1.default.DB_TEXT_EDITOR_MODEL_NAME;
var ModelTextEditor = (function () {
    function ModelTextEditor() {
    }
    ModelTextEditor.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelTextEditor.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.findAll(data, options, callback);
    };
    ModelTextEditor.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.prepareModelData = ModelTextEditor.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelTextEditor.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelTextEditor.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        pdb_1.default.remove(data, options, callback);
    };
    ModelTextEditor.getPublicData = function (doc) {
        var text = {};
        if (doc) {
            text.globalId = doc.globalId;
            text.noteGlobalId = doc.noteGlobalId;
            text.text = doc.text;
            text.dateUpdated = doc.dateUpdated;
        }
        return text;
    };
    ModelTextEditor.prepareModelData = function (data) {
        return ModelTextEditor.prepareCommonProperties({}, data);
    };
    ModelTextEditor.prepareCommonProperties = function (item, data) {
        item._id = data._id || generatorHandler_1.default.randomString(16);
        item.globalId = data.globalId;
        item.noteGlobalId = data.noteGlobalId;
        item.text = data.text;
        item.dateUpdated = data.dateUpdated;
        return data;
    };
    ModelTextEditor.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelTextEditor.getIndexList = function () {
        return ["globalId", "noteGlobalId", "dateUpdated", "text"];
    };
    return ModelTextEditor;
}());
exports.default = ModelTextEditor;
