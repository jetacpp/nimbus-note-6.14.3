"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var dateHandler_1 = __importDefault(require("../../utilities/dateHandler"));
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var workspaceMember_1 = __importDefault(require("./workspaceMember"));
var dbName = config_runtime_1.default.DB_WORKSPACE_INVITES_MODEL_NAME;
var ModelWorkspaceInvite = (function () {
    function ModelWorkspaceInvite() {
    }
    ModelWorkspaceInvite.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelWorkspaceInvite.count = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.count(data, options, callback);
    };
    ModelWorkspaceInvite.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.findAll(data, options, callback);
    };
    ModelWorkspaceInvite.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.workspaceId) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.prepareModelData = ModelWorkspaceInvite.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelWorkspaceInvite.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.workspaceId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelWorkspaceInvite.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.id) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelWorkspaceInvite.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (data.id) {
            options.dbName = dbName;
            pdb_1.default.remove(data, options, callback);
        }
        else {
            return callback(customError_1.default.wrongInput(), 0);
        }
    };
    ModelWorkspaceInvite.getPublicData = function (doc) {
        var workspaceInvite = {};
        if (doc) {
            workspaceInvite.id = doc._id;
            workspaceInvite.globalId = doc.globalId;
            workspaceInvite.createdAt = doc.createdAt;
            workspaceInvite.updatedAt = doc.updatedAt;
            if (doc.workspaceId)
                workspaceInvite.workspaceId = doc.workspaceId;
            if (doc.role)
                workspaceInvite.role = doc.role;
            if (doc.encryptRole)
                workspaceInvite.encryptRole = doc.encryptRole;
            if (doc.addedByUserId)
                workspaceInvite.addedByUserId = doc.addedByUserId;
            if (doc.email)
                workspaceInvite.email = doc.email ? doc.email.toLowerCase() : '';
            if (doc.used)
                workspaceInvite.used = doc.used;
            if (doc.syncDate)
                workspaceInvite.syncDate = doc.syncDate;
            if (doc.needSync)
                workspaceInvite.needSync = doc.needSync;
        }
        return workspaceInvite;
    };
    ModelWorkspaceInvite.getResponseJson = function (data) {
        if (!data) {
            return null;
        }
        return {
            globalId: data.globalId,
            workspaceId: data.workspaceId,
            role: data.role,
            encryptRole: data.encryptRole,
            addedByUserId: data.addedByUserId,
            email: data.email ? data.email.toLowerCase() : '',
            used: data.used,
            createdAt: data.createdAt,
            updatedAt: data.updatedAt
        };
    };
    ModelWorkspaceInvite.prepareModelData = function (data) {
        var item = {};
        item = ModelWorkspaceInvite.prepareCommonProperties(item, data);
        item = ModelWorkspaceInvite.prepareSyncOnlyProperties(item, data);
        return item;
    };
    ModelWorkspaceInvite.prepareCommonProperties = function (item, data) {
        var curDate = dateHandler_1.default.now();
        item.erised = data.erised || false;
        item.globalId = data.globalId || data.global_id || item._id || generatorHandler_1.default.randomString(16);
        item._id = item.globalId.toString();
        item.workspaceId = data.workspaceId || '';
        item.role = data.role || workspaceMember_1.default.ROLE_READER;
        item.encryptRole = data.encryptRole || workspaceMember_1.default.ENCRYPTION_ROLE_DENY;
        item.addedByUserId = data.addedByUserId || 0;
        item.email = data.email ? data.email.toLowerCase() : '';
        item.used = data.used || false;
        item.createdAt = data.createdAt || curDate;
        item.updatedAt = data.updatedAt || curDate;
        return item;
    };
    ModelWorkspaceInvite.prepareSyncOnlyProperties = function (item, data) {
        item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
        item.syncDate = data.syncDate || 0;
        return item;
    };
    ModelWorkspaceInvite.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelWorkspaceInvite.getIndexList = function () {
        return ["globalId", "workspaceId", "role", "encryptRole", "addedByUserId", "email", "used", "updatedAt"];
    };
    ModelWorkspaceInvite.ERROR_LIMIT = -20;
    ModelWorkspaceInvite.ERROR_UNIQUE = -21;
    return ModelWorkspaceInvite;
}());
exports.default = ModelWorkspaceInvite;
