"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var textHandler_1 = __importDefault(require("../../utilities/textHandler"));
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var dateHandler_1 = __importDefault(require("../../utilities/dateHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var dbName = config_runtime_1.default.DB_TEXT_MODEL_NAME;
var ModelText = (function () {
    function ModelText() {
    }
    ModelText.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelText.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.findAll(data, options, callback);
    };
    ModelText.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.noteGlobalId) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.prepareModelData = ModelText.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelText.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.noteGlobalId) {
            return callback(customError_1.default.wrongInput(), null);
        }
        if (updateFields.text) {
            updateFields.textShort = ModelText.makeShortText(updateFields.text);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelText.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.noteGlobalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelText.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.noteGlobalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        pdb_1.default.remove(data, options, callback);
    };
    ModelText.getResponseJson = function (data, onlyShort) {
        if (onlyShort === void 0) { onlyShort = false; }
        if (!data) {
            return null;
        }
        var result = {
            dateUpdated: data.dateUpdated,
            length: data.length,
            noteGlobalId: data.noteGlobalId
        };
        if (typeof (data.textShort) === "undefined") {
            result['textShort'] = "";
        }
        else {
            result['textShort'] = data.textShort;
        }
        if (!onlyShort) {
            result['text'] = data.text === "undefined" ? "" : data.text;
            if (!result['text']) {
                result['text'] = ModelText.getDefaultNoteText();
            }
        }
        if (typeof (data.text_version) !== 'undefined') {
            result['version'] = data.text_version;
        }
        return result;
    };
    ModelText.getResponseListJson = function (list, onlyShort) {
        onlyShort = onlyShort || false;
        return list.map(function (data) {
            return ModelText.getResponseJson(data, onlyShort);
        });
    };
    ModelText.getPublicData = function (doc) {
        var text = {};
        if (doc) {
            text.noteGlobalId = doc.noteGlobalId;
            text.dateUpdated = doc.dateUpdated;
            text.text = doc.text;
            text.textShort = doc.textShort;
            text['length'] = doc['length'];
            if (doc.dateAdded)
                text.dateAdded = doc.dateAdded;
            if (doc.parentId)
                text.parentId = doc.parentId;
            if (doc.syncDate)
                text.syncDate = doc.syncDate;
            if (doc.needSync)
                text.needSync = doc.needSync;
            if (doc.text_version)
                text.text_version = doc.text_version;
        }
        return text;
    };
    ModelText.prepareModelData = function (data) {
        var item = {};
        item = ModelText.prepareCommonProperties(item, data);
        item = ModelText.prepareSyncOnlyProperties(item, data);
        return item;
    };
    ModelText.prepareCommonProperties = function (item, data) {
        item.erised = data.erised || false;
        item._id = data._id || generatorHandler_1.default.randomString(16);
        item.noteGlobalId = data.noteGlobalId || data.parentId;
        item.dateUpdated = data.dateUpdated;
        item.text = data.text || ModelText.getDefaultNoteText();
        item.textShort = data.shortText || textHandler_1.default.stripTags(item.text);
        item['length'] = data.text ? data.text.length : 0;
        item.text_version = data.text_version || 1;
        return item;
    };
    ModelText.prepareSyncOnlyProperties = function (item, data) {
        item.shortText = data.shortText || textHandler_1.default.stripTags(item.text) || "";
        item.dateAdded = data.dateAdded || "";
        item.parentId = data.parentId || data.noteGlobalId || "";
        item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
        item.syncDate = data.syncDate || 0;
        item.text_version = data.text_version || 1;
        return item;
    };
    ModelText.getDefaultNoteText = function () {
        return "<div></div>";
    };
    ModelText.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelText.getDefaultModel = function (noteGlobalId) {
        var text = {};
        var curDate = dateHandler_1.default.now();
        text.erised = false;
        text._id = generatorHandler_1.default.randomString(16);
        text.noteGlobalId = noteGlobalId;
        text.dateUpdated = curDate;
        text.text = "<div></div>";
        text.textShort = textHandler_1.default.stripTags(text.text);
        text['length'] = text.text ? text.text.length : 0;
        text.text_version = text.text_version || 2;
        return text;
    };
    ModelText.getIndexList = function () {
        return ["noteGlobalId", "dateUpdated", "text", "textShort", "length"];
    };
    ModelText.makeShortText = function (text) {
        var clearedText = textHandler_1.default.stripTags(text);
        return clearedText.length > ModelText.SHORT_TEXT_LENGTH ?
            clearedText.substring(0, ModelText.SHORT_TEXT_LENGTH) + ModelText.SHORT_TEXT_LINE_END : clearedText;
    };
    ModelText.getTextAsync = function (noteGlobalId, workspaceId) {
        return new Promise(function (resolve) {
            ModelText.find({ noteGlobalId: noteGlobalId }, { workspaceId: workspaceId }, function (err, textItem) {
                if (err || !textItem) {
                    return '';
                }
                return resolve(textItem.text);
            });
        });
    };
    ModelText.SHORT_TEXT_LENGTH = 125;
    ModelText.SHORT_TEXT_LINE_END = "...";
    ModelText.MAX_TEXT_SIZE = 5242880;
    return ModelText;
}());
exports.default = ModelText;
