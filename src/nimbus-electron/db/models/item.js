"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var dateHandler_1 = __importDefault(require("../../utilities/dateHandler"));
var textHandler_1 = __importDefault(require("../../utilities/textHandler"));
var orgs_1 = __importDefault(require("./orgs"));
var workspace_1 = __importDefault(require("./workspace"));
var dbName = config_runtime_1.default.DB_ITEM_MODEL_NAME;
var dbTextName = config_runtime_1.default.DB_TEXT_MODEL_NAME;
var ModelItem = (function () {
    function ModelItem() {
    }
    ModelItem.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelItem.count = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.count(data, options, callback);
    };
    ModelItem.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var findParams, i, sortOrder, onlyTitleSearch, g, textOptions, searchByTagIdList, searchByStringIdList, noteIdList, _i, searchByStringIdList_1, findId, textQuery, docs, i, noteData, textData, tagsData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        findParams = { selector: {} };
                        if (options.sortQuery) {
                            for (i in options.sortQuery) {
                                if (options.sortQuery.hasOwnProperty(i)) {
                                    sortOrder = options.sortQuery[i] < 0 ? "desc" : "asc";
                                    if (i === "dateUpdated") {
                                        findParams.sort = [{ "dateUpdated": sortOrder }];
                                    }
                                    else if (i === "dateAdded") {
                                        findParams.sort = [{ "dateAdded": sortOrder }];
                                    }
                                    else if (i === "title") {
                                        findParams.sort = [{ "title": sortOrder }];
                                    }
                                }
                            }
                        }
                        if (typeof (options.limitValue) !== "undefined" && options.limitValue > 0) {
                            findParams.limit = options.limitValue;
                        }
                        if (typeof (options.offsetValue) !== "undefined" && options.offsetValue > 0) {
                            findParams.skip = options.offsetValue;
                        }
                        onlyTitleSearch = data && data.onlyTitleSearch;
                        if (data && typeof (data['onlyTitleSearch']) !== 'undefined') {
                            delete data['onlyTitleSearch'];
                        }
                        for (g in data) {
                            if (data.hasOwnProperty(g)) {
                                findParams.selector[g] = data[g];
                            }
                        }
                        if (findParams) {
                            options.findParams = findParams;
                        }
                        options.dbName = dbName;
                        if (!data.title || onlyTitleSearch) {
                            return [2, pdb_1.default.findAll(data, options, callback)];
                        }
                        textOptions = {};
                        textOptions.dbName = dbTextName;
                        textOptions.workspaceId = options.workspaceId;
                        searchByTagIdList = options.searchParams ? options.searchParams.searchByTagIdList : null;
                        searchByStringIdList = options.searchParams ? options.searchParams.searchByStringIdList : null;
                        noteIdList = [];
                        if (!searchByStringIdList) return [3, 1];
                        for (_i = 0, searchByStringIdList_1 = searchByStringIdList; _i < searchByStringIdList_1.length; _i++) {
                            findId = searchByStringIdList_1[_i];
                            noteIdList.push(findId);
                        }
                        return [3, 3];
                    case 1:
                        textQuery = { text: data.title };
                        return [4, ModelItem.serachByText(textQuery, textOptions)];
                    case 2:
                        docs = _a.sent();
                        for (i in docs) {
                            if (!docs.hasOwnProperty(i))
                                return [2];
                            noteIdList.push(docs[i].noteGlobalId);
                        }
                        _a.label = 3;
                    case 3:
                        noteData = Object.assign({}, findParams.selector);
                        textData = { globalId: { '$in': noteIdList } };
                        if (data.rootId) {
                            textData.rootId = data.rootId;
                        }
                        if (data.parentId) {
                            textData.parentId = data.parentId;
                        }
                        options.findParams.selector = {};
                        if (searchByStringIdList && searchByTagIdList) {
                            tagsData = { globalId: { '$in': searchByTagIdList } };
                            options.findParams.selector.$and = [{ "$or": [noteData, textData] }, tagsData];
                        }
                        else {
                            options.findParams.selector.$or = [noteData, textData];
                        }
                        pdb_1.default.findAll(options.findParams.selector, options, function (err, docsByTitle) {
                            return callback(err, docsByTitle);
                        });
                        return [2];
                }
            });
        });
    };
    ModelItem.serachByText = function (textData, textOptions) {
        return new Promise(function (resolve) {
            pdb_1.default.findAll(textData, textOptions, function (err, docs) {
                if (err || !docs) {
                    return resolve([]);
                }
                return resolve(docs);
            });
        });
    };
    ModelItem.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.parentId || !data.globalId || !data.type || typeof (data.title) === "undefined") {
            return callback(customError_1.default.wrongInput(), null);
        }
        if (!data.title) {
            data.title = ModelItem.getDefaultTitle();
        }
        options.dbName = dbName;
        options.prepareModelData = ModelItem.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelItem.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelItem.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelItem.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        pdb_1.default.remove(data, options, callback);
    };
    ModelItem.getApiNote = function (globalId, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        if (!globalId) {
            return callback(customError_1.default.wrongInput(), {});
        }
        var queryData = { globalId: globalId };
        ModelItem.find(queryData, options, function (err, itemInstance) {
            callback(null, ModelItem.getResponseJson(itemInstance));
        });
    };
    ModelItem.prepareNoteTitle = function (title) {
        if (title) {
            title = textHandler_1.default.stripTags(title);
        }
        return title ? title : ModelItem.getDefaultTitle();
    };
    ModelItem.getDefaultTitle = function () {
        return "Unnamed Note";
    };
    ModelItem.getPublicData = function (doc) {
        var item = {};
        if (doc) {
            item.id = doc._id;
            item.rootId = doc.rootId;
            item.globalId = doc.globalId;
            item.parentId = doc.parentId;
            item.rootParentId = doc.rootParentId;
            item.createdAt = doc.createdAt;
            item.dateAdded = doc.dateAdded;
            item.dateUpdated = doc.dateUpdated;
            item.updatedAt = doc.updatedAt;
            item.dateUpdatedUser = doc.dateUpdatedUser;
            item.type = doc.type;
            item.role = doc.role;
            item.title = doc.title;
            item.url = doc.url;
            item.locationLat = doc.locationLat;
            item.locationLng = doc.locationLng;
            item.shared = doc.shared;
            item.favorite = doc.favorite;
            item.lastChangeBy = doc.lastChangeBy;
            item.size = doc.size;
            item.editnote = doc.editnote;
            item.isEncrypted = doc.isEncrypted;
            if (doc.offlineOnly)
                item.offlineOnly = doc.offlineOnly;
            if (doc.folderDepth)
                item.folderDepth = doc.folderDepth;
            if (doc.folderList)
                item.folderList = doc.folderList;
            if (doc.titleTree)
                item.titleTree = doc.titleTree;
            if (doc.color)
                item.color = doc.color;
            if (doc.is_imported)
                item.is_imported = doc.is_imported;
            if (doc.index)
                item.index = doc.index;
            if (doc.existOnServer)
                item.existOnServer = doc.existOnServer;
            if (doc.syncDate)
                item.syncDate = doc.syncDate;
            if (doc.uniqueUserName)
                item.uniqueUserName = doc.uniqueUserName;
            if (doc.needSync)
                item.needSync = doc.needSync;
            if (doc.isMaybeInTrash)
                item.isMaybeInTrash = doc.isMaybeInTrash;
            if (doc.isClicked)
                item.isClicked = doc.isClicked;
            if (doc.subfoldersCount)
                item.subfoldersCount = doc.subfoldersCount;
            if (doc.notesCount)
                item.notesCount = doc.notesCount;
            if (doc.level)
                item.level = doc.level;
            if (doc.isTemp)
                item.isTemp = doc.isTemp;
            if (doc.shortText)
                item.shortText = doc.shortText;
            if (doc.firstImage)
                item.firstImage = doc.firstImage;
            if (doc.isMoreThanLimit)
                item.isMoreThanLimit = doc.isMoreThanLimit;
            if (doc.tags)
                item.tags = doc.tags;
            if (doc.color)
                item.color = doc.color;
            if (doc.textAttachmentGlobalId)
                item.textAttachmentGlobalId = doc.textAttachmentGlobalId;
            if (doc.attachmentsInListCount)
                item.attachmentsInListCount = doc.attachmentsInListCount;
            if (doc.attachmentsInListExist)
                item.attachmentsInListExist = doc.attachmentsInListExist;
            if (doc.reminderExist)
                item.reminderExist = doc.reminderExist;
            if (doc.todoExist)
                item.todoExist = doc.todoExist;
            if (doc.reminderLabel)
                item.reminderLabel = doc.reminderLabel;
            if (doc.todoCount)
                item.todoCount = doc.todoCount;
            if (doc.locationAddress)
                item.locationAddress = doc.locationAddress;
            if (doc.isLocationExist)
                item.isLocationExist = doc.isLocationExist;
            if (doc.isDownloaded)
                item.isDownloaded = doc.isDownloaded;
            if (doc.preview)
                item.preview = doc.preview;
        }
        return item;
    };
    ModelItem.getPreviewResponseJson = function (data) {
        if (!data) {
            return null;
        }
        return {
            "noteGlobalId": data.globalId,
            "attachmentGlobalId": (data.preview ? data.preview['global_id'] : null),
            "createdAt": data.createdAt,
            "updatedAt": data.updatedAt
        };
    };
    ModelItem.getPreviewResponseListJson = function (list) {
        return list.map(function (data) {
            return ModelItem.getPreviewResponseJson(data);
        });
    };
    ModelItem.getResponseJson = function (data, excerpts, matches, texts) {
        if (excerpts === void 0) { excerpts = null; }
        if (matches === void 0) { matches = null; }
        if (texts === void 0) { texts = null; }
        if (!data) {
            return null;
        }
        if (data.type === "note") {
            return ModelItem.getNotePublicData(data, excerpts, matches, texts);
        }
        else {
            return ModelItem.getFolderPublicData(data);
        }
    };
    ModelItem.getResponseListJson = function (list, searchParams) {
        var excerpts = searchParams ? searchParams.excerpts : null;
        var matches = searchParams ? searchParams.matches : null;
        var texts = searchParams ? searchParams.texts : null;
        return list.map(function (data) {
            return ModelItem.getResponseJson(data, excerpts, matches, texts);
        });
    };
    ModelItem.getNotePublicData = function (data, excerpts, matches, texts) {
        if (excerpts && matches && texts) {
            var matchesValue = [];
            if (typeof (matches[data.globalId]) !== 'undefined') {
                matchesValue = matches[data.globalId];
            }
            var excerptValue = '';
            if (typeof (excerpts[data.globalId]) !== 'undefined') {
                excerptValue = excerpts[data.globalId];
            }
            else if (typeof (texts[data.globalId]) !== 'undefined') {
                excerptValue = texts[data.globalId];
            }
            return {
                noteGlobalId: data.globalId,
                excerpt: excerptValue,
                matches: matchesValue,
            };
        }
        return {
            rootId: data.rootId,
            rootParentId: data.rootParentId,
            attachmentsCount: data.attachmentsCount,
            childrenNotesCount: data.childrenNotesCount,
            cntNotes: data.cntNotes,
            createdAt: data.createdAt,
            dateAdded: data.dateAdded,
            dateUpdated: data.dateUpdated,
            editnote: !!data.editnote,
            favorite: data.favorite,
            globalId: data.globalId,
            isCompleted: !!data.isCompleted,
            isFullwidth: !!data.isFullwidth,
            isEncrypted: data.isEncrypted,
            lastChangeBy: data.lastChangeBy,
            locationLat: data.locationLat,
            locationLng: data.locationLng,
            parentId: data.parentId,
            role: data.role,
            shared: !!data.shared,
            size: data.size,
            title: data.title,
            todosClosedCount: data.todosClosedCount,
            todosCount: data.todosCount,
            type: data.type,
            updatedAt: data.updatedAt,
            url: data.url,
            isOfflineOnly: data.offlineOnly,
            synced: !data.offlineOnly && data.existOnServer && !!data.syncDate,
            color: data.color ? data.color : '',
            isImported: !!data.is_imported,
            workspaceId: data.workspaceId
        };
    };
    ModelItem.getFolderPublicData = function (data) {
        return {
            rootId: data.rootId,
            rootParentId: data.rootParentId,
            childrenNotesCount: data.childrenNotesCount,
            cntNotes: data.cntNotes,
            createdAt: data.createdAt,
            dateAdded: data.dateAdded,
            dateUpdated: data.dateUpdated,
            editnote: !!data.editnote,
            favorite: !!data.favorite,
            globalId: data.globalId,
            isCompleted: !!data.isCompleted,
            isEncrypted: !!data.isEncrypted,
            isFullwidth: !!data.isFullwidth,
            lastChangeBy: data.lastChangeBy,
            locationLat: data.locationLat,
            locationLng: data.locationLng,
            parentId: data.parentId,
            role: data.role,
            shared: !!data.shared,
            size: data.size,
            title: data.title,
            todosClosedCount: 0,
            todosCount: 0,
            type: data.type,
            updatedAt: data.updatedAt,
            url: data.url,
            isOfflineOnly: data.offlineOnly,
            color: data.color ? data.color : '',
            isImported: !!data.is_imported,
        };
    };
    ModelItem.prepareModelData = function (data) {
        var item = {};
        item = ModelItem.prepareCommonProperties(item, data);
        item = ModelItem.prepareFolderOnlyProperties(item, data);
        item = ModelItem.prepareNoteOnlyProperties(item, data);
        return item;
    };
    ModelItem.prepareCommonProperties = function (item, data) {
        var cutDate = dateHandler_1.default.now();
        item.erised = data.erised || false;
        item.rootId = data.rootId || 'root';
        item.globalId = data.globalId || 'default';
        item._id = item.globalId.toString();
        item.parentId = data.parentId || 'root';
        item.rootParentId = data.rootParentId || 'root';
        item.createdAt = data.createdAt || data.dateAdded || cutDate;
        item.dateAdded = data.dateAdded || cutDate;
        item.dateUpdated = data.dateUpdated || cutDate;
        item.updatedAt = data.dateUpdated || data.updatedAt || cutDate;
        item.dateUpdatedUser = data.dateUpdatedUser || item.updatedAt || cutDate;
        item.type = data.type;
        item.role = data.role || 'note';
        item.title = data.title || '';
        item.url = data.url || '';
        item.locationLat = data.locationLat || 0;
        item.locationLng = data.locationLng || 0;
        if (typeof (data.favorite) !== 'undefined') {
            item.favorite = data.favorite;
        }
        if (typeof (data.reminder) !== 'undefined') {
            item.reminder = data.reminder;
        }
        item.lastChangeBy = data.lastChangeBy || 0;
        item.size = data.size || 0;
        if (typeof (data.editnote) !== 'undefined') {
            item.editnote = data.editnote;
        }
        if (typeof (data.isEncrypted) !== 'undefined') {
            item.isEncrypted = !!data.isEncrypted || false;
        }
        item.preview = data.preview || null;
        item.offlineOnly = !!data.offlineOnly;
        if (typeof (data.shared) !== 'undefined') {
            item.shared = data.shared;
        }
        if (!item.shared) {
            item.passwordRequired = false;
            item.shareId = '';
            item.securityKey = '';
        }
        if (typeof (data.text_version) !== 'undefined') {
            item.text_version = data.text_version;
        }
        if (typeof (data.is_imported) !== 'undefined') {
            item.is_imported = data.is_imported;
        }
        if (typeof (data.isFullwidth) !== 'undefined') {
            item.isFullwidth = data.isFullwidth;
        }
        return item;
    };
    ModelItem.prepareFolderOnlyProperties = function (item, data) {
        item.index = data.index || 0;
        item.existOnServer = !!data.existOnServer;
        item.uniqueUserName = data.uniqueUserName || "";
        item.syncDate = data.syncDate || 0;
        item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
        item.isMaybeInTrash = !!data.isMaybeInTrash;
        item.isClicked = !!data.isClicked;
        item.subfoldersCount = data.subfoldersCount || 0;
        item.notesCount = data.notesCount || 0;
        item.level = data.level || 0;
        item.color = data.color ? data.color : "";
        if (typeof (data.shared) !== "undefined") {
            item.shared = !!data.shared;
            if (data.shared_url) {
                item.shared_url = data.shared_url;
            }
        }
        if (!item.shared) {
            item.passwordRequired = false;
            item.shareId = "";
            item.securityKey = "";
            item.shared_url = "";
        }
        if (typeof (data.is_imported) !== "undefined") {
            item.is_imported = data.is_imported;
        }
        return item;
    };
    ModelItem.prepareNoteOnlyProperties = function (item, data) {
        item.syncDate = data.syncDate || 0;
        item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
        item.isTemp = !!data.isTemp;
        item.shortText = data.shortText || "";
        item.firstImage = data.firstImage || "";
        item.isMoreThanLimit = !!data.isMoreThanLimit;
        item.tags = data.tags || "";
        item.color = data.color ? data.color : "";
        item.textAttachmentGlobalId = data.textAttachmentGlobalId || "";
        item.attachmentsInListCount = data.attachmentsInListCount || 0;
        item.attachmentsInListExist = !!data.attachmentsInListExist;
        item.reminderExist = !!data.reminderExist;
        item.todoExist = !!data.todoExist;
        item.reminderLabel = data.reminderLabel || "";
        item.todoCount = data.todoCount || 0;
        item.locationAddress = data.locationAddress || "";
        item.isLocationExist = !!data.isLocationExist;
        item.isDownloaded = !!data.isDownloaded;
        if (typeof (data.shared) !== "undefined") {
            item.shared = !!data.shared;
            if (data.shared_url) {
                item.shared_url = data.shared_url;
            }
        }
        if (!item.shared) {
            item.passwordRequired = false;
            item.shareId = "";
            item.securityKey = "";
            item.shared_url = "";
        }
        if (typeof (data.preview) !== "undefined") {
            item.preview = data.preview || null;
        }
        if (typeof (data.editnote) !== 'undefined') {
            item.editnote = data.editnote || false;
        }
        if (typeof (data.isEncrypted) !== 'undefined') {
            item.isEncrypted = !!data.isEncrypted || false;
        }
        if (typeof (data.url) !== "undefined") {
            item.url = data.url;
        }
        if (typeof (data.color) !== 'undefined') {
            item.color = data.color;
        }
        if (typeof (data.favorite) !== 'undefined') {
            item.favorite = data.favorite;
        }
        if (typeof (data.reminder) !== 'undefined') {
            item.reminder = data.reminder;
        }
        if (typeof (data.text_version) !== 'undefined') {
            item.text_version = data.text_version;
        }
        if (typeof (data.is_imported) !== 'undefined') {
            item.is_imported = data.is_imported;
        }
        if (typeof (data.isFullwidth) !== 'undefined') {
            item.isFullwidth = data.isFullwidth;
        }
        return item;
    };
    ModelItem.changeDateUpdate = function (item, data) {
        if (data.dateUpdated) {
            item.dateUpdated = data.dateUpdated;
        }
        if (data.dateUpdated) {
            item.updatedAt = data.dateUpdated;
        }
        if (data.dateUpdatedUser || item.updatedAt) {
            item.dateUpdatedUser = data.dateUpdatedUser || item.updatedAt;
        }
        return item;
    };
    ModelItem.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelItem.getDefaultModel = function () {
        var item = {};
        var curDate = dateHandler_1.default.now();
        item.erised = false;
        item._id = generatorHandler_1.default.randomString(16);
        item.rootId = 'root';
        item.rootIdUpdateTime = curDate;
        item.globalId = 'default';
        item.parentId = 'root';
        item.rootParentId = 'root';
        item.createdAt = curDate;
        item.dateAdded = curDate;
        item.dateUpdated = curDate;
        item.updatedAt = curDate;
        item.dateUpdatedUser = curDate;
        item.type = "folder";
        item.role = "note";
        item.title = "My Notes";
        item.url = "";
        item.locationLat = 0;
        item.locationLng = 0;
        item.shared = false;
        item.favorite = false;
        item.lastChangeBy = 0;
        item.size = 0;
        item.editnote = 1;
        item.isEncrypted = false;
        item.preview = null;
        item.passwordRequired = false;
        item.shareId = "";
        item.securityKey = "";
        item.offlineOnly = false;
        item.color = "";
        item.text_version = 2;
        item.is_imported = 0;
        return item;
    };
    ModelItem.getIndexList = function () {
        return [
            "rootId", "globalId", "parentId", "rootParentId",
            "createdAt", "dateAdded", "dateUpdated", "updatedAt", "dateUpdatedUser",
            "type", "role", "title", "url", "locationLat", "locationLng", "shared",
            "favorite", "lastChangeBy", "favorite", "editnote", "isEncrypted", "preview",
            "passwordRequired", "shareId", "securityKey", "offlineOnly", "color", "text_version",
            "is_imported"
        ];
    };
    ModelItem.getNotesWithReminders = function (workspaceId) {
        return new Promise(function (resolve) {
            ModelItem.findAll({ type: 'note', reminder: { '$ne': null } }, { workspaceId: workspaceId }, function (err, notesList) {
                if (err || !notesList) {
                    return resolve([]);
                }
                return resolve(notesList);
            });
        });
    };
    ModelItem.getByGlobalId = function (_a) {
        var globalId = _a.globalId, workspaceId = _a.workspaceId;
        return new Promise(function (resolve) {
            ModelItem.find({ type: 'note', globalId: globalId }, { workspaceId: workspaceId }, function (err, noteInstance) {
                if (err || !noteInstance) {
                    return resolve(null);
                }
                return resolve(noteInstance);
            });
        });
    };
    ModelItem.validateNotesCountPerWorkspace = function (_a) {
        var _this = this;
        var workspaceId = _a.workspaceId;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var response, workspaceItem, _a, limits, currentNotesCount, notesPerWorkspace;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        response = {
                            canCreateNote: true,
                            currentNotesCount: 0,
                            notesPerWorkspace: orgs_1.default.NOTES_PER_WORKSPACE_MAX,
                            notesPerWorkspacePro: orgs_1.default.NOTES_PER_WORKSPACE_MAX_PRO,
                        };
                        if (!workspaceId) return [3, 2];
                        return [4, workspace_1.default.getById(workspaceId)];
                    case 1:
                        _a = _b.sent();
                        return [3, 4];
                    case 2: return [4, workspace_1.default.getDefault()];
                    case 3:
                        _a = _b.sent();
                        _b.label = 4;
                    case 4:
                        workspaceItem = (_a);
                        limits = workspaceItem && workspaceItem.org && workspaceItem.org.limits ? workspaceItem.org.limits : null;
                        if (limits && limits.notesPerWorkspace) {
                            response.notesPerWorkspace = limits.notesPerWorkspace;
                        }
                        if (workspaceItem && !workspaceItem.isNotesLimited) {
                            return [2, resolve(response)];
                        }
                        return [4, ModelItem.getCountForWorkspace({ workspaceId: workspaceId })];
                    case 5:
                        currentNotesCount = _b.sent();
                        if (typeof (currentNotesCount) === 'undefined') {
                            return [2, resolve(response)];
                        }
                        response.currentNotesCount = currentNotesCount;
                        if (!!limits) return [3, 7];
                        return [4, orgs_1.default.getLimits(workspaceId)];
                    case 6:
                        limits = (_b.sent());
                        _b.label = 7;
                    case 7:
                        if (!limits) {
                            return [2, resolve(response)];
                        }
                        notesPerWorkspace = limits.notesPerWorkspace;
                        if (typeof (notesPerWorkspace) === 'undefined') {
                            return [2, resolve(response)];
                        }
                        response.notesPerWorkspace = notesPerWorkspace;
                        response.canCreateNote = currentNotesCount < notesPerWorkspace;
                        return [2, resolve(response)];
                }
            });
        }); });
    };
    ModelItem.getCountForWorkspace = function (_a) {
        var workspaceId = _a.workspaceId;
        return new Promise(function (resolve) {
            ModelItem.count({
                type: 'note',
                role: {
                    "$nin": [
                        ModelItem.ITEM_SCREENSHOT,
                        ModelItem.ITEM_SCREENCAST,
                        ModelItem.ITEM_WELCOME_GUIDE,
                        ModelItem.ITEM_STREAMING_VIDEO,
                    ]
                },
                is_imported: {
                    '$ne': 1
                },
            }, { workspaceId: workspaceId }, function (err, countNotes) {
                if (err || !countNotes) {
                    return resolve(0);
                }
                return resolve(countNotes);
            });
        });
    };
    ModelItem.ITEM_SCREENSHOT = 'screenshot';
    ModelItem.ITEM_SCREENCAST = 'screencast';
    ModelItem.ITEM_WELCOME_GUIDE = 'welcome_guide';
    ModelItem.ITEM_STREAMING_VIDEO = 'streaming_video';
    return ModelItem;
}());
exports.default = ModelItem;
