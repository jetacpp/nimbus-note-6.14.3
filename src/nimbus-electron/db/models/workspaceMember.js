"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var dateHandler_1 = __importDefault(require("../../utilities/dateHandler"));
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var dbName = config_runtime_1.default.DB_WORKSPACE_MEMBERS_MODEL_NAME;
var ModelWorkspaceMember = (function () {
    function ModelWorkspaceMember() {
    }
    ModelWorkspaceMember.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelWorkspaceMember.count = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.count(data, options, callback);
    };
    ModelWorkspaceMember.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.findAll(data, options, callback);
    };
    ModelWorkspaceMember.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.workspaceId) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.prepareModelData = ModelWorkspaceMember.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelWorkspaceMember.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.workspaceId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelWorkspaceMember.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelWorkspaceMember.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (data.globalId) {
            options.dbName = dbName;
            pdb_1.default.remove(data, options, callback);
        }
        else {
            return callback(customError_1.default.wrongInput(), 0);
        }
    };
    ModelWorkspaceMember.getPublicData = function (doc) {
        var workspaceMember = {};
        if (doc) {
            workspaceMember.id = doc._id;
            workspaceMember.globalId = doc.globalId;
            workspaceMember.createdAt = doc.createdAt;
            workspaceMember.updatedAt = doc.updatedAt;
            if (doc.addedByUserId)
                workspaceMember.addedByUserId = doc.addedByUserId;
            if (doc.user)
                workspaceMember.user = doc.user;
            if (doc.workspaceId)
                workspaceMember.workspaceId = doc.workspaceId;
            if (doc.orgId)
                workspaceMember.orgId = doc.orgId;
            if (doc.type)
                workspaceMember.type = doc.type;
            if (doc.role)
                workspaceMember.role = doc.role;
            if (doc.encryptRole)
                workspaceMember.encryptRole = doc.encryptRole;
            if (doc.syncDate)
                workspaceMember.syncDate = doc.syncDate;
            if (doc.needSync)
                workspaceMember.needSync = doc.needSync;
        }
        return workspaceMember;
    };
    ModelWorkspaceMember.getResponseJson = function (data) {
        if (!data) {
            return null;
        }
        return {
            globalId: data.globalId,
            addedByUserId: data.addedByUserId,
            workspaceId: data.workspaceId,
            orgId: data.orgId,
            type: data.type,
            role: data.role,
            encryptRole: data.encryptRole ? data.encryptRole : '',
            createdAt: data.createdAt,
            updatedAt: data.updatedAt,
            email: data.user && data.user.email ? data.user.email.toLowerCase() : '',
            firstname: data.user ? data.user.firstname : '',
            lastname: data.user ? data.user.lastname : '',
            userId: data.user ? data.user.id : 0,
            username: data.user ? data.user.username : '',
        };
    };
    ModelWorkspaceMember.getResponseListJson = function (list) {
        var result = [];
        for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
            var data = list_1[_i];
            var jsonData = ModelWorkspaceMember.getResponseJson(data);
            result.push(jsonData);
        }
        return result;
    };
    ModelWorkspaceMember.prepareModelData = function (data) {
        var item = {};
        item = ModelWorkspaceMember.prepareCommonProperties(item, data);
        item = ModelWorkspaceMember.prepareSyncOnlyProperties(item, data);
        return item;
    };
    ModelWorkspaceMember.prepareCommonProperties = function (item, data) {
        var curDate = dateHandler_1.default.now();
        item.erised = data.erised || false;
        item.globalId = data.globalId || data.global_id || item._id || generatorHandler_1.default.randomString(16);
        item._id = item.globalId.toString();
        item.addedByUserId = data.addedByUserId || 0;
        item.user = data.user || {
            id: 0,
            email: null,
            username: '',
            firstname: null,
            lastname: null
        };
        item.workspaceId = data.workspaceId || '';
        item.orgId = data.orgId || '';
        item.type = data.type || ModelWorkspaceMember.TYPE_FULL;
        item.role = data.role || ModelWorkspaceMember.ROLE_READER;
        item.encryptRole = data.encryptRole || ModelWorkspaceMember.ENCRYPTION_ROLE_DENY;
        item.createdAt = data.createdAt || curDate;
        item.updatedAt = data.updatedAt || curDate;
        return item;
    };
    ModelWorkspaceMember.prepareSyncOnlyProperties = function (item, data) {
        item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
        item.syncDate = data.syncDate || 0;
        return item;
    };
    ModelWorkspaceMember.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelWorkspaceMember.getIndexList = function () {
        return ["globalId", "addedByUserId", "workspaceId", "orgId", "type", "role", "encryptRole", "updatedAt"];
    };
    ModelWorkspaceMember.TYPE_FULL = 'full';
    ModelWorkspaceMember.ROLE_MEMBER = 'member';
    ModelWorkspaceMember.ROLE_READER = 'reader';
    ModelWorkspaceMember.ROLE_EDITOR = 'editor';
    ModelWorkspaceMember.ROLE_ADMIN = 'admin';
    ModelWorkspaceMember.ENCRYPTION_ROLE_ACCESSOR = 'encryptionAccessor';
    ModelWorkspaceMember.ENCRYPTION_ROLE_ENCRYPTOR = 'encryptor';
    ModelWorkspaceMember.ENCRYPTION_ROLE_MANGER = 'encryptionManager';
    ModelWorkspaceMember.ENCRYPTION_ROLE_DENY = 'encryptionDeny';
    return ModelWorkspaceMember;
}());
exports.default = ModelWorkspaceMember;
