"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var dateHandler_1 = __importDefault(require("../../utilities/dateHandler"));
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var dbName = config_runtime_1.default.DB_TODO_MODEL_NAME;
var ModelTodo = (function () {
    function ModelTodo() {
    }
    ModelTodo.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelTodo.count = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.count(data, options, callback);
    };
    ModelTodo.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.findAll(data, options, callback);
    };
    ModelTodo.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.noteGlobalId || !data.label) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.prepareModelData = ModelTodo.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelTodo.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.noteGlobalId || !data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelTodo.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelTodo.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (data.globalId || data.noteGlobalId) {
            options.dbName = dbName;
            pdb_1.default.remove(data, options, callback);
        }
        else {
            return callback(customError_1.default.wrongInput(), 0);
        }
    };
    ModelTodo.removeByItemId = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.noteGlobalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelTodo.getPublicData = function (doc) {
        var todo = {};
        if (doc) {
            todo.id = doc._id;
            todo.globalId = doc.globalId;
            todo.noteGlobalId = doc.noteGlobalId;
            todo.date = doc.date;
            todo.checked = doc.checked;
            todo.label = doc.label;
            todo.dateUpdated = doc.dateUpdated;
            if (doc.parentId)
                todo.parentId = doc.parentId;
            if (doc.dateAdded)
                todo.dateAdded = doc.dateAdded;
            if (doc.uniqueUserName)
                todo.uniqueUserName = doc.uniqueUserName;
            if (doc.syncDate)
                todo.syncDate = doc.syncDate;
            if (doc.needSync)
                todo.needSync = doc.needSync;
            if (doc.orderNumber)
                todo.orderNumber = doc.orderNumber;
        }
        return todo;
    };
    ModelTodo.getResponseJson = function (data) {
        if (!data) {
            return null;
        }
        return {
            checked: data.checked,
            date: data.date,
            dateUpdated: data.dateUpdated,
            globalId: data.globalId,
            label: data.label,
            noteGlobalId: data.noteGlobalId
        };
    };
    ModelTodo.getResponseListJson = function (list) {
        return list.map(function (data) {
            return ModelTodo.getResponseJson(data);
        });
    };
    ModelTodo.prepareModelData = function (data) {
        var item = {};
        item = ModelTodo.prepareCommonProperties(item, data);
        item = ModelTodo.prepareSyncOnlyProperties(item, data);
        return item;
    };
    ModelTodo.prepareCommonProperties = function (item, data) {
        var curDate = dateHandler_1.default.now();
        item.erised = data.erised || false;
        item.globalId = data.globalId || data.global_id || item._id || generatorHandler_1.default.randomString(16);
        item._id = item.globalId.toString();
        item.noteGlobalId = data.noteGlobalId || data.parentId || "";
        item.date = data.date || dateHandler_1.default.now();
        item.checked = !!data.checked;
        item.label = data.label;
        item.dateUpdated = data.dateUpdated || curDate || 0;
        if (typeof (data.orderNumber) !== "undefined") {
            item.orderNumber = data.orderNumber;
        }
        return item;
    };
    ModelTodo.prepareSyncOnlyProperties = function (item, data) {
        var curDate = dateHandler_1.default.now();
        item.global_id = data.global_id || data.globalId || item._id;
        item.parentId = data.parentId || item.noteGlobalId || "";
        item.dateAdded = data.dateAdded || curDate || 0;
        item.uniqueUserName = data.uniqueUserName || "";
        item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
        item.syncDate = data.syncDate || 0;
        if (typeof (data.orderNumber) !== "undefined") {
            item.orderNumber = data.orderNumber;
        }
        return item;
    };
    ModelTodo.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelTodo.getIndexList = function () {
        return ["globalId", "noteGlobalId", "checked", "label", "orderNumber"];
    };
    return ModelTodo;
}());
exports.default = ModelTodo;
