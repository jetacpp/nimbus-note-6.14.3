"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var tag_1 = __importDefault(require("./tag"));
var dbName = config_runtime_1.default.DB_NOTE_TAGS_MODEL_NAME;
var ModelNoteTags = (function () {
    function ModelNoteTags() {
    }
    ModelNoteTags.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelNoteTags.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.findAll(data, options, callback);
    };
    ModelNoteTags.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.tagGlobalId || !data.noteGlobalId) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.prepareModelData = ModelNoteTags.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelNoteTags.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.tagGlobalId || !data.noteGlobalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelNoteTags.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (data.tagGlobalId || data.noteGlobalId) {
            options.dbName = dbName;
            options.updateFields = pdb_1.default.getErasedUpdateFields();
            pdb_1.default.update(data, options, callback);
        }
        else {
            return callback(customError_1.default.wrongInput(), 0);
        }
    };
    ModelNoteTags.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (data.tagGlobalId || data.noteGlobalId) {
            options.dbName = dbName;
            pdb_1.default.remove(data, options, callback);
        }
        else {
            return callback(customError_1.default.wrongInput(), 0);
        }
    };
    ModelNoteTags.removeByItemId = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.noteGlobalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelNoteTags.getPublicData = function (doc) {
        var tag = {};
        if (doc) {
            tag.id = doc._id;
            tag.tagGlobalId = doc.tagGlobalId;
            tag.noteGlobalId = doc.noteGlobalId;
            tag.dateUpdated = doc.dateUpdated;
            if (doc.dateAdded)
                tag.dateAdded = doc.dateAdded;
            if (doc.parentId)
                tag.parentId = doc.parentId;
            if (doc.syncDate)
                tag.syncDate = doc.syncDate;
            if (doc.needSync)
                tag.needSync = doc.needSync;
        }
        return tag;
    };
    ModelNoteTags.prepareModelData = function (data) {
        var item = {};
        item = ModelNoteTags.prepareCommonProperties(item, data);
        item = ModelNoteTags.prepareSyncOnlyProperties(item, data);
        return item;
    };
    ModelNoteTags.prepareCommonProperties = function (item, data) {
        item.erised = data.erised || false;
        item._id = data._id || generatorHandler_1.default.randomString(16);
        item.tagGlobalId = data.tagGlobalId;
        item.noteGlobalId = data.noteGlobalId || data.parentId;
        item.dateUpdated = data.dateUpdated;
        return item;
    };
    ModelNoteTags.prepareSyncOnlyProperties = function (item, data) {
        item.dateAdded = data.dateAdded || 0;
        item.parentId = data.parentId || data.noteGlobalId || "";
        item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
        item.syncDate = data.syncDate || 0;
        return item;
    };
    ModelNoteTags.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelNoteTags.getIndexList = function () {
        return ["tagGlobalId", "noteGlobalId", "dateUpdated"];
    };
    ModelNoteTags.getAllNoteTagItems = function (_a) {
        var noteGlobalId = _a.noteGlobalId, workspaceId = _a.workspaceId;
        return new Promise(function (resolve) {
            ModelNoteTags.findAll({ noteGlobalId: noteGlobalId }, { workspaceId: workspaceId }, function (err, noteTagsItem) {
                var noteTagsList = noteTagsItem ? noteTagsItem : [];
                if (!noteTagsList.length) {
                    return resolve([]);
                }
                var noteTagsItemsIdList = noteTagsList.map(function (noteTag) { return noteTag.tagGlobalId; });
                tag_1.default.findAll({ globalId: { "$in": noteTagsItemsIdList } }, { workspaceId: workspaceId }, function (err, tagItem) {
                    var tagsList = tagItem ? tagItem : [];
                    if (!tagsList.length) {
                        return resolve([]);
                    }
                    return resolve(tagsList);
                });
            });
        });
    };
    ModelNoteTags.getAllNoteTagItemsTitle = function (_a) {
        var noteGlobalId = _a.noteGlobalId, workspaceId = _a.workspaceId;
        return __awaiter(this, void 0, void 0, function () {
            var tagsList;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4, ModelNoteTags.getAllNoteTagItems({ noteGlobalId: noteGlobalId, workspaceId: workspaceId })];
                    case 1:
                        tagsList = _b.sent();
                        return [2, tagsList.map(function (tag) { return tag.title; })];
                }
            });
        });
    };
    return ModelNoteTags;
}());
exports.default = ModelNoteTags;
