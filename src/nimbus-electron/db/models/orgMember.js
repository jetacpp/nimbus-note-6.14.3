"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var dateHandler_1 = __importDefault(require("../../utilities/dateHandler"));
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var dbName = config_runtime_1.default.DB_ORG_MEMBERS_MODEL_NAME;
var ModelOrgMember = (function () {
    function ModelOrgMember() {
    }
    ModelOrgMember.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelOrgMember.count = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.count(data, options, callback);
    };
    ModelOrgMember.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.findAll(data, options, callback);
    };
    ModelOrgMember.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.orgId) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.prepareModelData = ModelOrgMember.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelOrgMember.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.orgId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelOrgMember.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelOrgMember.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (data.globalId) {
            options.dbName = dbName;
            pdb_1.default.remove(data, options, callback);
        }
        else {
            return callback(customError_1.default.wrongInput(), 0);
        }
    };
    ModelOrgMember.getPublicData = function (doc) {
        var orgMember = {};
        if (doc) {
            orgMember.id = doc._id;
            orgMember.globalId = doc.globalId;
            orgMember.createdAt = doc.createdAt;
            orgMember.updatedAt = doc.updatedAt;
            if (doc.orgId)
                orgMember.orgId = doc.orgId;
            if (doc.role)
                orgMember.role = doc.role;
            if (doc.disabled)
                orgMember.disabled = doc.disabled;
            if (doc.user)
                orgMember.user = doc.user;
            if (doc.syncDate)
                orgMember.syncDate = doc.syncDate;
            if (doc.needSync)
                orgMember.needSync = doc.needSync;
        }
        return orgMember;
    };
    ModelOrgMember.getResponseJson = function (data) {
        if (!data) {
            return null;
        }
        return {
            globalId: data.globalId,
            orgId: data.orgId,
            role: data.role,
            disabled: data.disabled,
            user: data.user,
            createdAt: data.createdAt,
            updatedAt: data.updatedAt
        };
    };
    ModelOrgMember.prepareModelData = function (data) {
        var item = {};
        item = ModelOrgMember.prepareCommonProperties(item, data);
        item = ModelOrgMember.prepareSyncOnlyProperties(item, data);
        return item;
    };
    ModelOrgMember.prepareCommonProperties = function (item, data) {
        var curDate = dateHandler_1.default.now();
        item.erised = data.erised || false;
        item.globalId = data.globalId || data.global_id || item._id || generatorHandler_1.default.randomString(16);
        item._id = item.globalId.toString();
        item.orgId = data.orgId || '';
        item.role = data.role || ModelOrgMember.TYPE_USER;
        item.disabled = data.disabled || '';
        item.user = data.user || {
            id: 0,
            email: null,
            username: '',
            firstname: null,
            lastname: null
        };
        item.createdAt = data.createdAt || curDate;
        item.updatedAt = data.updatedAt || curDate;
        return item;
    };
    ModelOrgMember.prepareSyncOnlyProperties = function (item, data) {
        item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
        item.syncDate = data.syncDate || 0;
        return item;
    };
    ModelOrgMember.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelOrgMember.getIndexList = function () {
        return ["globalId", "orgId", "role", "disabled", "updatedAt"];
    };
    ModelOrgMember.TYPE_ADMIN = 'admin';
    ModelOrgMember.TYPE_OWNER = 'owner';
    ModelOrgMember.TYPE_MANAGER = 'manager';
    ModelOrgMember.TYPE_USER = 'user';
    return ModelOrgMember;
}());
exports.default = ModelOrgMember;
