"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var dbName = config_runtime_1.default.DB_TAG_MODEL_NAME;
var ModelTag = (function () {
    function ModelTag() {
    }
    ModelTag.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelTag.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.findAll(data, options, callback);
    };
    ModelTag.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.tag) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.prepareModelData = ModelTag.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelTag.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.tag) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelTag.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.tag) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelTag.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (data.tag || data.globalId) {
            options.dbName = dbName;
            pdb_1.default.remove(data, options, callback);
        }
        else {
            return callback(customError_1.default.wrongInput(), 0);
        }
    };
    ModelTag.getPublicData = function (doc) {
        var tag = {};
        if (doc) {
            tag.id = doc._id;
            tag.globalId = doc.globalId;
            tag.tag = doc.tag;
            tag.dateUpdated = doc.dateUpdated;
            if (doc.title || doc.tag)
                tag.title = doc.title || doc.tag || "";
            if (doc.oldTitle)
                tag.oldTitle = doc.oldTitle;
            if (doc.dateAdded)
                tag.dateAdded = doc.dateAdded;
            if (doc.uniqueUserName)
                tag.uniqueUserName = doc.uniqueUserName;
            if (doc.syncDate)
                tag.syncDate = doc.syncDate;
            if (doc.parentId)
                tag.parentId = doc.parentId;
            if (doc.needSync)
                tag.needSync = doc.needSync;
            if (doc.isChecked)
                tag.isChecked = doc.isChecked;
            if (doc.notesCount)
                tag.notesCount = doc.notesCount;
        }
        return tag;
    };
    ModelTag.prepareModelData = function (data) {
        var item = {};
        item = ModelTag.prepareCommonProperties(item, data);
        item = ModelTag.prepareSyncOnlyProperties(item, data);
        return item;
    };
    ModelTag.prepareCommonProperties = function (item, data) {
        item.erised = data.erised || false;
        item._id = data._id || generatorHandler_1.default.randomString(16);
        item.globalId = item._id.toString();
        item.tag = data.tag || data.title || "";
        item.dateUpdated = data.dateUpdated;
        return item;
    };
    ModelTag.prepareSyncOnlyProperties = function (item, data) {
        item.tag = data.title || data.tag || "";
        item.title = data.title || data.tag || "";
        item.oldTitle = data.oldTitle || "";
        item.dateAdded = data.dateAdded || 0;
        item.uniqueUserName = data.uniqueUserName || "";
        item.parentId = data.parentId || "";
        item.needSync = data.needSync || false;
        item.syncDate = data.syncDate || 0;
        item.isChecked = !!data.isChecked;
        item.notesCount = data.notesCount || 0;
        return item;
    };
    ModelTag.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelTag.getIndexList = function () {
        return ["globalId", "tag", "dateUpdated"];
    };
    return ModelTag;
}());
exports.default = ModelTag;
