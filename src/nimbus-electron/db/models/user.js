"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var dateHandler_1 = __importDefault(require("../../utilities/dateHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var getTrial_1 = __importDefault(require("../../sync/process/rx/handlers/getTrial"));
var NimbusSDK_1 = __importDefault(require("../../sync/nimbussdk/net/NimbusSDK"));
var syncPropsHandler_1 = require("../../utilities/syncPropsHandler");
var userVariables_1 = __importDefault(require("../../sync/process/rx/handlers/userVariables"));
var orgs_1 = __importDefault(require("./orgs"));
var urlParser_1 = __importDefault(require("../../utilities/urlParser"));
var fs_extra_1 = __importDefault(require("fs-extra"));
var dbName = config_runtime_1.default.DB_USER_MODEL_NAME;
var ModelUser = (function () {
    function ModelUser() {
    }
    ModelUser.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (options && options.useLocalSession) {
            if (!data.localSession) {
                return callback(customError_1.default.wrongInput(), null);
            }
        }
        else {
            if (data.login) {
                data.email = data.login ? data.login.toLowerCase() : '';
                delete data.login;
            }
        }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelUser.exist = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options.dbName = dbName;
        pdb_1.default.find(data, options, function (err, userItem) {
            if (err) {
                return callback(err, false);
            }
            callback(err, !!userItem);
        });
    };
    ModelUser.create = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.login) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.prepareModelData = ModelUser.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelUser.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!(data.userId || data.email)) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelUser.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.login) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        data = { email: data.login };
        options.dbName = dbName;
        pdb_1.default.remove(data, options, callback);
    };
    ModelUser.getPublicData = function (doc) {
        var user = {};
        if (doc) {
            user.id = doc._id;
            user.userId = doc.userId;
            user.language = doc.language;
            user.email = doc.email ? doc.email.toLowerCase() : '';
            user.firstname = doc.firstname;
            user.lastname = doc.lastname;
            user.dateTimeLocale = doc.dateTimeLocale || '';
            user.avatar = doc.avatar;
            user.username = doc.username;
            user.subscribe = doc.subscribe;
            user.languages = doc.languages;
            user.settings = doc.settings || {};
            user.variables = doc.variables || ModelUser.getDefaultVariables();
            user.authProvider = doc.authProvider || '';
        }
        return user;
    };
    ModelUser.getPublicInfo = function (doc) {
        var info = {};
        if (doc) {
            info.userId = doc.userId;
            info.email = doc.email ? doc.email.toLowerCase() : '';
            info.firstname = doc.firstname;
            info.lastname = doc.lastname;
            info.dateTimeLocale = doc.dateTimeLocale || '';
            info.avatar = doc.avatar;
            info.languages = doc.languages;
            info.notesEmail = doc.notesEmail;
            info.dateNextQuotaReset = doc.dateNextQuotaReset;
            info.quotaResetDate = doc.quotaResetDate;
            info.subscribe = doc.subscribe;
            info.paymentStartDate = doc.paymentStartDate;
            info.paymentEndDate = doc.paymentEndDate;
            info.paymentSystemCode = doc.paymentSystemCode;
            info.settings = doc.settings || {};
            info.variables = doc.variables || ModelUser.getDefaultVariables();
            info.authProvider = doc.authProvider || '';
            if (doc.syncDate)
                info.syncDate = doc.syncDate;
            if (doc.needSync)
                info.needSync = doc.needSync;
        }
        return info;
    };
    ModelUser.getPublicUsage = function (doc) {
        var usage = {};
        if (doc) {
            usage.current = doc.usageCurrent;
            usage.max = doc.usageMax;
        }
        return usage;
    };
    ModelUser.getPublicTrial = function (doc) {
        if (doc.trial) {
            return doc.trial;
        }
        return ModelUser.getDefaultTrial();
    };
    ModelUser.getDefaultTrial = function () {
        return {
            dateStart: 0,
            dateUntil: 0,
            daysRemain: 0,
            requestorService: null,
            state: null
        };
    };
    ModelUser.getTrialAsync = function (userId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var findQuery = { userId: userId };
                        ModelUser.find(findQuery, {}, function (err, userItem) {
                            if (err || !userItem) {
                                return resolve(ModelUser.getDefaultTrial());
                            }
                            var apiTrial = ModelUser.getPublicTrial(userItem);
                            return resolve(apiTrial);
                        });
                    })];
            });
        });
    };
    ModelUser.getProfileAsync = function (email) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var findQuery = { login: email };
                        ModelUser.find(findQuery, {}, function (err, userItem) { return __awaiter(_this, void 0, void 0, function () {
                            var org, response, url, routeParams, storedFileUUID, targetPath, exists;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (err || !userItem) {
                                            return [2, resolve(null)];
                                        }
                                        return [4, orgs_1.default.getActive()];
                                    case 1:
                                        org = _a.sent();
                                        response = {};
                                        response.email = userItem.email ? userItem.email.toLowerCase() : '';
                                        response.id = userItem.id || 0;
                                        response.language = userItem.language || "";
                                        response.userId = org && org.user ? org.user.id : userItem.userId;
                                        response.username = userItem.username || "";
                                        response.firstname = userItem.firstname;
                                        response.lastname = userItem.lastname;
                                        response.notesEmail = "";
                                        if (userItem.notesEmail) {
                                            response.notesEmail = ModelUser.getNotesEmailPart(userItem.notesEmail);
                                        }
                                        if (userItem.dateNextQuotaReset) {
                                            response.dateNextQuotaReset = userItem.dateNextQuotaReset;
                                        }
                                        if (userItem.quotaResetDate) {
                                            response.quotaResetDate = userItem.quotaResetDate;
                                        }
                                        if (!userItem.avatar) return [3, 3];
                                        url = userItem.avatar.url;
                                        if (!url) return [3, 3];
                                        routeParams = urlParser_1.default.getPathParams(url);
                                        storedFileUUID = typeof (routeParams[2] !== 'undefined') ? routeParams[2] : null;
                                        if (!storedFileUUID) return [3, 3];
                                        targetPath = pdb_1.default.getClientAttachmentPath() + "/" + storedFileUUID;
                                        return [4, fs_extra_1.default.exists(targetPath)];
                                    case 2:
                                        exists = _a.sent();
                                        if (exists) {
                                            response.avatarFileUUID = "avatar/" + storedFileUUID;
                                        }
                                        _a.label = 3;
                                    case 3: return [2, resolve(response)];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    ModelUser.syncUserTrial = function () {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                getTrial_1.default({ saveUserTrial: true, skipSyncHandlers: true }, function () {
                    return resolve();
                });
                return [2];
            });
        }); });
    };
    ModelUser.getPublicPremium = function (userInfo, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var premium = { message: "premium not found", httpStatus: 404 };
        if (!userInfo) {
            return callback(null, premium);
        }
        if (!userInfo.email) {
            return callback(null, premium);
        }
        ModelUser.find({ login: userInfo.email.toLowerCase() }, {}, function (err, userInstance) {
            if (err || !userInstance) {
                return callback(null, premium);
            }
            if (!userInstance.subscribe) {
                return callback(null, premium);
            }
            var endDate = "";
            if (userInstance.paymentEndDate) {
                endDate = userInstance.paymentEndDate;
            }
            var paymentSystemCode = "";
            if (userInstance.paymentSystemCode) {
                paymentSystemCode = userInstance.paymentSystemCode;
            }
            var startDate = "";
            if (userInstance.paymentStartDate) {
                startDate = userInstance.paymentStartDate;
            }
            var status = "";
            if (userInstance.subscribe) {
                status = "active";
            }
            premium = {
                httpStatus: 200,
                endDate: endDate,
                paymentSystemCode: paymentSystemCode,
                startDate: startDate,
                status: status,
                userId: userInstance.userId
            };
            callback(null, premium);
        });
    };
    ModelUser.getNotesEmailPart = function (email) {
        return (email ? email.split("@", 1)[0] : "");
    };
    ModelUser.getDateNextQuotaReset = function (days) {
        var timestamp = parseInt(new Date().getTime() / 1000);
        if (days) {
            timestamp = timestamp + (days * 86400);
        }
        return timestamp;
    };
    ModelUser.prepareModelData = function (data) {
        var user = {};
        var userLoginPart = data.login.split('@');
        var userBaseName = typeof (userLoginPart[0]) !== "undefined" ? userLoginPart[0] : data.login;
        var userName = userBaseName + "-" + generatorHandler_1.default.randomString(6);
        var noteEmail = "";
        user.erised = false;
        user.userId = parseInt(dateHandler_1.default.now() + generatorHandler_1.default.randomString(2, '0123456789'));
        user._id = user.userId.toString();
        user.language = "en";
        user.email = data.login ? data.login.toLowerCase() : '';
        user.password = data.password;
        if (typeof (data.firstname) !== 'undefined') {
            user.firstname = data.firstname;
        }
        if (typeof (data.lastname) !== 'undefined') {
            user.lastname = data.lastname;
        }
        if (typeof (data.dateTimeLocale) !== 'undefined') {
            user.dateTimeLocale = data.dateTimeLocale;
        }
        if (typeof (data.avatar) !== 'undefined') {
            user.avatar = data.avatar;
        }
        if (typeof (data.languages) !== 'undefined') {
            user.languages = data.languages;
        }
        user.username = userName;
        user.localSession = '';
        user.notesEmail = noteEmail;
        user.popupLimitNotice = false;
        user.popupLimitError = false;
        user.usageCurrent = 0;
        user.usageMax = ModelUser.MAX_UPLOAD_SIZE;
        user.dateNextQuotaReset = 0;
        user.quotaResetDate = 0;
        user.subscribe = data.subscribe || 0;
        user.paymentStartDate = data.paymentStartDate || "";
        user.paymentEndDate = data.paymentEndDate || "";
        user.paymentSystemCode = data.paymentSystemCode || "";
        user.syncDate = data.syncDate || 0;
        user.needSync = data.needSync || true;
        user.settings = data.settings || {};
        user.variables = data.variables || ModelUser.getDefaultVariables();
        user.authProvider = data.authProvider || '';
        return user;
    };
    ModelUser.prepareName = function (email, id) {
        var result = null;
        if (email && id) {
            var pos = email.indexOf('@');
            if (pos >= 0) {
                var userName = email.substr(0, pos);
                if (userName) {
                    result = userName + '_' + id;
                }
            }
        }
        return result;
    };
    ModelUser.getIndexList = function () {
        return ["userId", "language", "email", "password", "firstname",
            "lastname", "dateTimeLocale", "avatar", "languages", "username",
            "subscribe", "localSession", "dateNextQuotaReset",
            "quotaResetDate", "notesEmail", "usageCurrent", "usageMax",
            "settings", "variables", "authProvider"];
    };
    ModelUser.saveUserSettings = function (authInfo, name, value) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var curDate, settings, newSettings, queryData, saveUserData;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!authInfo) {
                            return [2, resolve(null)];
                        }
                        curDate = dateHandler_1.default.now();
                        return [4, ModelUser.getUserSettings(authInfo)];
                    case 1:
                        settings = _b.sent();
                        newSettings = __assign(__assign({}, settings), (_a = {}, _a[name] = value, _a));
                        queryData = { email: authInfo.email };
                        saveUserData = { settings: newSettings };
                        ModelUser.update(queryData, syncPropsHandler_1.setUpdateProps(saveUserData, curDate), {}, function (err, count) {
                            if (err || !count) {
                                return resolve(null);
                            }
                            return resolve(newSettings);
                        });
                        return [2];
                }
            });
        }); });
    };
    ModelUser.getDefaultVariables = function () {
        return {
            ignoreIntro: false,
            ignoreSharesInfo: false,
            ignoreStartTooltips: false,
            noteAppearance: {
                "style": "normal",
                "size": "normal"
            },
            colorTheme: 0,
            helpTooltipsStartShown: false,
            helpTooltipsNoteShown: false,
            desktopAppsInformerShown: false,
            lastOpenedWorkspaces: {},
            isPinnedLeftBlock: true,
        };
    };
    ModelUser.getUserSettings = function (authInfo) {
        return new Promise(function (resolve) {
            var queryData = { email: authInfo.email };
            ModelUser.find(queryData, {}, function (err, userInstance) {
                if (err || !userInstance) {
                    return resolve({});
                }
                var settings = userInstance.settings;
                if (!settings) {
                    return resolve({});
                }
                return resolve(settings);
            });
        });
    };
    ModelUser.getAccountUserId = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        NimbusSDK_1.default.getAccountManager().getUserId(function (err, userId) {
                            if (!userId) {
                                return resolve(null);
                            }
                            return resolve(userId);
                        });
                    })];
            });
        });
    };
    ModelUser.syncUserVariables = function () {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                userVariables_1.default({ saveUserVariables: true, skipSyncHandlers: true }, function () {
                    return resolve();
                });
                return [2];
            });
        }); });
    };
    ModelUser.WARNING_UPLOAD_SIZE = 89128960;
    ModelUser.MAX_UPLOAD_SIZE = 104857600;
    ModelUser.MAX_UPLOAD_SIZE_PREMIUM = 5368709120;
    return ModelUser;
}());
exports.default = ModelUser;
