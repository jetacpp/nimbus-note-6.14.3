"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs-extra");
var fileKind = require("file-kind");
var dateHandler_1 = __importDefault(require("../../utilities/dateHandler"));
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var orgs_1 = __importDefault(require("./orgs"));
var dbName = config_runtime_1.default.DB_ATTACH_MODEL_NAME;
var ModelAttachment = (function () {
    function ModelAttachment() {
    }
    ModelAttachment.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelAttachment.count = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.count(data, options, callback);
    };
    ModelAttachment.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.findAll(data, options, callback);
    };
    ModelAttachment.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.noteGlobalId || typeof (data.displayName) === "undefined") {
            return callback(customError_1.default.wrongInput(), null);
        }
        if (!data.displayName) {
            data.displayName = ModelAttachment.getDefaultDisplayName();
        }
        options.dbName = dbName;
        options.prepareModelData = ModelAttachment.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelAttachment.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId || !(updateFields.displayName || updateFields.location)) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelAttachment.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelAttachment.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        pdb_1.default.remove(data, options, callback);
    };
    ModelAttachment.removeByItemId = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.noteGlobalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelAttachment.generateStoredFileUUID = function () {
        return generatorHandler_1.default.generateGUID();
    };
    ModelAttachment.getTypeByMimeType = function (mime) {
        var type = fileKind(mime);
        return type ? type : 'file';
    };
    ModelAttachment.getDefaultDisplayName = function () {
        return "Unnamed Attach";
    };
    ModelAttachment.getPublicData = function (doc) {
        var attach = {};
        if (doc) {
            attach.id = doc._id;
            attach.globalId = doc.globalId;
            attach.noteGlobalId = doc.noteGlobalId;
            attach.dateAdded = doc.dateAdded;
            attach.dateUpdated = doc.dateUpdated;
            attach.displayName = doc.displayName;
            attach.extra = doc.extra;
            attach.inList = doc.inList;
            attach.isEncrypted = doc.isEncrypted;
            attach.isScreenshot = doc.isScreenshot;
            attach.mime = doc.mime;
            attach.role = doc.role;
            attach.size = doc.size;
            attach.storedFileUUID = doc.storedFileUUID;
            attach.type = doc.type;
            if (doc.parentId || doc.noteGlobalId) {
                attach.parentId = doc.parentId || doc.noteGlobalId;
            }
            if (doc.syncDate)
                attach.syncDate = doc.syncDate;
            if (doc.uniqueUserName)
                attach.uniqueUserName = doc.uniqueUserName;
            if (doc.needSync)
                attach.needSync = doc.needSync;
            if (doc.location)
                attach.location = doc.location;
            if (doc.tempName)
                attach.tempName = doc.tempName;
            if (doc.typeExtra)
                attach.typeExtra = doc.typeExtra;
            if (doc.extension)
                attach.extension = doc.extension;
            if (doc.oldDisplayName)
                attach.oldDisplayName = doc.oldDisplayName;
            if (doc.isAttachedToNote)
                attach.isAttachedToNote = doc.isAttachedToNote;
            if (doc.fileUUID)
                attach.fileUUID = doc.storedFileUUID;
            if (doc.isDownloaded)
                attach.isDownloaded = doc.isDownloaded;
            if (doc.localPath)
                attach.localPath = doc.localPath;
            if (doc.isDownloadingRunning)
                attach.isDownloadingRunning = doc.isDownloadingRunning;
            if (doc.currentProgress)
                attach.currentProgress = doc.currentProgress;
            if (doc.currentProgress)
                attach.tryDownloadCounter = doc.tryDownloadCounter || 0;
        }
        return attach;
    };
    ModelAttachment.getResponseJson = function (data) {
        if (!data) {
            return null;
        }
        return {
            dateAdded: data.dateAdded,
            dateUpdated: data.dateUpdated,
            displayName: data.displayName,
            displayUrl: data.location ? ModelAttachment.convertDisplayUrl(data.location) : '',
            extra: data.extra,
            globalId: data.globalId,
            inList: data.inList,
            isEncrypted: data.isEncrypted,
            isScreenshot: data.isScreenshot,
            mime: data.mime,
            noteGlobalId: data.noteGlobalId,
            role: data.role,
            size: data.size,
            storedFileUUID: data.storedFileUUID,
            type: data.type,
            isDownloaded: data.isDownloaded,
            tryDownloadCounter: data.tryDownloadCounter,
            needSync: data.needSync
        };
    };
    ModelAttachment.getResponseListJson = function (list) {
        return list.map(function (data) {
            return ModelAttachment.getResponseJson(data);
        });
    };
    ModelAttachment.prepareModelData = function (data) {
        var item = {};
        item = ModelAttachment.prepareCommonProperties(item, data);
        item = ModelAttachment.prepareSyncOnlyProperties(item, data);
        return item;
    };
    ModelAttachment.prepareCommonProperties = function (item, data) {
        var curDate = dateHandler_1.default.now();
        item.erised = data.erised || false;
        item.globalId = data.globalId || data.global_id || item._id || generatorHandler_1.default.randomString(16);
        item._id = item.globalId.toString();
        item.noteGlobalId = data.noteGlobalId || data.parentId || "";
        item.dateAdded = data.dateAdded || data.date_added || curDate;
        item.dateUpdated = data.dateUpdated || data.date_updated || curDate;
        item.displayName = data.display_name || data.displayName || "";
        item.extra = data.extra || {};
        item.inList = !!(data.inList || data.in_list || false);
        item.isEncrypted = !!(data.isEncrypted || data.is_encrypted || false);
        item.isScreenshot = !!data.isScreenshot;
        item.mime = data.mime || "text/plain";
        item.role = data.role || "attachment";
        item.size = data.size || 0;
        if (!item.storedFileUUID) {
            item.storedFileUUID = data.storedFileUUID || data.file_uuid || data.fileUUID || ModelAttachment.generateStoredFileUUID();
        }
        item.type = data.type || ModelAttachment.getTypeByMimeType(data.mime);
        item.hashKey = data.hashKey;
        if (data.tempName) {
            item.tempName = data.tempName || "";
        }
        item.tryDownloadCounter = data.tryDownloadCounter || 0;
        if (typeof (data.isDownloaded) !== "undefined") {
            item.isDownloaded = !!data.isDownloaded;
        }
        return item;
    };
    ModelAttachment.prepareSyncOnlyProperties = function (item, data) {
        item.global_id = data.global_id || data.globalId || item._id;
        item.parentId = data.parentId || data.noteGlobalId || "";
        item.displayName = data.display_name || data.displayName || "";
        item.uniqueUserName = data.uniqueUserName || "";
        item.syncDate = data.syncDate || 0;
        item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
        item.location = data.location || "";
        item.tempName = data.tempName || "";
        item.typeExtra = data.typeExtra || "";
        item.extension = data.extension || "";
        item.oldDisplayName = data.oldDisplayName || "";
        item.isAttachedToNote = !!(data.isAttachedToNote || false);
        item.fileUUID = data.file_uuid || data.fileUUID || data.storedFileUUID || ModelAttachment.generateStoredFileUUID();
        if (typeof (data.isDownloaded) !== "undefined") {
            item.isDownloaded = !!data.isDownloaded;
        }
        item.localPath = data.localPath || "";
        item.isDownloadingRunning = !!(data.isDownloadingRunning || false);
        item.currentProgress = data.currentProgress || 0;
        item.tryDownloadCounter = data.tryDownloadCounter || 0;
        return item;
    };
    ModelAttachment.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelAttachment.getIndexList = function () {
        return ["globalId", "noteGlobalId", "dateAdded", "dateUpdated", "displayName", "inList",
            "isEncrypted", "isScreenshot", "mime", "role", "size", "storedFileUUID", "type", "hashKey"];
    };
    ModelAttachment.checkFileUploadLimit = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var size, maxSize;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        size = inputData.size;
                        return [4, ModelAttachment.getFileUploadLimit(inputData)];
                    case 1:
                        maxSize = _a.sent();
                        return [2, size <= maxSize];
                }
            });
        });
    };
    ModelAttachment.getFileUploadLimit = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, getLimitNotesMaxAttachmentSize;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId;
                        getLimitNotesMaxAttachmentSize = function () { return __awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                        var limits, maxLimit;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0: return [4, orgs_1.default.getLimits(workspaceId)];
                                                case 1:
                                                    limits = _a.sent();
                                                    maxLimit = limits && limits.attachmentSize ? limits.attachmentSize : ModelAttachment.MAX_UPLOAD_FILE_SIZE;
                                                    return [2, resolve(maxLimit)];
                                            }
                                        });
                                    }); })];
                            });
                        }); };
                        return [4, getLimitNotesMaxAttachmentSize()];
                    case 1: return [2, _a.sent()];
                }
            });
        });
    };
    ModelAttachment.getAttachmentPath = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                        var queryData = { globalId: globalId };
                        ModelAttachment.find(queryData, { workspaceId: workspaceId }, function (err, attachItem) { return __awaiter(_this, void 0, void 0, function () {
                            var targetPath, exists;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!(attachItem && Object.keys(attachItem).length)) return [3, 2];
                                        if (!pdb_1.default.getClientAttachmentPath()) {
                                            return [2, resolve(null)];
                                        }
                                        targetPath = pdb_1.default.getClientAttachmentPath() + "/" + attachItem.storedFileUUID;
                                        return [4, fs.exists(targetPath)];
                                    case 1:
                                        exists = _a.sent();
                                        if (!exists) {
                                            return [2, resolve(null)];
                                        }
                                        return [2, resolve(targetPath)];
                                    case 2: return [2, resolve(null)];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    ModelAttachment.convertDisplayUrl = function (location) {
        if (!location) {
            return location;
        }
        return location.replace(config_runtime_1.default.everhelperAttachmentBoxUrl, config_runtime_1.default.nimbusAttachmentBoxUrl);
    };
    ModelAttachment.MAX_UPLOAD_FILE_SIZE = 10485760;
    ModelAttachment.MAX_UPLOAD_FILE_SIZE_PREMIUM = 1073741824;
    return ModelAttachment;
}());
exports.default = ModelAttachment;
