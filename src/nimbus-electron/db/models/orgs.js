"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var dateHandler_1 = __importDefault(require("../../utilities/dateHandler"));
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var workspace_1 = __importDefault(require("./workspace"));
var user_1 = __importDefault(require("./user"));
var attach_1 = __importDefault(require("./attach"));
var text_1 = __importDefault(require("./text"));
var auth_1 = __importDefault(require("../../auth/auth"));
var downloadOrganizations_1 = __importDefault(require("../../sync/process/rx/handlers/downloadOrganizations"));
var SelectedWorkspace_1 = __importDefault(require("../../workspace/SelectedWorkspace"));
var urlParser_1 = __importDefault(require("../../utilities/urlParser"));
var fs_extra_1 = __importDefault(require("fs-extra"));
var dbName = config_runtime_1.default.DB_ORG_MODEL_NAME;
var ModelOrgs = (function () {
    function ModelOrgs() {
    }
    ModelOrgs.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelOrgs.count = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.count(data, options, callback);
    };
    ModelOrgs.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.findAll(data, options, callback);
    };
    ModelOrgs.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.id) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.prepareModelData = ModelOrgs.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelOrgs.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.id) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelOrgs.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.id) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelOrgs.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (data.id) {
            options.dbName = dbName;
            pdb_1.default.remove(data, options, callback);
        }
        else {
            return callback(customError_1.default.wrongInput(), 0);
        }
    };
    ModelOrgs.getPublicData = function (doc) {
        var org = {};
        if (doc) {
            org.id = doc._id;
            org.createdAt = doc.createdAt;
            org.updatedAt = doc.updatedAt;
            if (doc.type)
                org.type = doc.type;
            if (doc.serviceType)
                org.serviceType = doc.serviceType;
            if (doc.title)
                org.title = doc.title;
            if (doc.usage)
                org.usage = doc.usage;
            if (doc.limits)
                org.limits = doc.limits;
            if (doc.user)
                org.user = doc.user;
            if (doc.user)
                org.user = doc.user;
            if (doc.sub)
                org.sub = doc.sub;
            if (doc.suspended)
                org.suspended = doc.suspended;
            if (doc.suspendedReason)
                org.suspendedReason = doc.suspendedReason;
            if (doc.syncDate)
                org.syncDate = doc.syncDate;
            if (doc.needSync)
                org.needSync = doc.needSync;
        }
        return org;
    };
    ModelOrgs.getResponseJson = function (data) {
        if (!data) {
            return null;
        }
        return {
            id: data.id,
            type: data.serviceType || ModelOrgs.SERVICE_TYPE_NOTES,
            title: data.title,
            description: data.description,
            usage: data.usage,
            limits: data.limits,
            user: data.user,
            sub: data.sub,
            domain: data.domain,
            suspended: data.suspended,
            suspendedAt: data.suspendedAt,
            suspendedReason: data.suspendedReason,
            access: data.acccess,
            createdAt: data.createdAt,
            updatedAt: data.updatedAt,
        };
    };
    ModelOrgs.prepareModelData = function (data) {
        var item = {};
        item = ModelOrgs.prepareCommonProperties(item, data);
        item = ModelOrgs.prepareSyncOnlyProperties(item, data);
        return item;
    };
    ModelOrgs.prepareCommonProperties = function (item, data) {
        var curDate = dateHandler_1.default.now();
        item.erised = data.erised || false;
        item.id = data.id || item.id || generatorHandler_1.default.generateOrgID(data.userId);
        item._id = item.id.toString();
        item.title = data.title || ModelOrgs.DEFAULT_NAME;
        item.type = data.type || ModelOrgs.TYPE_PRIVATE;
        item.serviceType = data.serviceType || ModelOrgs.SERVICE_TYPE_NOTES;
        item.usage = data.usage || {
            current: 0,
            max: 0,
            daysToReset: 0
        };
        item.limits = data.limits || {
            traffic: 0,
            workspaces: 0,
            textSize: 0,
            attachmentSize: 0,
            encryptionKeysPerWorkspace: 0,
            membersPerWorkspace: 0,
            notesPerWorkspace: ModelOrgs.NOTES_PER_WORKSPACE_MAX
        };
        if (item.type === ModelOrgs.TYPE_PRIVATE) {
            item.user = data.user || {
                id: 0,
                email: null,
                username: '',
                firstname: null,
                lastname: null
            };
        }
        item.sub = data.sub || null;
        item.domain = data.domain || null;
        item.description = data.description || null;
        item.suspended = data.suspended || false;
        item.suspendedReason = data.suspendedReason || null;
        item.suspendedAt = data.suspendedAt || null;
        item.smallLogoUrl = data.smallLogoUrl || null;
        item.bigLogoUrl = data.bigLogoUrl || null;
        item.createdAt = data.createdAt || curDate;
        item.updatedAt = data.updatedAt || curDate;
        item.access = data.access || null;
        item.features = data.features || [];
        return item;
    };
    ModelOrgs.prepareSyncOnlyProperties = function (item, data) {
        item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
        item.syncDate = data.syncDate || 0;
        if (typeof (data.type) !== 'undefined') {
            item.type = data.type;
        }
        if (typeof (data.serviceType) !== 'undefined') {
            item.serviceType = data.serviceType;
        }
        if (typeof (data.title) !== 'undefined') {
            item.title = data.title;
        }
        if (typeof (data.title) !== 'undefined') {
            item.title = data.title;
        }
        if (typeof (data.user) !== 'undefined') {
            item.user = data.user;
        }
        if (typeof (data.usage) !== 'undefined') {
            item.usage = data.usage;
        }
        if (typeof (data.limits) !== 'undefined') {
            item.limits = data.limits;
        }
        if (typeof (data.sub) !== 'undefined') {
            item.sub = data.sub;
        }
        if (typeof (data.domain) !== 'undefined') {
            item.domain = data.domain;
        }
        if (typeof (data.description) !== 'undefined') {
            item.description = data.description;
        }
        if (typeof (data.features) !== 'undefined') {
            item.features = data.features;
        }
        if (typeof (data.access) !== 'undefined') {
            item.access = data.access;
        }
        if (typeof (data.features) !== 'undefined') {
            item.features = data.features;
        }
        if (typeof (data.suspended) !== 'undefined') {
            item.suspended = data.suspended;
        }
        if (typeof (data.suspendedReason) !== 'undefined') {
            item.suspendedReason = data.suspendedReason;
        }
        if (typeof (data.suspendedAt) !== 'undefined') {
            item.suspendedAt = data.suspendedAt;
        }
        if (typeof (data.smallLogoUrl) !== 'undefined') {
            item.smallLogoUrl = data.smallLogoUrl;
        }
        if (typeof (data.bigLogoUrl) !== 'undefined') {
            item.bigLogoUrl = data.bigLogoUrl;
        }
        return item;
    };
    ModelOrgs.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelOrgs.getIndexList = function () {
        return ["id", "type", "serviceType", "title", "sub", "domain", "suspended", "suspendedAt", "updatedAt"];
    };
    ModelOrgs.getDefaultOrg = function () {
        return __awaiter(this, void 0, void 0, function () {
            var getWorkspacesCount, getDefaultOrgData, workspacesCount, orgData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getWorkspacesCount = function () {
                            return new Promise(function (resolve) {
                                workspace_1.default.count({}, {}, function (err, workspacesCount) {
                                    if (err || !workspacesCount) {
                                        return resolve(0);
                                    }
                                    return resolve(workspacesCount);
                                });
                            });
                        };
                        getDefaultOrgData = function () {
                            return new Promise(function (resolve) {
                                var returnData = {
                                    traffic: {
                                        current: 0,
                                        max: user_1.default.MAX_UPLOAD_SIZE,
                                        daysToReset: 0
                                    },
                                    user: {
                                        id: 0,
                                        email: '',
                                        username: '',
                                        firstname: '',
                                        lastname: ''
                                    }
                                };
                                auth_1.default.getUser(function (err, authInfo) {
                                    if (authInfo && Object.keys(authInfo).length) {
                                        var findQuery = {};
                                        user_1.default.find(findQuery, {}, function (err, userItem) {
                                            if (userItem && Object.keys(userItem).length) {
                                                return resolve({
                                                    traffic: {
                                                        current: userItem.usageCurrent,
                                                        max: userItem.usageMax,
                                                        daysToReset: userItem.quotaResetDate
                                                    },
                                                    user: {
                                                        id: userItem.userId,
                                                        email: userItem.email ? userItem.email.toLowerCase() : '',
                                                        username: userItem.username,
                                                        firstname: '',
                                                        lastname: ''
                                                    }
                                                });
                                            }
                                            else {
                                                return resolve(returnData);
                                            }
                                        });
                                    }
                                    else {
                                        return resolve(returnData);
                                    }
                                });
                            });
                        };
                        return [4, getWorkspacesCount()];
                    case 1:
                        workspacesCount = _a.sent();
                        return [4, getDefaultOrgData()];
                    case 2:
                        orgData = _a.sent();
                        return [2, {
                                id: '',
                                title: '',
                                description: '',
                                usage: {
                                    traffic: orgData.traffic,
                                    workspaces: {
                                        current: workspacesCount,
                                        max: ModelOrgs.DEFAULT_WORKSPACES_MAX
                                    }
                                },
                                limits: {
                                    encryptionKeysPerWorkspace: ModelOrgs.DEFAULT_ENCRYPTION_KEYS_PER_WORKSPACE,
                                    membersPerWorkspace: ModelOrgs.DEFAULT_MEMBERS_PER_WORKSPACE,
                                    notesPerWorkspace: ModelOrgs.NOTES_PER_WORKSPACE_MAX
                                },
                                user: orgData.user
                            }];
                }
            });
        });
    };
    ModelOrgs.getActive = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var userInfo;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, auth_1.default.getUserAsync()];
                                case 1:
                                    userInfo = _a.sent();
                                    ModelOrgs.findAll({}, {}, function (err, orgsList) {
                                        if (err || !orgsList) {
                                            return resolve(null);
                                        }
                                        if (!orgsList.length) {
                                            return resolve(null);
                                        }
                                        var userOrgs = orgsList.filter(function (orgInstance) { return (orgInstance.user && orgInstance.user.email &&
                                            (orgInstance.user.email.toLowerCase() === userInfo.email.toLowerCase())); });
                                        if (!userOrgs.length) {
                                            return resolve(null);
                                        }
                                        return resolve(userOrgs[0]);
                                    });
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    ModelOrgs.getById = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        if (!id) {
                            return resolve(null);
                        }
                        ModelOrgs.find({ id: id }, {}, function (err, orgsInstance) {
                            if (err || !orgsInstance) {
                                return resolve(null);
                            }
                            return resolve(orgsInstance);
                        });
                    })];
            });
        });
    };
    ModelOrgs.getOrganizationHost = function (orgInstance) {
        var domain = orgInstance.domain, sub = orgInstance.sub, type = orgInstance.type;
        var authApiServiceDomain = config_runtime_1.default.authApiServiceDomain;
        if (type === ModelOrgs.TYPE_PRIVATE) {
            return authApiServiceDomain;
        }
        if (domain) {
            return domain;
        }
        if (sub) {
            return sub + "." + authApiServiceDomain;
        }
        return authApiServiceDomain;
    };
    ModelOrgs.getResponseListJson = function (list) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var result, accountUserId, _i, list_1, data, limits, user_2, userId, membersPerWorkspace, electronHost, _a, _b, _c;
                        return __generator(this, function (_d) {
                            switch (_d.label) {
                                case 0:
                                    result = [];
                                    return [4, user_1.default.getAccountUserId()];
                                case 1:
                                    accountUserId = _d.sent();
                                    _i = 0, list_1 = list;
                                    _d.label = 2;
                                case 2:
                                    if (!(_i < list_1.length)) return [3, 5];
                                    data = list_1[_i];
                                    if (!data) {
                                        return [3, 4];
                                    }
                                    limits = data.limits, user_2 = data.user;
                                    userId = user_2 ? user_2.id : accountUserId;
                                    membersPerWorkspace = limits ? limits.membersPerWorkspace : ModelOrgs.DEFAULT_MEMBERS_PER_WORKSPACE;
                                    electronHost = ModelOrgs.getOrganizationHost(data);
                                    _b = (_a = result).push;
                                    _c = {
                                        globalId: data.id,
                                        userId: userId,
                                        createdAt: data.createdAt,
                                        updatedAt: data.updatedAt,
                                        title: data.title,
                                        type: data.serviceType || ModelOrgs.SERVICE_TYPE_NOTES,
                                        sub: data.sub,
                                        suspended: data.suspended,
                                        suspendedReason: data.suspendedReason,
                                        suspendedAt: data.suspendedAt,
                                        domain: data.domain,
                                        description: data.description || '',
                                        peopleToCollaborate: null,
                                        maxMembers: membersPerWorkspace,
                                        pricingModel: null,
                                        access: data.access,
                                        electronHost: electronHost
                                    };
                                    return [4, ModelOrgs.getOrganizationSmallLogo(data)];
                                case 3:
                                    _b.apply(_a, [(_c.smallLogoStoredFileUUID = _d.sent(),
                                            _c)]);
                                    _d.label = 4;
                                case 4:
                                    _i++;
                                    return [3, 2];
                                case 5: return [2, resolve(result)];
                            }
                        });
                    }); })];
            });
        });
    };
    ModelOrgs.getOrganizationSmallLogo = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var response, smallLogoUrl, routeParams, storedFileUUID, targetPath, exists;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        response = null;
                        if (!data.smallLogoUrl) return [3, 3];
                        smallLogoUrl = data.smallLogoUrl;
                        if (!smallLogoUrl) return [3, 2];
                        routeParams = urlParser_1.default.getPathParams(smallLogoUrl);
                        storedFileUUID = typeof (routeParams[2] !== 'undefined') ? routeParams[2] : null;
                        if (!storedFileUUID) return [3, 2];
                        targetPath = pdb_1.default.getClientAttachmentPath() + "/" + storedFileUUID;
                        return [4, fs_extra_1.default.exists(targetPath)];
                    case 1:
                        exists = _a.sent();
                        if (exists) {
                            response = "avatar/" + storedFileUUID;
                        }
                        _a.label = 2;
                    case 2: return [2, response];
                    case 3: return [2];
                }
            });
        });
    };
    ModelOrgs.getAllByQuery = function (query) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            ModelOrgs.findAll(query, {}, function (err, orgsList) {
                                if (err || !orgsList) {
                                    return resolve([]);
                                }
                                return resolve(orgsList);
                            });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    ModelOrgs.getByWorkspaceId = function (workspaceId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceInstance, _a, orgInstance;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    if (!workspaceId) return [3, 2];
                                    return [4, workspace_1.default.getById(workspaceId)];
                                case 1:
                                    _a = _b.sent();
                                    return [3, 4];
                                case 2: return [4, workspace_1.default.getDefault()];
                                case 3:
                                    _a = _b.sent();
                                    _b.label = 4;
                                case 4:
                                    workspaceInstance = (_a);
                                    if (!(workspaceInstance && workspaceInstance.orgId)) return [3, 6];
                                    return [4, ModelOrgs.getById(workspaceInstance.orgId)];
                                case 5:
                                    orgInstance = _b.sent();
                                    return [2, resolve(orgInstance)];
                                case 6: return [2, resolve(null)];
                            }
                        });
                    }); })];
            });
        });
    };
    ModelOrgs.getLimits = function (workspaceId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var orgsInstance, limits;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, ModelOrgs.getByWorkspaceId(workspaceId)];
                                case 1:
                                    orgsInstance = _a.sent();
                                    if (!orgsInstance) {
                                        return [2, resolve(ModelOrgs.getDefaultLimits())];
                                    }
                                    limits = orgsInstance.limits;
                                    if (!limits) {
                                        return [2, resolve(ModelOrgs.getDefaultLimits())];
                                    }
                                    return [2, resolve(limits)];
                            }
                        });
                    }); })];
            });
        });
    };
    ModelOrgs.getLimitsByOrgId = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var orgsInstance, selectedWorkspace, limits_1, limits;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, ModelOrgs.getById(id)];
                                case 1:
                                    orgsInstance = _a.sent();
                                    if (!!orgsInstance) return [3, 3];
                                    return [4, SelectedWorkspace_1.default.get()];
                                case 2:
                                    selectedWorkspace = _a.sent();
                                    if (selectedWorkspace && selectedWorkspace.org) {
                                        limits_1 = selectedWorkspace.org.limits;
                                        if (!limits_1) {
                                            return [2, resolve(ModelOrgs.getDefaultLimits())];
                                        }
                                        return [2, resolve(limits_1)];
                                    }
                                    return [2, resolve(ModelOrgs.getDefaultLimits())];
                                case 3:
                                    limits = orgsInstance.limits;
                                    if (!limits) {
                                        return [2, resolve(ModelOrgs.getDefaultLimits())];
                                    }
                                    return [2, resolve(limits)];
                            }
                        });
                    }); })];
            });
        });
    };
    ModelOrgs.getDefaultLimits = function () {
        return {
            attachmentSize: attach_1.default.MAX_UPLOAD_FILE_SIZE,
            encryptionKeysPerWorkspace: ModelOrgs.DEFAULT_ENCRYPTION_KEYS_PER_WORKSPACE,
            membersPerWorkspace: ModelOrgs.DEFAULT_MEMBERS_PER_WORKSPACE,
            textSize: text_1.default.MAX_TEXT_SIZE,
            traffic: user_1.default.MAX_UPLOAD_SIZE,
            workspaces: ModelOrgs.DEFAULT_WORKSPACES_MAX,
            notesPerWorkspace: ModelOrgs.NOTES_PER_WORKSPACE_MAX
        };
    };
    ModelOrgs.syncUserOrganizations = function () {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                downloadOrganizations_1.default({ skipSyncHandlers: true }, function () {
                    return resolve();
                });
                return [2];
            });
        }); });
    };
    ModelOrgs.syncUserOrganizationsWorkspaces = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, user_1.default.syncUserVariables()];
                    case 1:
                        _a.sent();
                        return [4, ModelOrgs.syncUserOrganizations()];
                    case 2:
                        _a.sent();
                        return [4, workspace_1.default.syncUserWorkspaces()];
                    case 3:
                        _a.sent();
                        return [2];
                }
            });
        });
    };
    ModelOrgs.TYPE_PRIVATE = 'private';
    ModelOrgs.TYPE_BUSINESS = 'business';
    ModelOrgs.SERVICE_TYPE_NOTES = 'notes';
    ModelOrgs.SERVICE_TYPE_CAPTURES = 'capture';
    ModelOrgs.DEFAULT_NAME = 'My Organization';
    ModelOrgs.DEFAULT_WORKSPACES_MAX = 2;
    ModelOrgs.DEFAULT_ENCRYPTION_KEYS_PER_WORKSPACE = 1;
    ModelOrgs.DEFAULT_MEMBERS_PER_WORKSPACE = 3;
    ModelOrgs.NOTES_PER_WORKSPACE_MAX = 50;
    ModelOrgs.NOTES_PER_WORKSPACE_MAX_PRO = 100000;
    return ModelOrgs;
}());
exports.default = ModelOrgs;
