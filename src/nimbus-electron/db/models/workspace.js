"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var dateHandler_1 = __importDefault(require("../../utilities/dateHandler"));
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var customError_1 = __importDefault(require("../../utilities/customError"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../pdb"));
var orgs_1 = __importDefault(require("./orgs"));
var workspaceMember_1 = __importDefault(require("./workspaceMember"));
var downloadWorkspaces_1 = __importDefault(require("../../sync/process/rx/handlers/downloadWorkspaces"));
var auth_1 = __importDefault(require("../../auth/auth"));
var urlParser_1 = __importDefault(require("../../utilities/urlParser"));
var fs_extra_1 = __importDefault(require("fs-extra"));
var dbName = config_runtime_1.default.DB_WORKSPACE_MODEL_NAME;
var defaultWorkspace;
var ModelWorkspace = (function () {
    function ModelWorkspace() {
    }
    ModelWorkspace.find = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.find(data, options, callback);
    };
    ModelWorkspace.count = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.count(data, options, callback);
    };
    ModelWorkspace.findAll = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        options.dbName = dbName;
        pdb_1.default.findAll(data, options, callback);
    };
    ModelWorkspace.add = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        if (!data.title) {
            return callback(customError_1.default.wrongInput(), null);
        }
        options.dbName = dbName;
        options.prepareModelData = ModelWorkspace.prepareModelData;
        pdb_1.default.create(data, options, callback);
    };
    ModelWorkspace.update = function (data, updateFields, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = updateFields;
        pdb_1.default.update(data, options, callback);
    };
    ModelWorkspace.remove = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (!data.globalId) {
            return callback(customError_1.default.wrongInput(), 0);
        }
        options.dbName = dbName;
        options.updateFields = pdb_1.default.getErasedUpdateFields();
        pdb_1.default.update(data, options, callback);
    };
    ModelWorkspace.erase = function (data, options, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        data = data || {};
        options = options || {};
        if (data.globalId) {
            options.dbName = dbName;
            pdb_1.default.remove(data, options, callback);
        }
        else {
            return callback(customError_1.default.wrongInput(), 0);
        }
    };
    ModelWorkspace.getPublicData = function (doc) {
        var workspace = {};
        if (doc) {
            workspace.id = doc._id;
            workspace.globalId = doc.globalId;
            workspace.createdAt = doc.createdAt;
            workspace.updatedAt = doc.updatedAt;
            if (doc.title)
                workspace.title = doc.title;
            if (doc.orgId)
                workspace.orgId = doc.orgId;
            if (doc.userId)
                workspace.userId = doc.userId;
            if (doc.isDefault)
                workspace.isDefault = doc.isDefault;
            if (doc.countMembers)
                workspace.countMembers = doc.countMembers;
            if (doc.countInvites)
                workspace.countInvites = doc.countInvites;
            if (doc.access)
                workspace.access = doc.access;
            if (doc.lastUpdateTime) {
                workspace.lastUpdateTime = doc.lastUpdateTime;
            }
            if (doc.reason) {
                workspace.reason = doc.reason;
            }
            if (doc.defaultEncryptionKeyId) {
                workspace.defaultEncryptionKeyId = doc.defaultEncryptionKeyId;
            }
            if (doc.syncDate)
                workspace.syncDate = doc.syncDate;
            if (doc.needSync)
                workspace.needSync = doc.needSync;
            workspace.existOnServer = doc.existOnServer || false;
            if (doc.members)
                workspace.members = doc.members;
            if (doc.invites)
                workspace.invites = doc.invites;
            if (doc.isNotesLimited)
                workspace.isNotesLimited = doc.isNotesLimited;
        }
        return workspace;
    };
    ModelWorkspace.getResponseJson = function (data) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var userInfo, orgInstance, usage, owner, isPersonal, access, privileges, result, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!data) {
                            return [2, resolve(null)];
                        }
                        return [4, auth_1.default.getUserAsync()];
                    case 1:
                        userInfo = _b.sent();
                        return [4, ModelWorkspace.getWorkspaceOrgData(data)];
                    case 2:
                        orgInstance = _b.sent();
                        usage = {
                            traffic: orgInstance.usage.traffic,
                            encryptionKeys: {
                                current: 0,
                                max: orgInstance.limits.encryptionKeysPerWorkspace
                            },
                            members: {
                                current: data.countMembers || 0,
                                max: orgInstance.limits.membersPerWorkspace
                            }
                        };
                        owner = {
                            id: data.user ? data.user.id : 0,
                            email: data.user && data.user.email ? data.user.email.toLowerCase() : '',
                            username: data.user && data.user.username ? data.user.username : '',
                            firstname: data.user && data.user.firstname ? data.user.firstname : '',
                            lastname: data.user && data.user.lastname ? data.user.lastname : '',
                            displayName: data.user && data.user.username ? data.user.username : '',
                            avatar: data.user && data.user.avatar ? data.user.avatar : null,
                            defaultEncryptionKeyId: data ? data.defaultEncryptionKeyId : ''
                        };
                        isPersonal = false;
                        if (data.user && data.user.email && data.user.email.toLowerCase() === userInfo.email) {
                            isPersonal = true;
                        }
                        if (!data.user && orgInstance && orgInstance.type === orgs_1.default.TYPE_PRIVATE) {
                            if (orgInstance.user && orgInstance.user.email) {
                                if (orgInstance.user.email.toLowerCase() === userInfo.email) {
                                    owner = {
                                        id: orgInstance.user ? orgInstance.user.id : userInfo.id,
                                        email: userInfo.email ? userInfo.email.toLowerCase() : '',
                                        username: userInfo.username ? userInfo.username : '',
                                        firstname: userInfo.firstname ? userInfo.firstname : '',
                                        lastname: userInfo.lastname ? userInfo.lastname : '',
                                        displayName: userInfo.username ? userInfo.username : '',
                                        avatar: userInfo.avatar && userInfo.avatar.url ? userInfo.avatar.url : null,
                                        defaultEncryptionKeyId: data ? data.defaultEncryptionKeyId : ''
                                    };
                                    isPersonal = true;
                                }
                                else {
                                    owner = {
                                        id: orgInstance.user ? orgInstance.user.id : null,
                                        email: orgInstance.user && orgInstance.user.email ? orgInstance.user.email.toLowerCase() : '',
                                        username: orgInstance.user && orgInstance.user.username ? orgInstance.user.username : '',
                                        firstname: orgInstance.user && orgInstance.user.firstname ? orgInstance.user.firstname : '',
                                        lastname: orgInstance.user && orgInstance.user.lastname ? orgInstance.user.lastname : '',
                                        displayName: orgInstance.user && orgInstance.user.username ? orgInstance.user.username : '',
                                        avatar: orgInstance.user && orgInstance.user.avatar ? orgInstance.user.avatar : null,
                                        defaultEncryptionKeyId: data ? data.defaultEncryptionKeyId : ''
                                    };
                                }
                            }
                        }
                        access = null;
                        if (data.access && userInfo && userInfo.email && owner && owner.email &&
                            userInfo.email.toLowerCase() !== owner.email.toLowerCase()) {
                            privileges = data.access.privileges ? data.access.privileges : [];
                            if (data.access.member && data.access.member.encryptRole) {
                                switch (data.access.member.encryptRole) {
                                    case workspaceMember_1.default.ENCRYPTION_ROLE_ACCESSOR: {
                                        privileges.push('canAccessEncryptedNotes');
                                        break;
                                    }
                                    case workspaceMember_1.default.ENCRYPTION_ROLE_ENCRYPTOR: {
                                        privileges.push('canAccessEncryptedNotes');
                                        privileges.push('canEncryptNotes');
                                        break;
                                    }
                                    case workspaceMember_1.default.ENCRYPTION_ROLE_MANGER: {
                                        privileges.push('canAccessEncryptedNotes');
                                        privileges.push('canEncryptNotes');
                                        privileges.push('canManageEncryptionKeys');
                                        break;
                                    }
                                }
                            }
                            access = {
                                workspaceId: data.globalId,
                                access: {
                                    type: data.access.member ? 'member' : '',
                                    role: data.access.role ? data.access.role : '',
                                    encryptRole: data.access.member && data.access.member.encryptRole ? data.access.member.encryptRole : workspaceMember_1.default.ENCRYPTION_ROLE_DENY,
                                    privileges: privileges,
                                    reduced: false
                                }
                            };
                        }
                        _a = {
                            id: data.id || null,
                            globalId: data.globalId,
                            orgId: data.orgId,
                            userId: data.userId,
                            title: data.title,
                            isDefault: data.isDefault || false,
                            defaultEncryptionKeyId: data.defaultEncryptionKeyId,
                            createdAt: data.createdAt,
                            updatedAt: data.updatedAt,
                            usage: usage,
                            owner: owner,
                            access: access,
                            isNotesLimited: data.isNotesLimited || false
                        };
                        return [4, ModelWorkspace.isMainForUser(data)];
                    case 3:
                        _a.isDefaultForCurrentUser = _b.sent(),
                            _a.isPersonal = isPersonal;
                        return [4, ModelWorkspace.getWorkspaceAvatar(data)];
                    case 4:
                        result = (_a.avatar = _b.sent(),
                            _a.color = data.color ? data.color : null,
                            _a);
                        return [2, resolve(result)];
                }
            });
        }); });
    };
    ModelWorkspace.getWorkspaceAvatar = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var response, url, routeParams, storedFileUUID, targetPath, exists;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        response = { storedFileUUID: null };
                        if (!data.avatar) return [3, 3];
                        url = data.avatar.url;
                        if (!url) return [3, 2];
                        routeParams = urlParser_1.default.getPathParams(url);
                        storedFileUUID = typeof (routeParams[2] !== 'undefined') ? routeParams[2] : null;
                        if (!storedFileUUID) return [3, 2];
                        targetPath = pdb_1.default.getClientAttachmentPath() + "/" + storedFileUUID;
                        return [4, fs_extra_1.default.exists(targetPath)];
                    case 1:
                        exists = _a.sent();
                        if (exists) {
                            response.storedFileUUID = "avatar/" + storedFileUUID;
                        }
                        _a.label = 2;
                    case 2: return [2, response];
                    case 3: return [2];
                }
            });
        });
    };
    ModelWorkspace.getWorkspaceOrgData = function (data) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var orgInstance, isBusinessOrg;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, orgs_1.default.getById(data.orgId)];
                    case 1:
                        orgInstance = _a.sent();
                        if (!orgInstance) {
                            isBusinessOrg = data.orgId && data.orgId.charAt(0) === 'b';
                            if (!isBusinessOrg && data.org) {
                                orgInstance = data.org;
                            }
                        }
                        if (!!orgInstance) return [3, 3];
                        return [4, orgs_1.default.getDefaultOrg()];
                    case 2:
                        orgInstance = _a.sent();
                        _a.label = 3;
                    case 3: return [2, resolve(orgInstance)];
                }
            });
        }); });
    };
    ;
    ModelWorkspace.getResponseListJson = function (list) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var result, _i, list_1, data, jsonData;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    result = [];
                                    _i = 0, list_1 = list;
                                    _a.label = 1;
                                case 1:
                                    if (!(_i < list_1.length)) return [3, 4];
                                    data = list_1[_i];
                                    return [4, ModelWorkspace.getResponseJson(data)];
                                case 2:
                                    jsonData = _a.sent();
                                    result.push(jsonData);
                                    _a.label = 3;
                                case 3:
                                    _i++;
                                    return [3, 1];
                                case 4: return [2, resolve(result)];
                            }
                        });
                    }); })];
            });
        });
    };
    ModelWorkspace.getMentionResponseJson = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var members, jsonData, globalId, access, userId, updatedAt, createdAt, owner, orgId, isPersonal, mentionData, membersCount, result, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        members = data.members;
                        return [4, ModelWorkspace.getResponseJson(data)];
                    case 1:
                        jsonData = _a.sent();
                        if (!jsonData) {
                            return [2, null];
                        }
                        globalId = jsonData.globalId, access = jsonData.access, userId = jsonData.userId, updatedAt = jsonData.updatedAt, createdAt = jsonData.createdAt, owner = jsonData.owner, orgId = jsonData.orgId, isPersonal = jsonData.isPersonal;
                        if (!access) {
                            access = data.access;
                        }
                        if (!access) {
                            return [2, null];
                        }
                        mentionData = {
                            addedByUserId: owner ? owner.id : 0,
                            createdAt: createdAt,
                            encryptRole: access.encryptRole,
                            globalId: globalId,
                            orgId: orgId,
                            privileges: access.privileges,
                            role: access.role,
                            type: access.type,
                            updatedAt: updatedAt,
                            userId: userId,
                            workspaceId: globalId,
                        };
                        membersCount = members ? members.length : 0;
                        if (isPersonal && membersCount) {
                            membersCount = membersCount - 1;
                        }
                        result = [];
                        for (i = 0; i < membersCount; i++) {
                            result.push(mentionData);
                        }
                        return [2, result];
                }
            });
        });
    };
    ModelWorkspace.getDefaultAccess = function () {
        return {
            role: 'admin',
            privileges: [
                "canAdmin",
                "canDelete",
                "canEdit",
                "canChangePreview",
                "canChangeColor",
                "canCreateNote",
                "canShare",
                "canChangeReminder",
                "canChangeTodos",
                "canChangeAttachments",
                "canChangeText",
                "canRead"
            ],
            member: 0
        };
    };
    ModelWorkspace.prepareModelData = function (data) {
        var item = {};
        item = ModelWorkspace.prepareCommonProperties(item, data);
        item = ModelWorkspace.prepareSyncOnlyProperties(item, data);
        return item;
    };
    ModelWorkspace.prepareCommonProperties = function (item, data) {
        var curDate = dateHandler_1.default.now();
        item.erised = data.erised || false;
        item.globalId = data.globalId || data.global_id || item._id || generatorHandler_1.default.randomString(16);
        item.title = data.title || '';
        item._id = item.globalId.toString();
        item.orgId = data.orgId || '';
        item.org = data.org || null;
        item.userId = data.userId || null;
        item.isDefault = data.isDefault || data.default || false;
        item.countMembers = data.countMembers || 0;
        item.countInvites = data.countInvites || 0;
        item.access = data.access || {
            role: '',
            privileges: [],
            memberId: 0
        };
        item.lastUpdateTime = data.lastUpdateTime || 0;
        item.reason = data.reason || '';
        item.defaultEncryptionKeyId = data.defaultEncryptionKeyId || null;
        item.createdAt = data.createdAt || curDate;
        item.updatedAt = data.updatedAt || curDate;
        item.existOnServer = data.existOnServer || false;
        item.members = data.members || [];
        item.invites = data.invites || [];
        item.isNotesLimited = data.isNotesLimited || false;
        item.user = data.user || null;
        item.notesEmail = data.notesEmail || '';
        item.avatar = data.avatar || null;
        item.color = data.color || null;
        return item;
    };
    ModelWorkspace.prepareSyncOnlyProperties = function (item, data) {
        item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
        item.syncDate = data.syncDate || 0;
        if (typeof (data.title) !== 'undefined') {
            item.title = data.title;
        }
        if (typeof (data.userId) !== 'undefined') {
            item.userId = data.userId;
        }
        if (typeof (data.org) !== 'undefined') {
            item.org = data.org;
        }
        if (typeof (data.default) !== 'undefined' || typeof (data.isDefault) !== 'undefined') {
            item.isDefault = data.isDefault || data.default;
        }
        if (typeof (data.countMembers) !== 'undefined') {
            item.countMembers = data.countMembers;
        }
        if (typeof (data.countInvites) !== 'undefined') {
            item.countInvites = data.countInvites;
        }
        if (typeof (data.access) !== 'undefined') {
            item.access = data.access;
        }
        if (typeof (data.lastUpdateTime) !== 'undefined') {
            item.lastUpdateTime = data.lastUpdateTime;
        }
        if (typeof (data.reason) !== 'undefined') {
            item.reason = data.reason;
        }
        if (typeof (data.defaultEncryptionKeyId) !== 'undefined') {
            item.defaultEncryptionKeyId = data.defaultEncryptionKeyId;
        }
        if (typeof (data.createdAt) !== 'undefined') {
            item.createdAt = data.createdAt;
        }
        if (typeof (data.updatedAt) !== 'undefined') {
            item.updatedAt = data.updatedAt;
        }
        if (typeof (data.existOnServer) !== 'undefined') {
            item.existOnServer = data.existOnServer;
        }
        if (typeof (data.members) !== 'undefined') {
            item.members = data.members;
        }
        if (typeof (data.invites) !== 'undefined') {
            item.invites = data.invites;
        }
        if (typeof (data.isNotesLimited) !== 'undefined') {
            item.isNotesLimited = data.isNotesLimited;
        }
        if (typeof (data.user) !== 'undefined') {
            item.user = data.user;
        }
        if (typeof (data.notesEmail) !== 'undefined') {
            item.notesEmail = data.notesEmail;
        }
        if (typeof (data.avatar) !== 'undefined') {
            item.avatar = data.avatar;
        }
        if (typeof (data.color) !== 'undefined') {
            item.color = data.color;
        }
        return item;
    };
    ModelWorkspace.prepareItemDbProperties = function (item, data) {
        return pdb_1.default.prepareItemDbProperties(item, data);
    };
    ModelWorkspace.getIndexList = function () {
        return ["globalId", "orgId", "role", "disabled", "updatedAt"];
    };
    ModelWorkspace.getDefaultWorkspaceData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var defaultOrg, ogrId, defaultWorkspaceData;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, orgs_1.default.getDefaultOrg()];
                                case 1:
                                    defaultOrg = _a.sent();
                                    ogrId = defaultOrg ? defaultOrg.id : '';
                                    defaultWorkspaceData = {
                                        globalId: 'default',
                                        title: 'Default workspace',
                                        orgId: ogrId,
                                        userId: null,
                                        isDefault: true,
                                        countMembers: 0,
                                        countInvites: 0,
                                        access: {
                                            role: '',
                                            privileges: [],
                                            memberId: 0
                                        },
                                        defaultEncryptionKeyId: null,
                                        lastUpdateTime: 0,
                                        createdAt: 0,
                                        updatedAt: 0,
                                        members: [],
                                        invites: [],
                                        isNotesLimited: false,
                                        avatar: { storedFileUUID: null },
                                        color: null,
                                    };
                                    return [2, resolve(defaultWorkspaceData)];
                            }
                        });
                    }); })];
            });
        });
    };
    ModelWorkspace.getDefault = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            ModelWorkspace.findAll({ isDefault: true }, {}, function (err, workspaceInstances) { return __awaiter(_this, void 0, void 0, function () {
                                var _a, mainUserWorkspace, _i, workspaceInstances_1, workspaceInstance, _b;
                                return __generator(this, function (_c) {
                                    switch (_c.label) {
                                        case 0:
                                            if (!(err || !workspaceInstances)) return [3, 2];
                                            _a = resolve;
                                            return [4, ModelWorkspace.getDefaultWorkspaceData()];
                                        case 1: return [2, _a.apply(void 0, [_c.sent()])];
                                        case 2:
                                            mainUserWorkspace = null;
                                            _i = 0, workspaceInstances_1 = workspaceInstances;
                                            _c.label = 3;
                                        case 3:
                                            if (!(_i < workspaceInstances_1.length)) return [3, 6];
                                            workspaceInstance = workspaceInstances_1[_i];
                                            return [4, ModelWorkspace.isMainForUser(workspaceInstance)];
                                        case 4:
                                            if (_c.sent()) {
                                                mainUserWorkspace = workspaceInstance;
                                                return [3, 6];
                                            }
                                            _c.label = 5;
                                        case 5:
                                            _i++;
                                            return [3, 3];
                                        case 6:
                                            if (!!mainUserWorkspace) return [3, 8];
                                            _b = resolve;
                                            return [4, ModelWorkspace.getDefaultWorkspaceData()];
                                        case 7: return [2, _b.apply(void 0, [_c.sent()])];
                                        case 8: return [2, resolve(mainUserWorkspace)];
                                    }
                                });
                            }); });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    ModelWorkspace.getWithQuery = function (query) {
        if (query === void 0) { query = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            ModelWorkspace.find(query, {}, function (err, workspaceInstance) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    if (err || !workspaceInstance) {
                                        return [2, resolve(null)];
                                    }
                                    return [2, resolve(workspaceInstance)];
                                });
                            }); });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    ModelWorkspace.getLocalId = function (workspaceId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var result;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (typeof (workspaceId) === 'string' && workspaceId == 'null') {
                                        return [2, resolve(null)];
                                    }
                                    if (workspaceId === ModelWorkspace.DEFAULT_NAME) {
                                        return [2, resolve(null)];
                                    }
                                    if (!workspaceId) {
                                        return [2, resolve(null)];
                                    }
                                    if (!!defaultWorkspace) return [3, 2];
                                    if (!(typeof (defaultWorkspace) === 'undefined')) return [3, 2];
                                    return [4, ModelWorkspace.getDefault()];
                                case 1:
                                    defaultWorkspace = _a.sent();
                                    _a.label = 2;
                                case 2:
                                    if (!defaultWorkspace) {
                                        return [2, resolve(null)];
                                    }
                                    result = defaultWorkspace.globalId === workspaceId ? null : workspaceId;
                                    return [2, resolve(result)];
                            }
                        });
                    }); })];
            });
        });
    };
    ModelWorkspace.clearDefaultWorkspace = function () {
        defaultWorkspace = undefined;
    };
    ModelWorkspace.getAvailableIdList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var result;
                        var _this = this;
                        return __generator(this, function (_a) {
                            result = [null];
                            ModelWorkspace.findAll({}, {}, function (err, workspaceList) { return __awaiter(_this, void 0, void 0, function () {
                                var _i, workspaceList_1, workspaceInstance;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (err || !workspaceList) {
                                                return [2, resolve(result)];
                                            }
                                            if (!workspaceList.length) {
                                                return [2, resolve(result)];
                                            }
                                            _i = 0, workspaceList_1 = workspaceList;
                                            _a.label = 1;
                                        case 1:
                                            if (!(_i < workspaceList_1.length)) return [3, 4];
                                            workspaceInstance = workspaceList_1[_i];
                                            return [4, ModelWorkspace.isMainForUser(workspaceInstance)];
                                        case 2:
                                            if (!(_a.sent())) {
                                                result.push(workspaceInstance.globalId);
                                            }
                                            _a.label = 3;
                                        case 3:
                                            _i++;
                                            return [3, 1];
                                        case 4: return [2, resolve(result)];
                                    }
                                });
                            }); });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    ModelWorkspace.getUserWorkspacesCount = function () {
        return new Promise(function (resolve) {
            ModelWorkspace.findAll({}, {}, function (err, workspacesList) {
                if (err || !workspacesList.length) {
                    return resolve(0);
                }
                return resolve(workspacesList.length);
            });
        });
    };
    ModelWorkspace.syncUserWorkspaces = function () {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                downloadWorkspaces_1.default({ skipSyncHandlers: true }, function () {
                    return resolve();
                });
                return [2];
            });
        }); });
    };
    ModelWorkspace.isUserWorkspace = function (workspace) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        orgs_1.default.find({ id: workspace.orgId }, {}, function (err, orgInstance) { return __awaiter(_this, void 0, void 0, function () {
                            var userInfo;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (err || !orgInstance) {
                                            return [2, orgs_1.default.count({}, {}, function (err, orgsCount) {
                                                    if (err || !orgsCount) {
                                                        return resolve(workspace.isDefault);
                                                    }
                                                    return resolve(false);
                                                })];
                                        }
                                        if (!orgInstance.user) {
                                            return [2, resolve(false)];
                                        }
                                        if (!orgInstance.user.email) {
                                            return [2, resolve(false)];
                                        }
                                        return [4, auth_1.default.getUserAsync()];
                                    case 1:
                                        userInfo = _a.sent();
                                        if (!userInfo) {
                                            return [2, resolve(false)];
                                        }
                                        if (userInfo.email.toLowerCase() !== orgInstance.user.email.toLowerCase()) {
                                            return [2, resolve(false)];
                                        }
                                        return [2, resolve(true)];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    ModelWorkspace.isMainForUser = function (workspace) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var userInfo, orgInstance, isPersonal, isBusinessOrg, isMainWorkspaceForUser;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!workspace) {
                                        return [2, resolve(false)];
                                    }
                                    if (!workspace.isDefault) {
                                        return [2, resolve(false)];
                                    }
                                    if (workspace.globalId === 'default') {
                                        return [2, resolve(true)];
                                    }
                                    return [4, auth_1.default.getUserAsync()];
                                case 1:
                                    userInfo = _a.sent();
                                    if (!userInfo) {
                                        return [2, resolve(false)];
                                    }
                                    if (!userInfo.email) {
                                        return [2, resolve(false)];
                                    }
                                    return [4, ModelWorkspace.getWorkspaceOrgData(workspace)];
                                case 2:
                                    orgInstance = _a.sent();
                                    isPersonal = workspace.user && workspace.user.email && workspace.user.email.toLowerCase() === userInfo.email;
                                    if (!workspace.user && orgInstance && orgInstance.type === orgs_1.default.TYPE_PRIVATE) {
                                        isPersonal = orgInstance.user && orgInstance.user.email && orgInstance.user.email.toLowerCase() === userInfo.email;
                                    }
                                    isBusinessOrg = workspace.orgId && workspace.orgId.charAt(0) === 'b';
                                    isMainWorkspaceForUser = workspace.isDefault && isPersonal && !isBusinessOrg;
                                    return [2, resolve(isMainWorkspaceForUser)];
                            }
                        });
                    }); })];
            });
        });
    };
    ModelWorkspace.getById = function (workspaceId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        ModelWorkspace.find({ globalId: workspaceId }, {}, function (err, workspaceItem) {
                            if (err || !workspaceItem) {
                                return resolve(null);
                            }
                            return resolve(workspaceItem);
                        });
                    })];
            });
        });
    };
    ;
    ModelWorkspace.getPublicPremium = function (workspaceId, originalWorkspaceId) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var orgsInstance, _a, premium, id, type, status_1, startDate, endDate, isBusiness, inheritedFromOrg, workspaceInstance, wsUser, premium, id, type, status_2, startDate, endDate, isBusiness, inheritedFromOrg;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4, orgs_1.default.getByWorkspaceId(workspaceId)];
                    case 1:
                        orgsInstance = _b.sent();
                        if (orgsInstance && orgsInstance.user) {
                            _a = orgsInstance.user, premium = _a.premium, id = _a.id;
                            if (premium) {
                                type = premium.type, status_1 = premium.status, startDate = premium.startDate, endDate = premium.endDate, isBusiness = premium.isBusiness;
                                inheritedFromOrg = orgsInstance.type === orgs_1.default.TYPE_BUSINESS ? [orgsInstance.id] : [];
                                return [2, resolve({
                                        inheritedFromOrg: inheritedFromOrg,
                                        type: type,
                                        startDate: startDate,
                                        endDate: endDate,
                                        status: status_1,
                                        userId: id,
                                        isBusiness: isBusiness,
                                    })];
                            }
                        }
                        return [4, ModelWorkspace.getById(originalWorkspaceId)];
                    case 2:
                        workspaceInstance = _b.sent();
                        if (workspaceInstance) {
                            wsUser = workspaceInstance.org && workspaceInstance.org.user ? workspaceInstance.org.user : null;
                            if (!wsUser) {
                                wsUser = workspaceInstance.user ? workspaceInstance.user : null;
                            }
                            if (wsUser) {
                                premium = wsUser.premium, id = wsUser.id;
                                if (premium) {
                                    type = premium.type, status_2 = premium.status, startDate = premium.startDate, endDate = premium.endDate, isBusiness = premium.isBusiness;
                                    inheritedFromOrg = workspaceInstance.org && workspaceInstance.org.type === orgs_1.default.TYPE_BUSINESS ? [workspaceInstance.org.id] : [];
                                    return [2, resolve({
                                            inheritedFromOrg: inheritedFromOrg,
                                            type: type,
                                            startDate: startDate,
                                            endDate: endDate,
                                            status: status_2,
                                            userId: id,
                                            isBusiness: isBusiness,
                                        })];
                                }
                                else {
                                    if (workspaceInstance.org && workspaceInstance.org.access) {
                                        if (workspaceInstance.org.type === orgs_1.default.TYPE_BUSINESS &&
                                            workspaceInstance.org.access.role &&
                                            workspaceInstance.org.access.role !== workspaceMember_1.default.ROLE_ADMIN) {
                                            return [2, resolve({
                                                    inheritedFromOrg: [workspaceInstance.org.id],
                                                    type: '',
                                                    startDate: null,
                                                    endDate: null,
                                                    status: 'active',
                                                    userId: id,
                                                    isBusiness: true,
                                                })];
                                        }
                                    }
                                }
                            }
                        }
                        return [2, resolve({
                                dateEnd: null,
                                status: null
                            })];
                }
            });
        }); });
    };
    ModelWorkspace.findUserWorkspaces = function () {
        var _this = this;
        return new Promise(function (resolve) {
            ModelWorkspace.findAll({}, {}, function (err, workspacesList) { return __awaiter(_this, void 0, void 0, function () {
                var defaultWorkspace_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!(err || !workspacesList.length)) return [3, 2];
                            return [4, ModelWorkspace.getDefault()];
                        case 1:
                            defaultWorkspace_1 = _a.sent();
                            workspacesList = [defaultWorkspace_1];
                            _a.label = 2;
                        case 2: return [2, resolve(workspacesList)];
                    }
                });
            }); });
        });
    };
    ModelWorkspace.findUserWorkspaceByOrgId = function (orgId) {
        var _this = this;
        return new Promise(function (resolve) {
            ModelWorkspace.find({ orgId: orgId }, {}, function (err, workspaceItem) { return __awaiter(_this, void 0, void 0, function () {
                var result;
                return __generator(this, function (_a) {
                    result = workspaceItem || null;
                    return [2, resolve(result)];
                });
            }); });
        });
    };
    ModelWorkspace.countUserWorkspacesByOrgId = function (orgId) {
        var _this = this;
        return new Promise(function (resolve) {
            ModelWorkspace.count({ orgId: orgId }, {}, function (err, workspacesCount) { return __awaiter(_this, void 0, void 0, function () {
                var result;
                return __generator(this, function (_a) {
                    result = workspacesCount || 0;
                    return [2, resolve(result)];
                });
            }); });
        });
    };
    ModelWorkspace.DEFAULT_NAME = 'default';
    ModelWorkspace.PRIVILEGE_CAN_READ = 'canRead';
    ModelWorkspace.PRIVILEGE_CAN_EDIT = 'canEdit';
    ModelWorkspace.PRIVILEGE_CAN_COMMENT = 'canComment';
    ModelWorkspace.PRIVILEGE_CAN_ACCESS_ENCRYPTED_NOTES = 'canAccessEncryptedNotes';
    ModelWorkspace.PRIVILEGE_CAN_ENCRYPT_NOTES = 'canEncryptNotes';
    ModelWorkspace.PRIVILEGE_CAN_MANAGE_ENCRYPTION_KEYS = 'canManageEncryptionKeys';
    ModelWorkspace.ERROR_QUOTA_EXCEED = -20;
    return ModelWorkspace;
}());
exports.default = ModelWorkspace;
