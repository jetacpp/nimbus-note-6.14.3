"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_extra_1 = __importDefault(require("fs-extra"));
var pdb_1 = __importDefault(require("../../pdb"));
var electron_1 = require("electron");
var textEditor_1 = __importDefault(require("./models/textEditor"));
var workspace_1 = __importDefault(require("./models/workspace"));
var item_1 = __importDefault(require("./models/item"));
var dateHandler_1 = __importDefault(require("../utilities/dateHandler"));
var urlParser_1 = __importDefault(require("../utilities/urlParser"));
var instance_1 = __importDefault(require("../window/instance"));
var socketFunctions_1 = __importDefault(require("../sync/socket/socketFunctions"));
var text_1 = __importDefault(require("./models/text"));
var attach_1 = __importDefault(require("./models/attach"));
var NimbusSDK_1 = __importDefault(require("../sync/nimbussdk/net/NimbusSDK"));
var SelectedWorkspace_1 = __importDefault(require("../workspace/SelectedWorkspace"));
var AttachmentObjRepository_1 = __importDefault(require("../sync/process/repositories/AttachmentObjRepository"));
var AttachmentSingleDownloader_1 = __importDefault(require("../sync/downlaoder/AttachmentSingleDownloader"));
var syncPropsHandler_1 = require("../utilities/syncPropsHandler");
var textHandler_1 = __importDefault(require("../utilities/textHandler"));
var state_1 = __importDefault(require("../online/state"));
var TextEditor = (function () {
    function TextEditor() {
    }
    TextEditor.put = function (data) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var valueSerialized, valueShort, valueFull, _a, globalId, noteGlobalId, workspaceId, curDate, queryData, updateData;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        valueSerialized = data.valueSerialized, valueShort = data.valueShort, valueFull = data.valueFull;
                        return [4, TextEditor.getNoteData(data)];
                    case 1:
                        _a = _b.sent(), globalId = _a.globalId, noteGlobalId = _a.noteGlobalId, workspaceId = _a.workspaceId;
                        curDate = dateHandler_1.default.now();
                        if (typeof (valueShort) !== 'undefined' || typeof (valueFull) !== 'undefined') {
                            queryData = { noteGlobalId: noteGlobalId };
                            updateData = {
                                textShort: valueShort,
                                text: valueFull,
                                'length': valueFull.text ? valueFull.text.length : 0
                            };
                            text_1.default.update(queryData, updateData, { workspaceId: workspaceId }, function (err, count) {
                                if (count) {
                                    TextEditor.socketTextMessage({ workspaceId: workspaceId, noteGlobalId: noteGlobalId });
                                }
                            });
                        }
                        textEditor_1.default.add({
                            globalId: globalId,
                            noteGlobalId: noteGlobalId,
                            text: valueSerialized,
                            dateUpdated: curDate
                        }, { workspaceId: workspaceId }, function (err, res) {
                            if (err) {
                                return resolve(null);
                            }
                            return resolve(res.text);
                        });
                        return [2];
                }
            });
        }); });
    };
    TextEditor.saveSerializedItem = function (workspaceId, noteGlobalId, globalId, valueSerialized) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var curDate;
            return __generator(this, function (_a) {
                curDate = dateHandler_1.default.now();
                textEditor_1.default.add({
                    globalId: globalId,
                    noteGlobalId: noteGlobalId,
                    text: valueSerialized,
                    dateUpdated: curDate
                }, { workspaceId: workspaceId }, function (err, res) {
                    if (err) {
                        return resolve(null);
                    }
                    return resolve(res.text);
                });
                return [2];
            });
        }); });
    };
    TextEditor.putArray = function (data) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var entries, valueShort, valueFull, _a, noteGlobalId, workspaceId, _i, entries_1, entry, queryData, updateData;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        entries = data.entries, valueShort = data.valueShort, valueFull = data.valueFull;
                        return [4, TextEditor.getNoteData(data)];
                    case 1:
                        _a = _b.sent(), noteGlobalId = _a.noteGlobalId, workspaceId = _a.workspaceId;
                        _i = 0, entries_1 = entries;
                        _b.label = 2;
                    case 2:
                        if (!(_i < entries_1.length)) return [3, 5];
                        entry = entries_1[_i];
                        return [4, TextEditor.saveSerializedItem(workspaceId, noteGlobalId, entry.key, entry.valueSerialized)];
                    case 3:
                        _b.sent();
                        _b.label = 4;
                    case 4:
                        _i++;
                        return [3, 2];
                    case 5:
                        if (typeof (valueShort) !== 'undefined' || typeof (valueFull) !== 'undefined') {
                            queryData = { noteGlobalId: noteGlobalId };
                            updateData = {
                                textShort: valueShort,
                                text: valueFull,
                                'length': valueFull.text ? valueFull.text.length : 0
                            };
                            text_1.default.update(queryData, updateData, { workspaceId: workspaceId }, function (err, count) {
                                if (count) {
                                    TextEditor.socketTextMessage({ workspaceId: workspaceId, noteGlobalId: noteGlobalId });
                                }
                            });
                        }
                        return [2, resolve(true)];
                }
            });
        }); });
    };
    TextEditor.get = function (data) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var key, _a, globalId, workspaceId;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        key = data.key;
                        if (!key) {
                            return [2, resolve(null)];
                        }
                        return [4, TextEditor.getNoteData(data)];
                    case 1:
                        _a = _b.sent(), globalId = _a.globalId, workspaceId = _a.workspaceId;
                        textEditor_1.default.find({ globalId: globalId }, { workspaceId: workspaceId }, function (err, res) {
                            if (err) {
                                return resolve(null);
                            }
                            return resolve(res.text);
                        });
                        return [2];
                }
            });
        }); });
    };
    TextEditor.getFromRange = function (data) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var keyFrom, keyTo, _a, globalIdFrom, workspaceId, globalIdTo, query, regexp;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        keyFrom = data.keyFrom, keyTo = data.keyTo;
                        if (!keyFrom) {
                            return [2, resolve([])];
                        }
                        return [4, TextEditor.getNoteData({ key: keyFrom })];
                    case 1:
                        _a = _b.sent(), globalIdFrom = _a.globalId, workspaceId = _a.workspaceId;
                        return [4, TextEditor.getNoteData({ key: keyTo })];
                    case 2:
                        globalIdTo = (_b.sent()).globalId;
                        query = { globalId: {} };
                        regexp = new RegExp(urlParser_1.default.escapeRegExp(globalIdFrom.split('#')[0]));
                        query.globalId = { '$regex': regexp };
                        textEditor_1.default.findAll(query, { workspaceId: workspaceId }, function (err, res) {
                            if (err) {
                                return resolve([]);
                            }
                            return resolve(res.filter(function (textInstance) { return textInstance.text; }).map(function (textInstance) {
                                return textInstance.text;
                            }));
                        });
                        return [2];
                }
            });
        }); });
    };
    TextEditor.getKeysFromRange = function (data) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var keyFrom, keyTo, _a, globalIdFrom, workspaceId, globalIdTo, query, regexp;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        keyFrom = data.keyFrom, keyTo = data.keyTo;
                        if (!keyFrom) {
                            return [2, resolve([])];
                        }
                        return [4, TextEditor.getNoteData({ key: keyFrom })];
                    case 1:
                        _a = _b.sent(), globalIdFrom = _a.globalId, workspaceId = _a.workspaceId;
                        return [4, TextEditor.getNoteData({ key: keyTo })];
                    case 2:
                        globalIdTo = (_b.sent()).globalId;
                        query = { globalId: {} };
                        regexp = new RegExp(urlParser_1.default.escapeRegExp(globalIdFrom.split('#')[0]));
                        query.globalId = { '$regex': regexp };
                        textEditor_1.default.findAll(query, { workspaceId: workspaceId }, function (err, res) {
                            if (err) {
                                return resolve([]);
                            }
                            return resolve(res.map(function (textInstance) { return textInstance.globalId; }));
                        });
                        return [2];
                }
            });
        }); });
    };
    TextEditor.delete = function (data) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var keys, workspaceId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        keys = data.keys;
                        if (!keys) {
                            return [2, resolve(0)];
                        }
                        if (!keys.length) {
                            return [2, resolve(0)];
                        }
                        return [4, TextEditor.getNoteData(keys[0])];
                    case 1:
                        workspaceId = (_a.sent()).workspaceId;
                        textEditor_1.default.erase({ globalId: { $in: keys } }, { workspaceId: workspaceId }, function (err, res) {
                            if (err) {
                                return resolve(0);
                            }
                            return resolve(res);
                        });
                        return [2];
                }
            });
        }); });
    };
    TextEditor.getNoteData = function (args) {
        return __awaiter(this, void 0, void 0, function () {
            var key, workspaceId, noteGlobalId, data, noteData, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        key = args.key;
                        if (key) {
                            data = key.split('#')[0];
                            if (data) {
                                noteData = data.split('_');
                                if (noteData.length > 1) {
                                    workspaceId = noteData[0];
                                    noteGlobalId = noteData[1];
                                }
                            }
                        }
                        if (!workspaceId) return [3, 2];
                        return [4, workspace_1.default.getLocalId(workspaceId)];
                    case 1:
                        _a = _b.sent();
                        return [3, 3];
                    case 2:
                        _a = null;
                        _b.label = 3;
                    case 3:
                        workspaceId = _a;
                        return [2, {
                                globalId: key,
                                noteGlobalId: noteGlobalId,
                                workspaceId: workspaceId
                            }];
                }
            });
        });
    };
    TextEditor.updateSyncedStatus = function (inputData) {
        if (instance_1.default.get()) {
            instance_1.default.get().webContents.send('event:editor:update:synced-status:request', inputData);
        }
    };
    TextEditor.makeBgNoteSync = function (inputData) {
        var _this = this;
        return new Promise(function (resolve) {
            if (instance_1.default.get()) {
                electron_1.ipcMain.removeAllListeners('event:editor:sync-callback:data:request');
                var timeout_1 = setTimeout(function () {
                    electron_1.ipcMain.removeAllListeners('event:editor:sync-callback:data:request');
                    return resolve({
                        result: false
                    });
                }, 10000);
                electron_1.ipcMain.once('event:editor:sync-callback:data:request', function (event, args) { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        if (timeout_1) {
                            clearTimeout(timeout_1);
                        }
                        if (!args) {
                            return [2, resolve({
                                    result: false
                                })];
                        }
                        return [2, resolve(args)];
                    });
                }); });
                instance_1.default.get().webContents.send('event:editor:bg:note-sync:request', inputData);
            }
        });
    };
    TextEditor.updateNote = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, globalId, hasLocalChanges, curDate, itemQueryData, itemSaveData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, globalId = inputData.globalId, hasLocalChanges = inputData.hasLocalChanges;
                        if (!(workspaceId && globalId)) return [3, 2];
                        return [4, workspace_1.default.getLocalId(workspaceId)];
                    case 1:
                        workspaceId = _a.sent();
                        curDate = dateHandler_1.default.now();
                        itemQueryData = { globalId: globalId };
                        itemSaveData = {
                            "dateUpdated": curDate,
                            "updatedAt": curDate,
                            "dateUpdatedUser": curDate,
                        };
                        if (hasLocalChanges) {
                            itemSaveData["syncDate"] = curDate;
                            itemSaveData["needSync"] = true;
                        }
                        item_1.default.update(itemQueryData, itemSaveData, { workspaceId: workspaceId }, function () {
                            socketFunctions_1.default.sendItemUpdateMessage({ workspaceId: workspaceId, globalId: globalId });
                        });
                        _a.label = 2;
                    case 2: return [2];
                }
            });
        });
    };
    TextEditor.updateNoteText = function (_a) {
        var noteId = _a.noteId, workspaceId = _a.workspaceId, textString = _a.textString;
        return __awaiter(this, void 0, void 0, function () {
            var curDate_1, saveTextData_1, queryData;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!(workspaceId && noteId)) return [3, 2];
                        return [4, workspace_1.default.getLocalId(workspaceId)];
                    case 1:
                        workspaceId = _b.sent();
                        curDate_1 = dateHandler_1.default.now();
                        saveTextData_1 = {
                            noteGlobalId: noteId,
                            text: textString,
                            textShort: textHandler_1.default.stripTags(textString),
                            'length': textString.text ? textString.text.length : 0
                        };
                        queryData = { noteGlobalId: saveTextData_1.noteGlobalId };
                        text_1.default.find(queryData, { workspaceId: workspaceId }, function (err, textInstance) {
                            if (err) {
                                return;
                            }
                            if (textInstance) {
                                var queryUpdateData = { noteGlobalId: saveTextData_1.noteGlobalId };
                                text_1.default.update(queryUpdateData, syncPropsHandler_1.setUpdateProps(saveTextData_1, curDate_1), { workspaceId: workspaceId }, function () { });
                            }
                            else {
                                var saveItemData = text_1.default.getDefaultModel(saveTextData_1.noteGlobalId);
                                text_1.default.add(syncPropsHandler_1.setUpdateProps(saveItemData, curDate_1), { workspaceId: workspaceId }, function () { });
                            }
                        });
                        _b.label = 2;
                    case 2: return [2];
                }
            });
        });
    };
    TextEditor.downloadAttach = function (_a) {
        var item = _a.item, options = _a.options;
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_b) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var noteId, attachmentId, workspaceId;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!state_1.default.get()) {
                                        return [2, resolve(null)];
                                    }
                                    if (!item || !options) {
                                        return [2, resolve(null)];
                                    }
                                    noteId = item;
                                    attachmentId = options.attachmentId;
                                    return [4, SelectedWorkspace_1.default.getGlobalId()];
                                case 1:
                                    workspaceId = _a.sent();
                                    NimbusSDK_1.default.getApi().getStructureNotes({ workspaceId: workspaceId, start: 0, amount: 1, globalId: noteId }, function (err, body) { return __awaiter(_this, void 0, void 0, function () {
                                        var notes, note, attachements, attachmentData, attachmentObj, targetPath, exists, attachDownloadResult, _a, attachment, downloaded;
                                        return __generator(this, function (_b) {
                                            switch (_b.label) {
                                                case 0:
                                                    if (err || !body) {
                                                        return [2, resolve(null)];
                                                    }
                                                    notes = body.notes;
                                                    if (!notes) {
                                                        return [2, resolve(null)];
                                                    }
                                                    if (!notes.length) {
                                                        return [2, resolve(null)];
                                                    }
                                                    note = notes[0];
                                                    if (!note) {
                                                        return [2, resolve(null)];
                                                    }
                                                    attachements = note.attachements;
                                                    if (!attachements) {
                                                        return [2, resolve(null)];
                                                    }
                                                    if (!attachements.length) {
                                                        return [2, resolve(null)];
                                                    }
                                                    attachmentData = attachements.find(function (attachmentItem) { return (attachmentItem.global_id === attachmentId); });
                                                    if (!attachmentData) {
                                                        return [2, resolve(null)];
                                                    }
                                                    attachmentObj = attach_1.default.prepareModelData(attachmentData);
                                                    attachmentObj.noteGlobalId = noteId;
                                                    attachmentObj.globalId = attachmentId;
                                                    attachmentObj.needSync = false;
                                                    attachmentObj.isDownloaded = false;
                                                    targetPath = pdb_1.default.getClientAttachmentPath() + "/" + attachmentObj.storedFileUUID;
                                                    return [4, fs_extra_1.default.exists(targetPath)];
                                                case 1:
                                                    exists = _b.sent();
                                                    if (exists) {
                                                        TextEditor.updateTextAttachmentsSchedule(noteId, attachmentId, attachmentObj);
                                                        return [2, resolve(null)];
                                                    }
                                                    return [4, AttachmentObjRepository_1.default.update({
                                                            workspaceId: workspaceId,
                                                            attachmentObjs: [attachmentObj]
                                                        })];
                                                case 2:
                                                    _b.sent();
                                                    return [4, AttachmentSingleDownloader_1.default.download({
                                                            workspaceId: workspaceId,
                                                            globalId: attachmentId
                                                        })];
                                                case 3:
                                                    attachDownloadResult = _b.sent();
                                                    if (!attachDownloadResult) {
                                                        return [2, resolve(null)];
                                                    }
                                                    _a = attachDownloadResult.response, attachment = _a.attachment, downloaded = _a.downloaded;
                                                    if (!downloaded || !attachment) {
                                                        return [2, resolve(null)];
                                                    }
                                                    TextEditor.updateTextAttachmentsSchedule(noteId, attachmentId, attachment);
                                                    return [2];
                                            }
                                        });
                                    }); });
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    TextEditor.updateTextAttachmentsSchedule = function (noteId, attachmentId, attachment) {
        if (instance_1.default.get()) {
            instance_1.default.get().webContents.send('event:client:update:editor:downloaded:attach:response', {
                noteId: noteId,
                attachGlobalId: attachmentId,
                serverAttachment: attachment
            });
        }
    };
    TextEditor.socketTextMessage = function (inputData) {
        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
        socketFunctions_1.default.sendTextUpdateMessage({
            workspaceId: workspaceId,
            globalId: noteGlobalId,
            typing: true
        });
    };
    return TextEditor;
}());
exports.default = TextEditor;
