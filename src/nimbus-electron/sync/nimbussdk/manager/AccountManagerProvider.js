"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var AccountManager_1 = __importDefault(require("./AccountManager"));
var manager = null;
var AccountManagerProvider = (function () {
    function AccountManagerProvider() {
    }
    AccountManagerProvider.get = function () {
        if (!manager) {
            manager = new AccountManager_1.default();
        }
        return manager;
    };
    return AccountManagerProvider;
}());
exports.default = AccountManagerProvider;
