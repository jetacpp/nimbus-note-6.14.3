"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var userSettings_1 = __importDefault(require("../../../db/models/userSettings"));
var SyncManager_1 = __importDefault(require("../../process/SyncManager"));
var SelectedWorkspace_1 = __importDefault(require("../../../workspace/SelectedWorkspace"));
var syncTimer = {};
var nextQueueSync = {};
var AutoSyncManager = (function () {
    function AutoSyncManager() {
    }
    AutoSyncManager.initTimer = function (inputData) {
        var workspaceId = inputData.workspaceId, initState = inputData.initState;
        if (!initState) {
            initState = false;
        }
        AutoSyncManager.setTimer({ workspaceId: workspaceId, initState: initState });
    };
    AutoSyncManager.updateTimer = function (inputData) {
        var workspaceId = inputData.workspaceId;
        var initState = false;
        AutoSyncManager.setTimer({ workspaceId: workspaceId, initState: initState });
    };
    AutoSyncManager.setTimer = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        return __awaiter(this, void 0, void 0, function () {
                            var workspaceId, initState, syncInfo, syncTimerHandler;
                            var _this = this;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        workspaceId = inputData.workspaceId, initState = inputData.initState;
                                        return [4, AutoSyncManager.getSyncInfo(inputData)];
                                    case 1:
                                        syncInfo = _a.sent();
                                        if (syncInfo.syncStatus) {
                                            AutoSyncManager.clearTimer(inputData);
                                            syncTimerHandler = function () { return __awaiter(_this, void 0, void 0, function () {
                                                var _a, _b, _c;
                                                return __generator(this, function (_d) {
                                                    switch (_d.label) {
                                                        case 0:
                                                            _a = !workspaceId;
                                                            if (!_a) return [3, 2];
                                                            return [4, SelectedWorkspace_1.default.getGlobalId()];
                                                        case 1:
                                                            _a = !(_d.sent());
                                                            _d.label = 2;
                                                        case 2:
                                                            if (_a) {
                                                                AutoSyncManager.makeSync(inputData);
                                                            }
                                                            _b = workspaceId;
                                                            if (!_b) return [3, 4];
                                                            _c = workspaceId;
                                                            return [4, SelectedWorkspace_1.default.getGlobalId()];
                                                        case 3:
                                                            _b = (_c === (_d.sent()));
                                                            _d.label = 4;
                                                        case 4:
                                                            if (_b) {
                                                                AutoSyncManager.makeSync(inputData);
                                                            }
                                                            return [2];
                                                    }
                                                });
                                            }); };
                                            if (initState) {
                                                setTimeout(syncTimerHandler, 2000);
                                            }
                                            if (typeof (syncTimer[workspaceId]) === 'undefined') {
                                                syncTimer[workspaceId] = setInterval(syncTimerHandler, 60000 * syncInfo.syncTimer);
                                            }
                                        }
                                        return [2, resolve(true)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    AutoSyncManager.clearTimer = function (inputData) {
        var workspaceId = inputData.workspaceId;
        if (typeof (syncTimer[workspaceId]) !== "undefined" && syncTimer[workspaceId]) {
            clearInterval(syncTimer[workspaceId]);
            delete syncTimer[workspaceId];
        }
    };
    AutoSyncManager.clearTimers = function () {
        if (syncTimer) {
            for (var syncTimerKey in syncTimer) {
                if (syncTimer.hasOwnProperty(syncTimerKey) && syncTimer[syncTimerKey]) {
                    clearInterval(syncTimer[syncTimerKey]);
                    delete syncTimer[syncTimerKey];
                }
            }
        }
    };
    AutoSyncManager.makeSync = function (inputData) {
        SyncManager_1.default.startSyncOnline(inputData);
    };
    AutoSyncManager.getSyncInfo = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var syncInfo;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, userSettings_1.default.getSyncInfo(inputData)];
                                case 1:
                                    syncInfo = _a.sent();
                                    resolve(syncInfo);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    AutoSyncManager.checkAutoSyncEnable = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var syncInfo;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, AutoSyncManager.getSyncInfo(inputData)];
                                case 1:
                                    syncInfo = _a.sent();
                                    resolve(!!syncInfo.syncStatus);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    AutoSyncManager.getQueueSync = function (inputData) {
        var workspaceId = inputData.workspaceId;
        return typeof (nextQueueSync[workspaceId]) !== "undefined" ? nextQueueSync[workspaceId] : false;
    };
    AutoSyncManager.setQueueSync = function (inputData) {
        var workspaceId = inputData.workspaceId, state = inputData.state;
        state = state || false;
        nextQueueSync[workspaceId] = state;
    };
    AutoSyncManager.clearQueueSync = function (inputData) {
        var workspaceId = inputData.workspaceId;
        var state = false;
        AutoSyncManager.setQueueSync({ workspaceId: workspaceId, state: state });
    };
    AutoSyncManager.makeQueueSync = function (inputData) {
        if (AutoSyncManager.getQueueSync(inputData)) {
            AutoSyncManager.makeSync(inputData);
        }
    };
    return AutoSyncManager;
}());
exports.default = AutoSyncManager;
