"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var App_1 = __importDefault(require("../../process/application/App"));
var Session_1 = __importDefault(require("./Session"));
var SecurityManager_1 = __importDefault(require("./SecurityManager"));
var generatorHandler_1 = __importDefault(require("../../../utilities/generatorHandler"));
var settings_1 = __importDefault(require("../../../db/models/settings"));
var userSettings_1 = __importDefault(require("../../../db/models/userSettings"));
var workspace_1 = __importDefault(require("../../../db/models/workspace"));
var config_1 = __importDefault(require("../../../../config"));
var AccountManager = (function () {
    function AccountManager() {
    }
    AccountManager.prototype.isAuthorized = function (callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var accountData = [null, null];
        this.getAccountSession(function (err, session) {
            if (err || !session) {
                return _this.isAuthorizedHandler(null, accountData, callback);
            }
            _this.getUserEmail(function (err, userEmail) {
                if (userEmail !== null) {
                    _this.getUniqueUserName(function (err, uniqueUserName) {
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("is Authorized user: ", userEmail);
                        }
                        SecurityManager_1.default.get().getAccount(userEmail, function (err, accountData) {
                            _this.isAuthorizedHandler(userEmail, accountData, callback);
                        });
                    });
                }
                else {
                    _this.isAuthorizedHandler(userEmail, accountData, callback);
                }
            });
        });
    };
    AccountManager.prototype.isAuthorizedHandler = function (userEmail, accountData, callback) {
        userEmail = userEmail ? userEmail.trim() : userEmail;
        var sessionId = accountData[0] ? accountData[0].trim() : '';
        var isAuthorized = Boolean(userEmail && sessionId);
        return callback(null, isAuthorized);
    };
    AccountManager.prototype.setUserEmail = function (email, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.USER_EMAIL, email.toLowerCase(), function (err, value) {
            callback(null, value);
        });
    };
    AccountManager.prototype.getUserEmail = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.USER_EMAIL, null, callback);
    };
    AccountManager.prototype.isOfflineAccount = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.OFFLINE_ACCOUNT, false, callback);
    };
    AccountManager.prototype.enableOfflineAccount = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.OFFLINE_ACCOUNT, true);
        this.clearAccountSession();
        this.remove(AccountManager.USER_ID);
        this.remove(AccountManager.USER_LOGIN);
        this.remove(AccountManager.UNIQUE_USER_NAME);
        this.remove(AccountManager.PREMIUM_ACTIVE);
        this.remove(AccountManager.PREMIUM_START_DATE);
        this.remove(AccountManager.PREMIUM_END_DATE);
        this.remove(AccountManager.PREMIUM_SOURCE);
        this.remove(AccountManager.REGISTER_DATE);
        this.remove(AccountManager.DAYS_TO_QUOTA_RESET);
        this.remove(AccountManager.TRAFFIC_MAX);
        this.remove(AccountManager.TRAFFIC_CURRENT);
        this.remove(AccountManager.LIMIT_NOTES_MAX_SIZE);
        this.remove(AccountManager.LIMIT_NOTES_MONTH_USAGE_QUOTA);
        this.remove(AccountManager.LIMIT_NOTES_MAX_ATTACHMENT_SIZE);
        this.remove(AccountManager.EMAIL_FOR_SHARE_NOTES);
        this.remove(AccountManager.LAST_UPDATE_TIME);
    };
    AccountManager.prototype.logout = function () {
        this.disableOfflineAccount();
    };
    AccountManager.prototype.disableOfflineAccount = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.OFFLINE_ACCOUNT, false);
        return callback(null, true);
    };
    AccountManager.prototype.setAccountSession = function (session, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var email = session.getUserEmail();
        if (!email) {
            return callback(new Error('Can not get user session email: setAccountSession'), null);
        }
        var sessionId = session.getSessionId();
        if (!sessionId) {
            return callback(new Error('Can not get user session id: setAccountSession'), null);
        }
        SecurityManager_1.default.get().updateAccount(email.toLowerCase(), sessionId, function () {
            _this.set(AccountManager.USER_EMAIL, email.toLowerCase(), function (err, value) {
                callback(null, value);
            });
        });
    };
    AccountManager.prototype.getAccountSession = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        try {
            this.getUserEmail(function (err, userEmail) {
                if (!userEmail) {
                    return callback(new Error('Can not get user session email: getAccountSession'), null);
                }
                SecurityManager_1.default.get().getAccount(userEmail.toLowerCase(), function (err, accountData) {
                    if (err || !accountData) {
                        return callback(new Error('Can not get user session id: getAccountSession'), null);
                    }
                    return callback(err, new Session_1.default(userEmail.toLowerCase(), accountData[0]));
                });
            });
        }
        catch (err) {
            console.log('Problem getAccountSession:', err);
            return callback(new Error('Can not get user session email: getAccountSession'), null);
        }
    };
    AccountManager.prototype.setUserId = function (userId, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.USER_ID, userId, callback);
    };
    AccountManager.prototype.getUserId = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.USER_ID, -1, callback);
    };
    AccountManager.prototype.setUserLogin = function (userLogin, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.USER_LOGIN, userLogin.toLowerCase(), callback);
    };
    AccountManager.prototype.getUserLogin = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.USER_LOGIN, null, callback);
    };
    AccountManager.prototype.setUserFirstName = function (userFirstName, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.USER_FIRST_NAME, userFirstName, callback);
    };
    AccountManager.prototype.getUserFirstName = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.USER_FIRST_NAME, null, callback);
    };
    AccountManager.prototype.setUserLastName = function (userLastName, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.USER_LAST_NAME, userLastName, callback);
    };
    AccountManager.prototype.getUserLastName = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.USER_LAST_NAME, null, callback);
    };
    AccountManager.prototype.setUserAvatar = function (userAvatar, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.USER_AVATAR, userAvatar, callback);
    };
    AccountManager.prototype.getUserAvatar = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.USER_AVATAR, null, callback);
    };
    AccountManager.prototype.setPremiumActive = function (isActive, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.PREMIUM_ACTIVE, isActive, callback);
    };
    AccountManager.prototype.isPremiumActive = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.PREMIUM_ACTIVE, false, callback);
    };
    AccountManager.prototype.isPremiumActiveAsync = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        _this.get(AccountManager.PREMIUM_ACTIVE, false, function (err, res) {
                            resolve(res);
                        });
                    })];
            });
        });
    };
    AccountManager.prototype.setPremiumStartDate = function (startDate, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.PREMIUM_START_DATE, startDate, callback);
    };
    AccountManager.prototype.getPremiumStartDate = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.PREMIUM_START_DATE, null, callback);
    };
    AccountManager.prototype.setPremiumEndDate = function (endDate, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.PREMIUM_END_DATE, endDate, callback);
    };
    AccountManager.prototype.getPremiumEndDate = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.PREMIUM_END_DATE, null, callback);
    };
    AccountManager.prototype.setPremiumSource = function (premiumSource, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.PREMIUM_SOURCE, premiumSource, callback);
    };
    AccountManager.prototype.getPremiumSource = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.PREMIUM_SOURCE, null, callback);
    };
    AccountManager.prototype.setRegisterDate = function (registerDate, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.REGISTER_DATE, registerDate, callback);
    };
    AccountManager.prototype.getRegisterDate = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.REGISTER_DATE, null, callback);
    };
    AccountManager.prototype.setDaysToQuotaReset = function (daysToQuotaReset, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.DAYS_TO_QUOTA_RESET, daysToQuotaReset, callback);
    };
    AccountManager.prototype.getDaysToQuotaReset = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.DAYS_TO_QUOTA_RESET, 0, callback);
    };
    AccountManager.prototype.setTrafficMax = function (trafficMax, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.TRAFFIC_MAX, trafficMax, callback);
    };
    AccountManager.prototype.getTrafficMax = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.TRAFFIC_MAX, 0, callback);
    };
    AccountManager.prototype.getTrafficMaxAsync = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        _this.get(AccountManager.TRAFFIC_MAX, 0, function (err, res) {
                            resolve(res);
                        });
                    })];
            });
        });
    };
    AccountManager.prototype.setTrafficCurrent = function (trafficCurrent, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.TRAFFIC_CURRENT, trafficCurrent, callback);
    };
    AccountManager.prototype.getTrafficCurrent = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.TRAFFIC_CURRENT, 0, callback);
    };
    AccountManager.prototype.setLimitNotesMaxSize = function (limitNotesMaxSize, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.LIMIT_NOTES_MAX_SIZE, limitNotesMaxSize, callback);
    };
    AccountManager.prototype.getLimitNotesMaxSize = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.LIMIT_NOTES_MAX_SIZE, AccountManager.LIMIT_NOTES_MAX_SIZE_DEFAULT, callback);
    };
    AccountManager.prototype.setLimitNotesMonthUsageQuota = function (limitNotesMonthUsageQuota, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.LIMIT_NOTES_MONTH_USAGE_QUOTA, limitNotesMonthUsageQuota, callback);
    };
    AccountManager.prototype.getLimitNotesMonthUsageQuota = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.LIMIT_NOTES_MONTH_USAGE_QUOTA, 0, callback);
    };
    AccountManager.prototype.setLimitNotesMaxAttachmentSize = function (limitNotesMaxAttachmentSize, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.LIMIT_NOTES_MAX_ATTACHMENT_SIZE, limitNotesMaxAttachmentSize, callback);
    };
    AccountManager.prototype.getLimitNotesMaxAttachmentSize = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.LIMIT_NOTES_MAX_ATTACHMENT_SIZE, AccountManager.LIMIT_NOTES_MAX_ATTACHMENT_SIZE_DEFAULT, callback);
    };
    AccountManager.prototype.setEmailForShareNotes = function (emailForShareNotes, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.EMAIL_FOR_SHARE_NOTES, emailForShareNotes, callback);
    };
    AccountManager.prototype.getEmailForShareNotes = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.EMAIL_FOR_SHARE_NOTES, null, callback);
    };
    AccountManager.prototype.setSyncTypeIsHeader = function (isSyncHeader, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.set(AccountManager.SYNC_TYPE_IS_HEADER, isSyncHeader, callback);
    };
    AccountManager.prototype.getSyncTypeIsHeader = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return this.get(AccountManager.SYNC_TYPE_IS_HEADER, true, function (err, value) {
            if (!value) {
                value = App_1.default.SYNC_TYPES.NONE;
            }
            callback(err, value);
        });
    };
    AccountManager.prototype.getAvailableQuotaTraffic = function (callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.getTrafficMax(function (err, maxLimit) {
            _this.getTrafficCurrent(function (err, currentTraffic) {
                var availableQuotaTraffic = maxLimit - currentTraffic;
                callback(err, availableQuotaTraffic);
            });
        });
    };
    AccountManager.prototype.clearAccountSession = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.getUserEmail(function (err, userEmail) {
            if (err || !userEmail) {
                return callback(null, null);
            }
            SecurityManager_1.default.get().removeAccount(userEmail);
            return callback(null, true);
        });
    };
    AccountManager.setLastUpdateTime = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, lastUpdateTime = inputData.lastUpdateTime;
        if (!workspaceId) {
            workspaceId = workspace_1.default.DEFAULT_NAME;
        }
        userSettings_1.default.set("lastUpdateTime:" + workspaceId, lastUpdateTime, callback);
    };
    AccountManager.getLastUpdateTime = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        if (!workspaceId) {
            workspaceId = workspace_1.default.DEFAULT_NAME;
        }
        userSettings_1.default.get("lastUpdateTime:" + workspaceId, function (err, lastUpdateTime) {
            if (!lastUpdateTime || typeof (lastUpdateTime) === "object") {
                lastUpdateTime = 0;
            }
            callback(err, lastUpdateTime);
        });
    };
    AccountManager.prototype.setUniqueUserName = function (email, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var uniqueUserName = email.replace("@", "")
            .replace(".", "")
            .toLowerCase();
        this.set(AccountManager.UNIQUE_USER_NAME, uniqueUserName + (generatorHandler_1.default.randomString(5)), callback);
    };
    AccountManager.prototype.getUniqueUserName = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        this.get(AccountManager.UNIQUE_USER_NAME, AccountManager.OFFLINE_UNIQUE_USER_NAME, callback);
    };
    AccountManager.prototype.getOfflineUniqueUserName = function () {
        return AccountManager.OFFLINE_UNIQUE_USER_NAME;
    };
    AccountManager.prototype.get = function (key, defValue, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        settings_1.default.get(key, function (err, value) {
            if (typeof (value) === "undefined") {
                value = null;
            }
            else if (typeof (value) === "object") {
                if (Object.keys(value).length === 0) {
                    value = null;
                }
            }
            if (!value) {
                if (!defValue) {
                    if (typeof (defValue) === "number") {
                        defValue = 0;
                    }
                    else if (typeof (defValue) === "string") {
                        defValue = "";
                    }
                }
                value = defValue;
            }
            callback(err, value);
        });
    };
    AccountManager.prototype.set = function (key, value, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        if (typeof (value) === "undefined") {
            return callback(null, value);
        }
        settings_1.default.set(key, value, function (err, response) {
            callback(err, response);
        });
    };
    AccountManager.prototype.remove = function (key, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        settings_1.default.remove(key, callback);
    };
    AccountManager.prototype.contains = function (key, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return settings_1.default.has(key, callback);
    };
    AccountManager.OFFLINE_UNIQUE_USER_NAME = "offline-account";
    AccountManager.OFFLINE_ACCOUNT = "offline";
    AccountManager.USER_EMAIL = "user_email";
    AccountManager.LAST_UPDATE_TIME = "last_update_time";
    AccountManager.USER_ID = "user_id";
    AccountManager.USER_LOGIN = "user_login";
    AccountManager.USER_FIRST_NAME = "user_first_name";
    AccountManager.USER_LAST_NAME = "user_last_name";
    AccountManager.USER_AVATAR = "user_avatar";
    AccountManager.UNIQUE_USER_NAME = "unique_user_name";
    AccountManager.PREMIUM_ACTIVE = "premium_status";
    AccountManager.PREMIUM_START_DATE = "premium_start_date";
    AccountManager.PREMIUM_END_DATE = "premium_end_date";
    AccountManager.PREMIUM_SOURCE = "premium_source";
    AccountManager.REGISTER_DATE = "register_date";
    AccountManager.DAYS_TO_QUOTA_RESET = "days_to_quota_reset";
    AccountManager.TRAFFIC_MAX = "traffic_max";
    AccountManager.TRAFFIC_CURRENT = "traffic_current";
    AccountManager.LIMIT_NOTES_MAX_SIZE = "limit_NOTES_MAX_SIZE";
    AccountManager.LIMIT_NOTES_MONTH_USAGE_QUOTA = "limit_NOTES_MONTH_USAGE_QUOTA";
    AccountManager.LIMIT_NOTES_MAX_ATTACHMENT_SIZE = "limit_NOTES_MAX_ATTACHMENT_SIZE";
    AccountManager.SYNC_TYPE_IS_HEADER = "sync_type_is_header";
    AccountManager.EMAIL_FOR_SHARE_NOTES = "EMAIL_FOR_SHARE_NOTES";
    AccountManager.LIMIT_NOTES_MAX_SIZE_DEFAULT = 100000000;
    AccountManager.LIMIT_NOTES_MAX_ATTACHMENT_SIZE_DEFAULT = 100000000;
    return AccountManager;
}());
exports.default = AccountManager;
