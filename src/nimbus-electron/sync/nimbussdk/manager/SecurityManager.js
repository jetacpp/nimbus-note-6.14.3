"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var settings_1 = __importDefault(require("../../../db/models/settings"));
var INSTANCE;
var SecurityManager = (function () {
    function SecurityManager(context) {
        this.context = context;
    }
    SecurityManager.get = function () {
        if (!INSTANCE) {
            INSTANCE = new SecurityManager("nimbus-electron");
        }
        return INSTANCE;
    };
    SecurityManager.prototype.updateAccount = function (accountKey, sessionId, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        settings_1.default.set(accountKey + "_sessionId", accountKey + '_' + sessionId, function () {
            callback(null, true);
        });
    };
    SecurityManager.prototype.removeAccount = function (accountKey) {
        settings_1.default.remove(accountKey + "_sessionId");
        settings_1.default.remove("user_login");
        settings_1.default.remove("user_email");
        settings_1.default.remove("user_id");
    };
    SecurityManager.prototype.getAccount = function (accountKey, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var values = {};
        values[0] = null;
        settings_1.default.get(accountKey + "_sessionId", function (err, value) {
            values[0] = value && typeof (value) === "string" ? value : null;
            if (values[0] !== null) {
                values[0] = values[0].replace(accountKey + "_", "");
            }
            callback(null, values);
        });
    };
    return SecurityManager;
}());
exports.default = SecurityManager;
