"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var USER_EMAIL = "user_email";
var SESSION_ID = "sessionid";
var Session = (function () {
    function Session(userEmail, sessionId) {
        this.userEmail = userEmail;
        this.sessionId = sessionId;
    }
    Session.prototype.getUserEmail = function () {
        return this.userEmail;
    };
    Session.prototype.getSessionId = function () {
        return this.sessionId;
    };
    Session.prototype.getToken = function () {
        return '';
    };
    Session.prototype.getData = function () {
        var map = {};
        map[USER_EMAIL] = this.userEmail;
        map[SESSION_ID] = this.sessionId;
        return map;
    };
    return Session;
}());
exports.default = Session;
