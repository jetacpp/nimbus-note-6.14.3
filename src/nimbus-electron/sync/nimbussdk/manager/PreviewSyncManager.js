"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var socketFunctions_1 = __importDefault(require("../../socket/socketFunctions"));
var notesPreviewData = {};
var PreviewSyncManager = (function () {
    function PreviewSyncManager() {
    }
    PreviewSyncManager.init = function (inputData) {
        var workspaceId = inputData.workspaceId;
        if (typeof (notesPreviewData[workspaceId]) === 'undefined') {
            notesPreviewData[workspaceId] = {};
        }
        notesPreviewData[workspaceId] = {
            update: {},
            remove: {}
        };
    };
    PreviewSyncManager.clear = function (inputData) {
        PreviewSyncManager.init(inputData);
    };
    PreviewSyncManager.pushUpdateEvent = function (inputData) {
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, attachmentGlobalId = inputData.attachmentGlobalId;
        if (typeof (notesPreviewData[workspaceId]) === 'undefined') {
            notesPreviewData[workspaceId] = {
                update: {},
                remove: {}
            };
        }
        notesPreviewData[workspaceId].update[globalId] = attachmentGlobalId;
        socketFunctions_1.default.sendItemPreviewUpdateMessage(inputData);
    };
    PreviewSyncManager.pushRemoveEvent = function (inputData) {
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
        if (typeof (notesPreviewData[workspaceId]) === 'undefined') {
            notesPreviewData[workspaceId] = {
                update: {},
                remove: {}
            };
        }
        notesPreviewData[workspaceId].remove[globalId] = globalId;
        socketFunctions_1.default.sendItemPreviewRemoveMessage(inputData);
    };
    PreviewSyncManager.updateNotePreview = function (inputData) {
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, attachmentGlobalId = inputData.attachmentGlobalId;
        if (typeof (notesPreviewData[workspaceId]) === 'undefined') {
            return;
        }
        if (typeof (notesPreviewData[workspaceId].update) === 'undefined') {
            return;
        }
        if (typeof (notesPreviewData[workspaceId].update[globalId]) === 'undefined') {
            return;
        }
        if (notesPreviewData[workspaceId].update[globalId] == attachmentGlobalId) {
            socketFunctions_1.default.sendItemPreviewUpdateMessage(inputData);
        }
    };
    PreviewSyncManager.getEvents = function (inputData) {
        var workspaceId = inputData.workspaceId;
        if (typeof (notesPreviewData[workspaceId]) === 'undefined') {
            return {
                update: {},
                remove: {}
            };
        }
        return notesPreviewData[workspaceId];
    };
    return PreviewSyncManager;
}());
exports.default = PreviewSyncManager;
