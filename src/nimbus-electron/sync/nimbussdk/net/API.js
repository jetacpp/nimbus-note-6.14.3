"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var node_fetch_1 = __importDefault(require("node-fetch"));
var fs_extra_1 = __importDefault(require("fs-extra"));
var ApiConst_1 = __importDefault(require("./ApiConst"));
var Session_1 = __importDefault(require("../manager/Session"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../process/events/SyncStatusChangedEvent"));
var NimbusErrorHandler_1 = __importDefault(require("./exception/common/NimbusErrorHandler"));
var NoteIsInTrashException_1 = __importDefault(require("./exception/NoteIsInTrashException"));
var errorHandler_1 = __importDefault(require("../../../utilities/errorHandler"));
var AccountManager_1 = __importDefault(require("../manager/AccountManager"));
var SignInRequest_1 = __importDefault(require("./request/SignInRequest"));
var RegisterNewUserRequest_1 = __importDefault(require("./request/RegisterNewUserRequest"));
var NotesAccountRequest_1 = __importDefault(require("./request/NotesAccountRequest"));
var UserInfoRequest_1 = __importDefault(require("./request/UserInfoRequest"));
var UpdateUserInfoRequest_1 = __importDefault(require("./request/UpdateUserInfoRequest"));
var UserVariablesRequest_1 = __importDefault(require("./request/UserVariablesRequest"));
var UpdateUserVariableRequest_1 = __importDefault(require("./request/UpdateUserVariableRequest"));
var UserChangePasswordRequest_1 = __importDefault(require("./request/UserChangePasswordRequest"));
var RemindPasswordRequest_1 = __importDefault(require("./request/RemindPasswordRequest"));
var FilesTempExistsRequest_1 = __importDefault(require("./request/FilesTempExistsRequest"));
var FilesPreuploadRequest_1 = __importDefault(require("./request/FilesPreuploadRequest"));
var ProductRecipeUploadRequest_1 = __importDefault(require("./request/ProductRecipeUploadRequest"));
var NotesGetStructureRequest_1 = __importDefault(require("./request/NotesGetStructureRequest"));
var NotesTotalAmountRequest_1 = __importDefault(require("./request/NotesTotalAmountRequest"));
var GetRemovedItemsRequest_1 = __importDefault(require("./request/GetRemovedItemsRequest"));
var NotesGetAllRequest_1 = __importDefault(require("./request/NotesGetAllRequest"));
var NotesSearchRequest_1 = __importDefault(require("./request/NotesSearchRequest"));
var NotesGetFoldersRequest_1 = __importDefault(require("./request/NotesGetFoldersRequest"));
var NotesGetTagsRequest_1 = __importDefault(require("./request/NotesGetTagsRequest"));
var NotesShareRequest_1 = __importDefault(require("./request/NotesShareRequest"));
var NotesUnshareRequest_1 = __importDefault(require("./request/NotesUnshareRequest"));
var NotesGetBackupsRequest_1 = __importDefault(require("./request/NotesGetBackupsRequest"));
var NotesViewBackupRequest_1 = __importDefault(require("./request/NotesViewBackupRequest"));
var NotesRestoreBackupRequest_1 = __importDefault(require("./request/NotesRestoreBackupRequest"));
var NotesInviteRequest_1 = __importDefault(require("./request/NotesInviteRequest"));
var NotesAcceptInviteRequest_1 = __importDefault(require("./request/NotesAcceptInviteRequest"));
var NoteIsExistOnServerRequest_1 = __importDefault(require("./request/NoteIsExistOnServerRequest"));
var NotesUpdateRequest_1 = __importDefault(require("./request/NotesUpdateRequest"));
var OrganizationsGetRequest_1 = __importDefault(require("./request/OrganizationsGetRequest"));
var WorkspacesGetRequest_1 = __importDefault(require("./request/WorkspacesGetRequest"));
var WorkspaceGetRequest_1 = __importDefault(require("./request/WorkspaceGetRequest"));
var WorkspaceCreateRequest_1 = __importDefault(require("./request/WorkspaceCreateRequest"));
var WorkspaceUpdateRequest_1 = __importDefault(require("./request/WorkspaceUpdateRequest"));
var WorkspaceDeleteRequest_1 = __importDefault(require("./request/WorkspaceDeleteRequest"));
var WorkspaceMembersGetRequest_1 = __importDefault(require("./request/WorkspaceMembersGetRequest"));
var WorkspaceMemberAddRequest_1 = __importDefault(require("./request/WorkspaceMemberAddRequest"));
var WorkspaceMemberUpdateRequest_1 = __importDefault(require("./request/WorkspaceMemberUpdateRequest"));
var WorkspaceMemberDeleteRequest_1 = __importDefault(require("./request/WorkspaceMemberDeleteRequest"));
var WorkspaceInvitesGetRequest_1 = __importDefault(require("./request/WorkspaceInvitesGetRequest"));
var WorkspaceInviteResendRequest_1 = __importDefault(require("./request/WorkspaceInviteResendRequest"));
var WorkspaceInviteUpdateRequest_1 = __importDefault(require("./request/WorkspaceInviteUpdateRequest"));
var WorkspaceInviteDeleteRequest_1 = __importDefault(require("./request/WorkspaceInviteDeleteRequest"));
var WorkspaceSyncStateRequest_1 = __importDefault(require("./request/WorkspaceSyncStateRequest"));
var MoveToWorkspaceRequest_1 = __importDefault(require("./request/MoveToWorkspaceRequest"));
var NotesTextTokenUpdateRequest_1 = __importDefault(require("./request/NotesTextTokenUpdateRequest"));
var NotesAnnotateRequest_1 = __importDefault(require("./request/NotesAnnotateRequest"));
var NotesGetAnnotationsRequest_1 = __importDefault(require("./request/NotesGetAnnotationsRequest"));
var ChallengeRequest_1 = __importDefault(require("./request/ChallengeRequest"));
var instance_1 = __importDefault(require("../../../window/instance"));
var apiMeLogout_1 = __importDefault(require("../../../request/interceptors/jsonInterceptors/me/apiMeLogout"));
var state_1 = __importDefault(require("../../../online/state"));
var NimbusLibSDK = null;
var API = (function () {
    function API(userApi, filesApi, notesApi, workspacesApi, organizationsApi, NimbusSdkModule) {
        this.userApi = userApi;
        this.filesApi = filesApi;
        this.notesApi = notesApi;
        this.workspacesApi = workspacesApi;
        this.organizationsApi = organizationsApi;
        NimbusLibSDK = NimbusSdkModule;
    }
    API.checkSignInStatus = function (err, errSession) {
        if ((err === API.ERROR_USER_NOT_AUTHORIZED) && instance_1.default.get() && state_1.default.get()) {
            apiMeLogout_1.default({}, function () { });
        }
    };
    API.prototype.signIn = function (login, password, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        self.userApi.signIn(new SignInRequest_1.default(login, password), function (err, response) {
            if (err || !response) {
                errorHandler_1.default.log({
                    err: err, response: response,
                    description: "API problem => signIn",
                    data: {
                        login: login,
                        password: password
                    }
                });
                return callback(err, response);
            }
            if (!response.body || !Object.keys(response.body).length) {
                errorHandler_1.default.log({
                    err: err, response: response,
                    description: "API problem => signIn",
                    data: {
                        login: login,
                        password: password
                    }
                });
                return callback(err, response);
            }
            self.auth(login, response.body.sessionId, callback);
        });
    };
    API.prototype.auth = function (login, sessionId, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        var accountManager = NimbusLibSDK.getAccountManager();
        accountManager.setUniqueUserName(login, function () {
            var session = new Session_1.default(login, sessionId);
            NimbusLibSDK.getAccountManager().setAccountSession(session, function () {
                self.userInfo({ skipSyncHandlers: true }, function (err, userInfo) {
                    if (err) {
                        errorHandler_1.default.log({
                            err: err, response: userInfo,
                            description: "API problem => signIn => userInfo"
                        });
                        return callback(err, {});
                    }
                    accountManager.setUserLogin(userInfo.login);
                    accountManager.setUserId(userInfo.user_id);
                    accountManager.setUserFirstName(userInfo.firstname);
                    accountManager.setUserLastName(userInfo.lastname);
                    accountManager.setUserAvatar(userInfo.avatar);
                    accountManager.setPremiumActive(userInfo.premium.active);
                    accountManager.setPremiumStartDate(userInfo.premium.start_date);
                    accountManager.setPremiumEndDate(userInfo.premium.end_date);
                    accountManager.setPremiumSource(userInfo.premium.source);
                    accountManager.setRegisterDate(userInfo.register_date);
                    accountManager.setDaysToQuotaReset(userInfo.days_to_quota_reset);
                    accountManager.setTrafficMax(userInfo.usage.notes.max);
                    accountManager.setTrafficCurrent(userInfo.usage.notes.current);
                    accountManager.setLimitNotesMaxSize(userInfo.limits.NOTES_MAX_SIZE);
                    accountManager.setLimitNotesMonthUsageQuota(userInfo.limits.NOTES_MONTH_USAGE_QUOTA);
                    accountManager.setLimitNotesMaxAttachmentSize(userInfo.limits.NOTES_MAX_ATTACHMENT_SIZE);
                    var signInData = userInfo;
                    self.account(function (err, accountInfo) {
                        if (err) {
                            errorHandler_1.default.log({
                                err: err, response: accountInfo,
                                description: "API problem => signIn => account"
                            });
                            return callback(err, signInData);
                        }
                        if (accountInfo && accountInfo.notes_email) {
                            signInData.notes_email = accountInfo.notes_email;
                        }
                        callback(err, signInData);
                    });
                });
            });
        });
    };
    API.prototype.signUp = function (login, password, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        var language = "en";
        if (language !== null && language.length > 2) {
            language = language.substring(0, 2);
        }
        self.userApi.signUp(new RegisterNewUserRequest_1.default(login, password, ApiConst_1.default.SERVICE, language), function (err, response) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: response,
                    description: "API problem => signUp",
                    data: {
                        login: login,
                        password: password
                    }
                });
                return callback(err, response);
            }
            self.signIn(login, password, callback);
        });
    };
    API.prototype.userLogin = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NimbusLibSDK.getAccountManager().getUserLogin(function (err, login) {
            if (err || !login) {
                errorHandler_1.default.log({
                    err: err, response: login,
                    description: "API problem => userLogin"
                });
                return callback(err, null);
            }
            callback(err, login.toLowerCase());
        });
    };
    API.prototype.userInfo = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            var errSession = err;
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => userInfo => getAccountSession"
                });
                return callback(err, null);
            }
            self.userApi.userInfo(session.getSessionId(), session.getToken(), new UserInfoRequest_1.default(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                var responseExist, accountManager, trafficCurrent, trafficMax, daysToReset, maxTextSize, maxAttachmentSize, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => userInfo",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "userInfo" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => userInfo",
                                data: inputData
                            });
                            API.checkSignInStatus(err, errSession);
                            return [2, callback(err, null)];
                        case 3:
                            responseExist = response.body && Object.keys(response.body).length;
                            if (responseExist) {
                                accountManager = NimbusLibSDK.getAccountManager();
                                trafficCurrent = response.body.usage.notes.current;
                                trafficMax = response.body.usage.notes.max;
                                daysToReset = response.body.days_to_quota_reset;
                                maxTextSize = response.body.limits.NOTES_MAX_SIZE;
                                maxAttachmentSize = response.body.limits.NOTES_MAX_ATTACHMENT_SIZE;
                                accountManager.setUserId(response.body.user_id);
                                accountManager.setUserLogin(response.body.login);
                                accountManager.setPremiumActive(response.body.premium.active);
                                accountManager.setPremiumStartDate(response.body.premium.start_date);
                                accountManager.setPremiumEndDate(response.body.premium.end_date);
                                accountManager.setPremiumSource(response.body.premium.source);
                                accountManager.setRegisterDate(response.body.register_date);
                                accountManager.setDaysToQuotaReset(daysToReset);
                                accountManager.setTrafficMax(trafficMax);
                                accountManager.setTrafficCurrent(trafficCurrent);
                                accountManager.setLimitNotesMaxSize(maxTextSize);
                                accountManager.setLimitNotesMaxAttachmentSize(maxAttachmentSize);
                                accountManager.setLimitNotesMonthUsageQuota(response.body.limits.NOTES_MONTH_USAGE_QUOTA);
                            }
                            responseData = responseExist ? response.body : null;
                            callback(err, responseData);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.updateUserInfo = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, body = inputData.body, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => updateUserInfo => getAccountSession"
                });
                return callback(err, null);
            }
            self.userApi.updateUserInfo(session.getSessionId(), session.getToken(), new UpdateUserInfoRequest_1.default(body), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => updateUserInfo",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "updateUserInfo" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => updateUserInfo",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.userVariables = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => userVariables => getAccountSession"
                });
                return callback(err, null);
            }
            self.userApi.userVariables(session.getSessionId(), session.getToken(), new UserVariablesRequest_1.default(), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => userVariables",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "userVariables" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => userVariables",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            if (typeof (response.body.vars) === 'undefined') {
                                return [2, callback(err, null)];
                            }
                            callback(err, response.body.vars);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.updateUserVariable = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, body = inputData.body, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => updateUserVariable => getAccountSession"
                });
                return callback(err, null);
            }
            self.userApi.updateUserVariable(session.getSessionId(), session.getToken(), new UpdateUserVariableRequest_1.default(body), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => updateUserVariable",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "updateUserVariable" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => updateUserVariable",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.account = function (callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => account => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.account(session.getSessionId(), session.getToken(), new NotesAccountRequest_1.default(), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                var accountManager, email1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "account" })) return [3, 2];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ err: err })];
                        case 1:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => account"
                            });
                            return [2, callback(err, null)];
                        case 2:
                            accountManager = NimbusLibSDK.getAccountManager();
                            email1 = response.body.notes_email;
                            accountManager.setEmailForShareNotes(email1, function () {
                                callback(err, response.body);
                            });
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.challenge = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        var state = inputData.state, answer = inputData.answer;
        self.userApi.challenge(new ChallengeRequest_1.default(state, answer), function (err, response) {
            if (NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "challenge" })) {
                errorHandler_1.default.log({
                    err: err, response: response,
                    description: "API problem => challenge"
                });
                return callback(err, response);
            }
            callback(null, response);
        });
    };
    API.prototype.userChangePassword = function (oldPassword, newPassword, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => userChangePassword => getAccountSession"
                });
                return callback(err, null);
            }
            self.userApi.userChangePassword(session.getSessionId(), session.getToken(), new UserChangePasswordRequest_1.default(oldPassword, newPassword), function (err, response) {
                if (NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "userChangePassword" })) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => userChangePassword",
                        data: {
                            oldPassword: oldPassword,
                            newPassword: newPassword
                        }
                    });
                }
                callback(err, response);
            });
        });
    };
    API.prototype.remindPassword = function (email, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => remindPassword => getAccountSession"
                });
                return callback(err, null);
            }
            self.userApi.remindPassword(session.getSessionId(), session.getToken(), new RemindPasswordRequest_1.default(email), function (err, response) {
                if (NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "remindPassword" })) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => remindPassword",
                        data: {
                            email: email
                        }
                    });
                }
                callback(err, response);
            });
        });
    };
    API.prototype.logout = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => logout => getAccountSession"
                });
            }
            self.clearAccountSession(function (err, response) {
                if (err) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => clearAccountSession"
                    });
                }
                callback(err, response);
            });
        });
    };
    API.prototype.clearAccountSession = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NimbusLibSDK.getAccountManager().clearAccountSession(callback);
    };
    API.prototype.productRecipeUpload = function (fileUrl, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => productRecipeUpload => getAccountSession"
                });
                return callback(err, null);
            }
            self.filesApi.productRecipeUpload(session.getSessionId(), session.getToken(), new ProductRecipeUploadRequest_1.default(fileUrl), function (err, response) {
                if (NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "productRecipeUpload" })) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => productRecipeUpload",
                        data: {
                            fileUrl: fileUrl
                        }
                    });
                }
                callback(err, response);
            });
        });
    };
    API.prototype.unlockPremium = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => unlockPremium => getAccountSession"
                });
                return callback(err, null);
            }
            self.userApi.unlockPremium(session.getSessionId(), session.getToken(), ApiConst_1.default.getBillingUrl(inputData), function (err, response) {
                if (NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "unlockPremium" })) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => unlockPremium",
                        data: inputData
                    });
                }
                callback(err, response);
            });
        });
    };
    API.prototype.oauth = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var provider, token, url, res, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        provider = inputData.provider, token = inputData.token;
                        url = "https://everhelper.me/auth/openidconnect.php?env=app&provider=" + provider + "&oauth_email_key=" + token + "&format=json";
                        return [4, node_fetch_1.default(url, {
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                },
                            })];
                    case 1:
                        res = _a.sent();
                        return [2, res.json()];
                    case 2:
                        error_1 = _a.sent();
                        errorHandler_1.default.log(error_1.message);
                        return [2, null];
                    case 3: return [2];
                }
            });
        });
    };
    API.prototype.filesPreupload = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, tempname, self, fileExist, err;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, tempname = inputData.tempname;
                        self = this;
                        return [4, fs_extra_1.default.exists(tempname)];
                    case 1:
                        fileExist = _a.sent();
                        if (!fileExist) {
                            err = new Error("filesPreupload => file not exist by path: " + tempname);
                            errorHandler_1.default.log({
                                err: err, response: null,
                                description: "API problem => filesPreupload",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        }
                        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
                            if (err) {
                                errorHandler_1.default.log({
                                    err: err, response: session,
                                    description: "API problem => filesPreupload => getAccountSession"
                                });
                                return callback(err, null);
                            }
                            self.filesApi.filesPreupload(session.getSessionId(), session.getToken(), new FilesPreuploadRequest_1.default(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                                var filePath;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "filesPreupload" })) return [3, 2];
                                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                        case 1:
                                            _a.sent();
                                            errorHandler_1.default.log({
                                                err: err, response: response,
                                                description: "API problem => filesPreupload",
                                                data: inputData
                                            });
                                            return [2, callback(err, null)];
                                        case 2:
                                            filePath = null;
                                            if (response && response.body && response.body.files) {
                                                if (response.body.files.length && response.body.files[0].file1) {
                                                    filePath = response.body.files[0].file1;
                                                }
                                            }
                                            callback(err, filePath);
                                            return [2];
                                    }
                                });
                            }); });
                        });
                        return [2];
                }
            });
        });
    };
    API.prototype.filesTempExists = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => filesTempExists => getAccountSession"
                });
                return callback(err, null);
            }
            self.filesApi.filesTempExists(session.getSessionId(), session.getToken(), new FilesTempExistsRequest_1.default(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "filesTempExists" })) return [3, 2];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 1:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => filesTempExists",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 2:
                            callback(err, response);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.getStructureNotes = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => getStructureNotes => getAccountSession"
                });
                return callback(err, null);
            }
            AccountManager_1.default.getLastUpdateTime(inputData, function (err, lastUpdateTime) {
                if (err) {
                    errorHandler_1.default.log({
                        err: err, response: lastUpdateTime,
                        description: "API problem => getStructureNotes => getLastUpdateTime"
                    });
                    return callback(err, null);
                }
                inputData.lastUpdateTime = lastUpdateTime;
                self.notesApi.getStructureNotes(session.getSessionId(), session.getToken(), new NotesGetStructureRequest_1.default(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "getStructureNotes" })) return [3, 2];
                                return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                            case 1:
                                _a.sent();
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => getStructureNotes",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            case 2:
                                callback(err, response.body);
                                return [2];
                        }
                    });
                }); });
            });
        });
    };
    API.prototype.getNotesTotalAmount = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => getNotesTotalAmount => getAccountSession"
                });
                return callback(err, null);
            }
            AccountManager_1.default.getLastUpdateTime(inputData, function (err, lastUpdateTime) {
                if (err) {
                    errorHandler_1.default.log({
                        err: err, response: lastUpdateTime,
                        description: "API problem => getNotesTotalAmount => getLastUpdateTime"
                    });
                    return callback(err, null);
                }
                inputData.lastUpdateTime = lastUpdateTime;
                self.notesApi.getNotesTotalAmount(session.getSessionId(), session.getToken(), new NotesTotalAmountRequest_1.default(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "getNotesTotalAmount" })) return [3, 2];
                                return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                            case 1:
                                _a.sent();
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => getNotesTotalAmount",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            case 2:
                                callback(err, response.body);
                                return [2];
                        }
                    });
                }); });
            });
        });
    };
    API.prototype.getRemovedItems = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => getRemovedItems => getAccountSession"
                });
                return callback(err, null);
            }
            AccountManager_1.default.getLastUpdateTime(inputData, function (err, lastUpdateTime) {
                if (err) {
                    errorHandler_1.default.log({
                        err: err, response: lastUpdateTime,
                        description: "API problem => getRemovedItems => getLastUpdateTime"
                    });
                    return callback(err, null);
                }
                inputData.lastUpdateTime = lastUpdateTime;
                self.notesApi.getRemovedItems(session.getSessionId(), session.getToken(), new GetRemovedItemsRequest_1.default(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "getRemovedItems" })) return [3, 2];
                                return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                            case 1:
                                _a.sent();
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => getRemovedItems",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            case 2:
                                callback(err, response.body);
                                return [2];
                        }
                    });
                }); });
            });
        });
    };
    API.prototype.getFullNotes = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => getFullNotes => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.getFullNotes(session.getSessionId(), session.getToken(), new NotesGetAllRequest_1.default(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                var noteGlobalId, note;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "getFullNotes" })) return [3, 2];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 1:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => getFullNotes",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 2:
                            noteGlobalId = null;
                            if (response.body.totalAmount > 0) {
                                note = response.body.notes.get(0);
                                noteGlobalId = note.global_id;
                                if (!(note.parent_id.toLowerCase() === "JESUS_CHRIST")) {
                                    return [2, callback(err, response.body)];
                                }
                            }
                            throw new NoteIsInTrashException_1.default(noteGlobalId);
                    }
                });
            }); });
        });
    };
    API.prototype.notesSearch = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => notesSearch => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.notesSearch(session.getSessionId(), session.getToken(), new NotesSearchRequest_1.default(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    if (err) {
                        errorHandler_1.default.log({
                            err: err, response: response,
                            description: "API problem => notesSearch",
                            data: inputData
                        });
                        return [2, callback(err, null)];
                    }
                    if (response.body.totalAmount === 0) {
                        response.body.global_ids = "";
                    }
                    callback(err, response.body);
                    return [2];
                });
            }); });
        });
    };
    API.prototype.getFolders = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => getFolders => getAccountSession"
                });
                return callback(err, null);
            }
            AccountManager_1.default.getLastUpdateTime(inputData, function (err, lastUpdateTime) {
                if (err) {
                    errorHandler_1.default.log({
                        err: err, response: lastUpdateTime,
                        description: "API problem => getFolders => getLastUpdateTime"
                    });
                    return callback(err, null);
                }
                inputData.lastUpdateTime = lastUpdateTime;
                self.notesApi.getFolders(session.getSessionId(), session.getToken(), new NotesGetFoldersRequest_1.default(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "getFolders" })) return [3, 2];
                                return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                            case 1:
                                _a.sent();
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => getFolders",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            case 2:
                                callback(err, response.body.notes);
                                return [2];
                        }
                    });
                }); });
            });
        });
    };
    API.prototype.getTags = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => getTags => getAccountSession"
                });
                return callback(err, null);
            }
            AccountManager_1.default.getLastUpdateTime(inputData, function (err, lastUpdateTime) {
                if (err) {
                    errorHandler_1.default.log({
                        err: err, response: lastUpdateTime,
                        description: "API problem => getTags => getLastUpdateTime"
                    });
                    return callback(err, null);
                }
                inputData.lastUpdateTime = 0;
                self.notesApi.getTags(session.getSessionId(), session.getToken(), new NotesGetTagsRequest_1.default(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "getTags" })) return [3, 2];
                                return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                            case 1:
                                _a.sent();
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => getTags",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            case 2:
                                callback(err, response.body.tags);
                                return [2];
                        }
                    });
                }); });
            });
        });
    };
    API.prototype.shareNotes = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => shareNotes => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.shareNotes(session.getSessionId(), session.getToken(), new NotesShareRequest_1.default(inputData), function (err, response) {
                if (err) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => shareNotes",
                        data: inputData
                    });
                    return callback(err, null);
                }
                callback(err, response.body);
            });
        });
    };
    API.prototype.unshareNotes = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => unshareNotes => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.unshareNotes(session.getSessionId(), session.getToken(), new NotesUnshareRequest_1.default(inputData), function (err, response) {
                if (err) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => unshareNotes",
                        data: inputData
                    });
                }
                callback(err, response);
            });
        });
    };
    API.prototype.notesBackup = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => notesBackup => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.notesBackup(session.getSessionId(), session.getToken(), new NotesGetBackupsRequest_1.default(inputData), function (err, response) {
                if (NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "notesBackup" })) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => notesBackup",
                        data: inputData
                    });
                }
                callback(err, response.body);
            });
        });
    };
    API.prototype.viewBackup = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => viewBackup => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.viewBackup(session.getSessionId(), session.getToken(), new NotesViewBackupRequest_1.default(inputData), function (err, response) {
                if (NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "viewBackup" })) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => viewBackup",
                        data: inputData
                    });
                }
                callback(err, response);
            });
        });
    };
    API.prototype.restoreBackup = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => restoreBackup => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.restoreBackup(session.getSessionId(), session.getToken(), new NotesRestoreBackupRequest_1.default(inputData), function (err, response) {
                if (NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "restoreBackup" })) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => restoreBackup",
                        data: inputData
                    });
                }
                callback(err, response);
            });
        });
    };
    API.prototype.invite = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => invite => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.invite(session.getSessionId(), session.getToken(), new NotesInviteRequest_1.default(inputData), function (err, response) {
                if (NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "invite" })) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => invite",
                        data: inputData
                    });
                }
                callback(err, response.body);
            });
        });
    };
    API.prototype.acceptInvite = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => acceptInvite => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.acceptInvite(session.getSessionId(), session.getToken(), new NotesAcceptInviteRequest_1.default(inputData), function (err, response) {
                if (NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "acceptInvite" })) {
                    errorHandler_1.default.log({
                        err: err, response: response,
                        description: "API problem => acceptInvite",
                        data: inputData
                    });
                }
                callback(err, response);
            });
        });
    };
    API.prototype.checkIfNoteExistOnServer = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => checkIfNoteExistOnServer => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.checkIfNoteExistOnServer(session.getSessionId(), session.getToken(), new NoteIsExistOnServerRequest_1.default(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                var note;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "checkIfNoteExistOnServer" })) return [3, 2];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 1:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => checkIfNoteExistOnServer",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 2:
                            if (response.body.totalAmount > 0) {
                                note = response.body.notes.get(0);
                                if (!(note.parent_id.toLowerCase() === "trash")) {
                                    return [2, callback(err, response.body)];
                                }
                            }
                            throw new NoteIsInTrashException_1.default(noteGlobalId);
                    }
                });
            }); });
        });
    };
    API.prototype.updateNotes = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => updateNotes => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.updateNotes(session.getSessionId(), session.getToken(), NotesUpdateRequest_1.default.get(inputData), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                var accountManager;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "updateNotes" })) return [3, 2];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 1:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => updateNotes",
                                data: inputData
                            });
                            return [2, callback(err, response)];
                        case 2:
                            accountManager = NimbusLibSDK.getAccountManager();
                            accountManager.setTrafficMax(response.body.usage.max);
                            accountManager.setTrafficCurrent(response.body.usage.current);
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.organizationsGet = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => organizationsGet => getAccountSession"
                });
                return callback(err, null);
            }
            self.organizationsApi.organizationsGet(session.getSessionId(), session.getToken(), new OrganizationsGetRequest_1.default(), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                var body, organizations;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => organizationsGet"
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "organizationsGet" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => organizationsGet"
                            });
                            return [2, callback(err, null)];
                        case 3:
                            body = response.body;
                            organizations = body && body.orgs ? body.orgs : [];
                            callback(err, organizations);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspacesGet = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err || !session) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspacesGet => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspacesGet(session.getSessionId(), session.getToken(), new WorkspacesGetRequest_1.default(), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                var body, workspaces;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspacesGet"
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspacesGet" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspacesGet"
                            });
                            return [2, callback(err, null)];
                        case 3:
                            body = response.body;
                            workspaces = body && body.workspaces ? body.workspaces : [];
                            callback(err, workspaces);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspaceGet = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceGet => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceGet(session.getSessionId(), session.getToken(), new WorkspaceGetRequest_1.default(globalId), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspaceGet",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspaceGet" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspaceGet",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspaceCreate = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, body = inputData.body, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceCreate => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceCreate(session.getSessionId(), session.getToken(), new WorkspaceCreateRequest_1.default(body), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspaceCreate",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspaceCreate" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspaceCreate",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspaceUpdate = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, body = inputData.body, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceUpdate => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceUpdate(session.getSessionId(), session.getToken(), new WorkspaceUpdateRequest_1.default(body), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspaceUpdate",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspaceUpdate" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspaceUpdate",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspaceDelete = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceDelete => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceDelete(session.getSessionId(), session.getToken(), new WorkspaceDeleteRequest_1.default(globalId), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspaceDelete",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspaceDelete" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspaceDelete",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspaceMembersGet = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err || !session) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceMembersGet => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceMembersGet(session.getSessionId(), session.getToken(), new WorkspaceMembersGetRequest_1.default(globalId), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    if (err || !response) {
                        return [2, callback(err, null)];
                    }
                    callback(err, response.body);
                    return [2];
                });
            }); });
        });
    };
    API.prototype.workspaceMemberAdd = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, body = inputData.body, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceMemberAdd => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceMemberAdd(session.getSessionId(), session.getToken(), new WorkspaceMemberAddRequest_1.default(body), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspaceMemberAdd",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspaceMemberAdd" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspaceMemberAdd",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspaceMemberUpdate = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, body = inputData.body, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceMemberUpdate => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceMemberUpdate(session.getSessionId(), session.getToken(), new WorkspaceMemberUpdateRequest_1.default(body), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspaceMemberUpdate",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspaceMemberUpdate" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspaceMemberUpdate",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspaceMemberDelete = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, memberId = inputData.memberId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceMemberDelete => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceMemberDelete(session.getSessionId(), session.getToken(), new WorkspaceMemberDeleteRequest_1.default(memberId), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspaceMemberDelete",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspaceMemberDelete" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspaceMemberDelete",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspaceInvitesGet = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceInvitesGet => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceInvitesGet(session.getSessionId(), session.getToken(), new WorkspaceInvitesGetRequest_1.default(globalId), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    if (err || !response) {
                        return [2, callback(err, [])];
                    }
                    callback(err, response.body);
                    return [2];
                });
            }); });
        });
    };
    API.prototype.workspaceInviteResend = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceInviteResend => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceInviteResend(session.getSessionId(), session.getToken(), new WorkspaceInviteResendRequest_1.default(globalId), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspaceInviteResend",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspaceInviteResend" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspaceInviteResend",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspaceInviteUpdate = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, body = inputData.body, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceInviteUpdate => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceInviteUpdate(session.getSessionId(), session.getToken(), new WorkspaceInviteUpdateRequest_1.default(body), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspaceInviteUpdate",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspaceInviteUpdate" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspaceInviteUpdate",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspaceInviteDelete = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceInviteDelete => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceInviteDelete(session.getSessionId(), session.getToken(), new WorkspaceInviteDeleteRequest_1.default(globalId), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspaceInviteDelete",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspaceInviteDelete" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspaceInviteDelete",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.workspaceSyncState = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, workspaces = inputData.workspaces, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => workspaceSyncState => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.workspaceSyncState(session.getSessionId(), session.getToken(), new WorkspaceSyncStateRequest_1.default(workspaces), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => workspaceSyncState",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "workspaceSyncState" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => workspaceSyncState",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.moveToWorkspace = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, body = inputData.body, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => moveToWorkspace => getAccountSession"
                });
                return callback(err, null);
            }
            self.workspacesApi.moveToWorkspace(session.getSessionId(), session.getToken(), new MoveToWorkspaceRequest_1.default(body), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => moveToWorkspace",
                                    data: inputData
                                });
                                return [2, callback(err, response.body)];
                            }
                            if (!response.body) {
                                return [2, callback(err, response.body)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "moveToWorkspace" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => moveToWorkspace",
                                data: inputData
                            });
                            return [2, callback(err, response.body)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.getNoteTextToken = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            var errSession = err;
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => getNoteTextToken => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.getNoteTextToken(session.getSessionId(), session.getToken(), new NotesTextTokenUpdateRequest_1.default({
                workspaceId: workspaceId,
                globalId: globalId
            }), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => getNoteTextToken",
                                    data: inputData
                                });
                                API.checkSignInStatus(err, errSession);
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "getNoteTextToken" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => getNoteTextToken",
                                data: inputData
                            });
                            API.checkSignInStatus(err, errSession);
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.getTrial = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => getTrial => getAccountSession"
                });
                return callback(err, null);
            }
            self.userApi.getTrial(session.getSessionId(), session.getToken(), ApiConst_1.default.getTrialUrl(), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => getTrial",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "getTrial" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => getTrial",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.notesAnnotate = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, body = inputData.body, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => updateUserInfo => getAccountSession"
                });
                return callback(err, null);
            }
            self.notesApi.notesAnnotate(session.getSessionId(), session.getToken(), new NotesAnnotateRequest_1.default(body), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => notesAnnotate",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "notesAnnotate" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => notesAnnotate",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.prototype.notesGetAnnotations = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, body = inputData.body, skipSyncHandlers = inputData.skipSyncHandlers;
        var self = this;
        NimbusLibSDK.getAccountManager().getAccountSession(function (err, session) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: session,
                    description: "API problem => updateUserInfo => notesGetAnnotations"
                });
                return callback(err, null);
            }
            self.notesApi.notesAnnotate(session.getSessionId(), session.getToken(), new NotesGetAnnotationsRequest_1.default(body), function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!skipSyncHandlers) return [3, 1];
                            if (err || !response) {
                                errorHandler_1.default.log({
                                    err: err, response: response,
                                    description: "API problem => notesGetAnnotations",
                                    data: inputData
                                });
                                return [2, callback(err, null)];
                            }
                            if (!response.body) {
                                return [2, callback(err, null)];
                            }
                            return [3, 3];
                        case 1:
                            if (!NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({ response: response, functionName: "notesGetAnnotations" })) return [3, 3];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 2:
                            _a.sent();
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "API problem => notesGetAnnotations",
                                data: inputData
                            });
                            return [2, callback(err, null)];
                        case 3:
                            callback(err, response.body);
                            return [2];
                    }
                });
            }); });
        });
    };
    API.ERROR_ACCESS_DENIED = -14;
    API.ERROR_QUOTA_EXCEED = -20;
    API.ERROR_USER_NOT_AUTHORIZED = -6;
    API.ERROR_TEMPFILE_NOT_FOUND = -23;
    API.ERROR_ATTACHMENT_NOT_FOUND = -19;
    API.DESC_DEFAULT_QUOTA_EXCEED = 'default_quota_exceed';
    API.DESC_USAGE_QUOTA_EXCEED = 'usage_quota_exceed';
    API.DESC_TOO_MANY_WORKSPACES = 'too_many_workspaces';
    API.DESC_TOO_MANY_WORKSPACE_MEMBERS = 'too_many_workspace_members';
    API.DESC_TOO_MANY_WORKSPACE_INVITES = 'too_many_workspace_invites';
    API.DESC_TOO_MANY_NOTES = 'too_many_notes';
    return API;
}());
exports.default = API;
