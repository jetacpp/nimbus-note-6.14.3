"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("util");
var API_1 = __importDefault(require("./API"));
var IUserApi_1 = __importDefault(require("./IUserApi"));
var IFilesApi_1 = __importDefault(require("./IFilesApi"));
var INotesApi_1 = __importDefault(require("./INotesApi"));
var IWorkspacesApi_1 = __importDefault(require("./IWorkspacesApi"));
var IOrganizationsApi_1 = __importDefault(require("./IOrganizationsApi"));
var api = null;
var NimbusApiProvider = (function () {
    function NimbusApiProvider() {
    }
    NimbusApiProvider.setup = function (NimbusSdkModule) {
        api = new API_1.default(new IUserApi_1.default(), new IFilesApi_1.default(), new INotesApi_1.default(), new IWorkspacesApi_1.default(), new IOrganizationsApi_1.default(), NimbusSdkModule);
    };
    NimbusApiProvider.getApi = function (NimbusSdkModule) {
        if (util_1.isNull(api)) {
            NimbusApiProvider.setup(NimbusSdkModule);
        }
        return api;
    };
    return NimbusApiProvider;
}());
exports.default = NimbusApiProvider;
