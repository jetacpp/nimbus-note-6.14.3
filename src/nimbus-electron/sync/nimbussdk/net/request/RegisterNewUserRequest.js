"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var RegisterNewUserRequest = (function (_super) {
    __extends(RegisterNewUserRequest, _super);
    function RegisterNewUserRequest(login, password, service, language) {
        var _this = _super.call(this, "register") || this;
        _this.urlParams = {
            host: ApiConst_1.default.BASE_AUTH_DATA.host,
            port: ApiConst_1.default.BASE_AUTH_DATA.port,
            path: ApiConst_1.default.ACTION_USER_REGISTER
        };
        _this.login = login;
        _this.password = password;
        _this.service = service;
        _this.languages = [language];
        return _this;
    }
    return RegisterNewUserRequest;
}(Base_Request_1.default));
exports.default = RegisterNewUserRequest;
