"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var WorkspaceInviteDeleteRequest = (function (_super) {
    __extends(WorkspaceInviteDeleteRequest, _super);
    function WorkspaceInviteDeleteRequest(id) {
        var _this = _super.call(this, ApiConst_1.default.ACTION_WORKSPACE_INVITE_DELETE) || this;
        _this.body = new Body(id);
        return _this;
    }
    return WorkspaceInviteDeleteRequest;
}(Base_Request_1.default));
exports.default = WorkspaceInviteDeleteRequest;
var Body = (function () {
    function Body(id) {
        this.id = 0;
        if (id) {
            this.id = id;
        }
    }
    return Body;
}());
