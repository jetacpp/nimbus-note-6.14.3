"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var NotesAnnotateRequest = (function (_super) {
    __extends(NotesAnnotateRequest, _super);
    function NotesAnnotateRequest(inputData) {
        var _this = _super.call(this, ApiConst_1.default.ACTION_NOTES_ANNOTATE) || this;
        _this.body = new Body(inputData);
        _this.features = {};
        return _this;
    }
    return NotesAnnotateRequest;
}(Base_Request_1.default));
exports.default = NotesAnnotateRequest;
var Body = (function () {
    function Body(inputData) {
        var attachmentGlobalId = inputData.attachmentGlobalId, tempFileName = inputData.tempFileName, role = inputData.role, trialCredits = inputData.trialCredits;
        if (typeof (attachmentGlobalId) !== 'undefined') {
            this.attachmentGlobalId = attachmentGlobalId;
        }
        if (typeof (tempFileName) !== 'undefined') {
            this.tempFileName = tempFileName;
        }
        if (typeof (role) !== 'undefined') {
            this.role = role;
        }
        if (typeof (trialCredits) !== 'undefined') {
            this.trialCredits = trialCredits;
        }
    }
    return Body;
}());
