"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var NotesSearchRequest = (function (_super) {
    __extends(NotesSearchRequest, _super);
    function NotesSearchRequest(inputData) {
        var _this = _super.call(this, ApiConst_1.default.ACTION_NOTES_SEARCH) || this;
        _this.body = new Body(inputData);
        return _this;
    }
    return NotesSearchRequest;
}(Base_Request_1.default));
exports.default = NotesSearchRequest;
var Body = (function () {
    function Body(inputData) {
        var workspaceId = inputData.workspaceId, query = inputData.query, buildExcerpts = inputData.buildExcerpts, getMatches = inputData.getMatches;
        if (workspaceId) {
            this.workspaceId = workspaceId;
        }
        if (buildExcerpts) {
            this.buildExcerpts = buildExcerpts;
        }
        if (getMatches) {
            this.getMatches = true;
        }
        this.query = query;
        this.limit = new Limit(inputData);
    }
    return Body;
}());
var Limit = (function () {
    function Limit(inputData) {
        var start = inputData.start, amount = inputData.amount;
        this.start = start || 0;
        this.amount = amount || 20;
    }
    return Limit;
}());
