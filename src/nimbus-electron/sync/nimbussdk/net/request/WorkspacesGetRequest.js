"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var WorkspacesGetRequest = (function (_super) {
    __extends(WorkspacesGetRequest, _super);
    function WorkspacesGetRequest(orgId) {
        if (orgId === void 0) { orgId = null; }
        var _this = _super.call(this, ApiConst_1.default.ACTION_WORKSPACES_GET) || this;
        _this.features = {
            "encryptRole": true,
            "businessOrgs": true,
        };
        _this.body = new Body(orgId);
        return _this;
    }
    return WorkspacesGetRequest;
}(Base_Request_1.default));
exports.default = WorkspacesGetRequest;
var Body = (function () {
    function Body(orgId) {
        if (orgId) {
            this.orgId = orgId;
        }
    }
    return Body;
}());
