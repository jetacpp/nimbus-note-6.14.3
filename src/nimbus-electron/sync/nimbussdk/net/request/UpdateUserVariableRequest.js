"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var UpdateUserVariableRequest = (function (_super) {
    __extends(UpdateUserVariableRequest, _super);
    function UpdateUserVariableRequest(inputData) {
        var _this = _super.call(this, ApiConst_1.default.ACTION_UPDATE_USER_VARIABLES) || this;
        _this.body = __assign({}, inputData);
        _this.features = {
            extendedUserInfo: true
        };
        return _this;
    }
    return UpdateUserVariableRequest;
}(Base_Request_1.default));
exports.default = UpdateUserVariableRequest;
var Body = (function () {
    function Body(inputData) {
        var key = inputData.key, value = inputData.value;
        if (typeof (key) !== 'undefined') {
            this.key = key;
        }
        if (typeof (value) !== 'undefined') {
            this.value = value;
        }
    }
    return Body;
}());
