"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var UpdateUserInfoRequest = (function (_super) {
    __extends(UpdateUserInfoRequest, _super);
    function UpdateUserInfoRequest(inputData) {
        var _this = _super.call(this, ApiConst_1.default.ACTION_UPDATE_USER_INFO) || this;
        _this.body = new Body(inputData);
        _this.features = {
            extendedUserInfo: true
        };
        return _this;
    }
    return UpdateUserInfoRequest;
}(Base_Request_1.default));
exports.default = UpdateUserInfoRequest;
var Body = (function () {
    function Body(inputData) {
        var avatar = inputData.avatar, firstname = inputData.firstname, lastname = inputData.lastname, username = inputData.username;
        if (typeof (avatar) !== 'undefined') {
            this.avatar = avatar;
        }
        if (typeof (firstname) !== 'undefined') {
            this.firstname = firstname;
        }
        if (typeof (lastname) !== 'undefined') {
            this.lastname = lastname;
        }
        if (typeof (username) !== 'undefined') {
            this.username = username;
        }
    }
    return Body;
}());
