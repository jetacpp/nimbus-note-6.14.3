"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var FilesPreuploadRequest = (function (_super) {
    __extends(FilesPreuploadRequest, _super);
    function FilesPreuploadRequest(inputData) {
        var _this = _super.call(this, ApiConst_1.default.ACTION_FILES_PREUPLOAD) || this;
        _this.body = new Body(inputData);
        return _this;
    }
    return FilesPreuploadRequest;
}(Base_Request_1.default));
exports.default = FilesPreuploadRequest;
var Body = (function () {
    function Body(inputData) {
        var workspaceId = inputData.workspaceId, tempname = inputData.tempname, extension = inputData.extension, mime = inputData.mime;
        if (typeof (workspaceId) !== 'undefined') {
            this.workspaceId = workspaceId;
        }
        this.tempname = tempname;
        this.extension = extension;
        this.mime = mime;
        this.isFormData = true;
    }
    return Body;
}());
