"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var MoveToWorkspaceRequest = (function (_super) {
    __extends(MoveToWorkspaceRequest, _super);
    function MoveToWorkspaceRequest(body) {
        var _this = _super.call(this, ApiConst_1.default.ACTION_MOVE_TO_WORKSPACE) || this;
        _this.timeout = 300000;
        _this.body = new Body(body);
        return _this;
    }
    return MoveToWorkspaceRequest;
}(Base_Request_1.default));
exports.default = MoveToWorkspaceRequest;
var Body = (function () {
    function Body(body) {
        this.globalId = '';
        this.toWorkspaceId = '';
        this.workspaceId = '';
        this.remove = true;
        if (body.globalId) {
            this.globalId = body.globalId;
        }
        if (body.workspaceId) {
            this.workspaceId = body.workspaceId;
        }
        if (body.toWorkspaceId) {
            this.toWorkspaceId = body.toWorkspaceId;
        }
        if (typeof (body.remove) !== 'undefined') {
            this.remove = body.remove;
        }
    }
    return Body;
}());
