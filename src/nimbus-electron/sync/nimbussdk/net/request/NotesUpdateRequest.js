"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var Store = (function () {
    function Store() {
        this.notes = [];
        this.tags = [];
    }
    return Store;
}());
var Remove = (function () {
    function Remove() {
        this.notes = [];
        this.attachements = [];
        this.tags = [];
    }
    return Remove;
}());
var RenameTag = (function () {
    function RenameTag(oldtag, newtag) {
        this.oldtag = oldtag;
        this.newtag = newtag;
    }
    return RenameTag;
}());
var Body = (function () {
    function Body(inputData) {
        var workspaceId = inputData.workspaceId, body = inputData.body;
        this.remove = null;
        this.store = null;
        this.renameTags = null;
        if (typeof (workspaceId) !== 'undefined') {
            this.workspaceId = workspaceId;
        }
        if (body) {
            if (body.remove)
                this.remove = body.remove;
            if (body.store)
                this.store = body.store;
            if (body.renameTags)
                this.renameTags = body.renameTags;
        }
    }
    Body.prototype.storeFolders = function (storeFolders) {
        if (storeFolders === void 0) { storeFolders = []; }
        if (storeFolders && storeFolders.length) {
            if (!this.store) {
                this.store = new Store();
            }
            if (!this.store.notes) {
                this.store.notes = [];
            }
            for (var i in storeFolders) {
                this.store.notes.push(storeFolders[i]);
            }
        }
    };
    Body.prototype.storeNotes = function (storeNotes) {
        if (storeNotes === void 0) { storeNotes = []; }
        if (storeNotes && storeNotes.length) {
            if (!this.store) {
                this.store = new Store();
            }
            if (!this.store.notes) {
                this.store.notes = [];
            }
            for (var i in storeNotes) {
                this.store.notes.push(storeNotes[i]);
            }
        }
    };
    Body.prototype.storeTags = function (storeTags) {
        if (storeTags === void 0) { storeTags = []; }
        if (storeTags && storeTags.length) {
            if (!this.store) {
                this.store = new Store();
            }
            if (!this.store.tags) {
                this.store.tags = [];
            }
            for (var i in storeTags) {
                this.store.tags.push(storeTags[i]);
            }
        }
    };
    Body.prototype.removeTags = function (removeTags) {
        if (removeTags === void 0) { removeTags = []; }
        if (removeTags && removeTags.length) {
            if (!this.remove) {
                this.remove = new Remove();
            }
            if (!this.remove.tags) {
                this.remove.tags = [];
            }
            for (var i in removeTags) {
                this.remove.tags.push(removeTags[i]);
            }
        }
    };
    Body.prototype.removeFolders = function (removeFolders) {
        if (removeFolders === void 0) { removeFolders = []; }
        if (removeFolders && removeFolders.length) {
            if (!this.remove) {
                this.remove = new Remove();
            }
            if (!this.remove.notes) {
                this.remove.notes = [];
            }
            for (var i in removeFolders) {
                this.remove.notes.push(removeFolders[i]);
            }
        }
    };
    Body.prototype.removeNotes = function (removeNotes) {
        if (removeNotes === void 0) { removeNotes = []; }
        if (removeNotes && removeNotes.length) {
            if (!this.remove) {
                this.remove = new Remove();
            }
            if (!this.remove.notes) {
                this.remove.notes = [];
            }
            for (var i in removeNotes) {
                this.remove.notes.push(removeNotes[i]);
            }
        }
    };
    Body.prototype.removeAttachements = function (removeAttachments) {
        if (removeAttachments === void 0) { removeAttachments = []; }
        if (removeAttachments && removeAttachments.length) {
            if (!this.remove) {
                this.remove = new Remove();
            }
            if (!this.remove.attachements) {
                this.remove.attachements = [];
            }
            for (var i in removeAttachments) {
                this.remove.attachements.push(removeAttachments[i]);
            }
        }
    };
    Body.prototype.setRenameTags = function (renameTags) {
        if (renameTags === void 0) { renameTags = []; }
        if (renameTags && renameTags.length) {
            if (!this.renameTags) {
                this.renameTags = [];
            }
            for (var i in renameTags) {
                this.renameTags.push(renameTags[i]);
            }
        }
    };
    Body.Store = Store;
    Body.Remove = Remove;
    Body.RenameTag = RenameTag;
    return Body;
}());
var NotesUpdateRequest = (function (_super) {
    __extends(NotesUpdateRequest, _super);
    function NotesUpdateRequest(inputData) {
        var _this = _super.call(this, ApiConst_1.default.ACTION_NOTES_UPDATE) || this;
        _this.body = new Body(inputData);
        _this.features = {
            noteEditor: true
        };
        return _this;
    }
    NotesUpdateRequest.get = function (inputData) {
        return new NotesUpdateRequest(inputData);
    };
    NotesUpdateRequest.Body = Body;
    return NotesUpdateRequest;
}(Base_Request_1.default));
exports.default = NotesUpdateRequest;
