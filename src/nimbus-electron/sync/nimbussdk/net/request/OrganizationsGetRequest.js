"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var OrganizationsGetRequest = (function (_super) {
    __extends(OrganizationsGetRequest, _super);
    function OrganizationsGetRequest() {
        var _this = _super.call(this, ApiConst_1.default.ACTION_ORGANIZATIONS_GET) || this;
        _this.features = {
            "encryptRole": true,
            "businessOrgs": true,
        };
        _this.body = {};
        return _this;
    }
    return OrganizationsGetRequest;
}(Base_Request_1.default));
exports.default = OrganizationsGetRequest;
