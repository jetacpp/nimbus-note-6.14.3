"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var NotesShareRequest = (function (_super) {
    __extends(NotesShareRequest, _super);
    function NotesShareRequest(inputData) {
        var _this = _super.call(this, ApiConst_1.default.ACTION_NOTES_SHARE) || this;
        _this.body = new Body(inputData);
        return _this;
    }
    return NotesShareRequest;
}(Base_Request_1.default));
exports.default = NotesShareRequest;
var Body = (function () {
    function Body(inputData) {
        var workspaceId = inputData.workspaceId, globalIds = inputData.globalIds, password = inputData.password, recursive = inputData.recursive;
        if (typeof (workspaceId) !== 'undefined') {
            this.workspaceId = workspaceId;
        }
        this.global_id = globalIds;
        if (typeof (password) !== 'undefined' && password !== null) {
            this.password = password;
        }
        this.recursive = recursive || false;
    }
    return Body;
}());
