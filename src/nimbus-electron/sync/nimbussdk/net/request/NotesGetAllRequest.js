"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Request_1 = __importDefault(require("../common/Base_Request"));
var NotesGetAllRequest = (function (_super) {
    __extends(NotesGetAllRequest, _super);
    function NotesGetAllRequest(inputData) {
        var _this = _super.call(this, ApiConst_1.default.ACTION_NOTES_GET) || this;
        _this.body = new Body(inputData);
        return _this;
    }
    return NotesGetAllRequest;
}(Base_Request_1.default));
exports.default = NotesGetAllRequest;
var Body = (function () {
    function Body(inputData) {
        var workspaceId = inputData.workspaceId, globalIds = inputData.globalIds;
        if (typeof (workspaceId) !== 'undefined') {
            this.workspaceId = workspaceId;
        }
        this.returnData = "all";
        this.limit = new Limit(globalIds);
        this.last_update_time = 0;
    }
    return Body;
}());
var Limit = (function () {
    function Limit(globalIds) {
        this.global_id = globalIds;
    }
    return Limit;
}());
