"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Request_1 = __importDefault(require("./common/Base_Request"));
var WorkspacesGetResponse_1 = __importDefault(require("./response/WorkspacesGetResponse"));
var WorkspaceGetResponse_1 = __importDefault(require("./response/WorkspaceGetResponse"));
var WorkspaceCreateResponse_1 = __importDefault(require("./response/WorkspaceCreateResponse"));
var WorkspaceUpdateResponse_1 = __importDefault(require("./response/WorkspaceUpdateResponse"));
var WorkspaceDeleteResponse_1 = __importDefault(require("./response/WorkspaceDeleteResponse"));
var WorkspaceMembersGetResponse_1 = __importDefault(require("./response/WorkspaceMembersGetResponse"));
var WorkspaceMemberAddResponse_1 = __importDefault(require("./response/WorkspaceMemberAddResponse"));
var WorkspaceMemberUpdateResponse_1 = __importDefault(require("./response/WorkspaceMemberUpdateResponse"));
var WorkspaceMemberDeleteResponse_1 = __importDefault(require("./response/WorkspaceMemberDeleteResponse"));
var WorkspaceInvitesGetResponse_1 = __importDefault(require("./response/WorkspaceInvitesGetResponse"));
var WorkspaceInviteResendResponse_1 = __importDefault(require("./response/WorkspaceInviteResendResponse"));
var WorkspaceInviteUpdateResponse_1 = __importDefault(require("./response/WorkspaceInviteUpdateResponse"));
var WorkspaceInviteDeleteResponse_1 = __importDefault(require("./response/WorkspaceInviteDeleteResponse"));
var WorkspaceSyncStateResponse_1 = __importDefault(require("./response/WorkspaceSyncStateResponse"));
var MoveToWorkspaceResponse_1 = __importDefault(require("./response/MoveToWorkspaceResponse"));
var IWorkspacesApi = (function () {
    function IWorkspacesApi() {
    }
    IWorkspacesApi.prototype.workspacesGet = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspacesGetResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceGet = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceGetResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceCreate = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceCreateResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceUpdate = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceUpdateResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceDelete = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceDeleteResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceMembersGet = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceMembersGetResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceMemberAdd = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceMemberAddResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceMemberUpdate = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceMemberUpdateResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceMemberDelete = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceMemberDeleteResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceInvitesGet = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceInvitesGetResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceInviteResend = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceInviteResendResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceInviteUpdate = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceInviteUpdateResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceInviteDelete = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceInviteDeleteResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.workspaceSyncState = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), WorkspaceSyncStateResponse_1.default, callback);
    };
    ;
    IWorkspacesApi.prototype.moveToWorkspace = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), MoveToWorkspaceResponse_1.default, callback);
    };
    return IWorkspacesApi;
}());
exports.default = IWorkspacesApi;
