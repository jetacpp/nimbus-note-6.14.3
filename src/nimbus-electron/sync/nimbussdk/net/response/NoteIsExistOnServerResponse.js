"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Response_1 = __importDefault(require("../common/Base_Response"));
var NoteIsExistOnServerResponse = (function (_super) {
    __extends(NoteIsExistOnServerResponse, _super);
    function NoteIsExistOnServerResponse(data) {
        var _this = _super.call(this) || this;
        _this.body = {};
        _this.errorCode = null;
        if (data.body)
            _this.body = new Body(data.body);
        if (data.errorCode != null)
            _this.errorCode = data.errorCode;
        return _this;
    }
    return NoteIsExistOnServerResponse;
}(Base_Response_1.default));
exports.default = NoteIsExistOnServerResponse;
var Body = (function () {
    function Body(data) {
        this.totalAmount = 0;
        this.notes = [];
        this.last_update_time = 0;
        this.usage = {};
        if (data.totalAmount)
            this.totalAmount = data.totalAmount;
        if (data.notes)
            this.notes = data.notes;
        if (data.last_update_time)
            this.last_update_time = data.last_update_time;
        if (data.usage)
            this.usage = new Usage(data.usage);
    }
    return Body;
}());
var Usage = (function () {
    function Usage(data) {
        this.max = 0;
        this.current = 0;
        if (data.max)
            this.max = data.max;
        if (data.current)
            this.current = data.current;
    }
    return Usage;
}());
