"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractNote_1 = __importDefault(require("./AbstractNote"));
var SyncReminderEntity_1 = __importDefault(require("./SyncReminderEntity"));
var AttachmentsCount = (function () {
    function AttachmentsCount() {
        this.in_list = 0;
    }
    return AttachmentsCount;
}());
var SyncNoteDownloadEntity = (function (_super) {
    __extends(SyncNoteDownloadEntity, _super);
    function SyncNoteDownloadEntity() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.global_id = "";
        _this.parent_id = "";
        _this.root_parent_id = "";
        _this.index = 0;
        _this.date_added_user = 0;
        _this.date_updated_user = 0;
        _this.date_added = 0;
        _this.date_updated = 0;
        _this.type = "";
        _this.title = "";
        _this.text = "";
        _this.text_version = 1;
        _this.last_change_by = "";
        _this.location_lat = 0;
        _this.location_lng = 0;
        _this.role = "";
        _this.editnote = 0;
        _this.reminder = new SyncReminderEntity_1.default();
        _this.text_short = "";
        _this.color = "";
        _this.url = "";
        _this.is_encrypted = 0;
        _this.attachments_count = new AttachmentsCount();
        _this.todo = [];
        _this.tags = [];
        _this.attachements = [];
        _this.preview = null;
        _this.shared_url = "";
        return _this;
    }
    return SyncNoteDownloadEntity;
}(AbstractNote_1.default));
exports.default = SyncNoteDownloadEntity;
