"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UserVar = (function () {
    function UserVar(data) {
        if (data === void 0) { data = null; }
        this.key = '';
        this.value = '';
        this.key = data.key || '';
        this.value = data.value || '';
    }
    return UserVar;
}());
exports.default = UserVar;
