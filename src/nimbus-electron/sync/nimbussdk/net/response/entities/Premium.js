"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Premium = (function () {
    function Premium(data) {
        if (data === void 0) { data = null; }
        this.type = '';
        this.active = false;
        this.start_date = "";
        this.end_date = "";
        this.source = "";
        this.is_business = false;
        if (data) {
            this.type = data.type || '';
            this.active = data.active || false;
            this.start_date = data.start_date || "";
            this.end_date = data.end_date || "";
            this.source = data.source || "";
            this.is_business = data.is_business || false;
        }
    }
    return Premium;
}());
exports.default = Premium;
