"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TextDetailedElement = (function () {
    function TextDetailedElement(data) {
        this.description = '';
        this.poly = [];
        if (data.description)
            this.description = data.description;
        if (data.poly)
            this.poly = data.poly;
    }
    return TextDetailedElement;
}());
exports.default = TextDetailedElement;
