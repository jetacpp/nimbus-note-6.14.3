"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SyncTodoEntity = (function () {
    function SyncTodoEntity() {
        this.global_id = "";
        this.checked = false;
        this.label = "";
        this.date = 0;
        this.event_date = 0;
        this.location_lat = 0;
        this.location_lng = 0;
    }
    return SyncTodoEntity;
}());
exports.default = SyncTodoEntity;
