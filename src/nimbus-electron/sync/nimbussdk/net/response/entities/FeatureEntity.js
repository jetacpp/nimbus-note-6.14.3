"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FeatureEntity = (function () {
    function FeatureEntity(data) {
        if (data === void 0) { data = null; }
        this.name = '';
        if (data) {
            this.name = data.name || '';
        }
    }
    return FeatureEntity;
}());
exports.default = FeatureEntity;
