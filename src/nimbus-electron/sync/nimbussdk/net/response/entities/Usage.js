"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Usage = (function () {
    function Usage(data) {
        if (data === void 0) { data = null; }
        this.notes = null;
        this.notes = new Notes((data ? data.notes : null));
    }
    return Usage;
}());
exports.default = Usage;
var Notes = (function () {
    function Notes(data) {
        if (data === void 0) { data = null; }
        this.max = 0;
        this.current = 0;
        if (data) {
            this.max = data.max || 0;
            this.current = data.current || 0;
        }
    }
    return Notes;
}());
