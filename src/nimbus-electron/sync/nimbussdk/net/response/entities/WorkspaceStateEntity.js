"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WorkspaceStateEntity = (function () {
    function WorkspaceStateEntity() {
        this.globalId = '';
        this.lastUpdateTime = 0;
    }
    return WorkspaceStateEntity;
}());
exports.default = WorkspaceStateEntity;
