"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractNote_1 = __importDefault(require("./AbstractNote"));
var SyncFullNoteUpdateEntity = (function (_super) {
    __extends(SyncFullNoteUpdateEntity, _super);
    function SyncFullNoteUpdateEntity() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.global_id = "";
        _this.parent_id = "";
        _this.root_parent_id = "";
        _this.index = 0;
        _this.date_added_user = 0;
        _this.date_updated_user = 0;
        _this.type = "";
        _this.title = "";
        _this.text = "";
        _this.last_change_by = "";
        _this.location_lat = 0;
        _this.location_lng = 0;
        _this.shared = 0;
        _this.role = "";
        _this.editnote = 0;
        _this.reminder = {};
        _this.text_short = "";
        _this.color = "";
        _this.url = "";
        _this.is_encrypted = 0;
        _this.todo = [];
        _this.tags = [];
        _this.attachements = [];
        return _this;
    }
    SyncFullNoteUpdateEntity.prototype.setReminder = function (reminder) {
        this.reminder = reminder !== null ? reminder : false;
    };
    SyncFullNoteUpdateEntity.prototype.setTodo = function (todo) {
        this.todo = todo;
    };
    SyncFullNoteUpdateEntity.prototype.setTags = function (tags) {
        this.tags = tags;
    };
    SyncFullNoteUpdateEntity.prototype.setAttachments = function (attachments) {
        if (attachments !== null && attachments.length) {
            if (!this.attachements) {
                this.attachements = [];
            }
            for (var i in attachments) {
                if (attachments.hasOwnProperty(i)) {
                    this.attachements.push(attachments[i]);
                }
            }
        }
    };
    return SyncFullNoteUpdateEntity;
}(AbstractNote_1.default));
exports.default = SyncFullNoteUpdateEntity;
