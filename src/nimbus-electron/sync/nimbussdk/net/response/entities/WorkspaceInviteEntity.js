"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WorkspaceInviteEntity = (function () {
    function WorkspaceInviteEntity() {
        this.id = 0;
        this.workspaceId = '';
        this.role = '';
        this.encryptRole = '';
        this.addedByUserId = '';
        this.email = '';
        this.createdAt = '';
        this.updatedAt = '';
        this.used = false;
    }
    return WorkspaceInviteEntity;
}());
exports.default = WorkspaceInviteEntity;
