"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Limits = (function () {
    function Limits(data) {
        if (data === void 0) { data = null; }
        this.NOTES_MAX_SIZE = 0;
        this.NOTES_MONTH_USAGE_QUOTA = 0;
        this.NOTES_MAX_ATTACHMENT_SIZE = 0;
        if (data) {
            this.NOTES_MAX_SIZE = data.NOTES_MAX_SIZE || 0;
            this.NOTES_MONTH_USAGE_QUOTA = data.NOTES_MONTH_USAGE_QUOTA || 0;
            this.NOTES_MAX_ATTACHMENT_SIZE = data.NOTES_MAX_ATTACHMENT_SIZE || 0;
        }
    }
    return Limits;
}());
exports.default = Limits;
