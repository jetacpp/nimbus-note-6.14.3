"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SyncWorkspaceEntity = (function () {
    function SyncWorkspaceEntity() {
        this.globalId = "";
        this.title = '';
        this.org = null;
        this.userId = 0;
        this.createdAt = 0;
        this.updatedAt = 0;
        this.isDefault = false;
        this.countMembers = 0;
        this.countInvites = 0;
        this.access = null;
        this.defaultEncryptionKeyId = null;
        this.isNotesLimited = false;
    }
    return SyncWorkspaceEntity;
}());
exports.default = SyncWorkspaceEntity;
