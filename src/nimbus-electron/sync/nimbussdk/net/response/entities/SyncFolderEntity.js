"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractNote_1 = __importDefault(require("./AbstractNote"));
var SyncFolderEntity = (function (_super) {
    __extends(SyncFolderEntity, _super);
    function SyncFolderEntity() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.global_id = "";
        _this.parent_id = "";
        _this.root_parent_id = "";
        _this.index = 0;
        _this.date_added = 0;
        _this.date_added_user = 0;
        _this.date_updated_user = 0;
        _this.type = "";
        _this.title = "";
        _this.shared = 0;
        _this.color = "";
        return _this;
    }
    SyncFolderEntity.prototype.setTitle = function (title) {
        if (title != null) {
            this.title = title;
        }
    };
    SyncFolderEntity.prototype.toString = function () {
        return 'SyncFolderEntity{' + 'date_added=' + this.date_added + '}';
    };
    return SyncFolderEntity;
}(AbstractNote_1.default));
exports.default = SyncFolderEntity;
