"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrgUserEntity = (function () {
    function OrgUserEntity(data) {
        if (data === void 0) { data = null; }
        this.id = 0;
        this.email = null;
        this.username = '';
        this.firstname = null;
        this.lastname = null;
        if (data) {
            this.id = data.id || 0;
            this.email = data.email ? data.email.toLowerCase() : '';
            this.username = data.username || '';
            this.firstname = data.firstname || null;
            this.lastname = data.lastname || null;
        }
    }
    return OrgUserEntity;
}());
exports.default = OrgUserEntity;
