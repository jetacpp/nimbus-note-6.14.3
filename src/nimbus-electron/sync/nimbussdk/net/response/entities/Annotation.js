"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Annotation = (function () {
    function Annotation(data) {
        this.noteGlobalId = '';
        this.attachmentGlobalId = '';
        this.type = '';
        this.locale = '';
        this.text = '';
        this.textDetailed = [];
        this.label = '';
        this.extractions = [];
        if (data.noteGlobalId)
            this.noteGlobalId = data.noteGlobalId;
        if (data.attachmentGlobalId)
            this.attachmentGlobalId = data.attachmentGlobalId;
        if (data.type)
            this.type = data.type;
        if (data.textDetailed)
            this.textDetailed = data.textDetailed;
        if (data.label)
            this.label = data.label;
        if (data.extractions) {
            this.extractions = data.extractions;
        }
    }
    return Annotation;
}());
exports.default = Annotation;
