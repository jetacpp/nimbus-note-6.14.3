"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Challenge = (function () {
    function Challenge(data) {
        if (data === void 0) { data = null; }
        this.type = "";
        this.state = "";
        this.image = "";
        if (data) {
            this.type = data.type || "";
            this.state = data.state || "";
            this.image = data.image || "";
        }
    }
    Challenge.TYPE_CAPTCHA = 'captcha';
    Challenge.TYPE_OTP = 'otp';
    return Challenge;
}());
exports.default = Challenge;
