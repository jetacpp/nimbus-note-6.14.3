"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Extractions = (function () {
    function Extractions(data) {
        this.type = '';
        this.entities = [];
        if (data.type)
            this.type = data.type;
        if (data.entities)
            this.entities = data.entities;
    }
    return Extractions;
}());
exports.default = Extractions;
