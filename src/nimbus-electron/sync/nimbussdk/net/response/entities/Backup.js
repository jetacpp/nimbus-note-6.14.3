"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Backup = (function () {
    function Backup(data) {
        if (data === void 0) { data = null; }
        this.global_id = "";
        this.date = 0;
        this.count_notes = 0;
        this.count_folders = 0;
        if (data) {
            this.global_id = data.global_id || "";
            this.date = data.date || 0;
            this.count_notes = data.count_notes || 0;
            this.count_folders = data.count_folders || 0;
        }
    }
    return Backup;
}());
exports.default = Backup;
