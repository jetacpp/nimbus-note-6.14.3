"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Premium_1 = __importDefault(require("./Premium"));
var Usage_1 = __importDefault(require("./Usage"));
var Limits_1 = __importDefault(require("./Limits"));
var Info = (function () {
    function Info(data) {
        if (data === void 0) { data = null; }
        this.user_id = 0;
        this.premium = new Premium_1.default();
        this.register_date = "";
        this.days_to_quota_reset = 0;
        this.usage = new Usage_1.default();
        this.limits = new Limits_1.default();
        if (data) {
            this.user_id = data.user_id || 0;
            this.premium = data.premium || new Premium_1.default();
            this.register_date = data.register_date || "";
            this.days_to_quota_reset = data.days_to_quota_reset || 0;
            this.usage = data.usage || new Usage_1.default();
            this.limits = data.limits || new Limits_1.default();
        }
    }
    return Info;
}());
exports.default = Info;
