"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrgUsageEntity = (function () {
    function OrgUsageEntity(data) {
        if (data === void 0) { data = null; }
        this.current = 0;
        this.max = 0;
        this.daysToReset = 0;
        if (data) {
            this.current = data.current || 0;
            this.max = data.max || 0;
            this.daysToReset = data.daysToReset || 0;
        }
    }
    return OrgUsageEntity;
}());
exports.default = OrgUsageEntity;
