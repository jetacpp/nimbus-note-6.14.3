"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractReminder_1 = __importDefault(require("./AbstractReminder"));
var SyncReminderEntity = (function (_super) {
    __extends(SyncReminderEntity, _super);
    function SyncReminderEntity() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = "";
        _this.date = 0;
        _this.lat = 0;
        _this.lng = 0;
        _this.phone = "";
        _this.priority = 0;
        _this.interval = 0;
        _this.remind_until = 0;
        return _this;
    }
    return SyncReminderEntity;
}(AbstractReminder_1.default));
exports.default = SyncReminderEntity;
