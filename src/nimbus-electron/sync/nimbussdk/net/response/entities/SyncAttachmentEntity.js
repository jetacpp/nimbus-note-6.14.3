"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractAttachment_1 = __importDefault(require("./AbstractAttachment"));
var SyncAttachmentEntity = (function (_super) {
    __extends(SyncAttachmentEntity, _super);
    function SyncAttachmentEntity() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.global_id = "";
        _this.date_added = 0;
        _this.date_updated = 0;
        _this.location = "";
        _this.type = "";
        _this.role = "attachment";
        _this.type_extra = "";
        _this.size = 0;
        _this.in_list = 0;
        _this.is_encrypted = 0;
        _this.display_name = "";
        _this.mime = "";
        _this.tempname = "";
        _this.file_uuid = "";
        _this.extension = "";
        return _this;
    }
    SyncAttachmentEntity.getLocalPath = function () {
        return "";
    };
    ;
    SyncAttachmentEntity.getLocalPathWithFile = function () {
        return "";
    };
    ;
    return SyncAttachmentEntity;
}(AbstractAttachment_1.default));
exports.default = SyncAttachmentEntity;
