"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var OrgUsageEntity_1 = __importDefault(require("./OrgUsageEntity"));
var OrgLimitsEntity_1 = __importDefault(require("./OrgLimitsEntity"));
var OrgUserEntity_1 = __importDefault(require("./OrgUserEntity"));
var OrgEntity = (function () {
    function OrgEntity(data) {
        if (data === void 0) { data = null; }
        this.id = '';
        this.type = '';
        this.title = '';
        this.description = '';
        this.usage = new OrgUsageEntity_1.default();
        this.limits = new OrgLimitsEntity_1.default();
        this.features = [];
        this.user = new OrgUserEntity_1.default();
        this.sub = null;
        this.domain = null;
        this.suspended = false;
        this.suspendedAt = false;
        this.suspendedReason = null;
        this.access = null;
        if (data) {
            this.id = data.id || '';
            this.type = data.type || '';
            this.title = data.title || '';
            this.description = data.description || '';
            this.usage = data.usage || new OrgUsageEntity_1.default();
            this.limits = data.limits || new OrgLimitsEntity_1.default();
            this.features = data.features || [];
            this.user = data.user || new OrgUserEntity_1.default();
            this.sub = data.sub || null;
            this.suspended = data.suspended || false;
            this.suspendedAt = data.suspendedAt || 0;
            this.suspendedReason = data.suspendedReason || null;
            this.access = data.access || null;
        }
    }
    return OrgEntity;
}());
exports.default = OrgEntity;
