"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EntityValue = (function () {
    function EntityValue(data) {
        this.text = '';
        if (data.text)
            this.text = data.text;
    }
    return EntityValue;
}());
exports.default = EntityValue;
