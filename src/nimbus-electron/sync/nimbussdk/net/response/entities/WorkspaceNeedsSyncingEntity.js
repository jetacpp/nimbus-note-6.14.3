"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WorkspaceNeedsSyncingEntity = (function () {
    function WorkspaceNeedsSyncingEntity() {
        this.state = null;
        this.reason = '';
    }
    return WorkspaceNeedsSyncingEntity;
}());
exports.default = WorkspaceNeedsSyncingEntity;
