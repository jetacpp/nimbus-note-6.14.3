"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EntityGroup = (function () {
    function EntityGroup(data) {
        this.class = '';
        this.values = '';
        if (data.class)
            this.class = data.class;
        if (data.values)
            this.values = data.values;
    }
    return EntityGroup;
}());
exports.default = EntityGroup;
