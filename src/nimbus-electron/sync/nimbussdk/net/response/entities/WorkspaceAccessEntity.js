"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WorkspaceAccessEntity = (function () {
    function WorkspaceAccessEntity() {
        this.role = '';
        this.privileges = [];
        this.member = [];
    }
    return WorkspaceAccessEntity;
}());
exports.default = WorkspaceAccessEntity;
