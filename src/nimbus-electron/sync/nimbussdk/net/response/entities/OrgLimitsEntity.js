"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrgLimitsEntity = (function () {
    function OrgLimitsEntity(data) {
        if (data === void 0) { data = null; }
        this.traffic = 0;
        this.workspaces = 0;
        this.attachmentSize = 0;
        this.encryptionKeysPerWorkspace = 0;
        this.membersPerWorkspace = 0;
        if (data) {
            this.traffic = data.traffic || 0;
            this.workspaces = data.workspaces || 0;
            this.attachmentSize = data.attachmentSize || 0;
            this.encryptionKeysPerWorkspace = data.encryptionKeysPerWorkspace || 0;
            this.membersPerWorkspace = data.membersPerWorkspace || 0;
        }
    }
    return OrgLimitsEntity;
}());
exports.default = OrgLimitsEntity;
