"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WorkspaceMemberEntity = (function () {
    function WorkspaceMemberEntity() {
        this.globalId = '';
        this.addedByUserId = 0;
        this.user = 0;
        this.createdAt = 0;
        this.updatedAt = 0;
        this.workspaceId = '';
        this.type = 'full';
        this.role = '';
        this.encryptRole = '';
        this.orgId = '';
    }
    return WorkspaceMemberEntity;
}());
exports.default = WorkspaceMemberEntity;
