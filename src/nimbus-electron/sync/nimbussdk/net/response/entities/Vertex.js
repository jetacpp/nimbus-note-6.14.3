"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Vertex = (function () {
    function Vertex(data) {
        this.x = 0;
        this.y = 0;
        if (data.x)
            this.x = data.x;
        if (data.y)
            this.y = data.y;
    }
    return Vertex;
}());
exports.default = Vertex;
