"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ViewBackup = (function () {
    function ViewBackup() {
        this.global_id = "";
        this.title = "";
        this.parent_id = "";
        this.date_added_user = 0;
        this.date_updated_user = 0;
        this.type = "";
    }
    return ViewBackup;
}());
exports.default = ViewBackup;
