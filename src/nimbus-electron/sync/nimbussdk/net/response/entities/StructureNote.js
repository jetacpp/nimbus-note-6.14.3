"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var SyncReminderEntity_1 = __importDefault(require("./SyncReminderEntity"));
var StructureNote = (function () {
    function StructureNote() {
        this.global_id = "";
        this.parent_id = "";
        this.root_parent_id = "";
        this.date_added = 0;
        this.date_updated = 0;
        this.date_added_user = 0;
        this.date_updated_user = 0;
        this.type = "";
        this.role = "";
        this.title = "";
        this.url = "";
        this.location_lat = 0;
        this.location_lng = 0;
        this.shared = 0;
        this.favorite = 0;
        this.editnote = 0;
        this.is_encrypted = 0;
        this.is_completed = 0;
        this.color = "";
        this.todo_count = new TodoCount();
        this.index = 0;
        this.text_short = "";
        this.tags = [];
        this.reminder = new SyncReminderEntity_1.default();
        this.attachments_count = new AttachmentsCount();
    }
    StructureNote.prototype.setAttachmentsInListCount = function (noteAttachmentsInListCount) {
        this.attachments_count = new AttachmentsCount(noteAttachmentsInListCount);
    };
    ;
    StructureNote.prototype.setText = function (text) {
        if (text != null) {
            this.text_short = text;
        }
    };
    ;
    StructureNote.prototype.setReminderLabel = function (label) {
        if (this.reminder && label != null) {
            this.reminder.label = label;
        }
    };
    ;
    StructureNote.prototype.setTodoCount = function (activeTodoCount) {
        if (this.todo_count) {
            this.todo_count.unchecked = activeTodoCount;
        }
    };
    ;
    StructureNote.prototype.setLocation = function (locationLat, locationLng) {
        if (locationLat && locationLng) {
            this.location_lat = locationLat;
            this.location_lng = locationLng;
        }
    };
    ;
    StructureNote.prototype.toString = function () {
        return "StructureNote{" +
            "title='" + this.title + '\'' +
            ", parent_id='" + this.parent_id + '\'' +
            ", type='" + this.type + '\'' +
            '}';
    };
    ;
    return StructureNote;
}());
exports.default = StructureNote;
var AttachmentsCount = (function () {
    function AttachmentsCount(noteAttachmentsInListCount) {
        if (noteAttachmentsInListCount === void 0) { noteAttachmentsInListCount = null; }
        this.in_list = 0;
        this.in_list = noteAttachmentsInListCount;
    }
    return AttachmentsCount;
}());
var TodoCount = (function () {
    function TodoCount(data) {
        if (data === void 0) { data = null; }
        this.checked = 0;
        this.unchecked = 0;
        if (data) {
            this.checked = data.checked || 0;
            this.unchecked = data.unchecked || 0;
        }
    }
    return TodoCount;
}());
