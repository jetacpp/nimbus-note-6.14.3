"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Response_1 = __importDefault(require("../common/Base_Response"));
var NotesRestoreBackupResponse = (function (_super) {
    __extends(NotesRestoreBackupResponse, _super);
    function NotesRestoreBackupResponse(data) {
        var _this = _super.call(this) || this;
        _this.errorCode = null;
        _this.body = data.body;
        if (data.errorCode != null)
            _this.errorCode = data.errorCode;
        return _this;
    }
    return NotesRestoreBackupResponse;
}(Base_Response_1.default));
exports.default = NotesRestoreBackupResponse;
