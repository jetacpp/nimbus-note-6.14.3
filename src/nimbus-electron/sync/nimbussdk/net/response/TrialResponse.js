"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Response_1 = __importDefault(require("../common/Base_Response"));
var TrialResponse = (function (_super) {
    __extends(TrialResponse, _super);
    function TrialResponse(data) {
        var _this = _super.call(this) || this;
        _this.body = {};
        _this.errorCode = 0;
        if (data) {
            _this.body = new Body(data);
            if (data.errorCode != null) {
                _this.errorCode = data.errorCode;
            }
        }
        return _this;
    }
    return TrialResponse;
}(Base_Response_1.default));
exports.default = TrialResponse;
var Body = (function () {
    function Body(data) {
        this.state = null;
        this.dateStart = 0;
        this.dateUntil = 0;
        this.daysRemain = 0;
        this.requestorService = null;
        this.requestorApp = null;
        if (data.state)
            this.state = data.state;
        if (data.dateStart)
            this.dateStart = data.dateStart;
        if (data.dateUntil)
            this.dateUntil = data.dateUntil;
        if (data.daysRemain)
            this.daysRemain = data.daysRemain;
        if (data.requestorService)
            this.requestorService = data.requestorService;
        if (data.requestorApp)
            this.requestorApp = data.requestorApp;
    }
    return Body;
}());
