"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Response_1 = __importDefault(require("../common/Base_Response"));
var Premium_1 = __importDefault(require("./entities/Premium"));
var Usage_1 = __importDefault(require("./entities/Usage"));
var Limits_1 = __importDefault(require("./entities/Limits"));
var UpdateUserInfoResponse = (function (_super) {
    __extends(UpdateUserInfoResponse, _super);
    function UpdateUserInfoResponse(data) {
        var _this = _super.call(this) || this;
        _this.body = {};
        _this.errorCode = null;
        if (data.body)
            _this.body = new Body(data.body);
        if (data.errorCode != null)
            _this.errorCode = data.errorCode;
        return _this;
    }
    return UpdateUserInfoResponse;
}(Base_Response_1.default));
exports.default = UpdateUserInfoResponse;
var Body = (function () {
    function Body(data) {
        this.user_id = 0;
        this.premium = {};
        this.register_date = "";
        this.days_to_quota_reset = 0;
        this.usage = {};
        this.limits = {};
        this.login = "";
        this.firstname = "";
        this.lastname = "";
        this.avatar = null;
        if (data.user_id)
            this.user_id = data.user_id;
        if (data.premium)
            this.premium = new Premium_1.default(data.premium);
        if (data.register_date)
            this.register_date = data.register_date;
        if (data.days_to_quota_reset)
            this.days_to_quota_reset = data.days_to_quota_reset;
        if (data.usage)
            this.usage = new Usage_1.default(data.usage);
        if (data.limits)
            this.limits = new Limits_1.default(data.limits);
        if (data.login)
            this.login = data.login;
        if (data.firstname)
            this.firstname = data.firstname;
        if (data.lastname)
            this.lastname = data.lastname;
        if (data.avatar)
            this.avatar = data.avatar;
        if (data.languages)
            this.languages = data.languages;
        if (data.notes_predefined_folders)
            this.notes_predefined_folders = data.notes_predefined_folders;
    }
    return Body;
}());
