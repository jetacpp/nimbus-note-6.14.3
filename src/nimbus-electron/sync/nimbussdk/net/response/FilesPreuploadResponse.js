"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Response_1 = __importDefault(require("../common/Base_Response"));
var FilesPreuploadResponse = (function (_super) {
    __extends(FilesPreuploadResponse, _super);
    function FilesPreuploadResponse(data) {
        var _this = _super.call(this) || this;
        _this.body = {};
        _this.errorCode = null;
        if (data.body)
            _this.body = new Body(data.body);
        if (data.errorCode !== null)
            _this.errorCode = data.errorCode;
        return _this;
    }
    return FilesPreuploadResponse;
}(Base_Response_1.default));
exports.default = FilesPreuploadResponse;
var Body = (function () {
    function Body(data) {
        this.files = [];
        if (data.files) {
            for (var i in data.files) {
                if (data.files.hasOwnProperty(i)) {
                    this.files.push(new File({ file1: data.files[i] }));
                }
            }
        }
    }
    return Body;
}());
var File = (function () {
    function File(data) {
        this.file1 = "";
        if (data.file1) {
            this.file1 = data.file1;
        }
    }
    return File;
}());
