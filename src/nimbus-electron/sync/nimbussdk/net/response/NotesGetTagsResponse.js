"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Response_1 = __importDefault(require("../common/Base_Response"));
var Usage_1 = __importDefault(require("./entities/Usage"));
var NotesGetTagsResponse = (function (_super) {
    __extends(NotesGetTagsResponse, _super);
    function NotesGetTagsResponse(data) {
        var _this = _super.call(this) || this;
        _this.body = {};
        _this.errorCode = null;
        if (data.body)
            _this.body = new Body(data.body);
        if (data.errorCode != null)
            _this.errorCode = data.errorCode;
        return _this;
    }
    return NotesGetTagsResponse;
}(Base_Response_1.default));
exports.default = NotesGetTagsResponse;
var Body = (function () {
    function Body(data) {
        this.tags = [];
        this.last_update_time = 0;
        this.usage = {};
        if (data.tags)
            this.tags = data.tags;
        if (data.last_update_time)
            this.last_update_time = data.last_update_time;
        if (data.usage)
            this.usage = new Usage_1.default(data.usage);
    }
    return Body;
}());
