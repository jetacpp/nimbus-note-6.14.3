"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Response_1 = __importDefault(require("../common/Base_Response"));
var NotesUpdateResponse = (function (_super) {
    __extends(NotesUpdateResponse, _super);
    function NotesUpdateResponse(data) {
        var _this = _super.call(this) || this;
        _this.body = {};
        _this.errorCode = null;
        _this.failedItem = null;
        if (data.body) {
            _this.body = new Body(data.body);
        }
        if (data.errorCode != null) {
            _this.errorCode = data.errorCode;
        }
        if (data.failedItem) {
            _this.failedItem = data.failedItem;
        }
        return _this;
    }
    return NotesUpdateResponse;
}(Base_Response_1.default));
exports.default = NotesUpdateResponse;
var Body = (function () {
    function Body(data) {
        this.usage = {};
        this.last_update_time = 0;
        this.date_updated = {};
        this.savedAttachments = {};
        this._errorDesc = null;
        if (data.usage)
            this.usage = new Usage(data.usage);
        if (data.last_update_time)
            this.last_update_time = data.last_update_time;
        if (data.date_updated)
            this.date_updated = data.date_updated;
        if (data.savedAttachments)
            this.savedAttachments = data.savedAttachments;
        if (data._errorDesc != null)
            this._errorDesc = data._errorDesc;
    }
    return Body;
}());
var Usage = (function () {
    function Usage(data) {
        this.max = 0;
        this.current = 0;
        if (data.max)
            this.max = data.max;
        if (data.current)
            this.current = data.current;
    }
    return Usage;
}());
