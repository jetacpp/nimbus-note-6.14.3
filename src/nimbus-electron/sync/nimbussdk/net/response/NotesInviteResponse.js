"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Response_1 = __importDefault(require("../common/Base_Response"));
var NotesInviteResponse = (function (_super) {
    __extends(NotesInviteResponse, _super);
    function NotesInviteResponse(data) {
        var _this = _super.call(this) || this;
        _this.body = {};
        _this.errorCode = null;
        if (data.body)
            _this.body = new Body(data.body);
        if (data.errorCode != null)
            _this.errorCode = data.errorCode;
        return _this;
    }
    return NotesInviteResponse;
}(Base_Response_1.default));
exports.default = NotesInviteResponse;
var Body = (function () {
    function Body(data) {
        this.invite_id = 0;
        this.secret_key = "";
        if (data.invite_id)
            this.invite_id = data.invite_id;
        if (data.secret_key)
            this.secret_key = data.secret_key;
    }
    return Body;
}());
