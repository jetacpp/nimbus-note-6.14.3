"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Request_1 = __importDefault(require("./common/Base_Request"));
var SignInResponse_1 = __importDefault(require("./response/SignInResponse"));
var SignUpResponse_1 = __importDefault(require("./response/SignUpResponse"));
var UserInfoResponse_1 = __importDefault(require("./response/UserInfoResponse"));
var UpdateUserInfoResponse_1 = __importDefault(require("./response/UpdateUserInfoResponse"));
var UserVariablesResponse_1 = __importDefault(require("./response/UserVariablesResponse"));
var UpdateUserVariableResponse_1 = __importDefault(require("./response/UpdateUserVariableResponse"));
var ChallengeResponse_1 = __importDefault(require("./response/ChallengeResponse"));
var UserChangePasswordResponse_1 = __importDefault(require("./response/UserChangePasswordResponse"));
var RemindPasswordResponse_1 = __importDefault(require("./response/RemindPasswordResponse"));
var LogoutResponse_1 = __importDefault(require("./response/LogoutResponse"));
var UserUnlockPremiumResponse_1 = __importDefault(require("./response/UserUnlockPremiumResponse"));
var TrialResponse_1 = __importDefault(require("./response/TrialResponse"));
var IUserApi = (function () {
    function IUserApi() {
    }
    IUserApi.prototype.signIn = function (req, callback) {
        Base_Request_1.default.make(req, SignInResponse_1.default, callback);
    };
    ;
    IUserApi.prototype.signUp = function (req, callback) {
        Base_Request_1.default.make(req, SignUpResponse_1.default, callback);
    };
    ;
    IUserApi.prototype.userInfo = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), UserInfoResponse_1.default, callback);
    };
    ;
    IUserApi.prototype.updateUserInfo = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), UpdateUserInfoResponse_1.default, callback);
    };
    ;
    IUserApi.prototype.userVariables = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), UserVariablesResponse_1.default, callback);
    };
    ;
    IUserApi.prototype.updateUserVariable = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), UpdateUserVariableResponse_1.default, callback);
    };
    ;
    IUserApi.prototype.challenge = function (req, callback) {
        Base_Request_1.default.make(req, ChallengeResponse_1.default, callback);
    };
    ;
    IUserApi.prototype.userChangePassword = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), UserChangePasswordResponse_1.default, callback);
    };
    ;
    IUserApi.prototype.remindPassword = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), RemindPasswordResponse_1.default, callback);
    };
    ;
    IUserApi.prototype.logout = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), LogoutResponse_1.default, callback);
    };
    ;
    IUserApi.prototype.unlockPremium = function (sessionId, token, url, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, { url: url }), UserUnlockPremiumResponse_1.default, callback);
    };
    ;
    IUserApi.prototype.getTrial = function (sessionId, token, url, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, { trialUrl: url }), TrialResponse_1.default, callback);
    };
    ;
    return IUserApi;
}());
exports.default = IUserApi;
