"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Request_1 = __importDefault(require("./common/Base_Request"));
var FilesPreuploadResponse_1 = __importDefault(require("./response/FilesPreuploadResponse"));
var FilesTempExistsResponse_1 = __importDefault(require("./response/FilesTempExistsResponse"));
var ProductRecipeUploadResponse_1 = __importDefault(require("./response/ProductRecipeUploadResponse"));
var IFilesApi = (function () {
    function IFilesApi() {
    }
    IFilesApi.prototype.filesPreupload = function (sessionId, token, request, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, request), FilesPreuploadResponse_1.default, callback);
    };
    ;
    IFilesApi.prototype.filesTempExists = function (sessionId, token, request, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, request), FilesTempExistsResponse_1.default, callback);
    };
    ;
    IFilesApi.prototype.productRecipeUpload = function (sessionId, token, request, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, request), ProductRecipeUploadResponse_1.default, callback);
    };
    ;
    return IFilesApi;
}());
exports.default = IFilesApi;
