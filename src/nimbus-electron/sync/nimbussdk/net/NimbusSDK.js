"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("util");
var AccountManagerProvider_1 = __importDefault(require("../manager/AccountManagerProvider"));
var NimbusApiProvider_1 = __importDefault(require("./NimbusApiProvider"));
var accountManager = null;
var api = null;
var NimbusSDK = (function () {
    function NimbusSDK() {
    }
    NimbusSDK.setup = function () {
        NimbusApiProvider_1.default.setup(NimbusSDK);
    };
    NimbusSDK.getApi = function () {
        if (util_1.isNull(api)) {
            api = NimbusApiProvider_1.default.getApi(NimbusSDK);
        }
        return api;
    };
    NimbusSDK.getAccountManager = function () {
        if (util_1.isNull(accountManager)) {
            accountManager = AccountManagerProvider_1.default.get();
        }
        return accountManager;
    };
    return NimbusSDK;
}());
exports.default = NimbusSDK;
