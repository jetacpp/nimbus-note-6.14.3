"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = __importDefault(require("util"));
var config_1 = __importDefault(require("../../../../config"));
var BASE_DOMAIN = getBaseDomainFromJNI();
var BASE_URL = getBaseUrlFromJNI();
var BASE_AUTH_DATA = getBaseAuthDataFromJNI();
var BASE_BILLING_URL = getBillingUrlFromJNI();
var BASE_TRIAL_URL = getTrialUrlFromJNI();
var _CLIENT_SOFTWARE = getClientSoftwareFromJNI();
var SERVICE = getServiceNameFromJNI();
var ACTION_USER_AUTH = getActionUserAuthFromJNI();
var ACTION_USER_REGISTER = getActionUserRegisterFromJNI();
var ACTION_USER_INFO = getActionUserInfoFromJNI();
var ACTION_UPDATE_USER_INFO = getActionUpdateUserInfoFromJNI();
var ACTION_USER_VARIABLES = getActionUserVariablesFromJNI();
var ACTION_UPDATE_USER_VARIABLES = getActionUpdateUserVariablesFromJNI();
var ACTION_USER_GET_CHALLENGE = getActionUserGetChallengeFromJNI();
var ACTION_USER_CHANGE_PASSWORD = getActionUserChangePasswordFromJNI();
var ACTION_USER_REMIND_PASSWORD = getActionUserRemindPasswordFromJNI();
var ACTION_USER_LOGOUT = getActionUserLogoutFromJNI();
var ACTION_NOTES_GET = getActionNotesGetFromJNI();
var ACTION_NOTES_UPDATE = getActionNotesUpdateFromJNI();
var ACTION_NOTES_SEARCH = getActionNotesSearchFromJNI();
var ACTION_NOTES_GET_FOLDERS = getActionNotesGetFoldersFromJNI();
var ACTION_NOTES_GET_TAGS = getActionNotesGetTagsFromJNI();
var ACTION_NOTES_SHARE = getActionNotesShareFromJNI();
var ACTION_NOTES_UNSHARE = getActionNotesUnshareFromJNI();
var ACTION_NOTES_ACCOUNT = getActionNotesAccountFromJNI();
var ACTION_NOTES_GET_BACKUPS = getActionNotesGetBackupsFromJNI();
var ACTION_NOTES_VIEW_BACKUP = getActionNotesViewBackupFromJNI();
var ACTION_NOTES_RESTORE_BACKUP = getActionNotesRestoreBackupFromJNI();
var ACTION_NOTES_INVITE = getActionNotesInviteFromJNI();
var ACTION_NOTES_ACCEPT_INVITE = getActionNotesAcceptInviteFromJNI();
var ACTION_NOTES_ANNOTATE = getActionNotesAnnotateFromJNI();
var ACTION_NOTES_GET_ANNOTATIONS = getActionNotesGetAnnotationsFromJNI();
var ACTION_ORGANIZATIONS_GET = getActionOrganizationsGetFromJNI();
var ACTION_WORKSPACES_GET = getActionWorkspacesGetFromJNI();
var ACTION_WORKSPACE_GET = getActionWorkspaceGetFromJNI();
var ACTION_WORKSPACE_CREATE = getActionWorkspaceCreateFromJNI();
var ACTION_WORKSPACE_UPDATE = getActionWorkspaceUpdateFromJNI();
var ACTION_WORKSPACE_DELETE = getActionWorkspaceDeleteFromJNI();
var ACTION_WORKSPACE_MEMBERS_GET = getActionWorkspaceMembersGetFromJNI();
var ACTION_WORKSPACE_MEMBER_ADD = getActionWorkspaceMemberAddFromJNI();
var ACTION_WORKSPACE_MEMBER_UPDATE = getActionWorkspaceMemberUpdateFromJNI();
var ACTION_WORKSPACE_MEMBER_DELETE = getActionWorkspaceMemberDeleteFromJNI();
var ACTION_WORKSPACE_INVITES_GET = getActionWorkspaceInvitesGetFromJNI();
var ACTION_WORKSPACE_INVITE_RESEND = getActionWorkspaceInviteResendFromJNI();
var ACTION_WORKSPACE_INVITE_UPDATE = getActionWorkspaceInviteUpdateFromJNI();
var ACTION_WORKSPACE_INVITE_DELETE = getActionWorkspaceInviteDeleteFromJNI();
var ACTION_WORKSPACES_SYNC_STATE = getActionWorkspacesSyncStateFromJNI();
var ACTION_MOVE_TO_WORKSPACE = getActionMoveToWorkspaceFromJNI();
var ACTION_GET_NOTES_TEXT_TOKEN_UPDATE_REQUEST = getActionNotesTextTokenUpdateFromJNI();
var ACTION_FILES_PREUPLOAD = "files:preupload";
var ACTION_FILES_TEMP_EXISTS = getActionFilesTempExistsFromJNI();
var ACTION_PRODUCT_RECIPE_UPLOAD = getActionProductRecipeFromJNI();
function getBillingUrl(inputData) {
    var productId = inputData.productId, token = inputData.token;
    return util_1.default.format(BASE_BILLING_URL, productId, token);
}
function getTrialUrl() {
    return util_1.default.format(BASE_TRIAL_URL);
}
function getBaseDomainFromJNI() {
    return config_1.default.syncApiServiceDomain;
}
function getBaseUrlFromJNI() {
    return "https://" + config_1.default.syncApiServiceDomain;
}
function getBaseAuthDataFromJNI() {
    return {
        host: config_1.default.authApiServiceDomain,
        port: config_1.default.authApiServicePort
    };
}
function getBillingUrlFromJNI() {
    return "https://everhelper.me/billing/wh/gplay.php?subscriptionId=%s&token=%s";
}
function getTrialUrlFromJNI() {
    return config_1.default.trialApiService + "/trial";
}
function getClientSoftwareFromJNI() {
    return process.platform !== 'darwin' ? "win_notes" : "mac_notes";
}
function getServiceNameFromJNI() {
    return "nimbus";
}
function getActionUserAuthFromJNI() {
    return config_1.default.API_AUTH_LOGIN_PATH;
}
function getActionUserRegisterFromJNI() {
    return config_1.default.API_AUTH_REGISTER_PATH;
}
function getActionUserInfoFromJNI() {
    return "user:info";
}
function getActionUpdateUserInfoFromJNI() {
    return "user:update";
}
function getActionUserVariablesFromJNI() {
    return "user:getVars";
}
function getActionUpdateUserVariablesFromJNI() {
    return "user:setVar";
}
function getActionUserGetChallengeFromJNI() {
    return config_1.default.API_AUTH_CHALLENGE_PATH;
}
function getActionUserRemindPasswordFromJNI() {
    return config_1.default.API_AUTH_REMIND_PATH;
}
function getActionUserChangePasswordFromJNI() {
    return "user_change_password";
}
function getActionUserLogoutFromJNI() {
    return "user:logout";
}
function getActionNotesGetFromJNI() {
    return "notes:get";
}
function getActionNotesUpdateFromJNI() {
    return "notes:update";
}
function getActionNotesSearchFromJNI() {
    return "notes:search";
}
function getActionNotesGetFoldersFromJNI() {
    return "notes:getFolders";
}
function getActionNotesGetTagsFromJNI() {
    return "notes:getTags";
}
function getActionNotesShareFromJNI() {
    return "notes:share";
}
function getActionNotesUnshareFromJNI() {
    return "notes:unshare";
}
function getActionNotesAccountFromJNI() {
    return "notes:account";
}
function getActionNotesGetBackupsFromJNI() {
    return "notes:getBackups";
}
function getActionNotesViewBackupFromJNI() {
    return "notes:viewBackup";
}
function getActionNotesRestoreBackupFromJNI() {
    return "notes:restoreBackup";
}
function getActionNotesInviteFromJNI() {
    return "notes:invite";
}
function getActionNotesAcceptInviteFromJNI() {
    return "notes:acceptInvite";
}
function getActionNotesAnnotateFromJNI() {
    return "notes:annotate";
}
function getActionNotesGetAnnotationsFromJNI() {
    return "notes:getAnnotations";
}
function getActionFilesTempExistsFromJNI() {
    return "files:tempExists";
}
function getActionProductRecipeFromJNI() {
    return "https://everhelper.me/billing/wh/appstore.php";
}
function getActionOrganizationsGetFromJNI() {
    return "orgs:getAll";
}
function getActionWorkspacesGetFromJNI() {
    return "notes:getWorkspaces";
}
function getActionWorkspaceGetFromJNI() {
    return "notes:getWorkspace";
}
function getActionWorkspaceCreateFromJNI() {
    return "notes:createWorkspace";
}
function getActionWorkspaceUpdateFromJNI() {
    return "notes:updateWorkspace";
}
function getActionWorkspaceDeleteFromJNI() {
    return "notes:deleteWorkspace";
}
function getActionWorkspaceMembersGetFromJNI() {
    return "notes:getWorkspaceMembers";
}
function getActionWorkspaceMemberAddFromJNI() {
    return "notes:addWorkspaceMember";
}
function getActionWorkspaceMemberUpdateFromJNI() {
    return "notes:updateWorkspaceMember";
}
function getActionWorkspaceMemberDeleteFromJNI() {
    return "notes:deleteWorkspaceMember";
}
function getActionWorkspaceInvitesGetFromJNI() {
    return "notes:getWorkspaceInvites";
}
function getActionWorkspaceInviteResendFromJNI() {
    return "notes:resendWorkspaceInvite";
}
function getActionWorkspaceInviteUpdateFromJNI() {
    return "notes:updateWorkspaceInvite";
}
function getActionWorkspaceInviteDeleteFromJNI() {
    return "notes:deleteWorkspaceInvite";
}
function getActionWorkspacesSyncStateFromJNI() {
    return "notes:syncState";
}
function getActionMoveToWorkspaceFromJNI() {
    return "notes:moveToWorkspace";
}
function getActionNotesTextTokenUpdateFromJNI() {
    return "notes:issueTextToken";
}
exports.default = {
    BASE_DOMAIN: BASE_DOMAIN,
    BASE_URL: BASE_URL,
    BASE_AUTH_DATA: BASE_AUTH_DATA,
    BASE_BILLING_URL: BASE_BILLING_URL,
    _CLIENT_SOFTWARE: _CLIENT_SOFTWARE,
    SERVICE: SERVICE,
    ACTION_USER_AUTH: ACTION_USER_AUTH,
    ACTION_USER_REGISTER: ACTION_USER_REGISTER,
    ACTION_USER_INFO: ACTION_USER_INFO,
    ACTION_UPDATE_USER_INFO: ACTION_UPDATE_USER_INFO,
    ACTION_USER_VARIABLES: ACTION_USER_VARIABLES,
    ACTION_UPDATE_USER_VARIABLES: ACTION_UPDATE_USER_VARIABLES,
    ACTION_USER_GET_CHALLENGE: ACTION_USER_GET_CHALLENGE,
    ACTION_USER_CHANGE_PASSWORD: ACTION_USER_CHANGE_PASSWORD,
    ACTION_USER_REMIND_PASSWORD: ACTION_USER_REMIND_PASSWORD,
    ACTION_USER_LOGOUT: ACTION_USER_LOGOUT,
    ACTION_NOTES_GET: ACTION_NOTES_GET,
    ACTION_NOTES_UPDATE: ACTION_NOTES_UPDATE,
    ACTION_NOTES_SEARCH: ACTION_NOTES_SEARCH,
    ACTION_NOTES_GET_FOLDERS: ACTION_NOTES_GET_FOLDERS,
    ACTION_NOTES_GET_TAGS: ACTION_NOTES_GET_TAGS,
    ACTION_NOTES_SHARE: ACTION_NOTES_SHARE,
    ACTION_NOTES_UNSHARE: ACTION_NOTES_UNSHARE,
    ACTION_NOTES_ACCOUNT: ACTION_NOTES_ACCOUNT,
    ACTION_NOTES_GET_BACKUPS: ACTION_NOTES_GET_BACKUPS,
    ACTION_NOTES_VIEW_BACKUP: ACTION_NOTES_VIEW_BACKUP,
    ACTION_NOTES_RESTORE_BACKUP: ACTION_NOTES_RESTORE_BACKUP,
    ACTION_NOTES_INVITE: ACTION_NOTES_INVITE,
    ACTION_NOTES_ACCEPT_INVITE: ACTION_NOTES_ACCEPT_INVITE,
    ACTION_NOTES_ANNOTATE: ACTION_NOTES_ANNOTATE,
    ACTION_NOTES_GET_ANNOTATIONS: ACTION_NOTES_GET_ANNOTATIONS,
    ACTION_FILES_PREUPLOAD: ACTION_FILES_PREUPLOAD,
    ACTION_FILES_TEMP_EXISTS: ACTION_FILES_TEMP_EXISTS,
    ACTION_PRODUCT_RECIPE_UPLOAD: ACTION_PRODUCT_RECIPE_UPLOAD,
    ACTION_ORGANIZATIONS_GET: ACTION_ORGANIZATIONS_GET,
    ACTION_WORKSPACES_GET: ACTION_WORKSPACES_GET,
    ACTION_WORKSPACE_GET: ACTION_WORKSPACE_GET,
    ACTION_WORKSPACE_CREATE: ACTION_WORKSPACE_CREATE,
    ACTION_WORKSPACE_UPDATE: ACTION_WORKSPACE_UPDATE,
    ACTION_WORKSPACE_DELETE: ACTION_WORKSPACE_DELETE,
    ACTION_WORKSPACE_MEMBERS_GET: ACTION_WORKSPACE_MEMBERS_GET,
    ACTION_WORKSPACE_MEMBER_ADD: ACTION_WORKSPACE_MEMBER_ADD,
    ACTION_WORKSPACE_MEMBER_UPDATE: ACTION_WORKSPACE_MEMBER_UPDATE,
    ACTION_WORKSPACE_MEMBER_DELETE: ACTION_WORKSPACE_MEMBER_DELETE,
    ACTION_WORKSPACE_INVITES_GET: ACTION_WORKSPACE_INVITES_GET,
    ACTION_WORKSPACE_INVITE_RESEND: ACTION_WORKSPACE_INVITE_RESEND,
    ACTION_WORKSPACE_INVITE_UPDATE: ACTION_WORKSPACE_INVITE_UPDATE,
    ACTION_WORKSPACE_INVITE_DELETE: ACTION_WORKSPACE_INVITE_DELETE,
    ACTION_WORKSPACES_SYNC_STATE: ACTION_WORKSPACES_SYNC_STATE,
    ACTION_MOVE_TO_WORKSPACE: ACTION_MOVE_TO_WORKSPACE,
    ACTION_GET_NOTES_TEXT_TOKEN_UPDATE_REQUEST: ACTION_GET_NOTES_TEXT_TOKEN_UPDATE_REQUEST,
    getBillingUrl: getBillingUrl,
    getTrialUrl: getTrialUrl,
};
