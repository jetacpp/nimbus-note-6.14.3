"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_extra_1 = __importDefault(require("fs-extra"));
var http_1 = __importDefault(require("http"));
var https_1 = __importDefault(require("https"));
var form_data_1 = __importDefault(require("form-data"));
var config_1 = __importDefault(require("../../../../../config"));
var cjs_ponyfill_1 = require("abortcontroller-polyfill/dist/cjs-ponyfill");
var ConnectionCollector_1 = __importDefault(require("../../../process/connection/ConnectionCollector"));
var ApiConst_1 = __importDefault(require("../ApiConst"));
var Base_Response_1 = __importDefault(require("./Base_Response"));
var SelectedWorkspace_1 = __importDefault(require("../../../../workspace/SelectedWorkspace"));
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var fetch = cjs_ponyfill_1.abortableFetch(require('node-fetch')).fetch;
var method = 'POST';
var jsonMimeType = 'application/json';
var BaseRequest = (function () {
    function BaseRequest(action) {
        this.action = action;
        this._client_software = ApiConst_1.default._CLIENT_SOFTWARE;
    }
    BaseRequest.make = function (requestParams, responseClass, callback) {
        if (requestParams === void 0) { requestParams = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var params, paramsData, headers, paramsDataBody, _a, selectedWorkspaceId, workspaceId, requestUniqueId, form, filedName, formData, formFileHeaders, options, formAction, formHeaders, i, controller_1, signal, formParams, options, formAction, formHeaders, i, formParams, formData, recipePath, recipeContent, options, formAction, formHeaders, i, formParams, options, prop, timeout, httpProvider, req_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        params = BaseRequest.getBodyJson(requestParams);
                        paramsData = BaseRequest.getBody(requestParams);
                        headers = BaseRequest.getHeaders(requestParams);
                        paramsDataBody = {};
                        if (paramsData) {
                            paramsDataBody = paramsData;
                            if (paramsData.body) {
                                paramsDataBody = paramsData.body;
                            }
                        }
                        if (!paramsDataBody) return [3, 5];
                        if (!paramsDataBody.workspaceId) return [3, 2];
                        _a = paramsDataBody;
                        return [4, workspace_1.default.getLocalId(paramsDataBody.workspaceId)];
                    case 1:
                        _a.workspaceId = _b.sent();
                        return [3, 5];
                    case 2:
                        if (!(typeof (paramsDataBody.workspaceId) === 'undefined')) return [3, 4];
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 3:
                        selectedWorkspaceId = _b.sent();
                        if (selectedWorkspaceId) {
                            paramsDataBody.workspaceId = selectedWorkspaceId;
                        }
                        return [3, 5];
                    case 4:
                        delete paramsDataBody.workspaceId;
                        _b.label = 5;
                    case 5:
                        workspaceId = paramsDataBody.workspaceId ? paramsDataBody.workspaceId : null;
                        requestUniqueId = new Date().getTime() + ":api:" + paramsData.action;
                        if (paramsDataBody && paramsDataBody.isFormData) {
                            form = new form_data_1.default();
                            for (filedName in headers) {
                                if (headers.hasOwnProperty(filedName)) {
                                    form.append(filedName, headers[filedName]);
                                }
                            }
                            if (paramsDataBody.tempname) {
                                formData = new form_data_1.default();
                                formFileHeaders = {};
                                if (typeof (paramsDataBody.mime) !== "undefined" && paramsDataBody.mime) {
                                    formFileHeaders['contentType'] = paramsDataBody.mime;
                                }
                                else if (typeof (paramsDataBody.extension) !== "undefined" && paramsDataBody.extension) {
                                    formFileHeaders['filename'] = paramsDataBody.tempname + '.' + paramsDataBody.extension;
                                }
                                try {
                                    formData.append("file1", fs_extra_1.default.createReadStream(paramsDataBody.tempname), formFileHeaders);
                                }
                                catch (e) {
                                    if (config_1.default.SHOW_WEB_CONSOLE) {
                                        console.log("Problem to read file for upload", e);
                                    }
                                }
                                options = BaseRequest.getMultipartFormOptions(headers);
                                formAction = options.protocol + options.host + options.path;
                                formHeaders = formData.getHeaders();
                                for (i in options.headers) {
                                    if (options.headers.hasOwnProperty(i)) {
                                        formHeaders[i] = options.headers[i];
                                    }
                                }
                                controller_1 = new cjs_ponyfill_1.AbortController();
                                signal = controller_1.signal;
                                controller_1.uniqueId = requestUniqueId;
                                ConnectionCollector_1.default.add({
                                    workspaceId: workspaceId,
                                    uniqueId: requestUniqueId,
                                    xhrConnection: controller_1
                                });
                                formParams = {
                                    body: formData,
                                    headers: formHeaders,
                                    method: options.method,
                                    signal: signal
                                };
                                fetch(formAction, formParams)
                                    .then(function (res) {
                                    return res.json();
                                }).then(function (json) {
                                    ConnectionCollector_1.default.clearById({
                                        workspaceId: workspaceId,
                                        id: controller_1.uniqueId
                                    });
                                    var response = new responseClass(json);
                                    callback(response.getErrorCode(), response);
                                }).catch(function (err) {
                                    if (err.name === 'AbortError') {
                                        callback(BaseRequest.ERROR_REQUEST_ABORT_ERROR, {
                                            errorCode: BaseRequest.ERROR_REQUEST_ABORT_ERROR,
                                            errDesc: 'Abort request to sync server'
                                        });
                                    }
                                    else {
                                        callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
                                            errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
                                            errDesc: 'Fail to make request to sync server',
                                            err: err
                                        });
                                    }
                                });
                            }
                        }
                        else if (paramsDataBody && paramsDataBody.trialUrl) {
                            if (!paramsDataBody.trialUrl) {
                                return [2, callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
                                        errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
                                        errDesc: 'Fail to make request to trial server',
                                        err: new Error("trialUrl is not defined")
                                    })];
                            }
                            options = BaseRequest.getRecipeFormOptions(headers);
                            formAction = paramsDataBody.trialUrl;
                            formHeaders = headers;
                            for (i in options.headers) {
                                if (options.headers.hasOwnProperty(i)) {
                                    formHeaders[i] = options.headers[i];
                                }
                            }
                            formParams = {
                                headers: formHeaders,
                                method: 'GET'
                            };
                            fetch(formAction, formParams)
                                .then(function (res) {
                                return res.json();
                            }).then(function (json) {
                                var response = new responseClass(json);
                                callback(response.getErrorCode(), response);
                            }).catch(function (err) {
                                if (err.name === 'AbortError') {
                                    callback(BaseRequest.ERROR_REQUEST_ABORT_ERROR, {
                                        errorCode: BaseRequest.ERROR_REQUEST_ABORT_ERROR,
                                        errDesc: 'Abort request to trialUrl server'
                                    });
                                }
                                else {
                                    callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
                                        errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
                                        errDesc: 'Fail to make request to trialUrl server',
                                        err: err
                                    });
                                }
                            });
                        }
                        else if (paramsDataBody && paramsDataBody.recipeUrl) {
                            if (!paramsDataBody.recipeUrl) {
                                return [2, callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
                                        errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
                                        errDesc: 'Fail to make request to sync server',
                                        err: new Error("recipeUrl is not defined")
                                    })];
                            }
                            formData = new form_data_1.default();
                            recipePath = paramsDataBody.recipeUrl.replace("file://", "");
                            recipeContent = fs_extra_1.default.readFileSync(recipePath);
                            if (!recipeContent) {
                                return [2, callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, { errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR })];
                            }
                            formData.append("receipt", recipeContent, "receipt.iap");
                            options = BaseRequest.getRecipeFormOptions(headers);
                            formAction = options.url;
                            formHeaders = formData.getHeaders();
                            for (i in options.headers) {
                                if (options.headers.hasOwnProperty(i)) {
                                    formHeaders[i] = options.headers[i];
                                }
                            }
                            formParams = {
                                body: formData,
                                headers: formHeaders,
                                method: options.method
                            };
                            fetch(formAction, formParams)
                                .then(function (res) {
                                return res.json();
                            }).then(function (json) {
                                var response = new responseClass(json);
                                callback(response.getErrorCode(), response);
                            }).catch(function (err) {
                                if (err.name === 'AbortError') {
                                    callback(BaseRequest.ERROR_REQUEST_ABORT_ERROR, {
                                        errorCode: BaseRequest.ERROR_REQUEST_ABORT_ERROR,
                                        errDesc: 'Abort request to purchase validation server'
                                    });
                                }
                                else {
                                    callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
                                        errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
                                        errDesc: 'Fail to make request to purchase validation server',
                                        err: err
                                    });
                                }
                            });
                        }
                        else {
                            headers['Content-Length'] = Buffer.byteLength(params, 'utf-8');
                            options = BaseRequest.getOptions(headers);
                            if (paramsData.urlParams) {
                                for (prop in paramsData.urlParams) {
                                    if (paramsData.urlParams.hasOwnProperty(prop)) {
                                        options[prop] = paramsData.urlParams[prop];
                                    }
                                }
                            }
                            else {
                                options.host = ApiConst_1.default.BASE_DOMAIN;
                            }
                            timeout = paramsData.timeout;
                            if (timeout) {
                                options.timeout = timeout;
                            }
                            httpProvider = options.port && options.port === 443 ? https_1.default : http_1.default;
                            req_1 = httpProvider.request(options, function (res) {
                                var chunks = [];
                                res.on('data', function (chunk) {
                                    chunks.push(chunk);
                                });
                                res.on('end', function () {
                                    ConnectionCollector_1.default.clearById({
                                        workspaceId: workspaceId,
                                        id: req_1.uniqueId
                                    });
                                    Base_Response_1.default.get(chunks, res.headers['content-encoding'], responseClass, callback);
                                });
                            });
                            if (timeout) {
                                req_1.on('timeout', function () {
                                    req_1.abort();
                                });
                            }
                            req_1.on('error', function (err) {
                                callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
                                    errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
                                    errDesc: 'Fail to make request to sync server',
                                    err: err
                                });
                            });
                            req_1.on('abort', function () {
                                callback(BaseRequest.ERROR_REQUEST_ABORT_ERROR, {
                                    errorCode: BaseRequest.ERROR_REQUEST_ABORT_ERROR,
                                    errDesc: 'Abort request to sync server'
                                });
                            });
                            req_1.write(params);
                            req_1.uniqueId = requestUniqueId;
                            ConnectionCollector_1.default.add({
                                workspaceId: workspaceId,
                                uniqueId: requestUniqueId,
                                xhrConnection: req_1
                            });
                            req_1.end();
                        }
                        return [2];
                }
            });
        });
    };
    BaseRequest.prepareRequest = function (sessionId, token, req) {
        if (!req.headers) {
            req.headers = {};
        }
        req.headers["EverHelper-Session-ID"] = sessionId;
        return req;
    };
    BaseRequest.getOptions = function (clientHeaders) {
        if (clientHeaders === void 0) { clientHeaders = {}; }
        var options = {
            method: method,
            headers: {
                'Content-Type': jsonMimeType + '; charset=utf-8',
                'Accept-Encoding': 'gzip',
                'x-api-version': 3,
                'x-client-version': config_1.default.APP_VERSION,
                'x-client-software': ApiConst_1.default._CLIENT_SOFTWARE,
            }
        };
        for (var i in clientHeaders) {
            if (clientHeaders.hasOwnProperty(i)) {
                options.headers[i] = clientHeaders[i];
            }
        }
        return options;
    };
    BaseRequest.getMultipartFormOptions = function (clientHeaders) {
        if (clientHeaders === void 0) { clientHeaders = {}; }
        var options = {
            host: ApiConst_1.default.BASE_DOMAIN,
            path: '/' + ApiConst_1.default.ACTION_FILES_PREUPLOAD,
            protocol: 'http://',
            method: method,
            headers: {
                'x-allow-upload-errors': 1,
                'x-client-software': ApiConst_1.default._CLIENT_SOFTWARE,
                'x-client-version': config_1.default.APP_VERSION
            }
        };
        for (var i in clientHeaders) {
            if (clientHeaders.hasOwnProperty(i)) {
                options.headers[i] = clientHeaders[i];
            }
        }
        return options;
    };
    BaseRequest.getRecipeFormOptions = function (clientHeaders) {
        if (clientHeaders === void 0) { clientHeaders = {}; }
        var options = {
            url: ApiConst_1.default.ACTION_PRODUCT_RECIPE_UPLOAD,
            method: method,
            headers: {
                'x-allow-upload-errors': 1,
                'x-client-software': ApiConst_1.default._CLIENT_SOFTWARE,
                'x-client-version': config_1.default.APP_VERSION
            }
        };
        for (var i in clientHeaders) {
            if (clientHeaders.hasOwnProperty(i)) {
                options.headers[i] = clientHeaders[i];
            }
        }
        return options;
    };
    BaseRequest.getBody = function (requestParams) {
        if (requestParams === void 0) { requestParams = {}; }
        var params = {};
        for (var paramName in requestParams) {
            if (requestParams.hasOwnProperty(paramName) && paramName !== "headers") {
                params[paramName] = requestParams[paramName];
            }
        }
        return params;
    };
    BaseRequest.getBodyJson = function (requestParams) {
        requestParams = requestParams || {};
        return JSON.stringify(BaseRequest.getBody(requestParams));
    };
    BaseRequest.getHeaders = function (requestParams) {
        requestParams = requestParams || {};
        return typeof (requestParams.headers) === "undefined" ? {} : requestParams.headers;
    };
    BaseRequest.ERROR_INTERNAL_SERVER_ERROR = -10;
    BaseRequest.ERROR_REQUEST_ABORT_ERROR = -500;
    return BaseRequest;
}());
exports.default = BaseRequest;
