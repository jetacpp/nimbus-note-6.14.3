"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var zlib_1 = __importDefault(require("zlib"));
var BaseResponse = (function () {
    function BaseResponse() {
        this.errorCode = null;
    }
    BaseResponse.prototype.getErrorCode = function () {
        return this.errorCode;
    };
    BaseResponse.prototype.toJson = function () {
        return JSON.stringify(this);
    };
    BaseResponse.getDecodedJson = function (decoded) {
        try {
            var responseString = decoded.toString();
            var parsedString = JSON.parse(responseString);
            return parsedString ? parsedString : '';
        }
        catch (err) {
            return '';
        }
    };
    BaseResponse.get = function (chunks, encoding, responseClass, callback) {
        var buffer = Buffer.concat(chunks);
        if (encoding === 'gzip') {
            zlib_1.default.gunzip(buffer, function (err, decoded) {
                var response = new responseClass(BaseResponse.getDecodedJson(decoded));
                callback(response.getErrorCode(), response);
            });
        }
        else if (encoding === 'deflate') {
            zlib_1.default.inflate(buffer, function (err, decoded) {
                var response = new responseClass(BaseResponse.getDecodedJson(decoded));
                callback(response.getErrorCode(), response);
            });
        }
        else {
            var response = new responseClass(BaseResponse.getDecodedJson(buffer));
            callback(response.getErrorCode(), response);
        }
    };
    return BaseResponse;
}());
exports.default = BaseResponse;
