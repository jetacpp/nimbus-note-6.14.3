"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Request_1 = __importDefault(require("./common/Base_Request"));
var OrganizationsGetResponse_1 = __importDefault(require("./response/OrganizationsGetResponse"));
var IOrganizationsApi = (function () {
    function IOrganizationsApi() {
    }
    IOrganizationsApi.prototype.organizationsGet = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), OrganizationsGetResponse_1.default, callback);
    };
    ;
    return IOrganizationsApi;
}());
exports.default = IOrganizationsApi;
