"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Exception = (function () {
    function Exception(message) {
        this.message = "Exception: " + message;
    }
    Exception.prototype.getMessage = function () {
        return this.message;
    };
    return Exception;
}());
exports.default = Exception;
