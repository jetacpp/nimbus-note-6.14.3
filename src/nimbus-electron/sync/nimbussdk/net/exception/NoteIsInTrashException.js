"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var RuntimeException_1 = __importDefault(require("./RuntimeException"));
var NoteIsInTrashException = (function (_super) {
    __extends(NoteIsInTrashException, _super);
    function NoteIsInTrashException(globalId) {
        var _this = _super.call(this, "NoteIsInTrashException") || this;
        _this.globalId = globalId;
        return _this;
    }
    NoteIsInTrashException.prototype.getGlobalId = function () {
        return this.globalId;
    };
    return NoteIsInTrashException;
}(RuntimeException_1.default));
exports.default = NoteIsInTrashException;
