"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../../../config"));
var errorHandler_1 = __importDefault(require("../../../../../utilities/errorHandler"));
var ErrorCodes_1 = __importDefault(require("./ErrorCodes"));
var NimbusError_1 = __importDefault(require("./NimbusError"));
var NimbusException_1 = __importDefault(require("./NimbusException"));
var NimbusErrorHandler = (function () {
    function NimbusErrorHandler() {
    }
    NimbusErrorHandler.throwNimbusApiErrorIfExist = function (inputData) {
        var response = inputData.response, functionName = inputData.functionName;
        functionName = functionName || '';
        var errorCode = response ? response.errorCode : ErrorCodes_1.default.UNKNOWN_ERROR;
        if (errorCode !== ErrorCodes_1.default.OK) {
            if (config_1.default.SHOW_WEB_CONSOLE) {
                console.log("throwNimbusApiErrorIfExist", response);
            }
            var errMessage = '';
            if (!response) {
                errMessage = "Api " + functionName + " has empty response";
                errorHandler_1.default.log(errMessage);
                return true;
            }
            else if (response.errorCode === null) {
                try {
                    errMessage = "Api " + functionName + " has error code null and response: " + JSON.stringify(response);
                }
                catch (err) {
                    errMessage = "Api " + functionName + " response error code is null";
                }
                errorHandler_1.default.log(errMessage);
                return true;
            }
            else {
                try {
                    errMessage = "Api " + functionName + " has bad error code and response: " + JSON.stringify(response);
                }
                catch (err) {
                    errMessage = "Api " + functionName + " response error body is not an object";
                }
                errorHandler_1.default.log(errMessage);
            }
        }
        return response.errorCode;
    };
    NimbusErrorHandler.throwExceptionByErrorCode = function (errorCode) {
        throw new NimbusException_1.default(NimbusErrorHandler.getExceptionEnumByErrorCode(errorCode));
    };
    NimbusErrorHandler.getExceptionEnumByErrorCode = function (errorCode) {
        var nimbusError = NimbusError_1.default.UNKNOWN_ERROR;
        switch (errorCode) {
            case ErrorCodes_1.default.NOT_WELL_FORMED: {
                nimbusError = NimbusError_1.default.NOT_WELL_FORMED;
                break;
            }
            case ErrorCodes_1.default.ACTION_PARAM_IS_MISSED: {
                nimbusError = NimbusError_1.default.ACTION_PARAM_IS_MISSED;
                break;
            }
            case ErrorCodes_1.default.UNRECOGNIZED_ACTION: {
                nimbusError = NimbusError_1.default.UNRECOGNIZED_ACTION;
                break;
            }
            case ErrorCodes_1.default.USER_ALREADY_EXISTS: {
                nimbusError = NimbusError_1.default.USER_ALREADY_EXISTS;
                break;
            }
            case ErrorCodes_1.default.STORAGE_ENGINE_RETURNS_ERROR: {
                nimbusError = NimbusError_1.default.STORAGE_ENGINE_RETURNS_ERROR;
                break;
            }
            case ErrorCodes_1.default.AUTH_FAILED: {
                nimbusError = NimbusError_1.default.AUTH_FAILED;
                break;
            }
            case ErrorCodes_1.default.USER_NOT_EXISTS: {
                nimbusError = NimbusError_1.default.USER_NOT_EXISTS;
                break;
            }
            case ErrorCodes_1.default.INTERNAL_DATA_JSON_MAILFORMED: {
                nimbusError = NimbusError_1.default.INTERNAL_DATA_JSON_MAILFORMED;
                break;
            }
            case ErrorCodes_1.default.WRONG_ARGUMENTS_COUNT: {
                nimbusError = NimbusError_1.default.WRONG_ARGUMENTS_COUNT;
                break;
            }
            case ErrorCodes_1.default.INTERNAL_SERVER_ERROR: {
                nimbusError = NimbusError_1.default.INTERNAL_SERVER_ERROR;
                break;
            }
            case ErrorCodes_1.default.INTERNAL_FILE_SYSTEM_ERROR: {
                nimbusError = NimbusError_1.default.INTERNAL_FILE_SYSTEM_ERROR;
                break;
            }
            case ErrorCodes_1.default.INTERNAL_SENDMAIL_ERROR: {
                nimbusError = NimbusError_1.default.INTERNAL_SENDMAIL_ERROR;
                break;
            }
            case ErrorCodes_1.default.TOO_MUCH_REQUESTS: {
                nimbusError = NimbusError_1.default.TOO_MUCH_REQUESTS;
                break;
            }
            case ErrorCodes_1.default.ACCESS_DENIED: {
                nimbusError = NimbusError_1.default.ACCESS_DENIED;
                break;
            }
            case ErrorCodes_1.default.MAX_EMAILS_LIMIT_REACHED: {
                nimbusError = NimbusError_1.default.MAX_EMAILS_LIMIT_REACHED;
                break;
            }
            case ErrorCodes_1.default.DATA_TOO_LARGE: {
                nimbusError = NimbusError_1.default.DATA_TOO_LARGE;
                break;
            }
            case ErrorCodes_1.default.ALREADY_LOCKED: {
                nimbusError = NimbusError_1.default.ALREADY_LOCKED;
                break;
            }
            case ErrorCodes_1.default.EXTERNAL_DATA_MALFORMED: {
                nimbusError = NimbusError_1.default.EXTERNAL_DATA_MALFORMED;
                break;
            }
            case ErrorCodes_1.default.NOT_FOUND: {
                nimbusError = NimbusError_1.default.NOT_FOUND;
                break;
            }
            case ErrorCodes_1.default.COUNT_ITEMS_QUOTA_EXCEED: {
                nimbusError = NimbusError_1.default.COUNT_ITEMS_QUOTA_EXCEED;
                break;
            }
            case ErrorCodes_1.default.ITEM_ALREADY_EXISTS: {
                nimbusError = NimbusError_1.default.ITEM_ALREADY_EXISTS;
                break;
            }
            case ErrorCodes_1.default.TEMPFILE_NOT_FOUND: {
                nimbusError = NimbusError_1.default.TEMPFILE_NOT_FOUND;
                break;
            }
            case ErrorCodes_1.default.WRONG_CAPTCHA: {
                nimbusError = NimbusError_1.default.WRONG_CAPTCHA;
                break;
            }
            case ErrorCodes_1.default.USERNAME_ALREADY_EXISTS: {
                nimbusError = NimbusError_1.default.USERNAME_ALREADY_EXISTS;
                break;
            }
            case ErrorCodes_1.default.ERROR_INVALID_USERNAME: {
                nimbusError = NimbusError_1.default.ERROR_INVALID_USERNAME;
                break;
            }
        }
        return nimbusError;
    };
    return NimbusErrorHandler;
}());
exports.default = NimbusErrorHandler;
