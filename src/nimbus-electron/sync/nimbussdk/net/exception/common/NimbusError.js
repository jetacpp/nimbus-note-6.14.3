"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusError = (function () {
    function NimbusError(errorCode) {
        this.errorCode = errorCode;
    }
    NimbusError.prototype.getErrorCode = function () {
        return this.errorCode;
    };
    NimbusError.OK = 0;
    NimbusError.NOT_WELL_FORMED = -1;
    NimbusError.ACTION_PARAM_IS_MISSED = -2;
    NimbusError.UNRECOGNIZED_ACTION = -3;
    NimbusError.USER_ALREADY_EXISTS = -4;
    NimbusError.STORAGE_ENGINE_RETURNS_ERROR = -5;
    NimbusError.AUTH_FAILED = -6;
    NimbusError.USER_NOT_EXISTS = -7;
    NimbusError.INTERNAL_DATA_JSON_MAILFORMED = -8;
    NimbusError.WRONG_ARGUMENTS_COUNT = -9;
    NimbusError.INTERNAL_SERVER_ERROR = -10;
    NimbusError.INTERNAL_FILE_SYSTEM_ERROR = -11;
    NimbusError.INTERNAL_SENDMAIL_ERROR = -12;
    NimbusError.TOO_MUCH_REQUESTS = -13;
    NimbusError.ACCESS_DENIED = -14;
    NimbusError.MAX_EMAILS_LIMIT_REACHED = -15;
    NimbusError.DATA_TOO_LARGE = -16;
    NimbusError.ALREADY_LOCKED = -17;
    NimbusError.EXTERNAL_DATA_MALFORMED = -18;
    NimbusError.NOT_FOUND = -19;
    NimbusError.COUNT_ITEMS_QUOTA_EXCEED = -20;
    NimbusError.ITEM_ALREADY_EXISTS = -21;
    NimbusError.TEMPFILE_NOT_FOUND = -23;
    NimbusError.WRONG_CAPTCHA = -1001;
    NimbusError.USERNAME_ALREADY_EXISTS = -24;
    NimbusError.ERROR_INVALID_USERNAME = -25;
    NimbusError.UNKNOWN_ERROR = -1000;
    return NimbusError;
}());
exports.default = NimbusError;
