"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusError_1 = __importDefault(require("./NimbusError"));
var NimbusException = (function () {
    function NimbusException(nimbusError) {
        this.error = nimbusError;
        if (this.error)
            new NimbusError_1.default("name::" + this.error.toString() + "; errorCode::(" + this.error.getErrorCode() + ")");
    }
    NimbusException.prototype.getError = function () {
        return this.error;
    };
    return NimbusException;
}());
exports.default = NimbusException;
