"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RuntimeException = (function () {
    function RuntimeException(message) {
        this.message = "RuntimeException: " + message;
    }
    RuntimeException.prototype.getMessage = function () {
        return this.message;
    };
    return RuntimeException;
}());
exports.default = RuntimeException;
