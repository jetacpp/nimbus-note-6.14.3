"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Base_Request_1 = __importDefault(require("./common/Base_Request"));
var NotesAccountResponse_1 = __importDefault(require("./response/NotesAccountResponse"));
var NotesGetStructureResponse_1 = __importDefault(require("./response/NotesGetStructureResponse"));
var NotesTotalAmountResponse_1 = __importDefault(require("./response/NotesTotalAmountResponse"));
var GetRemovedItemsResponse_1 = __importDefault(require("./response/GetRemovedItemsResponse"));
var NotesGetAllResponse_1 = __importDefault(require("./response/NotesGetAllResponse"));
var NotesSearchResponse_1 = __importDefault(require("./response/NotesSearchResponse"));
var NotesGetFoldersResponse_1 = __importDefault(require("./response/NotesGetFoldersResponse"));
var NotesGetTagsResponse_1 = __importDefault(require("./response/NotesGetTagsResponse"));
var NotesShareResponse_1 = __importDefault(require("./response/NotesShareResponse"));
var NotesUnshareResponse_1 = __importDefault(require("./response/NotesUnshareResponse"));
var NotesGetBackupsResponse_1 = __importDefault(require("./response/NotesGetBackupsResponse"));
var NotesViewBackupResponse_1 = __importDefault(require("./response/NotesViewBackupResponse"));
var NotesRestoreBackupResponse_1 = __importDefault(require("./response/NotesRestoreBackupResponse"));
var NotesInviteResponse_1 = __importDefault(require("./response/NotesInviteResponse"));
var NotesAcceptInviteResponse_1 = __importDefault(require("./response/NotesAcceptInviteResponse"));
var NoteIsExistOnServerResponse_1 = __importDefault(require("./response/NoteIsExistOnServerResponse"));
var NotesUpdateResponse_1 = __importDefault(require("./response/NotesUpdateResponse"));
var NotesTextTokenUpdateResponse_1 = __importDefault(require("./response/NotesTextTokenUpdateResponse"));
var NotesAnnotateResponse_1 = __importDefault(require("./response/NotesAnnotateResponse"));
var INotesApi = (function () {
    function INotesApi() {
    }
    INotesApi.prototype.getStructureNotes = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesGetStructureResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.getNotesTotalAmount = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesTotalAmountResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.getRemovedItems = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), GetRemovedItemsResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.getFullNotes = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesGetAllResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.notesSearch = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesSearchResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.getFolders = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesGetFoldersResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.getTags = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesGetTagsResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.shareNotes = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesShareResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.unshareNotes = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesUnshareResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.account = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesAccountResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.notesBackup = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesGetBackupsResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.viewBackup = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesViewBackupResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.restoreBackup = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesRestoreBackupResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.invite = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesInviteResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.acceptInvite = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesAcceptInviteResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.checkIfNoteExistOnServer = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NoteIsExistOnServerResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.updateNotes = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesUpdateResponse_1.default, callback);
    };
    ;
    INotesApi.prototype.getNoteTextToken = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesTextTokenUpdateResponse_1.default, callback);
    };
    INotesApi.prototype.notesAnnotate = function (sessionId, token, req, callback) {
        Base_Request_1.default.make(Base_Request_1.default.prepareRequest(sessionId, token, req), NotesAnnotateResponse_1.default, callback);
    };
    return INotesApi;
}());
exports.default = INotesApi;
