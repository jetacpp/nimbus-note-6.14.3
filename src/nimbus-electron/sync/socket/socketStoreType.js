"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    NOTES_FOR_UPDATE_COUNTERS: "notes_for_update_counters",
    NOTES_FOR_UPDATE: "notes_for_update",
    NOTES_FOR_REMOVE: "notes_for_remove",
    NOTES_FOR_UPDATE_TEXT: "notes_for_update_text",
    FOLDERS_FOR_UPDATE: "folders_for_update",
    FOLDERS_FOR_REMOVE: "folders_for_remove",
    TODO_LIST_FOR_UPDATE: "todo_list_for_update",
    TODO_LIST_FOR_REMOVE: "todo_list_for_remove",
    NOTES_TAGS_FOR_UPDATE: "notes_tags_for_update",
    TAGS_FOR_UPDATE: "tags_for_update",
    NOTES_ATTACHMENTS_FOR_UPDATE: "notes_attachments_for_update",
    NOTES_ATTACHMENTS_FOR_REMOVE: "notes_attachments_for_remove",
    WORKSPACES_FOR_UPDATE: "workspaces_for_update",
    WORKSPACES_FOR_REMOVE: "workspaces_for_remove"
};
