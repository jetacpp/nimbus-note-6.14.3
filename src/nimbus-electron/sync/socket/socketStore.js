"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var collection = {};
var SocketStore = (function () {
    function SocketStore() {
    }
    SocketStore.add = function (inputData) {
        var workspaceId = inputData.workspaceId, storeType = inputData.storeType, data = inputData.data;
        if (typeof (collection[workspaceId]) === 'undefined') {
            collection[workspaceId] = {};
        }
        if (typeof (collection[workspaceId][storeType]) === 'undefined') {
            collection[workspaceId][storeType] = [];
        }
        collection[workspaceId][storeType].push(data);
    };
    SocketStore.get = function (inputData) {
        var workspaceId = inputData.workspaceId, storeType = inputData.storeType;
        if (typeof (collection[workspaceId]) === 'undefined') {
            collection[workspaceId] = {};
        }
        if (typeof (collection[workspaceId][storeType]) === 'undefined') {
            return [];
        }
        return collection[workspaceId][storeType];
    };
    SocketStore.clear = function (inputData) {
        var workspaceId = inputData.workspaceId, storeType = inputData.storeType;
        if (typeof (collection[workspaceId]) === 'undefined') {
            return;
        }
        if (typeof (collection[workspaceId][storeType]) !== 'undefined') {
            collection[workspaceId][storeType] = [];
        }
    };
    SocketStore.flush = function (inputData) {
        var workspaceId = inputData.workspaceId;
        if (typeof (collection[workspaceId]) === 'undefined') {
            return;
        }
        for (var storeType in collection[workspaceId]) {
            if (collection[workspaceId].hasOwnProperty(storeType)) {
                collection[workspaceId][storeType] = [];
            }
        }
    };
    SocketStore.flushAll = function () {
        collection = {};
    };
    return SocketStore;
}());
exports.default = SocketStore;
