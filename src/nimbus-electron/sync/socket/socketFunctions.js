"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var server_1 = __importDefault(require("./server"));
var socketStore_1 = __importDefault(require("./socketStore"));
var socketStoreType_1 = __importDefault(require("./socketStoreType"));
var item_1 = __importDefault(require("../../db/models/item"));
var todo_1 = __importDefault(require("../../db/models/todo"));
var attach_1 = __importDefault(require("../../db/models/attach"));
var SelectedWorkspace_1 = __importDefault(require("../../workspace/SelectedWorkspace"));
var socketStoreSizeOverloadState = {};
var socketAttachmentsCopy = {};
var SocketFunctions = (function () {
    function SocketFunctions() {
    }
    SocketFunctions.sendItemsCountMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, globalId, parentId, type;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, globalId = inputData.globalId, parentId = inputData.parentId, type = inputData.type;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        if ((_a.sent()) !== workspaceId) {
                            return [2];
                        }
                        if (type === "note") {
                            item_1.default.count({ parentId: parentId, type: type }, { workspaceId: workspaceId }, function (err, notesCount) {
                                if (!err) {
                                    var folderMessage = {
                                        globalId: parentId,
                                        childrenNotesCount: notesCount
                                    };
                                    server_1.default.emit('note:count', {
                                        'message': folderMessage
                                    });
                                }
                            });
                            todo_1.default.count({ noteGlobalId: globalId, "checked": false }, { workspaceId: workspaceId }, function (err, todosCount) {
                                var noteMessage = { globalId: globalId };
                                if (!err) {
                                    noteMessage.todosCount = todosCount;
                                }
                                attach_1.default.count({ noteGlobalId: globalId, inList: true }, { workspaceId: workspaceId }, function (err, attachCount) {
                                    if (!err) {
                                        noteMessage.attachmentsCount = attachCount;
                                    }
                                    server_1.default.emit('note:count', {
                                        'message': noteMessage
                                    });
                                });
                            });
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.sendItemUpdateMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var selectedWorkspaceId, workspaceId, globalId, parentId, isCreate, type, reminderRemove, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        selectedWorkspaceId = _a.sent();
                        workspaceId = inputData.workspaceId, globalId = inputData.globalId, parentId = inputData.parentId, isCreate = inputData.isCreate, type = inputData.type, reminderRemove = inputData.reminderRemove;
                        if (selectedWorkspaceId !== workspaceId) {
                            return [2];
                        }
                        if (!parentId) {
                            parentId = null;
                        }
                        if (!isCreate) {
                            isCreate = false;
                        }
                        if (!type) {
                            type = 'note';
                        }
                        if (globalId) {
                            message = {
                                globalId: globalId
                            };
                            if (parentId && type) {
                                message['parentId'] = parentId;
                                message['isCreate'] = isCreate;
                                message['type'] = type;
                            }
                            server_1.default.emit('note:update', {
                                'message': message
                            });
                            if (type === 'note') {
                                if (reminderRemove) {
                                    server_1.default.emit('noteReminder:remove', {
                                        'message': { globalId: globalId }
                                    });
                                }
                                else {
                                    server_1.default.emit('noteReminder:update', {
                                        'message': { globalId: globalId }
                                    });
                                }
                            }
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.sendItemRemoveMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, globalIdList;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, globalIdList = inputData.globalIdList;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        if ((_a.sent()) !== workspaceId) {
                            return [2];
                        }
                        if (globalIdList && globalIdList.length) {
                            server_1.default.emit('note:remove', {
                                'message': {
                                    globalId: globalIdList
                                }
                            });
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.sendTextUpdateMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, globalId, typing, textVersion;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, globalId = inputData.globalId, typing = inputData.typing, textVersion = inputData.textVersion;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        if ((_a.sent()) !== workspaceId) {
                            return [2];
                        }
                        if (globalId) {
                            if (textVersion >= 2) {
                                typing = true;
                            }
                            server_1.default.emit('noteText:update', {
                                'message': {
                                    globalId: globalId,
                                    typing: typing
                                }
                            });
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.sendItemPreviewUpdateMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, globalId, attachmentGlobalId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, globalId = inputData.globalId, attachmentGlobalId = inputData.attachmentGlobalId;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        if ((_a.sent()) !== workspaceId) {
                            return [2];
                        }
                        if (globalId && attachmentGlobalId) {
                            server_1.default.emit('notePreview:update', {
                                'message': {
                                    globalId: globalId,
                                    attachmentGlobalId: attachmentGlobalId
                                }
                            });
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.sendItemPreviewRemoveMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, globalId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        if ((_a.sent()) !== workspaceId) {
                            return [2];
                        }
                        if (globalId) {
                            server_1.default.emit('notePreview:remove', {
                                'message': {
                                    globalId: globalId,
                                    attachmentGlobalId: globalId
                                }
                            });
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.sendTodoListUpdateMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, noteGlobalId, globalId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, globalId = inputData.globalId;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        if ((_a.sent()) !== workspaceId) {
                            return [2];
                        }
                        if (globalId && noteGlobalId) {
                            server_1.default.emit('noteTodo:update', {
                                'message': {
                                    globalId: globalId,
                                    noteGlobalId: noteGlobalId
                                }
                            });
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.sendTodoListRemoveMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, noteGlobalId, globalId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, globalId = inputData.globalId;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        if ((_a.sent()) !== workspaceId) {
                            return [2];
                        }
                        if (globalId && noteGlobalId) {
                            server_1.default.emit('noteTodo:remove', {
                                'message': {
                                    noteGlobalId: noteGlobalId,
                                    globalId: globalId
                                }
                            });
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.sendNoteAttachmentsUpdateMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, noteGlobalId, globalId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, globalId = inputData.globalId;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        if ((_a.sent()) !== workspaceId) {
                            return [2];
                        }
                        if (globalId && noteGlobalId) {
                            setTimeout(function () {
                                server_1.default.emit('noteAttachment:update', {
                                    'message': {
                                        noteGlobalId: noteGlobalId,
                                        globalId: globalId
                                    }
                                });
                                server_1.default.emit('note:update', { 'message': { globalId: noteGlobalId } });
                            }, 1000);
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.sendNoteAttachmentsRemoveMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, noteGlobalId, globalId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, globalId = inputData.globalId;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        if ((_a.sent()) !== workspaceId) {
                            return [2];
                        }
                        if (globalId && noteGlobalId) {
                            setTimeout(function () {
                                server_1.default.emit('noteAttachment:remove', {
                                    'message': {
                                        noteGlobalId: noteGlobalId,
                                        globalId: globalId
                                    }
                                });
                                server_1.default.emit('note:update', { 'message': { globalId: noteGlobalId } });
                            }, 1000);
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.sendTagsMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, tagList, action;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, tagList = inputData.tagList, action = inputData.action;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        if ((_a.sent()) !== workspaceId) {
                            return [2];
                        }
                        if (tagList && tagList.length && action) {
                            server_1.default.emit('workspaceTag:' + action, {
                                'message': {
                                    tags: tagList
                                },
                                workspaceId: workspaceId
                            });
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.sendNoteTagsMessage = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, noteGlobalId, tag, action;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, tag = inputData.tag, action = inputData.action;
                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        if ((_a.sent()) !== workspaceId) {
                            return [2];
                        }
                        if (noteGlobalId && tag && action) {
                            server_1.default.emit('noteTag:' + action, {
                                'message': {
                                    noteGlobalId: noteGlobalId,
                                    tag: tag
                                }
                            });
                        }
                        return [2];
                }
            });
        });
    };
    SocketFunctions.addStoreData = function (inputData) {
        socketStore_1.default.add(inputData);
    };
    SocketFunctions.clearStoreData = function (inputData) {
        socketStore_1.default.clear(inputData);
    };
    SocketFunctions.getStoreData = function (inputData) {
        return socketStore_1.default.get(inputData);
    };
    SocketFunctions.flushStore = function (inputData) {
        var workspaceId = inputData.workspaceId;
        SocketFunctions.setSocketStoreSizeOverloadState({ workspaceId: workspaceId, state: false });
        SocketFunctions.clearAttachmentsDataCopy(inputData);
        socketStore_1.default.flush(inputData);
    };
    SocketFunctions.flushStores = function () {
        SocketFunctions.clearSocketStoresSizeOverloadState();
        SocketFunctions.clearAttachmentsDataCopies();
        socketStore_1.default.flushAll();
    };
    SocketFunctions.getSocketStoreSizeOverloadState = function (inputData) {
        var workspaceId = inputData.workspaceId;
        return typeof (socketStoreSizeOverloadState[workspaceId]) !== 'undefined' ? socketStoreSizeOverloadState[workspaceId] : false;
    };
    SocketFunctions.setSocketStoreSizeOverloadState = function (inputData) {
        var workspaceId = inputData.workspaceId, state = inputData.state;
        socketStoreSizeOverloadState[workspaceId] = state;
    };
    SocketFunctions.clearSocketStoresSizeOverloadState = function () {
        socketStoreSizeOverloadState = {};
    };
    SocketFunctions.sendCollectedStoreData = function (inputData) {
        var workspaceId = inputData.workspaceId, storeType = inputData.storeType;
        var storeItems = socketStore_1.default.get(inputData);
        var noReloadOnEvents = [
            socketStoreType_1.default.NOTES_TAGS_FOR_UPDATE,
            socketStoreType_1.default.TAGS_FOR_UPDATE
        ];
        if (noReloadOnEvents.indexOf(storeType) < 0) {
            if (SocketFunctions.getSocketStoreSizeOverloadState({ workspaceId: workspaceId })) {
                return;
            }
            else if (storeItems.length > SocketFunctions.SOCKET_STORE_SEND_MESSAGES_COUNT) {
                return SocketFunctions.setSocketStoreSizeOverloadState({ workspaceId: workspaceId, state: true });
            }
        }
        for (var _i = 0, storeItems_1 = storeItems; _i < storeItems_1.length; _i++) {
            var storeItem = storeItems_1[_i];
            if (storeType === socketStoreType_1.default.NOTES_FOR_UPDATE_COUNTERS) {
                SocketFunctions.sendItemsCountMessage({
                    workspaceId: workspaceId,
                    globalId: storeItem.globalId,
                    parentId: storeItem.parentId,
                    type: storeItem.type
                });
            }
            else if (storeType === socketStoreType_1.default.NOTES_FOR_UPDATE || storeType === socketStoreType_1.default.FOLDERS_FOR_UPDATE) {
                SocketFunctions.sendItemUpdateMessage({
                    workspaceId: workspaceId,
                    globalId: storeItem.globalId,
                    parentId: storeItem.parentId,
                    isCreate: storeItem.isCreate,
                    type: storeItem.type,
                    reminderRemove: storeItem.reminderRemove
                });
            }
            else if (storeType === socketStoreType_1.default.NOTES_FOR_REMOVE || storeType === socketStoreType_1.default.FOLDERS_FOR_REMOVE) {
                SocketFunctions.sendItemRemoveMessage({
                    workspaceId: workspaceId,
                    globalIdList: storeItem.globalId
                });
            }
            else if (storeType === socketStoreType_1.default.NOTES_FOR_UPDATE_TEXT) {
                SocketFunctions.sendTextUpdateMessage({
                    workspaceId: workspaceId,
                    globalId: storeItem.globalId,
                    textVersion: storeItem.textVersion
                });
            }
            else if (storeType === socketStoreType_1.default.TODO_LIST_FOR_UPDATE) {
                SocketFunctions.sendTodoListUpdateMessage({
                    workspaceId: workspaceId,
                    noteGlobalId: storeItem.noteGlobalId,
                    globalId: storeItem.globalId
                });
            }
            else if (storeType === socketStoreType_1.default.TODO_LIST_FOR_REMOVE) {
                SocketFunctions.sendTodoListRemoveMessage({
                    workspaceId: workspaceId,
                    noteGlobalId: storeItem.noteGlobalId,
                    globalId: storeItem.globalId
                });
            }
            else if (storeType === socketStoreType_1.default.NOTES_ATTACHMENTS_FOR_UPDATE) {
                SocketFunctions.sendNoteAttachmentsUpdateMessage({
                    workspaceId: workspaceId,
                    noteGlobalId: storeItem.noteGlobalId,
                    globalId: storeItem.globalId
                });
            }
            else if (storeType === socketStoreType_1.default.NOTES_ATTACHMENTS_FOR_REMOVE) {
                SocketFunctions.sendNoteAttachmentsRemoveMessage({
                    workspaceId: workspaceId,
                    noteGlobalId: storeItem.noteGlobalId,
                    globalId: storeItem.globalId
                });
            }
            else if (storeType === socketStoreType_1.default.NOTES_TAGS_FOR_UPDATE) {
                SocketFunctions.sendNoteTagsMessage({
                    workspaceId: workspaceId,
                    noteGlobalId: storeItem.noteGlobalId,
                    tag: storeItem.tag,
                    action: storeItem.action
                });
            }
            else if (storeType === socketStoreType_1.default.TAGS_FOR_UPDATE) {
                SocketFunctions.sendTagsMessage({
                    workspaceId: workspaceId,
                    tagList: storeItem.tags,
                    action: storeItem.action
                });
            }
        }
        socketStore_1.default.clear(inputData);
    };
    SocketFunctions.setAttachmentsDataCopy = function (inputData) {
        var workspaceId = inputData.workspaceId;
        socketAttachmentsCopy[workspaceId] = SocketFunctions.getCollectedStoreData({
            workspaceId: workspaceId,
            storeType: socketStoreType_1.default.NOTES_ATTACHMENTS_FOR_UPDATE
        });
    };
    SocketFunctions.getAttachmentsDataCopy = function (inputData) {
        var workspaceId = inputData.workspaceId;
        return typeof (socketAttachmentsCopy[workspaceId]) !== 'undefined' ? socketAttachmentsCopy[workspaceId] : [];
    };
    SocketFunctions.clearAttachmentsDataCopy = function (inputData) {
        var workspaceId = inputData.workspaceId;
        if (typeof (socketAttachmentsCopy[workspaceId]) !== 'undefined') {
            socketAttachmentsCopy[workspaceId] = [];
        }
    };
    SocketFunctions.clearAttachmentsDataCopies = function () {
        socketAttachmentsCopy = {};
    };
    SocketFunctions.getCollectedStoreData = function (inputData) {
        return socketStore_1.default.get(inputData);
    };
    SocketFunctions.SOCKET_STORE_SEND_MESSAGES_COUNT = 200;
    return SocketFunctions;
}());
exports.default = SocketFunctions;
