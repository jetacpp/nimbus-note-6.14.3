"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var io;
var localSocket;
var SocketServer = (function () {
    function SocketServer() {
    }
    SocketServer.start = function (server) {
        if (!localSocket) {
            io = require('socket.io')(server);
            io.on('connection', function (socket) {
                localSocket = socket;
                io.on('disconnect', function () {
                    localSocket = null;
                });
            });
        }
    };
    SocketServer.emit = function (eventName, eventData) {
        eventData = eventData || {};
        if (localSocket && eventName) {
            setTimeout(function () {
                localSocket.emit(eventName, eventData);
            }, 250);
        }
    };
    return SocketServer;
}());
exports.default = SocketServer;
