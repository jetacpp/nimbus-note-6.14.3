"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var instance_1 = __importDefault(require("../../../window/instance"));
var App_1 = __importDefault(require("../application/App"));
var status = {};
var SyncStatusDisplayEvent = (function () {
    function SyncStatusDisplayEvent() {
    }
    SyncStatusDisplayEvent.set = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, newStatus, props, data, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, newStatus = inputData.newStatus, props = inputData.props;
                        data = {
                            workspaceId: workspaceId,
                            status: newStatus
                        };
                        if (!(SyncStatusDisplayEvent.RESET_SYNC_TYPE_STATUS_LIST.indexOf(newStatus) >= 0)) return [3, 2];
                        return [4, App_1.default.setRunningSyncType({
                                workspaceId: workspaceId,
                                type: App_1.default.SYNC_TYPES.NONE
                            })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (instance_1.default.get()) {
                            if (props) {
                                for (i in props) {
                                    if (props.hasOwnProperty(i)) {
                                        data[i] = props[i];
                                    }
                                }
                            }
                            instance_1.default.get().webContents.send('event:client:sync:response', data);
                        }
                        return [2, status[workspaceId] = newStatus];
                }
            });
        });
    };
    SyncStatusDisplayEvent.get = function (inputData) {
        var workspaceId = inputData.workspaceId;
        return typeof (status[workspaceId]) === 'undefined' ? null : status[workspaceId];
    };
    SyncStatusDisplayEvent.STATUS = {
        "NOT_AVAILABLE": "NOT_AVAILABLE",
        "CANCELED": "CANCELED",
        "READY": "READY",
        "DONE": "DONE",
        "AUTHORIZED": "AUTHORIZED",
        "DOWNLOADING_META_INFO": "DOWNLOADING_META_INFO",
        "REMOVE_FOLDERS": "REMOVE_FOLDERS",
        "REMOVE_NOTES": "REMOVE_NOTES",
        "REMOVE_ATTACHMENTS": "REMOVE_ATTACHMENTS",
        "UPDATE_CONTENT": "UPDATE_CONTENT",
        "DOWNLOAD_NOTES": "DOWNLOAD_NOTES",
        "DOWNLOAD_ATTACHMENT": "DOWNLOAD_ATTACHMENT",
        "UPLOAD_FOLDERS": "UPLOAD_FOLDERS",
        "UPLOAD_TAGS": "UPLOAD_TAGS",
        "UPLOAD_NOTES": "UPLOAD_NOTES",
        "UPLOAD_ATTACHMENTS": "UPLOAD_ATTACHMENTS",
        "TRAFFIC_LIMIT": "TRAFFIC_LIMIT",
        "NEED_SYNC_BEFORE_SHARE": "NEED_SYNC_BEFORE_SHARE"
    };
    SyncStatusDisplayEvent.RESET_SYNC_TYPE_STATUS_LIST = [
        SyncStatusDisplayEvent.STATUS.CANCELED,
        SyncStatusDisplayEvent.STATUS.NOT_AVAILABLE,
        SyncStatusDisplayEvent.STATUS.TRAFFIC_LIMIT
    ];
    return SyncStatusDisplayEvent;
}());
exports.default = SyncStatusDisplayEvent;
