"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../nimbussdk/net/NimbusSDK"));
var settings_1 = __importDefault(require("../../../db/models/settings"));
var SyncConfigInstance = null;
var SyncConfig = (function () {
    function SyncConfig() {
    }
    SyncConfig.init = function () {
        if (!SyncConfigInstance) {
            SyncConfigInstance = new SyncConfig();
        }
    };
    SyncConfig.get = function (key, defValue, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        defValue = defValue || null;
        settings_1.default.get(key, function (err, value) {
            callback(err, value ? value : defValue);
        });
    };
    SyncConfig.put = function (key, value, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        if (!value) {
            return callback(null, value);
        }
        settings_1.default.set(key, value, function (err, response) {
            callback(err, response);
        });
    };
    SyncConfig.remove = function (key, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        settings_1.default.remove(key, callback);
    };
    SyncConfig.setDefaultFolderTitle = function (title) {
        NimbusSDK_1.default.getAccountManager().getUserEmail(function (err, userEmail) {
            SyncConfig.put(SyncConfig.DEFAULT + "_" + userEmail, title);
        });
    };
    SyncConfig.getDefaultFolderTitle = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NimbusSDK_1.default.getAccountManager().getUserEmail(function (err, userEmail) {
            SyncConfig.get((SyncConfig.DEFAULT + "_" + userEmail), "My Notes", function (err, value) {
                callback(err, value);
            });
        });
    };
    SyncConfig.SYNC_TYPE_IS_HEADER = "sync_type_is_header";
    SyncConfig.SHOWED_WELCOME_SCREEN = "showed_welcome_screen";
    SyncConfig.WEBVIEW_ONE_COLUMN_VIEW = "webview_one_column_view";
    SyncConfig.QUICK_NOTE_WIDGET_ITEM_TEXT = "quick_note_widget_item_text";
    SyncConfig.QUICK_NOTE_WIDGET_ITEM_TODO = "quick_note_widget_item_todo";
    SyncConfig.QUICK_NOTE_WIDGET_ITEM_CAMERA = "quick_note_widget_item_camera";
    SyncConfig.QUICK_NOTE_WIDGET_ITEM_AUDIO = "quick_note_widget_item_audio";
    SyncConfig.QUICK_NOTE_WIDGET_ITEM_VIDEO = "quick_note_widget_item_video";
    SyncConfig.QUICK_NOTE_WIDGET_ITEM_PAINTER = "quick_note_widget_item_painter";
    SyncConfig.QUICK_NOTE_WIDGET_ITEM_TIME_REMINDER = "quick_note_widget_item_time_reminder";
    SyncConfig.QUICK_NOTE_WIDGET_ITEM_PLACE_REMINDER = "quick_note_widget_item_place_reminder";
    SyncConfig.IS_AFTER_AUTH_SCREEN = "is_after_auth_screen";
    SyncConfig.APP_THEME = "theme";
    SyncConfig.SHOW_FORMAT_PANEL = "show_format_panel";
    SyncConfig.NOTE_ONE_WIDGET = "note_one_widget";
    SyncConfig.NOTE_ONE_WIDGET_FOLDER = "note_one_widget_folder";
    SyncConfig.NOTE_ONE_WIDGET_FOLDER_TEMP = "note_one_widget_folder_temp";
    SyncConfig.NOTE_ONE_WIDGET_NOTE = "note_one_widget_note";
    SyncConfig.TODO_WIDGET_NOTE = "todoWidget";
    SyncConfig.NOTE_ONE_WIDGET_VISIBILITY = "note_one_widget_visibility";
    SyncConfig.NOTE_ONE_WIDGET_ACCESS_PASSWORD_WIDGET_ID = "note_one_widget_access_password_widget_id";
    SyncConfig.EDITOR_SCALE_IMAGES = "editor_scale_images";
    SyncConfig.EDITOR_DEFAULT_TAGS = "editor_default_tags";
    SyncConfig.EDITOR_AUTOSAVE = "editor_autosave";
    SyncConfig.TODO_SHOW_COMPLETED_TODO = "todo_show_completed_todo";
    SyncConfig.SEARCH_IN_NOTE_TEXT = "search_in_note_text";
    SyncConfig.VISIBILITY_CONTROL_RECT_LEFT = "RECT_LEFT";
    SyncConfig.VISIBILITY_CONTROL_RECT_RIGHT = "RECT_RIGHT";
    SyncConfig.VISIBILITY_CONTROL_RECT_TOP = "RECT_TOP";
    SyncConfig.VISIBILITY_CONTROL_RECT_BOTTOM = "RECT_BOTTOM";
    SyncConfig.APP_LOCK_PASSWORD = "APP_LOCK_PASSWORD";
    SyncConfig.APP_LOCK_FINGERPRINT = "APP_LOCK_FINGERPRINT";
    SyncConfig.KEY_AUTOSYNC_DISABLED_AFTER_BACKUP = "autosync_disabled_after_backup";
    SyncConfig.KEY_SYNC_IMPORTED_DATA_ENABLED = "sync_imported_data_enabled";
    SyncConfig.KEY_NEED_4X_DB_MIGRATION = "need_4x_db_migration";
    SyncConfig.TOP_SCROLL = "topScroll_";
    SyncConfig.CURRENT_VISIBLE_FRAGMENT_ONE_NOTE_WIDGET = "CURRENT_VISIBLE_FRAGMENT_ONE_NOTE_WIDGET";
    SyncConfig.SORT_NOTE_TYPE = "sort_note_type";
    SyncConfig.SORT_TAG_TYPE = "sort_tag_type";
    SyncConfig.SORT_FOLDER_TYPE = "sort_folder_type";
    SyncConfig.SEARCH_QUERY_CAHCE = "search_query_cache";
    SyncConfig.NIMBUS_NOTE_3_0_TUTORIAL = "NimbusNote_3.0_tutorial";
    SyncConfig.IMAGE_QUALITY_LEVEL = "IMAGE_QUALITY_LEVEL";
    SyncConfig.NOTES_LIST_VIEW_SHOW_TAGS = "notes_list_view_show_tags";
    SyncConfig.NOTES_LIST_VIEW_MODE = "notes_list_view_mode";
    SyncConfig.NOTES_LIST_CIRCLE_PREVIEW_VIEW = "notes_list_view_circle_preview_view";
    SyncConfig.ATTACHMENT_GLOBAL_ID = "attachment_global_id";
    SyncConfig.DEFAULT = "default";
    SyncConfig.SECRET_MODE = "SECRET_MODE";
    SyncConfig.WHATS_NEW_SHOWED = "WHATS_NEW_SHOWED";
    SyncConfig.LAST_OPEN_FOLDER = "last_open_folder";
    SyncConfig.DEFAULT_FOLDER = "default_folder";
    SyncConfig.DEFAULT_WIDGET_FOLDER = "default_widget_folder";
    SyncConfig.FULL_LIMIT_QUOTA_SHOWED = "full_limit_quota";
    SyncConfig.FULL_LIMIT_NOTE_SHOWED = "full_limit_note";
    SyncConfig.FULL_LIMIT_ATTACH_SHOWED = "full_limit_attach";
    SyncConfig.LOCATION_IS_UNAVAILABLE_NEVER_ASK_AGAIN = "location_is_unavailable_never_ask_again";
    return SyncConfig;
}());
exports.default = SyncConfig;
