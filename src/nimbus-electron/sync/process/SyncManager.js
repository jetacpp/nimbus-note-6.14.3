"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusErrorHandler_1 = __importDefault(require("../nimbussdk/net/exception/common/NimbusErrorHandler"));
var config_1 = __importDefault(require("../../../config"));
var NimbusSDK_1 = __importDefault(require("../nimbussdk/net/NimbusSDK"));
var App_1 = __importDefault(require("./application/App"));
var NimbusSyncService_1 = __importDefault(require("./services/NimbusSyncService"));
var AutoSyncManager_1 = __importDefault(require("../nimbussdk/manager/AutoSyncManager"));
var state_1 = __importDefault(require("../../online/state"));
var SyncStatusChangedEvent_1 = __importDefault(require("./events/SyncStatusChangedEvent"));
var SyncStatusDisplayEvent_1 = __importDefault(require("./events/SyncStatusDisplayEvent"));
var ConnectionCollector_1 = __importDefault(require("./connection/ConnectionCollector"));
var errorHandler_1 = __importDefault(require("../../utilities/errorHandler"));
var workspace_1 = __importDefault(require("../../db/models/workspace"));
var socketFunctions_1 = __importDefault(require("../socket/socketFunctions"));
var API_1 = __importDefault(require("../nimbussdk/net/API"));
var syncHandler_1 = __importDefault(require("../../utilities/syncHandler"));
var DIFF_SYNC_TIME_TO_START_MSEC = 2500;
var syncService = null;
var lastSyncStartTime = 0;
var SyncManager = (function () {
    function SyncManager() {
    }
    SyncManager.startSyncOnline = function (inputData) {
        if (!SyncManager.readyToSync(inputData)) {
            return;
        }
        var workspaceId = inputData.workspaceId, timeout = inputData.timeout;
        if (timeout > 0) {
            setTimeout(function () {
                if (state_1.default.get()) {
                    if (config_1.default.SHOW_WEB_CONSOLE) {
                        console.log("SyncManager start online (ws: " + workspaceId + ")");
                    }
                    SyncManager.sync(inputData);
                }
                else {
                    errorHandler_1.default.log({
                        err: null, response: null,
                        description: "Sync problem => SyncManager => startSyncOnline",
                        data: inputData
                    });
                }
            }, timeout);
        }
        else {
            if (state_1.default.get()) {
                if (config_1.default.SHOW_WEB_CONSOLE) {
                    console.log("SyncManager start online (ws: " + workspaceId + ")");
                }
                SyncManager.sync(inputData);
            }
            else {
                errorHandler_1.default.log({
                    err: null, response: null,
                    description: "Sync problem => SyncManager => startSyncOnline",
                    data: inputData
                });
            }
        }
    };
    SyncManager.sync = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, userNotAuthorizedHandler;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId;
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("Sync start (ws: " + workspaceId + ")");
                        }
                        userNotAuthorizedHandler = function (err, inputData) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4, SyncStatusDisplayEvent_1.default.set({
                                            workspaceId: workspaceId,
                                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.CANCELED
                                        })];
                                    case 1:
                                        _a.sent();
                                        if (config_1.default.SHOW_WEB_CONSOLE) {
                                            console.log("Error: sync user not authorized");
                                        }
                                        errorHandler_1.default.log({
                                            err: null, response: null,
                                            description: "Sync problem => SyncManager => userNotAuthorizedHandler"
                                        });
                                        SyncManager.clearSyncQueue(inputData);
                                        API_1.default.checkSignInStatus(err, null);
                                        return [2];
                                }
                            });
                        }); };
                        return [4, SyncStatusDisplayEvent_1.default.set({
                                workspaceId: workspaceId,
                                newStatus: SyncStatusDisplayEvent_1.default.STATUS.AUTHORIZED
                            })];
                    case 1:
                        _a.sent();
                        NimbusSDK_1.default.getApi().userInfo({ workspaceId: workspaceId }, function (err, updatedInfo) {
                            if (err || !updatedInfo) {
                                return SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err }, function () { return __awaiter(_this, void 0, void 0, function () {
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0: return [4, userNotAuthorizedHandler(err, inputData)];
                                            case 1:
                                                _a.sent();
                                                return [2];
                                        }
                                    });
                                }); });
                            }
                            if (config_1.default.SHOW_WEB_CONSOLE) {
                                console.log("Success: get actual user info sync");
                            }
                            NimbusSDK_1.default.getAccountManager().isAuthorized(function (err, isAuthorized) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (err || !isAuthorized) {
                                                return [2, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err }, function () { return __awaiter(_this, void 0, void 0, function () {
                                                        return __generator(this, function (_a) {
                                                            switch (_a.label) {
                                                                case 0: return [4, userNotAuthorizedHandler(err, inputData)];
                                                                case 1:
                                                                    _a.sent();
                                                                    return [2];
                                                            }
                                                        });
                                                    }); })];
                                            }
                                            if (config_1.default.SHOW_WEB_CONSOLE) {
                                                console.log("Success: user authorized sync (ws: " + workspaceId + ")");
                                            }
                                            SyncManager.clearSyncQueue(inputData);
                                            return [4, App_1.default.canStartNewSync(inputData)];
                                        case 1:
                                            if (!_a.sent()) return [3, 3];
                                            syncService = new NimbusSyncService_1.default();
                                            return [4, syncService.onStartCommand(inputData, callback)];
                                        case 2:
                                            _a.sent();
                                            return [3, 4];
                                        case 3:
                                            if (config_1.default.SHOW_WEB_CONSOLE) {
                                                console.log("Warning: can't start sync because sync already started (ws: " + workspaceId + ")");
                                            }
                                            errorHandler_1.default.log({
                                                err: null, response: null,
                                                description: "Sync problem => SyncManager => canStartNewSync",
                                                data: {
                                                    workspaceId: workspaceId
                                                }
                                            });
                                            callback(null, false);
                                            _a.label = 4;
                                        case 4: return [2];
                                    }
                                });
                            }); });
                        });
                        return [2];
                }
            });
        });
    };
    SyncManager.stopAllSync = function () {
        var _this = this;
        NimbusSDK_1.default.getAccountManager().isAuthorized(function (err, isAuthorized) { return __awaiter(_this, void 0, void 0, function () {
            var availableWorkspaceList, _i, availableWorkspaceList_1, workspaceId, syncNeedStop;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(isAuthorized && syncService)) return [3, 6];
                        return [4, workspace_1.default.getAvailableIdList()];
                    case 1:
                        availableWorkspaceList = _a.sent();
                        _i = 0, availableWorkspaceList_1 = availableWorkspaceList;
                        _a.label = 2;
                    case 2:
                        if (!(_i < availableWorkspaceList_1.length)) return [3, 5];
                        workspaceId = availableWorkspaceList_1[_i];
                        return [4, SyncManager.stopSync(workspaceId)];
                    case 3:
                        syncNeedStop = _a.sent();
                        if (!syncNeedStop) {
                            return [3, 4];
                        }
                        errorHandler_1.default.log({
                            err: null, response: null,
                            description: "Sync => SyncManager => stopAllSync",
                            data: {
                                workspaceId: workspaceId
                            }
                        });
                        _a.label = 4;
                    case 4:
                        _i++;
                        return [3, 2];
                    case 5: return [3, 7];
                    case 6:
                        errorHandler_1.default.log({
                            err: null, response: null,
                            description: "Sync problem => SyncManager => stopAllSync => userNotAuthorizedHandler"
                        });
                        _a.label = 7;
                    case 7: return [2];
                }
            });
        }); });
    };
    SyncManager.stopSync = function (workspaceId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var syncType, response;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, App_1.default.getRunningSyncType({ workspaceId: workspaceId })];
                                case 1:
                                    syncType = _a.sent();
                                    if (syncType === App_1.default.SYNC_TYPES.NONE) {
                                        return [2, resolve(false)];
                                    }
                                    response = {
                                        errorCode: -500,
                                        errDesc: 'Stop all requests to sync server',
                                        workspaceId: workspaceId
                                    };
                                    NimbusErrorHandler_1.default.throwNimbusApiErrorIfExist({
                                        response: response,
                                        functionName: "stopAllSync"
                                    });
                                    return [4, ConnectionCollector_1.default.abort({ workspaceId: workspaceId })];
                                case 2:
                                    _a.sent();
                                    return [4, App_1.default.setRunningSyncType({
                                            workspaceId: workspaceId,
                                            type: App_1.default.SYNC_TYPES.NONE
                                        })];
                                case 3:
                                    _a.sent();
                                    return [4, SyncStatusChangedEvent_1.default.setStatus({
                                            workspaceId: workspaceId,
                                            newStatus: SyncStatusChangedEvent_1.default.STATUS.PAUSED,
                                        })];
                                case 4:
                                    _a.sent();
                                    return [4, SyncStatusDisplayEvent_1.default.set({
                                            workspaceId: workspaceId,
                                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.CANCELED,
                                        })];
                                case 5:
                                    _a.sent();
                                    NimbusSyncService_1.default.onSyncEnd({
                                        workspaceId: workspaceId,
                                        startQueueSync: false
                                    });
                                    return [2, resolve(true)];
                            }
                        });
                    }); })];
            });
        });
    };
    SyncManager.addSyncQueue = function (inputData) {
        var workspaceId = inputData.workspaceId;
        var state = true;
        AutoSyncManager_1.default.setQueueSync({ workspaceId: workspaceId, state: state });
    };
    SyncManager.clearSyncQueue = function (inputData) {
        AutoSyncManager_1.default.clearQueueSync(inputData);
    };
    SyncManager.clearAllSyncData = function () {
        ConnectionCollector_1.default.clearAll();
        socketFunctions_1.default.flushStores();
        AutoSyncManager_1.default.clearTimers();
        workspace_1.default.clearDefaultWorkspace();
    };
    SyncManager.readyToSync = function (inputData) {
        var workspaceId = inputData.workspaceId, timeout = inputData.timeout;
        var passTimeFromLastRun = new Date().getTime() - lastSyncStartTime;
        if (passTimeFromLastRun < DIFF_SYNC_TIME_TO_START_MSEC) {
            syncHandler_1.default.sendLog("Problem: try sync workspace too often for workspaceId: " + workspaceId + " and timeout: " + timeout);
            return false;
        }
        lastSyncStartTime = new Date().getTime();
        return true;
    };
    return SyncManager;
}());
exports.default = SyncManager;
