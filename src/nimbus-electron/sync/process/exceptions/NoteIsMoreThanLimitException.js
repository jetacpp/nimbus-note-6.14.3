"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var RuntimeException_1 = __importDefault(require("./RuntimeException"));
var NoteIsMoreThanLimitException = (function (_super) {
    __extends(NoteIsMoreThanLimitException, _super);
    function NoteIsMoreThanLimitException(type, globalId) {
        var _this = _super.call(this, "NoteIsMoreThanLimitException") || this;
        _this.globalId = globalId;
        _this.type = type;
        return _this;
    }
    NoteIsMoreThanLimitException.prototype.getGlobalId = function () {
        return this.globalId;
    };
    NoteIsMoreThanLimitException.prototype.getType = function () {
        return this.type;
    };
    NoteIsMoreThanLimitException.TYPE = {
        "QUOTA": "QUOTA",
        "NOTE": "NOTE",
        "ATTACHMENT": "ATTACHMENT"
    };
    return NoteIsMoreThanLimitException;
}(RuntimeException_1.default));
exports.default = NoteIsMoreThanLimitException;
