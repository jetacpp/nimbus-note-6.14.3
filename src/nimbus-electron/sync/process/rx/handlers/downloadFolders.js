"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var FolderObjRepository_1 = __importDefault(require("../../repositories/FolderObjRepository"));
var FolderObj_1 = __importDefault(require("../../db/FolderObj"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
function downloadFolders(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId;
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 1:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    NimbusSDK_1.default.getAccountManager().getUniqueUserName(function (err, uniqueUserName) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!err) return [3, 2];
                                    return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                case 1:
                                    _a.sent();
                                    errorHandler_1.default.log({
                                        err: err, response: uniqueUserName,
                                        description: "Sync problem => downloadFolders => getUniqueUserName"
                                    });
                                    return [2, callback(err, false)];
                                case 2: return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                case 3:
                                    if (_a.sent())
                                        return [2, callback(null, false)];
                                    NimbusSDK_1.default.getApi().getFolders({ workspaceId: workspaceId }, function (err, folders) { return __awaiter(_this, void 0, void 0, function () {
                                        var checkUserFolderItem, checkUserFolderItems;
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    if (!err) return [3, 2];
                                                    return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                                case 1:
                                                    _a.sent();
                                                    errorHandler_1.default.log({
                                                        err: err, response: folders,
                                                        description: "Sync problem => downloadFolders => getFolders"
                                                    });
                                                    return [2, callback(err, false)];
                                                case 2:
                                                    checkUserFolderItem = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                        var _this = this;
                                                        return __generator(this, function (_a) {
                                                            return [2, new Promise(function (resolve) {
                                                                    var workspaceId = inputData.workspaceId, uniqueUserName = inputData.uniqueUserName, folder = inputData.folder;
                                                                    FolderObjRepository_1.default.get({ workspaceId: workspaceId, globalId: folder.global_id }, function (err, folderObj) { return __awaiter(_this, void 0, void 0, function () {
                                                                        var createUserFolderObj, getUserParentFolderObj, parentFolder;
                                                                        var _this = this;
                                                                        return __generator(this, function (_a) {
                                                                            switch (_a.label) {
                                                                                case 0:
                                                                                    if (err) {
                                                                                        errorHandler_1.default.log({
                                                                                            err: err, response: folderObj,
                                                                                            description: "Sync problem => downloadFolders => checkUserFolderItem",
                                                                                            data: {
                                                                                                uniqueUserName: uniqueUserName,
                                                                                                folder: folder
                                                                                            }
                                                                                        });
                                                                                        return [2, resolve(null)];
                                                                                    }
                                                                                    createUserFolderObj = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                                        return __generator(this, function (_a) {
                                                                                            return [2, new Promise(function (resolve) {
                                                                                                    var workspaceId = inputData.workspaceId, folder = inputData.folder;
                                                                                                    FolderObjRepository_1.default.create({
                                                                                                        workspaceId: workspaceId,
                                                                                                        folderName: folder.title,
                                                                                                        parentId: folder.parent_id,
                                                                                                        globalId: folder.global_id
                                                                                                    }, function (err, folderObj) {
                                                                                                        if (err) {
                                                                                                            errorHandler_1.default.log({
                                                                                                                err: err, response: folderObj,
                                                                                                                description: "Sync problem => downloadFolders => createUserFolderObj",
                                                                                                                data: inputData
                                                                                                            });
                                                                                                            return resolve(null);
                                                                                                        }
                                                                                                        resolve(folderObj);
                                                                                                    });
                                                                                                })];
                                                                                        });
                                                                                    }); };
                                                                                    if (!folderObj) return [3, 4];
                                                                                    if (!(folderObj.syncDate > folder.date_updated)) return [3, 1];
                                                                                    return [2, resolve(null)];
                                                                                case 1: return [4, createUserFolderObj({ workspaceId: workspaceId, folder: folder })];
                                                                                case 2:
                                                                                    folderObj = _a.sent();
                                                                                    _a.label = 3;
                                                                                case 3: return [3, 6];
                                                                                case 4: return [4, createUserFolderObj({ workspaceId: workspaceId, folder: folder })];
                                                                                case 5:
                                                                                    folderObj = _a.sent();
                                                                                    _a.label = 6;
                                                                                case 6:
                                                                                    if (!folderObj) {
                                                                                        return [2, resolve(null)];
                                                                                    }
                                                                                    getUserParentFolderObj = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                                        var _this = this;
                                                                                        return __generator(this, function (_a) {
                                                                                            return [2, new Promise(function (resolve) {
                                                                                                    FolderObjRepository_1.default.get(inputData, function (err, folderObj) { return __awaiter(_this, void 0, void 0, function () {
                                                                                                        return __generator(this, function (_a) {
                                                                                                            if (err) {
                                                                                                                errorHandler_1.default.log({
                                                                                                                    err: err, response: folderObj,
                                                                                                                    description: "Sync problem => downloadFolders => getUserParentFolderObj",
                                                                                                                    data: inputData
                                                                                                                });
                                                                                                                return [2, resolve(null)];
                                                                                                            }
                                                                                                            resolve(folderObj);
                                                                                                            return [2];
                                                                                                        });
                                                                                                    }); });
                                                                                                })];
                                                                                        });
                                                                                    }); };
                                                                                    return [4, getUserParentFolderObj({ workspaceId: workspaceId, globalId: folder.parent_id })];
                                                                                case 7:
                                                                                    parentFolder = _a.sent();
                                                                                    folderObj = FolderObj_1.default.setTitle(folderObj, folder.title);
                                                                                    folderObj.parentId = folder.parent_id;
                                                                                    folderObj.rootParentId = folder.root_parent_id;
                                                                                    if (folderObj.rootParentId && folderObj.parentId !== folderObj.rootParentId) {
                                                                                        folderObj.rootId = "trash";
                                                                                        folderObj.rootParentId = "trash";
                                                                                    }
                                                                                    if (parentFolder && parentFolder.rootId === "trash") {
                                                                                        folderObj.rootId = "trash";
                                                                                        folderObj.rootParentId = "trash";
                                                                                    }
                                                                                    folderObj.index = folder.index;
                                                                                    folderObj.type = folder.type;
                                                                                    folderObj.existOnServer = true;
                                                                                    folderObj.dateAdded = folder.date_added_user;
                                                                                    folderObj.dateUpdated = folder.date_updated_user;
                                                                                    folderObj.syncDate = folder.date_updated;
                                                                                    folderObj.uniqueUserName = uniqueUserName;
                                                                                    folderObj.onlyOffline = false;
                                                                                    folderObj.needSync = false;
                                                                                    folderObj.shared = !!folder.shared;
                                                                                    folderObj.color = folder.color;
                                                                                    resolve(folderObj);
                                                                                    return [2];
                                                                            }
                                                                        });
                                                                    }); });
                                                                })];
                                                        });
                                                    }); };
                                                    checkUserFolderItems = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                        var _this = this;
                                                        return __generator(this, function (_a) {
                                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                                    var workspaceId, uniqueUserName, folders, _i, folders_1, folder, folderObj;
                                                                    return __generator(this, function (_a) {
                                                                        switch (_a.label) {
                                                                            case 0:
                                                                                workspaceId = inputData.workspaceId, uniqueUserName = inputData.uniqueUserName, folders = inputData.folders;
                                                                                _i = 0, folders_1 = folders;
                                                                                _a.label = 1;
                                                                            case 1:
                                                                                if (!(_i < folders_1.length)) return [3, 5];
                                                                                folder = folders_1[_i];
                                                                                return [4, checkUserFolderItem({ workspaceId: workspaceId, uniqueUserName: uniqueUserName, folder: folder })];
                                                                            case 2:
                                                                                folderObj = _a.sent();
                                                                                if (!folderObj) return [3, 4];
                                                                                return [4, FolderObjRepository_1.default.update({ workspaceId: workspaceId, folderObjs: folderObj })];
                                                                            case 3:
                                                                                _a.sent();
                                                                                _a.label = 4;
                                                                            case 4:
                                                                                _i++;
                                                                                return [3, 1];
                                                                            case 5:
                                                                                resolve(true);
                                                                                return [2];
                                                                        }
                                                                    });
                                                                }); })];
                                                        });
                                                    }); };
                                                    if (!(folders && folders.length)) return [3, 4];
                                                    return [4, checkUserFolderItems({ workspaceId: workspaceId, uniqueUserName: uniqueUserName, folders: folders })];
                                                case 3:
                                                    _a.sent();
                                                    _a.label = 4;
                                                case 4:
                                                    callback(err, true);
                                                    return [2];
                                            }
                                        });
                                    }); });
                                    return [2];
                            }
                        });
                    }); });
                    return [2];
            }
        });
    });
}
exports.default = downloadFolders;
