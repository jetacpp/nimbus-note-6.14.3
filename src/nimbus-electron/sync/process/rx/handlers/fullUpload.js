"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var uploadUpdatedFullNotesOnServer_1 = __importDefault(require("./uploadUpdatedFullNotesOnServer"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
function fullUpload(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    var workspaceId = inputData.workspaceId;
    syncHandler_1.default.log("sync => fullUserUpload => start (ws: " + workspaceId + ")");
    uploadUpdatedFullNotesOnServer_1.default(inputData, callback);
}
exports.default = fullUpload;
