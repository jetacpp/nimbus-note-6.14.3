"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var selectedItem_1 = __importDefault(require("../../../../window/selectedItem"));
var PreviewSyncManager_1 = __importDefault(require("../../../nimbussdk/manager/PreviewSyncManager"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var SyncStatusDisplayEvent_1 = __importDefault(require("../../events/SyncStatusDisplayEvent"));
var AttachmentObjRepository_1 = __importDefault(require("../../repositories/AttachmentObjRepository"));
var AttachmentHubDownloader_1 = __importDefault(require("../../hub/AttachmentHubDownloader"));
var socketFunctions_1 = __importDefault(require("../../../socket/socketFunctions"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
var MAX_QUEUE_SIZE = 20;
function fullAttachmentsInListDownloadForDownloadedNotes(inputData, callback) {
    var _this = this;
    if (callback === void 0) { callback = function (err, res) {
    }; }
    var workspaceId = inputData.workspaceId;
    syncHandler_1.default.log("sync => fullAttachmentsInListDownloadForDownloadedNotes => start (ws: " + workspaceId + ")");
    AttachmentObjRepository_1.default.getNotDownloadedAttachmentFromDownloadedNotesForDownloadFromServer(inputData, function (err, attachments) { return __awaiter(_this, void 0, void 0, function () {
        var updateAttachmentDownloadCounter, downloadAttachmentItem, checkAttachmentNeedDownload, attachmentsNeedDownload, attachmentsNotNeedDownload, attachmentsLength, i, entity, downloadAttachmentsQueue, attachmentsNeedDownloadSort, totalNeedDownloadAttachments, attachmentsNotNeedDownloadLength, i, j;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (err) {
                        errorHandler_1.default.log({
                            err: err, response: attachments,
                            description: "Sync problem => fullAttachmentsInListDownloadForDownloadedNotes => getNotDownloadedAttachmentFromDownloadedNotesForDownloadFromServer",
                            data: inputData
                        });
                        return [2, callback(err, false)];
                    }
                    if (!attachments) {
                        return [2, callback(err, false)];
                    }
                    if (!attachments.length) {
                        return [2, callback(err, true)];
                    }
                    if (!attachments.length) return [3, 3];
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 1:
                    if (_a.sent())
                        return [2, false];
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOAD_ATTACHMENT
                        })];
                case 2:
                    _a.sent();
                    _a.label = 3;
                case 3:
                    updateAttachmentDownloadCounter = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    return __generator(this, function (_a) {
                                        AttachmentObjRepository_1.default.updateAttachmentDownloadCounter(inputData, function (err, response) {
                                            if (err) {
                                                errorHandler_1.default.log({
                                                    err: err, response: response,
                                                    description: "Sync problem => fullAttachmentsInListDownloadForDownloadedNotes => updateAttachmentDownloadCounter",
                                                    data: inputData
                                                });
                                                return resolve(false);
                                            }
                                            resolve(response);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    downloadAttachmentItem = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, entity, attachmentData, attachment_1, noteGlobalId;
                                    var _this = this;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                workspaceId = inputData.workspaceId, entity = inputData.entity;
                                                return [4, AttachmentHubDownloader_1.default.getInstance().download(inputData)];
                                            case 1:
                                                attachmentData = _a.sent();
                                                if (!(attachmentData && attachmentData.attachment)) return [3, 4];
                                                attachment_1 = attachmentData.attachment;
                                                if (!!attachmentData.downloaded) return [3, 3];
                                                return [4, AttachmentObjRepository_1.default.updateAttachmentDownloadCounter({ workspaceId: workspaceId, globalId: attachment_1.globalId })];
                                            case 2:
                                                _a.sent();
                                                return [2, resolve(false)];
                                            case 3:
                                                noteGlobalId = selectedItem_1.default.get({
                                                    workspaceId: workspaceId,
                                                    itemType: selectedItem_1.default.TYPE_NOTE
                                                });
                                                if (attachment_1.noteGlobalId === noteGlobalId) {
                                                    socketFunctions_1.default.sendNoteAttachmentsUpdateMessage({
                                                        workspaceId: workspaceId,
                                                        noteGlobalId: attachment_1.noteGlobalId,
                                                        globalId: attachment_1.globalId
                                                    });
                                                }
                                                PreviewSyncManager_1.default.updateNotePreview({
                                                    workspaceId: workspaceId,
                                                    globalId: attachment_1.noteGlobalId,
                                                    attachmentGlobalId: attachment_1.globalId
                                                });
                                                AttachmentObjRepository_1.default.updateAttachmentFromManualDownloadI({
                                                    workspaceId: workspaceId,
                                                    attachmentObj: attachment_1
                                                }, function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                                                    return __generator(this, function (_a) {
                                                        if (err) {
                                                            errorHandler_1.default.log({
                                                                err: err, response: response,
                                                                description: "Sync problem => fullAttachmentsInListDownloadForDownloadedNotes => updateAttachmentFromManualDownloadI",
                                                                data: {
                                                                    attachment: attachment_1
                                                                }
                                                            });
                                                            return [2, resolve(false)];
                                                        }
                                                        resolve(response);
                                                        return [2];
                                                    });
                                                }); });
                                                return [3, 6];
                                            case 4: return [4, AttachmentObjRepository_1.default.updateAttachmentDownloadCounter({ workspaceId: workspaceId, globalId: entity.global_id })];
                                            case 5:
                                                _a.sent();
                                                return [2, resolve(false)];
                                            case 6: return [2];
                                        }
                                    });
                                }); })];
                        });
                    }); };
                    checkAttachmentNeedDownload = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, attachment, entity;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                workspaceId = inputData.workspaceId, attachment = inputData.attachment;
                                                if (!attachment) {
                                                    errorHandler_1.default.log({
                                                        err: err, response: null,
                                                        description: "Sync problem => fullAttachmentsInListDownloadForDownloadedNotes => checkAttachmentNeedDownload",
                                                        data: inputData
                                                    });
                                                    return [2, resolve(false)];
                                                }
                                                return [4, AttachmentHubDownloader_1.default.getInstance().checkNeedDownload({
                                                        workspaceId: workspaceId,
                                                        globalId: attachment.globalId
                                                    })];
                                            case 1:
                                                entity = _a.sent();
                                                resolve(entity);
                                                return [2];
                                        }
                                    });
                                }); })];
                        });
                    }); };
                    attachmentsNeedDownload = [];
                    attachmentsNotNeedDownload = [];
                    attachmentsLength = attachments.length;
                    i = 0;
                    _a.label = 4;
                case 4:
                    if (!(i < attachmentsLength)) return [3, 7];
                    return [4, checkAttachmentNeedDownload({ workspaceId: workspaceId, attachment: attachments[i] })];
                case 5:
                    entity = _a.sent();
                    if (entity) {
                        attachmentsNeedDownload.push(entity);
                    }
                    else {
                        attachmentsNotNeedDownload.push(attachments[i]);
                    }
                    _a.label = 6;
                case 6:
                    i++;
                    return [3, 4];
                case 7:
                    attachments = null;
                    downloadAttachmentsQueue = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, attachmentsList, attachments, totalCount, downloadedCount, downloadingCount, downloadAttachmentQueueItem, checkAttachQueueInterval;
                                    var _this = this;
                                    return __generator(this, function (_a) {
                                        workspaceId = inputData.workspaceId, attachmentsList = inputData.attachmentsList;
                                        if (!attachmentsList) {
                                            return [2, resolve(true)];
                                        }
                                        if (!attachmentsList.length) {
                                            return [2, resolve(true)];
                                        }
                                        attachments = attachmentsList.slice();
                                        totalCount = attachments.length;
                                        downloadedCount = 0;
                                        downloadingCount = 0;
                                        downloadAttachmentQueueItem = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                            var workspaceId, attachment;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        workspaceId = inputData.workspaceId, attachment = inputData.attachment;
                                                        if (!attachment) {
                                                            ++downloadedCount;
                                                            return [2, false];
                                                        }
                                                        return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                    case 1:
                                                        if (_a.sent())
                                                            return [2, false];
                                                        return [4, downloadAttachmentItem({ workspaceId: workspaceId, entity: attachment })];
                                                    case 2:
                                                        _a.sent();
                                                        ++downloadedCount;
                                                        --downloadingCount;
                                                        return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                    case 3:
                                                        if (_a.sent())
                                                            return [2, false];
                                                        return [4, SyncStatusDisplayEvent_1.default.set({
                                                                workspaceId: workspaceId,
                                                                newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOAD_ATTACHMENT,
                                                                props: {
                                                                    "current": downloadedCount,
                                                                    "total": totalCount
                                                                }
                                                            })];
                                                    case 4:
                                                        _a.sent();
                                                        return [2, true];
                                                }
                                            });
                                        }); };
                                        checkAttachQueueInterval = setInterval(function () { return __awaiter(_this, void 0, void 0, function () {
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0: return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                    case 1:
                                                        if (_a.sent()) {
                                                            if (checkAttachQueueInterval) {
                                                                clearInterval(checkAttachQueueInterval);
                                                            }
                                                            return [2, resolve(false)];
                                                        }
                                                        if (downloadedCount >= totalCount) {
                                                            if (checkAttachQueueInterval) {
                                                                clearInterval(checkAttachQueueInterval);
                                                            }
                                                            return [2, resolve(true)];
                                                        }
                                                        if (attachments.length && downloadingCount < MAX_QUEUE_SIZE) {
                                                            ++downloadingCount;
                                                            downloadAttachmentQueueItem({ workspaceId: workspaceId, attachment: attachments.shift() });
                                                        }
                                                        return [2];
                                                }
                                            });
                                        }); }, 10);
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    attachmentsNeedDownloadSort = [];
                    totalNeedDownloadAttachments = attachmentsNeedDownload.length;
                    attachmentsNotNeedDownloadLength = attachmentsNotNeedDownload.length;
                    if (!(totalNeedDownloadAttachments || attachmentsNotNeedDownloadLength)) return [3, 10];
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 8:
                    if (_a.sent())
                        return [2, false];
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOAD_ATTACHMENT
                        })];
                case 9:
                    _a.sent();
                    _a.label = 10;
                case 10:
                    for (i = totalNeedDownloadAttachments - 1; i >= 0; i--) {
                        if (attachmentsNeedDownload[i].in_list) {
                            attachmentsNeedDownloadSort.push(attachmentsNeedDownload[i]);
                        }
                        else {
                            attachmentsNeedDownloadSort.unshift(attachmentsNeedDownload[i]);
                        }
                    }
                    attachmentsNeedDownload = null;
                    return [4, downloadAttachmentsQueue({ workspaceId: workspaceId, attachmentsList: attachmentsNeedDownloadSort })];
                case 11:
                    _a.sent();
                    j = 0;
                    _a.label = 12;
                case 12:
                    if (!(j < attachmentsNotNeedDownloadLength)) return [3, 16];
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 13:
                    if (_a.sent()) {
                        return [2, callback(err, false)];
                    }
                    return [4, AttachmentObjRepository_1.default.updateDownloadStatus({
                            workspaceId: workspaceId,
                            globalId: attachmentsNotNeedDownload[j].globalId
                        })];
                case 14:
                    _a.sent();
                    _a.label = 15;
                case 15:
                    j++;
                    return [3, 12];
                case 16:
                    callback(err, true);
                    return [2];
            }
        });
    }); });
}
exports.default = fullAttachmentsInListDownloadForDownloadedNotes;
