"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var SocketCollection_1 = __importDefault(require("../../../socket/store/SocketCollection"));
var socketStoreType_1 = __importDefault(require("../../../socket/socketStoreType"));
function sendSocketStoreMessages(inputData) {
    var workspaceId = inputData.workspaceId;
    if (!SocketCollection_1.default.needSendSocketCollection({ workspaceId: workspaceId })) {
        return;
    }
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.NOTES_FOR_UPDATE_COUNTERS });
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.NOTES_FOR_UPDATE });
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.NOTES_FOR_REMOVE });
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.NOTES_FOR_UPDATE_TEXT });
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.FOLDERS_FOR_UPDATE });
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.FOLDERS_FOR_REMOVE });
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.TODO_LIST_FOR_UPDATE });
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.TODO_LIST_FOR_REMOVE });
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.NOTES_TAGS_FOR_UPDATE });
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.TAGS_FOR_UPDATE });
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.NOTES_ATTACHMENTS_FOR_UPDATE });
    SocketCollection_1.default.sendSocketCollectedData({ workspaceId: workspaceId, storeType: socketStoreType_1.default.NOTES_ATTACHMENTS_FOR_REMOVE });
}
exports.default = sendSocketStoreMessages;
