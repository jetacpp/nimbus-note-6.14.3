"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fullDownload_1 = __importDefault(require("./fullDownload"));
function fullSync(inputData, callback) {
    if (callback === void 0) { callback = function () {
    }; }
    fullDownload_1.default(inputData, callback);
}
exports.default = fullSync;
