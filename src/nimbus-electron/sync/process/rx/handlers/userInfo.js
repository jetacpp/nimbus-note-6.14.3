"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var user_1 = __importDefault(require("../../../../db/models/user"));
var instance_1 = __importDefault(require("../../../../window/instance"));
var AvatarSingleDonwloader_1 = __importDefault(require("../../../downlaoder/AvatarSingleDonwloader"));
function userInfo(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, saveUserInfo;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId, saveUserInfo = inputData.saveUserInfo;
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 1:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    NimbusSDK_1.default.getApi().userInfo({ workspaceId: workspaceId }, function (err, userInstance) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!err) return [3, 2];
                                    return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                case 1:
                                    _a.sent();
                                    errorHandler_1.default.log({
                                        err: err, response: userInstance,
                                        description: "Sync problem => userInfo",
                                        data: inputData
                                    });
                                    return [2, callback(err, false)];
                                case 2:
                                    if (saveUserInfo && userInstance) {
                                        user_1.default.find({ email: userInstance.login }, {}, function (err, findUserInstance) { return __awaiter(_this, void 0, void 0, function () {
                                            var saveUserData_1, avatarFileExist;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        if (!(!err && findUserInstance)) return [3, 8];
                                                        saveUserData_1 = {};
                                                        if (typeof (userInstance.firstname) !== 'undefined') {
                                                            if (findUserInstance.firstname !== userInstance.firstname) {
                                                                saveUserData_1.firstname = userInstance.firstname;
                                                            }
                                                        }
                                                        if (typeof (userInstance.lastname) !== 'undefined') {
                                                            if (findUserInstance.lastname !== userInstance.lastname) {
                                                                saveUserData_1.lastname = userInstance.lastname;
                                                            }
                                                        }
                                                        if (typeof (userInstance.username) !== 'undefined') {
                                                            if (findUserInstance.username !== userInstance.username) {
                                                                saveUserData_1.username = userInstance.username;
                                                            }
                                                        }
                                                        if (!(typeof (userInstance.avatar) !== 'undefined')) return [3, 7];
                                                        if (!userInstance.avatar) return [3, 6];
                                                        if (!findUserInstance.avatar) return [3, 4];
                                                        if (!(findUserInstance.avatar.url !== userInstance.avatar.url)) return [3, 1];
                                                        saveUserData_1.avatar = userInstance.avatar;
                                                        return [3, 3];
                                                    case 1: return [4, AvatarSingleDonwloader_1.default.checkAvatarExist(userInstance.avatar.url)];
                                                    case 2:
                                                        avatarFileExist = _a.sent();
                                                        if (!avatarFileExist) {
                                                            saveUserData_1.avatar = userInstance.avatar;
                                                        }
                                                        _a.label = 3;
                                                    case 3: return [3, 5];
                                                    case 4:
                                                        saveUserData_1.avatar = userInstance.avatar;
                                                        _a.label = 5;
                                                    case 5: return [3, 7];
                                                    case 6:
                                                        saveUserData_1.avatar = null;
                                                        _a.label = 7;
                                                    case 7:
                                                        if (Object.keys(saveUserData_1).length) {
                                                            user_1.default.update({ email: userInstance.login }, saveUserData_1, { workspaceId: workspaceId }, function (err, res) {
                                                                if (!err && res) {
                                                                    if (typeof (saveUserData_1.firstname) !== 'undefined' || typeof (saveUserData_1.lastname) !== 'undefined' || typeof (saveUserData_1.username) !== 'undefined') {
                                                                        instance_1.default.get().webContents.send('event:client:me:name:update:response', {
                                                                            firstname: typeof (saveUserData_1.firstname) === "undefined" ? findUserInstance.firstname : saveUserData_1.firstname,
                                                                            lastname: typeof (saveUserData_1.lastname) === "undefined" ? findUserInstance.lastname : saveUserData_1.lastname,
                                                                            username: typeof (saveUserData_1.username) === "undefined" ? findUserInstance.username : saveUserData_1.username
                                                                        });
                                                                    }
                                                                    if (typeof (saveUserData_1.avatar) !== 'undefined') {
                                                                        if (saveUserData_1.avatar) {
                                                                            AvatarSingleDonwloader_1.default.download({
                                                                                url: saveUserData_1.avatar.url,
                                                                                oldUrl: findUserInstance.avatar ? findUserInstance.avatar.url : '',
                                                                                type: AvatarSingleDonwloader_1.default.AVATAR_TYPE_USER,
                                                                            });
                                                                        }
                                                                        else {
                                                                            if (findUserInstance.avatar) {
                                                                                instance_1.default.get().webContents.send('event:client:me:avatar:remove:response', {});
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        _a.label = 8;
                                                    case 8: return [2];
                                                }
                                            });
                                        }); });
                                    }
                                    callback(err, userInstance);
                                    return [2];
                            }
                        });
                    }); });
                    return [2];
            }
        });
    });
}
exports.default = userInfo;
