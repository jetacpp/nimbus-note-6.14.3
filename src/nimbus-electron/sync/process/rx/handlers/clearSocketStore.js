"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var SocketCollection_1 = __importDefault(require("../../../socket/store/SocketCollection"));
function clearSocketStore(inputData) {
    SocketCollection_1.default.flushSocketCollectedData(inputData);
}
exports.default = clearSocketStore;
