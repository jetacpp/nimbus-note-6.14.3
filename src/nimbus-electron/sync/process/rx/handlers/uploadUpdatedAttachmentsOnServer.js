"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../../config"));
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var SyncStatusDisplayEvent_1 = __importDefault(require("../../events/SyncStatusDisplayEvent"));
var NoteIsMoreThanLimitException_1 = __importDefault(require("../../exceptions/NoteIsMoreThanLimitException"));
var AttachmentObjRepository_1 = __importDefault(require("../../repositories/AttachmentObjRepository"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var pdb_1 = __importDefault(require("../../../../../pdb"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
var fs_extra_1 = __importDefault(require("fs-extra"));
function uploadUpdatedAttachmentsOnServer(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, noteGlobalId, getNoteAttachmentForUploadOnServer, processUploadResponse, attachments, uploadedAttachmentsList, _i, attachments_1, attachmentObj, attachmentEntity;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
                    getNoteAttachmentForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    AttachmentObjRepository_1.default.getNoteAttachmentForUploadOnServer(inputData, function (err, attachments) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: attachments,
                                                description: "Sync problem => uploadUpdatedAttachmentsOnServer => getNoteAttachmentForUploadOnServer",
                                                data: inputData
                                            });
                                            return resolve([]);
                                        }
                                        resolve(attachments);
                                    });
                                })];
                        });
                    }); };
                    processUploadResponse = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, attachmentObj, checkIfAttachmentNotMoreThanLimitAttachmentSize, filesPreupload, updateTempNameAfterUploadOnServerI, tempname, extension, mime, fileExist, attachmentNotMoreThanLimit, uploadedName, updateResult;
                                    var _this = this;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                workspaceId = inputData.workspaceId, attachmentObj = inputData.attachmentObj;
                                                checkIfAttachmentNotMoreThanLimitAttachmentSize = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                    return __generator(this, function (_a) {
                                                        return [2, new Promise(function (resolve) {
                                                                AttachmentObjRepository_1.default.checkIfAttachmentNotMoreThanLimitAttachmentSize(inputData, function (err, attachmentNotMoreThanLimit) {
                                                                    if (err) {
                                                                        errorHandler_1.default.log({
                                                                            err: err, response: attachmentNotMoreThanLimit,
                                                                            description: "Sync problem => uploadUpdatedAttachmentsOnServer => checkIfAttachmentNotMoreThanLimitAttachmentSize",
                                                                            data: inputData
                                                                        });
                                                                        return resolve(false);
                                                                    }
                                                                    resolve(attachmentNotMoreThanLimit);
                                                                });
                                                            })];
                                                    });
                                                }); };
                                                filesPreupload = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                    var _this = this;
                                                    return __generator(this, function (_a) {
                                                        return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                                var workspaceId;
                                                                var _this = this;
                                                                return __generator(this, function (_a) {
                                                                    switch (_a.label) {
                                                                        case 0:
                                                                            workspaceId = inputData.workspaceId;
                                                                            return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                                        case 1:
                                                                            if (_a.sent())
                                                                                return [2, resolve(false)];
                                                                            NimbusSDK_1.default.getApi().filesPreupload(inputData, function (err, tempName) { return __awaiter(_this, void 0, void 0, function () {
                                                                                return __generator(this, function (_a) {
                                                                                    if (err || !tempName) {
                                                                                        syncHandler_1.default.log({
                                                                                            err: err, response: tempName,
                                                                                            description: "Sync problem => uploadUpdatedAttachmentsOnServer => filesPreupload",
                                                                                            data: inputData
                                                                                        });
                                                                                        return [2, resolve(null)];
                                                                                    }
                                                                                    resolve(tempName);
                                                                                    return [2];
                                                                                });
                                                                            }); });
                                                                            return [2];
                                                                    }
                                                                });
                                                            }); })];
                                                    });
                                                }); };
                                                updateTempNameAfterUploadOnServerI = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                    return __generator(this, function (_a) {
                                                        return [2, new Promise(function (resolve) {
                                                                AttachmentObjRepository_1.default.updateTempNameAfterUploadOnServerI(inputData, function (err) {
                                                                    if (err) {
                                                                        errorHandler_1.default.log({
                                                                            err: err, response: null,
                                                                            description: "Sync problem => uploadUpdatedAttachmentsOnServer => updateTempNameAfterUploadOnServerI",
                                                                            data: inputData
                                                                        });
                                                                        return resolve(null);
                                                                    }
                                                                    resolve(true);
                                                                });
                                                            })];
                                                    });
                                                }); };
                                                tempname = pdb_1.default.getClientAttachmentPath() + "/" + attachmentObj.storedFileUUID;
                                                extension = attachmentObj.extension;
                                                mime = attachmentObj.mime || '';
                                                return [4, fs_extra_1.default.exists(tempname)];
                                            case 1:
                                                fileExist = _a.sent();
                                                if (!!fileExist) return [3, 3];
                                                return [4, AttachmentObjRepository_1.default.eraseAttachmentFile({
                                                        workspaceId: inputData.workspaceId,
                                                        globalId: attachmentObj.globalId,
                                                    })];
                                            case 2:
                                                _a.sent();
                                                return [2, resolve(null)];
                                            case 3: return [4, checkIfAttachmentNotMoreThanLimitAttachmentSize({
                                                    workspaceId: workspaceId,
                                                    globalId: attachmentObj.globalId
                                                })];
                                            case 4:
                                                attachmentNotMoreThanLimit = _a.sent();
                                                if (!attachmentNotMoreThanLimit) {
                                                    if (config_1.default.SHOW_WEB_CONSOLE) {
                                                        console.log("NoteIsMoreThanLimitException: " + NoteIsMoreThanLimitException_1.default.TYPE.ATTACHMENT + " " + attachmentObj.globalId + " " + attachmentObj.parentId + " (ws: " + workspaceId + ")");
                                                    }
                                                    return [2, resolve(null)];
                                                }
                                                if (!attachmentObj.tempName) return [3, 5];
                                                if (config_1.default.SHOW_WEB_CONSOLE) {
                                                    console.log("Uploading attach already presented on sync server: " + attachmentObj.globalId + " " + attachmentObj.tempName + " (ws: " + workspaceId + ")");
                                                }
                                                return [3, 8];
                                            case 5: return [4, filesPreupload({ workspaceId: workspaceId, tempname: tempname, extension: extension, mime: mime })];
                                            case 6:
                                                uploadedName = _a.sent();
                                                if (!uploadedName) {
                                                    return [2, resolve(null)];
                                                }
                                                attachmentObj.tempName = uploadedName;
                                                return [4, updateTempNameAfterUploadOnServerI({
                                                        workspaceId: workspaceId,
                                                        globalId: attachmentObj.globalId,
                                                        tempName: attachmentObj.tempName
                                                    })];
                                            case 7:
                                                updateResult = _a.sent();
                                                if (!updateResult) {
                                                    return [2, resolve(null)];
                                                }
                                                _a.label = 8;
                                            case 8:
                                                AttachmentObjRepository_1.default.convertToSyncAttachmentEntity(attachmentObj, function (err, attachmentEntity) {
                                                    if (err || !attachmentEntity) {
                                                        return resolve(null);
                                                    }
                                                    resolve(attachmentEntity);
                                                });
                                                return [2];
                                        }
                                    });
                                }); })];
                        });
                    }); };
                    return [4, getNoteAttachmentForUploadOnServer({ workspaceId: workspaceId, noteGlobalId: noteGlobalId })];
                case 1:
                    attachments = _a.sent();
                    if (attachments.length) {
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("Start upload updated attaches on server for note: " + noteGlobalId + " (ws: " + workspaceId + ")");
                            console.log("Upload attaches count: " + attachments.length + " (ws: " + workspaceId + ")");
                        }
                    }
                    uploadedAttachmentsList = [];
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 2:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    if (!attachments.length) return [3, 4];
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.UPLOAD_ATTACHMENTS
                        })];
                case 3:
                    _a.sent();
                    _a.label = 4;
                case 4:
                    _i = 0, attachments_1 = attachments;
                    _a.label = 5;
                case 5:
                    if (!(_i < attachments_1.length)) return [3, 9];
                    attachmentObj = attachments_1[_i];
                    if (attachmentObj.location) {
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("No need to upload attach, just renamed: " + attachmentObj.globalId + " for note " + noteGlobalId + " (ws: " + workspaceId + ")");
                        }
                        return [3, 8];
                    }
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 6:
                    if (_a.sent()) {
                        return [3, 9];
                    }
                    return [4, processUploadResponse({ workspaceId: workspaceId, attachmentObj: attachmentObj })];
                case 7:
                    attachmentEntity = _a.sent();
                    if (attachmentEntity) {
                        uploadedAttachmentsList.push(attachmentEntity);
                    }
                    else {
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            syncHandler_1.default.log("Attach upload fail: " + attachmentObj.globalId + " for note " + noteGlobalId + " (ws: " + workspaceId + ")");
                        }
                    }
                    _a.label = 8;
                case 8:
                    _i++;
                    return [3, 5];
                case 9:
                    callback(null, uploadedAttachmentsList);
                    return [2];
            }
        });
    });
}
exports.default = uploadUpdatedAttachmentsOnServer;
