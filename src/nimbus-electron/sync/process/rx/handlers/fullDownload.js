"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var headerSync_1 = __importDefault(require("./headerSync"));
var socketStoreType_1 = __importDefault(require("../../../socket/socketStoreType"));
var PreviewSyncManager_1 = __importDefault(require("../../../nimbussdk/manager/PreviewSyncManager"));
var LimitsPopup_1 = __importDefault(require("../../../ipc/LimitsPopup"));
var user_1 = __importDefault(require("../../../../db/models/user"));
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var NoteObjRepository_1 = __importDefault(require("../../repositories/NoteObjRepository"));
var fullAttachmentsInListDownloadForDownloadedNotes_1 = __importDefault(require("./fullAttachmentsInListDownloadForDownloadedNotes"));
var sendSocketStoreMessages_1 = __importDefault(require("./sendSocketStoreMessages"));
var reloadCurrentNoteContent_1 = __importDefault(require("./reloadCurrentNoteContent"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var downloadOrganizations_1 = __importDefault(require("./downloadOrganizations"));
var downloadWorkspaces_1 = __importDefault(require("./downloadWorkspaces"));
var getTrial_1 = __importDefault(require("./getTrial"));
var userInfo_1 = __importDefault(require("./userInfo"));
var userVariables_1 = __importDefault(require("./userVariables"));
var SyncStatusDisplayEvent_1 = __importDefault(require("../../events/SyncStatusDisplayEvent"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
function fullDownload(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, manualSync, downloadUserInfo, downloadUserVariables, downloadUserTrial, downloadUserOrganizations, downloadUserWorkspaces, userVariablesList, userInstance, _a, orgTraffic, totalTrafficAfterSync, accountManager, isPremiumActive, trafficMax, _b;
        var _this = this;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    workspaceId = inputData.workspaceId, manualSync = inputData.manualSync;
                    syncHandler_1.default.log("sync => fullDownload => start (ws: " + workspaceId + ")");
                    PreviewSyncManager_1.default.clear(inputData);
                    downloadUserInfo = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    userInfo_1.default(__assign(__assign({}, inputData), { saveUserInfo: true }), function (err, responseUserInfo) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: responseUserInfo,
                                                description: "Sync problem => fullDownload => downloadUserInfo",
                                                data: inputData
                                            });
                                            return resolve(null);
                                        }
                                        resolve(responseUserInfo);
                                    });
                                })];
                        });
                    }); };
                    downloadUserVariables = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    userVariables_1.default(__assign(__assign({}, inputData), { saveUserVariables: true }), function (err, responseUserVariables) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: responseUserVariables,
                                                description: "Sync problem => fullDownload => downloadUserVariables",
                                                data: inputData
                                            });
                                            return resolve(null);
                                        }
                                        resolve(responseUserVariables);
                                    });
                                })];
                        });
                    }); };
                    downloadUserTrial = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    getTrial_1.default(__assign(__assign({}, inputData), { saveUserTrial: true, skipSyncHandlers: true }), function (err, responseUserTrial) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: responseUserTrial,
                                                description: "Sync problem => fullDownload => getUserTrial",
                                                data: inputData
                                            });
                                            return resolve(null);
                                        }
                                        resolve(responseUserTrial);
                                    });
                                })];
                        });
                    }); };
                    downloadUserOrganizations = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    downloadOrganizations_1.default(inputData, function (err, responseOrganizations) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: responseOrganizations,
                                                description: "Sync problem => fullDownload => downloadUserOrganizations",
                                                data: inputData
                                            });
                                            return resolve(null);
                                        }
                                        resolve(responseOrganizations);
                                    });
                                })];
                        });
                    }); };
                    downloadUserWorkspaces = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    downloadWorkspaces_1.default(inputData, function (err, responseWorkspaces) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: responseWorkspaces,
                                                description: "Sync problem => fullDownload => downloadUserWorkspaces",
                                                data: inputData
                                            });
                                            return resolve(null);
                                        }
                                        resolve(responseWorkspaces);
                                    });
                                })];
                        });
                    }); };
                    syncHandler_1.default.log("sync => fastDownload => start (ws: " + workspaceId + ")");
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOADING_META_INFO
                        })];
                case 1:
                    _c.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 2:
                    if (_c.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => fastDownload => downloadUserVariables (ws: " + workspaceId + ")");
                    return [4, downloadUserVariables(inputData)];
                case 3:
                    userVariablesList = _c.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 4:
                    if (_c.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => fastDownload => downloadUserInfo (ws: " + workspaceId + ")");
                    return [4, downloadUserInfo(inputData)];
                case 5:
                    userInstance = _c.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 6:
                    if (_c.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => fastDownload => downloadUserTrial (ws: " + workspaceId + ")");
                    return [4, downloadUserTrial(__assign(__assign({}, inputData), { userInstance: userInstance }))];
                case 7:
                    _c.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 8:
                    if (_c.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => fastDownload => downloadUserOrganizations (ws: " + workspaceId + ")");
                    return [4, downloadUserOrganizations(inputData)];
                case 9:
                    _c.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 10:
                    if (_c.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => fastDownload => downloadUserWorkspaces (ws: " + workspaceId + ")");
                    return [4, downloadUserWorkspaces(inputData)];
                case 11:
                    _c.sent();
                    syncHandler_1.default.log("sync => fastDownload => getTotalTrafficAfterSync (ws: " + workspaceId + ")");
                    return [4, NoteObjRepository_1.default.getTotalTrafficAfterSync(inputData)];
                case 12:
                    _a = _c.sent(), orgTraffic = _a.orgTraffic, totalTrafficAfterSync = _a.totalTrafficAfterSync;
                    if (!(totalTrafficAfterSync && totalTrafficAfterSync >= user_1.default.WARNING_UPLOAD_SIZE)) return [3, 18];
                    accountManager = NimbusSDK_1.default.getAccountManager();
                    return [4, accountManager.isPremiumActiveAsync()];
                case 13:
                    isPremiumActive = _c.sent();
                    if (!orgTraffic) return [3, 14];
                    _b = orgTraffic.max;
                    return [3, 16];
                case 14: return [4, accountManager.getTrafficMaxAsync()];
                case 15:
                    _b = _c.sent();
                    _c.label = 16;
                case 16:
                    trafficMax = _b;
                    LimitsPopup_1.default.check({
                        workspaceId: workspaceId,
                        manualSync: manualSync,
                        data: {
                            objectSize: totalTrafficAfterSync,
                            limitSize: trafficMax,
                            isPremiumActive: isPremiumActive
                        }
                    });
                    if (!(totalTrafficAfterSync >= trafficMax)) return [3, 18];
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.TRAFFIC_LIMIT
                        })];
                case 17:
                    _c.sent();
                    errorHandler_1.default.log({
                        err: null, response: null,
                        description: "Sync problem => fullDownload => (totalTrafficAfterSync >= trafficMax)",
                        data: {
                            totalTrafficAfterSync: totalTrafficAfterSync,
                            trafficMax: trafficMax
                        }
                    });
                    return [2, callback(null, false)];
                case 18: return [4, headerSync_1.default(inputData, function () { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                case 1:
                                    if (_a.sent())
                                        return [2, callback(null, false)];
                                    fullAttachmentsInListDownloadForDownloadedNotes_1.default(inputData, function (err) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    if (err) {
                                                        errorHandler_1.default.log({
                                                            err: err, response: null,
                                                            description: "Sync problem => fullDownload => headerSync"
                                                        });
                                                    }
                                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                case 1:
                                                    if (_a.sent())
                                                        return [2, callback(null, false)];
                                                    syncHandler_1.default.log("sync => reloadCurrentNoteContent (ws: " + workspaceId + ")");
                                                    return [4, reloadCurrentNoteContent_1.default({ workspaceId: workspaceId, storeType: socketStoreType_1.default.NOTES_ATTACHMENTS_FOR_UPDATE })];
                                                case 2:
                                                    _a.sent();
                                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                case 3:
                                                    if (_a.sent())
                                                        return [2, callback(null, false)];
                                                    syncHandler_1.default.log("sync => headerSync => sendSocketStoreMessages (ws: " + workspaceId + ")");
                                                    sendSocketStoreMessages_1.default(inputData);
                                                    PreviewSyncManager_1.default.clear(inputData);
                                                    callback(err, true);
                                                    return [2];
                                            }
                                        });
                                    }); });
                                    return [2];
                            }
                        });
                    }); })];
                case 19:
                    _c.sent();
                    return [2];
            }
        });
    });
}
exports.default = fullDownload;
