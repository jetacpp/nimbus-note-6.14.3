"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var sessionLastUpdateTime_1 = __importDefault(require("./sessionLastUpdateTime"));
var NotesUpdateRequest_1 = __importDefault(require("../../../nimbussdk/net/request/NotesUpdateRequest"));
var FolderObjRepository_1 = __importDefault(require("../../repositories/FolderObjRepository"));
var NoteObjRepository_1 = __importDefault(require("../../repositories/NoteObjRepository"));
var AttachmentObjRepository_1 = __importDefault(require("../../repositories/AttachmentObjRepository"));
var TagObjRepository_1 = __importDefault(require("../../repositories/TagObjRepository"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
function uploadErasedFromTrashItems(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, getErasedFromTrashFoldersForUploadOnServer, getErasedFromTrashNotesForUploadOnServer, getErasedAttachmentsForUploadOnServer, getErasedTagsForUploadOnServer, getUpdatedHeaderNotesForUploadOnServer, removedFolders, removedNotes, removedAttachments, removeTags, storeNotes, makeRequest, body;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId;
                    getErasedFromTrashFoldersForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    FolderObjRepository_1.default.getErasedFromTrashFoldersForUploadOnServer(inputData, function (err, removedFolders) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: removedFolders,
                                                description: "Sync problem => uploadErasedFromTrashItems => getErasedFromTrashFoldersForUploadOnServer",
                                                data: inputData
                                            });
                                            return resolve([]);
                                        }
                                        resolve(removedFolders);
                                    });
                                })];
                        });
                    }); };
                    getErasedFromTrashNotesForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    NoteObjRepository_1.default.getErasedFromTrashNotesForUploadOnServer(inputData, function (err, removedNotes) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: removedNotes,
                                                description: "Sync problem => uploadErasedFromTrashItems => getErasedFromTrashNotesForUploadOnServer",
                                                data: inputData
                                            });
                                            return resolve([]);
                                        }
                                        resolve(removedNotes);
                                    });
                                })];
                        });
                    }); };
                    getErasedAttachmentsForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    AttachmentObjRepository_1.default.getErasedAttachmentsForUploadOnServer(inputData, function (err, removedAttachments) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: removedAttachments,
                                                description: "Sync problem => uploadErasedFromTrashItems => getErasedAttachmentsForUploadOnServer",
                                                data: inputData
                                            });
                                            return resolve([]);
                                        }
                                        resolve(removedAttachments);
                                    });
                                })];
                        });
                    }); };
                    getErasedTagsForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    TagObjRepository_1.default.getErasedTagsForUploadOnServer(inputData, function (err, removeTags) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: removeTags,
                                                description: "Sync problem => uploadErasedFromTrashItems => getErasedTagsForUploadOnServer",
                                                data: inputData
                                            });
                                            return resolve([]);
                                        }
                                        resolve(removeTags);
                                    });
                                })];
                        });
                    }); };
                    getUpdatedHeaderNotesForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    NoteObjRepository_1.default.getUpdatedHeaderNotesForUploadOnServer(inputData, function (err, storeNotes) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: storeNotes,
                                                description: "Sync problem => uploadErasedFromTrashItems => getUpdatedHeaderNotesForUploadOnServer",
                                                data: inputData
                                            });
                                            return resolve([]);
                                        }
                                        resolve(storeNotes);
                                    });
                                })];
                        });
                    }); };
                    return [4, getErasedFromTrashFoldersForUploadOnServer(inputData)];
                case 1:
                    removedFolders = _a.sent();
                    return [4, getErasedFromTrashNotesForUploadOnServer(inputData)];
                case 2:
                    removedNotes = _a.sent();
                    return [4, getErasedAttachmentsForUploadOnServer(inputData)];
                case 3:
                    removedAttachments = _a.sent();
                    return [4, getErasedTagsForUploadOnServer(inputData)];
                case 4:
                    removeTags = _a.sent();
                    return [4, getUpdatedHeaderNotesForUploadOnServer(inputData)];
                case 5:
                    storeNotes = _a.sent();
                    makeRequest = removedFolders.length || removedNotes.length || removedAttachments.length || removeTags.length || storeNotes.length;
                    if (!makeRequest) {
                        return [2, callback(null, true)];
                    }
                    body = new NotesUpdateRequest_1.default.Body({});
                    body.removeFolders(removedFolders);
                    body.removeNotes(removedNotes);
                    body.removeAttachements(removedAttachments);
                    body.removeTags(removeTags);
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 6:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    NimbusSDK_1.default.getApi().updateNotes({ workspaceId: workspaceId, body: body }, function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                        var processUploadResponse;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!err) return [3, 2];
                                    return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                case 1:
                                    _a.sent();
                                    errorHandler_1.default.log({
                                        err: err, response: response,
                                        description: "Sync problem => uploadErasedFromTrashItems => updateNotes",
                                        data: {
                                            body: body
                                        }
                                    });
                                    return [2, callback(err, false)];
                                case 2:
                                    processUploadResponse = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var callAfterUploadErasedFromTrashFoldersOnServerI, callAfterUploadErasedFromTrashNotesOnServerI, callAfterUploadUpdatedHeaderNotesOnServerI, callAfterUploadErasedAttachmentsOnServerI, callAfterUploadErasedTagsOnServerI;
                                                    var _this = this;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                callAfterUploadErasedFromTrashFoldersOnServerI = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                    return __generator(this, function (_a) {
                                                                        return [2, new Promise(function (resolve) {
                                                                                FolderObjRepository_1.default.callAfterUploadErasedFromTrashFoldersOnServerI(inputData, function (err, response) {
                                                                                    if (err) {
                                                                                        errorHandler_1.default.log({
                                                                                            err: err, response: response,
                                                                                            description: "Sync problem => uploadErasedFromTrashItems => processUploadResponse",
                                                                                            data: inputData
                                                                                        });
                                                                                        return resolve(null);
                                                                                    }
                                                                                    resolve(response);
                                                                                });
                                                                            })];
                                                                    });
                                                                }); };
                                                                callAfterUploadErasedFromTrashNotesOnServerI = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                    return __generator(this, function (_a) {
                                                                        return [2, new Promise(function (resolve) {
                                                                                NoteObjRepository_1.default.callAfterUploadErasedFromTrashNotesOnServerI(inputData, function (err, response) {
                                                                                    if (err) {
                                                                                        errorHandler_1.default.log({
                                                                                            err: err, response: response,
                                                                                            description: "Sync problem => uploadErasedFromTrashItems => callAfterUploadErasedFromTrashNotesOnServerI",
                                                                                            data: inputData
                                                                                        });
                                                                                        return resolve(null);
                                                                                    }
                                                                                    resolve(response);
                                                                                });
                                                                            })];
                                                                    });
                                                                }); };
                                                                callAfterUploadUpdatedHeaderNotesOnServerI = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                    return __generator(this, function (_a) {
                                                                        return [2, new Promise(function (resolve) {
                                                                                NoteObjRepository_1.default.callAfterUploadUpdatedHeaderNotesOnServerI(inputData, function (err, response) {
                                                                                    if (err) {
                                                                                        errorHandler_1.default.log({
                                                                                            err: err, response: response,
                                                                                            description: "Sync problem => uploadErasedFromTrashItems => callAfterUploadUpdatedHeaderNotesOnServerI",
                                                                                            data: inputData
                                                                                        });
                                                                                        return resolve(null);
                                                                                    }
                                                                                    resolve(response);
                                                                                });
                                                                            })];
                                                                    });
                                                                }); };
                                                                callAfterUploadErasedAttachmentsOnServerI = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                    return __generator(this, function (_a) {
                                                                        return [2, new Promise(function (resolve) {
                                                                                AttachmentObjRepository_1.default.callAfterUploadErasedAttachmentsOnServerI(inputData, function (err, response) {
                                                                                    if (err) {
                                                                                        errorHandler_1.default.log({
                                                                                            err: err, response: response,
                                                                                            description: "Sync problem => uploadErasedFromTrashItems => callAfterUploadErasedAttachmentsOnServerI",
                                                                                            data: inputData
                                                                                        });
                                                                                        return resolve(null);
                                                                                    }
                                                                                    resolve(response);
                                                                                });
                                                                            })];
                                                                    });
                                                                }); };
                                                                callAfterUploadErasedTagsOnServerI = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                    return __generator(this, function (_a) {
                                                                        return [2, new Promise(function (resolve) {
                                                                                TagObjRepository_1.default.callAfterUploadErasedTagsOnServerI(inputData, function (err, response) {
                                                                                    if (err) {
                                                                                        errorHandler_1.default.log({
                                                                                            err: err, response: response,
                                                                                            description: "Sync problem => uploadErasedFromTrashItems => callAfterUploadErasedTagsOnServerI",
                                                                                            data: inputData
                                                                                        });
                                                                                        return resolve(null);
                                                                                    }
                                                                                    resolve(response);
                                                                                });
                                                                            })];
                                                                    });
                                                                }); };
                                                                return [4, callAfterUploadErasedFromTrashFoldersOnServerI(inputData)];
                                                            case 1:
                                                                _a.sent();
                                                                return [4, callAfterUploadErasedFromTrashNotesOnServerI(inputData)];
                                                            case 2:
                                                                _a.sent();
                                                                return [4, callAfterUploadErasedAttachmentsOnServerI(inputData)];
                                                            case 3:
                                                                _a.sent();
                                                                return [4, callAfterUploadErasedTagsOnServerI(inputData)];
                                                            case 4:
                                                                _a.sent();
                                                                resolve(true);
                                                                return [2];
                                                        }
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    return [4, processUploadResponse({ workspaceId: workspaceId, response: response })];
                                case 3:
                                    _a.sent();
                                    if (response && response.last_update_time) {
                                        sessionLastUpdateTime_1.default.setSyncSessionUpdateTime({ workspaceId: workspaceId, lastUpdateTime: response.last_update_time });
                                    }
                                    else {
                                        errorHandler_1.default.log({
                                            err: err, response: response,
                                            description: "Sync problem => uploadErasedFromTrashItems"
                                        });
                                    }
                                    callback(err, true);
                                    return [2];
                            }
                        });
                    }); });
                    return [2];
            }
        });
    });
}
exports.default = uploadErasedFromTrashItems;
