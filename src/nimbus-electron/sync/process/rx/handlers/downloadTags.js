"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var TagObjRepository_1 = __importDefault(require("../../repositories/TagObjRepository"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
function downloadTags(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId;
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 1:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    NimbusSDK_1.default.getApi().getTags({ workspaceId: workspaceId }, function (err, tags) { return __awaiter(_this, void 0, void 0, function () {
                        var checkUserTagItem, checkUserTagItems;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!err) return [3, 2];
                                    return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                case 1:
                                    _a.sent();
                                    errorHandler_1.default.log({
                                        err: err, response: tags,
                                        description: "Sync problem => downloadTags => getTags"
                                    });
                                    return [2, callback(err, false)];
                                case 2:
                                    checkUserTagItem = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    TagObjRepository_1.default.checkIfTagRenamed(inputData, function (err, tagObj) { return __awaiter(_this, void 0, void 0, function () {
                                                        var createUserTag, checkUserTagRemoved, isUserTagRemoved;
                                                        var _this = this;
                                                        return __generator(this, function (_a) {
                                                            switch (_a.label) {
                                                                case 0:
                                                                    if (err) {
                                                                        errorHandler_1.default.log({
                                                                            err: err, response: tagObj,
                                                                            description: "Sync problem => downloadTags => checkUserTagItem",
                                                                            data: inputData
                                                                        });
                                                                        return [2, resolve(null)];
                                                                    }
                                                                    createUserTag = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                        return __generator(this, function (_a) {
                                                                            return [2, new Promise(function (resolve) {
                                                                                    TagObjRepository_1.default.create(inputData, function (err, tagObj) {
                                                                                        if (err) {
                                                                                            errorHandler_1.default.log({
                                                                                                err: err, response: tagObj,
                                                                                                description: "Sync problem => downloadTags => createUserTag",
                                                                                                data: inputData
                                                                                            });
                                                                                            return resolve(null);
                                                                                        }
                                                                                        resolve(tagObj);
                                                                                    });
                                                                                })];
                                                                        });
                                                                    }); };
                                                                    checkUserTagRemoved = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                        return __generator(this, function (_a) {
                                                                            return [2, new Promise(function (resolve) {
                                                                                    TagObjRepository_1.default.getErasedTagForUploadOnServer(inputData, function (err, tagObj) {
                                                                                        if (err) {
                                                                                            errorHandler_1.default.log({
                                                                                                err: err, response: tagObj,
                                                                                                description: "Sync problem => downloadTags => checkUserTagRemoved",
                                                                                                data: inputData
                                                                                            });
                                                                                            return resolve(false);
                                                                                        }
                                                                                        resolve(tagObj);
                                                                                    });
                                                                                })];
                                                                        });
                                                                    }); };
                                                                    if (!!tagObj) return [3, 3];
                                                                    return [4, checkUserTagRemoved(inputData)];
                                                                case 1:
                                                                    isUserTagRemoved = _a.sent();
                                                                    if (!!isUserTagRemoved) return [3, 3];
                                                                    return [4, createUserTag(inputData)];
                                                                case 2:
                                                                    tagObj = _a.sent();
                                                                    _a.label = 3;
                                                                case 3:
                                                                    if (!tagObj) {
                                                                        errorHandler_1.default.log({
                                                                            err: err, response: tagObj,
                                                                            description: "Sync problem => downloadTags => !tagObj",
                                                                            data: inputData
                                                                        });
                                                                        return [2, resolve(null)];
                                                                    }
                                                                    tagObj.needSync = false;
                                                                    resolve(tagObj);
                                                                    return [2];
                                                            }
                                                        });
                                                    }); });
                                                })];
                                        });
                                    }); };
                                    checkUserTagItems = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var workspaceId, tags, tagObjs, _i, tags_1, tag;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                workspaceId = inputData.workspaceId, tags = inputData.tags;
                                                                tagObjs = [];
                                                                _i = 0, tags_1 = tags;
                                                                _a.label = 1;
                                                            case 1:
                                                                if (!(_i < tags_1.length)) return [3, 4];
                                                                tag = tags_1[_i];
                                                                return [4, checkUserTagItem({ workspaceId: workspaceId, tag: tag })];
                                                            case 2:
                                                                tag = _a.sent();
                                                                if (tag) {
                                                                    tagObjs.push(tag);
                                                                }
                                                                _a.label = 3;
                                                            case 3:
                                                                _i++;
                                                                return [3, 1];
                                                            case 4:
                                                                resolve(tagObjs);
                                                                return [2];
                                                        }
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    TagObjRepository_1.default.getAllTagsR(inputData, function (err, tagList) { return __awaiter(_this, void 0, void 0, function () {
                                        var removeTagsList, _i, tagList_1, tagItem, _a, removeTagsList_1, removeTag, tagObjs_1;
                                        return __generator(this, function (_b) {
                                            switch (_b.label) {
                                                case 0:
                                                    removeTagsList = [];
                                                    _i = 0, tagList_1 = tagList;
                                                    _b.label = 1;
                                                case 1:
                                                    if (!(_i < tagList_1.length)) return [3, 4];
                                                    tagItem = tagList_1[_i];
                                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                case 2:
                                                    if (_b.sent())
                                                        return [2, callback(null, false)];
                                                    if (tags.indexOf(tagItem.tag) < 0 && tagItem.needSync == false) {
                                                        removeTagsList.push(tagItem.tag);
                                                    }
                                                    _b.label = 3;
                                                case 3:
                                                    _i++;
                                                    return [3, 1];
                                                case 4:
                                                    _a = 0, removeTagsList_1 = removeTagsList;
                                                    _b.label = 5;
                                                case 5:
                                                    if (!(_a < removeTagsList_1.length)) return [3, 10];
                                                    removeTag = removeTagsList_1[_a];
                                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                case 6:
                                                    if (_b.sent())
                                                        return [2, callback(null, false)];
                                                    return [4, TagObjRepository_1.default.deleteNoteData({ workspaceId: workspaceId, tag: removeTag })];
                                                case 7:
                                                    _b.sent();
                                                    return [4, TagObjRepository_1.default.deleteData({ workspaceId: workspaceId, tag: removeTag })];
                                                case 8:
                                                    _b.sent();
                                                    _b.label = 9;
                                                case 9:
                                                    _a++;
                                                    return [3, 5];
                                                case 10: return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                case 11:
                                                    if (_b.sent())
                                                        return [2, callback(null, false)];
                                                    TagObjRepository_1.default.emitSocketEvent({
                                                        workspaceId: workspaceId,
                                                        tagList: removeTagsList,
                                                        action: "remove"
                                                    });
                                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                case 12:
                                                    if (_b.sent())
                                                        return [2, callback(null, false)];
                                                    if (!(tags && tags.length)) return [3, 14];
                                                    return [4, checkUserTagItems({ workspaceId: workspaceId, tags: tags })];
                                                case 13:
                                                    tagObjs_1 = _b.sent();
                                                    TagObjRepository_1.default.updateTagsDownloadedFromServerI({ workspaceId: workspaceId, tagObjs: tagObjs_1 }, function (err, response) {
                                                        if (err) {
                                                            errorHandler_1.default.log({
                                                                err: err, response: response,
                                                                description: "Sync problem => downloadTags => updateTagsDownloadedFromServerI",
                                                                data: {
                                                                    workspaceId: workspaceId,
                                                                    tagObjs: tagObjs_1
                                                                }
                                                            });
                                                        }
                                                        callback(err, response);
                                                    });
                                                    return [3, 15];
                                                case 14:
                                                    callback(err, true);
                                                    _b.label = 15;
                                                case 15: return [2];
                                            }
                                        });
                                    }); });
                                    return [2];
                            }
                        });
                    }); });
                    return [2];
            }
        });
    });
}
exports.default = downloadTags;
