"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var PreviewSyncManager_1 = __importDefault(require("../../../nimbussdk/manager/PreviewSyncManager"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var SyncStatusDisplayEvent_1 = __importDefault(require("../../events/SyncStatusDisplayEvent"));
var TrashRepository_1 = __importDefault(require("../../repositories/TrashRepository"));
var NoteObjRepository_1 = __importDefault(require("../../repositories/NoteObjRepository"));
var NoteObj_1 = __importDefault(require("../../db/NoteObj"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var TextEditor_1 = __importDefault(require("../../../../db/TextEditor"));
var item_1 = __importDefault(require("../../../../db/models/item"));
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
function downloadStructureNotesPerStep(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, start, amount, totalNotes;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId, start = inputData.start, amount = inputData.amount, totalNotes = inputData.totalNotes;
                    syncHandler_1.default.log("sync => downloadStructureNotesPerStep (ws: " + workspaceId + ")");
                    if ((start + amount) > totalNotes) {
                        syncHandler_1.default.log("sync => downloadStructureNotesPerStep => start: " + start + " amount: " + (totalNotes - start));
                    }
                    else {
                        syncHandler_1.default.log("sync => downloadStructureNotesPerStep => start: " + start + " amount: " + amount);
                    }
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 1:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    NimbusSDK_1.default.getApi().getStructureNotes({ workspaceId: workspaceId, start: start, amount: amount }, function (err, body) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!err) return [3, 2];
                                    return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                case 1:
                                    _a.sent();
                                    errorHandler_1.default.log({
                                        err: err, response: body,
                                        description: "Sync problem => downloadStructureNotesPerStep => getStructureNotes",
                                        data: {
                                            start: start,
                                            amount: amount
                                        }
                                    });
                                    return [2, callback(err, false)];
                                case 2:
                                    NimbusSDK_1.default.getAccountManager().getUniqueUserName(function (err, uniqueUserName) { return __awaiter(_this, void 0, void 0, function () {
                                        var checkUserNoteInTrash, getNoteInfo, updateUserStructureNotes, fieldsUpdateDates, i, noteItem, noteFieldsUpdateDates, noteObj, workspaceInstance, _a, saveTextDate, saveTextRes;
                                        var _this = this;
                                        return __generator(this, function (_b) {
                                            switch (_b.label) {
                                                case 0:
                                                    if (!err) return [3, 2];
                                                    return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                                case 1:
                                                    _b.sent();
                                                    errorHandler_1.default.log({
                                                        err: err, response: uniqueUserName,
                                                        description: "Sync problem => downloadStructureNotesPerStep => getUniqueUserName"
                                                    });
                                                    return [2, callback(err, false)];
                                                case 2:
                                                    checkUserNoteInTrash = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                        return __generator(this, function (_a) {
                                                            return [2, new Promise(function (resolve) {
                                                                    var workspaceId = inputData.workspaceId, note = inputData.note;
                                                                    TrashRepository_1.default.checkIfNoteInTrash({ workspaceId: workspaceId, parentId: note.parent_id }, function (err, isMaybeInTrash) {
                                                                        if (err) {
                                                                            errorHandler_1.default.log({
                                                                                err: err, response: isMaybeInTrash,
                                                                                description: "Sync problem => downloadStructureNotesPerStep => checkUserNoteInTrash",
                                                                                data: {
                                                                                    note: note
                                                                                }
                                                                            });
                                                                            return resolve(false);
                                                                        }
                                                                        resolve(isMaybeInTrash);
                                                                    });
                                                                })];
                                                        });
                                                    }); };
                                                    getNoteInfo = function (inputData) {
                                                        return new Promise(function (resolve) {
                                                            var workspaceId = inputData.workspaceId, note = inputData.note, noteFieldsUpdateDates = inputData.noteFieldsUpdateDates;
                                                            NoteObjRepository_1.default.get({ workspaceId: workspaceId, globalId: note.global_id }, function (err, noteObj) { return __awaiter(_this, void 0, void 0, function () {
                                                                var createUserNoteObj, addPreviewEvents, existNoteObj, needPropUpdate, todoCount, _i, _a, todoItem, attachementsCount, _b, _c, attachementItem, _d, previewInput;
                                                                var _this = this;
                                                                return __generator(this, function (_e) {
                                                                    switch (_e.label) {
                                                                        case 0:
                                                                            if (err) {
                                                                                errorHandler_1.default.log({
                                                                                    err: err, response: noteObj,
                                                                                    description: "Sync problem => downloadStructureNotesPerStep => getNoteInfo",
                                                                                    data: {
                                                                                        note: note
                                                                                    }
                                                                                });
                                                                                return [2, resolve(null)];
                                                                            }
                                                                            createUserNoteObj = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                                return __generator(this, function (_a) {
                                                                                    return [2, new Promise(function (resolve) {
                                                                                            var workspaceId = inputData.workspaceId, note = inputData.note;
                                                                                            NoteObjRepository_1.default.create({
                                                                                                workspaceId: workspaceId,
                                                                                                parentId: note.parent_id,
                                                                                                role: note.role,
                                                                                                tag: null
                                                                                            }, function (err, noteObj) {
                                                                                                if (err) {
                                                                                                    errorHandler_1.default.log({
                                                                                                        err: err, response: noteObj,
                                                                                                        description: "Sync problem => downloadStructureNotesPerStep => createUserNoteObj",
                                                                                                        data: {
                                                                                                            note: note
                                                                                                        }
                                                                                                    });
                                                                                                    return resolve(null);
                                                                                                }
                                                                                                resolve(noteObj);
                                                                                            });
                                                                                        })];
                                                                                });
                                                                            }); };
                                                                            addPreviewEvents = function (inputData) {
                                                                                var workspaceId = inputData.workspaceId, note = inputData.note, existNoteObj = inputData.existNoteObj, noteObj = inputData.noteObj;
                                                                                if (!existNoteObj) {
                                                                                    if (noteObj && noteObj.preview && noteObj.preview.global_id) {
                                                                                        PreviewSyncManager_1.default.pushUpdateEvent({
                                                                                            workspaceId: workspaceId,
                                                                                            globalId: note.global_id,
                                                                                            attachmentGlobalId: noteObj.preview.global_id
                                                                                        });
                                                                                    }
                                                                                    return;
                                                                                }
                                                                                if (existNoteObj.preview && existNoteObj.preview.global_id) {
                                                                                    if (noteObj.preview) {
                                                                                        if (noteObj.preview.global_id) {
                                                                                            if (existNoteObj.preview.global_id !== noteObj.preview.global_id) {
                                                                                                PreviewSyncManager_1.default.pushUpdateEvent({
                                                                                                    workspaceId: workspaceId,
                                                                                                    globalId: note.global_id,
                                                                                                    attachmentGlobalId: noteObj.preview.global_id
                                                                                                });
                                                                                            }
                                                                                        }
                                                                                        else {
                                                                                            PreviewSyncManager_1.default.pushRemoveEvent({
                                                                                                workspaceId: workspaceId,
                                                                                                globalId: note.global_id
                                                                                            });
                                                                                        }
                                                                                    }
                                                                                    else {
                                                                                        PreviewSyncManager_1.default.pushRemoveEvent({
                                                                                            workspaceId: workspaceId,
                                                                                            globalId: note.global_id
                                                                                        });
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    if (noteObj.preview && noteObj.preview.global_id) {
                                                                                        PreviewSyncManager_1.default.pushUpdateEvent({
                                                                                            workspaceId: workspaceId,
                                                                                            globalId: note.global_id,
                                                                                            attachmentGlobalId: noteObj.preview.global_id
                                                                                        });
                                                                                    }
                                                                                    else {
                                                                                        if (existNoteObj.preview) {
                                                                                            PreviewSyncManager_1.default.pushRemoveEvent({
                                                                                                workspaceId: workspaceId,
                                                                                                globalId: note.global_id
                                                                                            });
                                                                                        }
                                                                                    }
                                                                                }
                                                                            };
                                                                            existNoteObj = noteObj ? __assign({}, noteObj) : null;
                                                                            needPropUpdate = true;
                                                                            if (existNoteObj) {
                                                                                if (existNoteObj.syncDate > note.date_updated) {
                                                                                    needPropUpdate = false;
                                                                                }
                                                                            }
                                                                            if (!!existNoteObj) return [3, 2];
                                                                            return [4, createUserNoteObj({ workspaceId: workspaceId, note: note })];
                                                                        case 1:
                                                                            noteObj = _e.sent();
                                                                            _e.label = 2;
                                                                        case 2:
                                                                            if (!noteObj) {
                                                                                return [2, resolve(null)];
                                                                            }
                                                                            if (note.todo) {
                                                                                todoCount = { total: 0, unchecked: 0 };
                                                                                for (_i = 0, _a = note.todo; _i < _a.length; _i++) {
                                                                                    todoItem = _a[_i];
                                                                                    todoCount.total += 1;
                                                                                    if (!todoItem.checked) {
                                                                                        todoCount.unchecked += 1;
                                                                                    }
                                                                                }
                                                                                note.todo_count = todoCount;
                                                                            }
                                                                            if (note.attachements) {
                                                                                attachementsCount = { total: 0, in_list: 0 };
                                                                                for (_b = 0, _c = note.attachements; _b < _c.length; _b++) {
                                                                                    attachementItem = _c[_b];
                                                                                    attachementsCount.total += 1;
                                                                                    if (!attachementItem.in_list) {
                                                                                        attachementsCount.in_list += 1;
                                                                                    }
                                                                                }
                                                                                note.attachements_count = attachementsCount;
                                                                            }
                                                                            if (!needPropUpdate) return [3, 4];
                                                                            noteObj.globalId = note.global_id;
                                                                            noteObj.parentId = note.parent_id;
                                                                            noteObj.rootParentId = note.root_parent_id;
                                                                            noteObj.dateAdded = note.date_added_user;
                                                                            noteObj.dateUpdated = note.date_updated_user;
                                                                            noteObj.syncDate = note.date_updated;
                                                                            noteObj.type = note.type;
                                                                            _d = noteObj;
                                                                            return [4, checkUserNoteInTrash({ workspaceId: workspaceId, note: note })];
                                                                        case 3:
                                                                            _d.isMaybeInTrash = _e.sent();
                                                                            noteObj.is_imported = note.is_imported;
                                                                            _e.label = 4;
                                                                        case 4:
                                                                            if (needPropUpdate) {
                                                                                noteObj = NoteObj_1.default.setLocation(noteObj, note.location_lat, note.location_lng);
                                                                                noteObj.attachements = note.attachements ? note.attachements.slice() : [];
                                                                                noteObj.todo = note.todo ? note.todo.slice() : [];
                                                                            }
                                                                            previewInput = { existNoteObj: existNoteObj, workspaceId: workspaceId, note: note, noteObj: null };
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'preview', serverNoteObjProp: 'preview' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.preview = note.preview ? note.preview : null;
                                                                            }
                                                                            else {
                                                                                noteObj.preview = existNoteObj.preview;
                                                                            }
                                                                            addPreviewEvents(__assign(__assign({}, previewInput), { noteObj: noteObj }));
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'tags', serverNoteObjProp: 'tags' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.tags = note.tags;
                                                                            }
                                                                            else {
                                                                                noteObj.tags = existNoteObj.tags;
                                                                            }
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'title', serverNoteObjProp: 'title' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.title = note.title;
                                                                            }
                                                                            else {
                                                                                noteObj.title = existNoteObj.title;
                                                                            }
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'shortText', serverNoteObjProp: 'text_short' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.shortText = note.text_short;
                                                                            }
                                                                            else {
                                                                                noteObj.shortText = existNoteObj.shortText;
                                                                            }
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'text', serverNoteObjProp: 'text' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                if (note.text) {
                                                                                    noteObj = NoteObj_1.default.setText(noteObj, note.text);
                                                                                }
                                                                            }
                                                                            else {
                                                                                noteObj.text = existNoteObj.text;
                                                                            }
                                                                            if (needPropUpdate) {
                                                                                noteObj.text_version = note.text_version;
                                                                                noteObj.isDownloaded = false;
                                                                                noteObj.needSync = false;
                                                                                if (existNoteObj && existNoteObj.needSync) {
                                                                                    if (existNoteObj.text_version >= 2) {
                                                                                        noteObj.needSync = existNoteObj.needSync;
                                                                                    }
                                                                                }
                                                                            }
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'isEncrypted', serverNoteObjProp: 'is_encrypted' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.isEncrypted = note.is_encrypted;
                                                                            }
                                                                            else {
                                                                                noteObj.isEncrypted = existNoteObj.isEncrypted;
                                                                            }
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'isFullwidth', serverNoteObjProp: 'is_fullwidth' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.isFullwidth = note.is_fullwidth;
                                                                            }
                                                                            else {
                                                                                noteObj.isFullwidth = existNoteObj.isFullwidth;
                                                                            }
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'url', serverNoteObjProp: 'url' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.url = note.url;
                                                                            }
                                                                            else {
                                                                                noteObj.url = existNoteObj.url;
                                                                            }
                                                                            noteObj.uniqueUserName = uniqueUserName;
                                                                            noteObj.existOnServer = true;
                                                                            noteObj.isTemp = false;
                                                                            noteObj.isMoreThanLimit = false;
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'editnote', serverNoteObjProp: 'editnote' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.editnote = note.editnote;
                                                                            }
                                                                            else {
                                                                                noteObj.editnote = existNoteObj.editnote;
                                                                            }
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'color', serverNoteObjProp: 'color' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.color = note.color;
                                                                            }
                                                                            else {
                                                                                noteObj.color = existNoteObj.color;
                                                                            }
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'shared', serverNoteObjProp: 'shared' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.shared = !!note.shared;
                                                                                noteObj.shared_url = note.shared_url ? note.shared_url : '';
                                                                            }
                                                                            else {
                                                                                noteObj.shared = existNoteObj.shared;
                                                                                noteObj.shared_url = existNoteObj.shared_url ? existNoteObj.shared_url : '';
                                                                            }
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'favorite', serverNoteObjProp: 'favorite' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.favorite = !!note.favorite;
                                                                            }
                                                                            else {
                                                                                noteObj.favorite = existNoteObj.favorite;
                                                                            }
                                                                            if (syncPropsHandler_1.propOutDate({ existNoteObjProp: 'reminder', serverNoteObjProp: 'reminder' }, existNoteObj, noteFieldsUpdateDates)) {
                                                                                noteObj.reminder = note.reminder ? note.reminder : null;
                                                                            }
                                                                            else {
                                                                                noteObj.reminder = existNoteObj.reminder;
                                                                            }
                                                                            resolve(noteObj);
                                                                            return [2];
                                                                    }
                                                                });
                                                            }); });
                                                        });
                                                    };
                                                    updateUserStructureNotes = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                        return __generator(this, function (_a) {
                                                            return [2, new Promise(function (resolve) {
                                                                    NoteObjRepository_1.default.updateStructureNotesDownloadedFromServerI(inputData, function (err, response) {
                                                                        if (err) {
                                                                            errorHandler_1.default.log({
                                                                                err: err, response: response,
                                                                                description: "Sync problem => downloadStructureNotesPerStep => updateUserStructureNotes",
                                                                                data: inputData
                                                                            });
                                                                            return resolve(false);
                                                                        }
                                                                        resolve(response);
                                                                    });
                                                                })];
                                                        });
                                                    }); };
                                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                case 3:
                                                    if (_b.sent())
                                                        return [2, callback(null, false)];
                                                    if (!body) return [3, 20];
                                                    if (!(body.notes && body.notes.length)) return [3, 20];
                                                    if (!body.notes.length) return [3, 5];
                                                    return [4, SyncStatusDisplayEvent_1.default.set({
                                                            workspaceId: workspaceId,
                                                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOAD_NOTES
                                                        })];
                                                case 4:
                                                    _b.sent();
                                                    _b.label = 5;
                                                case 5:
                                                    fieldsUpdateDates = body.fieldsUpdateDates;
                                                    i = 0;
                                                    _b.label = 6;
                                                case 6:
                                                    if (!(i < body.notes.length)) return [3, 20];
                                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                case 7:
                                                    if (_b.sent())
                                                        return [2, callback(null, false)];
                                                    noteItem = body.notes[i];
                                                    noteFieldsUpdateDates = {};
                                                    if (noteItem && noteItem.global_id) {
                                                        if (fieldsUpdateDates && typeof (fieldsUpdateDates[noteItem.global_id]) !== 'undefined') {
                                                            noteFieldsUpdateDates = fieldsUpdateDates[noteItem.global_id];
                                                        }
                                                    }
                                                    return [4, getNoteInfo({
                                                            workspaceId: workspaceId,
                                                            note: noteItem,
                                                            noteFieldsUpdateDates: noteFieldsUpdateDates
                                                        })];
                                                case 8:
                                                    noteObj = _b.sent();
                                                    if (!noteObj) return [3, 19];
                                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                case 9:
                                                    if (_b.sent())
                                                        return [2, callback(null, false)];
                                                    return [4, updateUserStructureNotes({ workspaceId: workspaceId, noteObjs: [noteObj] })];
                                                case 10:
                                                    _b.sent();
                                                    if (!(noteObj.text_version > 1)) return [3, 16];
                                                    if (!workspaceId) return [3, 12];
                                                    return [4, workspace_1.default.getById(workspaceId)];
                                                case 11:
                                                    _a = _b.sent();
                                                    return [3, 14];
                                                case 12: return [4, workspace_1.default.getDefault()];
                                                case 13:
                                                    _a = _b.sent();
                                                    _b.label = 14;
                                                case 14:
                                                    workspaceInstance = (_a);
                                                    if (!workspaceInstance) return [3, 16];
                                                    saveTextDate = {
                                                        note: item_1.default.getResponseJson(noteObj),
                                                        workspaceId: workspaceInstance.globalId,
                                                        noteId: noteObj.globalId
                                                    };
                                                    return [4, TextEditor_1.default.makeBgNoteSync(saveTextDate)];
                                                case 15:
                                                    saveTextRes = _b.sent();
                                                    if (saveTextRes && !saveTextRes.result) {
                                                        syncHandler_1.default.sendLog({
                                                            err: new Error("Can not download note text v2: " + workspaceInstance.globalId + "_" + noteObj.globalId),
                                                        });
                                                    }
                                                    _b.label = 16;
                                                case 16: return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                case 17:
                                                    if (_b.sent())
                                                        return [2, callback(null, false)];
                                                    if (!body.notes.length) return [3, 19];
                                                    return [4, SyncStatusDisplayEvent_1.default.set({
                                                            workspaceId: workspaceId,
                                                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOAD_NOTES,
                                                            props: {
                                                                "current": (start + i + 1),
                                                                "total": totalNotes
                                                            }
                                                        })];
                                                case 18:
                                                    _b.sent();
                                                    _b.label = 19;
                                                case 19:
                                                    i++;
                                                    return [3, 6];
                                                case 20:
                                                    callback(err, body ? body.last_update_time : null);
                                                    return [2];
                                            }
                                        });
                                    }); });
                                    return [2];
                            }
                        });
                    }); });
                    return [2];
            }
        });
    });
}
exports.default = downloadStructureNotesPerStep;
