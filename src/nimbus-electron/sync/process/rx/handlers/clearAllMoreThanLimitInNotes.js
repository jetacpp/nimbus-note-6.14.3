"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NoteObjRepository_1 = __importDefault(require("../../repositories/NoteObjRepository"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
function clearAllMoreThanLimitInNotes(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    var workspaceId = inputData.workspaceId;
    NoteObjRepository_1.default.clearAllMoreThanLimitInNotesI(inputData, function (err, response) {
        if (err) {
            errorHandler_1.default.log({
                err: err, response: response,
                description: "Sync problem => clearAllMoreThanLimitInNotes => clearAllMoreThanLimitInNotesI",
                data: inputData
            });
        }
        callback(err, response);
    });
}
exports.default = clearAllMoreThanLimitInNotes;
