"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var SyncStatusDisplayEvent_1 = __importDefault(require("../../events/SyncStatusDisplayEvent"));
var FolderObjRepository_1 = __importDefault(require("../../repositories/FolderObjRepository"));
var NoteObjRepository_1 = __importDefault(require("../../repositories/NoteObjRepository"));
var AttachmentObjRepository_1 = __importDefault(require("../../repositories/AttachmentObjRepository"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var TYPE_NOTE = "note";
var TYPE_ATTACHEMENT = "attachement";
function downloadRemovedItems(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId;
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 1:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    NimbusSDK_1.default.getApi().getRemovedItems(inputData, function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                        var getAvailableFolder, getAvailableNote, getAvailableAttachment, checkIfFolderExist, checkUserRemovedItems, removedItems, folders, notes, attachments, removedItemsObj, removeUserFolders, deleteUserNotes, deleteUserAttachments;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!err) return [3, 2];
                                    return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                case 1:
                                    _a.sent();
                                    errorHandler_1.default.log({
                                        err: err, response: response,
                                        description: "Sync problem => downloadRemovedItems => getRemovedItems"
                                    });
                                    return [2, callback(err, false)];
                                case 2:
                                    getAvailableFolder = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, globalId = inputData.globalId, removedTime = inputData.removedTime;
                                                    FolderObjRepository_1.default.get({ workspaceId: workspaceId, globalId: globalId }, function (err, folder) {
                                                        if (err) {
                                                            errorHandler_1.default.log({
                                                                err: err, response: folder,
                                                                description: "Sync problem => downloadRemovedItems => getAvailableFolder",
                                                                data: inputData
                                                            });
                                                            return resolve(null);
                                                        }
                                                        if (folder && (folder.syncDate < removedTime)) {
                                                            resolve(globalId);
                                                        }
                                                        else {
                                                            resolve(null);
                                                        }
                                                    });
                                                })];
                                        });
                                    }); };
                                    getAvailableNote = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, globalId = inputData.globalId, removedTime = inputData.removedTime;
                                                    NoteObjRepository_1.default.get({ workspaceId: workspaceId, globalId: globalId }, function (err, note) {
                                                        if (err) {
                                                            errorHandler_1.default.log({
                                                                err: err, response: note,
                                                                description: "Sync problem => downloadRemovedItems => getAvailableNote",
                                                                data: inputData
                                                            });
                                                            return resolve(null);
                                                        }
                                                        if (note && note.syncDate < removedTime) {
                                                            resolve(globalId);
                                                        }
                                                        else {
                                                            resolve(null);
                                                        }
                                                    });
                                                })];
                                        });
                                    }); };
                                    getAvailableAttachment = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, globalId = inputData.globalId, removedTime = inputData.removedTime;
                                                    AttachmentObjRepository_1.default.get({ workspaceId: workspaceId, globalId: globalId }, function (err, attachment) {
                                                        if (err) {
                                                            errorHandler_1.default.log({
                                                                err: err, response: attachment,
                                                                description: "Sync problem => downloadRemovedItems => getAvailableAttachment",
                                                                data: inputData
                                                            });
                                                        }
                                                        if (attachment && attachment.syncDate < removedTime) {
                                                            resolve(globalId);
                                                        }
                                                        else {
                                                            resolve(null);
                                                        }
                                                    });
                                                })];
                                        });
                                    }); };
                                    checkIfFolderExist = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    FolderObjRepository_1.default.checkIfFolderExist(inputData, function (err, isFolderExist) {
                                                        if (err) {
                                                            errorHandler_1.default.log({
                                                                err: err, response: isFolderExist,
                                                                description: "Sync problem => downloadRemovedItems => checkIfFolderExist",
                                                                data: inputData
                                                            });
                                                        }
                                                        resolve(isFolderExist);
                                                    });
                                                })];
                                        });
                                    }); };
                                    checkUserRemovedItems = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var workspaceId, removedItems, removedItemsObj, _i, removedItems_1, removedItem, globalId, removedTime, type, findGlobalId, isFolderExist;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                workspaceId = inputData.workspaceId, removedItems = inputData.removedItems;
                                                                removedItemsObj = {
                                                                    'folders': [],
                                                                    'notes': [],
                                                                    'attachments': []
                                                                };
                                                                _i = 0, removedItems_1 = removedItems;
                                                                _a.label = 1;
                                                            case 1:
                                                                if (!(_i < removedItems_1.length)) return [3, 10];
                                                                removedItem = removedItems_1[_i];
                                                                globalId = removedItem.global_id;
                                                                removedTime = removedItem.time;
                                                                type = removedItem.type;
                                                                findGlobalId = null;
                                                                if (!(type.toLowerCase() === TYPE_NOTE)) return [3, 7];
                                                                return [4, checkIfFolderExist({ workspaceId: workspaceId, globalId: globalId })];
                                                            case 2:
                                                                isFolderExist = _a.sent();
                                                                if (!isFolderExist) return [3, 4];
                                                                return [4, getAvailableFolder({ workspaceId: workspaceId, globalId: globalId, removedTime: removedTime })];
                                                            case 3:
                                                                findGlobalId = _a.sent();
                                                                if (findGlobalId) {
                                                                    removedItemsObj['folders'].push(findGlobalId);
                                                                }
                                                                return [3, 6];
                                                            case 4: return [4, getAvailableNote({ workspaceId: workspaceId, globalId: globalId, removedTime: removedTime })];
                                                            case 5:
                                                                findGlobalId = _a.sent();
                                                                if (findGlobalId) {
                                                                    removedItemsObj['notes'].push(findGlobalId);
                                                                }
                                                                _a.label = 6;
                                                            case 6: return [3, 9];
                                                            case 7:
                                                                if (!(type.toLowerCase() === TYPE_ATTACHEMENT)) return [3, 9];
                                                                return [4, getAvailableAttachment({ workspaceId: workspaceId, globalId: globalId, removedTime: removedTime })];
                                                            case 8:
                                                                findGlobalId = _a.sent();
                                                                if (findGlobalId) {
                                                                    removedItemsObj['attachments'].push(findGlobalId);
                                                                }
                                                                _a.label = 9;
                                                            case 9:
                                                                _i++;
                                                                return [3, 1];
                                                            case 10:
                                                                resolve(removedItemsObj);
                                                                return [2];
                                                        }
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    removedItems = response && response.removedItems ? response.removedItems : [];
                                    folders = [], notes = [], attachments = [];
                                    if (!(removedItems && removedItems.length)) return [3, 16];
                                    return [4, checkUserRemovedItems({ workspaceId: workspaceId, removedItems: removedItems })];
                                case 3:
                                    removedItemsObj = _a.sent();
                                    folders = removedItemsObj['folders'];
                                    notes = removedItemsObj['notes'];
                                    attachments = removedItemsObj['attachments'];
                                    removeUserFolders = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    FolderObjRepository_1.default.deleteRemovedItemsDownloadedFromServerI(inputData, function (err, response) {
                                                        if (err) {
                                                            errorHandler_1.default.log({
                                                                err: err, response: response,
                                                                description: "Sync problem => downloadRemovedItems => checkUserRemovedItems => removeUserFolders",
                                                                data: inputData
                                                            });
                                                            return resolve(false);
                                                        }
                                                        resolve(response);
                                                    });
                                                })];
                                        });
                                    }); };
                                    deleteUserNotes = function (notes) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    NoteObjRepository_1.default.deleteRemovedItemsDownloadedFromServerI({
                                                        workspaceId: workspaceId,
                                                        globalIds: notes
                                                    }, function (err, response) {
                                                        if (err) {
                                                            errorHandler_1.default.log({
                                                                err: err, response: response,
                                                                description: "Sync problem => downloadRemovedItems => checkUserRemovedItems => deleteUserNotes",
                                                                data: {
                                                                    notes: notes
                                                                }
                                                            });
                                                            return resolve(false);
                                                        }
                                                        resolve(response);
                                                    });
                                                })];
                                        });
                                    }); };
                                    deleteUserAttachments = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, globalIds = inputData.globalIds;
                                                    AttachmentObjRepository_1.default.deleteRemovedItemsDownloadedFromServerI({ workspaceId: workspaceId, globalIds: globalIds }, function (err, response) {
                                                        if (err) {
                                                            errorHandler_1.default.log({
                                                                err: err, response: response,
                                                                description: "Sync problem => downloadRemovedItems => checkUserRemovedItems => deleteUserAttachments",
                                                                data: inputData
                                                            });
                                                            return resolve(false);
                                                        }
                                                        resolve(response);
                                                    });
                                                })];
                                        });
                                    }); };
                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                case 4:
                                    if (_a.sent())
                                        return [2, callback(null, false)];
                                    if (!(folders.length || notes.length || attachments.length)) return [3, 16];
                                    if (!folders.length) return [3, 8];
                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                case 5:
                                    if (_a.sent())
                                        return [2, callback(null, false)];
                                    return [4, SyncStatusDisplayEvent_1.default.set({
                                            workspaceId: workspaceId,
                                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.REMOVE_FOLDERS
                                        })];
                                case 6:
                                    _a.sent();
                                    return [4, removeUserFolders({ workspaceId: workspaceId, folderIds: folders })];
                                case 7:
                                    _a.sent();
                                    _a.label = 8;
                                case 8:
                                    if (!notes.length) return [3, 12];
                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                case 9:
                                    if (_a.sent())
                                        return [2, callback(null, false)];
                                    return [4, SyncStatusDisplayEvent_1.default.set({
                                            workspaceId: workspaceId,
                                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.REMOVE_NOTES
                                        })];
                                case 10:
                                    _a.sent();
                                    return [4, deleteUserNotes(notes)];
                                case 11:
                                    _a.sent();
                                    _a.label = 12;
                                case 12:
                                    if (!attachments.length) return [3, 16];
                                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                case 13:
                                    if (_a.sent())
                                        return [2, callback(null, false)];
                                    return [4, SyncStatusDisplayEvent_1.default.set({
                                            workspaceId: workspaceId,
                                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.REMOVE_ATTACHMENTS
                                        })];
                                case 14:
                                    _a.sent();
                                    return [4, deleteUserAttachments({ workspaceId: workspaceId, globalIds: attachments })];
                                case 15:
                                    _a.sent();
                                    _a.label = 16;
                                case 16:
                                    callback(err, true);
                                    return [2];
                            }
                        });
                    }); });
                    return [2];
            }
        });
    });
}
exports.default = downloadRemovedItems;
