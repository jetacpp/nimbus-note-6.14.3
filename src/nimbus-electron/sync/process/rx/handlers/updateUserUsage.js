"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var UserObjRepository_1 = __importDefault(require("../../repositories/UserObjRepository"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
function updateUserUsage(inputData) {
    return __awaiter(this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                    var workspaceId, useForSync;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                workspaceId = inputData.workspaceId, useForSync = inputData.useForSync;
                                if (!useForSync) return [3, 2];
                                return [4, SyncStatusChangedEvent_1.default.needPreventSync({})];
                            case 1:
                                if (_a.sent())
                                    return [2, resolve(false)];
                                _a.label = 2;
                            case 2:
                                NimbusSDK_1.default.getApi().userInfo({
                                    workspaceId: workspaceId,
                                    skipSyncHandlers: !useForSync
                                }, function (err, userInstance) { return __awaiter(_this, void 0, void 0, function () {
                                    var queryData_1, updateData_1;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                if (!(err || !userInstance)) return [3, 3];
                                                if (!useForSync) return [3, 2];
                                                return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                            case 1:
                                                _a.sent();
                                                errorHandler_1.default.log({
                                                    err: err, response: userInstance,
                                                    description: "Sync/Purchase problem => updateUserUsage => userInfo",
                                                    data: inputData
                                                });
                                                _a.label = 2;
                                            case 2: return [2, resolve(null)];
                                            case 3:
                                                if (userInstance && userInstance.user_id) {
                                                    queryData_1 = {
                                                        email: userInstance.login
                                                    };
                                                    updateData_1 = {
                                                        usageCurrent: null,
                                                        usageMax: null,
                                                        dateNextQuotaReset: null,
                                                        subscribe: null,
                                                        paymentStartDate: null,
                                                        paymentEndDate: null,
                                                        paymentSystemCode: null
                                                    };
                                                    if (userInstance.usage && userInstance.usage.notes) {
                                                        updateData_1.usageCurrent = userInstance.usage.notes.current;
                                                        updateData_1.usageMax = userInstance.usage.notes.max;
                                                    }
                                                    updateData_1.dateNextQuotaReset = UserObjRepository_1.default.getDateNextQuotaReset(userInstance.days_to_quota_reset);
                                                    if (userInstance.premium) {
                                                        updateData_1.subscribe = userInstance.premium.active ? 1 : 0;
                                                        updateData_1.paymentStartDate = userInstance.premium.start_date;
                                                        updateData_1.paymentEndDate = userInstance.premium.end_date;
                                                        updateData_1.paymentSystemCode = userInstance.premium.source;
                                                    }
                                                    UserObjRepository_1.default.update({ queryData: queryData_1, updateData: updateData_1 }, function (err, res) {
                                                        if (err) {
                                                            errorHandler_1.default.log({
                                                                err: err, response: res,
                                                                description: "Sync/Purchase problem => updateUserUsage => UserObjRepository.update",
                                                                data: {
                                                                    queryData: queryData_1,
                                                                    updateData: updateData_1
                                                                }
                                                            });
                                                        }
                                                        resolve(userInstance.usage.notes);
                                                    });
                                                }
                                                else {
                                                    errorHandler_1.default.log({
                                                        err: err, response: userInstance,
                                                        description: "Sync/Purchase problem => updateUserUsage",
                                                        data: inputData
                                                    });
                                                    return [2, resolve(null)];
                                                }
                                                return [2];
                                        }
                                    });
                                }); });
                                return [2];
                        }
                    });
                }); })];
        });
    });
}
exports.default = updateUserUsage;
