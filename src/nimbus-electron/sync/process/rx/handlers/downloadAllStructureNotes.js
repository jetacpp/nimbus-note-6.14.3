"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../../config"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var SyncStatusDisplayEvent_1 = __importDefault(require("../../events/SyncStatusDisplayEvent"));
var getDownloadNotesTotalAmount_1 = __importDefault(require("./getDownloadNotesTotalAmount"));
var downloadStructureNotesPerStep_1 = __importDefault(require("./downloadStructureNotesPerStep"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var AccountManager_1 = __importDefault(require("../../../nimbussdk/manager/AccountManager"));
function downloadAllStructureNotes(inputData, callback) {
    var _this = this;
    if (callback === void 0) { callback = function (err, res) {
    }; }
    var workspaceId = inputData.workspaceId;
    getDownloadNotesTotalAmount_1.default(inputData, function (err, data) { return __awaiter(_this, void 0, void 0, function () {
        var totalIterations, totalNotes, downloadUserNotesIteration, lastUpdateTime, iterationCount, lastUpdateTimePerIteration;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (err || !data) {
                        errorHandler_1.default.log({
                            err: err, response: data,
                            description: "Sync problem => downloadAllStructureNotes => getDownloadNotesTotalAmount"
                        });
                        return [2, callback(err, null)];
                    }
                    totalIterations = data.totalIterations;
                    totalNotes = data.totalNotes;
                    downloadUserNotesIteration = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    var workspaceId = inputData.workspaceId, iterationCount = inputData.iterationCount, downloadNoteCountPerStep = inputData.downloadNoteCountPerStep, totalNotes = inputData.totalNotes;
                                    var start = iterationCount * downloadNoteCountPerStep;
                                    var amount = downloadNoteCountPerStep;
                                    downloadStructureNotesPerStep_1.default({ workspaceId: workspaceId, start: start, amount: amount, totalNotes: totalNotes }, function (err, lastUpdateTime) {
                                        if (err || !lastUpdateTime) {
                                            return resolve(false);
                                        }
                                        setTimeout(function () {
                                            resolve(lastUpdateTime);
                                        }, 250);
                                    });
                                })];
                        });
                    }); };
                    if (!totalIterations) return [3, 2];
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOAD_NOTES
                        })];
                case 1:
                    _a.sent();
                    _a.label = 2;
                case 2:
                    lastUpdateTime = null;
                    iterationCount = 0;
                    _a.label = 3;
                case 3:
                    if (!(iterationCount < totalIterations)) return [3, 7];
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 4:
                    if (_a.sent())
                        return [2, callback(err, false)];
                    return [4, downloadUserNotesIteration({
                            workspaceId: workspaceId,
                            iterationCount: iterationCount,
                            downloadNoteCountPerStep: config_1.default.DOWNLOAD_NOTE_COUNT_PER_STEP,
                            totalNotes: totalNotes
                        })];
                case 5:
                    lastUpdateTimePerIteration = _a.sent();
                    if (lastUpdateTimePerIteration) {
                        lastUpdateTime = lastUpdateTimePerIteration;
                    }
                    _a.label = 6;
                case 6:
                    iterationCount++;
                    return [3, 3];
                case 7:
                    if (!lastUpdateTime) {
                        return [2, callback(err, null)];
                    }
                    AccountManager_1.default.setLastUpdateTime({ workspaceId: workspaceId, lastUpdateTime: lastUpdateTime }, function (err, response) {
                        if (err) {
                            errorHandler_1.default.log({
                                err: err, response: response,
                                description: "Sync problem => downloadAllStructureNotes => setLastUpdateTime",
                                data: {
                                    lastUpdateTime: lastUpdateTime
                                }
                            });
                        }
                        callback(err, response);
                    });
                    return [2];
            }
        });
    }); });
}
exports.default = downloadAllStructureNotes;
