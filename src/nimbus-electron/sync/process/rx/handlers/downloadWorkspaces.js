"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var WorkspaceObjRepository_1 = __importDefault(require("../../repositories/WorkspaceObjRepository"));
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var auth_1 = __importDefault(require("../../../../auth/auth"));
var OrgObjRepository_1 = __importDefault(require("../../repositories/OrgObjRepository"));
var orgs_1 = __importDefault(require("../../../../db/models/orgs"));
var SelectedOrganization_1 = __importDefault(require("../../../../organization/SelectedOrganization"));
var page_1 = __importDefault(require("../../../../request/page"));
var SelectedWorkspace_1 = __importDefault(require("../../../../workspace/SelectedWorkspace"));
function downloadWorkspaces(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, skipSyncHandlers;
        var _this = this;
        return __generator(this, function (_a) {
            workspaceId = inputData.workspaceId, skipSyncHandlers = inputData.skipSyncHandlers;
            NimbusSDK_1.default.getApi().workspacesGet({ workspaceId: workspaceId, skipSyncHandlers: skipSyncHandlers }, function (err, workspaces) { return __awaiter(_this, void 0, void 0, function () {
                var checkUserWorkspaceItem, checkUserRemovedWorkspaces, updateWorkspaceInvites, updateWorkspaceMembers, checkUserWorkspaceItems, saveUserDefaultOrg;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!(err || !workspaces)) return [3, 3];
                            if (!!skipSyncHandlers) return [3, 2];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 1:
                            _a.sent();
                            _a.label = 2;
                        case 2:
                            errorHandler_1.default.log({
                                err: err, response: workspaces,
                                description: "Sync problem => downloadWorkspaces => getWorkspaces"
                            });
                            return [2, callback(err, false)];
                        case 3:
                            checkUserWorkspaceItem = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    return [2, new Promise(function (resolve) {
                                            var workspace = inputData.workspace, skipSyncHandlers = inputData.skipSyncHandlers;
                                            var globalId = workspace.globalId;
                                            WorkspaceObjRepository_1.default.get({ globalId: globalId }, function (err, workspaceObj) { return __awaiter(_this, void 0, void 0, function () {
                                                var createUserWorkspaceObj, members, invites;
                                                var _this = this;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            if (err) {
                                                                errorHandler_1.default.log({
                                                                    err: err, response: workspaceObj,
                                                                    description: "Sync problem => downloadWorkspaces => checkUserWorkspaceItem",
                                                                    data: inputData
                                                                });
                                                                return [2, resolve(null)];
                                                            }
                                                            createUserWorkspaceObj = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                return __generator(this, function (_a) {
                                                                    return [2, new Promise(function (resolve) {
                                                                            var workspace = inputData.workspace;
                                                                            WorkspaceObjRepository_1.default.create({ workspace: workspace }, function (err, workspaceObj) {
                                                                                if (err) {
                                                                                    errorHandler_1.default.log({
                                                                                        err: err, response: workspaceObj,
                                                                                        description: "Sync problem => downloadWorkspaces => createUserWorkspaceObj",
                                                                                        data: inputData
                                                                                    });
                                                                                    return resolve(null);
                                                                                }
                                                                                resolve(workspaceObj);
                                                                            });
                                                                        })];
                                                                });
                                                            }); };
                                                            if (!!workspaceObj) return [3, 2];
                                                            return [4, createUserWorkspaceObj({ workspace: workspace })];
                                                        case 1:
                                                            workspaceObj = _a.sent();
                                                            _a.label = 2;
                                                        case 2:
                                                            if (!workspaceObj) {
                                                                return [2, resolve(null)];
                                                            }
                                                            workspaceObj.title = workspace.title;
                                                            workspaceObj.userId = workspace.userId;
                                                            workspaceObj.org = workspace.org;
                                                            workspaceObj.isDefault = workspace.isDefault;
                                                            workspaceObj.countMembers = workspace.countMembers;
                                                            workspaceObj.countInvites = workspace.countInvites;
                                                            workspaceObj.defaultEncryptionKeyId = workspace.defaultEncryptionKeyId;
                                                            workspaceObj.access = workspace.access;
                                                            workspaceObj.updatedAt = workspace.updatedAt;
                                                            workspaceObj.lastUpdateTime = workspace.updatedAt;
                                                            workspaceObj.syncDate = workspace.updatedAt;
                                                            workspaceObj.needSync = false;
                                                            workspaceObj.existOnServer = true;
                                                            workspaceObj.isNotesLimited = workspace.isNotesLimited;
                                                            workspaceObj.user = workspace.user;
                                                            workspaceObj.notesEmail = workspace.notesEmail;
                                                            workspaceObj.avatar = workspace.avatar || null;
                                                            workspaceObj.color = workspace.color || null;
                                                            if (!!skipSyncHandlers) return [3, 6];
                                                            return [4, updateWorkspaceMembers({
                                                                    workspaceId: workspaceId,
                                                                    globalId: workspace.globalId,
                                                                    skipSyncHandlers: skipSyncHandlers
                                                                })];
                                                        case 3:
                                                            members = _a.sent();
                                                            if (members) {
                                                                workspaceObj.members = members;
                                                            }
                                                            if (!workspaceObj.members || !workspace.countMembers) {
                                                                workspaceObj.members = [];
                                                            }
                                                            if (!workspace.countInvites) return [3, 5];
                                                            return [4, updateWorkspaceInvites({
                                                                    workspaceId: workspaceId,
                                                                    globalId: workspace.globalId,
                                                                    skipSyncHandlers: skipSyncHandlers
                                                                })];
                                                        case 4:
                                                            invites = _a.sent();
                                                            if (invites) {
                                                                workspaceObj.invites = invites;
                                                            }
                                                            _a.label = 5;
                                                        case 5:
                                                            if (!workspaceObj.invites || !workspace.countInvites) {
                                                                workspaceObj.invites = [];
                                                            }
                                                            _a.label = 6;
                                                        case 6:
                                                            resolve(workspaceObj);
                                                            return [2];
                                                    }
                                                });
                                            }); });
                                        })];
                                });
                            }); };
                            checkUserRemovedWorkspaces = function (inputData) {
                                return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaces, skipSyncHandlers;
                                    var _this = this;
                                    return __generator(this, function (_a) {
                                        workspaces = inputData.workspaces, skipSyncHandlers = inputData.skipSyncHandlers;
                                        workspace_1.default.findAll({}, {}, function (err, localWorkspaces) { return __awaiter(_this, void 0, void 0, function () {
                                            var workspacesIdList, removeUserWorkspace, checkRemovedWorkspace, _i, localWorkspaces_1, localWorkspace;
                                            var _this = this;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        if (err || !localWorkspaces) {
                                                            localWorkspaces = [];
                                                        }
                                                        workspacesIdList = workspaces.map(function (workspace) {
                                                            return workspace.globalId;
                                                        });
                                                        removeUserWorkspace = function (workspace) {
                                                            return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                                var count;
                                                                return __generator(this, function (_a) {
                                                                    switch (_a.label) {
                                                                        case 0: return [4, WorkspaceObjRepository_1.default.remove({ workspaceObjs: workspace })];
                                                                        case 1:
                                                                            count = _a.sent();
                                                                            if (!count) return [3, 3];
                                                                            return [4, checkRemovedWorkspace(workspace)];
                                                                        case 2:
                                                                            _a.sent();
                                                                            _a.label = 3;
                                                                        case 3: return [2, resolve(count)];
                                                                    }
                                                                });
                                                            }); });
                                                        };
                                                        checkRemovedWorkspace = function (workspaceItem) { return __awaiter(_this, void 0, void 0, function () {
                                                            var org, isBusinessOrg, selectedWorkspaceId, countWorkspaces;
                                                            return __generator(this, function (_a) {
                                                                switch (_a.label) {
                                                                    case 0: return [4, orgs_1.default.getByWorkspaceId(workspaceItem.globalId)];
                                                                    case 1:
                                                                        org = _a.sent();
                                                                        isBusinessOrg = org && org.type === orgs_1.default.TYPE_BUSINESS;
                                                                        if (!org) {
                                                                            isBusinessOrg = workspaceItem.orgId && workspaceItem.orgId.charAt(0) === 'b';
                                                                        }
                                                                        return [4, SelectedWorkspace_1.default.getGlobalId()];
                                                                    case 2:
                                                                        selectedWorkspaceId = _a.sent();
                                                                        return [4, workspace_1.default.countUserWorkspacesByOrgId(workspaceItem.orgId)];
                                                                    case 3:
                                                                        countWorkspaces = _a.sent();
                                                                        if (!isBusinessOrg) return [3, 10];
                                                                        if (!countWorkspaces) return [3, 5];
                                                                        return [4, SelectedOrganization_1.default.set(workspaceItem.orgId)];
                                                                    case 4:
                                                                        _a.sent();
                                                                        return [3, 7];
                                                                    case 5: return [4, SelectedOrganization_1.default.set(null)];
                                                                    case 6:
                                                                        _a.sent();
                                                                        _a.label = 7;
                                                                    case 7:
                                                                        if (!(!countWorkspaces || (selectedWorkspaceId === workspaceItem.globalId))) return [3, 9];
                                                                        return [4, page_1.default.reload(false)];
                                                                    case 8:
                                                                        _a.sent();
                                                                        _a.label = 9;
                                                                    case 9: return [3, 13];
                                                                    case 10:
                                                                        if (!(selectedWorkspaceId === workspaceItem.globalId)) return [3, 13];
                                                                        return [4, SelectedWorkspace_1.default.set(null)];
                                                                    case 11:
                                                                        _a.sent();
                                                                        return [4, page_1.default.reload(false)];
                                                                    case 12:
                                                                        _a.sent();
                                                                        _a.label = 13;
                                                                    case 13: return [2];
                                                                }
                                                            });
                                                        }); };
                                                        _i = 0, localWorkspaces_1 = localWorkspaces;
                                                        _a.label = 1;
                                                    case 1:
                                                        if (!(_i < localWorkspaces_1.length)) return [3, 4];
                                                        localWorkspace = localWorkspaces_1[_i];
                                                        if (!(workspacesIdList.indexOf(localWorkspace.globalId) < 0)) return [3, 3];
                                                        return [4, removeUserWorkspace(localWorkspace)];
                                                    case 2:
                                                        _a.sent();
                                                        _a.label = 3;
                                                    case 3:
                                                        _i++;
                                                        return [3, 1];
                                                    case 4: return [2, resolve()];
                                                }
                                            });
                                        }); });
                                        return [2];
                                    });
                                }); });
                            };
                            updateWorkspaceInvites = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                            var workspaceId, globalId, skipSyncHandlers;
                                            var _this = this;
                                            return __generator(this, function (_a) {
                                                workspaceId = inputData.workspaceId, globalId = inputData.globalId, skipSyncHandlers = inputData.skipSyncHandlers;
                                                NimbusSDK_1.default.getApi().workspaceInvitesGet({
                                                    workspaceId: workspaceId,
                                                    globalId: globalId,
                                                    skipSyncHandlers: skipSyncHandlers
                                                }, function (err, workspaceInvites) { return __awaiter(_this, void 0, void 0, function () {
                                                    return __generator(this, function (_a) {
                                                        if (err) {
                                                            return [2, resolve(null)];
                                                        }
                                                        return [2, resolve(workspaceInvites.invites)];
                                                    });
                                                }); });
                                                return [2];
                                            });
                                        }); })];
                                });
                            }); };
                            updateWorkspaceMembers = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                            var workspaceId, globalId, skipSyncHandlers;
                                            var _this = this;
                                            return __generator(this, function (_a) {
                                                workspaceId = inputData.workspaceId, globalId = inputData.globalId, skipSyncHandlers = inputData.skipSyncHandlers;
                                                NimbusSDK_1.default.getApi().workspaceMembersGet({
                                                    workspaceId: workspaceId,
                                                    globalId: globalId,
                                                    skipSyncHandlers: skipSyncHandlers
                                                }, function (err, workspaceMembers) { return __awaiter(_this, void 0, void 0, function () {
                                                    return __generator(this, function (_a) {
                                                        if (err || !workspaceMembers) {
                                                            return [2, resolve(null)];
                                                        }
                                                        return [2, resolve(workspaceMembers.members)];
                                                    });
                                                }); });
                                                return [2];
                                            });
                                        }); })];
                                });
                            }); };
                            checkUserWorkspaceItems = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                            var workspaces, skipSyncHandlers, _i, workspaces_1, workspace_2, workspaceObj;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        workspaces = inputData.workspaces, skipSyncHandlers = inputData.skipSyncHandlers;
                                                        _i = 0, workspaces_1 = workspaces;
                                                        _a.label = 1;
                                                    case 1:
                                                        if (!(_i < workspaces_1.length)) return [3, 5];
                                                        workspace_2 = workspaces_1[_i];
                                                        return [4, checkUserWorkspaceItem({ workspace: workspace_2, skipSyncHandlers: skipSyncHandlers })];
                                                    case 2:
                                                        workspaceObj = _a.sent();
                                                        if (!workspaceObj) return [3, 4];
                                                        return [4, WorkspaceObjRepository_1.default.update({ workspaceObjs: workspaceObj })];
                                                    case 3:
                                                        _a.sent();
                                                        if (!skipSyncHandlers) {
                                                            WorkspaceObjRepository_1.default.sendSocketEventOnUpdateInvites(workspaceObj.globalId);
                                                            WorkspaceObjRepository_1.default.sendSocketEventOnUpdateMembers(workspaceObj.globalId);
                                                        }
                                                        _a.label = 4;
                                                    case 4:
                                                        _i++;
                                                        return [3, 1];
                                                    case 5:
                                                        if (!skipSyncHandlers) {
                                                            WorkspaceObjRepository_1.default.sendSocketEventOnWorkspacesAccessChange();
                                                        }
                                                        resolve(true);
                                                        return [2];
                                                }
                                            });
                                        }); })];
                                });
                            }); };
                            saveUserDefaultOrg = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                            var workspaces, userInfo, findOrg, _i, workspaces_2, workspace_3, org, user, orgSaved;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        workspaces = inputData.workspaces;
                                                        return [4, auth_1.default.getUserAsync()];
                                                    case 1:
                                                        userInfo = _a.sent();
                                                        if (!userInfo) {
                                                            return [2, resolve(false)];
                                                        }
                                                        findOrg = null;
                                                        for (_i = 0, workspaces_2 = workspaces; _i < workspaces_2.length; _i++) {
                                                            workspace_3 = workspaces_2[_i];
                                                            org = workspace_3.org;
                                                            if (!org) {
                                                                continue;
                                                            }
                                                            user = org.user;
                                                            if (!user) {
                                                                continue;
                                                            }
                                                            if (user.email === userInfo.email) {
                                                                findOrg = org;
                                                                break;
                                                            }
                                                        }
                                                        if (!findOrg) {
                                                            return [2, resolve(false)];
                                                        }
                                                        return [4, OrgObjRepository_1.default.update({ orgObjs: findOrg })];
                                                    case 2:
                                                        orgSaved = _a.sent();
                                                        return [2, resolve(orgSaved)];
                                                }
                                            });
                                        }); })];
                                });
                            }); };
                            if (!(workspaces && workspaces.length)) return [3, 7];
                            return [4, saveUserDefaultOrg({ workspaces: workspaces })];
                        case 4:
                            _a.sent();
                            return [4, checkUserRemovedWorkspaces({ workspaces: workspaces, skipSyncHandlers: skipSyncHandlers })];
                        case 5:
                            _a.sent();
                            return [4, checkUserWorkspaceItems({ workspaces: workspaces, skipSyncHandlers: skipSyncHandlers })];
                        case 6:
                            _a.sent();
                            _a.label = 7;
                        case 7:
                            callback(err, true);
                            return [2];
                    }
                });
            }); });
            return [2];
        });
    });
}
exports.default = downloadWorkspaces;
