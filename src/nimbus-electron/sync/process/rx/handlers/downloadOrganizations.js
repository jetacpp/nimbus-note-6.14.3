"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var orgs_1 = __importDefault(require("../../../../db/models/orgs"));
var OrgObjRepository_1 = __importDefault(require("../../repositories/OrgObjRepository"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
function downloadOrganizations(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, skipSyncHandlers;
        var _this = this;
        return __generator(this, function (_a) {
            workspaceId = inputData.workspaceId, skipSyncHandlers = inputData.skipSyncHandlers;
            syncHandler_1.default.log("=> downloadOrganizations start for workspaceId: " + workspaceId);
            syncHandler_1.default.log("=> downloadOrganizations => organizationsGet");
            NimbusSDK_1.default.getApi().organizationsGet({ workspaceId: workspaceId, skipSyncHandlers: skipSyncHandlers }, function (err, organizations) { return __awaiter(_this, void 0, void 0, function () {
                var checkUserOrganizationItem, checkUserRemovedOrganizations, checkUserOrganizationsItems;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            syncHandler_1.default.log("=> downloadOrganizations => organizationsGet => err: " + err);
                            if (!err) return [3, 3];
                            if (!!skipSyncHandlers) return [3, 2];
                            return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                        case 1:
                            _a.sent();
                            _a.label = 2;
                        case 2:
                            errorHandler_1.default.log({
                                err: err, response: organizations,
                                description: "Sync problem => downloadOrganizations => getOrganizations"
                            });
                            return [2, callback(err, false)];
                        case 3:
                            checkUserOrganizationItem = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    return [2, new Promise(function (resolve) {
                                            var organization = inputData.organization, skipSyncHandlers = inputData.skipSyncHandlers;
                                            var id = organization.id;
                                            OrgObjRepository_1.default.get({ id: id }, function (err, organizationObj) { return __awaiter(_this, void 0, void 0, function () {
                                                var createUserOrganizationObj;
                                                var _this = this;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            if (err) {
                                                                errorHandler_1.default.log({
                                                                    err: err, response: organizationObj,
                                                                    description: "Sync problem => downloadOrganizations => checkUserOrganizationItem",
                                                                    data: inputData
                                                                });
                                                                return [2, resolve(null)];
                                                            }
                                                            createUserOrganizationObj = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                return __generator(this, function (_a) {
                                                                    return [2, new Promise(function (resolve) {
                                                                            var organization = inputData.organization;
                                                                            OrgObjRepository_1.default.create({ organization: organization }, function (err, organizationObj) {
                                                                                if (err) {
                                                                                    errorHandler_1.default.log({
                                                                                        err: err, response: organizationObj,
                                                                                        description: "Sync problem => downloadOrganizations => createUserOrganizationObj",
                                                                                        data: inputData
                                                                                    });
                                                                                    return resolve(null);
                                                                                }
                                                                                resolve(organizationObj);
                                                                            });
                                                                        })];
                                                                });
                                                            }); };
                                                            if (!!organizationObj) return [3, 2];
                                                            return [4, createUserOrganizationObj({ organization: organization })];
                                                        case 1:
                                                            organizationObj = _a.sent();
                                                            _a.label = 2;
                                                        case 2:
                                                            if (!organizationObj) {
                                                                return [2, resolve(null)];
                                                            }
                                                            organizationObj.id = organization.id;
                                                            organizationObj.type = organization.type;
                                                            organizationObj.serviceType = organization.serviceType;
                                                            organizationObj.title = organization.title;
                                                            organizationObj.description = organization.description;
                                                            organizationObj.usage = organization.usage;
                                                            organizationObj.limits = organization.limits;
                                                            organizationObj.features = organization.features;
                                                            organizationObj.user = organization.user;
                                                            organizationObj.sub = organization.sub;
                                                            organizationObj.suspended = organization.suspended;
                                                            organizationObj.suspendedAt = organization.suspendedAt;
                                                            organizationObj.suspendedReason = organization.suspendedReason;
                                                            organizationObj.access = organization.access;
                                                            organizationObj.smallLogoUrl = organization.smallLogoUrl || null;
                                                            organizationObj.bigLogoUrl = organization.bigLogoUrl || null;
                                                            resolve(organizationObj);
                                                            return [2];
                                                    }
                                                });
                                            }); });
                                        })];
                                });
                            }); };
                            checkUserRemovedOrganizations = function (inputData) {
                                return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var organizations, skipSyncHandlers;
                                    var _this = this;
                                    return __generator(this, function (_a) {
                                        organizations = inputData.organizations, skipSyncHandlers = inputData.skipSyncHandlers;
                                        orgs_1.default.findAll({}, {}, function (err, localOrganizations) { return __awaiter(_this, void 0, void 0, function () {
                                            var organizationsIdList, removeUserOrganization, _i, localOrganizations_1, localOrganization;
                                            var _this = this;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        if (err || !localOrganizations) {
                                                            localOrganizations = [];
                                                        }
                                                        organizationsIdList = organizations.map(function (organization) {
                                                            return organization.id;
                                                        });
                                                        removeUserOrganization = function (organization) {
                                                            return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                                var count;
                                                                return __generator(this, function (_a) {
                                                                    switch (_a.label) {
                                                                        case 0: return [4, OrgObjRepository_1.default.remove({ organizationObjs: organization })];
                                                                        case 1:
                                                                            count = _a.sent();
                                                                            return [2, resolve(count)];
                                                                    }
                                                                });
                                                            }); });
                                                        };
                                                        _i = 0, localOrganizations_1 = localOrganizations;
                                                        _a.label = 1;
                                                    case 1:
                                                        if (!(_i < localOrganizations_1.length)) return [3, 4];
                                                        localOrganization = localOrganizations_1[_i];
                                                        if (localOrganization.type !== orgs_1.default.TYPE_BUSINESS) {
                                                            return [3, 3];
                                                        }
                                                        if (!(organizationsIdList.indexOf(localOrganization.id) < 0)) return [3, 3];
                                                        return [4, removeUserOrganization(localOrganization)];
                                                    case 2:
                                                        _a.sent();
                                                        _a.label = 3;
                                                    case 3:
                                                        _i++;
                                                        return [3, 1];
                                                    case 4: return [2, resolve()];
                                                }
                                            });
                                        }); });
                                        return [2];
                                    });
                                }); });
                            };
                            checkUserOrganizationsItems = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                            var organizations, skipSyncHandlers, _i, organizations_1, organization, organizationObj;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        organizations = inputData.organizations, skipSyncHandlers = inputData.skipSyncHandlers;
                                                        _i = 0, organizations_1 = organizations;
                                                        _a.label = 1;
                                                    case 1:
                                                        if (!(_i < organizations_1.length)) return [3, 5];
                                                        organization = organizations_1[_i];
                                                        return [4, checkUserOrganizationItem({ organization: organization, skipSyncHandlers: skipSyncHandlers })];
                                                    case 2:
                                                        organizationObj = _a.sent();
                                                        if (!organizationObj) return [3, 4];
                                                        return [4, OrgObjRepository_1.default.update({ orgObjs: organizationObj })];
                                                    case 3:
                                                        _a.sent();
                                                        _a.label = 4;
                                                    case 4:
                                                        _i++;
                                                        return [3, 1];
                                                    case 5:
                                                        resolve(true);
                                                        return [2];
                                                }
                                            });
                                        }); })];
                                });
                            }); };
                            if (!organizations) return [3, 6];
                            return [4, checkUserRemovedOrganizations({ organizations: organizations, skipSyncHandlers: skipSyncHandlers })];
                        case 4:
                            _a.sent();
                            return [4, checkUserOrganizationsItems({ organizations: organizations, skipSyncHandlers: skipSyncHandlers })];
                        case 5:
                            _a.sent();
                            _a.label = 6;
                        case 6:
                            callback(err, true);
                            return [2];
                    }
                });
            }); });
            return [2];
        });
    });
}
exports.default = downloadOrganizations;
