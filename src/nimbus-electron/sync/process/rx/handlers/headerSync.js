"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var fastDownload_1 = __importDefault(require("./fastDownload"));
var userInfo_1 = __importDefault(require("./userInfo"));
var fastUpload_1 = __importDefault(require("./fastUpload"));
var fullUpload_1 = __importDefault(require("./fullUpload"));
var clearRemovedData_1 = __importDefault(require("./clearRemovedData"));
var clearSocketStore_1 = __importDefault(require("./clearSocketStore"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
function headerSync(inputData, callback) {
    var _this = this;
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return new Promise(function () { return __awaiter(_this, void 0, void 0, function () {
        var workspaceId, fastUserDownload, getUserInfo, fastUserUpload, fullUserUpload, clearUserRemovedData, err, syncStatus;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId;
                    fastUserDownload = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    fastDownload_1.default(inputData, function (err) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: null,
                                                description: "Sync problem => headerSync => fastUserDownload",
                                                data: inputData
                                            });
                                        }
                                        resolve(err);
                                    });
                                })];
                        });
                    }); };
                    getUserInfo = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    userInfo_1.default(__assign(__assign({}, inputData), { saveUserInfo: true }), function (err) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: null,
                                                description: "Sync problem => headerSync => getUserInfo",
                                                data: inputData
                                            });
                                        }
                                        resolve(err);
                                    });
                                })];
                        });
                    }); };
                    fastUserUpload = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    fastUpload_1.default(inputData, function (err) {
                                        if (err) {
                                            var errorData = {
                                                err: err, response: null,
                                                description: "Sync problem => headerSync => fastUserUpload",
                                                data: inputData
                                            };
                                            errorHandler_1.default.displayErrorPopup(errorData);
                                            errorHandler_1.default.log(errorData);
                                        }
                                        resolve(err);
                                    });
                                })];
                        });
                    }); };
                    fullUserUpload = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    fullUpload_1.default(inputData, function (err) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: null,
                                                description: "Sync problem => headerSync => fullUserUpload",
                                                data: inputData
                                            });
                                        }
                                        resolve(err);
                                    });
                                })];
                        });
                    }); };
                    clearUserRemovedData = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    clearRemovedData_1.default(inputData, function (err) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: null,
                                                description: "Sync problem => headerSync => clearUserRemovedData",
                                                data: inputData
                                            });
                                        }
                                        resolve(err);
                                    });
                                })];
                        });
                    }); };
                    syncHandler_1.default.log("sync => headerSync => start (ws: " + workspaceId + ")");
                    clearSocketStore_1.default({ workspaceId: workspaceId });
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 1:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    SyncStatusChangedEvent_1.default.setStatus({ workspaceId: workspaceId, newStatus: SyncStatusChangedEvent_1.default.STATUS.HEADER_START });
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 2:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => headerSync => fastUserDownload (ws: " + workspaceId + ")");
                    return [4, fastUserDownload(inputData)];
                case 3:
                    if (err = _a.sent()) {
                        return [2, callback(err, false)];
                    }
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 4:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => headerSync => getUserInfo (ws: " + workspaceId + ")");
                    return [4, getUserInfo(inputData)];
                case 5:
                    if (err = _a.sent()) {
                        return [2, callback(err, false)];
                    }
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 6:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => headerSync => fastUserUpload (ws: " + workspaceId + ")");
                    return [4, fastUserUpload(inputData)];
                case 7:
                    if (err = _a.sent()) {
                        return [2, callback(err, false)];
                    }
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 8:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => headerSync => fullUserUpload (ws: " + workspaceId + ")");
                    return [4, fullUserUpload(inputData)];
                case 9:
                    if (err = _a.sent()) {
                        return [2, callback(err, false)];
                    }
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 10:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => headerSync => clearUserRemovedData (ws: " + workspaceId + ")");
                    return [4, clearUserRemovedData(inputData)];
                case 11:
                    if (err = _a.sent()) {
                        return [2, callback(err, false)];
                    }
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 12:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    return [4, SyncStatusChangedEvent_1.default.getStatus({ workspaceId: workspaceId })];
                case 13:
                    syncStatus = _a.sent();
                    if (syncStatus !== SyncStatusChangedEvent_1.default.STATUS.FAILED) {
                        SyncStatusChangedEvent_1.default.setStatus({ workspaceId: workspaceId, newStatus: SyncStatusChangedEvent_1.default.STATUS.HEADER_FINISH });
                    }
                    callback(err, true);
                    return [2];
            }
        });
    }); });
}
exports.default = headerSync;
