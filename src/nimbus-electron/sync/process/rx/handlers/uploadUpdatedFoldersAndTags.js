"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../../config"));
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var SyncStatusDisplayEvent_1 = __importDefault(require("../../events/SyncStatusDisplayEvent"));
var sessionLastUpdateTime_1 = __importDefault(require("./sessionLastUpdateTime"));
var FolderObjRepository_1 = __importDefault(require("../../repositories/FolderObjRepository"));
var TagObjRepository_1 = __importDefault(require("../../repositories/TagObjRepository"));
var NotesUpdateRequest_1 = __importDefault(require("../../../nimbussdk/net/request/NotesUpdateRequest"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
function uploadUpdatedFoldersAndTags(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, getUpdatedFoldersForUploadOnServer, getRenamedTagsForUploadOnServer, getUpdatedTagsForUploadOnServer, folders, renameTags, storeTags, makeRequest, body;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId;
                    syncHandler_1.default.log("=> uploadUpdatedFoldersAndTags => start for workspaceId: " + workspaceId);
                    getUpdatedFoldersForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    FolderObjRepository_1.default.getUpdatedFoldersForUploadOnServer(inputData, function (err, folders) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: folders,
                                                description: "Sync problem => uploadUpdatedFoldersAndTags => getUpdatedFoldersForUploadOnServer",
                                                data: inputData
                                            });
                                        }
                                        resolve(folders);
                                    });
                                })];
                        });
                    }); };
                    getRenamedTagsForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    TagObjRepository_1.default.getRenamedTagsForUploadOnServer(inputData, function (err, renameTags) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: renameTags,
                                                description: "Sync problem => uploadUpdatedFoldersAndTags => getRenamedTagsForUploadOnServer",
                                                data: inputData
                                            });
                                        }
                                        resolve(renameTags);
                                    });
                                })];
                        });
                    }); };
                    getUpdatedTagsForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    TagObjRepository_1.default.getUpdatedTagsForUploadOnServer(inputData, function (err, storeTags) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: storeTags,
                                                description: "Sync problem => uploadUpdatedFoldersAndTags => getUpdatedTagsForUploadOnServer",
                                                data: inputData
                                            });
                                        }
                                        resolve(storeTags);
                                    });
                                })];
                        });
                    }); };
                    syncHandler_1.default.log("=> uploadUpdatedFoldersAndTags => getUpdatedFoldersForUploadOnServer");
                    return [4, getUpdatedFoldersForUploadOnServer(inputData)];
                case 1:
                    folders = _a.sent();
                    syncHandler_1.default.log("=> uploadUpdatedFoldersAndTags => getRenamedTagsForUploadOnServer");
                    return [4, getRenamedTagsForUploadOnServer(inputData)];
                case 2:
                    renameTags = _a.sent();
                    syncHandler_1.default.log("=> uploadUpdatedFoldersAndTags => getUpdatedTagsForUploadOnServer");
                    return [4, getUpdatedTagsForUploadOnServer(inputData)];
                case 3:
                    storeTags = _a.sent();
                    makeRequest = folders.length || renameTags.length || storeTags.length;
                    if (!makeRequest) {
                        return [2, callback(null, true)];
                    }
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 4:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    if (!folders.length) return [3, 6];
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.UPLOAD_FOLDERS
                        })];
                case 5:
                    _a.sent();
                    _a.label = 6;
                case 6: return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 7:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    if (!renameTags.length) return [3, 9];
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.UPLOAD_TAGS
                        })];
                case 8:
                    _a.sent();
                    _a.label = 9;
                case 9:
                    syncHandler_1.default.log("=> uploadUpdatedFoldersAndTags => NotesUpdateRequest.Body => workspaceId: " + workspaceId);
                    body = new NotesUpdateRequest_1.default.Body({});
                    body.storeFolders(folders);
                    body.setRenameTags(renameTags);
                    body.storeTags(storeTags);
                    syncHandler_1.default.log(body);
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 10:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("=> uploadUpdatedFoldersAndTags => updateNotes");
                    NimbusSDK_1.default.getApi().updateNotes({ workspaceId: workspaceId, body: body }, function (err, response) {
                        return __awaiter(this, void 0, void 0, function () {
                            var processUploadResponse;
                            var _this = this;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        syncHandler_1.default.log("=> uploadUpdatedFoldersAndTags => updateNotes => err: " + err);
                                        if (!err) return [3, 2];
                                        return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                    case 1:
                                        _a.sent();
                                        errorHandler_1.default.log({
                                            err: err, response: response,
                                            description: "Sync problem => uploadUpdatedFoldersAndTags => updateNotes"
                                        });
                                        return [2, callback(err, false)];
                                    case 2:
                                        processUploadResponse = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                            var _this = this;
                                            return __generator(this, function (_a) {
                                                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                        var workspaceId, folders, renameTags, storeTags;
                                                        return __generator(this, function (_a) {
                                                            switch (_a.label) {
                                                                case 0:
                                                                    workspaceId = inputData.workspaceId, folders = inputData.folders, renameTags = inputData.renameTags, storeTags = inputData.storeTags;
                                                                    return [4, FolderObjRepository_1.default.callAfterUploadExistFoldersOnServerI({ workspaceId: workspaceId, folders: folders })];
                                                                case 1:
                                                                    _a.sent();
                                                                    return [4, TagObjRepository_1.default.callAfterUploadStoreTagsI({ workspaceId: workspaceId, tags: storeTags })];
                                                                case 2:
                                                                    _a.sent();
                                                                    return [4, TagObjRepository_1.default.callAfterUploadRenamedTagsI({ workspaceId: workspaceId, tags: renameTags })];
                                                                case 3:
                                                                    _a.sent();
                                                                    resolve(true);
                                                                    return [2];
                                                            }
                                                        });
                                                    }); })];
                                            });
                                        }); };
                                        syncHandler_1.default.log("=> uploadUpdatedFoldersAndTags => updateNotes => processUploadResponse");
                                        return [4, processUploadResponse({ workspaceId: workspaceId, folders: folders, renameTags: renameTags, storeTags: storeTags })];
                                    case 3:
                                        _a.sent();
                                        syncHandler_1.default.log("=> uploadUpdatedFoldersAndTags => updateNotes => response");
                                        if (response && response.last_update_time) {
                                            sessionLastUpdateTime_1.default.setSyncSessionUpdateTime({ workspaceId: workspaceId, lastUpdateTime: response.last_update_time });
                                        }
                                        else {
                                            if (config_1.default.SHOW_WEB_CONSOLE) {
                                                console.log("Set Sync Session Update Time fail: (ws: " + workspaceId + ")", response);
                                            }
                                            errorHandler_1.default.log({
                                                err: err, response: response,
                                                description: "Sync problem => uploadUpdatedFoldersAndTags"
                                            });
                                        }
                                        syncHandler_1.default.log("=> uploadUpdatedFoldersAndTags => done");
                                        callback(err, true);
                                        return [2];
                                }
                            });
                        });
                    });
                    return [2];
            }
        });
    });
}
exports.default = uploadUpdatedFoldersAndTags;
