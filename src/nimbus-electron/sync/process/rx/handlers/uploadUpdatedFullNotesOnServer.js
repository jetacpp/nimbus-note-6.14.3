"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../../config"));
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var SyncStatusDisplayEvent_1 = __importDefault(require("../../events/SyncStatusDisplayEvent"));
var sessionLastUpdateTime_1 = __importDefault(require("./sessionLastUpdateTime"));
var NoteObjRepository_1 = __importDefault(require("../../repositories/NoteObjRepository"));
var TagObjRepository_1 = __importDefault(require("../../repositories/TagObjRepository"));
var TodoObjRepository_1 = __importDefault(require("../../repositories/TodoObjRepository"));
var AttachmentObjRepository_1 = __importDefault(require("../../repositories/AttachmentObjRepository"));
var NotesUpdateRequest_1 = __importDefault(require("../../../nimbussdk/net/request/NotesUpdateRequest"));
var uploadUpdatedAttachmentsOnServer_1 = __importDefault(require("./uploadUpdatedAttachmentsOnServer"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var item_1 = __importDefault(require("../../../../db/models/item"));
var TextEditor_1 = __importDefault(require("../../../../db/TextEditor"));
var socketFunctions_1 = __importDefault(require("../../../../sync/socket/socketFunctions"));
var API_1 = __importDefault(require("../../../nimbussdk/net/API"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
function uploadUpdatedFullNotesOnServer(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, syncStartDate, getUpdatedFullNotesCountForUploadOnServer, getUpdatedNotesForUploadOnServer, checkIfNoteCanBePassedInAvailableTrafficQuotaForUploadOnServer, checkIfNoteNotMoreThanLimitNoteSize, uploadUserUpdatedAttachmentsOnServer, getSyncNoteEntity, getSyncTodoEntityList, getRenamedAttachmentsForUploadOnServer, getRenamedTagsForUploadOnServer, getUpdatedTagsForUploadOnServer, uploadNotesOnServer, availableNoteCount, noteObjs, uploadNotesList, _i, noteObjs_1, noteObj, noteGlobalId, noteCanBePassed, noteNotMoreThanLimitSize, note, noteTodos, attachments, renamedAttachments, notesUploadResult;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId, syncStartDate = inputData.syncStartDate;
                    syncHandler_1.default.log("sync => fullUserUpload => uploadUpdatedFullNotesOnServer => start (ws: " + workspaceId + ")");
                    getUpdatedFullNotesCountForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    NoteObjRepository_1.default.getUpdatedFullNotesCountForUploadOnServer(inputData, function (err, availableNoteCount) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: availableNoteCount,
                                                description: "Sync problem => uploadUpdatedFullNotesOnServer => getUpdatedFullNotesCountForUploadOnServer",
                                                data: inputData
                                            });
                                            return resolve(0);
                                        }
                                        resolve(availableNoteCount);
                                    });
                                })];
                        });
                    }); };
                    getUpdatedNotesForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    NoteObjRepository_1.default.getUpdatedNotesForUploadOnServer(inputData, function (err, noteObjs) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: noteObjs,
                                                description: "Sync problem => uploadUpdatedFullNotesOnServer => getUpdatedNotesForUploadOnServer",
                                                data: inputData
                                            });
                                            return resolve([]);
                                        }
                                        resolve(noteObjs);
                                    });
                                })];
                        });
                    }); };
                    checkIfNoteCanBePassedInAvailableTrafficQuotaForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    NoteObjRepository_1.default.checkIfNoteCanBePassedInAvailableTrafficQuotaForUploadOnServer(inputData, function (err, noteCanBePassed) {
                                        if (err || !noteCanBePassed) {
                                            errorHandler_1.default.log({
                                                err: err, response: noteCanBePassed,
                                                description: "Sync problem => uploadUpdatedFullNotesOnServer => checkIfNoteCanBePassedInAvailableTrafficQuotaForUploadOnServer",
                                                data: inputData
                                            });
                                            return resolve(false);
                                        }
                                        resolve(noteCanBePassed);
                                    });
                                })];
                        });
                    }); };
                    checkIfNoteNotMoreThanLimitNoteSize = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    NoteObjRepository_1.default.checkIfNoteNotMoreThanLimitNoteSize(inputData, function (err, noteNotMoreThanLimitSize) {
                                        if (err || !noteNotMoreThanLimitSize) {
                                            errorHandler_1.default.log({
                                                err: err, response: noteNotMoreThanLimitSize,
                                                description: "Sync problem => uploadUpdatedFullNotesOnServer => checkIfNoteNotMoreThanLimitNoteSize",
                                                data: inputData
                                            });
                                            return resolve(false);
                                        }
                                        resolve(noteNotMoreThanLimitSize);
                                    });
                                })];
                        });
                    }); };
                    uploadUserUpdatedAttachmentsOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    uploadUpdatedAttachmentsOnServer_1.default(inputData, function (err, attachments) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: attachments,
                                                description: "Sync problem => uploadUpdatedFullNotesOnServer => uploadUserUpdatedAttachmentsOnServer",
                                                data: inputData
                                            });
                                            return resolve([]);
                                        }
                                        resolve(attachments);
                                    });
                                })];
                        });
                    }); };
                    getSyncNoteEntity = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    NoteObjRepository_1.default.getSyncNoteEntity(inputData, function (err, note) {
                                        if (err || !note) {
                                            errorHandler_1.default.log({
                                                err: err, response: note,
                                                description: "Sync problem => uploadUpdatedFullNotesOnServer => getSyncNoteEntity",
                                                data: inputData
                                            });
                                            return resolve(null);
                                        }
                                        note.date_updated_user = note.date_updated_user ? note.date_updated_user : note.date_added_user;
                                        resolve(note);
                                    });
                                })];
                        });
                    }); };
                    getSyncTodoEntityList = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    TodoObjRepository_1.default.getSyncTodoEntityList(inputData, function (err, noteTodos) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: noteTodos,
                                                description: "Sync problem => uploadUpdatedFullNotesOnServer => getSyncTodoEntityList",
                                                data: inputData
                                            });
                                            return resolve([]);
                                        }
                                        if (!noteTodos.length) {
                                            return resolve([]);
                                        }
                                        resolve(noteTodos.map(function (noteTodo) { return (__assign({}, noteTodo)); }));
                                    });
                                })];
                        });
                    }); };
                    getRenamedAttachmentsForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    AttachmentObjRepository_1.default.getRenamedAttachmentsForUploadOnServer(inputData, function (err, renamedAttachments) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: renamedAttachments,
                                                description: "Sync problem => uploadUpdatedFullNotesOnServer => getRenamedAttachmentsForUploadOnServer",
                                                data: inputData
                                            });
                                            return resolve([]);
                                        }
                                        resolve(renamedAttachments);
                                    });
                                })];
                        });
                    }); };
                    getRenamedTagsForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    TagObjRepository_1.default.getRenamedTagsForUploadOnServer(inputData, function (err, renameTags) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: renameTags,
                                                description: "Sync problem => uploadUpdatedFullNotesOnServer => getRenamedAttachmentsForUploadOnServer",
                                                data: inputData
                                            });
                                        }
                                        resolve(renameTags);
                                    });
                                })];
                        });
                    }); };
                    getUpdatedTagsForUploadOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    TagObjRepository_1.default.getUpdatedTagsForUploadOnServer(inputData, function (err, storeTags) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: storeTags,
                                                description: "Sync problem => uploadUpdatedFullNotesOnServer => getUpdatedTagsForUploadOnServer",
                                                data: inputData
                                            });
                                        }
                                        resolve(storeTags);
                                    });
                                })];
                        });
                    }); };
                    uploadNotesOnServer = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, notesForUpload, body, notes, renameTags, storeTags;
                                    var _this = this;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                workspaceId = inputData.workspaceId, notesForUpload = inputData.notesForUpload;
                                                syncHandler_1.default.log("=> uploadNotesOnServer => start for workspaceId: " + workspaceId);
                                                body = new NotesUpdateRequest_1.default.Body({});
                                                notes = notesForUpload.map(function (note) { return NoteObjRepository_1.default.cleanUploadItem(note); });
                                                return [4, getRenamedTagsForUploadOnServer(inputData)];
                                            case 1:
                                                renameTags = _a.sent();
                                                return [4, getUpdatedTagsForUploadOnServer(inputData)];
                                            case 2:
                                                storeTags = _a.sent();
                                                body.setRenameTags(renameTags);
                                                body.storeTags(storeTags);
                                                body.storeNotes(notes.filter(function (note) { return note.global_id; }));
                                                body.removeAttachements();
                                                return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                            case 3:
                                                if (_a.sent())
                                                    return [2, resolve(false)];
                                                syncHandler_1.default.log("=> uploadNotesOnServer => updateNotes => body");
                                                NimbusSDK_1.default.getApi().updateNotes({ workspaceId: workspaceId, body: body }, function (err, responseBody) { return __awaiter(_this, void 0, void 0, function () {
                                                    var errorData, errData_1, noteItem, noteCallAfterUploadFullSyncI, attachmentCallAfterUploadFullSyncI, attachmentCallAfterUploadRenamedAttachmentsI, updateNotesIdList, _i, notes_1, note;
                                                    var _this = this;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                syncHandler_1.default.log("=> uploadNotesOnServer => updateNotes => err: " + err);
                                                                if (!(err || !responseBody)) return [3, 7];
                                                                return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                                            case 1:
                                                                _a.sent();
                                                                errorData = {
                                                                    err: err, response: responseBody,
                                                                    description: "Sync problem => uploadUpdatedFullNotesOnServer => updateNotes",
                                                                    data: inputData
                                                                };
                                                                errorHandler_1.default.log(errorData);
                                                                errorHandler_1.default.displayErrorPopup(errorData);
                                                                if (!(err === API_1.default.ERROR_TEMPFILE_NOT_FOUND)) return [3, 3];
                                                                return [4, checkFailedItem({
                                                                        workspaceId: workspaceId,
                                                                        responseBody: responseBody
                                                                    })];
                                                            case 2:
                                                                _a.sent();
                                                                return [3, 6];
                                                            case 3:
                                                                if (!(err === API_1.default.ERROR_ATTACHMENT_NOT_FOUND)) return [3, 6];
                                                                if (!(responseBody && responseBody.body && responseBody.body._errorDesc)) return [3, 6];
                                                                errData_1 = responseBody.body._errorDesc.split(' ');
                                                                if (!(typeof errData_1[1] !== 'undefined' && errData_1[1] && errData_1[1].length > 10)) return [3, 6];
                                                                return [4, AttachmentObjRepository_1.default.eraseAttachmentFile({
                                                                        workspaceId: workspaceId,
                                                                        globalId: errData_1[1],
                                                                    })];
                                                            case 4:
                                                                _a.sent();
                                                                if (!(body && body.store && body.store.notes)) return [3, 6];
                                                                noteItem = body.store.notes.find(function (note) {
                                                                    return note.preview && (note.preview.global_id === errData_1[1]);
                                                                });
                                                                if (!(noteItem && noteItem.global_id)) return [3, 6];
                                                                return [4, NoteObjRepository_1.default.removeItemPreview({
                                                                        workspaceId: workspaceId,
                                                                        globalId: noteItem.global_id,
                                                                    })];
                                                            case 5:
                                                                _a.sent();
                                                                _a.label = 6;
                                                            case 6: return [2, resolve(false)];
                                                            case 7:
                                                                noteCallAfterUploadFullSyncI = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                    var _this = this;
                                                                    return __generator(this, function (_a) {
                                                                        return [2, new Promise(function (resolve) {
                                                                                var note = inputData.note;
                                                                                NoteObjRepository_1.default.callAfterUploadFullSyncI(inputData, function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                                                                                    var noteObj, noteObjJson, workspaceInstance, _a, saveTextDate, saveTextRes;
                                                                                    return __generator(this, function (_b) {
                                                                                        switch (_b.label) {
                                                                                            case 0:
                                                                                                if (err) {
                                                                                                    errorHandler_1.default.log({
                                                                                                        err: err, response: response,
                                                                                                        description: "Sync problem => uploadUpdatedFullNotesOnServer => noteCallAfterUploadFullSyncI",
                                                                                                        data: {
                                                                                                            note: note
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                                if (!(note && note.global_id && note.noteObj)) return [3, 7];
                                                                                                if (!(note.noteObj.text_version > 1)) return [3, 6];
                                                                                                noteObj = note.noteObj;
                                                                                                noteObjJson = item_1.default.getResponseJson(noteObj);
                                                                                                if (!noteObjJson) return [3, 6];
                                                                                                noteObjJson['synced'] = true;
                                                                                                if (!workspaceId) return [3, 2];
                                                                                                return [4, workspace_1.default.getById(workspaceId)];
                                                                                            case 1:
                                                                                                _a = _b.sent();
                                                                                                return [3, 4];
                                                                                            case 2: return [4, workspace_1.default.getDefault()];
                                                                                            case 3:
                                                                                                _a = _b.sent();
                                                                                                _b.label = 4;
                                                                                            case 4:
                                                                                                workspaceInstance = (_a);
                                                                                                if (!workspaceInstance) return [3, 6];
                                                                                                saveTextDate = {
                                                                                                    note: noteObjJson,
                                                                                                    workspaceId: workspaceInstance.globalId,
                                                                                                    noteId: noteObj.globalId
                                                                                                };
                                                                                                return [4, TextEditor_1.default.makeBgNoteSync(saveTextDate)];
                                                                                            case 5:
                                                                                                saveTextRes = _b.sent();
                                                                                                if (saveTextRes && !saveTextRes.result) {
                                                                                                    syncHandler_1.default.sendLog({
                                                                                                        err: new Error("Can not upload note text v2: " + workspaceInstance.globalId + "_" + noteObj.globalId),
                                                                                                        response: saveTextRes,
                                                                                                        description: "Sync problem => uploadNotesOnServer => makeBgNoteSync",
                                                                                                        data: saveTextDate
                                                                                                    });
                                                                                                }
                                                                                                _b.label = 6;
                                                                                            case 6:
                                                                                                delete note.noteObj;
                                                                                                _b.label = 7;
                                                                                            case 7:
                                                                                                resolve(true);
                                                                                                return [2];
                                                                                        }
                                                                                    });
                                                                                }); });
                                                                            })];
                                                                    });
                                                                }); };
                                                                attachmentCallAfterUploadFullSyncI = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                    return __generator(this, function (_a) {
                                                                        return [2, new Promise(function (resolve) {
                                                                                var workspaceId = inputData.workspaceId, note = inputData.note;
                                                                                AttachmentObjRepository_1.default.callAfterUploadFullSyncI({
                                                                                    workspaceId: workspaceId,
                                                                                    parentId: note.globalId
                                                                                }, function (err, response) {
                                                                                    if (err) {
                                                                                        errorHandler_1.default.log({
                                                                                            err: err, response: response,
                                                                                            description: "Sync problem => uploadUpdatedFullNotesOnServer => attachmentCallAfterUploadFullSyncI",
                                                                                            data: inputData
                                                                                        });
                                                                                    }
                                                                                    resolve(true);
                                                                                });
                                                                            })];
                                                                    });
                                                                }); };
                                                                attachmentCallAfterUploadRenamedAttachmentsI = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                    return __generator(this, function (_a) {
                                                                        return [2, new Promise(function (resolve) {
                                                                                var workspaceId = inputData.workspaceId, note = inputData.note;
                                                                                AttachmentObjRepository_1.default.callAfterUploadRenamedAttachmentsI({
                                                                                    workspaceId: workspaceId,
                                                                                    parentId: note.globalId
                                                                                }, function (err, response) {
                                                                                    if (err) {
                                                                                        errorHandler_1.default.log({
                                                                                            err: err, response: response,
                                                                                            description: "Sync problem => uploadUpdatedFullNotesOnServer => attachmentCallAfterUploadRenamedAttachmentsI",
                                                                                            data: inputData
                                                                                        });
                                                                                    }
                                                                                    resolve(note);
                                                                                });
                                                                            })];
                                                                    });
                                                                }); };
                                                                updateNotesIdList = [];
                                                                _i = 0, notes_1 = notes;
                                                                _a.label = 8;
                                                            case 8:
                                                                if (!(_i < notes_1.length)) return [3, 18];
                                                                note = notes_1[_i];
                                                                return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                            case 9:
                                                                if (_a.sent()) {
                                                                    return [3, 18];
                                                                }
                                                                if (!(responseBody && responseBody.date_updated && typeof (responseBody.date_updated[note.global_id]) !== "undefined")) return [3, 11];
                                                                return [4, noteCallAfterUploadFullSyncI({
                                                                        workspaceId: workspaceId,
                                                                        note: note,
                                                                        lastUpdateTime: responseBody.date_updated[note.global_id],
                                                                        syncStartDate: syncStartDate
                                                                    })];
                                                            case 10:
                                                                _a.sent();
                                                                updateNotesIdList.push(note.global_id);
                                                                _a.label = 11;
                                                            case 11: return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                            case 12:
                                                                if (_a.sent()) {
                                                                    return [3, 18];
                                                                }
                                                                return [4, attachmentCallAfterUploadFullSyncI({ workspaceId: workspaceId, note: note })];
                                                            case 13:
                                                                _a.sent();
                                                                return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                            case 14:
                                                                if (_a.sent()) {
                                                                    return [3, 18];
                                                                }
                                                                return [4, attachmentCallAfterUploadRenamedAttachmentsI({ workspaceId: workspaceId, note: note })];
                                                            case 15:
                                                                _a.sent();
                                                                return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                            case 16:
                                                                if (_a.sent()) {
                                                                    return [3, 18];
                                                                }
                                                                socketFunctions_1.default.sendItemUpdateMessage({ workspaceId: workspaceId, globalId: note.global_id });
                                                                _a.label = 17;
                                                            case 17:
                                                                _i++;
                                                                return [3, 8];
                                                            case 18:
                                                                if (!(responseBody && responseBody.last_update_time)) return [3, 22];
                                                                return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                            case 19:
                                                                if (_a.sent())
                                                                    return [2, resolve(false)];
                                                                sessionLastUpdateTime_1.default.setSyncSessionUpdateTime({ workspaceId: workspaceId, lastUpdateTime: responseBody.last_update_time });
                                                                return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                                                            case 20:
                                                                if (_a.sent())
                                                                    return [2, resolve(false)];
                                                                return [4, AttachmentObjRepository_1.default.updateLocations({ workspaceId: workspaceId, savedAttachments: responseBody.savedAttachments })];
                                                            case 21:
                                                                _a.sent();
                                                                return [3, 23];
                                                            case 22:
                                                                if (config_1.default.SHOW_WEB_CONSOLE) {
                                                                    console.log("Set Sync Session Update Time fail (ws: " + workspaceId + "):", responseBody);
                                                                }
                                                                errorHandler_1.default.log({
                                                                    err: err, response: responseBody,
                                                                    description: "Sync problem => uploadUpdatedFullNotesOnServer"
                                                                });
                                                                _a.label = 23;
                                                            case 23:
                                                                resolve(true);
                                                                return [2];
                                                        }
                                                    });
                                                }); });
                                                return [2];
                                        }
                                    });
                                }); })];
                        });
                    }); };
                    return [4, getUpdatedFullNotesCountForUploadOnServer(inputData)];
                case 1:
                    availableNoteCount = _a.sent();
                    if (availableNoteCount) {
                        syncHandler_1.default.log("sync => uploadUpdatedFullNotesOnServer => availableNoteCount: " + availableNoteCount + " (ws: " + workspaceId + ")");
                    }
                    else {
                        syncHandler_1.default.log("sync => uploadUpdatedFullNotesOnServer => All notes uploaded. No notes to upload: " + availableNoteCount + " (ws: " + workspaceId + ")");
                        return [2, callback(null, true)];
                    }
                    return [4, getUpdatedNotesForUploadOnServer(inputData)];
                case 2:
                    noteObjs = _a.sent();
                    if (!noteObjs.length) {
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("No notes for upload (ws: " + workspaceId + ")");
                        }
                        return [2, callback(null, true)];
                    }
                    uploadNotesList = [];
                    _i = 0, noteObjs_1 = noteObjs;
                    _a.label = 3;
                case 3:
                    if (!(_i < noteObjs_1.length)) return [3, 17];
                    noteObj = noteObjs_1[_i];
                    noteGlobalId = noteObj.globalId;
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 4:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    return [4, checkIfNoteCanBePassedInAvailableTrafficQuotaForUploadOnServer({
                            workspaceId: workspaceId,
                            noteGlobalId: noteGlobalId
                        })];
                case 5:
                    noteCanBePassed = _a.sent();
                    if (config_1.default.SHOW_WEB_CONSOLE) {
                    }
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 6:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    if (!!noteCanBePassed) return [3, 8];
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.TRAFFIC_LIMIT
                        })];
                case 7:
                    _a.sent();
                    errorHandler_1.default.log({
                        err: null, response: null,
                        description: "Sync problem => uploadUpdatedFullNotesOnServer",
                        data: {
                            noteCanBePassed: noteCanBePassed,
                            noteGlobalId: noteGlobalId
                        }
                    });
                    return [2, callback(null, false)];
                case 8: return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 9:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    return [4, checkIfNoteNotMoreThanLimitNoteSize({ workspaceId: workspaceId, noteGlobalId: noteGlobalId })];
                case 10:
                    noteNotMoreThanLimitSize = _a.sent();
                    if (!noteNotMoreThanLimitSize) {
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("noteNotMoreThanLimitSize: " + noteNotMoreThanLimitSize + " " + noteGlobalId + " (ws: " + workspaceId + ")");
                        }
                        errorHandler_1.default.log({
                            err: null, response: null,
                            description: "Sync problem => uploadUpdatedFullNotesOnServer",
                            data: {
                                noteNotMoreThanLimitSize: noteNotMoreThanLimitSize,
                                noteGlobalId: noteGlobalId
                            }
                        });
                        return [3, 16];
                    }
                    return [4, getSyncNoteEntity({ workspaceId: workspaceId, noteGlobalId: noteGlobalId })];
                case 11:
                    note = _a.sent();
                    if (config_1.default.SHOW_WEB_CONSOLE) {
                    }
                    if (!note) {
                        return [3, 16];
                    }
                    return [4, getSyncTodoEntityList({ workspaceId: workspaceId, noteGlobalId: noteGlobalId })];
                case 12:
                    noteTodos = _a.sent();
                    if (config_1.default.SHOW_WEB_CONSOLE) {
                    }
                    if (noteTodos.length) {
                        note.todo = noteTodos;
                    }
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 13:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    return [4, uploadUserUpdatedAttachmentsOnServer({ workspaceId: workspaceId, noteGlobalId: noteGlobalId })];
                case 14:
                    attachments = _a.sent();
                    if (config_1.default.SHOW_WEB_CONSOLE) {
                    }
                    if (attachments.length) {
                        note.setAttachments(attachments);
                    }
                    return [4, getRenamedAttachmentsForUploadOnServer({ workspaceId: workspaceId, noteGlobalId: noteGlobalId })];
                case 15:
                    renamedAttachments = _a.sent();
                    if (config_1.default.SHOW_WEB_CONSOLE) {
                    }
                    if (renamedAttachments.length) {
                        note.setAttachments(renamedAttachments);
                    }
                    if (note.is_encrypted) {
                        if (typeof (note.text) !== "undefined") {
                            delete note.text;
                        }
                        if (typeof (note.text_short) !== "undefined") {
                            delete note.text_short;
                        }
                    }
                    uploadNotesList.push(note);
                    _a.label = 16;
                case 16:
                    _i++;
                    return [3, 3];
                case 17: return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 18:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    if (!uploadNotesList.length) return [3, 20];
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.UPLOAD_NOTES
                        })];
                case 19:
                    _a.sent();
                    _a.label = 20;
                case 20:
                    syncHandler_1.default.log("sync => fullUpload => uploadUpdatedFullNotesOnServer => uploadNotesOnServer (ws: " + workspaceId + ")");
                    return [4, uploadNotesOnServer({ workspaceId: workspaceId, syncStartDate: syncStartDate, notesForUpload: uploadNotesList })];
                case 21:
                    notesUploadResult = _a.sent();
                    if (config_1.default.SHOW_WEB_CONSOLE) {
                    }
                    callback(null, true);
                    return [2];
            }
        });
    });
}
exports.default = uploadUpdatedFullNotesOnServer;
function checkFailedItem(_a) {
    var workspaceId = _a.workspaceId, responseBody = _a.responseBody;
    return __awaiter(this, void 0, void 0, function () {
        var failedItem, type, item, globalId, removeAttachment;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    if (!responseBody) {
                        return [2];
                    }
                    failedItem = responseBody.failedItem;
                    if (!failedItem) {
                        return [2];
                    }
                    type = failedItem.type, item = failedItem.item;
                    if (!item) {
                        return [2];
                    }
                    if (!(type === 'attachment')) return [3, 3];
                    globalId = item.global_id;
                    if (!globalId) {
                        return [2];
                    }
                    return [4, AttachmentObjRepository_1.default.findAttachment({ workspaceId: workspaceId, globalId: globalId })];
                case 1:
                    removeAttachment = _b.sent();
                    if (!removeAttachment) {
                        return [2];
                    }
                    return [4, AttachmentObjRepository_1.default.removeNoteAttachmentList({
                            workspaceId: workspaceId,
                            attachmentList: [removeAttachment]
                        })];
                case 2:
                    _b.sent();
                    _b.label = 3;
                case 3: return [2];
            }
        });
    });
}
