"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var SyncStatusDisplayEvent_1 = __importDefault(require("../../events/SyncStatusDisplayEvent"));
var downloadFolders_1 = __importDefault(require("./downloadFolders"));
var downloadTags_1 = __importDefault(require("./downloadTags"));
var downloadRemovedItems_1 = __importDefault(require("./downloadRemovedItems"));
var downloadAllStructureNotes_1 = __importDefault(require("./downloadAllStructureNotes"));
var clearAllMoreThanLimitInNotes_1 = __importDefault(require("./clearAllMoreThanLimitInNotes"));
var updateFolderCounters_1 = __importDefault(require("./updateFolderCounters"));
var sendSocketStoreMessages_1 = __importDefault(require("./sendSocketStoreMessages"));
var SocketCollection_1 = __importDefault(require("../../../socket/store/SocketCollection"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
function fastDownload(inputData, callback) {
    var _this = this;
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return new Promise(function () { return __awaiter(_this, void 0, void 0, function () {
        var workspaceId, service, downloadUserFolders, downloadUserTags, downloadUserRemovedItems, downloadAllUserStructureNotes, clearAllUserMoreThanLimitInNotes, updateUserFolderCounters;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId, service = inputData.service;
                    syncHandler_1.default.log("sync => fastDownload => start (ws: " + workspaceId + ")");
                    downloadUserFolders = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    downloadFolders_1.default(inputData, function (err, responseFolders) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: responseFolders,
                                                description: "Sync problem => fastDownload => downloadUserFolders",
                                                data: inputData
                                            });
                                            resolve(null);
                                        }
                                        resolve(responseFolders);
                                    });
                                })];
                        });
                    }); };
                    downloadUserTags = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    downloadTags_1.default(inputData, function (err, responseTags) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: responseTags,
                                                description: "Sync problem => fastDownload => downloadUserTags",
                                                data: inputData
                                            });
                                            resolve(null);
                                        }
                                        resolve(responseTags);
                                    });
                                })];
                        });
                    }); };
                    downloadUserRemovedItems = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    downloadRemovedItems_1.default(inputData, function (err, responseRemovedItems) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: responseRemovedItems,
                                                description: "Sync problem => fastDownload => responseRemovedItems",
                                                data: inputData
                                            });
                                            resolve(null);
                                        }
                                        resolve(responseRemovedItems);
                                    });
                                })];
                        });
                    }); };
                    downloadAllUserStructureNotes = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    downloadAllStructureNotes_1.default(inputData, function (err, responseAllNotes) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: responseAllNotes,
                                                description: "Sync problem => fastDownload => downloadAllUserStructureNotes",
                                                data: inputData
                                            });
                                            resolve(null);
                                        }
                                        resolve(responseAllNotes);
                                    });
                                })];
                        });
                    }); };
                    clearAllUserMoreThanLimitInNotes = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    clearAllMoreThanLimitInNotes_1.default(inputData, function (err, responseClearNotes) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: responseClearNotes,
                                                description: "Sync problem => fastDownload => clearAllUserMoreThanLimitInNotes",
                                                data: inputData
                                            });
                                            resolve(null);
                                        }
                                        resolve(responseClearNotes);
                                    });
                                })];
                        });
                    }); };
                    updateUserFolderCounters = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    updateFolderCounters_1.default(inputData, function (err, responseFolderCounters) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: responseFolderCounters,
                                                description: "Sync problem => fastDownload => updateUserFolderCounters",
                                                data: inputData
                                            });
                                            resolve(null);
                                        }
                                        resolve(responseFolderCounters);
                                    });
                                })];
                        });
                    }); };
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 1:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => fastUserDownload => downloadUserFolders (ws: " + workspaceId + ")");
                    return [4, downloadUserFolders(inputData)];
                case 2:
                    _a.sent();
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOADING_META_INFO
                        })];
                case 3:
                    _a.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 4:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    sendSocketStoreMessages_1.default(inputData);
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOADING_META_INFO
                        })];
                case 5:
                    _a.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 6:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => fastUserDownload => downloadUserTags (ws: " + workspaceId + ")");
                    return [4, downloadUserTags(inputData)];
                case 7:
                    _a.sent();
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOADING_META_INFO
                        })];
                case 8:
                    _a.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 9:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    sendSocketStoreMessages_1.default(inputData);
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOADING_META_INFO
                        })];
                case 10:
                    _a.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 11:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => fastUserDownload => downloadUserRemovedItems (ws: " + workspaceId + ")");
                    return [4, downloadUserRemovedItems(inputData)];
                case 12:
                    _a.sent();
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOADING_META_INFO
                        })];
                case 13:
                    _a.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 14:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    sendSocketStoreMessages_1.default(inputData);
                    return [4, SyncStatusDisplayEvent_1.default.set({
                            workspaceId: workspaceId,
                            newStatus: SyncStatusDisplayEvent_1.default.STATUS.DOWNLOADING_META_INFO
                        })];
                case 15:
                    _a.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 16:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => fastUserDownload => downloadAllUserStructureNotes (ws: " + workspaceId + ")");
                    return [4, downloadAllUserStructureNotes(inputData)];
                case 17:
                    _a.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 18:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    SocketCollection_1.default.saveAttachmentsCopy(inputData);
                    sendSocketStoreMessages_1.default(inputData);
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 19:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => fastUserDownload => clearAllUserMoreThanLimitInNotes (ws: " + workspaceId + ")");
                    return [4, clearAllUserMoreThanLimitInNotes(inputData)];
                case 20:
                    _a.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 21:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    sendSocketStoreMessages_1.default(inputData);
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 22:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("sync => fastUserDownload => updateUserFolderCounters (ws: " + workspaceId + ")");
                    return [4, updateUserFolderCounters(inputData)];
                case 23:
                    _a.sent();
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 24:
                    if (_a.sent())
                        return [2, callback(null, false)];
                    sendSocketStoreMessages_1.default(inputData);
                    callback(null, true);
                    return [2];
            }
        });
    }); });
}
exports.default = fastDownload;
