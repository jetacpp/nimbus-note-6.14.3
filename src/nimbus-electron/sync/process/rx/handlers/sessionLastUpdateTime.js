"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var syncSessionLastUpdateTime = {};
var SessionLastUpdateTime = (function () {
    function SessionLastUpdateTime() {
    }
    SessionLastUpdateTime.getSyncSessionUpdateTime = function (inputData) {
        var workspaceId = inputData.workspaceId;
        return typeof (syncSessionLastUpdateTime[workspaceId]) !== 'undefined' ? syncSessionLastUpdateTime[workspaceId] : 0;
    };
    SessionLastUpdateTime.setSyncSessionUpdateTime = function (inputData) {
        var workspaceId = inputData.workspaceId, lastUpdateTime = inputData.lastUpdateTime;
        syncSessionLastUpdateTime[workspaceId] = lastUpdateTime;
    };
    return SessionLastUpdateTime;
}());
exports.default = SessionLastUpdateTime;
