"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var AttachmentObjRepository_1 = __importDefault(require("../../repositories/AttachmentObjRepository"));
var TodoObjRepository_1 = __importDefault(require("../../repositories/TodoObjRepository"));
var TagObjRepository_1 = __importDefault(require("../../repositories/TagObjRepository"));
var NoteObjRepository_1 = __importDefault(require("../../repositories/NoteObjRepository"));
var FolderObjRepository_1 = __importDefault(require("../../repositories/FolderObjRepository"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
function clearRemovedData(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, clearRemovedAttaches, clearRemovedTodos, clearRemovedTags, clearRemovedNotes, clearRemovedFolders;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId;
                    syncHandler_1.default.log("sync => clearRemovedData => start (ws: " + workspaceId + ")");
                    clearRemovedAttaches = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    AttachmentObjRepository_1.default.clearRemovedData(inputData, function (err, response) {
                                        if (err) {
                                            errorHandler_1.default.log({
                                                err: err, response: response,
                                                description: "Sync problem => clearRemovedData => clearRemovedAttaches",
                                                data: inputData
                                            });
                                            return resolve(false);
                                        }
                                        resolve(response);
                                    });
                                })];
                        });
                    }); };
                    clearRemovedTodos = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    TodoObjRepository_1.default.clearRemovedData(inputData, function (err, response) {
                                        if (err) {
                                            return resolve(false);
                                        }
                                        resolve(response);
                                    });
                                })];
                        });
                    }); };
                    clearRemovedTags = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    TagObjRepository_1.default.clearRemovedData(inputData, function (err, response) {
                                        if (err) {
                                            return resolve(false);
                                        }
                                        resolve(response);
                                    });
                                })];
                        });
                    }); };
                    clearRemovedNotes = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    NoteObjRepository_1.default.clearRemovedData(inputData, function (err, response) {
                                        if (err) {
                                            return resolve(false);
                                        }
                                        resolve(response);
                                    });
                                })];
                        });
                    }); };
                    clearRemovedFolders = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) {
                                    FolderObjRepository_1.default.clearRemovedData(inputData, function (err, response) {
                                        if (err) {
                                            return resolve(false);
                                        }
                                        resolve(response);
                                    });
                                })];
                        });
                    }); };
                    syncHandler_1.default.log("sync => clearRemovedData => clearRemovedAttaches (ws: " + workspaceId + ")");
                    return [4, clearRemovedAttaches(inputData)];
                case 1:
                    _a.sent();
                    syncHandler_1.default.log("sync => clearRemovedData => clearRemovedTodos (ws: " + workspaceId + ")");
                    return [4, clearRemovedTodos(inputData)];
                case 2:
                    _a.sent();
                    syncHandler_1.default.log("sync => clearRemovedData => clearRemovedTags (ws: " + workspaceId + ")");
                    return [4, clearRemovedTags(inputData)];
                case 3:
                    _a.sent();
                    syncHandler_1.default.log("sync => clearRemovedData => clearRemovedNotes (ws: " + workspaceId + ")");
                    return [4, clearRemovedNotes(inputData)];
                case 4:
                    _a.sent();
                    syncHandler_1.default.log("sync => clearRemovedData => clearRemovedFolders (ws: " + workspaceId + ")");
                    return [4, clearRemovedFolders(inputData)];
                case 5:
                    _a.sent();
                    callback(null, true);
                    return [2];
            }
        });
    });
}
exports.default = clearRemovedData;
