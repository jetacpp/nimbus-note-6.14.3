"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../../events/SyncStatusChangedEvent"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var user_1 = __importDefault(require("../../../../db/models/user"));
var auth_1 = __importDefault(require("../../../../auth/auth"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
function userVariables(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, saveUserVariables, skipSyncHandlers, _a;
        var _this = this;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    workspaceId = inputData.workspaceId, saveUserVariables = inputData.saveUserVariables, skipSyncHandlers = inputData.skipSyncHandlers;
                    syncHandler_1.default.log("=> userVariables => start for workspaceId: " + workspaceId);
                    _a = !skipSyncHandlers;
                    if (!_a) return [3, 2];
                    return [4, SyncStatusChangedEvent_1.default.needPreventSync(inputData)];
                case 1:
                    _a = (_b.sent());
                    _b.label = 2;
                case 2:
                    if (_a)
                        return [2, callback(null, false)];
                    syncHandler_1.default.log("=> userVariables => userVariables");
                    NimbusSDK_1.default.getApi().userVariables(inputData, function (err, userVariables) { return __awaiter(_this, void 0, void 0, function () {
                        var curDate, authInfo, variables, saveUserData, _i, userVariables_1, userVariable, queryData;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    syncHandler_1.default.log("=> userVariables => userVariables => err: " + err);
                                    if (!(err || !userVariables)) return [3, 3];
                                    if (!!skipSyncHandlers) return [3, 2];
                                    return [4, SyncStatusChangedEvent_1.default.setErrorStatus({ workspaceId: workspaceId, err: err })];
                                case 1:
                                    _a.sent();
                                    _a.label = 2;
                                case 2:
                                    errorHandler_1.default.log({
                                        err: err, response: userVariables,
                                        description: "Sync problem => userVariables",
                                        data: inputData
                                    });
                                    return [2, callback(err, false)];
                                case 3:
                                    syncHandler_1.default.log("=> userVariables => userVariables => userVariables");
                                    if (!saveUserVariables) {
                                        callback(err, null);
                                    }
                                    curDate = dateHandler_1.default.now();
                                    syncHandler_1.default.log("=> userVariables => userVariables => fetchActualUserAsync");
                                    return [4, auth_1.default.fetchActualUserAsync()];
                                case 4:
                                    authInfo = _a.sent();
                                    syncHandler_1.default.log("=> userVariables => userVariables => variables");
                                    variables = authInfo && authInfo.variables ? authInfo.variables : user_1.default.getDefaultVariables();
                                    saveUserData = { variables: __assign({}, variables) };
                                    for (_i = 0, userVariables_1 = userVariables; _i < userVariables_1.length; _i++) {
                                        userVariable = userVariables_1[_i];
                                        if (!userVariable.key || typeof (userVariable.value) === 'undefined') {
                                            continue;
                                        }
                                        saveUserData['variables'][userVariable.key] = userVariable.value;
                                    }
                                    queryData = { email: authInfo.email };
                                    syncHandler_1.default.log("=> userVariables => userVariables => user.update => saveUserData");
                                    user_1.default.update(queryData, syncPropsHandler_1.setUpdateProps(saveUserData, curDate), {}, function (err, count) {
                                        if (!err && count) {
                                        }
                                    });
                                    syncHandler_1.default.log("=> userVariables => end with variables");
                                    return [2, callback(err, saveUserData['variables'])];
                            }
                        });
                    }); });
                    return [2];
            }
        });
    });
}
exports.default = userVariables;
