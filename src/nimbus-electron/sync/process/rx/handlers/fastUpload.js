"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var uploadErasedFromTrashItems_1 = __importDefault(require("./uploadErasedFromTrashItems"));
var uploadUpdatedFoldersAndTags_1 = __importDefault(require("./uploadUpdatedFoldersAndTags"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var syncHandler_1 = __importDefault(require("../../../../utilities/syncHandler"));
function fastUpload(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    var workspaceId = inputData.workspaceId;
    syncHandler_1.default.log("sync => fastUserUpload => start (ws: " + workspaceId + ")");
    syncHandler_1.default.log("sync => fastUserUpload => uploadErasedFromTrashItems (ws: " + workspaceId + ")");
    uploadErasedFromTrashItems_1.default(inputData, function (err) {
        if (err) {
            errorHandler_1.default.log({
                err: err, response: null,
                description: "Sync problem => fastUpload => uploadErasedFromTrashItems",
                data: inputData
            });
            return callback(err, null);
        }
        syncHandler_1.default.log("sync => fastUserUpload => uploadUpdatedFoldersAndTags (ws: " + workspaceId + ")");
        uploadUpdatedFoldersAndTags_1.default(inputData, function (err) {
            if (err) {
                errorHandler_1.default.log({
                    err: err, response: null,
                    description: "Sync problem => fastUpload => uploadUpdatedFoldersAndTags",
                    data: inputData
                });
                return callback(err, null);
            }
            callback(err, true);
        });
    });
}
exports.default = fastUpload;
