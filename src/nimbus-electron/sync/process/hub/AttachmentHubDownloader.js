"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs-extra");
var request = require("request");
var NimbusSDK_1 = __importDefault(require("../../nimbussdk/net/NimbusSDK"));
var ConnectionCollector_1 = __importDefault(require("../connection/ConnectionCollector"));
var config_runtime_1 = __importDefault(require("../../../../config.runtime"));
var urlParser_1 = __importDefault(require("../../../utilities/urlParser"));
var errorHandler_1 = __importDefault(require("../../../utilities/errorHandler"));
var AttachmentObjRepository_1 = __importDefault(require("../repositories/AttachmentObjRepository"));
var pdb_1 = __importDefault(require("../../../../pdb"));
var ApiConst_1 = __importDefault(require("../../nimbussdk/net/ApiConst"));
var path = __importStar(require("path"));
var hubDownloader;
var AttachmentHubDownloader = (function () {
    function AttachmentHubDownloader() {
    }
    AttachmentHubDownloader.getInstance = function () {
        if (!hubDownloader) {
            hubDownloader = new AttachmentHubDownloader();
        }
        return hubDownloader;
    };
    AttachmentHubDownloader.getUserAttachmentEntity = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        AttachmentObjRepository_1.default.get(inputData, function (err, attachmentObj) {
                            if (err) {
                                return resolve(null);
                            }
                            if (!attachmentObj) {
                                return resolve(null);
                            }
                            resolve(AttachmentObjRepository_1.default.convertToSyncAttachmentEntityForManualDownload(attachmentObj));
                        });
                    })];
            });
        });
    };
    AttachmentHubDownloader.getUserAttachment = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            AttachmentObjRepository_1.default.get(inputData, function (err, attachmentObj) {
                                if (err) {
                                    return resolve(null);
                                }
                                resolve(attachmentObj);
                            });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    AttachmentHubDownloader.saveFile = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, entity, saveRemoteFile, createFile, globalId, attachment, remotePath, targetPath, baseDirectoryExists, baseDirectory, _a;
                        var _this = this;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, entity = inputData.entity;
                                    saveRemoteFile = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var getRemoteFile, _a;
                                                    var _this = this;
                                                    return __generator(this, function (_b) {
                                                        switch (_b.label) {
                                                            case 0:
                                                                getRemoteFile = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                    var _this = this;
                                                                    return __generator(this, function (_a) {
                                                                        return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                                                var workspaceId, globalId, remotePath, targetPath, attachment, fileExist, errMsg;
                                                                                var _this = this;
                                                                                return __generator(this, function (_a) {
                                                                                    switch (_a.label) {
                                                                                        case 0:
                                                                                            workspaceId = inputData.workspaceId, globalId = inputData.globalId, remotePath = inputData.remotePath, targetPath = inputData.targetPath;
                                                                                            if (targetPath) {
                                                                                                targetPath = path.resolve(targetPath);
                                                                                            }
                                                                                            return [4, AttachmentHubDownloader.getUserAttachment({ workspaceId: workspaceId, globalId: globalId })];
                                                                                        case 1:
                                                                                            attachment = _a.sent();
                                                                                            return [4, fs.exists(targetPath)];
                                                                                        case 2:
                                                                                            fileExist = _a.sent();
                                                                                            if (fileExist) {
                                                                                                return [2, resolve({
                                                                                                        attachment: attachment,
                                                                                                        downloaded: true,
                                                                                                        wasDownloadedBefore: true
                                                                                                    })];
                                                                                            }
                                                                                            else {
                                                                                                try {
                                                                                                    NimbusSDK_1.default.getAccountManager().getAccountSession(function (err, session) {
                                                                                                        if (err || !session) {
                                                                                                            return resolve({
                                                                                                                attachment: attachment
                                                                                                            });
                                                                                                        }
                                                                                                        if (!session.getSessionId()) {
                                                                                                            return resolve({
                                                                                                                attachment: attachment
                                                                                                            });
                                                                                                        }
                                                                                                        var headers = {
                                                                                                            "EverHelper-Session-ID": session.getSessionId(),
                                                                                                            'x-client-software': ApiConst_1.default._CLIENT_SOFTWARE,
                                                                                                            'x-client-version': config_runtime_1.default.APP_VERSION
                                                                                                        };
                                                                                                        var options = {
                                                                                                            uri: remotePath,
                                                                                                            method: "GET",
                                                                                                            followRedirect: true,
                                                                                                            maxRedirects: 10,
                                                                                                            forever: false,
                                                                                                            headers: headers
                                                                                                        };
                                                                                                        var requestUniqueId = new Date().getTime() + ":attach:" + globalId;
                                                                                                        var writeStream = fs.createWriteStream(targetPath);
                                                                                                        writeStream.uniqueId = requestUniqueId;
                                                                                                        writeStream.on('error', function (e) { return __awaiter(_this, void 0, void 0, function () {
                                                                                                            var e_1;
                                                                                                            return __generator(this, function (_a) {
                                                                                                                switch (_a.label) {
                                                                                                                    case 0:
                                                                                                                        writeStream.close();
                                                                                                                        ConnectionCollector_1.default.clearById({
                                                                                                                            workspaceId: workspaceId,
                                                                                                                            id: req.uniqueId
                                                                                                                        });
                                                                                                                        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                                                                                                            console.log("Problem: save to file: ", targetPath, " with error: ", e.message);
                                                                                                                        }
                                                                                                                        _a.label = 1;
                                                                                                                    case 1:
                                                                                                                        _a.trys.push([1, 3, , 4]);
                                                                                                                        return [4, fs.exists(targetPath)];
                                                                                                                    case 2:
                                                                                                                        if (_a.sent()) {
                                                                                                                            try {
                                                                                                                                fs.unlink(targetPath, function () {
                                                                                                                                });
                                                                                                                            }
                                                                                                                            catch (e) {
                                                                                                                                if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                                                                                                                    console.log("Error => AttachmentHubDownloader => getRemoteFile => remove file: ", targetPath, e);
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        return [3, 4];
                                                                                                                    case 3:
                                                                                                                        e_1 = _a.sent();
                                                                                                                        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                                                                                                            console.log("Problem: remove file info that got error during the load: ", targetPath);
                                                                                                                        }
                                                                                                                        return [3, 4];
                                                                                                                    case 4: return [2, resolve({
                                                                                                                            attachment: attachment
                                                                                                                        })];
                                                                                                                }
                                                                                                            });
                                                                                                        }); });
                                                                                                        writeStream.on('finish', function () { return __awaiter(_this, void 0, void 0, function () {
                                                                                                            return __generator(this, function (_a) {
                                                                                                                ConnectionCollector_1.default.clearById({
                                                                                                                    workspaceId: workspaceId,
                                                                                                                    id: req.uniqueId
                                                                                                                });
                                                                                                                return [2, resolve({
                                                                                                                        attachment: attachment,
                                                                                                                        downloaded: true
                                                                                                                    })];
                                                                                                            });
                                                                                                        }); });
                                                                                                        var req = request
                                                                                                            .get(options)
                                                                                                            .on('error', function (err) {
                                                                                                            errorHandler_1.default.log("Can not download attach: " + options.uri);
                                                                                                            var errMessage = "Bad response from server";
                                                                                                            if (err.code === 'ESOCKETTIMEDOUT' || err.code === 'ETIMEDOUT') {
                                                                                                                errMessage = "Timeout request from server";
                                                                                                            }
                                                                                                            writeStream.emit('error', new Error(errMessage));
                                                                                                            if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                                                                                                console.log('options: ', options.uri);
                                                                                                                console.log('err: ', err.code);
                                                                                                            }
                                                                                                            return resolve({
                                                                                                                attachment: attachment
                                                                                                            });
                                                                                                        })
                                                                                                            .on('abort', function () {
                                                                                                            errorHandler_1.default.log("Abort download attach: " + options.uri);
                                                                                                            writeStream.emit('error', new Error("Abort download attach from server"));
                                                                                                            return resolve({
                                                                                                                attachment: attachment
                                                                                                            });
                                                                                                        })
                                                                                                            .on('response', function (response) {
                                                                                                            var statusCode = response.statusCode;
                                                                                                            if (statusCode != 200) {
                                                                                                                errorHandler_1.default.log("Can not download attach: " + options.uri);
                                                                                                                writeStream.emit('error', new Error("No response from server"));
                                                                                                                return resolve({
                                                                                                                    attachment: attachment
                                                                                                                });
                                                                                                            }
                                                                                                        });
                                                                                                        req.uniqueId = requestUniqueId;
                                                                                                        ConnectionCollector_1.default.add({
                                                                                                            workspaceId: workspaceId,
                                                                                                            uniqueId: requestUniqueId,
                                                                                                            xhrConnection: req
                                                                                                        });
                                                                                                        req.pipe(writeStream);
                                                                                                    });
                                                                                                }
                                                                                                catch (err) {
                                                                                                    errMsg = "Problem to download and save file with id: ";
                                                                                                    errMsg += globalId + " and path: " + targetPath;
                                                                                                    errMsg += " details: " + err.toString();
                                                                                                    if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                                                                                        console.log(errMsg);
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            return [2];
                                                                                    }
                                                                                });
                                                                            }); })];
                                                                    });
                                                                }); };
                                                                _a = resolve;
                                                                return [4, getRemoteFile(inputData)];
                                                            case 1:
                                                                _a.apply(void 0, [_b.sent()]);
                                                                return [2];
                                                        }
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    createFile = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var clientAttachmentBasePath = inputData.clientAttachmentBasePath;
                                                    fs.mkdir(clientAttachmentBasePath, function () { return __awaiter(_this, void 0, void 0, function () {
                                                        var _a;
                                                        return __generator(this, function (_b) {
                                                            switch (_b.label) {
                                                                case 0:
                                                                    _a = resolve;
                                                                    return [4, saveRemoteFile(inputData)];
                                                                case 1:
                                                                    _a.apply(void 0, [_b.sent()]);
                                                                    return [2];
                                                            }
                                                        });
                                                    }); });
                                                })];
                                        });
                                    }); };
                                    globalId = entity.global_id;
                                    return [4, AttachmentHubDownloader.getUserAttachment({ workspaceId: workspaceId, globalId: globalId })];
                                case 1:
                                    attachment = _b.sent();
                                    if (!pdb_1.default.getClientAttachmentPath()) {
                                        return [2, resolve({
                                                attachment: attachment
                                            })];
                                    }
                                    remotePath = entity.location ? urlParser_1.default.getAttachmentRemoteUri(entity.location) : "";
                                    targetPath = pdb_1.default.getClientAttachmentPath() + "/" + entity.file_uuid;
                                    if (!remotePath || !targetPath) {
                                        return [2, resolve({
                                                attachment: attachment
                                            })];
                                    }
                                    return [4, fs.exists(pdb_1.default.getClientAttachmentPath())];
                                case 2:
                                    baseDirectoryExists = _b.sent();
                                    if (!!baseDirectoryExists) return [3, 4];
                                    return [4, createFile({
                                            workspaceId: workspaceId,
                                            globalId: globalId,
                                            remotePath: remotePath,
                                            targetPath: targetPath,
                                            clientAttachmentBasePath: pdb_1.default.getClientAttachmentPath()
                                        })];
                                case 3:
                                    baseDirectory = _b.sent();
                                    if (!baseDirectory) {
                                        return [2, resolve({
                                                attachment: attachment
                                            })];
                                    }
                                    _b.label = 4;
                                case 4:
                                    _a = resolve;
                                    return [4, saveRemoteFile({
                                            workspaceId: workspaceId,
                                            globalId: globalId,
                                            remotePath: remotePath,
                                            targetPath: targetPath,
                                            clientAttachmentBasePath: pdb_1.default.getClientAttachmentPath()
                                        })];
                                case 5: return [2, _a.apply(void 0, [_b.sent()])];
                            }
                        });
                    }); })];
            });
        });
    };
    AttachmentHubDownloader.prototype.download = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var attachment;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, AttachmentHubDownloader.saveFile(inputData)];
                                case 1:
                                    attachment = _a.sent();
                                    if (!attachment) {
                                        return [2, resolve(null)];
                                    }
                                    resolve(attachment);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    AttachmentHubDownloader.prototype.checkNeedDownload = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var entity, targetPath, fileAlreadyDownloaded;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!pdb_1.default.getClientAttachmentPath()) {
                                        return [2, resolve(false)];
                                    }
                                    return [4, AttachmentHubDownloader.getUserAttachmentEntity(inputData)];
                                case 1:
                                    entity = _a.sent();
                                    if (!entity) {
                                        return [2, resolve(false)];
                                    }
                                    if (!entity.file_uuid) {
                                        return [2, resolve(false)];
                                    }
                                    targetPath = pdb_1.default.getClientAttachmentPath() + "/" + entity.file_uuid;
                                    if (!targetPath) {
                                        return [2, resolve(false)];
                                    }
                                    return [4, fs.exists(targetPath)];
                                case 2:
                                    fileAlreadyDownloaded = _a.sent();
                                    if (fileAlreadyDownloaded) {
                                        return [2, resolve(false)];
                                    }
                                    resolve(entity);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    return AttachmentHubDownloader;
}());
exports.default = AttachmentHubDownloader;
