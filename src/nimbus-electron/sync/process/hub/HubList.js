"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HubList = (function () {
    function HubList() {
        this.list = [];
    }
    HubList.prototype.add = function (item) {
        this.list.push(item);
    };
    HubList.prototype.get = function (pos) {
        return typeof (this.list[pos]) === "undefined" ? this.list[pos] : null;
    };
    HubList.prototype.remove = function (item) {
        for (var i = 0; i < this.list.length; i++) {
            if (item === this.list[i]) {
                this.list.splice(i, 1);
            }
        }
    };
    HubList.prototype.size = function () {
        return this.list.length;
    };
    return HubList;
}());
exports.default = HubList;
