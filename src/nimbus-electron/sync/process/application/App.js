"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../nimbussdk/net/NimbusSDK"));
var SyncType_1 = __importDefault(require("../SyncType"));
var userSettings_1 = __importDefault(require("../../../db/models/userSettings"));
var workspace_1 = __importDefault(require("../../../db/models/workspace"));
var auth_1 = __importDefault(require("../../../auth/auth"));
var App = (function () {
    function App() {
    }
    App.initSyncState = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        auth_1.default.getUser(function (err, authInfo) {
                            if (authInfo && Object.keys(authInfo).length) {
                                workspace_1.default.findAll({}, {}, function (err, workspaceInstances) { return __awaiter(_this, void 0, void 0, function () {
                                    var updateUserSettings, workspacesIdList, _i, workspaceInstances_1, workspaceInstance, workspaceId, _a, workspacesIdList_1, workspaceId;
                                    var _this = this;
                                    return __generator(this, function (_b) {
                                        switch (_b.label) {
                                            case 0:
                                                if (err) {
                                                    return [2, resolve(true)];
                                                }
                                                updateUserSettings = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                    return __generator(this, function (_a) {
                                                        return [2, new Promise(function (resolve) {
                                                                var workspaceId = inputData.workspaceId;
                                                                userSettings_1.default.set(App.CURRENT_RUNNING_SYNC_SETTING + ":" + workspaceId, App.SYNC_TYPES.NONE, function () {
                                                                    return resolve(true);
                                                                });
                                                            })];
                                                    });
                                                }); };
                                                workspacesIdList = ['', null, workspace_1.default.DEFAULT_NAME];
                                                for (_i = 0, workspaceInstances_1 = workspaceInstances; _i < workspaceInstances_1.length; _i++) {
                                                    workspaceInstance = workspaceInstances_1[_i];
                                                    workspaceId = workspaceInstance.globalId;
                                                    workspacesIdList.push(workspaceId);
                                                }
                                                _a = 0, workspacesIdList_1 = workspacesIdList;
                                                _b.label = 1;
                                            case 1:
                                                if (!(_a < workspacesIdList_1.length)) return [3, 4];
                                                workspaceId = workspacesIdList_1[_a];
                                                return [4, updateUserSettings({ workspaceId: workspaceId })];
                                            case 2:
                                                _b.sent();
                                                _b.label = 3;
                                            case 3:
                                                _a++;
                                                return [3, 1];
                                            case 4:
                                                NimbusSDK_1.default.setup();
                                                resolve(true);
                                                return [2];
                                        }
                                    });
                                }); });
                            }
                            else {
                                resolve(false);
                            }
                        });
                    })];
            });
        });
    };
    App.getRunningSyncType = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId;
                        userSettings_1.default.get(App.CURRENT_RUNNING_SYNC_SETTING + ":" + workspaceId, function (err, data) {
                            if (err || !data) {
                                return resolve(App.SYNC_TYPES.NONE);
                            }
                            if (data && Object.keys(data).length === 0) {
                                return resolve(App.SYNC_TYPES.NONE);
                            }
                            resolve(data);
                        });
                    })];
            });
        });
    };
    App.setRunningSyncType = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, type = inputData.type;
                        userSettings_1.default.set(App.CURRENT_RUNNING_SYNC_SETTING + ":" + workspaceId, type, function (err, list) {
                            if (!list) {
                                return resolve(App.SYNC_TYPES.NONE);
                            }
                            resolve(type);
                        });
                    })];
            });
        });
    };
    App.canStartNewSync = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var currentRunningSync;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, App.getRunningSyncType(inputData)];
                                case 1:
                                    currentRunningSync = _a.sent();
                                    resolve(currentRunningSync === App.SYNC_TYPES.NONE);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    App.SYNC_TYPES = SyncType_1.default;
    App.CURRENT_RUNNING_SYNC_SETTING = 'sync-type-running';
    return App;
}());
exports.default = App;
