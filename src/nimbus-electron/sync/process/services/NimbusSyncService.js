"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../config"));
var App_1 = __importDefault(require("../application/App"));
var NimbusSDK_1 = __importDefault(require("../../nimbussdk/net/NimbusSDK"));
var SyncStatusChangedEvent_1 = __importDefault(require("../events/SyncStatusChangedEvent"));
var SyncStatusDisplayEvent_1 = __importDefault(require("../events/SyncStatusDisplayEvent"));
var socketFunctions_1 = __importDefault(require("../../socket/socketFunctions"));
var sessionLastUpdateTime_1 = __importDefault(require("../rx/handlers/sessionLastUpdateTime"));
var ConnectionCollector_1 = __importDefault(require("../connection/ConnectionCollector"));
var headerSync_1 = __importDefault(require("../rx/handlers/headerSync"));
var saveSyncUpdateTime_1 = __importDefault(require("../rx/handlers/saveSyncUpdateTime"));
var fullSync_1 = __importDefault(require("../rx/handlers/fullSync"));
var updateUserUsage_1 = __importDefault(require("../rx/handlers/updateUserUsage"));
var Exception_1 = __importDefault(require("../../nimbussdk/net/exception/Exception"));
var AutoSyncManager_1 = __importDefault(require("../../nimbussdk/manager/AutoSyncManager"));
var errorHandler_1 = __importDefault(require("../../../utilities/errorHandler"));
var dateHandler_1 = __importDefault(require("../../../utilities/dateHandler"));
var syncHandler_1 = __importDefault(require("../../../utilities/syncHandler"));
var NimbusSyncService = (function () {
    function NimbusSyncService() {
    }
    NimbusSyncService.prototype.onStartCommand = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, manualSync, syncStartDate, self;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, manualSync = inputData.manualSync;
                        syncStartDate = dateHandler_1.default.now();
                        self = this;
                        sessionLastUpdateTime_1.default.setSyncSessionUpdateTime({ workspaceId: workspaceId, lastUpdateTime: 0 });
                        return [4, App_1.default.canStartNewSync({ workspaceId: workspaceId })];
                    case 1:
                        if (_a.sent()) {
                            SyncStatusChangedEvent_1.default.setCleanStatus({ workspaceId: workspaceId });
                            ConnectionCollector_1.default.clear({ workspaceId: workspaceId });
                            socketFunctions_1.default.flushStore({ workspaceId: workspaceId });
                            syncHandler_1.default.init();
                            syncHandler_1.default.log("sync => start " + new Date().toISOString() + " (ws: " + workspaceId + ")");
                            NimbusSDK_1.default.getAccountManager().getSyncTypeIsHeader(function (err, isHeaderSync) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (err) {
                                                errorHandler_1.default.log({
                                                    err: err, response: isHeaderSync,
                                                    description: "Sync problem => NimbusSyncService => getSyncTypeIsHeader",
                                                    data: inputData
                                                });
                                            }
                                            isHeaderSync = false;
                                            if (config_1.default.SHOW_WEB_CONSOLE) {
                                                syncHandler_1.default.log("sync => start => isHeader: " + isHeaderSync + " (ws: " + workspaceId + ")");
                                            }
                                            return [4, App_1.default.setRunningSyncType({
                                                    workspaceId: workspaceId,
                                                    type: isHeaderSync ? App_1.default.SYNC_TYPES.HEADER : App_1.default.SYNC_TYPES.FULL
                                                })];
                                        case 1:
                                            _a.sent();
                                            if (!isHeaderSync) return [3, 3];
                                            return [4, headerSync_1.default({
                                                    workspaceId: workspaceId,
                                                    service: self,
                                                    manualSync: manualSync,
                                                    syncStartDate: syncStartDate
                                                }, function (err, syncComplete) { return __awaiter(_this, void 0, void 0, function () {
                                                    var errorData;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                if (err) {
                                                                    errorData = {
                                                                        err: err, response: syncComplete,
                                                                        description: "Sync problem => NimbusSyncService => headerSync",
                                                                        data: inputData
                                                                    };
                                                                    errorHandler_1.default.log(errorData);
                                                                }
                                                                syncHandler_1.default.log("sync => complete header sync: " + syncComplete + " (ws: " + workspaceId + ")");
                                                                return [4, saveSyncUpdateTime_1.default(inputData)];
                                                            case 1:
                                                                _a.sent();
                                                                return [4, self.onSyncComplete({ workspaceId: workspaceId, syncComplete: syncComplete, syncType: App_1.default.SYNC_TYPES.HEADER })];
                                                            case 2:
                                                                _a.sent();
                                                                callback(err, true);
                                                                return [2];
                                                        }
                                                    });
                                                }); })];
                                        case 2:
                                            _a.sent();
                                            return [3, 4];
                                        case 3:
                                            fullSync_1.default({
                                                workspaceId: workspaceId,
                                                service: self,
                                                manualSync: manualSync,
                                                syncStartDate: syncStartDate
                                            }, function (err, syncComplete) { return __awaiter(_this, void 0, void 0, function () {
                                                var errorData;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            if (err) {
                                                                errorData = {
                                                                    err: err, response: syncComplete,
                                                                    description: "Sync problem => NimbusSyncService => fullSync",
                                                                    data: inputData
                                                                };
                                                                errorHandler_1.default.log(errorData);
                                                            }
                                                            syncHandler_1.default.log("sync => complete full sync: " + syncComplete + " (ws: " + workspaceId + ")");
                                                            return [4, saveSyncUpdateTime_1.default(inputData)];
                                                        case 1:
                                                            _a.sent();
                                                            return [4, self.onSyncComplete({ workspaceId: workspaceId, syncComplete: syncComplete, syncType: App_1.default.SYNC_TYPES.FULL })];
                                                        case 2:
                                                            _a.sent();
                                                            callback(err, true);
                                                            return [2];
                                                    }
                                                });
                                            }); });
                                            _a.label = 4;
                                        case 4: return [2];
                                    }
                                });
                            }); });
                        }
                        else {
                            errorHandler_1.default.log({
                                err: null, response: null,
                                description: "Sync problem => NimbusSyncService => canStartNewSync",
                                data: {
                                    workspaceId: workspaceId
                                }
                            });
                            return [2, callback(new Exception_1.default("NimbusSyncService.onStartCommand already run sync"), null)];
                        }
                        return [2];
                }
            });
        });
    };
    NimbusSyncService.prototype.onSyncComplete = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, syncComplete, syncType, syncStatus, status_1, status_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, syncComplete = inputData.syncComplete, syncType = inputData.syncType;
                        return [4, SyncStatusChangedEvent_1.default.getStatus({ workspaceId: workspaceId })];
                    case 1:
                        syncStatus = _a.sent();
                        return [4, App_1.default.setRunningSyncType({
                                workspaceId: workspaceId,
                                type: App_1.default.SYNC_TYPES.NONE
                            })];
                    case 2:
                        _a.sent();
                        if (!syncComplete) return [3, 6];
                        return [4, updateUserUsage_1.default({
                                workspaceId: workspaceId,
                                useForSync: true
                            })];
                    case 3:
                        _a.sent();
                        if (!(syncStatus !== SyncStatusChangedEvent_1.default.STATUS.FAILED)) return [3, 5];
                        status_1 = SyncStatusDisplayEvent_1.default.get({ workspaceId: workspaceId });
                        if (!(status_1 !== SyncStatusDisplayEvent_1.default.STATUS.TRAFFIC_LIMIT)) return [3, 5];
                        return [4, SyncStatusDisplayEvent_1.default.set({
                                workspaceId: workspaceId,
                                newStatus: SyncStatusDisplayEvent_1.default.STATUS.DONE
                            })];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        ConnectionCollector_1.default.clear({ workspaceId: workspaceId });
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            syncHandler_1.default.log("sync => complete for syncType: " + syncType + " (ws: " + workspaceId + ")");
                        }
                        return [3, 11];
                    case 6:
                        errorHandler_1.default.log({
                            err: null, response: null,
                            description: "Sync problem => NimbusSyncService => onSyncComplete",
                            data: __assign(__assign({}, inputData), { syncComplete: syncComplete, syncType: syncType })
                        });
                        status_2 = SyncStatusDisplayEvent_1.default.get({ workspaceId: workspaceId });
                        if (!(status_2 !== SyncStatusDisplayEvent_1.default.STATUS.TRAFFIC_LIMIT)) return [3, 8];
                        return [4, SyncStatusDisplayEvent_1.default.set({
                                workspaceId: workspaceId,
                                newStatus: SyncStatusDisplayEvent_1.default.STATUS.CANCELED
                            })];
                    case 7:
                        _a.sent();
                        _a.label = 8;
                    case 8:
                        if (!(SyncStatusChangedEvent_1.default.getStatus({ workspaceId: workspaceId }) === SyncStatusChangedEvent_1.default.STATUS.PAUSED)) return [3, 10];
                        return [4, ConnectionCollector_1.default.abort({ workspaceId: workspaceId })];
                    case 9:
                        _a.sent();
                        _a.label = 10;
                    case 10:
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("Sync failed: " + syncType + " (ws: " + workspaceId + ")");
                        }
                        syncHandler_1.default.showLogs();
                        _a.label = 11;
                    case 11:
                        NimbusSyncService.onSyncEnd({
                            workspaceId: workspaceId,
                            startQueueSync: true
                        });
                        return [2];
                }
            });
        });
    };
    NimbusSyncService.prototype.cancelSync = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NimbusSyncService.cancelSync(inputData, callback);
    };
    NimbusSyncService.cancelSync = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, syncType;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId;
                        errorHandler_1.default.log({
                            err: null, response: null,
                            description: "Sync => NimbusSyncService => cancelSync",
                            data: inputData
                        });
                        return [4, updateUserUsage_1.default({
                                workspaceId: workspaceId,
                                useForSync: true
                            })];
                    case 1:
                        _a.sent();
                        return [4, App_1.default.getRunningSyncType({ workspaceId: workspaceId })];
                    case 2:
                        syncType = _a.sent();
                        return [4, App_1.default.setRunningSyncType({
                                workspaceId: workspaceId,
                                type: App_1.default.SYNC_TYPES.NONE
                            })];
                    case 3:
                        _a.sent();
                        ConnectionCollector_1.default.clear({ workspaceId: workspaceId });
                        return [4, SyncStatusDisplayEvent_1.default.set({
                                workspaceId: workspaceId,
                                newStatus: SyncStatusDisplayEvent_1.default.STATUS.CANCELED
                            })];
                    case 4:
                        _a.sent();
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("Sync failed: " + syncType + " (ws: " + workspaceId + ")");
                        }
                        NimbusSyncService.onSyncEnd({
                            workspaceId: workspaceId,
                            startQueueSync: true
                        });
                        return [2];
                }
            });
        });
    };
    NimbusSyncService.onSyncEnd = function (inputData) {
        var workspaceId = inputData.workspaceId, startQueueSync = inputData.startQueueSync;
        socketFunctions_1.default.flushStore(inputData);
        AutoSyncManager_1.default.updateTimer(inputData);
        if (startQueueSync) {
            AutoSyncManager_1.default.makeQueueSync(inputData);
        }
    };
    ;
    return NimbusSyncService;
}());
exports.default = NimbusSyncService;
