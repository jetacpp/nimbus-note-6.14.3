"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../config"));
var NimbusSDK_1 = __importDefault(require("../../nimbussdk/net/NimbusSDK"));
var NoteObj_1 = __importDefault(require("../db/NoteObj"));
var FolderObj_1 = __importDefault(require("../db/FolderObj"));
var AttachmentObj_1 = __importDefault(require("../db/AttachmentObj"));
var FolderObjRepository_1 = __importDefault(require("./FolderObjRepository"));
var AttachmentObjRepository_1 = __importDefault(require("./AttachmentObjRepository"));
var TodoObjRepository_1 = __importDefault(require("./TodoObjRepository"));
var TagObjRepository_1 = __importDefault(require("./TagObjRepository"));
var SyncFullNoteUpdateEntity_1 = __importDefault(require("../../nimbussdk/net/response/entities/SyncFullNoteUpdateEntity"));
var SyncHeaderNoteUpdateEntity_1 = __importDefault(require("../../nimbussdk/net/response/entities/SyncHeaderNoteUpdateEntity"));
var generatorHandler_1 = __importDefault(require("../../../utilities/generatorHandler"));
var item_1 = __importDefault(require("../../../db/models/item"));
var text_1 = __importDefault(require("../../../db/models/text"));
var tag_1 = __importDefault(require("../../../db/models/tag"));
var todo_1 = __importDefault(require("../../../db/models/todo"));
var noteTags_1 = __importDefault(require("../../../db/models/noteTags"));
var attach_1 = __importDefault(require("../../../db/models/attach"));
var socketFunctions_1 = __importDefault(require("../../socket/socketFunctions"));
var socketStoreType_1 = __importDefault(require("../../socket/socketStoreType"));
var orgs_1 = __importDefault(require("../../../db/models/orgs"));
var ReminderNotice_1 = __importDefault(require("../../../popup/ReminderNotice"));
var workspace_1 = __importDefault(require("../../../db/models/workspace"));
var TextEditor_1 = __importDefault(require("../../../db/TextEditor"));
var NoteObjRepository = (function () {
    function NoteObjRepository() {
    }
    NoteObjRepository.get = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
        if (globalId === null) {
            globalId = "";
        }
        item_1.default.find({ "globalId": globalId, "type": "note" }, { workspaceId: workspaceId }, function (err, noteObj) {
            if (NoteObjRepository.isValid(noteObj)) {
                callback(err, Object.assign({}, noteObj));
            }
            else {
                callback(err, null);
            }
        });
    };
    ;
    NoteObjRepository.getTitle = function (title) {
        return title;
    };
    NoteObjRepository.create = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, parentId = inputData.parentId, role = inputData.role, tag = inputData.tag;
        if (tag) {
            var noteObj = new NoteObj_1.default();
            noteObj.parentId = parentId;
            noteObj.role = role;
            if (tag.trim()) {
                noteObj = NoteObj_1.default.setTags(noteObj, JSON.parse(tag));
            }
            return callback(null, noteObj);
        }
        else {
            if (parentId === null) {
                parentId = FolderObj_1.default.DEFAULT;
            }
            if (role === null) {
                role = NoteObj_1.default.ROLE_NOTE;
            }
            var globalId_1 = NoteObjRepository.generateGlobalId();
            var currentTimeInSeconds_1 = new Date().getTime();
            NimbusSDK_1.default.getAccountManager().getUniqueUserName(function (uniqueUserName) {
                var note = new NoteObj_1.default();
                note.parentId = parentId;
                note.globalId = globalId_1;
                note.index = 0;
                note.dateAdded = currentTimeInSeconds_1;
                note.dateUpdated = currentTimeInSeconds_1;
                note.syncDate = 0;
                note.rootParentId = null;
                note.type = "note";
                note.title = NoteObjRepository.getTitle(role);
                note = NoteObj_1.default.setText(note, "");
                note.shortText = "";
                note = NoteObj_1.default.setLocation(note, 0, 0);
                note = NoteObj_1.default.setTodoCount(note, 0);
                note.url = null;
                note.role = role;
                note.isEncrypted = 0;
                note.uniqueUserName = uniqueUserName;
                note.firstImage = null;
                note.isDownloaded = true;
                note.needSync = true;
                note.isMoreThanLimit = false;
                note.existOnServer = false;
                note.isTemp = false;
                note = NoteObj_1.default.setReminderLabel(note, null);
                note.editnote = 1;
                note.color = null;
                note.textAttachmentGlobalId = null;
                note = NoteObj_1.default.setAttachmentsInListCount(note, 0);
                return callback(null, note);
            });
        }
    };
    NoteObjRepository.generateGlobalId = function () {
        return generatorHandler_1.default.randomString(16);
    };
    NoteObjRepository.update = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, noteObjs, processUserNote, result, _i, noteObjs_1, noteObj;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, noteObjs = inputData.noteObjs;
                                    if (!noteObjs) {
                                        return [2, resolve(false)];
                                    }
                                    processUserNote = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var workspaceId, noteObj, globalId, existItemInstance, saveInstance, reminderRemove, workspaceInstance, _a, reminderData, existReminder, saveReminder, userNoteSaved, isCreate, rootIdChanged, parentIdChanged, userNoteTextSaved;
                                                    return __generator(this, function (_b) {
                                                        switch (_b.label) {
                                                            case 0:
                                                                workspaceId = inputData.workspaceId, noteObj = inputData.noteObj;
                                                                if (noteObj.type !== "note") {
                                                                    return [2, resolve(false)];
                                                                }
                                                                globalId = noteObj.globalId;
                                                                return [4, NoteObjRepository.getUserNote({ workspaceId: workspaceId, globalId: globalId })];
                                                            case 1:
                                                                existItemInstance = _b.sent();
                                                                return [4, NoteObjRepository.convertSyncInstanceToLocal({
                                                                        workspaceId: workspaceId,
                                                                        noteObj: noteObj,
                                                                        existItemInstance: existItemInstance
                                                                    })];
                                                            case 2:
                                                                saveInstance = _b.sent();
                                                                if (!saveInstance) {
                                                                    return [2, resolve(false)];
                                                                }
                                                                reminderRemove = false;
                                                                if (!saveInstance.reminder) return [3, 7];
                                                                if (!workspaceId) return [3, 4];
                                                                return [4, workspace_1.default.getById(workspaceId)];
                                                            case 3:
                                                                _a = _b.sent();
                                                                return [3, 6];
                                                            case 4: return [4, workspace_1.default.getDefault()];
                                                            case 5:
                                                                _a = _b.sent();
                                                                _b.label = 6;
                                                            case 6:
                                                                workspaceInstance = (_a);
                                                                reminderData = {
                                                                    workspaceId: workspaceInstance.globalId,
                                                                    workspaceTitle: workspaceInstance.title,
                                                                    globalId: saveInstance.globalId,
                                                                    parentId: saveInstance.parentId,
                                                                    title: saveInstance.title,
                                                                    reminder: saveInstance.reminder
                                                                };
                                                                if (existItemInstance && existItemInstance.reminder) {
                                                                    existReminder = existItemInstance.reminder;
                                                                    saveReminder = saveInstance.reminder;
                                                                    if ((existReminder.date !== saveReminder.date) ||
                                                                        (existReminder.interval !== saveReminder.interval)) {
                                                                        ReminderNotice_1.default.flushByGlobalId(existItemInstance.globalId);
                                                                        ReminderNotice_1.default.setTimeoutByGlobalId(reminderData);
                                                                    }
                                                                }
                                                                else {
                                                                    ReminderNotice_1.default.flushByGlobalId(saveInstance.globalId);
                                                                    ReminderNotice_1.default.setTimeoutByGlobalId(reminderData);
                                                                }
                                                                return [3, 8];
                                                            case 7:
                                                                if (existItemInstance && existItemInstance.reminder) {
                                                                    ReminderNotice_1.default.flushByGlobalId(existItemInstance.globalId);
                                                                    reminderRemove = true;
                                                                }
                                                                _b.label = 8;
                                                            case 8:
                                                                saveInstance = NoteObjRepository.updateTrashProperties({
                                                                    itemObj: noteObj,
                                                                    saveInstance: saveInstance,
                                                                    existItemInstance: existItemInstance
                                                                });
                                                                userNoteSaved = null;
                                                                isCreate = false;
                                                                if (!existItemInstance) return [3, 10];
                                                                return [4, NoteObjRepository.updateUserNote({ workspaceId: workspaceId, item: saveInstance })];
                                                            case 9:
                                                                userNoteSaved = _b.sent();
                                                                if (userNoteSaved) {
                                                                    rootIdChanged = existItemInstance.rootId != saveInstance.rootId;
                                                                    parentIdChanged = existItemInstance.parentId != saveInstance.parentId;
                                                                    if (rootIdChanged || parentIdChanged) {
                                                                        NoteObjRepository.sendSocketEventOnUpdate({
                                                                            workspaceId: workspaceId,
                                                                            globalId: existItemInstance.globalId,
                                                                            parentId: existItemInstance.parentId,
                                                                            isCreate: isCreate,
                                                                            reminderRemove: reminderRemove,
                                                                            textVersion: existItemInstance.text_version
                                                                        });
                                                                    }
                                                                }
                                                                return [3, 12];
                                                            case 10: return [4, NoteObjRepository.createUserNote({
                                                                    workspaceId: workspaceId,
                                                                    item: saveInstance
                                                                })];
                                                            case 11:
                                                                userNoteSaved = _b.sent();
                                                                isCreate = true;
                                                                _b.label = 12;
                                                            case 12:
                                                                userNoteTextSaved = null;
                                                                if (!userNoteSaved) return [3, 15];
                                                                if (!(typeof (noteObj.text) !== "undefined")) return [3, 14];
                                                                return [4, NoteObjRepository.saveUserNoteText({
                                                                        workspaceId: workspaceId,
                                                                        saveInstance: saveInstance,
                                                                        saveText: noteObj.text
                                                                    })];
                                                            case 13:
                                                                userNoteTextSaved = _b.sent();
                                                                _b.label = 14;
                                                            case 14:
                                                                if (noteObj.text_version >= 2) {
                                                                    userNoteTextSaved = true;
                                                                }
                                                                _b.label = 15;
                                                            case 15:
                                                                if (!(userNoteSaved && typeof (noteObj.todo) !== "undefined")) return [3, 17];
                                                                return [4, NoteObjRepository.saveUserNoteTodoList({
                                                                        workspaceId: workspaceId,
                                                                        saveInstance: saveInstance,
                                                                        saveTodoList: noteObj.todo
                                                                    })];
                                                            case 16:
                                                                _b.sent();
                                                                _b.label = 17;
                                                            case 17:
                                                                if (!(userNoteSaved && typeof (noteObj.attachements) !== "undefined")) return [3, 19];
                                                                return [4, NoteObjRepository.saveUserNoteAttachmentList({
                                                                        workspaceId: workspaceId,
                                                                        saveInstance: saveInstance,
                                                                        saveAttachmentList: noteObj.attachements
                                                                    })];
                                                            case 18:
                                                                _b.sent();
                                                                _b.label = 19;
                                                            case 19:
                                                                if (userNoteTextSaved) {
                                                                    NoteObjRepository.sendSocketEventOnUpdate({
                                                                        workspaceId: workspaceId,
                                                                        globalId: userNoteSaved.globalId,
                                                                        parentId: userNoteSaved.parentId,
                                                                        isCreate: isCreate,
                                                                        reminderRemove: reminderRemove,
                                                                        textVersion: userNoteSaved.text_version
                                                                    });
                                                                }
                                                                resolve(userNoteSaved && userNoteTextSaved);
                                                                return [2];
                                                        }
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    result = null;
                                    if (!(noteObjs instanceof Array)) return [3, 5];
                                    _i = 0, noteObjs_1 = noteObjs;
                                    _a.label = 1;
                                case 1:
                                    if (!(_i < noteObjs_1.length)) return [3, 4];
                                    noteObj = noteObjs_1[_i];
                                    return [4, processUserNote({ workspaceId: workspaceId, noteObj: noteObj })];
                                case 2:
                                    result = _a.sent();
                                    _a.label = 3;
                                case 3:
                                    _i++;
                                    return [3, 1];
                                case 4: return [3, 7];
                                case 5: return [4, processUserNote({ workspaceId: workspaceId, noteObj: noteObjs })];
                                case 6:
                                    result = _a.sent();
                                    _a.label = 7;
                                case 7:
                                    resolve(result);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    NoteObjRepository.getUserNote = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                        item_1.default.find({ "globalId": globalId, "type": "note" }, { workspaceId: workspaceId }, function (err, noteObj) {
                            if (err || !noteObj) {
                                return resolve(null);
                            }
                            resolve(noteObj);
                        });
                    })];
            });
        });
    };
    NoteObjRepository.updateUserNote = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, item = inputData.item;
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                        }
                        item_1.default.update({ "globalId": item.globalId }, item, { workspaceId: workspaceId }, function (err, count) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (err || !count) {
                                            return [2, resolve(null)];
                                        }
                                        return [4, NoteObjRepository.saveUserNoteTags({ workspaceId: workspaceId, item: item })];
                                    case 1:
                                        _a.sent();
                                        resolve(!!count ? item : null);
                                        return [2];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    NoteObjRepository.createUserNote = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, item, getFolderByQuery, queryData, parentFolder;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, item = inputData.item;
                                    if (config_1.default.SHOW_WEB_CONSOLE) {
                                    }
                                    getFolderByQuery = function (queryData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    return __generator(this, function (_a) {
                                                        item_1.default.find(queryData, { workspaceId: workspaceId }, function (err, item) {
                                                            if (err) {
                                                                return resolve(null);
                                                            }
                                                            resolve(item);
                                                        });
                                                        return [2];
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    queryData = {
                                        globalId: item.parentId,
                                        type: 'folder'
                                    };
                                    return [4, getFolderByQuery(queryData)];
                                case 1:
                                    parentFolder = _a.sent();
                                    if (parentFolder) {
                                        item.rootId = parentFolder.rootId;
                                    }
                                    item_1.default.add(item, { workspaceId: workspaceId }, function (err, itemInstance) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    if (err || !itemInstance) {
                                                        return [2, resolve(null)];
                                                    }
                                                    return [4, NoteObjRepository.saveUserNoteTags({ workspaceId: workspaceId, item: item })];
                                                case 1:
                                                    _a.sent();
                                                    resolve(!!itemInstance ? item : null);
                                                    return [2];
                                            }
                                        });
                                    }); });
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    NoteObjRepository.updateTrashProperties = function (inputData) {
        var itemObj = inputData.itemObj, saveInstance = inputData.saveInstance, existItemInstance = inputData.existItemInstance;
        if (itemObj.parentId) {
            saveInstance["parentId"] = itemObj.parentId === 'root' ? 'default' : itemObj.parentId;
            saveInstance["rootId"] = itemObj.parentId === 'trash' ? 'trash' : 'root';
            if (itemObj.parentId !== "trash") {
                saveInstance["rootParentId"] = itemObj.parentId;
            }
        }
        saveInstance.needSync = false;
        if (existItemInstance && existItemInstance.needSync) {
            if (existItemInstance.text_version >= 2) {
                saveInstance.needSync = existItemInstance.needSync;
            }
        }
        return saveInstance;
    };
    NoteObjRepository.saveUserNoteTodoList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, saveInstance, saveTodoList, i, saveTodoGlobalIdList, noteTodoList, removeTodoList, j, k, noteTodoItem, n, saveTodoItem, result, todoCount, todoTotalCount;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, saveInstance = inputData.saveInstance, saveTodoList = inputData.saveTodoList;
                                    for (i in saveTodoList) {
                                        if (saveTodoList.hasOwnProperty(i)) {
                                            saveTodoList[i].noteGlobalId = saveInstance.globalId;
                                            saveTodoList[i].globalId = saveTodoList[i].global_id;
                                        }
                                    }
                                    saveTodoGlobalIdList = saveTodoList.map(function (todoInstance) {
                                        return todoInstance.globalId;
                                    });
                                    return [4, TodoObjRepository_1.default.findNoteTodoList({
                                            workspaceId: workspaceId,
                                            noteGlobalId: saveInstance.globalId
                                        })];
                                case 1:
                                    noteTodoList = _a.sent();
                                    removeTodoList = [];
                                    for (j in noteTodoList) {
                                        if (noteTodoList.hasOwnProperty(j)) {
                                            if (saveTodoGlobalIdList.indexOf(noteTodoList[j].globalId) < 0 && !noteTodoList[j].needSync) {
                                                removeTodoList.push(noteTodoList[j]);
                                            }
                                        }
                                    }
                                    if (!removeTodoList.length) return [3, 3];
                                    return [4, TodoObjRepository_1.default.removeNoteTodoList({ workspaceId: workspaceId, todoList: removeTodoList })];
                                case 2:
                                    _a.sent();
                                    _a.label = 3;
                                case 3:
                                    for (k in noteTodoList) {
                                        if (!noteTodoList.hasOwnProperty(k))
                                            continue;
                                        noteTodoItem = noteTodoList[k];
                                        for (n in saveTodoList) {
                                            if (!saveTodoList.hasOwnProperty(n))
                                                continue;
                                            saveTodoItem = saveTodoList[n];
                                            if (noteTodoItem.globalId !== saveTodoItem.global_id) {
                                                continue;
                                            }
                                            if (noteTodoItem.needSync) {
                                                saveTodoList.splice(n, 1);
                                            }
                                        }
                                    }
                                    return [4, TodoObjRepository_1.default.update({
                                            workspaceId: workspaceId,
                                            todoObjs: saveTodoList
                                        })];
                                case 4:
                                    result = _a.sent();
                                    return [4, TodoObjRepository_1.default.getUncheckedCount({
                                            workspaceId: workspaceId,
                                            noteGlobalId: saveInstance.globalId
                                        })];
                                case 5:
                                    todoCount = _a.sent();
                                    return [4, TodoObjRepository_1.default.getCount({
                                            workspaceId: workspaceId,
                                            noteGlobalId: saveInstance.globalId
                                        })];
                                case 6:
                                    todoTotalCount = _a.sent();
                                    return [4, NoteObjRepository.updateUserNoteProps({
                                            workspaceId: workspaceId,
                                            globalId: saveInstance.globalId,
                                            props: {
                                                todoCount: todoCount,
                                                todoExist: !!todoTotalCount
                                            }
                                        })];
                                case 7:
                                    _a.sent();
                                    resolve(result);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    NoteObjRepository.saveUserNoteAttachmentList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, saveInstance, saveAttachmentList, i, saveAttachmentGlobalIdList, noteAttachmentList, removeAttachmentList, j, k, noteAttachmentItem, n, saveAttachmentItem, result, attachmentsInListCount;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, saveInstance = inputData.saveInstance, saveAttachmentList = inputData.saveAttachmentList;
                                    for (i in saveAttachmentList) {
                                        saveAttachmentList[i].noteGlobalId = saveInstance.globalId;
                                        saveAttachmentList[i].globalId = saveAttachmentList[i].global_id;
                                    }
                                    saveAttachmentGlobalIdList = saveAttachmentList.map(function (attachmentInstance) {
                                        return attachmentInstance.globalId;
                                    });
                                    return [4, AttachmentObjRepository_1.default.findNoteAttachmentList({
                                            workspaceId: workspaceId,
                                            noteGlobalId: saveInstance.globalId
                                        })];
                                case 1:
                                    noteAttachmentList = _a.sent();
                                    removeAttachmentList = [];
                                    for (j in noteAttachmentList) {
                                        if (noteAttachmentList.hasOwnProperty(j)) {
                                            if (saveAttachmentGlobalIdList.indexOf(noteAttachmentList[j].globalId) < 0 && !noteAttachmentList[j].needSync) {
                                                removeAttachmentList.push(noteAttachmentList[j]);
                                            }
                                        }
                                    }
                                    if (!removeAttachmentList.length) return [3, 3];
                                    return [4, AttachmentObjRepository_1.default.removeNoteAttachmentList({
                                            workspaceId: workspaceId,
                                            attachmentList: removeAttachmentList
                                        })];
                                case 2:
                                    _a.sent();
                                    _a.label = 3;
                                case 3:
                                    for (k in noteAttachmentList) {
                                        if (!noteAttachmentList.hasOwnProperty(k))
                                            continue;
                                        noteAttachmentItem = noteAttachmentList[k];
                                        for (n in saveAttachmentList) {
                                            if (!saveAttachmentList.hasOwnProperty(n))
                                                continue;
                                            saveAttachmentItem = saveAttachmentList[n];
                                            if (noteAttachmentItem.globalId !== saveAttachmentItem.global_id) {
                                                continue;
                                            }
                                            if (noteAttachmentItem.needSync) {
                                                saveAttachmentList.splice(n, 1);
                                            }
                                        }
                                    }
                                    return [4, AttachmentObjRepository_1.default.update({
                                            workspaceId: workspaceId,
                                            attachmentObjs: saveAttachmentList,
                                            sendSocketEvents: true
                                        })];
                                case 4:
                                    result = _a.sent();
                                    return [4, AttachmentObjRepository_1.default.getInListCount({
                                            workspaceId: workspaceId,
                                            noteGlobalId: saveInstance.globalId
                                        })];
                                case 5:
                                    attachmentsInListCount = _a.sent();
                                    return [4, NoteObjRepository.updateUserNoteProps({
                                            workspaceId: workspaceId,
                                            globalId: saveInstance.globalId,
                                            props: {
                                                attachmentsInListCount: attachmentsInListCount,
                                                attachmentsInListExist: !!attachmentsInListCount
                                            }
                                        })];
                                case 6:
                                    _a.sent();
                                    resolve(result);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    NoteObjRepository.updateUserNoteProps = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, globalId, props;
                        var _this = this;
                        return __generator(this, function (_a) {
                            workspaceId = inputData.workspaceId, globalId = inputData.globalId, props = inputData.props;
                            if (!globalId || !props) {
                                return [2, resolve(false)];
                            }
                            item_1.default.update({ globalId: globalId }, props, { workspaceId: workspaceId }, function (err, count) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    if (err || !count) {
                                        return [2, resolve(false)];
                                    }
                                    return [2, resolve(true)];
                                });
                            }); });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    NoteObjRepository.saveUserNoteText = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, saveInstance, saveText, getUserNoteText, updateUserNoteText, createUserNoteText, userNoteTextSaved, existUserNoteText;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, saveInstance = inputData.saveInstance, saveText = inputData.saveText;
                                    getUserNoteText = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var workspaceId, noteGlobalId;
                                                    return __generator(this, function (_a) {
                                                        workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
                                                        text_1.default.find({ noteGlobalId: noteGlobalId }, { workspaceId: workspaceId }, function (err, userNoteText) {
                                                            if (err || !userNoteText) {
                                                                return resolve(null);
                                                            }
                                                            resolve(userNoteText);
                                                        });
                                                        return [2];
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    updateUserNoteText = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, item = inputData.item, userNoteText = inputData.userNoteText, saveText = inputData.saveText;
                                                    var userNoteTextData = {
                                                        noteGlobalId: userNoteText.noteGlobalId,
                                                        shortText: item.shortText,
                                                        needSync: false
                                                    };
                                                    var noteTextInstance = text_1.default.prepareSyncOnlyProperties(userNoteText, userNoteTextData);
                                                    noteTextInstance.textShort = item.shortText;
                                                    noteTextInstance.text = saveText ? saveText : item.shortText;
                                                    if (item.text_version) {
                                                        noteTextInstance.text_version = item.text_version;
                                                    }
                                                    text_1.default.update({ noteGlobalId: item.globalId }, noteTextInstance, { workspaceId: workspaceId }, function (err, count) {
                                                        if (err || !count) {
                                                            return resolve(false);
                                                        }
                                                        NoteObjRepository.sendSocketTextEventOnUpdate({
                                                            workspaceId: workspaceId,
                                                            globalId: userNoteText.noteGlobalId,
                                                            textVersion: item.text_version
                                                        });
                                                        resolve(!!count);
                                                    });
                                                })];
                                        });
                                    }); };
                                    createUserNoteText = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, item = inputData.item, saveText = inputData.saveText;
                                                    var userNoteText = text_1.default.getDefaultModel(item.globalId);
                                                    var userNoteTextData = {
                                                        noteGlobalId: item.globalId,
                                                        shortText: item.shortText,
                                                        needSync: false,
                                                    };
                                                    var noteTextInstance = text_1.default.prepareSyncOnlyProperties(userNoteText, userNoteTextData);
                                                    noteTextInstance.textShort = item.shortText;
                                                    noteTextInstance.text = saveText ? saveText : item.shortText;
                                                    if (item.text_version) {
                                                        noteTextInstance.text_version = item.text_version;
                                                    }
                                                    text_1.default.add(noteTextInstance, { workspaceId: workspaceId }, function (err, itemInstance) {
                                                        if (err || !itemInstance) {
                                                            return resolve(false);
                                                        }
                                                        NoteObjRepository.sendSocketTextEventOnUpdate({
                                                            workspaceId: workspaceId,
                                                            globalId: userNoteTextData.noteGlobalId,
                                                            textVersion: item.text_version
                                                        });
                                                        resolve(!!itemInstance);
                                                    });
                                                })];
                                        });
                                    }); };
                                    userNoteTextSaved = null;
                                    return [4, getUserNoteText({ workspaceId: workspaceId, noteGlobalId: saveInstance.globalId })];
                                case 1:
                                    existUserNoteText = _a.sent();
                                    if (!existUserNoteText) return [3, 3];
                                    return [4, updateUserNoteText({
                                            workspaceId: workspaceId,
                                            item: saveInstance,
                                            userNoteText: existUserNoteText,
                                            saveText: saveText
                                        })];
                                case 2:
                                    userNoteTextSaved = _a.sent();
                                    return [3, 5];
                                case 3: return [4, createUserNoteText({
                                        workspaceId: workspaceId,
                                        item: saveInstance,
                                        saveText: saveText
                                    })];
                                case 4:
                                    userNoteTextSaved = _a.sent();
                                    _a.label = 5;
                                case 5:
                                    resolve(userNoteTextSaved);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    NoteObjRepository.saveUserNoteTags = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, item, tagList, refreshUserItemNoteTags, getUserTag, checkUserTagRemoved, addUserTag, updateUserTag, processUserNoteTag, noteTagsList, removeTagsList, j, k, noteTagItem, n, saveTag, _i, tagList_1, tag, userTag, tagItem;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, item = inputData.item;
                        tagList = typeof (item.tags) == "string" && item.tags ? JSON.parse(item.tags) : item.tags;
                        if (!tagList) {
                            tagList = [];
                        }
                        refreshUserItemNoteTags = function (noteGlobalId, tagList) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        noteTags_1.default.findAll({
                                            noteGlobalId: noteGlobalId,
                                        }, { workspaceId: workspaceId }, function (err, userNoteTags) {
                                            if (err || !userNoteTags) {
                                                return resolve([]);
                                            }
                                            for (var _i = 0, userNoteTags_1 = userNoteTags; _i < userNoteTags_1.length; _i++) {
                                                var userNoteTag = userNoteTags_1[_i];
                                                tag_1.default.find({ globalId: userNoteTag.tagGlobalId }, { workspaceId: workspaceId }, function (err, userTag) {
                                                    if (userTag) {
                                                        if (tagList.indexOf(userTag.tag) < 0) {
                                                            noteTags_1.default.remove({
                                                                tagGlobalId: userTag.globalId,
                                                                noteGlobalId: noteGlobalId
                                                            }, { workspaceId: workspaceId }, function (err, count) {
                                                                TagObjRepository_1.default.emitSocketEventForNote({
                                                                    workspaceId: workspaceId,
                                                                    tag: userTag.tag,
                                                                    noteGlobalId: noteGlobalId,
                                                                    action: "remove"
                                                                });
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                            resolve(tagList);
                                        });
                                    })];
                            });
                        }); };
                        return [4, refreshUserItemNoteTags(item.globalId, tagList)];
                    case 1:
                        _a.sent();
                        if (!(tagList && tagList.length)) return [3, 13];
                        getUserTag = function (tag) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        tag_1.default.find({ "tag": tag }, { workspaceId: workspaceId }, function (err, tagItem) {
                                            if (err || !tagItem) {
                                                return resolve(null);
                                            }
                                            resolve(tagItem);
                                        });
                                    })];
                            });
                        }); };
                        checkUserTagRemoved = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        TagObjRepository_1.default.getErasedTagForUploadOnServer(inputData, function (err, tagObj) {
                                            if (err) {
                                                return resolve(false);
                                            }
                                            resolve(!!tagObj);
                                        });
                                    })];
                            });
                        }); };
                        addUserTag = function (tagObj) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        var fillModelProperties = function (item, data) {
                                            return tag_1.default.prepareCommonProperties(tag_1.default.prepareItemDbProperties(item, data), data);
                                        };
                                        var tagInstance = fillModelProperties(tag_1.default.prepareModelData(tagObj), tagObj);
                                        tag_1.default.add(tagInstance, { workspaceId: workspaceId }, function (err, result) {
                                            if (err || !result) {
                                                return resolve(null);
                                            }
                                            resolve(tagInstance);
                                        });
                                    })];
                            });
                        }); };
                        updateUserTag = function (tagObj) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        var fillSyncObjProperties = function (item, data) {
                                            return tag_1.default.prepareSyncOnlyProperties(tag_1.default.prepareItemDbProperties(item, data), data);
                                        };
                                        var tagInstance = fillSyncObjProperties(tag_1.default.prepareModelData(tagObj), tagObj);
                                        tag_1.default.update({ tag: tagInstance.tag }, tagInstance, { workspaceId: workspaceId }, function (err, count) {
                                            if (err || !count) {
                                                return resolve(null);
                                            }
                                            TagObjRepository_1.default.emitSocketEventForNote({
                                                workspaceId: workspaceId,
                                                tag: tagObj.tag,
                                                noteGlobalId: tagObj.noteGlobalId,
                                                action: "update"
                                            });
                                            resolve(tagInstance);
                                        });
                                    })];
                            });
                        }); };
                        processUserNoteTag = function (noteGlobalId, tagGlobalId, tag) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        noteTags_1.default.find({
                                            noteGlobalId: noteGlobalId,
                                            tagGlobalId: tagGlobalId
                                        }, { workspaceId: workspaceId }, function (err, userNoteTag) {
                                            if (err) {
                                                return resolve(false);
                                            }
                                            if (userNoteTag) {
                                                return resolve(true);
                                            }
                                            var userNoteTagData = noteTags_1.default.prepareModelData({
                                                tagGlobalId: tagGlobalId,
                                                noteGlobalId: noteGlobalId,
                                                needSync: false
                                            });
                                            noteTags_1.default.add(userNoteTagData, { workspaceId: workspaceId }, function (err, result) {
                                                if (err || !result) {
                                                    return resolve(false);
                                                }
                                                TagObjRepository_1.default.emitSocketEventForNote({
                                                    workspaceId: workspaceId,
                                                    tag: tag,
                                                    noteGlobalId: noteGlobalId,
                                                    action: "add"
                                                });
                                                resolve(true);
                                            });
                                        });
                                    })];
                            });
                        }); };
                        return [4, TagObjRepository_1.default.findNoteTagsList({
                                workspaceId: workspaceId,
                                noteGlobalId: item.globalId
                            })];
                    case 2:
                        noteTagsList = _a.sent();
                        removeTagsList = [];
                        for (j in noteTagsList) {
                            if (noteTagsList.hasOwnProperty(j)) {
                                if (tagList.indexOf(noteTagsList[j].globalId) < 0 && !noteTagsList[j].needSync) {
                                    removeTagsList.push(noteTagsList[j]);
                                }
                            }
                        }
                        if (!removeTagsList.length) return [3, 4];
                        return [4, TagObjRepository_1.default.removeNoteTagList({
                                workspaceId: workspaceId,
                                tagList: removeTagsList
                            })];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        for (k in noteTagsList) {
                            if (!noteTagsList.hasOwnProperty(k))
                                continue;
                            noteTagItem = noteTagsList[k];
                            for (n in tagList) {
                                if (!tagList.hasOwnProperty(n))
                                    continue;
                                saveTag = tagList[n];
                                if (noteTagItem.tag !== saveTag) {
                                    continue;
                                }
                                if (noteTagItem.needSync) {
                                    tagList.splice(n, 1);
                                }
                            }
                        }
                        _i = 0, tagList_1 = tagList;
                        _a.label = 5;
                    case 5:
                        if (!(_i < tagList_1.length)) return [3, 13];
                        tag = tagList_1[_i];
                        userTag = null;
                        return [4, getUserTag(tag)];
                    case 6:
                        tagItem = _a.sent();
                        if (!tagItem) return [3, 8];
                        return [4, updateUserTag(tagItem)];
                    case 7:
                        userTag = _a.sent();
                        return [3, 10];
                    case 8: return [4, addUserTag(tag_1.default.prepareModelData({ tag: tag }))];
                    case 9:
                        userTag = _a.sent();
                        _a.label = 10;
                    case 10:
                        if (!userTag) return [3, 12];
                        return [4, processUserNoteTag(item.globalId, userTag.globalId, tag)];
                    case 11:
                        _a.sent();
                        _a.label = 12;
                    case 12:
                        _i++;
                        return [3, 5];
                    case 13: return [2];
                }
            });
        });
    };
    NoteObjRepository.sendSocketEventOnUpdate = function (inputData) {
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, parentId = inputData.parentId, isCreate = inputData.isCreate, reminderRemove = inputData.reminderRemove, textVersion = inputData.textVersion;
        socketFunctions_1.default.addStoreData({
            workspaceId: workspaceId,
            storeType: socketStoreType_1.default.NOTES_FOR_UPDATE,
            data: {
                globalId: globalId,
                parentId: parentId,
                isCreate: isCreate,
                type: 'note',
                reminderRemove: reminderRemove
            }
        });
        NoteObjRepository.sendSocketTextEventOnUpdate({
            workspaceId: workspaceId,
            globalId: globalId,
            textVersion: textVersion
        });
        socketFunctions_1.default.addStoreData({
            workspaceId: workspaceId,
            storeType: socketStoreType_1.default.NOTES_FOR_UPDATE_COUNTERS,
            data: {
                globalId: globalId,
                parentId: parentId,
                type: 'note'
            }
        });
    };
    NoteObjRepository.sendSocketEventOnRemove = function (inputData) {
        var workspaceId = inputData.workspaceId, globalIdList = inputData.globalIdList;
        if (globalIdList && globalIdList.length) {
            socketFunctions_1.default.addStoreData({
                workspaceId: workspaceId,
                storeType: socketStoreType_1.default.NOTES_FOR_REMOVE,
                data: {
                    globalId: globalIdList
                }
            });
        }
    };
    NoteObjRepository.sendSocketTextEventOnUpdate = function (inputData) {
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, textVersion = inputData.textVersion;
        socketFunctions_1.default.addStoreData({
            workspaceId: workspaceId,
            storeType: socketStoreType_1.default.NOTES_FOR_UPDATE_TEXT,
            data: {
                globalId: globalId,
                textVersion: textVersion
            }
        });
    };
    NoteObjRepository.sendSocketEventOnCounter = function (inputData) {
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, parentId = inputData.parentId, type = inputData.type;
        socketFunctions_1.default.addStoreData({
            workspaceId: workspaceId,
            storeType: socketStoreType_1.default.NOTES_FOR_UPDATE_COUNTERS,
            data: {
                globalId: globalId,
                parentId: parentId,
                type: type
            }
        });
    };
    NoteObjRepository.convertSyncInstanceToLocal = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, noteObj, existItemInstance, fillSyncObjProperties, fillModelProperties, itemInstance;
                        return __generator(this, function (_a) {
                            workspaceId = inputData.workspaceId, noteObj = inputData.noteObj, existItemInstance = inputData.existItemInstance;
                            if (!noteObj) {
                                return [2, resolve(null)];
                            }
                            fillSyncObjProperties = function (item, data) {
                                return item_1.default.prepareNoteOnlyProperties(item_1.default.prepareItemDbProperties(item, data), data);
                            };
                            fillModelProperties = function (item, data) {
                                item.role = data.role || "note";
                                return item_1.default.prepareCommonProperties(item_1.default.prepareItemDbProperties(item, data), data);
                            };
                            itemInstance = null;
                            if (existItemInstance) {
                                itemInstance = fillSyncObjProperties(item_1.default.prepareModelData(existItemInstance), noteObj);
                                itemInstance = item_1.default.changeDateUpdate(itemInstance, noteObj);
                                if (itemInstance.title !== noteObj.title) {
                                    itemInstance.title = noteObj.title;
                                }
                            }
                            else {
                                itemInstance = fillModelProperties(item_1.default.prepareModelData(noteObj), noteObj);
                            }
                            resolve(itemInstance);
                            return [2];
                        });
                    }); })];
            });
        });
    };
    NoteObjRepository.clearAllMoreThanLimitInNotesI = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({
            "type": "note",
            "isMoreThanLimit": true
        }, { workspaceId: workspaceId }, function (err, noteObjs) { return __awaiter(_this, void 0, void 0, function () {
            var _i, noteObjs_2, noteObj, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!noteObjs.length) {
                            return [2, callback(err, false)];
                        }
                        for (_i = 0, noteObjs_2 = noteObjs; _i < noteObjs_2.length; _i++) {
                            noteObj = noteObjs_2[_i];
                            noteObj.isMoreThanLimit = false;
                        }
                        return [4, NoteObjRepository.update({ workspaceId: workspaceId, noteObjs: noteObjs })];
                    case 1:
                        result = _a.sent();
                        callback(null, result);
                        return [2];
                }
            });
        }); });
    };
    NoteObjRepository.isAvailableNotesForSync = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.count({
            "type": "note",
            "isTemp": false,
            "needSync": true,
            "isMoreThanLimit": false
        }, { workspaceId: workspaceId }, function (err, count) {
            callback(err, count !== 0);
        });
    };
    NoteObjRepository.getErasedFromTrashNotesForUploadOnServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({
            "type": "note",
            "erised": true,
            "offlineOnly": { "$ne": true },
            "needSync": true
        }, { workspaceId: workspaceId }, function (err, noteObjs) {
            var notes = [];
            for (var _i = 0, noteObjs_3 = noteObjs; _i < noteObjs_3.length; _i++) {
                var noteObj = noteObjs_3[_i];
                notes.push(noteObj.globalId);
            }
            callback(err, notes);
        });
    };
    ;
    NoteObjRepository.getUpdatedHeaderNotesForUploadOnServer = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({
            "type": "note",
            "parentId": { "$ne": FolderObj_1.default.ERASED_FROM_TRASH },
            "offlineOnly": { "$ne": true },
            "needSync": true,
        }, {
            workspaceId: workspaceId,
            "order": { "dateUpdated": "desc" }
        }, function (err, noteObjs) { return __awaiter(_this, void 0, void 0, function () {
            var notes, _i, noteObjs_4, noteObj, syncNote, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        notes = [];
                        _i = 0, noteObjs_4 = noteObjs;
                        _b.label = 1;
                    case 1:
                        if (!(_i < noteObjs_4.length)) return [3, 4];
                        noteObj = noteObjs_4[_i];
                        syncNote = new SyncHeaderNoteUpdateEntity_1.default();
                        syncNote.global_id = noteObj.globalId;
                        syncNote.parent_id = noteObj.parentId;
                        syncNote.root_parent_id = noteObj.rootParentId;
                        _a = syncNote;
                        return [4, NoteObjRepository.getNoteTagsList({ workspaceId: workspaceId, globalId: noteObj.globalId })];
                    case 2:
                        _a.tags = (_b.sent());
                        syncNote.shared = parseInt(noteObj.shared);
                        syncNote.date_added_user = noteObj.dateAdded;
                        syncNote.date_updated_user = noteObj.dateUpdated;
                        notes.push(syncNote);
                        _b.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        callback(err, notes);
                        return [2];
                }
            });
        }); });
    };
    NoteObjRepository.getNoteTagsList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                        noteTags_1.default.findAll({ "noteGlobalId": globalId }, { workspaceId: workspaceId }, function (err, noteTagObjs) {
                            if (err) {
                                resolve([]);
                            }
                            var tagGlobalIdList = [];
                            for (var _i = 0, noteTagObjs_1 = noteTagObjs; _i < noteTagObjs_1.length; _i++) {
                                var noteTagObj = noteTagObjs_1[_i];
                                tagGlobalIdList.push(noteTagObj.tagGlobalId);
                            }
                            if (!tagGlobalIdList.length) {
                                return resolve([]);
                            }
                            tag_1.default.findAll({ "globalId": { "$in": tagGlobalIdList } }, { workspaceId: workspaceId }, function (err, tagObjs) {
                                if (err) {
                                    resolve([]);
                                }
                                var tagObjTitleList = [];
                                for (var _i = 0, tagObjs_1 = tagObjs; _i < tagObjs_1.length; _i++) {
                                    var tagObj = tagObjs_1[_i];
                                    tagObjTitleList.push(tagObj.tag);
                                }
                                resolve(tagObjTitleList);
                            });
                        });
                    })];
            });
        });
    };
    NoteObjRepository.getAvailableNoteForDownloadFromServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var globalId = null;
        item_1.default.findAll({
            "type": "note",
            "needSync": false,
            "isTemp": false,
            "isEncrypted": 0,
            "parentId": { "$nin": [FolderObj_1.default.TRASH, FolderObj_1.default.ERASED_FROM_TRASH] },
            "isDownloaded": false
        }, {
            workspaceId: workspaceId,
            "order": { "dateUpdated": "desc" }
        }, function (err, noteObjs) {
            if (noteObjs.length > 0) {
                globalId = noteObjs[0].globalId;
            }
            callback(err, globalId);
        });
    };
    NoteObjRepository.getUpdatedNotesForUploadOnServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({
            "type": "note",
            "offlineOnly": { "$ne": true },
            "needSync": true,
            "globalId": { "$ne": "default" }
        }, {
            workspaceId: workspaceId,
            "order": { "dateUpdated": "desc" }
        }, function (err, noteObjs) {
            if (err) {
                return callback(err, []);
            }
            callback(err, noteObjs);
        });
    };
    NoteObjRepository.getUpdatedFullNotesCountForUploadOnServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({
            "type": "note",
            "offlineOnly": { "$ne": true },
            "needSync": true
        }, {
            workspaceId: workspaceId,
            "order": { "dateUpdated": "desc" }
        }, function (err, noteObjs) {
            callback(err, noteObjs.length);
        });
    };
    NoteObjRepository.getAvailableNotesCountForDownloadFromServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({
            "type": "note"
        }, { workspaceId: workspaceId }, function (err, noteObjs) {
            callback(err, noteObjs.length);
        });
    };
    NoteObjRepository.getSyncNoteEntity = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
        item_1.default.find({ "globalId": noteGlobalId, "type": "note" }, { workspaceId: workspaceId }, function (err, noteObj) { return __awaiter(_this, void 0, void 0, function () {
            var note, textItem, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        note = new SyncFullNoteUpdateEntity_1.default();
                        if (!NoteObjRepository.isValid(noteObj)) return [3, 5];
                        note.global_id = noteObj.globalId;
                        note.parent_id = noteObj.parentId;
                        note.root_parent_id = noteObj.rootParentId;
                        note.index = noteObj.index;
                        note.date_added_user = noteObj.dateAdded;
                        note.date_updated_user = noteObj.dateUpdated;
                        note.type = noteObj.type;
                        note.title = noteObj.title;
                        note.location_lat = NoteObj_1.default.getLocationLat(noteObj);
                        note.location_lng = NoteObj_1.default.getLocationLng(noteObj);
                        note.shared = noteObj.shared ? 1 : 0;
                        note.role = noteObj.role;
                        note.editnote = noteObj.editnote ? 1 : 0;
                        note.color = noteObj.color ? noteObj.color : '';
                        note.url = noteObj.url;
                        note.favorite = noteObj.favorite ? 1 : 0;
                        note.is_encrypted = noteObj.isEncrypted ? 1 : 0;
                        note.is_fullwidth = noteObj.isFullwidth ? 1 : 0;
                        note.text_version = 1;
                        if (noteObj.text_version) {
                            note.text_version = noteObj.text_version;
                        }
                        if (!(noteObj.text_version < 2)) return [3, 2];
                        return [4, NoteObjRepository.getTextItemForNote({ workspaceId: workspaceId, noteGlobalId: noteObj.globalId })];
                    case 1:
                        textItem = _b.sent();
                        if (textItem && textItem['text']) {
                            note.text = textItem['text'];
                            note.text_short = textItem['textShort'];
                        }
                        else {
                            note.text = text_1.default.getDefaultNoteText();
                            note.text_short = "";
                        }
                        return [3, 3];
                    case 2:
                        if (noteObj.text_version >= 2) {
                            delete note.text;
                            delete note.text_short;
                            note.noteObj = noteObj;
                        }
                        _b.label = 3;
                    case 3:
                        _a = note;
                        return [4, NoteObjRepository.getTagsForNote({ workspaceId: workspaceId, noteGlobalId: noteObj.globalId })];
                    case 4:
                        _a.tags = _b.sent();
                        note.preview = noteObj.preview ? noteObj.preview : null;
                        note.reminder = noteObj.reminder ? noteObj.reminder : null;
                        if (note.reminder && !note.reminder.phone) {
                            note.reminder.phone = '';
                        }
                        _b.label = 5;
                    case 5:
                        callback(err, note);
                        return [2];
                }
            });
        }); });
    };
    NoteObjRepository.getTagsForNote = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
                        if (!noteGlobalId) {
                            return resolve([]);
                        }
                        noteTags_1.default.findAll({ noteGlobalId: noteGlobalId }, { workspaceId: workspaceId }, function (err, noteTagItems) {
                            if (err) {
                                return resolve([]);
                            }
                            var noteTagItemsIdList = [];
                            for (var _i = 0, noteTagItems_1 = noteTagItems; _i < noteTagItems_1.length; _i++) {
                                var noteTagItem = noteTagItems_1[_i];
                                noteTagItemsIdList.push(noteTagItem.tagGlobalId);
                            }
                            tag_1.default.findAll({
                                "globalId": { "$in": noteTagItemsIdList }
                            }, { workspaceId: workspaceId }, function (err, tagObjs) {
                                if (err) {
                                    return resolve([]);
                                }
                                var tags = [];
                                for (var _i = 0, tagObjs_2 = tagObjs; _i < tagObjs_2.length; _i++) {
                                    var tagObj = tagObjs_2[_i];
                                    tags.push(tagObj.title);
                                }
                                resolve(tags);
                            });
                        });
                    })];
            });
        });
    };
    NoteObjRepository.getTextItemForNote = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
                        if (!noteGlobalId) {
                            return resolve(null);
                        }
                        text_1.default.find({ noteGlobalId: noteGlobalId }, { workspaceId: workspaceId }, function (err, textItem) {
                            if (err) {
                                return resolve(null);
                            }
                            resolve(textItem);
                        });
                    })];
            });
        });
    };
    NoteObjRepository.checkIfNoteCanBePassedInAvailableTrafficQuotaForUploadOnServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NoteObjRepository.getNeedForUploadNoteSizeInBytes(inputData, function (err, noteSizeInBytes) {
            var accountManager = NimbusSDK_1.default.getAccountManager();
            accountManager.getTrafficMax(function (err, trafficMax) {
                accountManager.getTrafficCurrent(function (err, trafficCurrent) {
                    var availableTraffic = trafficMax - trafficCurrent;
                    callback(err, availableTraffic >= noteSizeInBytes);
                });
            });
        });
    };
    NoteObjRepository.getNeedForUploadNoteSizeInBytes = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
        var size = 0;
        NoteObjRepository.get({ workspaceId: workspaceId, globalId: noteGlobalId }, function (err, noteObj) { return __awaiter(_this, void 0, void 0, function () {
            var noteTextItem;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!NoteObjRepository.isValid(noteObj)) return [3, 2];
                        return [4, NoteObjRepository.getTextItemForNote({ workspaceId: workspaceId, noteGlobalId: noteObj.globalId })];
                    case 1:
                        noteTextItem = _a.sent();
                        if (noteTextItem && noteTextItem['text']) {
                            size += Buffer.byteLength(noteTextItem['text'], 'utf8');
                        }
                        AttachmentObjRepository_1.default.getNoteAttachmentsForUploadSizeInBytes(inputData, function (err, attachmentSize) {
                            size += attachmentSize;
                            callback(err, size);
                        });
                        return [3, 3];
                    case 2:
                        callback(err, 0);
                        _a.label = 3;
                    case 3: return [2];
                }
            });
        }); });
    };
    NoteObjRepository.getNeedForUploadNoteSizeInBytesAsync = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        NoteObjRepository.getNeedForUploadNoteSizeInBytes(inputData, function (err, res) {
                            resolve(res);
                        });
                    })];
            });
        });
    };
    NoteObjRepository.getUploadNotesTraffic = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId;
                        NoteObjRepository.getUpdatedNotesForUploadOnServer(inputData, function (err, noteObjs) { return __awaiter(_this, void 0, void 0, function () {
                            var size, _i, noteObjs_5, noteObj, _a;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        if (err || !noteObjs) {
                                            return [2, resolve(0)];
                                        }
                                        if (!noteObjs.length) {
                                            return [2, resolve(0)];
                                        }
                                        size = 0;
                                        _i = 0, noteObjs_5 = noteObjs;
                                        _b.label = 1;
                                    case 1:
                                        if (!(_i < noteObjs_5.length)) return [3, 4];
                                        noteObj = noteObjs_5[_i];
                                        _a = size;
                                        return [4, NoteObjRepository.getNeedForUploadNoteSizeInBytesAsync({
                                                workspaceId: workspaceId,
                                                noteGlobalId: noteObj.globalId
                                            })];
                                    case 2:
                                        size = _a + (_b.sent());
                                        _b.label = 3;
                                    case 3:
                                        _i++;
                                        return [3, 1];
                                    case 4:
                                        resolve(size);
                                        return [2];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    NoteObjRepository.getTotalTrafficAfterSync = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId;
                        var accountManager = NimbusSDK_1.default.getAccountManager();
                        accountManager.getTrafficCurrent(function (err, trafficCurrent) { return __awaiter(_this, void 0, void 0, function () {
                            var orgTraffic, totalTraffic, uploadTraffic, orgInstance;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        totalTraffic = 0;
                                        return [4, NoteObjRepository.getUploadNotesTraffic(inputData)];
                                    case 1:
                                        uploadTraffic = _a.sent();
                                        return [4, orgs_1.default.getByWorkspaceId(workspaceId)];
                                    case 2:
                                        orgInstance = _a.sent();
                                        if (orgInstance && orgInstance.usage && orgInstance.usage.traffic) {
                                            orgTraffic = orgInstance.usage.traffic;
                                            trafficCurrent = orgTraffic.current;
                                        }
                                        if (trafficCurrent) {
                                            totalTraffic = trafficCurrent;
                                        }
                                        if (uploadTraffic) {
                                            totalTraffic += uploadTraffic;
                                        }
                                        resolve({
                                            orgTraffic: orgTraffic,
                                            totalTrafficAfterSync: uploadTraffic + trafficCurrent
                                        });
                                        return [2];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    NoteObjRepository.checkIfNoteNotMoreThanLimitNoteSize = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NoteObjRepository.getNeedForUploadNoteSizeInBytes(inputData, function (err, noteSizeinBytes) {
            NimbusSDK_1.default.getAccountManager().getLimitNotesMaxSize(function (err, maxSize) {
                callback(err, maxSize >= noteSizeinBytes);
            });
        });
    };
    NoteObjRepository.updateFullNoteDownloadedFromServerI = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var noteObj, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        noteObj = inputData.noteObj;
                        if (!noteObj) {
                            return [2, callback(null, false)];
                        }
                        return [4, NoteObjRepository.update(inputData)];
                    case 1:
                        result = _a.sent();
                        callback(null, result);
                        return [2];
                }
            });
        });
    };
    NoteObjRepository.updateStructureNotesDownloadedFromServerI = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, noteObjs, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, noteObjs = inputData.noteObjs;
                        if (!noteObjs.length) {
                            return [2, callback(null, false)];
                        }
                        return [4, NoteObjRepository.update({ workspaceId: workspaceId, noteObjs: noteObjs })];
                    case 1:
                        result = _a.sent();
                        callback(null, result);
                        return [2];
                }
            });
        });
    };
    NoteObjRepository.deleteRemovedItemsDownloadedFromServerI = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NoteObjRepository.deleteNotesAndAllDataFromDevice(inputData, function (err, result) {
            callback(err, true);
        });
    };
    NoteObjRepository.deleteNotesAndAllDataFromDevice = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, globalIds, deleteUserNote, removeIdList, _i, globalIds_1, globalId, itemInstance;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, globalIds = inputData.globalIds;
                        if (!(globalIds && globalIds.length)) return [3, 5];
                        deleteUserNote = function (globalId) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        var findQuery = { "globalId": globalId, "type": "note" };
                                        item_1.default.find(findQuery, { workspaceId: workspaceId }, function (err, itemInstance) {
                                            item_1.default.erase(findQuery, { workspaceId: workspaceId }, function (err, numRemoved) {
                                                if (err || !numRemoved) {
                                                    return resolve(null);
                                                }
                                                resolve(itemInstance);
                                            });
                                        });
                                    })];
                            });
                        }); };
                        removeIdList = [];
                        _i = 0, globalIds_1 = globalIds;
                        _a.label = 1;
                    case 1:
                        if (!(_i < globalIds_1.length)) return [3, 4];
                        globalId = globalIds_1[_i];
                        return [4, deleteUserNote(globalId)];
                    case 2:
                        itemInstance = _a.sent();
                        if (itemInstance) {
                            removeIdList.push(itemInstance.globalId);
                            NoteObjRepository.sendSocketEventOnCounter({
                                workspaceId: workspaceId,
                                globalId: itemInstance.globalId,
                                parentId: itemInstance.parentId,
                                type: itemInstance.type
                            });
                        }
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        NoteObjRepository.sendSocketEventOnRemove({
                            workspaceId: workspaceId,
                            globalIdList: removeIdList
                        });
                        _a.label = 5;
                    case 5:
                        callback(null, true);
                        return [2];
                }
            });
        });
    };
    NoteObjRepository.callAfterUploadErasedFromTrashNotesOnServerI = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NoteObjRepository.deleteFolderNotesAndAllDataFromDevice(inputData, function (err, result) {
            callback(err, true);
        });
    };
    NoteObjRepository.deleteFolderNotesAndAllDataFromDevice = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
        var queryData = { "type": "note", "erised": true };
        if (globalId) {
            queryData = { "globalId": globalId };
        }
        item_1.default.findAll(queryData, { workspaceId: workspaceId }, function (err, itemObjs) { return __awaiter(_this, void 0, void 0, function () {
            var deleteItemAndAllRelatedData, removeIdList, _i, itemObjs_1, itemInstance;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        deleteItemAndAllRelatedData = function (itemObj) { return __awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                        var eraseItem, eraseNoteTodos, eraseNoteTags, eraseNoteText, eraseNoteAttachments;
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    eraseItem = function (itemObj) { return __awaiter(_this, void 0, void 0, function () {
                                                        var _this = this;
                                                        return __generator(this, function (_a) {
                                                            return [2, new Promise(function (resolve) {
                                                                    item_1.default.erase({ "globalId": itemObj.globalId }, { workspaceId: workspaceId }, function (err, result) { return __awaiter(_this, void 0, void 0, function () {
                                                                        return __generator(this, function (_a) {
                                                                            resolve(true);
                                                                            return [2];
                                                                        });
                                                                    }); });
                                                                })];
                                                        });
                                                    }); };
                                                    eraseNoteTodos = function (noteObj) { return __awaiter(_this, void 0, void 0, function () {
                                                        return __generator(this, function (_a) {
                                                            return [2, new Promise(function (resolve) {
                                                                    todo_1.default.erase({ "noteGlobalId": noteObj.globalId }, { workspaceId: workspaceId }, function (err, result) {
                                                                        resolve(true);
                                                                    });
                                                                })];
                                                        });
                                                    }); };
                                                    eraseNoteTags = function (noteObj) { return __awaiter(_this, void 0, void 0, function () {
                                                        return __generator(this, function (_a) {
                                                            return [2, new Promise(function (resolve) {
                                                                    noteTags_1.default.erase({ "noteGlobalId": noteObj.globalId }, { workspaceId: workspaceId }, function (err, result) {
                                                                        resolve(true);
                                                                    });
                                                                })];
                                                        });
                                                    }); };
                                                    eraseNoteText = function (noteObj) { return __awaiter(_this, void 0, void 0, function () {
                                                        return __generator(this, function (_a) {
                                                            return [2, new Promise(function (resolve) {
                                                                    text_1.default.erase({ "noteGlobalId": noteObj.globalId }, { workspaceId: workspaceId }, function (err, result) {
                                                                        resolve(true);
                                                                    });
                                                                })];
                                                        });
                                                    }); };
                                                    eraseNoteAttachments = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                        var _this = this;
                                                        return __generator(this, function (_a) {
                                                            return [2, new Promise(function (resolve) {
                                                                    var workspaceId = inputData.workspaceId, noteObj = inputData.noteObj;
                                                                    attach_1.default.findAll({ "noteGlobalId": noteObj.globalId }, { workspaceId: workspaceId }, function (err, attachObjs) { return __awaiter(_this, void 0, void 0, function () {
                                                                        var deleteUserAttachment, _i, attachObjs_1, attachmentObj;
                                                                        var _this = this;
                                                                        return __generator(this, function (_a) {
                                                                            switch (_a.label) {
                                                                                case 0:
                                                                                    if (err) {
                                                                                        return [2, resolve(false)];
                                                                                    }
                                                                                    deleteUserAttachment = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                                        return __generator(this, function (_a) {
                                                                                            return [2, new Promise(function (resolve) {
                                                                                                    var workspaceId = inputData.workspaceId, attachmentObj = inputData.attachmentObj;
                                                                                                    attach_1.default.erase({
                                                                                                        "globalId": attachmentObj.globalId
                                                                                                    }, { workspaceId: workspaceId }, function (err, response) {
                                                                                                        AttachmentObjRepository_1.default.deleteAttachmentFile(AttachmentObj_1.default.getLocalPath(attachmentObj), function (err, response) {
                                                                                                            resolve(response);
                                                                                                        });
                                                                                                    });
                                                                                                })];
                                                                                        });
                                                                                    }); };
                                                                                    _i = 0, attachObjs_1 = attachObjs;
                                                                                    _a.label = 1;
                                                                                case 1:
                                                                                    if (!(_i < attachObjs_1.length)) return [3, 4];
                                                                                    attachmentObj = attachObjs_1[_i];
                                                                                    return [4, deleteUserAttachment({ workspaceId: workspaceId, attachmentObj: attachmentObj })];
                                                                                case 2:
                                                                                    _a.sent();
                                                                                    _a.label = 3;
                                                                                case 3:
                                                                                    _i++;
                                                                                    return [3, 1];
                                                                                case 4:
                                                                                    resolve(true);
                                                                                    return [2];
                                                                            }
                                                                        });
                                                                    }); });
                                                                })];
                                                        });
                                                    }); };
                                                    if (!(itemObj.type === "note")) return [3, 5];
                                                    return [4, eraseNoteTodos(itemObj)];
                                                case 1:
                                                    _a.sent();
                                                    return [4, eraseNoteTags(itemObj)];
                                                case 2:
                                                    _a.sent();
                                                    return [4, eraseNoteText(itemObj)];
                                                case 3:
                                                    _a.sent();
                                                    return [4, eraseNoteAttachments({ workspaceId: workspaceId, noteObj: itemObj })];
                                                case 4:
                                                    _a.sent();
                                                    _a.label = 5;
                                                case 5: return [4, eraseItem(itemObj)];
                                                case 6:
                                                    _a.sent();
                                                    resolve(true);
                                                    return [2];
                                            }
                                        });
                                    }); })];
                            });
                        }); };
                        removeIdList = [];
                        _i = 0, itemObjs_1 = itemObjs;
                        _a.label = 1;
                    case 1:
                        if (!(_i < itemObjs_1.length)) return [3, 4];
                        itemInstance = itemObjs_1[_i];
                        return [4, deleteItemAndAllRelatedData(itemInstance)];
                    case 2:
                        _a.sent();
                        removeIdList.push(itemInstance.globalId);
                        NoteObjRepository.sendSocketEventOnCounter({
                            workspaceId: workspaceId,
                            globalId: itemInstance.globalId,
                            parentId: itemInstance.parentId,
                            type: itemInstance.type
                        });
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        NoteObjRepository.sendSocketEventOnRemove({
                            workspaceId: workspaceId,
                            globalIdList: removeIdList
                        });
                        callback(null, true);
                        return [2];
                }
            });
        }); });
    };
    NoteObjRepository.getUserNotesR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NoteObjRepository.getAllNotesR(inputData, function (err, list) {
            callback(err, list);
        });
    };
    NoteObjRepository.getAllNotesR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({ "type": "note" }, { workspaceId: workspaceId }, function (err, list) {
            return callback(err, list);
        });
    };
    NoteObjRepository.getR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
        if (globalId === null) {
            globalId = "";
        }
        item_1.default.find({ "globalId": globalId, "type": "note" }, { workspaceId: workspaceId }, function (err, item) {
            callback(err, item);
        });
    };
    NoteObjRepository.callAfterUploadUpdatedHeaderNotesOnServerI = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({
            "type": "note",
            "parentId": { "$ne": FolderObj_1.default.ERASED_FROM_TRASH },
            "needSync": true
        }, { workspaceId: workspaceId }, function (err, noteObjs) { return __awaiter(_this, void 0, void 0, function () {
            var _i, noteObjs_6, noteObj, _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        if (err) {
                            return [2, callback(err, false)];
                        }
                        for (_i = 0, noteObjs_6 = noteObjs; _i < noteObjs_6.length; _i++) {
                            noteObj = noteObjs_6[_i];
                            noteObj.needSync = false;
                        }
                        _a = callback;
                        _b = [null];
                        return [4, NoteObjRepository.update({ workspaceId: workspaceId, noteObjs: noteObjs })];
                    case 1:
                        _a.apply(void 0, _b.concat([_c.sent()]));
                        return [2];
                }
            });
        }); });
    };
    NoteObjRepository.callAfterUploadFullSyncI = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, note = inputData.note, lastUpdateTime = inputData.lastUpdateTime, syncStartDate = inputData.syncStartDate;
        NoteObjRepository.getR({ workspaceId: workspaceId, globalId: note.global_id }, function (err, noteObj) { return __awaiter(_this, void 0, void 0, function () {
            var updateSyncProp, existItemInstance, saveInstance, result, noteTodoList, _i, noteTodoList_1, noteTodo, noteAttachmentList, _a, noteAttachmentList_1, noteAttachment, noteTagList, _b, noteTagList_1, noteTag;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        if (err) {
                            callback(null, null);
                        }
                        if (!NoteObjRepository.isValid(noteObj)) {
                            return [2, callback(null, null)];
                        }
                        updateSyncProp = noteObj.syncDate <= syncStartDate;
                        noteObj.tags = note.tags;
                        return [4, NoteObjRepository.getUserNote({ workspaceId: workspaceId, globalId: noteObj.globalId })];
                    case 1:
                        existItemInstance = _c.sent();
                        return [4, NoteObjRepository.convertSyncInstanceToLocal({
                                workspaceId: workspaceId,
                                noteObj: noteObj,
                                existItemInstance: existItemInstance
                            })];
                    case 2:
                        saveInstance = _c.sent();
                        if (!saveInstance) {
                            return [2, callback(null, false)];
                        }
                        saveInstance['isDownloaded'] = true;
                        saveInstance['existOnServer'] = true;
                        if (updateSyncProp) {
                            saveInstance['needSync'] = false;
                            saveInstance['syncDate'] = lastUpdateTime;
                        }
                        return [4, NoteObjRepository.updateUserNote({ workspaceId: workspaceId, item: saveInstance })];
                    case 3:
                        result = _c.sent();
                        if (!result) return [3, 18];
                        return [4, TodoObjRepository_1.default.findNoteTodoList({
                                workspaceId: workspaceId,
                                noteGlobalId: noteObj.globalId
                            })];
                    case 4:
                        noteTodoList = _c.sent();
                        _i = 0, noteTodoList_1 = noteTodoList;
                        _c.label = 5;
                    case 5:
                        if (!(_i < noteTodoList_1.length)) return [3, 8];
                        noteTodo = noteTodoList_1[_i];
                        if (noteTodo.syncDate > syncStartDate) {
                            return [3, 7];
                        }
                        return [4, TodoObjRepository_1.default.updateNeedSync({
                                workspaceId: workspaceId,
                                globalId: noteTodo.globalId,
                                noteGlobalId: noteTodo.noteGlobalId
                            })];
                    case 6:
                        _c.sent();
                        _c.label = 7;
                    case 7:
                        _i++;
                        return [3, 5];
                    case 8: return [4, AttachmentObjRepository_1.default.findNoteAttachmentList({
                            workspaceId: workspaceId,
                            noteGlobalId: noteObj.globalId
                        })];
                    case 9:
                        noteAttachmentList = _c.sent();
                        _a = 0, noteAttachmentList_1 = noteAttachmentList;
                        _c.label = 10;
                    case 10:
                        if (!(_a < noteAttachmentList_1.length)) return [3, 13];
                        noteAttachment = noteAttachmentList_1[_a];
                        if (noteAttachment.syncDate > syncStartDate) {
                            return [3, 12];
                        }
                        return [4, AttachmentObjRepository_1.default.updateNeedSync({
                                workspaceId: workspaceId,
                                globalId: noteAttachment.globalId,
                                noteGlobalId: noteAttachment.noteGlobalId,
                                displayName: noteAttachment.displayName
                            })];
                    case 11:
                        _c.sent();
                        _c.label = 12;
                    case 12:
                        _a++;
                        return [3, 10];
                    case 13: return [4, TagObjRepository_1.default.findNoteTagList({
                            workspaceId: workspaceId,
                            noteGlobalId: noteObj.globalId
                        })];
                    case 14:
                        noteTagList = _c.sent();
                        _b = 0, noteTagList_1 = noteTagList;
                        _c.label = 15;
                    case 15:
                        if (!(_b < noteTagList_1.length)) return [3, 18];
                        noteTag = noteTagList_1[_b];
                        if (noteTag.syncDate > syncStartDate) {
                            return [3, 17];
                        }
                        return [4, TagObjRepository_1.default.updateNeedSync({
                                workspaceId: workspaceId,
                                globalId: noteTag.globalId,
                                noteGlobalId: noteTag.noteGlobalId,
                                tag: noteTag.tag
                            })];
                    case 16:
                        _c.sent();
                        _c.label = 17;
                    case 17:
                        _b++;
                        return [3, 15];
                    case 18:
                        if (noteObj.text_version >= 2) {
                            TextEditor_1.default.updateSyncedStatus({ noteId: noteObj.globalId });
                        }
                        callback(null, result);
                        return [2];
                }
            });
        }); });
    };
    NoteObjRepository.isValid = function (noteObj) {
        return !!noteObj;
    };
    NoteObjRepository.checkIfNoteInTrash = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, parentId = inputData.parentId;
        var isInTrash = false;
        if (FolderObj_1.default.TRASH === parentId) {
            return callback(null, true);
        }
        FolderObjRepository_1.default.get({ workspaceId: workspaceId, globalId: parentId }, function (err, folderObj) {
            isInTrash = false;
            return callback(err, isInTrash);
        });
    };
    NoteObjRepository.deleteTodoData = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                        text_1.default.erase({
                            "noteGlobalId": globalId,
                            "type": "note"
                        }, { workspaceId: workspaceId }, function (err, response) {
                            if (err) {
                                return resolve(null);
                            }
                            item_1.default.erase({
                                "globalId": globalId,
                                "type": "note"
                            }, { workspaceId: workspaceId }, function (err, response) {
                                if (err) {
                                    return resolve(null);
                                }
                                resolve(response);
                            });
                        });
                    })];
            });
        });
    };
    NoteObjRepository.clearRemovedData = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({
            "erised": true,
            "type": "note"
        }, { workspaceId: workspaceId }, function (err, noteObjs) { return __awaiter(_this, void 0, void 0, function () {
            var _i, noteObjs_7, noteObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _i = 0, noteObjs_7 = noteObjs;
                        _a.label = 1;
                    case 1:
                        if (!(_i < noteObjs_7.length)) return [3, 4];
                        noteObj = noteObjs_7[_i];
                        return [4, NoteObjRepository.deleteTodoData({ workspaceId: workspaceId, globalId: noteObj.globalId })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        callback(null, true);
                        return [2];
                }
            });
        }); });
    };
    NoteObjRepository.removeItemPreview = function (inputData) {
        return new Promise(function (resolve) {
            var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
            item_1.default.find({ globalId: globalId }, { workspaceId: workspaceId }, function (err, itemObj) {
                if (err || !itemObj) {
                    return resolve(false);
                }
                item_1.default.update({
                    globalId: globalId,
                    type: 'note',
                }, { preview: null }, { workspaceId: workspaceId }, function () {
                    return resolve(true);
                });
            });
        });
    };
    NoteObjRepository.cleanUploadItem = function (note) {
        if (!note) {
            return note;
        }
        var cleanProps = [
            'erised',
            'rootId',
            'globalId',
            '_id',
            'parentId',
            'rootParentId',
            'createdAt',
            'dateAdded',
            'dateUpdated',
            'updatedAt',
            'dateUpdatedUser',
            'locationLat',
            'locationLng',
            'lastChangeBy',
            'size',
            'isEncrypted',
            'offlineOnly',
            'shared',
            'passwordRequired',
            'shareId',
            'securityKey',
            'isFullwidth',
            'existOnServer',
            'uniqueUserName',
            'syncDate',
            'needSync',
            'isMaybeInTrash',
            'isClicked',
            'subfoldersCount',
            'notesCount',
            'level',
            'isTemp',
            'shortText',
            'firstImage',
            'isMoreThanLimit',
            'textAttachmentGlobalId',
            'attachmentsInListCount',
            'attachmentsInListExist',
            'reminderExist',
            'todoExist',
            'reminderLabel',
            'todoCount',
            'locationAddress',
            'isLocationExist',
            'isDownloaded',
            'noteObj',
        ];
        for (var prop in note) {
            if (!note.hasOwnProperty(prop)) {
                continue;
            }
            if (prop && prop.indexOf('UpdateTime') > 0) {
                delete note[prop];
                continue;
            }
            if (cleanProps.indexOf(prop) >= 0) {
                delete note[prop];
            }
            if (prop === 'attachements' && note[prop]) {
                note[prop] = note[prop].map(function (attachment) { return AttachmentObjRepository_1.default.cleanUploadItem(attachment); });
            }
        }
        return __assign({}, note);
    };
    return NoteObjRepository;
}());
exports.default = NoteObjRepository;
