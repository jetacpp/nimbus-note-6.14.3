"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_extra_1 = __importDefault(require("fs-extra"));
var config_1 = __importDefault(require("../../../../config"));
var NimbusSDK_1 = __importDefault(require("../../nimbussdk/net/NimbusSDK"));
var AttachmentObj_1 = __importDefault(require("../db/AttachmentObj"));
var NoteObjRepository_1 = __importDefault(require("./NoteObjRepository"));
var SyncAttachmentRenamedEntity_1 = __importDefault(require("../../nimbussdk/net/response/entities/SyncAttachmentRenamedEntity"));
var SyncAttachmentEntity_1 = __importDefault(require("../../nimbussdk/net/response/entities/SyncAttachmentEntity"));
var attach_1 = __importDefault(require("../../../db/models/attach"));
var item_1 = __importDefault(require("../../../db/models/item"));
var socketFunctions_1 = __importDefault(require("../../socket/socketFunctions"));
var socketStoreType_1 = __importDefault(require("../../socket/socketStoreType"));
var pdb_1 = __importDefault(require("../../../../pdb"));
var AttachmentObjRepository = (function () {
    function AttachmentObjRepository() {
    }
    AttachmentObjRepository.create = function (globalId, parentId, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NimbusSDK_1.default.getAccountManager().getUniqueUserName(function (err, uniqueUserName) {
            var attachment = new AttachmentObj_1.default();
            attachment.globalId = globalId;
            attachment.parentId = parentId;
            attachment.dateAdded = new Date().getTime();
            attachment.syncDate = 0;
            attachment.needSync = true;
            attachment.location = null;
            attachment = attachment.setLocalPath(null);
            attachment.tempName = null;
            attachment.type = null;
            attachment.typeExtra = null;
            attachment.size = 0;
            attachment.extension = null;
            attachment.displayName = null;
            attachment.oldDisplayName = null;
            attachment.inList = 0;
            attachment.uniqueUserName = uniqueUserName;
            attachment.fileUUID = null;
            callback(err, attachment);
        });
    };
    AttachmentObjRepository.isAvailableNotesForSync = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NoteObjRepository_1.default.isAvailableNotesForSync(inputData, callback);
    };
    AttachmentObjRepository.getErasedAttachmentsForUploadOnServer = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        attach_1.default.findAll({
            "erised": true,
            "needSync": true,
        }, { workspaceId: workspaceId }, function (err, attatchObjs) { return __awaiter(_this, void 0, void 0, function () {
            var attachmentsList, _i, attatchObjs_1, attachmentObj, attachmentsIdList;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        attachmentsList = {};
                        for (_i = 0, attatchObjs_1 = attatchObjs; _i < attatchObjs_1.length; _i++) {
                            attachmentObj = attatchObjs_1[_i];
                            if (typeof (attachmentsList[attachmentObj.noteGlobalId]) === "undefined") {
                                attachmentsList[attachmentObj.noteGlobalId] = [];
                            }
                            attachmentsList[attachmentObj.noteGlobalId].push(attachmentObj.globalId);
                        }
                        return [4, AttachmentObjRepository.filterOfflineAttachmentList({ workspaceId: workspaceId, attachmentsList: attachmentsList })];
                    case 1:
                        attachmentsIdList = _a.sent();
                        callback(err, attachmentsIdList);
                        return [2];
                }
            });
        }); });
    };
    AttachmentObjRepository.filterOfflineAttachmentList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, attachmentsList = inputData.attachmentsList;
                        var noteIdList = Object.keys(attachmentsList);
                        item_1.default.findAll({
                            "globalId": { "$in": noteIdList },
                            "type": "note",
                        }, { workspaceId: workspaceId }, function (err, noteObjs) {
                            var getAttachmentsList = function (attachmentsList) {
                                var result = [];
                                for (var noteGlobalId in attachmentsList) {
                                    if (attachmentsList.hasOwnProperty(noteGlobalId)) {
                                        var attachmentsChunk = attachmentsList[noteGlobalId];
                                        for (var _i = 0, attachmentsChunk_1 = attachmentsChunk; _i < attachmentsChunk_1.length; _i++) {
                                            var attachmentId = attachmentsChunk_1[_i];
                                            result.push(attachmentId);
                                        }
                                    }
                                }
                                return result;
                            };
                            for (var _i = 0, noteObjs_1 = noteObjs; _i < noteObjs_1.length; _i++) {
                                var noteObj = noteObjs_1[_i];
                                if (noteObj.offlineOnly) {
                                    if (typeof (attachmentsList[noteObj.globalId]) !== 'undefined') {
                                        delete attachmentsList[noteObj.globalId];
                                    }
                                }
                            }
                            resolve(getAttachmentsList(attachmentsList));
                        });
                    })];
            });
        });
    };
    AttachmentObjRepository.getUserAttachmentsR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        AttachmentObjRepository.getAllAttachmentsR(inputData, function (err, list) {
            callback(err, list);
        });
    };
    AttachmentObjRepository.getAllAttachmentsR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        attach_1.default.findAll({}, { workspaceId: workspaceId }, function (err, list) {
            return callback(err, list);
        });
    };
    AttachmentObjRepository.getRenamedAttachmentsForUploadOnServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
        attach_1.default.findAll({
            "parentId": noteGlobalId,
            "oldDisplayName": { "$ne": null },
            "needSync": true
        }, { workspaceId: workspaceId }, function (err, attachObjs) {
            var attachmentEntities = [];
            for (var _i = 0, attachObjs_1 = attachObjs; _i < attachObjs_1.length; _i++) {
                var attachmentObj = attachObjs_1[_i];
                if (attachmentObj.tempName || attachmentObj.location) {
                    var entity = new SyncAttachmentRenamedEntity_1.default();
                    entity.global_id = attachmentObj.globalId;
                    entity.display_name = attachmentObj.displayName;
                    attachmentEntities.push(entity);
                }
            }
            callback(err, attachmentEntities);
        });
    };
    AttachmentObjRepository.getNoteAttachmentForUploadOnServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
        attach_1.default.findAll({
            "noteGlobalId": noteGlobalId,
            "needSync": true,
            "erised": false
        }, { workspaceId: workspaceId }, function (err, attachObjs) {
            if (config_1.default.SHOW_WEB_CONSOLE) {
            }
            if (err) {
                return callback(err, []);
            }
            callback(err, attachObjs);
        });
    };
    AttachmentObjRepository.getNotDownloadedAttachmentFromDownloadedNotesForDownloadFromServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        attach_1.default.findAll({
            "location": { "$ne": null },
            "isDownloaded": false,
            "tryDownloadCounter": { "$lt": AttachmentObjRepository.DOWNLOAD_MAX_TRY },
        }, {
            workspaceId: workspaceId,
            "order": {
                "dateAdded": "desc"
            }
        }, function (err, attachObjs) {
            callback(err, attachObjs);
        });
    };
    AttachmentObjRepository.getNoteAttachmentsInListCount = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, parentId = inputData.parentId;
        attach_1.default.count({
            "parentId": parentId,
            "inList": 1
        }, { workspaceId: workspaceId }, function (err, attachmentsCount) {
            callback(err, attachmentsCount);
        });
    };
    AttachmentObjRepository.updateTempNameAfterUploadOnServerI = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, tempName = inputData.tempName;
        AttachmentObjRepository.getR(inputData, function (err, attachmentObj) { return __awaiter(_this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = null;
                        if (!AttachmentObjRepository.isValid(attachmentObj)) return [3, 2];
                        attachmentObj.tempName = tempName;
                        return [4, AttachmentObjRepository.update({ workspaceId: workspaceId, attachmentObjs: [attachmentObj] })];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2:
                        callback(err, result);
                        return [2];
                }
            });
        }); });
    };
    AttachmentObjRepository.updateDownloadStatus = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId;
                        var _this = this;
                        return __generator(this, function (_a) {
                            workspaceId = inputData.workspaceId;
                            AttachmentObjRepository.getR(inputData, function (err, attachmentObj) { return __awaiter(_this, void 0, void 0, function () {
                                var result;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            result = null;
                                            if (!AttachmentObjRepository.isValid(attachmentObj)) return [3, 2];
                                            attachmentObj.isDownloaded = true;
                                            return [4, AttachmentObjRepository.update({ workspaceId: workspaceId, attachmentObjs: [attachmentObj] })];
                                        case 1:
                                            result = _a.sent();
                                            _a.label = 2;
                                        case 2:
                                            resolve(result);
                                            return [2];
                                    }
                                });
                            }); });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    AttachmentObjRepository.getR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
        if (globalId === null) {
            globalId = "";
        }
        attach_1.default.find({
            "globalId": globalId
        }, { workspaceId: workspaceId }, function (err, attachObj) {
            callback(err, attachObj);
        });
    };
    AttachmentObjRepository.updateAttachmentFromManualDownloadI = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, attachmentObj, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, attachmentObj = inputData.attachmentObj;
                        result = null;
                        if (!(attachmentObj && attachmentObj.globalId)) return [3, 2];
                        attachmentObj.isDownloaded = true;
                        return [4, AttachmentObjRepository.update({ workspaceId: workspaceId, attachmentObjs: [attachmentObj] })];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2:
                        callback(null, result);
                        return [2];
                }
            });
        });
    };
    AttachmentObjRepository.update = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, attachmentObjs, sendSocketEvents, getUserAttachment, updateUserAttachment, createUserAttachment, processUserAttachment, _i, attachmentObjs_1, attachmentObj;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, attachmentObjs = inputData.attachmentObjs, sendSocketEvents = inputData.sendSocketEvents;
                                    sendSocketEvents = sendSocketEvents || false;
                                    if (!attachmentObjs) {
                                        return [2, resolve(false)];
                                    }
                                    getUserAttachment = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                                                    attach_1.default.find({ globalId: globalId }, { workspaceId: workspaceId }, function (err, attachmentObj) {
                                                        if (err || !attachmentObj) {
                                                            return resolve(null);
                                                        }
                                                        resolve(attachmentObj);
                                                    });
                                                })];
                                        });
                                    }); };
                                    updateUserAttachment = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            if (config_1.default.SHOW_WEB_CONSOLE) {
                                            }
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, item = inputData.item;
                                                    attach_1.default.update({
                                                        globalId: item.globalId,
                                                        noteGlobalId: item.noteGlobalId
                                                    }, item, { workspaceId: workspaceId }, function (err, count) {
                                                        if (err || !count) {
                                                            return resolve(false);
                                                        }
                                                        resolve(!!count);
                                                    });
                                                })];
                                        });
                                    }); };
                                    createUserAttachment = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            if (config_1.default.SHOW_WEB_CONSOLE) {
                                            }
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, item = inputData.item;
                                                    attach_1.default.add(item, { workspaceId: workspaceId }, function (err, attachmentInstance) {
                                                        if (err || !attachmentInstance) {
                                                            return resolve(false);
                                                        }
                                                        resolve(!!attachmentInstance);
                                                    });
                                                })];
                                        });
                                    }); };
                                    processUserAttachment = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var workspaceId, attachmentObj, existItemInstance, saveInstance, result;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                workspaceId = inputData.workspaceId, attachmentObj = inputData.attachmentObj;
                                                                return [4, getUserAttachment({ workspaceId: workspaceId, globalId: attachmentObj.globalId })];
                                                            case 1:
                                                                existItemInstance = _a.sent();
                                                                return [4, AttachmentObjRepository.convertSyncInstanceToLocal(attachmentObj, existItemInstance)];
                                                            case 2:
                                                                saveInstance = _a.sent();
                                                                if (!saveInstance) {
                                                                    return [2, resolve(false)];
                                                                }
                                                                saveInstance['needSync'] = false;
                                                                result = null;
                                                                if (!existItemInstance) return [3, 4];
                                                                return [4, updateUserAttachment({ workspaceId: workspaceId, item: saveInstance })];
                                                            case 3:
                                                                result = _a.sent();
                                                                return [3, 6];
                                                            case 4: return [4, createUserAttachment({ workspaceId: workspaceId, item: saveInstance })];
                                                            case 5:
                                                                result = _a.sent();
                                                                _a.label = 6;
                                                            case 6:
                                                                if (result && sendSocketEvents) {
                                                                    AttachmentObjRepository.sendSocketEventOnUpdate({ workspaceId: workspaceId, attachmentObj: saveInstance });
                                                                }
                                                                resolve(result);
                                                                return [2];
                                                        }
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    _i = 0, attachmentObjs_1 = attachmentObjs;
                                    _a.label = 1;
                                case 1:
                                    if (!(_i < attachmentObjs_1.length)) return [3, 4];
                                    attachmentObj = attachmentObjs_1[_i];
                                    return [4, processUserAttachment({ workspaceId: workspaceId, attachmentObj: attachmentObj })];
                                case 2:
                                    _a.sent();
                                    _a.label = 3;
                                case 3:
                                    _i++;
                                    return [3, 1];
                                case 4:
                                    resolve(true);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    AttachmentObjRepository.sendSocketEventOnUpdate = function (inputData) {
        var workspaceId = inputData.workspaceId, attachmentObj = inputData.attachmentObj;
        if (attachmentObj) {
            socketFunctions_1.default.addStoreData({
                workspaceId: workspaceId,
                storeType: socketStoreType_1.default.NOTES_ATTACHMENTS_FOR_UPDATE,
                data: {
                    noteGlobalId: attachmentObj.noteGlobalId,
                    globalId: attachmentObj.globalId
                }
            });
        }
    };
    AttachmentObjRepository.sendSocketEventOnRemove = function (inputData) {
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, noteGlobalId = inputData.noteGlobalId;
        if (globalId && noteGlobalId) {
            socketFunctions_1.default.addStoreData({
                workspaceId: workspaceId,
                storeType: socketStoreType_1.default.NOTES_ATTACHMENTS_FOR_REMOVE,
                data: {
                    noteGlobalId: noteGlobalId,
                    globalId: globalId
                }
            });
        }
    };
    AttachmentObjRepository.convertSyncInstanceToLocal = function (attachmentObj, existItemInstance) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var fillSyncObjProperties, fillModelProperties, attachmentInstance;
                        return __generator(this, function (_a) {
                            if (!attachmentObj) {
                                return [2, resolve(null)];
                            }
                            fillSyncObjProperties = function (item, data) {
                                return attach_1.default.prepareSyncOnlyProperties(attach_1.default.prepareItemDbProperties(item, data), data);
                            };
                            fillModelProperties = function (item, data) {
                                return attach_1.default.prepareCommonProperties(attach_1.default.prepareItemDbProperties(item, data), data);
                            };
                            attachmentInstance = null;
                            if (existItemInstance) {
                                attachmentInstance = fillSyncObjProperties(attach_1.default.prepareModelData(existItemInstance), attachmentObj);
                                if (attachmentInstance.displayName !== attachmentObj.display_name) {
                                    attachmentInstance.oldDisplayName = attachmentInstance.displayName;
                                }
                                if (attachmentObj.display_name) {
                                    attachmentInstance.displayName = attachmentObj.display_name;
                                }
                            }
                            else {
                                attachmentInstance = fillModelProperties(attach_1.default.prepareModelData(attachmentObj), attachmentObj);
                                attachmentInstance.isDownloaded = false;
                            }
                            resolve(attachmentInstance);
                            return [2];
                        });
                    }); })];
            });
        });
    };
    AttachmentObjRepository.findNoteAttachmentList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
                        attach_1.default.findAll({
                            "noteGlobalId": noteGlobalId
                        }, { workspaceId: workspaceId }, function (err, attachObjs) {
                            if (err) {
                                return resolve([]);
                            }
                            resolve(attachObjs);
                        });
                    })];
            });
        });
    };
    AttachmentObjRepository.findAttachment = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                        attach_1.default.find({ globalId: globalId }, { workspaceId: workspaceId }, function (err, attachObjs) {
                            if (err) {
                                return resolve([]);
                            }
                            resolve(attachObjs);
                        });
                    })];
            });
        });
    };
    AttachmentObjRepository.removeNoteAttachmentList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, attachmentList = inputData.attachmentList;
                        if (!attachmentList.length) {
                            return resolve(true);
                        }
                        var attachmentGlobalIdList = [];
                        for (var _i = 0, attachmentList_1 = attachmentList; _i < attachmentList_1.length; _i++) {
                            var attachmentInstance = attachmentList_1[_i];
                            AttachmentObjRepository.sendSocketEventOnRemove({
                                workspaceId: workspaceId,
                                globalId: attachmentInstance.globalId,
                                noteGlobalId: attachmentInstance.noteGlobalId
                            });
                            attachmentGlobalIdList.push(attachmentInstance.globalId);
                        }
                        if (!attachmentGlobalIdList.length) {
                            return resolve(true);
                        }
                        attach_1.default.erase({
                            "globalId": { "$in": attachmentGlobalIdList }
                        }, { workspaceId: workspaceId }, function (err, numRemoved) {
                            if (err) {
                                return resolve(false);
                            }
                            resolve(true);
                        });
                    })];
            });
        });
    };
    AttachmentObjRepository.updateAttachmentsDownloadedFromServerI = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, attachmentObjs, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, attachmentObjs = inputData.attachmentObjs;
                        result = null;
                        if (!(attachmentObjs && attachmentObjs.length > 0)) return [3, 2];
                        return [4, AttachmentObjRepository.update({ workspaceId: workspaceId, attachmentObjs: attachmentObjs })];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2:
                        callback(null, result);
                        return [2];
                }
            });
        });
    };
    AttachmentObjRepository.checkIfAttachmentNotMoreThanLimitAttachmentSize = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        AttachmentObjRepository.get(inputData, function (err, attachmentObj) { return __awaiter(_this, void 0, void 0, function () {
            var attachSize, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!attachmentObj) {
                            return [2, callback(null, false)];
                        }
                        attachSize = attachmentObj.size || 0;
                        return [4, attach_1.default.checkFileUploadLimit({ workspaceId: workspaceId, size: attachSize })];
                    case 1:
                        result = _a.sent();
                        return [2, callback(err, result)];
                }
            });
        }); });
    };
    AttachmentObjRepository.convertToSyncAttachmentEntity = function (attachmentObj, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var attachment = new SyncAttachmentEntity_1.default();
        attachment.global_id = attachmentObj.globalId;
        attachment.date_added = attachmentObj.dateAdded;
        attachment.display_name = attachmentObj.displayName;
        attachment.in_list = attachmentObj.inList ? 1 : 0;
        attachment.type = attachmentObj.type;
        attachment.tempname = attachmentObj.tempName;
        attachment.mime = AttachmentObjRepository.getMimeTypeFromExtension(attachmentObj.extension);
        callback(null, attachment);
    };
    AttachmentObjRepository.convertToSyncAttachmentEntityForManualDownload = function (attachmentObj) {
        var attachment = new SyncAttachmentEntity_1.default();
        attachment.global_id = attachmentObj.globalId;
        attachment.date_added = attachmentObj.dateAdded;
        attachment.date_updated = attachmentObj.syncDate;
        attachment.location = attachmentObj.location;
        attachment.type = attachmentObj.type;
        attachment.role = null;
        attachment.type_extra = attachmentObj.typeExtra;
        attachment.size = attachmentObj.size;
        attachment.in_list = attachmentObj.inList;
        attachment.is_encrypted = 0;
        attachment.display_name = attachmentObj.displayName;
        attachment.mime = AttachmentObjRepository.getMimeTypeFromExtension(attachmentObj.extension);
        attachment.tempname = attachmentObj.tempName;
        attachment.file_uuid = attachmentObj.storedFileUUID ? attachmentObj.storedFileUUID : attachmentObj.fileUUID;
        return attachment;
    };
    AttachmentObjRepository.getMimeTypeFromExtension = function (extension) {
        return "";
    };
    AttachmentObjRepository.getExtensionFromMimeType = function (mimeType) {
        return "";
    };
    AttachmentObjRepository.deleteRemovedItemsDownloadedFromServerI = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var globalIds = inputData.globalIds;
        if (!globalIds.length) {
            return callback(null, false);
        }
        AttachmentObjRepository.deleteAttachmentsAndAllDataFromDevice(inputData, function (err, response) {
            callback(err, true);
        });
    };
    AttachmentObjRepository.deleteAttachmentsAndAllDataFromDevice = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, globalIds, deleteUserAttachment, _i, globalIds_1, globalId;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, globalIds = inputData.globalIds;
                        deleteUserAttachment = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        AttachmentObjRepository.deleteAttachmentAndAllDataFromDevice(inputData, function (err, response) {
                                            if (err) {
                                                return resolve(false);
                                            }
                                            resolve(response);
                                        });
                                    })];
                            });
                        }); };
                        _i = 0, globalIds_1 = globalIds;
                        _a.label = 1;
                    case 1:
                        if (!(_i < globalIds_1.length)) return [3, 4];
                        globalId = globalIds_1[_i];
                        return [4, deleteUserAttachment({ workspaceId: workspaceId, globalId: globalId })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        callback(null, true);
                        return [2];
                }
            });
        });
    };
    AttachmentObjRepository.deleteAttachmentAndAllDataFromDevice = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        AttachmentObjRepository.checkIfAttachmentExist(inputData, function (err, attachExist) {
            if (attachExist) {
                var workspaceId_1 = inputData.workspaceId, globalId_1 = inputData.globalId;
                AttachmentObjRepository.get(inputData, function (err, attachment) {
                    attach_1.default.erase({
                        "globalId": globalId_1
                    }, { workspaceId: workspaceId_1 }, function (err, response) {
                        var targetPath = AttachmentObjRepository.getAttachLocalFilePath(attachment);
                        AttachmentObjRepository.deleteAttachmentFile(targetPath, function (err, response) {
                            AttachmentObjRepository.sendSocketEventOnRemove({
                                workspaceId: workspaceId_1,
                                globalId: attachment.globalId,
                                noteGlobalId: attachment.noteGlobalId
                            });
                            callback(err, response);
                        });
                    });
                });
            }
            else {
                callback(err, false);
            }
        });
    };
    AttachmentObjRepository.eraseAttachmentFile = function (inputData) {
        return new Promise(function (resolve) {
            var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
            attach_1.default.find({ globalId: globalId }, { workspaceId: workspaceId }, function (err, attachObj) {
                if (err || !attachObj) {
                    return resolve(false);
                }
                item_1.default.find({ globalId: attachObj.noteGlobalId }, { workspaceId: workspaceId }, function (err, itemObj) {
                    if (err || !itemObj) {
                        return resolve(false);
                    }
                    if (itemObj && itemObj.preview && itemObj.preview.global_id && itemObj.preview.global_id === globalId) {
                        item_1.default.update({
                            globalId: attachObj.noteGlobalId,
                            type: 'note',
                        }, { preview: null }, { workspaceId: workspaceId }, function () {
                            attach_1.default.erase({
                                "globalId": globalId
                            }, { workspaceId: workspaceId }, function () {
                                return resolve(true);
                            });
                        });
                    }
                    else {
                        attach_1.default.erase({
                            "globalId": globalId
                        }, { workspaceId: workspaceId }, function () {
                            return resolve(true);
                        });
                    }
                });
            });
        });
    };
    AttachmentObjRepository.getAttachLocalFilePath = function (attachItem) {
        var targetPath = "";
        if (attachItem && attachItem.storedFileUUID) {
            targetPath = pdb_1.default.getClientAttachmentPath() + "/" + attachItem.storedFileUUID;
        }
        return targetPath;
    };
    AttachmentObjRepository.deleteAttachmentFile = function (localPath, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var exists;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!localPath) {
                            return [2, callback(null, true)];
                        }
                        if (!localPath) return [3, 2];
                        return [4, fs_extra_1.default.exists(localPath)];
                    case 1:
                        exists = _a.sent();
                        if (exists) {
                            try {
                                fs_extra_1.default.unlink(localPath, function () {
                                });
                            }
                            catch (e) {
                                if (config_1.default.SHOW_WEB_CONSOLE) {
                                    console.log("Error => AttachmentObjRepository => deleteAttachmentFile => remove file: ", localPath, e);
                                }
                            }
                        }
                        callback(null, true);
                        _a.label = 2;
                    case 2: return [2];
                }
            });
        });
    };
    AttachmentObjRepository.checkIfAttachmentExist = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
        attach_1.default.find({
            "globalId": globalId
        }, { workspaceId: workspaceId }, function (err, attachObj) {
            callback(err, !!attachObj);
        });
    };
    AttachmentObjRepository.get = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
        if (globalId === null) {
            globalId = "";
        }
        attach_1.default.find({
            "globalId": globalId
        }, { workspaceId: workspaceId }, function (err, attachObj) {
            if (AttachmentObjRepository.isValid(attachObj)) {
                callback(err, __assign({}, attachObj));
            }
            else {
                callback(err, null);
            }
        });
    };
    AttachmentObjRepository.isValid = function (attachmentObj) {
        return attachmentObj !== null && AttachmentObj_1.default.isValid(attachmentObj);
    };
    AttachmentObjRepository.callAfterUploadErasedAttachmentsOnServerI = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        attach_1.default.findAll({
            "erised": true,
            "needSync": true
        }, { workspaceId: workspaceId }, function (err, attachObjs) { return __awaiter(_this, void 0, void 0, function () {
            var deleteUserAttachment, _i, attachObjs_2, attachmentObj;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (err) {
                            return [2, callback(err, false)];
                        }
                        deleteUserAttachment = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        var workspaceId = inputData.workspaceId, attachmentObj = inputData.attachmentObj;
                                        attach_1.default.erase({
                                            "globalId": attachmentObj.globalId
                                        }, { workspaceId: workspaceId }, function (err, response) {
                                            var targetPath = AttachmentObjRepository.getAttachLocalFilePath(attachmentObj);
                                            AttachmentObjRepository.deleteAttachmentFile(targetPath, function (err, response) {
                                                resolve(response);
                                            });
                                        });
                                    })];
                            });
                        }); };
                        _i = 0, attachObjs_2 = attachObjs;
                        _a.label = 1;
                    case 1:
                        if (!(_i < attachObjs_2.length)) return [3, 4];
                        attachmentObj = attachObjs_2[_i];
                        return [4, deleteUserAttachment({ workspaceId: workspaceId, attachmentObj: attachmentObj })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        callback(null, true);
                        return [2];
                }
            });
        }); });
    };
    AttachmentObjRepository.callAfterUploadFullSyncI = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, parentId = inputData.parentId;
        attach_1.default.findAll({
            "parentId": parentId,
            "isAttachedToNote": false
        }, { workspaceId: workspaceId }, function (err, attachmentObjs) { return __awaiter(_this, void 0, void 0, function () {
            var i, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        for (i in attachmentObjs) {
                            if (attachmentObjs.hasOwnProperty(i)) {
                                if (attachmentObjs[i].tempName || attachmentObjs[i].location) {
                                    attachmentObjs[i].isAttachedToNote = true;
                                }
                            }
                        }
                        return [4, AttachmentObjRepository.update({ workspaceId: workspaceId, attachmentObjs: attachmentObjs })];
                    case 1:
                        result = _a.sent();
                        callback(err, result);
                        return [2];
                }
            });
        }); });
    };
    AttachmentObjRepository.callAfterUploadRenamedAttachmentsI = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, parentId = inputData.parentId;
        attach_1.default.findAll({
            "parentId": parentId,
            "oldDisplayName": { "$ne": null }
        }, { workspaceId: workspaceId }, function (err, attachmentObjs) { return __awaiter(_this, void 0, void 0, function () {
            var i, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        for (i in attachmentObjs) {
                            if (attachmentObjs.hasOwnProperty(i)) {
                                attachmentObjs[i].oldDisplayName = null;
                            }
                        }
                        return [4, AttachmentObjRepository.update({ workspaceId: workspaceId, attachmentObjs: attachmentObjs })];
                    case 1:
                        result = _a.sent();
                        callback(err, result);
                        return [2];
                }
            });
        }); });
    };
    AttachmentObjRepository.getNoteAttachmentsForUploadSizeInBytes = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
        attach_1.default.findAll({
            "noteGlobalId": noteGlobalId,
            "location": "",
            "erised": false
        }, { workspaceId: workspaceId, secure: true }, function (err, attachmentObjs) {
            var size = 0;
            for (var _i = 0, attachmentObjs_2 = attachmentObjs; _i < attachmentObjs_2.length; _i++) {
                var attachmentObj = attachmentObjs_2[_i];
                if (attachmentObj.size) {
                    size += attachmentObj.size;
                }
            }
            callback(err, size);
        });
    };
    AttachmentObjRepository.clearRemovedData = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        attach_1.default.findAll({
            "erised": true
        }, { workspaceId: workspaceId }, function (err, attachObjs) {
            for (var _i = 0, attachObjs_3 = attachObjs; _i < attachObjs_3.length; _i++) {
                var attachmentObj = attachObjs_3[_i];
                AttachmentObjRepository.deleteAttachmentAndAllDataFromDevice({
                    workspaceId: workspaceId,
                    globalId: attachmentObj.globalId
                }, function (err, response) {
                });
            }
            callback(null, true);
        });
    };
    AttachmentObjRepository.updateAttachmentDownloadCounter = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        AttachmentObjRepository.get(inputData, function (err, attachItem) {
            if (err || !attachItem) {
                return callback(err, false);
            }
            if (!attachItem.tryDownloadCounter) {
                attachItem.tryDownloadCounter = 0;
            }
            attachItem.tryDownloadCounter += 1;
            attachItem.isDownloaded = false;
            attach_1.default.update({
                globalId: attachItem.globalId,
                noteGlobalId: attachItem.noteGlobalId
            }, attachItem, { workspaceId: workspaceId }, function (err, count) {
                return callback(err, !!count);
            });
        });
    };
    AttachmentObjRepository.updateNeedSync = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, noteGlobalId = inputData.noteGlobalId, displayName = inputData.displayName;
                        if (!globalId || !noteGlobalId) {
                            return resolve(0);
                        }
                        attach_1.default.update({
                            globalId: globalId,
                            noteGlobalId: noteGlobalId
                        }, { needSync: false, displayName: displayName }, { workspaceId: workspaceId }, function (err, count) {
                            if (err || !count) {
                                return resolve(0);
                            }
                            return resolve(count);
                        });
                    })];
            });
        });
    };
    AttachmentObjRepository.updateLocation = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, location = inputData.location;
                        if (!globalId || !location)
                            return resolve(false);
                        attach_1.default.update({ globalId: globalId }, { location: location }, { workspaceId: workspaceId }, function (err, count) {
                            if (err || !count) {
                                return resolve(0);
                            }
                            return resolve(count);
                        });
                    })];
            });
        });
    };
    AttachmentObjRepository.updateLocations = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, savedAttachments, _a, _b, _i, globalId, attachment;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, savedAttachments = inputData.savedAttachments;
                        if (!savedAttachments)
                            return [2];
                        if (!Object.keys(savedAttachments).length)
                            return [2];
                        _a = [];
                        for (_b in savedAttachments)
                            _a.push(_b);
                        _i = 0;
                        _c.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3, 4];
                        globalId = _a[_i];
                        if (!savedAttachments.hasOwnProperty(globalId))
                            return [3, 3];
                        attachment = savedAttachments[globalId];
                        if (!globalId || !attachment)
                            return [3, 3];
                        if (!attachment.location)
                            return [3, 3];
                        return [4, AttachmentObjRepository.updateLocation({
                                workspaceId: workspaceId,
                                globalId: globalId,
                                location: attachment.location,
                            })];
                    case 2:
                        _c.sent();
                        _c.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4: return [2];
                }
            });
        });
    };
    AttachmentObjRepository.getInListCount = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
                        if (!noteGlobalId) {
                            return resolve(0);
                        }
                        attach_1.default.count({
                            noteGlobalId: noteGlobalId,
                            inList: true
                        }, { workspaceId: workspaceId }, function (err, itemAttachCount) {
                            if (err || !itemAttachCount) {
                                return resolve(0);
                            }
                            return resolve(itemAttachCount);
                        });
                    })];
            });
        });
    };
    AttachmentObjRepository.cleanUploadItem = function (attachment) {
        if (!attachment) {
            return attachment;
        }
        var cleanProps = [
            'location',
            'size',
            'mime',
            'file_uuid',
            'extension',
        ];
        for (var prop in attachment) {
            if (!attachment.hasOwnProperty(prop)) {
                continue;
            }
            if (prop && prop.indexOf('UpdateTime') > 0) {
                delete attachment[prop];
                continue;
            }
            if (cleanProps.indexOf(prop) >= 0) {
                delete attachment[prop];
            }
        }
        return __assign({}, attachment);
    };
    AttachmentObjRepository.DOWNLOAD_MAX_TRY = 5;
    return AttachmentObjRepository;
}());
exports.default = AttachmentObjRepository;
