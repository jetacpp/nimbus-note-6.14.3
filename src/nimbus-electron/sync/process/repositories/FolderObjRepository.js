"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../config"));
var NimbusSDK_1 = __importDefault(require("../../nimbussdk/net/NimbusSDK"));
var FolderObj_1 = __importDefault(require("../db/FolderObj"));
var SyncFolderEntity_1 = __importDefault(require("../../nimbussdk/net/response/entities/SyncFolderEntity"));
var NoteObjRepository_1 = __importDefault(require("./NoteObjRepository"));
var item_1 = __importDefault(require("../../../db/models/item"));
var socketFunctions_1 = __importDefault(require("../../socket/socketFunctions"));
var socketStoreType_1 = __importDefault(require("../../socket/socketStoreType"));
var FolderObjRepository = (function () {
    function FolderObjRepository() {
    }
    FolderObjRepository.get = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
        item_1.default.find({ "globalId": globalId, "type": "folder" }, { workspaceId: workspaceId }, function (err, folderObj) {
            if (FolderObjRepository.isValid(folderObj)) {
                callback(err, __assign({}, folderObj));
            }
            else {
                callback(err, null);
            }
        });
    };
    FolderObjRepository.isValid = function (folderObj) {
        return folderObj && FolderObj_1.default.isValid(folderObj);
    };
    FolderObjRepository.getUserFoldersR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        FolderObjRepository.getAllFoldersR(inputData, function (err, list) {
            callback(err, list);
        });
    };
    FolderObjRepository.getAllFoldersR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({ "type": "folder" }, { workspaceId: workspaceId }, function (err, list) {
            return callback(err, list);
        });
    };
    FolderObjRepository.create = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, folderName = inputData.folderName, parentId = inputData.parentId, globalId = inputData.globalId;
        if (globalId == null) {
            globalId = FolderObjRepository.generateGlobalId();
        }
        NimbusSDK_1.default.getAccountManager().getUniqueUserName(function (err, uniqueUserName) {
            FolderObjRepository.checkIfFolderInTrash({ workspaceId: workspaceId, globalId: parentId }, function (err, folderInTrash) {
                var folder = new FolderObj_1.default();
                folder.needSync = true;
                folder.globalId = globalId;
                folder.parentId = parentId;
                folder.rootParentId = null;
                folder.index = 0;
                folder.type = "folder";
                folder.existOnServer = false;
                folder.uniqueUserName = uniqueUserName;
                folder = FolderObj_1.default.setTitle(folder, folderName);
                folder.dateAdded = new Date().getTime();
                folder.dateUpdated = folder.dateAdded;
                folder.syncDate = 0;
                folder.onlyOffline = false;
                folder.isMaybeInTrash = folderInTrash;
                folder.color = "";
                callback(err, folder);
            });
        });
    };
    FolderObjRepository.generateGlobalId = function () {
        return NoteObjRepository_1.default.generateGlobalId();
    };
    FolderObjRepository.checkIfFolderInTrash = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
        var isInTrash = false;
        if (FolderObj_1.default.TRASH === globalId) {
            isInTrash = true;
            return callback(null, isInTrash);
        }
        var getUserFolderById = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        FolderObjRepository.get(inputData, function (err, folderObj) {
                            if (err) {
                                return resolve(null);
                            }
                            resolve(folderObj);
                        });
                    })];
            });
        }); };
        FolderObjRepository.get(inputData, function (err, folderObj) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!folderObj) return [3, 4];
                        if (FolderObj_1.default.TRASH === folderObj.parentId) {
                            isInTrash = true;
                            return [2, callback(err, isInTrash)];
                        }
                        _a.label = 1;
                    case 1:
                        if (!true) return [3, 3];
                        if (folderObj === null) {
                            return [3, 3];
                        }
                        if (FolderObj_1.default.TRASH === folderObj.parentId) {
                            isInTrash = true;
                            return [3, 3];
                        }
                        if (FolderObj_1.default.GOD === folderObj.parentId || FolderObj_1.default.ROOT === folderObj.parentId) {
                            return [3, 3];
                        }
                        return [4, getUserFolderById({ workspaceId: workspaceId, globalId: folderObj.parentId })];
                    case 2:
                        folderObj = _a.sent();
                        return [3, 1];
                    case 3:
                        callback(err, isInTrash);
                        return [3, 5];
                    case 4:
                        callback(err, false);
                        _a.label = 5;
                    case 5: return [2];
                }
            });
        }); });
    };
    FolderObjRepository.checkIfFolderExist = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        FolderObjRepository.get(inputData, function (err, folderObj) {
            callback(null, !!folderObj);
        });
    };
    FolderObjRepository.isAvailableNotesForSync = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NoteObjRepository_1.default.isAvailableNotesForSync(inputData, callback);
    };
    FolderObjRepository.getErasedFromTrashFoldersForUploadOnServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({
            "type": "folder",
            "erised": true,
            "offlineOnly": { "$ne": true },
            "needSync": true,
        }, { workspaceId: workspaceId }, function (err, folderObjs) {
            var foldersIdList = [];
            for (var _i = 0, folderObjs_1 = folderObjs; _i < folderObjs_1.length; _i++) {
                var folder = folderObjs_1[_i];
                foldersIdList.push(folder.globalId);
            }
            callback(err, foldersIdList);
        });
    };
    FolderObjRepository.getUpdatedFoldersForUploadOnServer = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var convertToSyncUserFolderEntity = function (folderObj) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        FolderObjRepository.convertToSyncFolderEntity(folderObj, function (err, folder) {
                            if (err) {
                                return resolve(null);
                            }
                            resolve(folder);
                        });
                    })];
            });
        }); };
        item_1.default.findAll({
            "type": "folder",
            "parentId": { "$nin": [FolderObj_1.default.ERASED_FROM_TRASH, FolderObj_1.default.GOD] },
            "offlineOnly": { "$ne": true },
            "needSync": true,
        }, {
            workspaceId: workspaceId,
            "order": { "dateAdded": "asc" }
        }, function (err, folderObjs) { return __awaiter(_this, void 0, void 0, function () {
            var foldersConvertedList, _i, folderObjs_2, folderObj, folder;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        foldersConvertedList = [];
                        _i = 0, folderObjs_2 = folderObjs;
                        _a.label = 1;
                    case 1:
                        if (!(_i < folderObjs_2.length)) return [3, 4];
                        folderObj = folderObjs_2[_i];
                        return [4, convertToSyncUserFolderEntity(folderObj)];
                    case 2:
                        folder = _a.sent();
                        if (folder) {
                            foldersConvertedList.push(folder);
                        }
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        callback(err, foldersConvertedList);
                        return [2];
                }
            });
        }); });
    };
    FolderObjRepository.convertToSyncFolderEntity = function (folderObj, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        FolderObj_1.default.getTitle(folderObj, function (err, title) {
            var folder = new SyncFolderEntity_1.default();
            folder.global_id = folderObj.globalId;
            folder.parent_id = folderObj.parentId;
            folder.root_parent_id = folderObj.rootParentId;
            folder.index = folderObj.index;
            folder.date_added_user = folderObj.dateAdded;
            folder.date_updated_user = folderObj.dateUpdated;
            folder.type = folderObj.type;
            folder.title = title;
            folder.shared = folderObj.shared ? 1 : 0;
            folder.color = folderObj.color;
            callback(err, folder);
        });
    };
    FolderObjRepository.updateFoldersDownloadedFromServerI = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var folderObjs, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        folderObjs = inputData.folderObjs;
                        if (!folderObjs.length) {
                            return [2, callback(null, false)];
                        }
                        return [4, FolderObjRepository.update(inputData)];
                    case 1:
                        result = _a.sent();
                        callback(null, result);
                        return [2];
                }
            });
        });
    };
    FolderObjRepository.update = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, folderObjs, processUserFolder, result, _i, folderObjs_3, folderObj;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, folderObjs = inputData.folderObjs;
                                    if (!folderObjs) {
                                        return [2, resolve(false)];
                                    }
                                    processUserFolder = function (folderObj) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var existItemInstance, saveInstance, updateNestedItems, updateUserFolder, createdUserFolder;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                if (folderObj.type !== "folder") {
                                                                    return [2, resolve(false)];
                                                                }
                                                                return [4, FolderObjRepository.getUserFolder({ workspaceId: workspaceId, globalId: folderObj.globalId })];
                                                            case 1:
                                                                existItemInstance = _a.sent();
                                                                return [4, FolderObjRepository.convertSyncInstanceToLocal(folderObj, existItemInstance)];
                                                            case 2:
                                                                saveInstance = _a.sent();
                                                                if (!saveInstance) {
                                                                    return [2, resolve(false)];
                                                                }
                                                                saveInstance = FolderObjRepository.updateTrashProperties(folderObj, saveInstance);
                                                                if (!existItemInstance) return [3, 4];
                                                                updateNestedItems = (existItemInstance.rootId !== saveInstance.rootId);
                                                                return [4, FolderObjRepository.updateUserFolder({
                                                                        workspaceId: workspaceId,
                                                                        item: saveInstance,
                                                                        updateNestedItems: updateNestedItems
                                                                    })];
                                                            case 3:
                                                                updateUserFolder = _a.sent();
                                                                resolve(updateUserFolder);
                                                                return [3, 6];
                                                            case 4: return [4, FolderObjRepository.createUserFolder({ workspaceId: workspaceId, item: saveInstance })];
                                                            case 5:
                                                                createdUserFolder = _a.sent();
                                                                resolve(createdUserFolder);
                                                                _a.label = 6;
                                                            case 6: return [2];
                                                        }
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    result = null;
                                    if (!(folderObjs instanceof Array)) return [3, 5];
                                    _i = 0, folderObjs_3 = folderObjs;
                                    _a.label = 1;
                                case 1:
                                    if (!(_i < folderObjs_3.length)) return [3, 4];
                                    folderObj = folderObjs_3[_i];
                                    return [4, processUserFolder(folderObj)];
                                case 2:
                                    result = _a.sent();
                                    _a.label = 3;
                                case 3:
                                    _i++;
                                    return [3, 1];
                                case 4: return [3, 7];
                                case 5: return [4, processUserFolder(folderObjs)];
                                case 6:
                                    result = _a.sent();
                                    _a.label = 7;
                                case 7:
                                    resolve(result);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    FolderObjRepository.getUserFolder = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                        item_1.default.find({ "globalId": globalId, "type": "folder" }, { workspaceId: workspaceId }, function (err, folderObj) {
                            if (err || !folderObj) {
                                return resolve(null);
                            }
                            resolve(folderObj);
                        });
                    })];
            });
        });
    };
    FolderObjRepository.updateUserFolder = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, item = inputData.item, updateNestedItems = inputData.updateNestedItems;
                        updateNestedItems = updateNestedItems || false;
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                        }
                        item_1.default.update({ "globalId": item.globalId }, item, { workspaceId: workspaceId }, function (err, count) { return __awaiter(_this, void 0, void 0, function () {
                            var updateUserNestedItems;
                            var _this = this;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (err || !count) {
                                            return [2, resolve(false)];
                                        }
                                        if (!updateNestedItems) return [3, 2];
                                        updateUserNestedItems = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                            var _this = this;
                                            return __generator(this, function (_a) {
                                                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                        var workspaceId, item, getItemsByParentId, updateItemByQuery, nestedItems, getAllNestedItems, _a, _b, _i, t, nestedItem, queryData, updateData;
                                                        var _this = this;
                                                        return __generator(this, function (_c) {
                                                            switch (_c.label) {
                                                                case 0:
                                                                    workspaceId = inputData.workspaceId, item = inputData.item;
                                                                    getItemsByParentId = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                        var _this = this;
                                                                        return __generator(this, function (_a) {
                                                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                                                    var workspaceId, parentId;
                                                                                    return __generator(this, function (_a) {
                                                                                        workspaceId = inputData.workspaceId, parentId = inputData.parentId;
                                                                                        item_1.default.findAll({ 'parentId': parentId }, { workspaceId: workspaceId }, function (err, itemList) {
                                                                                            if (err) {
                                                                                                return resolve([]);
                                                                                            }
                                                                                            resolve(itemList);
                                                                                        });
                                                                                        return [2];
                                                                                    });
                                                                                }); })];
                                                                        });
                                                                    }); };
                                                                    updateItemByQuery = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                        var _this = this;
                                                                        return __generator(this, function (_a) {
                                                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                                                    var workspaceId, queryData, updateData;
                                                                                    return __generator(this, function (_a) {
                                                                                        workspaceId = inputData.workspaceId, queryData = inputData.queryData, updateData = inputData.updateData;
                                                                                        item_1.default.update(queryData, updateData, { workspaceId: workspaceId }, function () {
                                                                                            resolve(true);
                                                                                        });
                                                                                        return [2];
                                                                                    });
                                                                                }); })];
                                                                        });
                                                                    }); };
                                                                    nestedItems = [];
                                                                    getAllNestedItems = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                                                        var workspaceId, itemInstance, nestedItemList, _a, _b, _i, k;
                                                                        return __generator(this, function (_c) {
                                                                            switch (_c.label) {
                                                                                case 0:
                                                                                    workspaceId = inputData.workspaceId, itemInstance = inputData.itemInstance;
                                                                                    if (!itemInstance) return [3, 5];
                                                                                    nestedItems.push(itemInstance);
                                                                                    return [4, getItemsByParentId({ workspaceId: workspaceId, parentId: itemInstance.globalId })];
                                                                                case 1:
                                                                                    nestedItemList = _c.sent();
                                                                                    _a = [];
                                                                                    for (_b in nestedItemList)
                                                                                        _a.push(_b);
                                                                                    _i = 0;
                                                                                    _c.label = 2;
                                                                                case 2:
                                                                                    if (!(_i < _a.length)) return [3, 5];
                                                                                    k = _a[_i];
                                                                                    return [4, getAllNestedItems({ workspaceId: workspaceId, itemInstance: nestedItemList[k] })];
                                                                                case 3:
                                                                                    _c.sent();
                                                                                    _c.label = 4;
                                                                                case 4:
                                                                                    _i++;
                                                                                    return [3, 2];
                                                                                case 5: return [2];
                                                                            }
                                                                        });
                                                                    }); };
                                                                    return [4, getAllNestedItems({ workspaceId: workspaceId, itemInstance: item })];
                                                                case 1:
                                                                    _c.sent();
                                                                    _a = [];
                                                                    for (_b in nestedItems)
                                                                        _a.push(_b);
                                                                    _i = 0;
                                                                    _c.label = 2;
                                                                case 2:
                                                                    if (!(_i < _a.length)) return [3, 5];
                                                                    t = _a[_i];
                                                                    nestedItem = nestedItems[t];
                                                                    if (nestedItem.globalId === "default") {
                                                                        return [3, 4];
                                                                    }
                                                                    queryData = { globalId: nestedItem.globalId };
                                                                    updateData = { rootId: item.rootId };
                                                                    return [4, updateItemByQuery({ workspaceId: workspaceId, queryData: queryData, updateData: updateData })];
                                                                case 3:
                                                                    _c.sent();
                                                                    _c.label = 4;
                                                                case 4:
                                                                    _i++;
                                                                    return [3, 2];
                                                                case 5:
                                                                    resolve(true);
                                                                    return [2];
                                                            }
                                                        });
                                                    }); })];
                                            });
                                        }); };
                                        return [4, updateUserNestedItems({ workspaceId: workspaceId, item: item })];
                                    case 1:
                                        _a.sent();
                                        _a.label = 2;
                                    case 2:
                                        FolderObjRepository.sendSocketEventOnUpdate({
                                            workspaceId: workspaceId,
                                            globalId: item.globalId,
                                            parentId: item.parentId,
                                            isCreate: false
                                        });
                                        resolve(!!count);
                                        return [2];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    FolderObjRepository.createUserFolder = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, item, getFolderByQuery, queryData, parentFolder;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, item = inputData.item;
                                    if (config_1.default.SHOW_WEB_CONSOLE) {
                                    }
                                    getFolderByQuery = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var workspaceId, queryData;
                                                    return __generator(this, function (_a) {
                                                        workspaceId = inputData.workspaceId, queryData = inputData.queryData;
                                                        item_1.default.find(queryData, { workspaceId: workspaceId }, function (err, item) {
                                                            if (err) {
                                                                return resolve(null);
                                                            }
                                                            resolve(item);
                                                        });
                                                        return [2];
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    queryData = {
                                        globalId: item.parentId,
                                        type: 'folder'
                                    };
                                    return [4, getFolderByQuery({ workspaceId: workspaceId, queryData: queryData })];
                                case 1:
                                    parentFolder = _a.sent();
                                    if (parentFolder) {
                                        item.rootId = parentFolder.rootId;
                                    }
                                    item_1.default.add(item, { workspaceId: workspaceId }, function (err, itemInstance) {
                                        if (err || !itemInstance) {
                                            return resolve(false);
                                        }
                                        FolderObjRepository.sendSocketEventOnUpdate({
                                            workspaceId: workspaceId,
                                            globalId: item.globalId,
                                            parentId: item.parentId,
                                            isCreate: true
                                        });
                                        resolve(!!itemInstance);
                                    });
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    FolderObjRepository.updateTrashProperties = function (itemObj, saveInstance) {
        if (itemObj.parentId) {
            saveInstance.parentId = itemObj.parentId === 'root' ? 'root' : itemObj.parentId;
            saveInstance.rootId = itemObj.parentId === 'trash' ? 'trash' : itemObj.rootId;
            if (itemObj.parentId !== "trash") {
                saveInstance["rootParentId"] = itemObj.parentId;
            }
        }
        saveInstance.needSync = false;
        return saveInstance;
    };
    FolderObjRepository.sendSocketEventOnUpdate = function (inputData) {
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, parentId = inputData.parentId, isCreate = inputData.isCreate;
        socketFunctions_1.default.addStoreData({
            workspaceId: workspaceId,
            storeType: socketStoreType_1.default.FOLDERS_FOR_UPDATE,
            data: {
                globalId: globalId,
                parentId: parentId,
                isCreate: isCreate,
                type: 'folder'
            }
        });
    };
    FolderObjRepository.sendSocketEventOnRemove = function (inputData) {
        var workspaceId = inputData.workspaceId, globalIdList = inputData.globalIdList;
        if (globalIdList && globalIdList.length) {
            socketFunctions_1.default.addStoreData({
                workspaceId: workspaceId,
                storeType: socketStoreType_1.default.FOLDERS_FOR_REMOVE,
                data: {
                    globalId: globalIdList
                }
            });
        }
    };
    FolderObjRepository.convertSyncInstanceToLocal = function (folderObj, existItemInstance) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var fillSyncObjProperties, fillModelProperties, itemInstance;
                        return __generator(this, function (_a) {
                            if (!folderObj) {
                                return [2, resolve(null)];
                            }
                            fillSyncObjProperties = function (item, data) {
                                return item_1.default.prepareFolderOnlyProperties(item_1.default.prepareItemDbProperties(item, data), data);
                            };
                            fillModelProperties = function (item, data) {
                                item.role = data.role || "folder";
                                return item_1.default.prepareCommonProperties(item_1.default.prepareItemDbProperties(item, data), data);
                            };
                            itemInstance = null;
                            if (existItemInstance) {
                                itemInstance = fillSyncObjProperties(item_1.default.prepareModelData(existItemInstance), folderObj);
                                itemInstance = item_1.default.changeDateUpdate(itemInstance, folderObj);
                                if (itemInstance.rootId !== folderObj.rootId) {
                                    itemInstance.rootId = folderObj.rootId || 'root';
                                }
                                if (itemInstance.title !== folderObj.title) {
                                    itemInstance.title = folderObj.title;
                                }
                            }
                            else {
                                itemInstance = fillModelProperties(item_1.default.prepareModelData(folderObj), folderObj);
                            }
                            resolve(itemInstance);
                            return [2];
                        });
                    }); })];
            });
        });
    };
    FolderObjRepository.createUserFolderObj = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, folder = inputData.folder;
                        FolderObjRepository.create({
                            workspaceId: workspaceId,
                            folderName: folder.title,
                            parentId: folder.parent_id,
                            globalId: folder.global_id
                        }, function (err, folderObj) {
                            if (err) {
                                return resolve(null);
                            }
                            resolve(folderObj);
                        });
                    })];
            });
        });
    };
    FolderObjRepository.deleteRemovedItemsDownloadedFromServerI = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        FolderObjRepository.deleteFoldersAndAllDataOnDevice(inputData, function (err, result) {
            callback(err, true);
        });
    };
    FolderObjRepository.deleteFoldersAndAllDataOnDevice = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, folderIds, deleteUserFolder, _i, folderIds_1, globalId;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, folderIds = inputData.folderIds;
                        if (!(folderIds && folderIds.length)) return [3, 4];
                        deleteUserFolder = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        FolderObjRepository.deleteFolderAndAllDataFromDevice(inputData, function (err, response) {
                                            if (err) {
                                                return resolve(false);
                                            }
                                            resolve(true);
                                        });
                                    })];
                            });
                        }); };
                        _i = 0, folderIds_1 = folderIds;
                        _a.label = 1;
                    case 1:
                        if (!(_i < folderIds_1.length)) return [3, 4];
                        globalId = folderIds_1[_i];
                        return [4, deleteUserFolder({ workspaceId: workspaceId, globalId: globalId })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        FolderObjRepository.sendSocketEventOnRemove({ workspaceId: workspaceId, globalIdList: folderIds });
                        callback(null, true);
                        return [2];
                }
            });
        });
    };
    FolderObjRepository.deleteFolderAndAllDataFromDevice = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
        item_1.default.find({ "globalId": globalId, "type": "folder" }, { workspaceId: workspaceId }, function (err, folderObj) { return __awaiter(_this, void 0, void 0, function () {
            var removeUserItemAndAllDataFromDevice, getItemsByParentId, nestedItems, getAllNestedItems, _a, _b, _i, t, nestedItem;
            var _this = this;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        if (err || !folderObj) {
                            return [2, callback(err, false)];
                        }
                        removeUserItemAndAllDataFromDevice = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        NoteObjRepository_1.default.deleteFolderNotesAndAllDataFromDevice(inputData, function (err, result) {
                                            if (err) {
                                                return resolve(false);
                                            }
                                            resolve(result);
                                        });
                                    })];
                            });
                        }); };
                        getItemsByParentId = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                        var workspaceId, parentId;
                                        return __generator(this, function (_a) {
                                            workspaceId = inputData.workspaceId, parentId = inputData.parentId;
                                            item_1.default.findAll({ 'parentId': parentId }, { workspaceId: workspaceId }, function (err, itemList) {
                                                if (err) {
                                                    return resolve([]);
                                                }
                                                resolve(itemList);
                                            });
                                            return [2];
                                        });
                                    }); })];
                            });
                        }); };
                        nestedItems = [];
                        getAllNestedItems = function (inputData) {
                            return __awaiter(this, void 0, void 0, function () {
                                var workspaceId, itemInstance, nestedItemList, _a, _b, _i, k;
                                return __generator(this, function (_c) {
                                    switch (_c.label) {
                                        case 0:
                                            workspaceId = inputData.workspaceId, itemInstance = inputData.itemInstance;
                                            if (!itemInstance) return [3, 5];
                                            nestedItems.push(itemInstance);
                                            return [4, getItemsByParentId({ workspaceId: workspaceId, parentId: itemInstance.globalId })];
                                        case 1:
                                            nestedItemList = _c.sent();
                                            _a = [];
                                            for (_b in nestedItemList)
                                                _a.push(_b);
                                            _i = 0;
                                            _c.label = 2;
                                        case 2:
                                            if (!(_i < _a.length)) return [3, 5];
                                            k = _a[_i];
                                            return [4, getAllNestedItems({ workspaceId: workspaceId, itemInstance: nestedItemList[k] })];
                                        case 3:
                                            _c.sent();
                                            _c.label = 4;
                                        case 4:
                                            _i++;
                                            return [3, 2];
                                        case 5: return [2];
                                    }
                                });
                            });
                        };
                        return [4, getAllNestedItems({ workspaceId: workspaceId, itemInstance: folderObj })];
                    case 1:
                        _c.sent();
                        _a = [];
                        for (_b in nestedItems)
                            _a.push(_b);
                        _i = 0;
                        _c.label = 2;
                    case 2:
                        if (!(_i < _a.length)) return [3, 5];
                        t = _a[_i];
                        nestedItem = nestedItems[t];
                        if (nestedItem.globalId === "default") {
                            return [3, 4];
                        }
                        return [4, removeUserItemAndAllDataFromDevice({ workspaceId: workspaceId, globalId: nestedItem.globalId })];
                    case 3:
                        _c.sent();
                        _c.label = 4;
                    case 4:
                        _i++;
                        return [3, 2];
                    case 5:
                        callback(err, true);
                        return [2];
                }
            });
        }); });
    };
    FolderObjRepository.callAfterUploadErasedFromTrashFoldersOnServerI = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId;
            var _this = this;
            return __generator(this, function (_a) {
                workspaceId = inputData.workspaceId;
                item_1.default.findAll({
                    "type": "folder",
                    "erised": true
                }, { workspaceId: workspaceId }, function (err, folderObjs) { return __awaiter(_this, void 0, void 0, function () {
                    var deleteFolderAndAllDataFromDevice, _i, folderObjs_4, folderObj;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (err) {
                                    return [2, callback(err, false)];
                                }
                                deleteFolderAndAllDataFromDevice = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                    return __generator(this, function (_a) {
                                        return [2, new Promise(function (resolve) {
                                                var workspaceId = inputData.workspaceId, folderObj = inputData.folderObj;
                                                FolderObjRepository.deleteFolderAndAllDataFromDevice({
                                                    workspaceId: workspaceId,
                                                    globalId: folderObj.globalId
                                                }, function (err, result) {
                                                    if (err) {
                                                        return resolve(null);
                                                    }
                                                    resolve(result);
                                                });
                                            })];
                                    });
                                }); };
                                _i = 0, folderObjs_4 = folderObjs;
                                _a.label = 1;
                            case 1:
                                if (!(_i < folderObjs_4.length)) return [3, 4];
                                folderObj = folderObjs_4[_i];
                                return [4, deleteFolderAndAllDataFromDevice({ workspaceId: workspaceId, folderObj: folderObj })];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3:
                                _i++;
                                return [3, 1];
                            case 4:
                                callback(null, true);
                                return [2];
                        }
                    });
                }); });
                return [2];
            });
        });
    };
    FolderObjRepository.callAfterUploadExistFoldersOnServerI = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, folders = inputData.folders;
                        if (!folders.length) {
                            return resolve(true);
                        }
                        var globalIdList = folders.map(function (folderSynObj) {
                            return folderSynObj['global_id'];
                        });
                        item_1.default.findAll({
                            "type": "folder",
                            "globalId": { "$in": globalIdList },
                            "parentId": { "$nin": [FolderObj_1.default.ERASED_FROM_TRASH, FolderObj_1.default.GOD] }
                        }, { workspaceId: workspaceId }, function (err, folderObjs) { return __awaiter(_this, void 0, void 0, function () {
                            var i;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        for (i in folderObjs) {
                                            if (folderObjs.hasOwnProperty(i)) {
                                                folderObjs[i].existOnServer = true;
                                                folderObjs[i].needSync = false;
                                            }
                                        }
                                        return [4, FolderObjRepository.update({ workspaceId: workspaceId, folderObjs: folderObjs })];
                                    case 1:
                                        _a.sent();
                                        resolve(true);
                                        return [2];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    FolderObjRepository.getDefaultFolder = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var globalId = FolderObj_1.default.DEFAULT;
        FolderObjRepository.get({ workspaceId: workspaceId, globalId: globalId }, function (err, folderObj) {
            if (folderObj == null) {
                globalId = FolderObj_1.default.DEFAULT;
            }
            callback(err, globalId);
        });
    };
    FolderObjRepository.deleteData = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                        item_1.default.erase({
                            "globalId": globalId,
                            "type": "folder"
                        }, { workspaceId: workspaceId }, function (err, response) {
                            if (err) {
                                return resolve(null);
                            }
                            resolve(response);
                        });
                    })];
            });
        });
    };
    FolderObjRepository.clearRemovedData = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        item_1.default.findAll({
            "erised": true,
            "type": "folder"
        }, { workspaceId: workspaceId }, function (err, folderObjs) { return __awaiter(_this, void 0, void 0, function () {
            var _i, folderObjs_5, folderObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _i = 0, folderObjs_5 = folderObjs;
                        _a.label = 1;
                    case 1:
                        if (!(_i < folderObjs_5.length)) return [3, 4];
                        folderObj = folderObjs_5[_i];
                        return [4, FolderObjRepository.deleteData({ workspaceId: workspaceId, globalId: folderObj.globalId })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        callback(null, true);
                        return [2];
                }
            });
        }); });
    };
    FolderObjRepository.DEFAULT_FOLDER = "default_folder";
    return FolderObjRepository;
}());
exports.default = FolderObjRepository;
