"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../config"));
var orgs_1 = __importDefault(require("../../../db/models/orgs"));
var instance_1 = __importDefault(require("../../../window/instance"));
var OrgObj_1 = __importDefault(require("../db/OrgObj"));
var orgs_2 = __importDefault(require("../../../db/models/orgs"));
var SelectedOrganization_1 = __importDefault(require("../../../organization/SelectedOrganization"));
var auth_1 = __importDefault(require("../../../auth/auth"));
var page_1 = __importDefault(require("../../../request/page"));
var SyncManager_1 = __importDefault(require("../SyncManager"));
var AvatarSingleDonwloader_1 = __importDefault(require("../../downlaoder/AvatarSingleDonwloader"));
var OrgObjRepository = (function () {
    function OrgObjRepository() {
    }
    OrgObjRepository.get = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var id = inputData.id;
        orgs_1.default.find({ id: id }, {}, function (err, orgObj) {
            if (err || !orgObj) {
                return callback(err, null);
            }
            callback(err, __assign({}, orgObj));
        });
    };
    OrgObjRepository.create = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var organization = inputData.organization;
        if (!organization) {
            return callback(null, null);
        }
        var organizationObj = new OrgObj_1.default();
        organizationObj.id = organization.id;
        organizationObj.type = organization.type;
        organizationObj.serviceType = organization.serviceType;
        organizationObj.title = organization.title;
        organizationObj.description = organization.description;
        organizationObj.usage = organization.usage;
        organizationObj.limits = organization.limits;
        organizationObj.features = organization.features;
        organizationObj.user = organization.user;
        organizationObj.sub = organization.sub;
        organizationObj.suspended = organization.suspended;
        organizationObj.suspendedAt = organization.suspendedAt;
        organizationObj.suspendedReason = organization.suspendedReason;
        organizationObj.access = organization.access;
        organizationObj.smallLogoUrl = organization.smallLogoUrl;
        organizationObj.bigLogoUrl = organization.bigLogoUrl;
        organizationObj.syncDate = 0;
        organizationObj.needSync = true;
        return callback(null, organizationObj);
    };
    OrgObjRepository.update = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var orgObjs, processUserOrg, result, _i, orgObjs_1, orgObj;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    orgObjs = inputData.orgObjs;
                                    if (!orgObjs) {
                                        return [2, resolve(false)];
                                    }
                                    processUserOrg = function (orgObj) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var existItemInstance, saveInstance, authInfo, activeOrganization, smallLogoUrl, avatarFileExist, updateOrg, createdOrg;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0: return [4, OrgObjRepository.getUserOrg({ id: orgObj.id })];
                                                            case 1:
                                                                existItemInstance = _a.sent();
                                                                return [4, OrgObjRepository.convertSyncInstanceToLocal(orgObj, existItemInstance)];
                                                            case 2:
                                                                saveInstance = _a.sent();
                                                                if (!saveInstance) {
                                                                    return [2, resolve(false)];
                                                                }
                                                                if (!(existItemInstance && existItemInstance.type === orgs_2.default.TYPE_BUSINESS)) return [3, 5];
                                                                if (!(!existItemInstance.suspended && saveInstance.suspended)) return [3, 5];
                                                                return [4, auth_1.default.fetchActualUserAsync()];
                                                            case 3:
                                                                authInfo = _a.sent();
                                                                return [4, SelectedOrganization_1.default.getCurrentOrganization(authInfo)];
                                                            case 4:
                                                                activeOrganization = _a.sent();
                                                                if (activeOrganization && existItemInstance.id === activeOrganization.globalId) {
                                                                    setTimeout(function () {
                                                                        SyncManager_1.default.stopAllSync();
                                                                        page_1.default.reload(false);
                                                                    }, 3000);
                                                                }
                                                                _a.label = 5;
                                                            case 5:
                                                                if (!(typeof (saveInstance.smallLogoUrl) !== 'undefined')) return [3, 12];
                                                                if (!saveInstance.smallLogoUrl) return [3, 11];
                                                                if (!(existItemInstance && existItemInstance.smallLogoUrl)) return [3, 9];
                                                                if (!(existItemInstance.smallLogoUrl !== saveInstance.smallLogoUrl)) return [3, 6];
                                                                smallLogoUrl = saveInstance.smallLogoUrl;
                                                                return [3, 8];
                                                            case 6: return [4, AvatarSingleDonwloader_1.default.checkAvatarExist(saveInstance.smallLogoUrl)];
                                                            case 7:
                                                                avatarFileExist = _a.sent();
                                                                if (!avatarFileExist) {
                                                                    smallLogoUrl = saveInstance.smallLogoUrl;
                                                                }
                                                                _a.label = 8;
                                                            case 8: return [3, 10];
                                                            case 9:
                                                                smallLogoUrl = saveInstance.smallLogoUrl;
                                                                _a.label = 10;
                                                            case 10: return [3, 12];
                                                            case 11:
                                                                smallLogoUrl = null;
                                                                _a.label = 12;
                                                            case 12:
                                                                if (smallLogoUrl) {
                                                                    AvatarSingleDonwloader_1.default.download({
                                                                        url: smallLogoUrl,
                                                                        oldUrl: existItemInstance && existItemInstance.smallLogoUrl ? existItemInstance.smallLogoUrl : '',
                                                                        type: AvatarSingleDonwloader_1.default.AVATAR_TYPE_ORGANIZATION,
                                                                    });
                                                                }
                                                                saveInstance.needSync = false;
                                                                if (!existItemInstance) return [3, 14];
                                                                return [4, OrgObjRepository.updateUserOrg({ item: saveInstance })];
                                                            case 13:
                                                                updateOrg = _a.sent();
                                                                resolve(updateOrg);
                                                                return [3, 16];
                                                            case 14: return [4, OrgObjRepository.createUserOrg({ item: saveInstance })];
                                                            case 15:
                                                                createdOrg = _a.sent();
                                                                resolve(createdOrg);
                                                                _a.label = 16;
                                                            case 16: return [2];
                                                        }
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    result = null;
                                    if (!(orgObjs instanceof Array)) return [3, 5];
                                    _i = 0, orgObjs_1 = orgObjs;
                                    _a.label = 1;
                                case 1:
                                    if (!(_i < orgObjs_1.length)) return [3, 4];
                                    orgObj = orgObjs_1[_i];
                                    return [4, processUserOrg(orgObj)];
                                case 2:
                                    result = _a.sent();
                                    _a.label = 3;
                                case 3:
                                    _i++;
                                    return [3, 1];
                                case 4: return [3, 7];
                                case 5: return [4, processUserOrg(orgObjs)];
                                case 6:
                                    result = _a.sent();
                                    _a.label = 7;
                                case 7:
                                    resolve(result);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    OrgObjRepository.getUserOrg = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var id = inputData.id;
                        orgs_1.default.find({ id: id }, {}, function (err, orgObj) {
                            if (err || !orgObj) {
                                return resolve(null);
                            }
                            resolve(orgObj);
                        });
                    })];
            });
        });
    };
    OrgObjRepository.updateUserOrg = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var item = inputData.item;
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                        }
                        orgs_1.default.update({ id: item.id }, item, {}, function (err, count) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                if (err || !count) {
                                    return [2, resolve(false)];
                                }
                                OrgObjRepository.sendSocketEventOnUpdate(item.id, false);
                                resolve(!!count);
                                return [2];
                            });
                        }); });
                    })];
            });
        });
    };
    OrgObjRepository.createUserOrg = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var item;
                        return __generator(this, function (_a) {
                            item = inputData.item;
                            if (config_1.default.SHOW_WEB_CONSOLE) {
                            }
                            orgs_1.default.add(item, {}, function (err, itemInstance) {
                                if (err || !itemInstance) {
                                    return resolve(false);
                                }
                                OrgObjRepository.sendSocketEventOnUpdate(item.id, true);
                                resolve(!!itemInstance);
                            });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    OrgObjRepository.removeUserOrg = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var item;
                        var _this = this;
                        return __generator(this, function (_a) {
                            item = inputData.item;
                            if (!item) {
                                return [2, resolve(false)];
                            }
                            orgs_1.default.erase({
                                "id": item.id,
                                "erised": { "$in": [true, false] }
                            }, {}, function (err, count) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    if (err || !count) {
                                        return [2, resolve(false)];
                                    }
                                    OrgObjRepository.sendSocketEventOnRemove([item.id]);
                                    resolve(!!count);
                                    return [2];
                                });
                            }); });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    ;
    OrgObjRepository.sendSocketEventOnUpdate = function (id, isCreate) {
        return __awaiter(this, void 0, void 0, function () {
            var organization, organizations, organizationData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, OrgObjRepository.getUserOrg({ id: id })];
                    case 1:
                        organization = _a.sent();
                        if (!organization) {
                            return [2];
                        }
                        if (organization['type'] !== orgs_1.default.TYPE_BUSINESS) {
                            return [2];
                        }
                        return [4, orgs_1.default.getResponseListJson([organization])];
                    case 2:
                        organizations = _a.sent();
                        organizationData = organizations && organizations.length ? organizations[0] : null;
                        if (!organizationData) {
                            return [2];
                        }
                        if (instance_1.default.get()) {
                            instance_1.default.get().webContents.send('event:client:update:organization:response', {
                                org: organizationData
                            });
                        }
                        return [2];
                }
            });
        });
    };
    OrgObjRepository.sendSocketEventOnRemove = function (idList) {
        return __awaiter(this, void 0, void 0, function () {
            var _i, idList_1, id, organization, organizationData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(idList && idList.length)) return [3, 5];
                        _i = 0, idList_1 = idList;
                        _a.label = 1;
                    case 1:
                        if (!(_i < idList_1.length)) return [3, 5];
                        id = idList_1[_i];
                        return [4, OrgObjRepository.getUserOrg({ id: id })];
                    case 2:
                        organization = _a.sent();
                        return [4, orgs_1.default.getResponseJson(organization)];
                    case 3:
                        organizationData = _a.sent();
                        if (organizationData && organizationData.type !== orgs_1.default.TYPE_BUSINESS) {
                            return [2];
                        }
                        if (instance_1.default.get()) {
                            instance_1.default.get().webContents.send('event:client:remove:organization:response', { id: id });
                        }
                        _a.label = 4;
                    case 4:
                        _i++;
                        return [3, 1];
                    case 5: return [2];
                }
            });
        });
    };
    OrgObjRepository.convertSyncInstanceToLocal = function (orgObj, existItemInstance) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var fillSyncObjProperties, fillModelProperties, orgInstance;
                        return __generator(this, function (_a) {
                            if (!orgObj) {
                                return [2, resolve(null)];
                            }
                            fillSyncObjProperties = function (item, data) {
                                return orgs_1.default.prepareSyncOnlyProperties(orgs_1.default.prepareItemDbProperties(item, data), data);
                            };
                            fillModelProperties = function (item, data) {
                                return orgs_1.default.prepareCommonProperties(orgs_1.default.prepareItemDbProperties(item, data), data);
                            };
                            orgInstance = null;
                            if (existItemInstance) {
                                orgInstance = fillSyncObjProperties(orgs_1.default.prepareModelData(existItemInstance), orgObj);
                            }
                            else {
                                orgInstance = fillModelProperties(orgs_1.default.prepareModelData(orgObj), orgObj);
                            }
                            resolve(orgInstance);
                            return [2];
                        });
                    }); })];
            });
        });
    };
    OrgObjRepository.remove = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var organizationObjs, result, _i, organizationObjs_1, organizationObj;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    organizationObjs = inputData.organizationObjs;
                                    if (!organizationObjs) {
                                        return [2, resolve(false)];
                                    }
                                    result = false;
                                    if (!(organizationObjs instanceof Array)) return [3, 5];
                                    _i = 0, organizationObjs_1 = organizationObjs;
                                    _a.label = 1;
                                case 1:
                                    if (!(_i < organizationObjs_1.length)) return [3, 4];
                                    organizationObj = organizationObjs_1[_i];
                                    return [4, OrgObjRepository.removeUserOrg({ item: organizationObj })];
                                case 2:
                                    result = (_a.sent());
                                    _a.label = 3;
                                case 3:
                                    _i++;
                                    return [3, 1];
                                case 4: return [3, 7];
                                case 5: return [4, OrgObjRepository.removeUserOrg({ item: organizationObjs })];
                                case 6:
                                    result = (_a.sent());
                                    _a.label = 7;
                                case 7:
                                    resolve(result);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    return OrgObjRepository;
}());
exports.default = OrgObjRepository;
