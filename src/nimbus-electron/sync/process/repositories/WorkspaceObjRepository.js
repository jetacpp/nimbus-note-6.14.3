"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_extra_1 = __importDefault(require("fs-extra"));
var config_1 = __importDefault(require("../../../../config"));
var WorkspaceObj_1 = __importDefault(require("../db/WorkspaceObj"));
var workspace_1 = __importDefault(require("../../../db/models/workspace"));
var SyncWorkspaceEntity_1 = __importDefault(require("../../nimbussdk/net/response/entities/SyncWorkspaceEntity"));
var OrgObjRepository_1 = __importDefault(require("./OrgObjRepository"));
var instance_1 = __importDefault(require("../../../window/instance"));
var item_1 = __importDefault(require("../../../db/models/item"));
var attach_1 = __importDefault(require("../../../db/models/attach"));
var pdb_1 = __importDefault(require("../../../../pdb"));
var SyncManager_1 = __importDefault(require("../../../sync/process/SyncManager"));
var AccountManager_1 = __importDefault(require("../../../sync/nimbussdk/manager/AccountManager"));
var AvatarSingleDonwloader_1 = __importDefault(require("../../downlaoder/AvatarSingleDonwloader"));
var clearHandler_1 = require("../../../utilities/clearHandler");
var WorkspaceObjRepository = (function () {
    function WorkspaceObjRepository() {
    }
    WorkspaceObjRepository.get = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var globalId = inputData.globalId;
        if (globalId === null) {
            globalId = "";
        }
        workspace_1.default.find({ globalId: globalId }, {}, function (err, workspaceObj) {
            if (err || !workspaceObj) {
                return callback(err, null);
            }
            callback(err, __assign({}, workspaceObj));
        });
    };
    ;
    WorkspaceObjRepository.create = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspace = inputData.workspace;
        if (!workspace) {
            return callback(null, null);
        }
        var workspaceObj = new WorkspaceObj_1.default();
        workspaceObj.userId = workspace.userId;
        workspaceObj.globalId = workspace.globalId;
        workspaceObj.title = workspace.title;
        workspaceObj.isDefault = workspace.isDefault;
        workspaceObj.createdAt = workspace.createdAt;
        workspaceObj.updatedAt = workspace.updatedAt;
        workspaceObj.countMembers = workspace.countMembers;
        workspaceObj.countInvites = 0;
        workspaceObj.access = null;
        workspaceObj.defaultEncryptionKeyId = null;
        workspaceObj.syncDate = 0;
        workspaceObj.needSync = true;
        workspaceObj.isNotesLimited = workspace.isNotesLimited;
        workspaceObj.user = workspace.user;
        workspaceObj.notesEmail = workspace.notesEmail;
        workspaceObj.avatar = workspace.avatar || null;
        workspaceObj.color = workspace.color || null;
        return callback(null, workspaceObj);
    };
    WorkspaceObjRepository.update = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceObjs, processUserWorkspace, result, _i, workspaceObjs_1, workspaceObj;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceObjs = inputData.workspaceObjs;
                                    if (!workspaceObjs) {
                                        return [2, resolve(false)];
                                    }
                                    processUserWorkspace = function (workspaceObj) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var org, existItemInstance, saveInstance, saveAvatarData, avatarFileExist, updateUserWorkspace, createdUserWorkspace;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                org = workspaceObj.org;
                                                                workspaceObj.orgId = org ? org.id : null;
                                                                return [4, WorkspaceObjRepository.getUserWorkspace({ globalId: workspaceObj.globalId })];
                                                            case 1:
                                                                existItemInstance = _a.sent();
                                                                return [4, WorkspaceObjRepository.convertSyncInstanceToLocal(workspaceObj, existItemInstance)];
                                                            case 2:
                                                                saveInstance = _a.sent();
                                                                if (!saveInstance) {
                                                                    return [2, resolve(false)];
                                                                }
                                                                saveAvatarData = {};
                                                                if (!(typeof (saveInstance.avatar) !== 'undefined')) return [3, 9];
                                                                if (!saveInstance.avatar) return [3, 8];
                                                                if (!(existItemInstance && existItemInstance.avatar)) return [3, 6];
                                                                if (!(existItemInstance.avatar.url !== saveInstance.avatar.url)) return [3, 3];
                                                                saveAvatarData.avatar = saveInstance.avatar;
                                                                return [3, 5];
                                                            case 3: return [4, AvatarSingleDonwloader_1.default.checkAvatarExist(saveInstance.avatar.url)];
                                                            case 4:
                                                                avatarFileExist = _a.sent();
                                                                if (!avatarFileExist) {
                                                                    saveAvatarData.avatar = saveInstance.avatar;
                                                                }
                                                                _a.label = 5;
                                                            case 5: return [3, 7];
                                                            case 6:
                                                                saveAvatarData.avatar = saveInstance.avatar;
                                                                _a.label = 7;
                                                            case 7: return [3, 9];
                                                            case 8:
                                                                saveAvatarData.avatar = null;
                                                                _a.label = 9;
                                                            case 9:
                                                                if (Object.keys(saveAvatarData).length) {
                                                                    if (typeof (saveAvatarData.avatar) !== 'undefined') {
                                                                        if (saveAvatarData.avatar) {
                                                                            AvatarSingleDonwloader_1.default.download({
                                                                                url: saveAvatarData.avatar.url,
                                                                                oldUrl: existItemInstance && existItemInstance.avatar ? existItemInstance.avatar.url : '',
                                                                                type: AvatarSingleDonwloader_1.default.AVATAR_TYPE_WORKSPACE,
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                                saveInstance.needSync = false;
                                                                if (!existItemInstance) return [3, 11];
                                                                return [4, WorkspaceObjRepository.updateUserWorkspace({ item: saveInstance })];
                                                            case 10:
                                                                updateUserWorkspace = _a.sent();
                                                                resolve(updateUserWorkspace);
                                                                return [3, 13];
                                                            case 11: return [4, WorkspaceObjRepository.createUserWorkspace({ item: saveInstance })];
                                                            case 12:
                                                                createdUserWorkspace = _a.sent();
                                                                resolve(createdUserWorkspace);
                                                                _a.label = 13;
                                                            case 13: return [2];
                                                        }
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    result = null;
                                    if (!(workspaceObjs instanceof Array)) return [3, 5];
                                    _i = 0, workspaceObjs_1 = workspaceObjs;
                                    _a.label = 1;
                                case 1:
                                    if (!(_i < workspaceObjs_1.length)) return [3, 4];
                                    workspaceObj = workspaceObjs_1[_i];
                                    return [4, processUserWorkspace(workspaceObj)];
                                case 2:
                                    result = _a.sent();
                                    _a.label = 3;
                                case 3:
                                    _i++;
                                    return [3, 1];
                                case 4: return [3, 7];
                                case 5: return [4, processUserWorkspace(workspaceObjs)];
                                case 6:
                                    result = _a.sent();
                                    _a.label = 7;
                                case 7:
                                    resolve(result);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    WorkspaceObjRepository.remove = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceObjs, result, _i, workspaceObjs_2, workspaceObj;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceObjs = inputData.workspaceObjs;
                                    if (!workspaceObjs) {
                                        return [2, resolve(false)];
                                    }
                                    result = false;
                                    if (!(workspaceObjs instanceof Array)) return [3, 5];
                                    _i = 0, workspaceObjs_2 = workspaceObjs;
                                    _a.label = 1;
                                case 1:
                                    if (!(_i < workspaceObjs_2.length)) return [3, 4];
                                    workspaceObj = workspaceObjs_2[_i];
                                    return [4, WorkspaceObjRepository.removeUserWorkspace({ item: workspaceObj })];
                                case 2:
                                    result = (_a.sent());
                                    _a.label = 3;
                                case 3:
                                    _i++;
                                    return [3, 1];
                                case 4: return [3, 7];
                                case 5: return [4, WorkspaceObjRepository.removeUserWorkspace({ item: workspaceObjs })];
                                case 6:
                                    result = (_a.sent());
                                    _a.label = 7;
                                case 7:
                                    resolve(result);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    WorkspaceObjRepository.getUserWorkspace = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var globalId = inputData.globalId;
                        workspace_1.default.find({ "globalId": globalId }, {}, function (err, workspaceObj) {
                            if (err || !workspaceObj) {
                                return resolve(null);
                            }
                            resolve(workspaceObj);
                        });
                    })];
            });
        });
    };
    WorkspaceObjRepository.removeUserWorkspace = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var item;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    item = inputData.item;
                                    if (!item) {
                                        return [2, resolve(false)];
                                    }
                                    return [4, WorkspaceObjRepository.removeWorkspaceData(inputData)];
                                case 1:
                                    _a.sent();
                                    workspace_1.default.erase({
                                        "globalId": item.globalId,
                                        "erised": { "$in": [true, false] }
                                    }, {}, function (err, count) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            if (err || !count) {
                                                return [2, resolve(false)];
                                            }
                                            WorkspaceObjRepository.sendSocketEventOnRemove([item.globalId]);
                                            resolve(!!count);
                                            return [2];
                                        });
                                    }); });
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    ;
    WorkspaceObjRepository.removeWorkspaceData = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var item, workspaceId, query;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    item = inputData.item;
                                    if (!item) {
                                        return [2, resolve(false)];
                                    }
                                    workspaceId = item.globalId;
                                    return [4, SyncManager_1.default.stopSync(workspaceId)];
                                case 1:
                                    _a.sent();
                                    query = {
                                        "type": "note",
                                        "erised": { "$in": [true, false] }
                                    };
                                    item_1.default.findAll(query, { workspaceId: workspaceId }, function (err, noteObjs) { return __awaiter(_this, void 0, void 0, function () {
                                        var removeUserWorkspaceAttachment, removeUserWorkspaceAttachments, _i, noteObjs_1, noteObj, dbDirPath, _a, workspaceDirPath, exists;
                                        var _this = this;
                                        return __generator(this, function (_b) {
                                            switch (_b.label) {
                                                case 0:
                                                    if (err || !noteObjs) {
                                                        return [2, resolve(true)];
                                                    }
                                                    removeUserWorkspaceAttachment = function (inputData) {
                                                        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                            var workspaceId, attachItem, removeAttachQuery;
                                                            var _this = this;
                                                            return __generator(this, function (_a) {
                                                                workspaceId = inputData.workspaceId, attachItem = inputData.attachItem;
                                                                removeAttachQuery = { globalId: attachItem.globalId };
                                                                attach_1.default.remove(removeAttachQuery, { workspaceId: workspaceId }, function () { return __awaiter(_this, void 0, void 0, function () {
                                                                    var targetPath, exists;
                                                                    return __generator(this, function (_a) {
                                                                        switch (_a.label) {
                                                                            case 0:
                                                                                if (!pdb_1.default.getClientAttachmentPath()) return [3, 2];
                                                                                targetPath = pdb_1.default.getClientAttachmentPath() + "/" + attachItem.storedFileUUID;
                                                                                return [4, fs_extra_1.default.exists(targetPath)];
                                                                            case 1:
                                                                                exists = _a.sent();
                                                                                if (exists) {
                                                                                    try {
                                                                                        fs_extra_1.default.unlink(targetPath, function () {
                                                                                        });
                                                                                    }
                                                                                    catch (e) {
                                                                                        if (config_1.default.SHOW_WEB_CONSOLE) {
                                                                                            console.log("Error => removeUserWorkspaceAttachment: ", targetPath);
                                                                                        }
                                                                                    }
                                                                                }
                                                                                _a.label = 2;
                                                                            case 2: return [2, resolve(true)];
                                                                        }
                                                                    });
                                                                }); });
                                                                return [2];
                                                            });
                                                        }); });
                                                    };
                                                    removeUserWorkspaceAttachments = function (inputData) {
                                                        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                            var workspaceId, noteObj;
                                                            var _this = this;
                                                            return __generator(this, function (_a) {
                                                                workspaceId = inputData.workspaceId, noteObj = inputData.noteObj;
                                                                attach_1.default.findAll({ noteGlobalId: noteObj.globalId }, { workspaceId: workspaceId }, function (err, attachList) { return __awaiter(_this, void 0, void 0, function () {
                                                                    var _i, attachList_1, attachItem;
                                                                    return __generator(this, function (_a) {
                                                                        switch (_a.label) {
                                                                            case 0:
                                                                                if (!(attachList && attachList.length)) return [3, 4];
                                                                                _i = 0, attachList_1 = attachList;
                                                                                _a.label = 1;
                                                                            case 1:
                                                                                if (!(_i < attachList_1.length)) return [3, 4];
                                                                                attachItem = attachList_1[_i];
                                                                                return [4, removeUserWorkspaceAttachment({ workspaceId: workspaceId, attachItem: attachItem })];
                                                                            case 2:
                                                                                _a.sent();
                                                                                _a.label = 3;
                                                                            case 3:
                                                                                _i++;
                                                                                return [3, 1];
                                                                            case 4: return [2, resolve(true)];
                                                                        }
                                                                    });
                                                                }); });
                                                                return [2];
                                                            });
                                                        }); });
                                                    };
                                                    _i = 0, noteObjs_1 = noteObjs;
                                                    _b.label = 1;
                                                case 1:
                                                    if (!(_i < noteObjs_1.length)) return [3, 4];
                                                    noteObj = noteObjs_1[_i];
                                                    return [4, removeUserWorkspaceAttachments({ workspaceId: workspaceId, noteObj: noteObj })];
                                                case 2:
                                                    _b.sent();
                                                    _b.label = 3;
                                                case 3:
                                                    _i++;
                                                    return [3, 1];
                                                case 4:
                                                    dbDirPath = pdb_1.default.getClientDbPath();
                                                    _a = dbDirPath;
                                                    if (!_a) return [3, 6];
                                                    return [4, fs_extra_1.default.exists(dbDirPath)];
                                                case 5:
                                                    _a = (_b.sent());
                                                    _b.label = 6;
                                                case 6:
                                                    if (!_a) return [3, 9];
                                                    workspaceDirPath = dbDirPath + "/" + workspaceId;
                                                    return [4, clearHandler_1.clearDirectory(workspaceDirPath)];
                                                case 7:
                                                    _b.sent();
                                                    return [4, fs_extra_1.default.exists(workspaceDirPath)];
                                                case 8:
                                                    exists = _b.sent();
                                                    if (exists) {
                                                        try {
                                                            fs_extra_1.default.rmdir(workspaceDirPath, function () {
                                                            });
                                                        }
                                                        catch (e) {
                                                            if (config_1.default.SHOW_WEB_CONSOLE) {
                                                                console.log("Error => removeUserWorkspaceAttachments: ", workspaceDirPath, e);
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        if (config_1.default.SHOW_WEB_CONSOLE) {
                                                            console.log("Error => removeWorkspaceData => empty workspaceDirPath: ", workspaceDirPath);
                                                        }
                                                    }
                                                    return [3, 10];
                                                case 9:
                                                    if (config_1.default.SHOW_WEB_CONSOLE) {
                                                        console.log("Error => removeWorkspaceData => empty dbDirPath: ", dbDirPath);
                                                    }
                                                    _b.label = 10;
                                                case 10:
                                                    AccountManager_1.default.setLastUpdateTime({
                                                        workspaceId: workspaceId,
                                                        lastUpdateTime: 1
                                                    });
                                                    return [2, resolve(true)];
                                            }
                                        });
                                    }); });
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    WorkspaceObjRepository.updateUserWorkspace = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var item = inputData.item;
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                        }
                        workspace_1.default.update({ "globalId": item.globalId }, item, {}, function (err, count) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                if (err || !count) {
                                    return [2, resolve(false)];
                                }
                                WorkspaceObjRepository.sendSocketEventOnUpdate(item.globalId, false);
                                resolve(!!count);
                                return [2];
                            });
                        }); });
                    })];
            });
        });
    };
    WorkspaceObjRepository.createUserWorkspace = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var item;
                        return __generator(this, function (_a) {
                            item = inputData.item;
                            if (config_1.default.SHOW_WEB_CONSOLE) {
                            }
                            workspace_1.default.add(item, {}, function (err, itemInstance) {
                                if (err || !itemInstance) {
                                    return resolve(false);
                                }
                                WorkspaceObjRepository.sendSocketEventOnUpdate(item.globalId, true);
                                resolve(!!itemInstance);
                            });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    WorkspaceObjRepository.sendSocketEventOnUpdate = function (globalId, isCreate) {
        return __awaiter(this, void 0, void 0, function () {
            var workspace, workspaceData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, WorkspaceObjRepository.getUserWorkspace({ globalId: globalId })];
                    case 1:
                        workspace = _a.sent();
                        return [4, workspace_1.default.getResponseJson(workspace)];
                    case 2:
                        workspaceData = _a.sent();
                        if (instance_1.default.get()) {
                            instance_1.default.get().webContents.send('event:client:update:workspace:response', {
                                workspace: workspaceData
                            });
                        }
                        return [2];
                }
            });
        });
    };
    WorkspaceObjRepository.sendSocketEventOnRemove = function (globalIdList) {
        if (globalIdList && globalIdList.length) {
            for (var _i = 0, globalIdList_1 = globalIdList; _i < globalIdList_1.length; _i++) {
                var globalId = globalIdList_1[_i];
                if (instance_1.default.get()) {
                    instance_1.default.get().webContents.send('event:client:remove:workspace:response', {
                        workspaceId: globalId
                    });
                }
            }
        }
    };
    WorkspaceObjRepository.sendSocketEventOnUpdateMembers = function (workspaceId) {
        if (instance_1.default.get()) {
            instance_1.default.get().webContents.send('event:client:update:workspace:members:response', { workspaceId: workspaceId });
        }
    };
    WorkspaceObjRepository.sendSocketEventOnUpdateInvites = function (workspaceId) {
        if (instance_1.default.get()) {
            instance_1.default.get().webContents.send('event:client:update:workspace:invites:response', { workspaceId: workspaceId });
        }
    };
    WorkspaceObjRepository.sendSocketEventOnWorkspacesAccessChange = function () {
        var _this = this;
        workspace_1.default.findAll({}, {}, function (err, workspacesList) { return __awaiter(_this, void 0, void 0, function () {
            var defaultWorkspace, workspaces, workspacesAccess;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(err || !workspacesList.length)) return [3, 2];
                        return [4, workspace_1.default.getDefault()];
                    case 1:
                        defaultWorkspace = _a.sent();
                        workspacesList = [defaultWorkspace];
                        _a.label = 2;
                    case 2: return [4, workspace_1.default.getResponseListJson(workspacesList)];
                    case 3:
                        workspaces = _a.sent();
                        workspacesAccess = workspaces
                            .filter(function (workspaceInstance) { return (workspaceInstance.access); })
                            .map(function (workspaceInstance) { return (workspaceInstance.access); });
                        if (instance_1.default.get()) {
                            instance_1.default.get().webContents.send('event:client:update:workspaces:access:response', { workspacesAccess: workspacesAccess });
                            instance_1.default.get().webContents.send('event:client:update:workspaces:premium:response', {});
                        }
                        return [2];
                }
            });
        }); });
    };
    WorkspaceObjRepository.convertSyncInstanceToLocal = function (workspaceObj, existItemInstance) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var fillSyncObjProperties, fillModelProperties, workspaceInstance;
                        return __generator(this, function (_a) {
                            if (!workspaceObj) {
                                return [2, resolve(null)];
                            }
                            fillSyncObjProperties = function (item, data) {
                                return workspace_1.default.prepareSyncOnlyProperties(workspace_1.default.prepareItemDbProperties(item, data), data);
                            };
                            fillModelProperties = function (item, data) {
                                return workspace_1.default.prepareCommonProperties(workspace_1.default.prepareItemDbProperties(item, data), data);
                            };
                            workspaceInstance = null;
                            if (existItemInstance) {
                                workspaceInstance = fillSyncObjProperties(workspace_1.default.prepareModelData(existItemInstance), workspaceObj);
                            }
                            else {
                                workspaceInstance = fillModelProperties(workspace_1.default.prepareModelData(workspaceObj), workspaceObj);
                            }
                            resolve(workspaceInstance);
                            return [2];
                        });
                    }); })];
            });
        });
    };
    WorkspaceObjRepository.getErasedWorkspacesForUploadOnServer = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        workspace_1.default.findAll({
            "erised": true,
            "needSync": true
        }, {}, function (err, workspacesObjs) {
            var workspaces = [];
            for (var _i = 0, workspacesObjs_1 = workspacesObjs; _i < workspacesObjs_1.length; _i++) {
                var workspaceObjs = workspacesObjs_1[_i];
                workspaces.push(workspaceObjs.globalId);
            }
            callback(err, workspaces);
        });
    };
    ;
    WorkspaceObjRepository.getUpdatedWorkspacesForUploadOnServer = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        workspace_1.default.findAll({
            "needSync": true
        }, {
            "order": { "updatedAt": "desc" }
        }, function (err, noteObjs) {
            if (err) {
                return callback(err, []);
            }
            callback(err, noteObjs);
        });
    };
    WorkspaceObjRepository.getUpdatedWorkspacesCountForUploadOnServer = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        workspace_1.default.findAll({
            "needSync": true
        }, {
            "order": { "updatedAt": "desc" }
        }, function (err, workspaceObjs) {
            callback(err, workspaceObjs.length);
        });
    };
    WorkspaceObjRepository.getSyncWorkspaceEntity = function (globalId, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        workspace_1.default.find({ "globalId": globalId }, {}, function (err, workspaceObj) { return __awaiter(_this, void 0, void 0, function () {
            var workspace, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        workspace = new SyncWorkspaceEntity_1.default();
                        if (!workspaceObj) return [3, 2];
                        workspace.globalId = workspaceObj.globalId;
                        workspace.title = workspaceObj.title;
                        _a = workspace;
                        return [4, OrgObjRepository_1.default.getUserOrg({ globalId: workspaceObj.orgId })];
                    case 1:
                        _a.org = _b.sent();
                        workspace.userId = workspaceObj.userId;
                        workspace.createdAt = workspaceObj.createdAt;
                        workspace.updatedAt = workspaceObj.updatedAt;
                        workspace.isDefault = workspaceObj.isDefault;
                        workspace.countMembers = workspaceObj.countMembers;
                        workspace.countInvites = workspaceObj.countInvites;
                        workspace.access = workspaceObj.access;
                        workspace.isNotesLimited = workspaceObj.isNotesLimited;
                        _b.label = 2;
                    case 2:
                        callback(err, workspace);
                        return [2];
                }
            });
        }); });
    };
    WorkspaceObjRepository.updateWorkspaceDownloadedFromServerI = function (workspaceObj, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!workspaceObj) {
                            return [2, callback(null, false)];
                        }
                        return [4, WorkspaceObjRepository.update({ workspaceObjs: workspaceObj })];
                    case 1:
                        result = _a.sent();
                        callback(null, result);
                        return [2];
                }
            });
        });
    };
    WorkspaceObjRepository.updateWorkspacesDownloadedFromServerI = function (workspaceObjs, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!workspaceObjs.length) {
                            return [2, callback(null, false)];
                        }
                        return [4, WorkspaceObjRepository.update({ workspaceObjs: workspaceObjs })];
                    case 1:
                        result = _a.sent();
                        callback(null, result);
                        return [2];
                }
            });
        });
    };
    WorkspaceObjRepository.getUserWorkspacesR = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        WorkspaceObjRepository.getAllWorkspacesR(function (err, list) {
            callback(err, list);
        });
    };
    WorkspaceObjRepository.getAllWorkspacesR = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        workspace_1.default.findAll({}, {}, function (err, list) {
            return callback(err, list);
        });
    };
    WorkspaceObjRepository.getR = function (globalId, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        if (globalId === null) {
            globalId = "";
        }
        workspace_1.default.find({ globalId: globalId }, {}, function (err, item) {
            callback(err, item);
        });
    };
    WorkspaceObjRepository.clearRemovedData = function (callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        workspace_1.default.findAll({
            "erised": true,
        }, {}, function (err, workspaceObjs) { return __awaiter(_this, void 0, void 0, function () {
            var _i, workspaceObjs_3, workspaceObj;
            return __generator(this, function (_a) {
                for (_i = 0, workspaceObjs_3 = workspaceObjs; _i < workspaceObjs_3.length; _i++) {
                    workspaceObj = workspaceObjs_3[_i];
                }
                callback(null, true);
                return [2];
            });
        }); });
    };
    return WorkspaceObjRepository;
}());
exports.default = WorkspaceObjRepository;
