"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_1 = __importDefault(require("../../../db/models/user"));
var UserObjRepository = (function () {
    function UserObjRepository() {
    }
    UserObjRepository.update = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var queryData = inputData.queryData, updateData = inputData.updateData;
        user_1.default.update(queryData, updateData, {}, callback);
    };
    UserObjRepository.getDateNextQuotaReset = function (days) {
        return user_1.default.getDateNextQuotaReset(days);
    };
    return UserObjRepository;
}());
exports.default = UserObjRepository;
