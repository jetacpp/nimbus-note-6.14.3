"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../config"));
var NimbusSDK_1 = __importDefault(require("../../nimbussdk/net/NimbusSDK"));
var TodoObj_1 = __importDefault(require("../db/TodoObj"));
var NoteObjRepository_1 = __importDefault(require("./NoteObjRepository"));
var SyncTodoEntity_1 = __importDefault(require("../../nimbussdk/net/response/entities/SyncTodoEntity"));
var todo_1 = __importDefault(require("../../../db/models/todo"));
var socketFunctions_1 = __importDefault(require("../../socket/socketFunctions"));
var socketStoreType_1 = __importDefault(require("../../socket/socketStoreType"));
var TodoObjRepository = (function () {
    function TodoObjRepository() {
    }
    TodoObjRepository.create = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var parentId = inputData.parentId;
        NimbusSDK_1.default.getAccountManager().getUniqueUserName(function (err, uniqueUserName) {
            var todo = new TodoObj_1.default();
            todo.globalId = TodoObjRepository.generateGlobalId();
            todo.parentId = parentId;
            todo.dateAdded = new Date().getTime();
            todo.label = null;
            todo.checked = false;
            todo.uniqueUserName = uniqueUserName;
            todo.needSync = true;
            todo.syncDate = 0;
            callback(err, todo);
        });
    };
    TodoObjRepository.generateGlobalId = function () {
        return NoteObjRepository_1.default.generateGlobalId();
    };
    TodoObjRepository.getSyncTodoEntityList = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
        var findQuery = { noteGlobalId: noteGlobalId };
        var options = { workspaceId: workspaceId };
        options.findParams = {
            selector: findQuery,
            sort: [{ "orderNumber": "asc" }]
        };
        todo_1.default.findAll(findQuery, options, function (err, todoObjs) {
            var todoList = [];
            for (var _i = 0, todoObjs_1 = todoObjs; _i < todoObjs_1.length; _i++) {
                var todoObj = todoObjs_1[_i];
                var todo_2 = new SyncTodoEntity_1.default();
                todo_2.global_id = todoObj.globalId;
                todo_2.checked = todoObj.checked;
                todo_2.label = todoObj.label;
                todo_2.date = todoObj.dateAdded;
                todoList.push(todo_2);
            }
            callback(err, todoList);
        });
    };
    TodoObjRepository.updateNoteTodosDownloadedFromServerI = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, todoObjs = inputData.todoObjs;
        if (todoObjs && todoObjs.length > 0) {
            todo_1.default.findAll({
                "parentId": todoObjs[0].parentId
            }, { workspaceId: workspaceId }, function (err, list) {
                todo_1.default.remove({ "parentId": todoObjs[0].parentId }, {}, function (err, response) { return __awaiter(_this, void 0, void 0, function () {
                    var result;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4, TodoObjRepository.update({ workspaceId: workspaceId, todoObjs: list })];
                            case 1:
                                result = _a.sent();
                                callback(err, result);
                                return [2];
                        }
                    });
                }); });
            });
        }
        else {
            callback(null, true);
        }
    };
    TodoObjRepository.getUserTodoR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        TodoObjRepository.getAllTodoR(inputData, function (err, list) {
            callback(err, list);
        });
    };
    TodoObjRepository.getAllTodoR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        todo_1.default.findAll({}, { workspaceId: workspaceId }, function (err, list) {
            callback(err, list);
        });
    };
    TodoObjRepository.update = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, todoObjs, getUserTodo, updateUserTodo, createUserTodo, processUserTodo, orderNumber, _i, todoObjs_2, todoObj, result;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, todoObjs = inputData.todoObjs;
                                    if (!todoObjs) {
                                        return [2, resolve(false)];
                                    }
                                    getUserTodo = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                                                    todo_1.default.find({ globalId: globalId }, { workspaceId: workspaceId }, function (err, todoObj) {
                                                        if (err || !todoObj) {
                                                            return resolve(null);
                                                        }
                                                        resolve(todoObj);
                                                    });
                                                })];
                                        });
                                    }); };
                                    updateUserTodo = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, item = inputData.item;
                                                    if (config_1.default.SHOW_WEB_CONSOLE) {
                                                    }
                                                    todo_1.default.update({
                                                        globalId: item.globalId,
                                                        noteGlobalId: item.noteGlobalId
                                                    }, item, { workspaceId: workspaceId }, function (err, count) {
                                                        if (err || !count) {
                                                            return resolve(false);
                                                        }
                                                        resolve(!!count);
                                                    });
                                                })];
                                        });
                                    }); };
                                    createUserTodo = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, item = inputData.item;
                                                    if (config_1.default.SHOW_WEB_CONSOLE) {
                                                    }
                                                    todo_1.default.add(item, { workspaceId: workspaceId }, function (err, todoInstance) {
                                                        if (err || !todoInstance) {
                                                            return resolve(false);
                                                        }
                                                        resolve(!!todoInstance);
                                                    });
                                                })];
                                        });
                                    }); };
                                    processUserTodo = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var workspaceId, todoObj, existItemInstance, saveInstance, savedInstance;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                workspaceId = inputData.workspaceId, todoObj = inputData.todoObj;
                                                                return [4, getUserTodo({ workspaceId: workspaceId, globalId: todoObj.globalId })];
                                                            case 1:
                                                                existItemInstance = _a.sent();
                                                                return [4, TodoObjRepository.convertSyncInstanceToLocal(todoObj, existItemInstance)];
                                                            case 2:
                                                                saveInstance = _a.sent();
                                                                if (!saveInstance) {
                                                                    return [2, resolve(false)];
                                                                }
                                                                if (typeof (todoObj.orderNum) !== "undefined") {
                                                                    saveInstance.orderNumber = todoObj.orderNum;
                                                                }
                                                                saveInstance['needSync'] = false;
                                                                savedInstance = null;
                                                                if (!existItemInstance) return [3, 4];
                                                                return [4, updateUserTodo({ workspaceId: workspaceId, item: saveInstance })];
                                                            case 3:
                                                                savedInstance = _a.sent();
                                                                resolve(savedInstance);
                                                                return [3, 6];
                                                            case 4: return [4, createUserTodo({ workspaceId: workspaceId, item: saveInstance })];
                                                            case 5:
                                                                savedInstance = _a.sent();
                                                                resolve(savedInstance);
                                                                _a.label = 6;
                                                            case 6: return [2];
                                                        }
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    orderNumber = 1;
                                    _i = 0, todoObjs_2 = todoObjs;
                                    _a.label = 1;
                                case 1:
                                    if (!(_i < todoObjs_2.length)) return [3, 4];
                                    todoObj = todoObjs_2[_i];
                                    todoObj.orderNumber = orderNumber;
                                    orderNumber++;
                                    return [4, processUserTodo({ workspaceId: workspaceId, todoObj: todoObj })];
                                case 2:
                                    result = _a.sent();
                                    if (result) {
                                        TodoObjRepository.sendSocketEventOnUpdate({
                                            workspaceId: workspaceId,
                                            noteGlobalId: todoObj.noteGlobalId,
                                            globalId: todoObj.globalId
                                        });
                                    }
                                    _a.label = 3;
                                case 3:
                                    _i++;
                                    return [3, 1];
                                case 4:
                                    resolve(true);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    TodoObjRepository.sendSocketEventOnUpdate = function (inputData) {
        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, globalId = inputData.globalId;
        if (globalId && noteGlobalId) {
            socketFunctions_1.default.addStoreData({
                workspaceId: workspaceId,
                storeType: socketStoreType_1.default.TODO_LIST_FOR_UPDATE,
                data: {
                    noteGlobalId: noteGlobalId,
                    globalId: globalId
                }
            });
        }
    };
    TodoObjRepository.sendSocketEventOnRemove = function (inputData) {
        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, globalId = inputData.globalId;
        if (globalId && noteGlobalId) {
            socketFunctions_1.default.addStoreData({
                workspaceId: workspaceId,
                storeType: socketStoreType_1.default.TODO_LIST_FOR_REMOVE,
                data: {
                    noteGlobalId: noteGlobalId,
                    globalId: globalId
                }
            });
        }
    };
    TodoObjRepository.convertSyncInstanceToLocal = function (todoObj, existItemInstance) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var fillSyncObjProperties, fillModelProperties, todoInstance;
                        return __generator(this, function (_a) {
                            if (!todoObj) {
                                return [2, resolve(null)];
                            }
                            fillSyncObjProperties = function (item, data) {
                                return todo_1.default.prepareSyncOnlyProperties(todo_1.default.prepareItemDbProperties(item, data), data);
                            };
                            fillModelProperties = function (item, data) {
                                return todo_1.default.prepareCommonProperties(todo_1.default.prepareItemDbProperties(item, data), data);
                            };
                            todoInstance = null;
                            if (existItemInstance) {
                                todoInstance = fillSyncObjProperties(todo_1.default.prepareModelData(existItemInstance), todoObj);
                                if (todoInstance.label !== todoObj.label) {
                                    todoInstance.label = todoObj.label;
                                }
                                if (todoInstance.checked !== todoObj.checked) {
                                    todoInstance.checked = todoObj.checked;
                                }
                            }
                            else {
                                todoInstance = fillModelProperties(todo_1.default.prepareModelData(todoObj), todoObj);
                            }
                            resolve(todoInstance);
                            return [2];
                        });
                    }); })];
            });
        });
    };
    TodoObjRepository.findNoteTodoList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
                        todo_1.default.findAll({
                            "noteGlobalId": noteGlobalId
                        }, { workspaceId: workspaceId }, function (err, todoObjs) {
                            if (err) {
                                return resolve([]);
                            }
                            resolve(todoObjs);
                        });
                    })];
            });
        });
    };
    TodoObjRepository.removeNoteTodoList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, todoList = inputData.todoList;
                        if (!todoList.length) {
                            return resolve(true);
                        }
                        var todoGlobalIdList = [];
                        for (var _i = 0, todoList_1 = todoList; _i < todoList_1.length; _i++) {
                            var todoInstance = todoList_1[_i];
                            TodoObjRepository.sendSocketEventOnRemove({
                                workspaceId: workspaceId,
                                noteGlobalId: todoInstance.noteGlobalId,
                                globalId: todoInstance.globalId
                            });
                            todoGlobalIdList.push(todoInstance.globalId);
                        }
                        if (!todoGlobalIdList.length) {
                            return resolve(true);
                        }
                        todo_1.default.erase({
                            "globalId": { "$in": todoGlobalIdList }
                        }, { workspaceId: workspaceId }, function (err, numRemoved) {
                            if (err) {
                                return resolve(false);
                            }
                            resolve(true);
                        });
                    })];
            });
        });
    };
    TodoObjRepository.deleteData = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                        todo_1.default.erase({
                            "globalId": globalId
                        }, { workspaceId: workspaceId }, function (err, response) {
                            if (err) {
                                return resolve(null);
                            }
                            resolve(response);
                        });
                    })];
            });
        });
    };
    TodoObjRepository.clearRemovedData = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        todo_1.default.findAll({
            "erised": true
        }, { workspaceId: workspaceId }, function (err, todoObjs) { return __awaiter(_this, void 0, void 0, function () {
            var _i, todoObjs_3, todoObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _i = 0, todoObjs_3 = todoObjs;
                        _a.label = 1;
                    case 1:
                        if (!(_i < todoObjs_3.length)) return [3, 4];
                        todoObj = todoObjs_3[_i];
                        return [4, TodoObjRepository.deleteData({ workspaceId: workspaceId, globalId: todoObj.globalId })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        callback(null, true);
                        return [2];
                }
            });
        }); });
    };
    TodoObjRepository.updateNeedSync = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, noteGlobalId = inputData.noteGlobalId;
                        if (!globalId || !noteGlobalId) {
                            return resolve(0);
                        }
                        todo_1.default.update({
                            globalId: globalId,
                            noteGlobalId: noteGlobalId
                        }, { needSync: false }, { workspaceId: workspaceId }, function (err, count) {
                            if (err || !count) {
                                return resolve(0);
                            }
                            return resolve(count);
                        });
                    })];
            });
        });
    };
    TodoObjRepository.getUncheckedCount = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
                        if (!noteGlobalId) {
                            return resolve(0);
                        }
                        todo_1.default.count({
                            noteGlobalId: noteGlobalId,
                            checked: false
                        }, { workspaceId: workspaceId }, function (err, itemTodoCount) {
                            if (!err || itemTodoCount) {
                                return resolve(0);
                            }
                            return resolve(itemTodoCount);
                        });
                    })];
            });
        });
    };
    TodoObjRepository.getCount = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
                        if (!noteGlobalId) {
                            return resolve(0);
                        }
                        todo_1.default.count({
                            noteGlobalId: noteGlobalId
                        }, { workspaceId: workspaceId }, function (err, itemTodoCount) {
                            if (!err || itemTodoCount) {
                                return resolve(0);
                            }
                            return resolve(itemTodoCount);
                        });
                    })];
            });
        });
    };
    return TodoObjRepository;
}());
exports.default = TodoObjRepository;
