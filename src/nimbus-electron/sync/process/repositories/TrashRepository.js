"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NoteObjRepository_1 = __importDefault(require("./NoteObjRepository"));
var TrashObjRepository = (function () {
    function TrashObjRepository() {
    }
    TrashObjRepository.checkIfNoteInTrash = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NoteObjRepository_1.default.checkIfNoteInTrash(inputData, function (err, noteInTrash) {
            if (err) {
                return callback(err, false);
            }
            callback(err, noteInTrash);
        });
    };
    return TrashObjRepository;
}());
exports.default = TrashObjRepository;
