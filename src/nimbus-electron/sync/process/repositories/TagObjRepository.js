"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../config"));
var NimbusSDK_1 = __importDefault(require("../../nimbussdk/net/NimbusSDK"));
var TagObj_1 = __importDefault(require("../db/TagObj"));
var NoteObjRepository_1 = __importDefault(require("./NoteObjRepository"));
var NotesUpdateRequest_1 = __importDefault(require("../../nimbussdk/net/request/NotesUpdateRequest"));
var tag_1 = __importDefault(require("../../../db/models/tag"));
var noteTags_1 = __importDefault(require("../../../db/models/noteTags"));
var socketFunctions_1 = __importDefault(require("../../socket/socketFunctions"));
var socketStoreType_1 = __importDefault(require("../../socket/socketStoreType"));
var selectedItem_1 = __importDefault(require("../../../window/selectedItem"));
var TagObjRepository = (function () {
    function TagObjRepository() {
    }
    TagObjRepository.create = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var tag = inputData.tag;
        NimbusSDK_1.default.getAccountManager().getUniqueUserName(function (err, uniqueUserName) {
            var tagObj = new TagObj_1.default();
            tagObj.title = tag;
            tagObj.oldTitle = null;
            tagObj.dateAdded = new Date().getTime();
            tagObj.dateUpdated = tagObj.dateAdded;
            tagObj.needSync = true;
            tagObj.syncDate = 0;
            tagObj.parentId = null;
            tagObj.uniqueUserName = uniqueUserName;
            callback(err, tagObj);
        });
    };
    TagObjRepository.getUserTagsR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        TagObjRepository.getAllTagsR(inputData, function (err, list) {
            callback(err, list);
        });
    };
    TagObjRepository.getAllTagsR = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        tag_1.default.findAll({}, { workspaceId: workspaceId }, function (err, list) {
            return callback(err, list);
        });
    };
    TagObjRepository.isAvailableNotesForSync = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        NoteObjRepository_1.default.isAvailableNotesForSync(inputData, callback);
    };
    TagObjRepository.getErasedTagsForUploadOnServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        tag_1.default.findAll({
            "erised": true,
            "needSync": true
        }, { workspaceId: workspaceId }, function (err, userTagsObjs) {
            var userTagsTitle = [];
            for (var _i = 0, userTagsObjs_1 = userTagsObjs; _i < userTagsObjs_1.length; _i++) {
                var userTagsObj = userTagsObjs_1[_i];
                userTagsTitle.push(userTagsObj.title);
            }
            callback(null, userTagsTitle);
        });
    };
    TagObjRepository.getErasedTagForUploadOnServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, tag = inputData.tag;
        tag_1.default.find({
            "tag": tag,
            "erised": true,
            "needSync": true
        }, { workspaceId: workspaceId }, function (err, userTagObj) {
            if (err || !userTagObj) {
                return callback(null, null);
            }
            callback(null, userTagObj);
        });
    };
    TagObjRepository.getRenamedTagsForUploadOnServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        tag_1.default.findAll({
            "oldTitle": { "$nin": [null, ""] }
        }, { workspaceId: workspaceId }, function (err, userTags) {
            var renamedTags = [];
            for (var _i = 0, userTags_1 = userTags; _i < userTags_1.length; _i++) {
                var tagObj = userTags_1[_i];
                renamedTags.push(new NotesUpdateRequest_1.default.Body.RenameTag(tagObj.oldTitle, tagObj.title));
            }
            callback(err, renamedTags);
        });
    };
    TagObjRepository.getUpdatedTagsForUploadOnServer = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        tag_1.default.findAll({}, {
            workspaceId: workspaceId,
            "order": { "title": "asc" }
        }, function (err, tagsObjs) {
            var tags = [];
            for (var _i = 0, tagsObjs_1 = tagsObjs; _i < tagsObjs_1.length; _i++) {
                var tagObj = tagsObjs_1[_i];
                tags.push(tagObj.title);
            }
            callback(err, tags);
        });
    };
    TagObjRepository.callAfterUploadErasedTagsOnServerI = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        tag_1.default.findAll({
            "erised": true,
            "needSync": true
        }, { workspaceId: workspaceId }, function (err, userTags) { return __awaiter(_this, void 0, void 0, function () {
            var deleteTags, _i, userTags_2, userTag;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (err) {
                            return [2, callback(err, false)];
                        }
                        deleteTags = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        var workspaceId = inputData.workspaceId, userTag = inputData.userTag;
                                        tag_1.default.erase({ tag: userTag.tag }, { workspaceId: workspaceId }, function (err, response) {
                                            resolve(true);
                                        });
                                    })];
                            });
                        }); };
                        _i = 0, userTags_2 = userTags;
                        _a.label = 1;
                    case 1:
                        if (!(_i < userTags_2.length)) return [3, 4];
                        userTag = userTags_2[_i];
                        return [4, deleteTags({ workspaceId: workspaceId, userTag: userTag })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        callback(null, true);
                        return [2];
                }
            });
        }); });
    };
    TagObjRepository.callAfterUploadStoreTagsI = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, tags = inputData.tags;
                        if (!tags.length) {
                            return resolve(true);
                        }
                        tag_1.default.findAll({
                            "tag": { "$in": tags }
                        }, { workspaceId: workspaceId }, function (err, tagObjs) { return __awaiter(_this, void 0, void 0, function () {
                            var i;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        for (i in tagObjs) {
                                            if (tagObjs.hasOwnProperty(i)) {
                                                tagObjs[i].needSync = false;
                                            }
                                        }
                                        return [4, TagObjRepository.update({ workspaceId: workspaceId, tagObjs: tagObjs })];
                                    case 1:
                                        _a.sent();
                                        resolve(true);
                                        return [2];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    TagObjRepository.callAfterUploadRenamedTagsI = function (inputData) {
        var _this = this;
        return new Promise(function (resolve) {
            var workspaceId = inputData.workspaceId, tags = inputData.tags;
            if (!tags.length) {
                return resolve(true);
            }
            var tagList = tags.map(function (tagSynObj) {
                return tagSynObj['newtag'];
            });
            tag_1.default.findAll({
                "oldTitle": { "$ne": [null, ""] },
                "tag": { "$in": tagList }
            }, { workspaceId: workspaceId }, function (err, tagObjs) { return __awaiter(_this, void 0, void 0, function () {
                var i;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            for (i in tagObjs) {
                                if (tagObjs.hasOwnProperty(i)) {
                                    tagObjs[i].oldTitle = null;
                                    tagObjs[i].needSync = false;
                                }
                            }
                            return [4, TagObjRepository.update({ workspaceId: workspaceId, tagObjs: tagObjs })];
                        case 1:
                            _a.sent();
                            resolve(true);
                            return [2];
                    }
                });
            }); });
        });
    };
    TagObjRepository.checkIfTagRenamed = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId, tag = inputData.tag;
        tag_1.default.findAll({
            "oldTitle": tag,
            "needSync": true
        }, { workspaceId: workspaceId }, function (err, tagObjs) {
            callback(err, tagObjs.length ? tagObjs[0] : null);
        });
    };
    TagObjRepository.update = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, tagObjs, getUserTag, updateUserTag, createUserTag, processUserTag, result, updatedList, _i, tagObjs_1, tagObj;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, tagObjs = inputData.tagObjs;
                                    if (!tagObjs) {
                                        return [2, resolve(false)];
                                    }
                                    getUserTag = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, title = inputData.title;
                                                    tag_1.default.find({ "title": title }, { workspaceId: workspaceId }, function (err, tagObj) {
                                                        if (err || !tagObj) {
                                                            return resolve(null);
                                                        }
                                                        resolve(tagObj);
                                                    });
                                                })];
                                        });
                                    }); };
                                    updateUserTag = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, item = inputData.item;
                                                    if (config_1.default.SHOW_WEB_CONSOLE) {
                                                    }
                                                    tag_1.default.update({ "tag": item.title }, item, { workspaceId: workspaceId }, function (err, count) {
                                                        if (err || !count) {
                                                            return resolve(false);
                                                        }
                                                        TagObjRepository.emitSocketEvent({
                                                            workspaceId: workspaceId,
                                                            tagList: [(item.title || item.tag)],
                                                            action: "update"
                                                        });
                                                        resolve(!!count);
                                                    });
                                                })];
                                        });
                                    }); };
                                    createUserTag = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) {
                                                    var workspaceId = inputData.workspaceId, item = inputData.item;
                                                    if (config_1.default.SHOW_WEB_CONSOLE) {
                                                    }
                                                    tag_1.default.add(item, { workspaceId: workspaceId }, function (err, itemInstance) {
                                                        if (err || !itemInstance) {
                                                            return resolve(false);
                                                        }
                                                        TagObjRepository.emitSocketEvent({
                                                            workspaceId: workspaceId,
                                                            tagList: [(item.title || item.tag)],
                                                            action: "add"
                                                        });
                                                        resolve(!!itemInstance);
                                                    });
                                                })];
                                        });
                                    }); };
                                    processUserTag = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                                    var workspaceId, tagObj, existItemInstance, saveInstance, _a, _b;
                                                    return __generator(this, function (_c) {
                                                        switch (_c.label) {
                                                            case 0:
                                                                workspaceId = inputData.workspaceId, tagObj = inputData.tagObj;
                                                                return [4, getUserTag({ workspaceId: workspaceId, title: tagObj.title })];
                                                            case 1:
                                                                existItemInstance = _c.sent();
                                                                return [4, TagObjRepository.convertSyncInstanceToLocal(tagObj, existItemInstance)];
                                                            case 2:
                                                                saveInstance = _c.sent();
                                                                if (!saveInstance) {
                                                                    return [2, resolve(false)];
                                                                }
                                                                saveInstance['needSync'] = false;
                                                                if (!existItemInstance) return [3, 4];
                                                                _a = resolve;
                                                                return [4, updateUserTag({ workspaceId: workspaceId, item: saveInstance })];
                                                            case 3:
                                                                _a.apply(void 0, [_c.sent()]);
                                                                return [3, 6];
                                                            case 4:
                                                                _b = resolve;
                                                                return [4, createUserTag({ workspaceId: workspaceId, item: saveInstance })];
                                                            case 5:
                                                                _b.apply(void 0, [_c.sent()]);
                                                                _c.label = 6;
                                                            case 6: return [2];
                                                        }
                                                    });
                                                }); })];
                                        });
                                    }); };
                                    result = null;
                                    updatedList = [];
                                    if (!(tagObjs instanceof Array)) return [3, 5];
                                    _i = 0, tagObjs_1 = tagObjs;
                                    _a.label = 1;
                                case 1:
                                    if (!(_i < tagObjs_1.length)) return [3, 4];
                                    tagObj = tagObjs_1[_i];
                                    return [4, processUserTag({ workspaceId: workspaceId, tagObj: tagObj })];
                                case 2:
                                    result = _a.sent();
                                    if (result) {
                                        updatedList.push(tagObj);
                                    }
                                    _a.label = 3;
                                case 3:
                                    _i++;
                                    return [3, 1];
                                case 4: return [3, 7];
                                case 5: return [4, processUserTag({ workspaceId: workspaceId, tagObj: tagObjs })];
                                case 6:
                                    result = _a.sent();
                                    if (result) {
                                        updatedList.push(tagObjs);
                                    }
                                    _a.label = 7;
                                case 7:
                                    resolve(result);
                                    return [2];
                            }
                        });
                    }); })];
            });
        });
    };
    TagObjRepository.sendSocketEventOnUpdate = function (inputData) {
        var workspaceId = inputData.workspaceId, tagObjs = inputData.tagObjs;
        var tagList = [];
        for (var _i = 0, tagObjs_2 = tagObjs; _i < tagObjs_2.length; _i++) {
            var tagObj = tagObjs_2[_i];
            var updatedTag = tagObj.title ? tagObj.title : tagObj.tag;
            if (updatedTag) {
                tagList.push(updatedTag);
            }
        }
        TagObjRepository.emitSocketEvent({
            workspaceId: workspaceId,
            tagList: tagList,
            action: "add"
        });
    };
    TagObjRepository.emitSocketEvent = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var workspaceId, tagList, action, findUserTag, findUserNoteTag, _i, tagList_1, tag, tagInstance, noteTagsList, _a, noteTagsList_1, noteTagInstance;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        workspaceId = inputData.workspaceId, tagList = inputData.tagList, action = inputData.action;
                        if (!tagList.length) return [3, 5];
                        socketFunctions_1.default.addStoreData({
                            workspaceId: workspaceId,
                            storeType: socketStoreType_1.default.TAGS_FOR_UPDATE,
                            data: {
                                action: action,
                                tags: tagList
                            }
                        });
                        findUserTag = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                        var workspaceId, tag;
                                        return __generator(this, function (_a) {
                                            workspaceId = inputData.workspaceId, tag = inputData.tag;
                                            tag_1.default.find({ tag: tag }, { workspaceId: workspaceId }, function (err, tagInstance) {
                                                if (err || !tagInstance) {
                                                    return resolve(null);
                                                }
                                                resolve(tagInstance);
                                            });
                                            return [2];
                                        });
                                    }); })];
                            });
                        }); };
                        findUserNoteTag = function (inputData) {
                            return __awaiter(this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                            var workspaceId, globalId, selectedNoteGlobalId;
                                            return __generator(this, function (_a) {
                                                workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                                                selectedNoteGlobalId = selectedItem_1.default.get({
                                                    workspaceId: workspaceId,
                                                    itemType: selectedItem_1.default.TYPE_NOTE
                                                });
                                                noteTags_1.default.findAll({
                                                    tagGlobalId: globalId,
                                                    noteGlobalId: selectedNoteGlobalId
                                                }, { workspaceId: workspaceId }, function (err, noteTagsList) {
                                                    if (err || !noteTagsList) {
                                                        return resolve([]);
                                                    }
                                                    resolve(noteTagsList);
                                                });
                                                return [2];
                                            });
                                        }); })];
                                });
                            });
                        };
                        _i = 0, tagList_1 = tagList;
                        _b.label = 1;
                    case 1:
                        if (!(_i < tagList_1.length)) return [3, 5];
                        tag = tagList_1[_i];
                        return [4, findUserTag({ workspaceId: workspaceId, tag: tag })];
                    case 2:
                        tagInstance = _b.sent();
                        if (!tagInstance) return [3, 4];
                        return [4, findUserNoteTag({ workspaceId: workspaceId, globalId: tagInstance.globalId })];
                    case 3:
                        noteTagsList = _b.sent();
                        for (_a = 0, noteTagsList_1 = noteTagsList; _a < noteTagsList_1.length; _a++) {
                            noteTagInstance = noteTagsList_1[_a];
                            TagObjRepository.emitSocketEventForNote({
                                workspaceId: workspaceId,
                                tag: tag,
                                noteGlobalId: noteTagInstance.noteGlobalId,
                                action: action
                            });
                        }
                        _b.label = 4;
                    case 4:
                        _i++;
                        return [3, 1];
                    case 5: return [2];
                }
            });
        });
    };
    TagObjRepository.emitSocketEventForNote = function (inputData) {
        var workspaceId = inputData.workspaceId, tag = inputData.tag, noteGlobalId = inputData.noteGlobalId, action = inputData.action;
        if (tag && noteGlobalId) {
            socketFunctions_1.default.addStoreData({
                workspaceId: workspaceId,
                storeType: socketStoreType_1.default.NOTES_TAGS_FOR_UPDATE,
                data: {
                    action: action,
                    noteGlobalId: noteGlobalId,
                    tag: tag
                }
            });
        }
    };
    TagObjRepository.convertSyncInstanceToLocal = function (tagObj, existItemInstance) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var fillSyncObjProperties, fillModelProperties, itemInstance;
                        return __generator(this, function (_a) {
                            if (!tagObj) {
                                return [2, resolve(null)];
                            }
                            fillSyncObjProperties = function (item, data) {
                                return tag_1.default.prepareSyncOnlyProperties(tag_1.default.prepareItemDbProperties(item, data), data);
                            };
                            fillModelProperties = function (item, data) {
                                return tag_1.default.prepareCommonProperties(tag_1.default.prepareItemDbProperties(item, data), data);
                            };
                            itemInstance = null;
                            if (existItemInstance) {
                                itemInstance = fillSyncObjProperties(tag_1.default.prepareModelData(existItemInstance), tagObj);
                            }
                            else {
                                itemInstance = fillModelProperties(tag_1.default.prepareModelData(tagObj), tagObj);
                            }
                            resolve(itemInstance);
                            return [2];
                        });
                    }); })];
            });
        });
    };
    TagObjRepository.findNoteTagsList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
                        tag_1.default.findAll({
                            "noteGlobalId": noteGlobalId
                        }, { workspaceId: workspaceId }, function (err, tagObjs) {
                            if (err) {
                                return resolve([]);
                            }
                            resolve(tagObjs);
                        });
                    })];
            });
        });
    };
    TagObjRepository.findNoteTagList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId;
                        tag_1.default.findAll({
                            "noteGlobalId": noteGlobalId
                        }, { workspaceId: workspaceId }, function (err, tagObjs) {
                            if (err) {
                                return resolve([]);
                            }
                            resolve(tagObjs);
                        });
                    })];
            });
        });
    };
    TagObjRepository.updateTagsDownloadedFromServerI = function (inputData, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        return __awaiter(this, void 0, void 0, function () {
            var tagObjs, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        tagObjs = inputData.tagObjs;
                        if (!tagObjs.length) {
                            return [2, callback(null, false)];
                        }
                        return [4, TagObjRepository.update(inputData)];
                    case 1:
                        result = _a.sent();
                        callback(null, result);
                        return [2];
                }
            });
        });
    };
    TagObjRepository.deleteData = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, tag = inputData.tag;
                        tag_1.default.erase({
                            "tag": tag
                        }, { workspaceId: workspaceId }, function (err, response) {
                            if (err) {
                                return resolve(null);
                            }
                            resolve(response);
                        });
                    })];
            });
        });
    };
    TagObjRepository.removeNoteTagList = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, tagList = inputData.tagList;
                        if (!tagList.length) {
                            return resolve(true);
                        }
                        var tagGlobalIdList = [];
                        for (var _i = 0, tagList_2 = tagList; _i < tagList_2.length; _i++) {
                            var tagInstance = tagList_2[_i];
                            TagObjRepository.emitSocketEventForNote({
                                workspaceId: workspaceId,
                                tag: tagInstance.tag,
                                noteGlobalId: tagInstance.noteGlobalId,
                                action: "remove"
                            });
                            tagGlobalIdList.push(tagInstance.globalId);
                        }
                        if (!tagGlobalIdList.length) {
                            return resolve(true);
                        }
                        tag_1.default.erase({
                            "globalId": { "$in": tagGlobalIdList }
                        }, { workspaceId: workspaceId }, function (err, numRemoved) {
                            if (err) {
                                return resolve(false);
                            }
                            resolve(true);
                        });
                    })];
            });
        });
    };
    TagObjRepository.deleteNoteData = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, tag = inputData.tag;
                        if (!tag) {
                            return resolve(false);
                        }
                        var eraseUserNoteTags = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        var workspaceId = inputData.workspaceId, tagGlobalId = inputData.tagGlobalId, noteGlobalId = inputData.noteGlobalId;
                                        noteTags_1.default.erase({
                                            "tagGlobalId": tagGlobalId,
                                            "noteGlobalId": noteGlobalId
                                        }, { workspaceId: workspaceId }, function (err, response) {
                                            if (err) {
                                                return resolve(null);
                                            }
                                            resolve(response);
                                        });
                                    })];
                            });
                        }); };
                        var removeUserNoteTags = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            return __generator(this, function (_a) {
                                return [2, new Promise(function (resolve) {
                                        var workspaceId = inputData.workspaceId, tagGlobalId = inputData.tagGlobalId;
                                        noteTags_1.default.findAll({ tagGlobalId: tagGlobalId }, { workspaceId: workspaceId }, function (err, noteTagsList) { return __awaiter(_this, void 0, void 0, function () {
                                            var _i, noteTagsList_2, noteTagInstance;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        if (err) {
                                                            return [2, resolve(false)];
                                                        }
                                                        _i = 0, noteTagsList_2 = noteTagsList;
                                                        _a.label = 1;
                                                    case 1:
                                                        if (!(_i < noteTagsList_2.length)) return [3, 4];
                                                        noteTagInstance = noteTagsList_2[_i];
                                                        return [4, eraseUserNoteTags({
                                                                workspaceId: workspaceId,
                                                                tagGlobalId: noteTagInstance.tagGlobalId,
                                                                noteGlobalId: noteTagInstance.noteGlobalId
                                                            })];
                                                    case 2:
                                                        _a.sent();
                                                        TagObjRepository.emitSocketEventForNote({
                                                            workspaceId: workspaceId,
                                                            tag: tag,
                                                            noteGlobalId: noteTagInstance.noteGlobalId,
                                                            action: "remove"
                                                        });
                                                        _a.label = 3;
                                                    case 3:
                                                        _i++;
                                                        return [3, 1];
                                                    case 4:
                                                        resolve(true);
                                                        return [2];
                                                }
                                            });
                                        }); });
                                    })];
                            });
                        }); };
                        tag_1.default.findAll({ title: tag }, { workspaceId: workspaceId }, function (err, tagsList) { return __awaiter(_this, void 0, void 0, function () {
                            var _i, tagsList_1, tagInstance;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (err || !tagsList) {
                                            return [2, resolve(false)];
                                        }
                                        _i = 0, tagsList_1 = tagsList;
                                        _a.label = 1;
                                    case 1:
                                        if (!(_i < tagsList_1.length)) return [3, 4];
                                        tagInstance = tagsList_1[_i];
                                        return [4, removeUserNoteTags({ workspaceId: workspaceId, tagGlobalId: tagInstance.globalId })];
                                    case 2:
                                        _a.sent();
                                        _a.label = 3;
                                    case 3:
                                        _i++;
                                        return [3, 1];
                                    case 4:
                                        resolve(true);
                                        return [2];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    TagObjRepository.clearRemovedData = function (inputData, callback) {
        var _this = this;
        if (callback === void 0) { callback = function (err, res) {
        }; }
        var workspaceId = inputData.workspaceId;
        var deleteEraisedData = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, tag = inputData.tag;
                        tag_1.default.erase({
                            "tag": tag,
                            "erised": true
                        }, { workspaceId: workspaceId }, function (err, response) {
                            if (err) {
                                return resolve(null);
                            }
                            resolve(response);
                        });
                    })];
            });
        }); };
        tag_1.default.findAll({
            "erised": true
        }, { workspaceId: workspaceId }, function (err, tagObjs) { return __awaiter(_this, void 0, void 0, function () {
            var _i, tagObjs_3, tagObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _i = 0, tagObjs_3 = tagObjs;
                        _a.label = 1;
                    case 1:
                        if (!(_i < tagObjs_3.length)) return [3, 6];
                        tagObj = tagObjs_3[_i];
                        return [4, TagObjRepository.deleteNoteData({ workspaceId: workspaceId, tag: tagObj.tag })];
                    case 2:
                        _a.sent();
                        return [4, TagObjRepository.deleteData({ workspaceId: workspaceId, tag: tagObj.tag })];
                    case 3:
                        _a.sent();
                        return [4, deleteEraisedData({ workspaceId: workspaceId, tag: tagObj.tag })];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        _i++;
                        return [3, 1];
                    case 6:
                        callback(null, true);
                        return [2];
                }
            });
        }); });
    };
    TagObjRepository.updateNeedSync = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId, noteGlobalId = inputData.noteGlobalId, tag = inputData.tag;
                        if (!globalId || !noteGlobalId) {
                            return resolve(0);
                        }
                        tag_1.default.update({
                            globalId: globalId,
                            noteGlobalId: noteGlobalId
                        }, { needSync: false, tag: tag }, { workspaceId: workspaceId }, function (err, count) {
                            if (err || !count) {
                                return resolve(0);
                            }
                            return resolve(count);
                        });
                    })];
            });
        });
    };
    TagObjRepository.LAST_OPEN_TAG = "last_open_tag";
    return TagObjRepository;
}());
exports.default = TagObjRepository;
