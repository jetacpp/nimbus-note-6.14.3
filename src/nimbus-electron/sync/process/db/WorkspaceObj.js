"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WorkspaceObj = (function () {
    function WorkspaceObj() {
        this.globalId = "";
        this.title = "";
        this.org = null;
        this.userId = 0;
        this.createdAt = 0;
        this.updatedAt = 0;
        this.isDefault = false;
        this.countMembers = 0;
        this.countInvites = 0;
        this.access = null;
        this.defaultEncryptionKeyId = null;
        this.syncDate = 0;
        this.needSync = false;
        this.isNotesLimited = false;
        this.user = null;
        this.notesEmail = '';
        this.avatar = '';
        this.color = null;
    }
    WorkspaceObj.getTitle = function (obj, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        if (obj.isDefault && !obj.title) {
            return callback(null, WorkspaceObj.DEFAULT_NAME);
        }
        callback(null, obj.title);
    };
    WorkspaceObj.setTitle = function (obj, title) {
        obj.title = title;
        return obj;
    };
    WorkspaceObj.prototype.getTitle = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        WorkspaceObj.getTitle(this, callback);
    };
    WorkspaceObj.prototype.setTitle = function (title) {
        return WorkspaceObj.setTitle(this, title);
    };
    WorkspaceObj.DEFAULT_NAME = "default";
    return WorkspaceObj;
}());
exports.default = WorkspaceObj;
var WorkspaceAvatar = (function () {
    function WorkspaceAvatar() {
        this.url = "";
        this.createdAt = "";
        this.updatedAt = "";
    }
    return WorkspaceAvatar;
}());
exports.WorkspaceAvatar = WorkspaceAvatar;
