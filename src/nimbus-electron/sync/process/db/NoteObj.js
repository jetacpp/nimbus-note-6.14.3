"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../config"));
var NoteObj = (function () {
    function NoteObj() {
        this.globalId = "";
        this.isTemp = false;
        this.dateAdded = 0;
        this.dateUpdated = 0;
        this.syncDate = 0;
        this.isDownloaded = false;
        this.needSync = false;
        this.parentId = "";
        this.rootParentId = "";
        this.index = 0;
        this.type = "";
        this.title = "";
        this.shortText = "";
        this.url = "";
        this.role = "";
        this.uniqueUserName = "";
        this.firstImage = "";
        this.isMoreThanLimit = false;
        this.existOnServer = false;
        this.tags = "";
        this.editnote = 0;
        this.isEncrypted = 0;
        this.color = "";
        this.textAttachmentGlobalId = "";
        this.isMaybeInTrash = false;
        this.attachmentsInListCount = 0;
        this.attachmentsInListExist = false;
        this.reminderExist = false;
        this.todoExist = false;
        this.reminderLabel = "";
        this.todoCount = 0;
        this.locationLat = 0;
        this.locationLng = 0;
        this.locationAddress = "";
        this.isLocationExist = false;
        this.preview = {
            'global_id': ""
        };
    }
    NoteObj.getGlobalId = function (obj) {
        return obj.globalId;
    };
    NoteObj.setGlobalId = function (obj, globalId) {
        obj.globalId = globalId;
        return obj;
    };
    NoteObj.setLocation = function (obj, latitude, longitude) {
        obj = NoteObj.setLocationLat(obj, latitude);
        obj = NoteObj.setLocationLng(obj, longitude);
        var isExist = latitude && longitude;
        var address = null;
        obj = NoteObj.setLocationAddress(obj, address);
        return obj;
    };
    NoteObj.getLocationLat = function (obj) {
        return obj.locationLat;
    };
    NoteObj.setLocationLat = function (obj, locationLat) {
        obj.locationLat = locationLat;
        return obj;
    };
    NoteObj.getLocationLng = function (obj) {
        return obj.locationLng;
    };
    NoteObj.setLocationLng = function (obj, locationLng) {
        obj.locationLng = locationLng;
        return obj;
    };
    NoteObj.getLocationAddress = function (obj) {
        return obj.locationAddress;
    };
    NoteObj.setLocationAddress = function (obj, locationAddress) {
        obj.locationAddress = locationAddress;
        return obj;
    };
    NoteObj.isLocationExist = function (obj) {
        return obj.isLocationExist;
    };
    NoteObj.setLocationExist = function (obj, locationExist) {
        obj.isLocationExist = locationExist;
        return obj;
    };
    NoteObj.isReminderExist = function (obj) {
        return obj.reminderExist;
    };
    NoteObj.isTodoExist = function (obj) {
        return obj.todoExist;
    };
    NoteObj.getReminderLabel = function (obj) {
        return obj.reminderLabel;
    };
    NoteObj.getTodoCount = function (obj) {
        return obj.todoCount;
    };
    NoteObj.setReminderExist = function (obj, reminderExist) {
        obj.reminderExist = reminderExist;
        return obj;
    };
    NoteObj.setTodoExist = function (obj, todoExist) {
        obj.todoExist = todoExist;
        return obj;
    };
    NoteObj.setReminderLabel = function (obj, reminderLabel) {
        obj.reminderLabel = reminderLabel;
        obj = NoteObj.setReminderExist(obj, !!reminderLabel);
        return obj;
    };
    NoteObj.setTodoCount = function (obj, todoCount) {
        obj.todoCount = todoCount;
        obj = NoteObj.setTodoExist(obj, todoCount > 0);
        return obj;
    };
    NoteObj.getNotesListDate = function (obj, isDateAddedSelected) {
        return isDateAddedSelected ? obj.dateAdded : obj.dateUpdated;
    };
    NoteObj.getTagsAsString = function (obj) {
        var builder = "";
        var tagsList = NoteObj.getTagsList(obj);
        for (var i = 0; i < tagsList.length; i++) {
            builder += tagsList[i];
            if (i !== tagsList.length - 1) {
                builder += ", ";
            }
        }
        return builder;
    };
    NoteObj.getAttachmentsInListCount = function (obj) {
        return obj.attachmentsInListCount;
    };
    NoteObj.setAttachmentsInListCount = function (obj, attachmentsInListCount) {
        obj.attachmentsInListCount = attachmentsInListCount;
        obj = NoteObj.setAttachmentsInListExist(obj, attachmentsInListCount > 0);
        return obj;
    };
    NoteObj.isAttachmentsInListExist = function (obj) {
        return obj.attachmentsInListExist;
    };
    NoteObj.setAttachmentsInListExist = function (obj, attachmentsInListExist) {
        obj.attachmentsInListExist = attachmentsInListExist;
        return obj;
    };
    NoteObj.getText = function (obj) {
        var text = "";
        try {
            text = obj.shortText;
        }
        catch (e) {
            if (config_1.default.SHOW_WEB_CONSOLE) {
                console.log("NoteObj.prototype.getText problem: ", e);
            }
        }
        return text;
    };
    NoteObj.getTags = function (obj) {
        return obj.tags;
    };
    NoteObj.getTagsList = function (obj) {
        if (obj.tags) {
            return JSON.parse(obj.tags);
        }
        else {
            return [];
        }
    };
    NoteObj.getTagsArray = function (obj) {
        if (!obj.tags) {
            obj.tags = "";
        }
        return JSON.parse(obj.tags);
    };
    NoteObj.setTags = function (obj, tags) {
        if (!tags) {
            tags = [];
        }
        obj.tags = JSON.stringify(tags);
        return obj;
    };
    NoteObj.setText = function (obj, text) {
        obj.text = text;
        return obj;
    };
    NoteObj.prototype.getGlobalId = function () {
        return NoteObj.getGlobalId(this);
    };
    NoteObj.prototype.setGlobalId = function (globalId) {
        return NoteObj.setGlobalId(this, globalId);
    };
    NoteObj.prototype.setLocation = function (latitude, longitude) {
        return NoteObj.setLocation(this, latitude, longitude);
    };
    NoteObj.prototype.getLocationLat = function () {
        return NoteObj.getLocationLat(this);
    };
    NoteObj.prototype.setLocationLat = function (locationLat) {
        return NoteObj.setLocationLat(this, locationLat);
    };
    NoteObj.prototype.getLocationLng = function () {
        return NoteObj.getLocationLng(this);
    };
    NoteObj.prototype.setLocationLng = function (locationLng) {
        return NoteObj.setLocationLng(this, locationLng);
    };
    NoteObj.prototype.getLocationAddress = function () {
        return NoteObj.getLocationAddress(this);
    };
    NoteObj.prototype.setLocationAddress = function (locationAddress) {
        return NoteObj.setLocationAddress(this, locationAddress);
    };
    NoteObj.prototype.setLocationExist = function (locationExist) {
        return NoteObj.setLocationExist(this, locationExist);
    };
    NoteObj.prototype.isReminderExist = function () {
        return NoteObj.isReminderExist(this);
    };
    NoteObj.prototype.isTodoExist = function () {
        return NoteObj.isTodoExist(this);
    };
    NoteObj.prototype.getReminderLabel = function () {
        return NoteObj.getReminderLabel(this);
    };
    NoteObj.prototype.getTodoCount = function () {
        return NoteObj.getTodoCount(this);
    };
    NoteObj.prototype.setReminderExist = function (reminderExist) {
        return NoteObj.setReminderExist(this, reminderExist);
    };
    NoteObj.prototype.setTodoExist = function (todoExist) {
        return NoteObj.setTodoExist(this, todoExist);
    };
    NoteObj.prototype.setReminderLabel = function (reminderLabel) {
        return NoteObj.setReminderLabel(this, reminderLabel);
    };
    NoteObj.prototype.setTodoCount = function (todoCount) {
        return NoteObj.setTodoCount(this, todoCount);
    };
    NoteObj.prototype.getNotesListDate = function (isDateAddedSelected) {
        return NoteObj.getNotesListDate(this, isDateAddedSelected);
    };
    NoteObj.prototype.getTagsAsString = function () {
        return NoteObj.getTagsAsString(this);
    };
    NoteObj.prototype.getAttachmentsInListCount = function () {
        return NoteObj.getAttachmentsInListCount(this);
    };
    NoteObj.prototype.setAttachmentsInListCount = function (attachmentsInListCount) {
        return NoteObj.setAttachmentsInListCount(this, attachmentsInListCount);
    };
    NoteObj.prototype.isAttachmentsInListExist = function () {
        return NoteObj.isAttachmentsInListExist(this);
    };
    NoteObj.prototype.setAttachmentsInListExist = function (attachmentsInListExist) {
        return NoteObj.setAttachmentsInListExist(this, attachmentsInListExist);
    };
    NoteObj.prototype.getText = function () {
        return NoteObj.getText(this);
    };
    NoteObj.prototype.getTags = function () {
        return NoteObj.getTags(this);
    };
    NoteObj.prototype.getTagsList = function () {
        return NoteObj.getTagsList(this);
    };
    NoteObj.prototype.getTagsArray = function () {
        return NoteObj.getTagsArray(this);
    };
    NoteObj.prototype.setTags = function (tags) {
        return NoteObj.setTags(this, tags);
    };
    NoteObj.prototype.setText = function (text) {
        return NoteObj.setText(this, text);
    };
    NoteObj.ROLE_NOTE = "note";
    NoteObj.ROLE_CLIP = "clip";
    NoteObj.ROLE_WATCHES_TEXT = "watches_text";
    NoteObj.ROLE_WATCHES_AUDIO = "watches_audio";
    NoteObj.ROLE_PICTURE = "picture";
    NoteObj.ROLE_AUDIO = "audio";
    NoteObj.ROLE_VIDEO = "video";
    NoteObj.ROLE_TODO = "todo";
    NoteObj.ROLE_SCREENSHOT = "screenshot";
    NoteObj.ROLE_SCREENCAST = "screencast";
    NoteObj.ROLE_PDF = "pdf";
    NoteObj.ROLE_EDIT = "edit";
    return NoteObj;
}());
exports.default = NoteObj;
