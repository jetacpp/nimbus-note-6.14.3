"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TagObj = (function () {
    function TagObj() {
        this.title = "";
        this.oldTitle = "";
        this.dateAdded = 0;
        this.dateUpdated = 0;
        this.syncDate = 0;
        this.uniqueUserName = "";
        this.parentId = "";
        this.needSync = false;
        this.isChecked = false;
        this.notesCount = -1;
    }
    return TagObj;
}());
exports.default = TagObj;
