"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TodoObj = (function () {
    function TodoObj() {
        this.globalId = "";
        this.parentId = "";
        this.dateAdded = 0;
        this.syncDate = 0;
        this.needSync = false;
        this.label = "";
        this.checked = false;
        this.uniqueUserName = "";
    }
    return TodoObj;
}());
exports.default = TodoObj;
