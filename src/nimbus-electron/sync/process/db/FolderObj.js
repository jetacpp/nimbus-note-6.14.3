"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var AppConf_1 = __importDefault(require("../utils/AppConf"));
var FolderObj = (function () {
    function FolderObj() {
        this.globalId = "";
        this.parentId = "";
        this.rootParentId = "";
        this.index = 0;
        this.type = "";
        this.existOnServer = false;
        this.color = "";
        this.title = "";
        this.dateAdded = 0;
        this.dateUpdated = 0;
        this.syncDate = 0;
        this.uniqueUserName = "";
        this.onlyOffline = false;
        this.needSync = false;
        this.isMaybeInTrash = false;
        this.isClicked = false;
        this.subfoldersCount = -1;
        this.notesCount = -1;
        this.level = -1;
        this.shared = 0;
    }
    FolderObj.getTitle = function (obj, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        if (FolderObj.DEFAULT === obj.globalId && obj.title === FolderObj.DEFAULT_NAME) {
            return callback(null, FolderObj.DEFAULT_NAME);
        }
        callback(null, obj.title);
    };
    FolderObj.setTitle = function (obj, title) {
        if (FolderObj.DEFAULT === obj.globalId) {
            AppConf_1.default.setDefaultFolderTitle(title);
        }
        obj.title = title;
        return obj;
    };
    FolderObj.isValid = function (obj) {
        return true;
    };
    FolderObj.prototype.getTitle = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        FolderObj.getTitle(this, callback);
    };
    FolderObj.prototype.setTitle = function (title) {
        return FolderObj.setTitle(this, title);
    };
    FolderObj.prototype.isValid = function () {
        return FolderObj.isValid(this);
    };
    FolderObj.ROOT = "root";
    FolderObj.DEFAULT = "default";
    FolderObj.TRASH = "trash";
    FolderObj.GOD = "JESUS_CHRIST";
    FolderObj.ERASED_FROM_TRASH = "ERASED_FROM_TRASH";
    FolderObj.ALL_NOTES = "ALL_NOTES";
    FolderObj.DEFAULT_NAME = "My Notes";
    return FolderObj;
}());
exports.default = FolderObj;
