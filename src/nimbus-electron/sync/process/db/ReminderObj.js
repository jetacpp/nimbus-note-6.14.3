"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ReminderObj = (function () {
    function ReminderObj() {
        this.date = "";
        this.lat = 0;
        this.lng = 0;
        this.phone = "";
        this.interval = 0;
        this.priority = 2;
        this.remind_until = 0;
    }
    return ReminderObj;
}());
exports.default = ReminderObj;
