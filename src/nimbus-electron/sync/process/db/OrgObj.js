"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var OrgUsageEntity_1 = __importDefault(require("../../nimbussdk/net/response/entities/OrgUsageEntity"));
var OrgLimitsEntity_1 = __importDefault(require("../../nimbussdk/net/response/entities/OrgLimitsEntity"));
var OrgUserEntity_1 = __importDefault(require("../../nimbussdk/net/response/entities/OrgUserEntity"));
var OrgObj = (function () {
    function OrgObj() {
        this.id = '';
        this.type = '';
        this.serviceType = '';
        this.title = '';
        this.description = '';
        this.usage = new OrgUsageEntity_1.default();
        this.limits = new OrgLimitsEntity_1.default();
        this.features = [];
        this.user = new OrgUserEntity_1.default();
        this.sub = null;
        this.domain = null;
        this.suspended = false;
        this.suspendedAt = false;
        this.suspendedReason = null;
        this.access = null;
        this.smallLogoUrl = null;
        this.bigLogoUrl = null;
        this.syncDate = 0;
        this.needSync = false;
    }
    return OrgObj;
}());
exports.default = OrgObj;
