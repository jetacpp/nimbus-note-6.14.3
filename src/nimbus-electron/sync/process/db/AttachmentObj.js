"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AttachmentObj = (function () {
    function AttachmentObj() {
        this.globalId = "";
        this.parentId = "";
        this.dateAdded = 0;
        this.syncDate = 0;
        this.needSync = false;
        this.location = "";
        this.tempName = "";
        this.type = "";
        this.typeExtra = "";
        this.size = 0;
        this.extension = "";
        this.displayName = "";
        this.oldDisplayName = "";
        this.inList = 0;
        this.uniqueUserName = "";
        this.isAttachedToNote = false;
        this.fileUUID = "";
        this.isDownloaded = false;
        this.localPath = "";
        this.isDownloadingRunning = false;
        this.currentProgress = 0;
    }
    AttachmentObj.isDownloaded = function (obj) {
        return obj.isDownloaded;
    };
    AttachmentObj.setDownloaded = function (obj, downloaded) {
        obj.isDownloaded = downloaded;
        return obj;
    };
    AttachmentObj.getLocalPath = function (obj) {
        return obj.localPath;
    };
    AttachmentObj.setLocalPath = function (obj, localPath) {
        obj.localPath = localPath;
        obj = AttachmentObj.setDownloaded(obj, !!localPath);
        return obj;
    };
    AttachmentObj.getLocalPathWithFile = function (obj) {
        return "file://" + obj.localPath;
    };
    AttachmentObj.isValid = function (obj) {
        return true;
    };
    AttachmentObj.prototype.setDownloaded = function (downloaded) {
        return AttachmentObj.setDownloaded(this, downloaded);
    };
    AttachmentObj.prototype.getLocalPath = function () {
        return AttachmentObj.getLocalPath(this);
    };
    AttachmentObj.prototype.setLocalPath = function (localPath) {
        return AttachmentObj.setLocalPath(this, localPath);
    };
    AttachmentObj.prototype.getLocalPathWithFile = function () {
        return AttachmentObj.getLocalPathWithFile(this);
    };
    AttachmentObj.prototype.isValid = function () {
        return AttachmentObj.isValid(this);
    };
    AttachmentObj.TYPE_IMAGE = "image";
    AttachmentObj.TYPE_VIDEO = "video";
    AttachmentObj.TYPE_AUDIO = "audio";
    AttachmentObj.TYPE_DOC = "doc";
    AttachmentObj.TYPE_ARCHIVE = "archive";
    AttachmentObj.TYPE_FILE = "file";
    return AttachmentObj;
}());
exports.default = AttachmentObj;
