"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var xhrConnections = {};
var ConnectionCollector = (function () {
    function ConnectionCollector() {
    }
    ConnectionCollector.add = function (inputData) {
        var workspaceId = inputData.workspaceId, uniqueId = inputData.uniqueId, xhrConnection = inputData.xhrConnection;
        if (typeof (xhrConnections[workspaceId]) === 'undefined') {
            xhrConnections[workspaceId] = {};
        }
        if (uniqueId && xhrConnection) {
            var connectionsId = Object.keys(xhrConnections[workspaceId]);
            if (connectionsId.length >= ConnectionCollector.MAX_STORED_CONNECTIONS_PER_WORKSPACE) {
                if (typeof (xhrConnections[workspaceId][connectionsId[0]]) !== "undefined") {
                    delete xhrConnections[workspaceId][connectionsId[0]];
                }
            }
            xhrConnections[workspaceId][uniqueId] = xhrConnection;
        }
    };
    ConnectionCollector.getById = function (inputData) {
        var workspaceId = inputData.workspaceId, id = inputData.id;
        if (typeof (xhrConnections[workspaceId]) === 'undefined') {
            return null;
        }
        if (typeof (xhrConnections[workspaceId][id]) === "undefined") {
            return null;
        }
        return xhrConnections[workspaceId][id];
    };
    ConnectionCollector.get = function (inputData) {
        var workspaceId = inputData.workspaceId;
        return typeof (xhrConnections[workspaceId]) !== 'undefined' ? xhrConnections[workspaceId] : {};
    };
    ConnectionCollector.clearById = function (inputData) {
        var workspaceId = inputData.workspaceId, id = inputData.id;
        if (typeof (xhrConnections[workspaceId]) === 'undefined') {
            return;
        }
        if (typeof (xhrConnections[workspaceId][id]) === "undefined") {
            return;
        }
        delete xhrConnections[workspaceId][id];
    };
    ConnectionCollector.clear = function (inputData) {
        var workspaceId = inputData.workspaceId;
        xhrConnections[workspaceId] = {};
    };
    ConnectionCollector.clearAll = function () {
        xhrConnections = {};
    };
    ConnectionCollector.abortById = function (inputData) {
        var workspaceId = inputData.workspaceId, id = inputData.id;
        if (typeof (xhrConnections[workspaceId]) === 'undefined') {
            return;
        }
        if (typeof (xhrConnections[workspaceId][id]) === "undefined") {
            return;
        }
        xhrConnections[workspaceId][id].abort();
        ConnectionCollector.clearById(inputData);
    };
    ConnectionCollector.abort = function (inputData) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var workspaceId, id;
            return __generator(this, function (_a) {
                workspaceId = inputData.workspaceId;
                if (typeof (xhrConnections[workspaceId]) !== 'undefined') {
                    if (xhrConnections[workspaceId] && Object.keys(xhrConnections[workspaceId]).length) {
                        for (id in xhrConnections[workspaceId]) {
                            if (xhrConnections[workspaceId].hasOwnProperty(id)) {
                                xhrConnections[workspaceId][id].abort();
                                ConnectionCollector.clearById({ workspaceId: workspaceId, id: id });
                            }
                        }
                    }
                }
                ConnectionCollector.clear(inputData);
                resolve(true);
                return [2];
            });
        }); });
    };
    ConnectionCollector.MAX_STORED_CONNECTIONS_PER_WORKSPACE = 50;
    return ConnectionCollector;
}());
exports.default = ConnectionCollector;
