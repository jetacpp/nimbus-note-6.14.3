"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs-extra");
var electron_1 = require("electron");
var pdb_1 = __importDefault(require("../../../pdb"));
var page_1 = __importDefault(require("../../request/page"));
var instance_1 = __importDefault(require("../../window/instance"));
var SyncStatusChangedEvent_1 = __importDefault(require("../process/events/SyncStatusChangedEvent"));
var Translations_1 = __importDefault(require("../../translations/Translations"));
var NimbusSDK_1 = __importDefault(require("../nimbussdk/net/NimbusSDK"));
var SelectedWorkspace_1 = __importDefault(require("../../workspace/SelectedWorkspace"));
var state_1 = __importDefault(require("../../online/state"));
var config_1 = __importDefault(require("../../../config"));
var workspace_1 = __importDefault(require("../../db/models/workspace"));
var errorHandler_1 = __importDefault(require("../../utilities/errorHandler"));
var orgs_1 = __importDefault(require("../../db/models/orgs"));
var clearHandler_1 = require("../../utilities/clearHandler");
var exportHandler_1 = require("../../utilities/exportHandler");
var LocalCache = (function () {
    function LocalCache() {
    }
    LocalCache.requestClear = function () {
        var _this = this;
        var dialogOptions = {
            type: "warning",
            title: electron_1.app.name,
            message: "" + Translations_1.default.get('cache_clear__confirm_message'),
            detail: "" + Translations_1.default.get('cache_clear__confirm_details'),
            buttons: [
                "" + Translations_1.default.get('cache_clear__confirm_btn_cancel'),
                "" + Translations_1.default.get('cache_clear__confirm_btn_submit')
            ],
            defaultId: 1,
            cancelId: 0
        };
        if (instance_1.default.get()) {
            electron_1.dialog.showMessageBox(instance_1.default.get(), dialogOptions).then(function (_a) {
                var response = _a.response;
                return __awaiter(_this, void 0, void 0, function () {
                    var availableWorkspaceList, _i, availableWorkspaceList_1, workspaceId, attachmentPath, tempAttachmentPath, exportPath, dbPath, completeClearCache;
                    var _this = this;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (!response) return [3, 12];
                                return [4, workspace_1.default.getAvailableIdList()];
                            case 1:
                                availableWorkspaceList = _b.sent();
                                _i = 0, availableWorkspaceList_1 = availableWorkspaceList;
                                _b.label = 2;
                            case 2:
                                if (!(_i < availableWorkspaceList_1.length)) return [3, 5];
                                workspaceId = availableWorkspaceList_1[_i];
                                return [4, SyncStatusChangedEvent_1.default.setPauseStatus({ workspaceId: workspaceId })];
                            case 3:
                                _b.sent();
                                _b.label = 4;
                            case 4:
                                _i++;
                                return [3, 2];
                            case 5:
                                attachmentPath = pdb_1.default.getClientAttachmentPath();
                                if (!attachmentPath) return [3, 7];
                                return [4, clearHandler_1.clearDirectory(attachmentPath)];
                            case 6:
                                _b.sent();
                                _b.label = 7;
                            case 7:
                                tempAttachmentPath = pdb_1.default.getClientTempAttachmentPath();
                                if (tempAttachmentPath) {
                                    clearHandler_1.clearDirectory(tempAttachmentPath);
                                }
                                exportPath = pdb_1.default.getClientExportPath();
                                if (exportPath) {
                                    exportHandler_1.clearExportDir(exportPath);
                                }
                                dbPath = pdb_1.default.getClientDbPath();
                                if (!dbPath) return [3, 12];
                                return [4, errorHandler_1.default.clearSessionLogsStream()];
                            case 8:
                                _b.sent();
                                pdb_1.default.clearDbConnections();
                                return [4, LocalCache.clearWorkspaces(dbPath)];
                            case 9:
                                _b.sent();
                                if (!NimbusSDK_1.default.getAccountManager()) return [3, 12];
                                completeClearCache = function () {
                                    var jsCode = "\n                  if(window.localStorage) {\n                    for (let i = 0, len = window.localStorage.length; i < len; ++i) { \n                      let key = window.localStorage.key(i);\n                      if(key && (key.indexOf('nimbus-note-tree') === 0 || key.indexOf('nimbus-note-folders') === 0)) {\n                        window.localStorage.removeItem(key);\n                      }\n                    }\n                  }\n                ";
                                    instance_1.default.get().webContents.executeJavaScript(jsCode);
                                    page_1.default.reload();
                                    setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
                                        var workspaceId;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0: return [4, SelectedWorkspace_1.default.getGlobalId()];
                                                case 1:
                                                    workspaceId = _a.sent();
                                                    if (instance_1.default.get()) {
                                                        instance_1.default.get().webContents.send('event:client:autosync:response', { workspaceId: workspaceId });
                                                    }
                                                    return [2];
                                            }
                                        });
                                    }); }, 5000);
                                };
                                if (!state_1.default.get()) return [3, 11];
                                return [4, orgs_1.default.syncUserOrganizationsWorkspaces()];
                            case 10:
                                _b.sent();
                                _b.label = 11;
                            case 11:
                                completeClearCache();
                                _b.label = 12;
                            case 12: return [2];
                        }
                    });
                });
            });
        }
    };
    LocalCache.clearWorkspaces = function (dirPath) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var points, e_1, i, pointPath, pointStat, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        points = [];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, fs.readdir(dirPath)];
                    case 2:
                        points = _a.sent();
                        return [3, 4];
                    case 3:
                        e_1 = _a.sent();
                        return [2, resolve(false)];
                    case 4:
                        if (!points.length) {
                            return [2, resolve(false)];
                        }
                        i = 0;
                        _a.label = 5;
                    case 5:
                        if (!(i < points.length)) return [3, 15];
                        pointPath = dirPath + '/' + points[i].split('/')[0].split('\\')[0];
                        _a.label = 6;
                    case 6:
                        _a.trys.push([6, 13, , 14]);
                        return [4, fs.lstat(pointPath)];
                    case 7:
                        pointStat = _a.sent();
                        if (!pointStat.isFile()) return [3, 9];
                        return [4, fs.unlink(pointPath)];
                    case 8:
                        _a.sent();
                        return [3, 12];
                    case 9:
                        if (!pointStat.isDirectory()) return [3, 12];
                        return [4, clearHandler_1.clearDirectory(pointPath)];
                    case 10:
                        _a.sent();
                        return [4, fs.rmdir(pointPath)];
                    case 11:
                        _a.sent();
                        _a.label = 12;
                    case 12: return [3, 14];
                    case 13:
                        e_2 = _a.sent();
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("Error => LocalCache => clearWorkspaces => remove file or directory: ", pointPath, e_2);
                        }
                        return [3, 14];
                    case 14:
                        i++;
                        return [3, 5];
                    case 15: return [2, resolve(true)];
                }
            });
        }); });
    };
    return LocalCache;
}());
exports.default = LocalCache;
