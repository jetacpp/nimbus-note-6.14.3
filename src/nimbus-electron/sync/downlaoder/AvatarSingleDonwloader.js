"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_extra_1 = __importDefault(require("fs-extra"));
var AttachmentHubDownloader_1 = __importDefault(require("../process/hub/AttachmentHubDownloader"));
var instance_1 = __importDefault(require("../../window/instance"));
var pdb_1 = __importDefault(require("../../../pdb"));
var SyncAttachmentEntity_1 = __importDefault(require("../nimbussdk/net/response/entities/SyncAttachmentEntity"));
var config_runtime_1 = __importDefault(require("../../../config.runtime"));
var urlParser_1 = __importDefault(require("../../utilities/urlParser"));
var AvatarSingleDownloader = (function () {
    function AvatarSingleDownloader() {
    }
    AvatarSingleDownloader.download = function (inputData) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var url, oldUrl, type, storedFileUUID, targetPath, exists, entity, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        url = inputData.url, oldUrl = inputData.oldUrl, type = inputData.type;
                        if (!url) {
                            return [2, resolve({
                                    errorCode: AvatarSingleDownloader.ERROR_BAD_AVATAR_INPUT_DATA,
                                    complete: false,
                                    description: "Bad avatar url input data: " + url
                                })];
                        }
                        storedFileUUID = url.split("/").pop();
                        if (!storedFileUUID) {
                            return [2, resolve({
                                    errorCode: AvatarSingleDownloader.ERROR_BAD_AVATAR_INPUT_DATA,
                                    complete: false,
                                    description: "Bad avatar storedFileUUID input data: " + storedFileUUID
                                })];
                        }
                        targetPath = pdb_1.default.getClientAttachmentPath() + "/" + storedFileUUID;
                        return [4, fs_extra_1.default.exists(targetPath)];
                    case 1:
                        exists = _a.sent();
                        if (exists) {
                            return [2, resolve({
                                    errorCode: 0,
                                    isDownloaded: true,
                                    description: "Avatar already exist with this storedFileUUID: " + storedFileUUID
                                })];
                        }
                        entity = new SyncAttachmentEntity_1.default();
                        entity.file_uuid = storedFileUUID;
                        entity.location = url;
                        return [4, AttachmentHubDownloader_1.default.getInstance().download({ workspaceId: null, entity: entity })];
                    case 2:
                        result = _a.sent();
                        if (!result) {
                            return [2, resolve({
                                    errorCode: AvatarSingleDownloader.ERROR_AVATAR_DOWNLOAD_PROBLEM,
                                    complete: false,
                                    description: "Avatar download problem: " + url
                                })];
                        }
                        if (!(url !== oldUrl)) return [3, 4];
                        return [4, AvatarSingleDownloader.removeAvatarFile({ url: oldUrl })];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        if (type === AvatarSingleDownloader.AVATAR_TYPE_USER) {
                            AvatarSingleDownloader.socketUserAttachMessage({ avatar: "avatar/" + storedFileUUID });
                        }
                        resolve({
                            errorCode: 0,
                            complete: !!result.downloaded,
                            description: "Avatar download success: " + storedFileUUID,
                            response: result
                        });
                        return [2];
                }
            });
        }); });
    };
    AvatarSingleDownloader.checkAvatarExist = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var routeParams, storedFileUUID;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        routeParams = urlParser_1.default.getPathParams(url);
                        if (!routeParams.length) {
                            return [2, false];
                        }
                        storedFileUUID = typeof (routeParams[2] !== 'undefined') ? routeParams[2] : null;
                        if (storedFileUUID) {
                            return [2, false];
                        }
                        return [4, fs_extra_1.default.exists(pdb_1.default.getClientAttachmentPath() + "/" + storedFileUUID)];
                    case 1: return [2, _a.sent()];
                }
            });
        });
    };
    AvatarSingleDownloader.removeAvatarFile = function (avatar) {
        return __awaiter(this, void 0, void 0, function () {
            var url, storedPath, storedFileUUID, targetPath, fileExist, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!avatar) {
                            return [2];
                        }
                        url = avatar.url;
                        if (!url) {
                            return [2];
                        }
                        storedPath = pdb_1.default.getClientAttachmentPath();
                        if (!storedPath) {
                            return [2];
                        }
                        storedFileUUID = url.split("/").pop();
                        if (!storedFileUUID) {
                            return [2];
                        }
                        targetPath = storedPath + "/" + storedFileUUID;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, fs_extra_1.default.exists(targetPath)];
                    case 2:
                        fileExist = _a.sent();
                        if (fileExist) {
                            fs_extra_1.default.unlink(targetPath, function (err) {
                                if (err) {
                                    if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                        console.log(err);
                                    }
                                }
                            });
                        }
                        return [3, 4];
                    case 3:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    AvatarSingleDownloader.socketUserAttachMessage = function (inputData) {
        var avatar = inputData.avatar;
        if (avatar) {
            instance_1.default.get().webContents.send('event:client:me:name:update:response', { avatar: avatar });
        }
    };
    AvatarSingleDownloader.ERROR_BAD_AVATAR_INPUT_DATA = 1;
    AvatarSingleDownloader.ERROR_AVATAR_DOWNLOAD_PROBLEM = 2;
    AvatarSingleDownloader.AVATAR_TYPE_USER = 'user';
    AvatarSingleDownloader.AVATAR_TYPE_WORKSPACE = 'workspace';
    AvatarSingleDownloader.AVATAR_TYPE_ORGANIZATION = 'organization';
    return AvatarSingleDownloader;
}());
exports.default = AvatarSingleDownloader;
