"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_extra_1 = __importDefault(require("fs-extra"));
var AttachmentHubDownloader_1 = __importDefault(require("../process/hub/AttachmentHubDownloader"));
var socketFunctions_1 = __importDefault(require("../socket/socketFunctions"));
var item_1 = __importDefault(require("../../db/models/item"));
var attach_1 = __importDefault(require("../../db/models/attach"));
var instance_1 = __importDefault(require("../../window/instance"));
var pdb_1 = __importDefault(require("../../../pdb"));
var config_1 = __importDefault(require("../../../config"));
var AttachmentSingleDownloader = (function () {
    function AttachmentSingleDownloader() {
    }
    AttachmentSingleDownloader.download = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                        if (!globalId) {
                            return resolve({
                                errorCode: AttachmentSingleDownloader.ERROR_BAD_ATTACHMENT_INPUT_DATA,
                                complete: false,
                                description: "Bad attachment id input data: " + globalId
                            });
                        }
                        var queryData = { globalId: globalId };
                        attach_1.default.find(queryData, { workspaceId: workspaceId }, function (err, attachItem) { return __awaiter(_this, void 0, void 0, function () {
                            var findUserItem, itemInstance;
                            var _this = this;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!attachItem) {
                                            return [2, resolve({
                                                    errorCode: AttachmentSingleDownloader.ERROR_ATTACHMENT_NOT_FOUND_BY_ID,
                                                    complete: false,
                                                    description: "Attachment not found by id: " + globalId
                                                })];
                                        }
                                        findUserItem = function (inputData) {
                                            return new Promise(function (resolve) {
                                                var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                                                item_1.default.find({ globalId: globalId }, { workspaceId: workspaceId }, function (err, itemInstance) {
                                                    if (err || !itemInstance) {
                                                        return resolve(null);
                                                    }
                                                    resolve(itemInstance);
                                                });
                                            });
                                        };
                                        return [4, findUserItem({ workspaceId: workspaceId, globalId: attachItem.noteGlobalId })];
                                    case 1:
                                        itemInstance = _a.sent();
                                        if (itemInstance.offlineOnly) {
                                            AttachmentSingleDownloader.socketUserAttachMessage({ workspaceId: workspaceId, attachItem: attachItem });
                                            return [2, resolve({
                                                    errorCode: AttachmentSingleDownloader.ERROR_ATTACHMENT_FOR_OFFILNE_NOTE,
                                                    complete: true,
                                                    description: "Attachment exist for offline note: " + globalId
                                                })];
                                        }
                                        attach_1.default.update(queryData, {
                                            displayName: attachItem.displayName,
                                            isDownloaded: false
                                        }, { workspaceId: workspaceId }, function () { return __awaiter(_this, void 0, void 0, function () {
                                            var targetPath, exists, entity, e_1, result;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        targetPath = pdb_1.default.getClientAttachmentPath() + "/" + attachItem.storedFileUUID;
                                                        return [4, fs_extra_1.default.exists(targetPath)];
                                                    case 1:
                                                        exists = _a.sent();
                                                        return [4, AttachmentHubDownloader_1.default.getUserAttachmentEntity({ workspaceId: workspaceId, globalId: globalId })];
                                                    case 2:
                                                        entity = _a.sent();
                                                        if (!(exists && entity.location)) return [3, 7];
                                                        return [4, fs_extra_1.default.exists(targetPath)];
                                                    case 3:
                                                        if (!_a.sent()) return [3, 7];
                                                        _a.label = 4;
                                                    case 4:
                                                        _a.trys.push([4, 6, , 7]);
                                                        return [4, fs_extra_1.default.unlink(targetPath)];
                                                    case 5:
                                                        _a.sent();
                                                        return [3, 7];
                                                    case 6:
                                                        e_1 = _a.sent();
                                                        if (config_1.default.SHOW_WEB_CONSOLE) {
                                                            console.log("Error => AttachmentSingleDownloader => remove: ", targetPath, e_1);
                                                        }
                                                        return [3, 7];
                                                    case 7:
                                                        if (!entity) {
                                                            AttachmentSingleDownloader.socketUserAttachMessage({ workspaceId: workspaceId, attachItem: attachItem });
                                                            return [2, resolve({
                                                                    errorCode: AttachmentSingleDownloader.ERROR_ATTACHMENT_NOT_FOUND_BY_ID,
                                                                    complete: false,
                                                                    description: "Attachment not found by id: " + globalId
                                                                })];
                                                        }
                                                        return [4, AttachmentHubDownloader_1.default.getInstance().download({ workspaceId: workspaceId, entity: entity })];
                                                    case 8:
                                                        result = _a.sent();
                                                        if (!result) {
                                                            AttachmentSingleDownloader.socketUserAttachMessage({ workspaceId: workspaceId, attachItem: attachItem });
                                                            return [2, resolve({
                                                                    errorCode: AttachmentSingleDownloader.ERROR_ATTACHMENT_DOWNLOAD_PROBLEM,
                                                                    complete: false,
                                                                    description: "Attachment download problem: " + globalId
                                                                })];
                                                        }
                                                        attach_1.default.update(queryData, {
                                                            displayName: attachItem.displayName,
                                                            isDownloaded: !!result.downloaded
                                                        }, { workspaceId: workspaceId }, function () {
                                                            AttachmentSingleDownloader.socketUserAttachMessage({ workspaceId: workspaceId, attachItem: attachItem });
                                                        });
                                                        resolve({
                                                            errorCode: 0,
                                                            complete: !!result.downloaded,
                                                            description: "Attachment download success: " + globalId,
                                                            response: result
                                                        });
                                                        return [2];
                                                }
                                            });
                                        }); });
                                        return [2];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    AttachmentSingleDownloader.socketUserAttachMessage = function (inputData) {
        var workspaceId = inputData.workspaceId, attachItem = inputData.attachItem;
        if (attachItem) {
            if (attachItem.inList) {
                socketFunctions_1.default.sendNoteAttachmentsUpdateMessage({
                    workspaceId: workspaceId,
                    noteGlobalId: attachItem.noteGlobalId,
                    globalId: attachItem.globalId
                });
            }
            else {
                if (instance_1.default.get()) {
                    instance_1.default.get().webContents.send('event:client:update:inline:attach:response', {
                        noteGlobalId: attachItem.noteGlobalId,
                        globalId: attachItem.globalId
                    });
                }
            }
        }
    };
    AttachmentSingleDownloader.cancel = function (inputData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                        if (!globalId) {
                            return resolve({
                                errorCode: AttachmentSingleDownloader.ERROR_BAD_ATTACHMENT_INPUT_DATA,
                                complete: false,
                                description: "Bad attachment id input data: " + globalId
                            });
                        }
                    })];
            });
        });
    };
    AttachmentSingleDownloader.ERROR_BAD_ATTACHMENT_INPUT_DATA = 1;
    AttachmentSingleDownloader.ERROR_ATTACHMENT_NOT_FOUND_BY_ID = 2;
    AttachmentSingleDownloader.ERROR_ATTACHMENT_DOWNLOAD_PROBLEM = 3;
    AttachmentSingleDownloader.ERROR_ATTACHMENT_FOR_OFFILNE_NOTE = 4;
    return AttachmentSingleDownloader;
}());
exports.default = AttachmentSingleDownloader;
