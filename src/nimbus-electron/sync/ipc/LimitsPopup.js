"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var instance_1 = __importDefault(require("../../window/instance"));
var auth_1 = __importDefault(require("../../auth/auth"));
var user_1 = __importDefault(require("../../db/models/user"));
var workspace_1 = __importDefault(require("../../db/models/workspace"));
var SelectedWorkspace_1 = __importDefault(require("../../workspace/SelectedWorkspace"));
var NimbusSDK_1 = __importDefault(require("../nimbussdk/net/NimbusSDK"));
var LimitsPopup = (function () {
    function LimitsPopup() {
    }
    LimitsPopup.show = function (inputData) {
        var data = inputData.data;
        var isNotice = data.objectSize < data.limitSize;
        var isPremium = data.isPremium;
        var isUserWorkspace = data.isUserWorkspace;
        if (instance_1.default.get()) {
            instance_1.default.get().webContents.send('event:client:popup:limits:show:response', {
                limitSize: data.limitSize,
                name: LimitsPopup.NOTICE_TYPE_LIMIT_ERROR,
                objectSize: data.objectSize,
                objectType: LimitsPopup.NOTICE_OBJECT_ACCOUNT,
                isPremium: isPremium,
                premiumSize: data.premiumSize,
                isNotice: isNotice,
                isUserWorkspace: isUserWorkspace,
                errorType: isNotice ? 'NoticeText' : 'ErrorText',
                errorMessage: isPremium ? 'ProText' : 'FreeText'
            });
        }
    };
    LimitsPopup.check = function (inputData) {
        var _this = this;
        var workspaceId = inputData.workspaceId, manualSync = inputData.manualSync, data = inputData.data;
        auth_1.default.getUser(function (err, authInfo) {
            if (authInfo && Object.keys(authInfo).length) {
                var findQuery_1 = { userId: authInfo.userId };
                user_1.default.find(findQuery_1, {}, function (err, userItem) { return __awaiter(_this, void 0, void 0, function () {
                    var isUserWorkspace, isPremium, workspaceInstance, errorData, _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (err || !userItem) {
                                    return [2];
                                }
                                if (!!workspaceId) return [3, 1];
                                isUserWorkspace = true;
                                isPremium = data.isPremiumActive;
                                return [3, 7];
                            case 1: return [4, workspace_1.default.getById(workspaceId)];
                            case 2:
                                workspaceInstance = _b.sent();
                                if (!!workspaceInstance) return [3, 3];
                                isUserWorkspace = true;
                                isPremium = data.isPremiumActive;
                                return [3, 7];
                            case 3: return [4, workspace_1.default.isUserWorkspace(workspaceInstance)];
                            case 4:
                                isUserWorkspace = _b.sent();
                                if (!isUserWorkspace) return [3, 5];
                                isPremium = data.isPremiumActive;
                                return [3, 7];
                            case 5: return [4, NimbusSDK_1.default.getAccountManager().isPremiumActiveAsync()];
                            case 6:
                                isPremium = _b.sent();
                                _b.label = 7;
                            case 7:
                                _a = {
                                    objectSize: data.objectSize,
                                    limitSize: data.limitSize
                                };
                                return [4, NimbusSDK_1.default.getAccountManager().getTrafficMaxAsync()];
                            case 8:
                                errorData = (_a.premiumSize = _b.sent(),
                                    _a.isPremium = isPremium,
                                    _a.isUserWorkspace = isUserWorkspace,
                                    _a);
                                if (!(errorData.objectSize >= errorData.limitSize)) return [3, 10];
                                return [4, SelectedWorkspace_1.default.getGlobalId()];
                            case 9:
                                if ((_b.sent()) === workspaceId) {
                                    if (manualSync) {
                                        LimitsPopup.show({ workspaceId: workspaceId, data: errorData });
                                    }
                                    else {
                                        if (!userItem.popupLimitError) {
                                            LimitsPopup.show({ workspaceId: workspaceId, data: errorData });
                                        }
                                    }
                                    return [2, user_1.default.update(findQuery_1, { popupLimitError: true, popupLimitNotice: true }, {}, function () {
                                        })];
                                }
                                _b.label = 10;
                            case 10:
                                if (errorData.isPremium) {
                                    return [2];
                                }
                                if (!!userItem.popupLimitNotice) return [3, 12];
                                if (!(errorData.objectSize >= user_1.default.WARNING_UPLOAD_SIZE)) return [3, 12];
                                return [4, SelectedWorkspace_1.default.getGlobalId()];
                            case 11:
                                if ((_b.sent()) === workspaceId) {
                                    LimitsPopup.show({ workspaceId: workspaceId, data: errorData });
                                    return [2, user_1.default.update(findQuery_1, { popupLimitNotice: true }, {}, function () {
                                        })];
                                }
                                _b.label = 12;
                            case 12: return [2];
                        }
                    });
                }); });
            }
        });
    };
    LimitsPopup.NOTICE_TYPE_LIMIT_ERROR = "LimitError";
    LimitsPopup.NOTICE_OBJECT_ACCOUNT = "Account";
    return LimitsPopup;
}());
exports.default = LimitsPopup;
