"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var instance_1 = __importDefault(require("../../window/instance"));
var AutoSyncRequest = (function () {
    function AutoSyncRequest() {
    }
    AutoSyncRequest.requestAutoSync = function (inputData) {
        if (instance_1.default.get()) {
            instance_1.default.get().webContents.send('event:client:autosync:request', inputData);
        }
    };
    return AutoSyncRequest;
}());
exports.default = AutoSyncRequest;
