"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs-extra");
var electron_1 = require("electron");
var config_runtime_1 = __importDefault(require("../../config.runtime"));
var instance_1 = __importDefault(require("../window/instance"));
var printWin;
var Print = (function () {
    function Print() {
    }
    Print.listenPrint = function () {
        electron_1.ipcMain.on('event:print', function (event, arg) {
            if (printWin) {
                printWin.close();
            }
            if (arg && arg.template) {
                var tempFilePath_1 = config_runtime_1.default.clientDataPath + "/print.html";
                try {
                    fs.writeFile(tempFilePath_1, arg.template, function (err) {
                        if (!err) {
                            var title = arg.title || "Nimbus Note";
                            var width = arg.width || 600;
                            var height = arg.height || 600;
                            printWin = new electron_1.BrowserWindow({
                                show: false,
                                width: width,
                                height: height,
                                title: title,
                                parent: instance_1.default.get() || null
                            });
                            printWin.webContents.on('did-finish-load', function () {
                                printWin.webContents.executeJavaScript("window.print()");
                                setTimeout(function () {
                                    try {
                                        fs.writeFile(tempFilePath_1, '', function (err) {
                                            printWin = null;
                                        });
                                    }
                                    catch (e) {
                                        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                            console.log("Problem: print => clear print file => did-finish-load", e);
                                        }
                                    }
                                }, 1000);
                            });
                            printWin.once('close', function () {
                                try {
                                    fs.writeFile(tempFilePath_1, '', function (err) {
                                        printWin = null;
                                    });
                                }
                                catch (e) {
                                    if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                        console.log("Problem: print => clear print file => close", e);
                                    }
                                }
                            });
                            printWin.loadURL("file://" + tempFilePath_1);
                        }
                        else {
                            if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                console.log(err);
                            }
                        }
                    });
                }
                catch (e) {
                    if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                        console.log("Problem: print => to save print file", e);
                    }
                }
            }
        });
    };
    return Print;
}());
exports.default = Print;
