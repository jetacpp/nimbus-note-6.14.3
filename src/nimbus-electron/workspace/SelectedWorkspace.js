"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var workspace_1 = __importDefault(require("../db/models/workspace"));
var userSettings_1 = __importDefault(require("../db/models/userSettings"));
var instance_1 = __importDefault(require("../window/instance"));
var AutoSyncManager_1 = __importDefault(require("../sync/nimbussdk/manager/AutoSyncManager"));
var pdb_1 = __importDefault(require("../../pdb"));
var user_1 = __importDefault(require("../db/models/user"));
var SelectedOrganization_1 = __importDefault(require("../organization/SelectedOrganization"));
var auth_1 = __importDefault(require("../auth/auth"));
var selectedWorkspaceId;
var SelectedWorkspace = (function () {
    function SelectedWorkspace() {
    }
    SelectedWorkspace.getGlobalId = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(typeof (selectedWorkspaceId) === 'undefined')) return [3, 2];
                        return [4, SelectedWorkspace.get()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!selectedWorkspaceId) {
                            return [2, null];
                        }
                        return [2, selectedWorkspaceId];
                }
            });
        });
    };
    SelectedWorkspace.setGlobalId = function (globalId) {
        selectedWorkspaceId = globalId;
    };
    SelectedWorkspace.get = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        userSettings_1.default.get(SelectedWorkspace.SETTINGS_KEY, function (err, res) { return __awaiter(_this, void 0, void 0, function () {
                            var defaultWorkspaceInstance;
                            var _this = this;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!(err || !res)) return [3, 2];
                                        return [4, workspace_1.default.getDefault()];
                                    case 1:
                                        defaultWorkspaceInstance = _a.sent();
                                        if (typeof (selectedWorkspaceId) === 'undefined') {
                                            selectedWorkspaceId = null;
                                        }
                                        return [2, resolve(defaultWorkspaceInstance)];
                                    case 2:
                                        workspace_1.default.find({ globalId: res }, {}, function (err, workspaceInstance) { return __awaiter(_this, void 0, void 0, function () {
                                            var defaultWorkspaceInstance;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        if (!(err || !workspaceInstance)) return [3, 2];
                                                        return [4, workspace_1.default.getDefault()];
                                                    case 1:
                                                        defaultWorkspaceInstance = _a.sent();
                                                        if (typeof (selectedWorkspaceId) === 'undefined') {
                                                            selectedWorkspaceId = null;
                                                        }
                                                        return [2, resolve(defaultWorkspaceInstance)];
                                                    case 2: return [4, workspace_1.default.isMainForUser(workspaceInstance)];
                                                    case 3:
                                                        if (_a.sent()) {
                                                            if (typeof (selectedWorkspaceId) === 'undefined') {
                                                                selectedWorkspaceId = null;
                                                            }
                                                        }
                                                        else {
                                                            if (typeof (selectedWorkspaceId) === 'undefined') {
                                                                selectedWorkspaceId = workspaceInstance.globalId;
                                                            }
                                                        }
                                                        resolve(workspaceInstance);
                                                        return [2];
                                                }
                                            });
                                        }); });
                                        return [2];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    SelectedWorkspace.set = function (workspaceGlobalId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var prevSelectedWorkspaceId, updateWorkspaceStatus, updateWorkspaceSyncStatus;
                        var _this = this;
                        return __generator(this, function (_a) {
                            prevSelectedWorkspaceId = selectedWorkspaceId;
                            if (workspaceGlobalId === workspace_1.default.DEFAULT_NAME) {
                                workspaceGlobalId = null;
                            }
                            updateWorkspaceStatus = function (inputData) {
                                var workspaceId = inputData.workspaceId;
                                setTimeout(function () {
                                    if (instance_1.default.get()) {
                                        instance_1.default.get().webContents.send('event:client:update:workspace:sync:status:response', { workspaceId: workspaceId });
                                    }
                                }, 3000);
                                updateWorkspaceSyncStatus(inputData);
                            };
                            updateWorkspaceSyncStatus = function (inputData) {
                                var workspaceId = inputData.workspaceId, prevWorkspaceId = inputData.prevWorkspaceId;
                                AutoSyncManager_1.default.clearTimer({ prevWorkspaceId: prevWorkspaceId });
                                AutoSyncManager_1.default.initTimer({
                                    workspaceId: workspaceId,
                                    initState: true
                                });
                            };
                            if (!workspaceGlobalId) {
                                if (prevSelectedWorkspaceId === workspaceGlobalId) {
                                    return [2, resolve(prevSelectedWorkspaceId)];
                                }
                                selectedWorkspaceId = null;
                                updateWorkspaceStatus({
                                    prevWorkspaceId: prevSelectedWorkspaceId,
                                    workspaceId: selectedWorkspaceId
                                });
                                return [2, resolve(selectedWorkspaceId)];
                            }
                            workspace_1.default.find({ globalId: workspaceGlobalId }, {}, function (err, workspaceInstance) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (err || !workspaceInstance) {
                                                workspaceGlobalId = null;
                                            }
                                            return [4, workspace_1.default.isMainForUser(workspaceInstance)];
                                        case 1:
                                            if (_a.sent()) {
                                                workspaceGlobalId = null;
                                            }
                                            if (prevSelectedWorkspaceId === workspaceGlobalId) {
                                                return [2, resolve(prevSelectedWorkspaceId)];
                                            }
                                            selectedWorkspaceId = workspaceGlobalId;
                                            if (selectedWorkspaceId) {
                                                pdb_1.default.setWorkspaceDbPath(selectedWorkspaceId);
                                            }
                                            updateWorkspaceStatus({
                                                prevWorkspaceId: prevSelectedWorkspaceId,
                                                workspaceId: selectedWorkspaceId
                                            });
                                            userSettings_1.default.set(SelectedWorkspace.SETTINGS_KEY, selectedWorkspaceId, function () { return __awaiter(_this, void 0, void 0, function () {
                                                return __generator(this, function (_a) {
                                                    resolve(selectedWorkspaceId);
                                                    return [2];
                                                });
                                            }); });
                                            return [2];
                                    }
                                });
                            }); });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    SelectedWorkspace.getUserWorkspaceIdForOrganization = function (organization, workspaceId) {
        if (workspaceId === void 0) { workspaceId = null; }
        return __awaiter(this, void 0, void 0, function () {
            var selectedWorkspace, authInfo, userVariables, lastOpenedWorkspaces;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, auth_1.default.fetchActualUserAsync()];
                    case 1:
                        authInfo = _a.sent();
                        userVariables = authInfo && authInfo.variables ? authInfo.variables : user_1.default.getDefaultVariables();
                        lastOpenedWorkspaces = userVariables && userVariables.lastOpenedWorkspaces ? userVariables.lastOpenedWorkspaces : {};
                        if (!(organization && organization.globalId)) return [3, 8];
                        if (!(typeof (lastOpenedWorkspaces[organization.globalId]) !== 'undefined' && lastOpenedWorkspaces[organization.globalId])) return [3, 3];
                        return [4, workspace_1.default.getById(lastOpenedWorkspaces[organization.globalId])];
                    case 2:
                        selectedWorkspace = (_a.sent());
                        return [3, 7];
                    case 3:
                        if (!(typeof (lastOpenedWorkspaces['default']) !== 'undefined' && lastOpenedWorkspaces['default'])) return [3, 5];
                        return [4, workspace_1.default.getById(lastOpenedWorkspaces['default'])];
                    case 4:
                        selectedWorkspace = (_a.sent());
                        return [3, 7];
                    case 5: return [4, workspace_1.default.findUserWorkspaceByOrgId(organization.globalId)];
                    case 6:
                        selectedWorkspace = (_a.sent());
                        _a.label = 7;
                    case 7: return [3, 12];
                    case 8:
                        if (!(typeof (lastOpenedWorkspaces['default']) !== 'undefined' && lastOpenedWorkspaces['default'])) return [3, 10];
                        return [4, workspace_1.default.getById(lastOpenedWorkspaces['default'])];
                    case 9:
                        selectedWorkspace = (_a.sent());
                        return [3, 12];
                    case 10: return [4, workspace_1.default.getWithQuery({ isDefault: true })];
                    case 11:
                        selectedWorkspace = (_a.sent());
                        _a.label = 12;
                    case 12: return [2, selectedWorkspace ? selectedWorkspace.globalId : null];
                }
            });
        });
    };
    SelectedWorkspace.setUserWorkspaceForSelectedOrganization = function (userInfo, workspaceId) {
        if (workspaceId === void 0) { workspaceId = null; }
        return __awaiter(this, void 0, void 0, function () {
            var accountUserId, activeOrganization, activeWorkspaceId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, user_1.default.getAccountUserId()];
                    case 1:
                        accountUserId = _a.sent();
                        return [4, SelectedOrganization_1.default.getCurrentOrganization(userInfo)];
                    case 2:
                        activeOrganization = _a.sent();
                        if (!activeOrganization) {
                            activeOrganization = { globalId: null, userId: accountUserId };
                        }
                        return [4, SelectedWorkspace.getUserWorkspaceIdForOrganization(activeOrganization, workspaceId)];
                    case 3:
                        activeWorkspaceId = _a.sent();
                        return [4, SelectedWorkspace.set(activeWorkspaceId)];
                    case 4:
                        _a.sent();
                        return [2];
                }
            });
        });
    };
    SelectedWorkspace.SETTINGS_KEY = "selected_workspace";
    return SelectedWorkspace;
}());
exports.default = SelectedWorkspace;
