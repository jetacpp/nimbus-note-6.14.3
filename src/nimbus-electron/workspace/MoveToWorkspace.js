"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var workspace_1 = __importDefault(require("../db/models/workspace"));
var item_1 = __importDefault(require("../db/models/item"));
var NimbusSDK_1 = __importDefault(require("../sync/nimbussdk/net/NimbusSDK"));
var Translations_1 = __importDefault(require("../translations/Translations"));
var API_1 = __importDefault(require("../sync/nimbussdk/net/API"));
var NOTE_OF_FOLDER_IS_OFFLINE = 1;
var NOTE_OF_FOLDER_IS_ENCRYPTED = 2;
var NOTE_OF_FOLDER_IS_NOT_SYNC = 3;
var NOTE_OF_FOLDER_WORKSPACE_DOES_NOT_EXIST = 4;
var MoveToWorkspace = (function () {
    function MoveToWorkspace() {
    }
    MoveToWorkspace.copy = function (inputData) {
        return new Promise(function (resolve) {
            var fromWorkspaceInstance = inputData.fromWorkspaceInstance, toWorkspaceInstance = inputData.toWorkspaceInstance, itemInstance = inputData.itemInstance, removeSourceItem = inputData.removeSourceItem;
            NimbusSDK_1.default.getApi().moveToWorkspace({
                workspaceId: fromWorkspaceInstance.globalId,
                body: {
                    workspaceId: fromWorkspaceInstance.globalId,
                    globalId: itemInstance.globalId,
                    toWorkspaceId: toWorkspaceInstance.globalId,
                    remove: removeSourceItem
                },
                skipSyncHandlers: true
            }, function (err, res) {
                var message = "";
                if (err) {
                    if (err === API_1.default.ERROR_QUOTA_EXCEED || err === API_1.default.ERROR_ACCESS_DENIED) {
                        if (res) {
                            if (res._errorDesc === 'attachment_too_large') {
                                if (itemInstance.type === 'folder') {
                                    message += Translations_1.default.get('toast__can_not_move_to_workspace__quota_exceed_folder_attachments');
                                }
                                else {
                                    message += Translations_1.default.get('toast__can_not_move_to_workspace__quota_exceed_note_attachment');
                                }
                                if (removeSourceItem) {
                                    message += " " + Translations_1.default.get('toast__can_not_move_to_workspace__quota_exceed_attachments_user');
                                }
                                else {
                                    message += " " + Translations_1.default.get('toast__can_not_move_to_workspace__quota_exceed_attachments_owner');
                                }
                                if (message) {
                                    res._errorDesc = message;
                                }
                            }
                        }
                    }
                    return resolve({
                        err: err,
                        errorDesc: res && res._errorDesc ? res._errorDesc : null,
                        newGlobalId: null,
                        isElectron: !!message
                    });
                }
                return resolve({
                    err: err,
                    errorDesc: res && res._errorDesc ? res._errorDesc : null,
                    newGlobalId: res.newGlobalId,
                    remove: removeSourceItem
                });
            });
        });
    };
    MoveToWorkspace.process = function (inputData) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var fromWorkspaceId, toWorkspaceId, globalId, isMove, fromWorkspaceInstance, _a, toWorkspaceInstance, _b, findUserItem, itemInstance, result;
            var _this = this;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        fromWorkspaceId = inputData.fromWorkspaceId, toWorkspaceId = inputData.toWorkspaceId, globalId = inputData.globalId, isMove = inputData.isMove;
                        if (!fromWorkspaceId) return [3, 2];
                        return [4, workspace_1.default.getById(fromWorkspaceId)];
                    case 1:
                        _a = _c.sent();
                        return [3, 4];
                    case 2: return [4, workspace_1.default.getDefault()];
                    case 3:
                        _a = _c.sent();
                        _c.label = 4;
                    case 4:
                        fromWorkspaceInstance = _a;
                        if (!toWorkspaceId) return [3, 6];
                        return [4, workspace_1.default.getById(toWorkspaceId)];
                    case 5:
                        _b = _c.sent();
                        return [3, 8];
                    case 6: return [4, workspace_1.default.getDefault()];
                    case 7:
                        _b = _c.sent();
                        _c.label = 8;
                    case 8:
                        toWorkspaceInstance = _b;
                        findUserItem = function (inputData) {
                            return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                var globalId, fromWorkspaceInstance, workspaceId, _a;
                                return __generator(this, function (_b) {
                                    switch (_b.label) {
                                        case 0:
                                            globalId = inputData.globalId, fromWorkspaceInstance = inputData.fromWorkspaceInstance;
                                            if (!fromWorkspaceInstance.globalId) return [3, 2];
                                            return [4, workspace_1.default.getLocalId(fromWorkspaceInstance.globalId)];
                                        case 1:
                                            _a = _b.sent();
                                            return [3, 3];
                                        case 2:
                                            _a = null;
                                            _b.label = 3;
                                        case 3:
                                            workspaceId = _a;
                                            item_1.default.find({ globalId: globalId }, { workspaceId: workspaceId }, function (err, itemInstance) {
                                                if (err || !itemInstance) {
                                                    return resolve(null);
                                                }
                                                return resolve(itemInstance);
                                            });
                                            return [2];
                                    }
                                });
                            }); });
                        };
                        return [4, findUserItem({ globalId: globalId, fromWorkspaceInstance: fromWorkspaceInstance })];
                    case 9:
                        itemInstance = _c.sent();
                        if (!itemInstance) {
                            return [2, resolve({
                                    err: NOTE_OF_FOLDER_WORKSPACE_DOES_NOT_EXIST,
                                    errorDesc: Translations_1.default.get('toast__note_folder_source_workspace_not_exist'),
                                    newGlobalId: null,
                                    isElectron: true,
                                })];
                        }
                        if (itemInstance.offlineOnly) {
                            return [2, resolve({
                                    err: NOTE_OF_FOLDER_IS_OFFLINE,
                                    errorDesc: Translations_1.default.get('toast__can_not_move_to_workspace__item_offline'),
                                    newGlobalId: null,
                                    isElectron: true,
                                })];
                        }
                        if (itemInstance.isEncrypted) {
                            return [2, resolve({
                                    err: NOTE_OF_FOLDER_IS_ENCRYPTED,
                                    errorDesc: 'contains_encrypted_notes',
                                    newGlobalId: null,
                                    isElectron: true,
                                })];
                        }
                        if (itemInstance.needSync) {
                            return [2, resolve({
                                    err: NOTE_OF_FOLDER_IS_NOT_SYNC,
                                    errorDesc: Translations_1.default.get('toast__can_not_move_to_workspace__item_not_sync'),
                                    newGlobalId: null,
                                    isElectron: true,
                                })];
                        }
                        return [4, MoveToWorkspace.copy({
                                fromWorkspaceInstance: fromWorkspaceInstance,
                                toWorkspaceInstance: toWorkspaceInstance,
                                itemInstance: itemInstance,
                                removeSourceItem: isMove
                            })];
                    case 10:
                        result = _c.sent();
                        return [2, resolve(result)];
                }
            });
        }); });
    };
    return MoveToWorkspace;
}());
exports.default = MoveToWorkspace;
