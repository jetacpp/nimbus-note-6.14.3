"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var page_1 = __importDefault(require("../request/page"));
var instance_1 = __importDefault(require("../window/instance"));
var LocalCache_1 = __importDefault(require("../sync/cache/LocalCache"));
var Translations_1 = __importDefault(require("../translations/Translations"));
var errorHandler_1 = __importDefault(require("../utilities/errorHandler"));
var SelectedWorkspace_1 = __importDefault(require("../workspace/SelectedWorkspace"));
var About_1 = __importDefault(require("../popup/About"));
var apiMeLogout_1 = __importDefault(require("../request/interceptors/jsonInterceptors/me/apiMeLogout"));
var helpHandler_1 = require("../utilities/helpHandler");
var syncHandler_1 = __importDefault(require("../utilities/syncHandler"));
var ApplicationMenu = (function () {
    function ApplicationMenu() {
    }
    ApplicationMenu.add = function () {
        var template = [
            {
                label: "" + Translations_1.default.get('menu_edit'),
                submenu: [
                    {
                        label: "" + Translations_1.default.get('menu_edit__undo'),
                        role: 'undo'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_edit__redo'),
                        role: 'redo'
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_edit__cut'),
                        role: 'cut'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_edit__copy'),
                        role: 'copy'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_edit__paste'),
                        role: 'paste'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_edit__pasteandmatchstyle'),
                        role: 'pasteandmatchstyle'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_edit__delete'),
                        role: 'delete'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_edit__selectall'),
                        role: 'selectall'
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_edit__add_note'),
                        click: function () {
                            if (instance_1.default.get()) {
                                instance_1.default.get().webContents.send('event:client:add:note:response');
                            }
                        },
                        accelerator: "Alt+N"
                    },
                    {
                        label: "" + Translations_1.default.get('menu_edit__find_notes'),
                        click: function () {
                            if (instance_1.default.get()) {
                                instance_1.default.get().webContents.send('event:client:find:notes:response');
                            }
                        },
                        accelerator: "CommandOrControl+Alt+F"
                    }
                ]
            },
            {
                label: "" + Translations_1.default.get('menu_view'),
                submenu: [
                    {
                        role: 'toggledevtools'
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_view__reload'),
                        click: function () {
                            page_1.default.reload(true);
                        },
                        accelerator: "CommandOrControl+Alt+R"
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_view__togglefullscreen'),
                        role: 'togglefullscreen'
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_view__resetzoom'),
                        role: 'resetzoom'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_view__zoomin'),
                        role: 'zoomin'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_view__zoomout'),
                        role: 'zoomout'
                    }
                ]
            },
            {
                label: "" + Translations_1.default.get('menu_account'),
                submenu: [
                    {
                        label: "" + Translations_1.default.get('menu_account__traffic_info'),
                        accelerator: "CommandOrControl+Alt+P",
                        click: function () {
                            if (instance_1.default.get()) {
                                instance_1.default.get().webContents.send('event:client:traffic:info:response');
                            }
                        }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_account__start_sync'),
                        accelerator: "CommandOrControl+S",
                        click: function () {
                            return __awaiter(this, void 0, void 0, function () {
                                var workspaceId;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0: return [4, SelectedWorkspace_1.default.getGlobalId()];
                                        case 1:
                                            workspaceId = _a.sent();
                                            if (instance_1.default.get()) {
                                                instance_1.default.get().webContents.send('event:client:autosync:response', { workspaceId: workspaceId });
                                            }
                                            return [2];
                                    }
                                });
                            });
                        }
                    },
                    {
                        label: "" + Translations_1.default.get('cache_clear__confirm_details'),
                        accelerator: "CommandOrControl+Alt+T",
                        click: function () {
                            LocalCache_1.default.requestClear();
                        }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('show_sync_logs__details'),
                        accelerator: "CommandOrControl+Alt+K",
                        click: function () {
                            syncHandler_1.default.showLogs();
                        }
                    },
                    {
                        label: "" + Translations_1.default.get('send_logs__confirm_details'),
                        accelerator: "CommandOrControl+Alt+L",
                        click: function () {
                            errorHandler_1.default.requestSendSessionLogs();
                        }
                    }
                ]
            },
            {
                label: "" + Translations_1.default.get('menu_window'),
                role: 'window',
                submenu: [
                    {
                        label: "" + Translations_1.default.get('menu_window__minimize'),
                        role: 'minimize'
                    }
                ]
            },
            {
                label: "" + Translations_1.default.get('menu_help'),
                role: 'help',
                submenu: [
                    {
                        label: "" + Translations_1.default.get('menu_help__submit_bug'),
                        click: function () {
                            helpHandler_1.openSubmitBugLink();
                        }
                    },
                    {
                        label: "" + Translations_1.default.get('menu_help__help'),
                        click: function () {
                            helpHandler_1.openHelpLink();
                        }
                    },
                    {
                        label: "" + Translations_1.default.get('menu_help__contact_us'),
                        click: function () {
                            helpHandler_1.openContactUsLink();
                        }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_help__about'),
                        click: function () {
                            require('electron').shell.openExternal('http://nimbusweb.co');
                        }
                    }
                ]
            }
        ];
        if (process.platform === 'darwin') {
            template.unshift({
                label: "" + Translations_1.default.get('menu_file'),
                submenu: [
                    {
                        label: "" + Translations_1.default.get('menu_account__export'),
                        click: function () {
                            if (instance_1.default.get()) {
                                instance_1.default.get().webContents.send('event:client:export:workspace:notes:response');
                            }
                        }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_file__logout'),
                        click: function () {
                            if (instance_1.default.get()) {
                                apiMeLogout_1.default({}, function () { });
                            }
                        }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_file__close'),
                        role: 'close'
                    }
                ]
            });
        }
        else {
            template.unshift({
                label: "" + Translations_1.default.get('menu_file'),
                submenu: [
                    {
                        label: "" + Translations_1.default.get('menu_account__export'),
                        click: function () {
                            if (instance_1.default.get()) {
                                instance_1.default.get().webContents.send('event:client:export:workspace:notes:response');
                            }
                        }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_app__preferences'),
                        click: function () {
                            if (instance_1.default.get()) {
                                instance_1.default.get().webContents.send('event:client:display:settings:response');
                            }
                        }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_file__logout'),
                        click: function () {
                            if (instance_1.default.get()) {
                                instance_1.default.get().webContents.send('event:logout:request');
                            }
                        }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_file__close'),
                        role: 'close'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_app__quit'),
                        role: 'quit',
                        accelerator: "CommandOrControl+Q",
                    }
                ]
            });
        }
        if (process.platform === 'darwin') {
            template.unshift({
                label: 'Nimbus Note',
                submenu: [
                    {
                        label: "" + Translations_1.default.get('menu_app__about'),
                        role: 'about'
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_app__preferences'),
                        click: function () {
                            if (instance_1.default.get()) {
                                instance_1.default.get().webContents.send('event:client:display:settings:response');
                            }
                        }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_app__hide'),
                        role: 'hide'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_app__hideothers'),
                        role: 'hideothers'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_app__unhide'),
                        role: 'unhide'
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: "" + Translations_1.default.get('menu_app__quit'),
                        role: 'quit'
                    }
                ]
            });
        }
        if (process.platform === 'darwin') {
            if (typeof (template[5]) !== "undefined") {
                template[5].submenu.push({ label: "" + Translations_1.default.get('menu_window__maximize'), role: 'zoom' });
                template[5].submenu.push({ type: 'separator' });
                template[5].submenu.push({ label: "" + Translations_1.default.get('menu_window__front'), role: 'front' });
                template[5].submenu.push({ type: 'separator' });
                template[5].submenu.push({
                    label: "" + Translations_1.default.get('menu_window__show'),
                    click: function () {
                        if (instance_1.default.get()) {
                            if (!instance_1.default.get().isVisible()) {
                                instance_1.default.get().show();
                            }
                        }
                    }
                });
            }
        }
        else {
            template[4].submenu.push({
                label: "" + Translations_1.default.get('menu_window__maximize'),
                click: function () {
                    if (instance_1.default.get()) {
                        instance_1.default.get().maximize();
                    }
                }
            });
        }
        if (process.platform === 'darwin') {
            if (typeof (template[5]) !== "undefined") {
                template[5].submenu.push({ type: 'separator' });
                template[5].submenu.push({
                    label: "" + Translations_1.default.get('menu_speech'),
                    submenu: [
                        {
                            label: "" + Translations_1.default.get('menu_speech__startspeaking'),
                            role: 'startspeaking'
                        },
                        {
                            label: "" + Translations_1.default.get('menu_speech__stopspeaking'),
                            role: 'stopspeaking'
                        }
                    ]
                });
            }
            var dockTemplate = [
                {
                    label: "" + Translations_1.default.get('dock_menu__window_show'),
                    click: function () {
                        if (instance_1.default.get()) {
                            if (!instance_1.default.get().isVisible()) {
                                instance_1.default.get().show();
                            }
                        }
                    }
                }
            ];
            var dockMenu = electron_1.Menu.buildFromTemplate(dockTemplate);
            electron_1.app.dock.setMenu(dockMenu);
        }
        else {
            template[5].submenu.push({
                label: "" + Translations_1.default.get('menu_help__updates'),
                click: function (item) {
                    var menuCheck = require('./../utilities/autoUpdater').menuCheck;
                    menuCheck(item);
                }
            });
            template[5].submenu.push({
                label: "" + Translations_1.default.get('menu_app__about_short'),
                click: function () {
                    About_1.default.show();
                }
            });
        }
        var menu = electron_1.Menu.buildFromTemplate(template);
        electron_1.Menu.setApplicationMenu(menu);
    };
    ApplicationMenu.update = function () {
        if (instance_1.default.get()) {
            ApplicationMenu.add();
        }
    };
    ApplicationMenu.toggleAppWindow = function () {
        if (instance_1.default.get()) {
            if (instance_1.default.get().isMinimized()) {
                instance_1.default.get().restore();
            }
            if (!instance_1.default.get().isVisible()) {
                instance_1.default.get().show();
            }
        }
    };
    ApplicationMenu.getTrayMenu = function () {
        return electron_1.Menu.buildFromTemplate([
            {
                label: "" + Translations_1.default.get('dock_menu__window_show'),
                click: function () {
                    ApplicationMenu.toggleAppWindow();
                }
            },
            {
                label: "" + Translations_1.default.get('menu_edit__add_note'),
                click: function () {
                    if (instance_1.default.get()) {
                        instance_1.default.get().webContents.send('event:client:add:note:response');
                        ApplicationMenu.toggleAppWindow();
                    }
                },
                accelerator: "Alt+N"
            },
            {
                label: "" + Translations_1.default.get('menu_file__logout'),
                click: function () {
                    if (instance_1.default.get()) {
                        instance_1.default.get().webContents.send('event:logout:request');
                        ApplicationMenu.toggleAppWindow();
                    }
                }
            },
            {
                label: "" + Translations_1.default.get('menu_app__quit'),
                role: 'quit',
            }
        ]);
    };
    ApplicationMenu.getContextMenu = function () {
        var menu = [];
        menu.push({
            role: "cut",
            label: "" + Translations_1.default.get('menu_edit__cut'),
        });
        menu.push({
            role: "copy",
            label: "" + Translations_1.default.get('menu_edit__copy'),
        });
        menu.push({
            role: "paste",
            label: "" + Translations_1.default.get('menu_edit__paste'),
        });
        menu.push({
            type: 'separator'
        });
        menu.push({
            role: "pasteAndMatchStyle",
            label: "" + Translations_1.default.get('menu_edit__pasteandmatchstyle'),
        });
        menu.push({
            role: "delete",
            label: "" + Translations_1.default.get('menu_edit__delete'),
        });
        menu.push({
            role: "selectAll",
            label: "" + Translations_1.default.get('menu_edit__selectall'),
        });
        return menu;
    };
    return ApplicationMenu;
}());
exports.default = ApplicationMenu;
