"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var childWindows = {};
var ChildWindow = (function () {
    function ChildWindow() {
    }
    ChildWindow.set = function (windowType, child) {
        childWindows[windowType] = child;
    };
    ChildWindow.get = function (windowType) {
        return typeof (childWindows[windowType]) !== 'undefined' ? childWindows[windowType] : null;
    };
    ChildWindow.remove = function (windowType) {
        if (typeof (childWindows[windowType]) === "undefined") {
            return;
        }
        delete childWindows[windowType];
    };
    ChildWindow.TYPE_PURCHASE = 'purchaise';
    ChildWindow.TYPE_MESSAGE = 'message';
    ChildWindow.TYPE_ATTACH = 'attach';
    ChildWindow.TYPE_UPDATE = 'update';
    ChildWindow.TYPE_EXPORT = 'export';
    return ChildWindow;
}());
exports.default = ChildWindow;
