"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var socketFunctions_1 = __importDefault(require("../sync/socket/socketFunctions"));
var attach_1 = __importDefault(require("../db/models/attach"));
var selectedItems = {};
var SelectedItem = (function () {
    function SelectedItem() {
    }
    SelectedItem.set = function (inputData) {
        var workspaceId = inputData.workspaceId, itemType = inputData.itemType, item = inputData.item;
        if (typeof (selectedItems[workspaceId]) === 'undefined') {
            selectedItems[workspaceId] = {};
        }
        selectedItems[workspaceId][itemType] = item;
        if (itemType === SelectedItem.TYPE_NOTE) {
            SelectedItem.updateSelectedNoteAttachments({ workspaceId: workspaceId, globalId: item });
        }
    };
    SelectedItem.get = function (inputData) {
        var workspaceId = inputData.workspaceId, itemType = inputData.itemType;
        if (typeof (selectedItems[workspaceId]) === 'undefined') {
            return null;
        }
        return typeof (selectedItems[workspaceId][itemType]) !== 'undefined' ? selectedItems[workspaceId][itemType] : null;
    };
    SelectedItem.updateSelectedNoteAttachments = function (inputData) {
        var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
        var queryData = { noteGlobalId: globalId };
        attach_1.default.findAll(queryData, { workspaceId: workspaceId }, function (err, attachItems) {
            if (attachItems && attachItems.length) {
                for (var _i = 0, attachItems_1 = attachItems; _i < attachItems_1.length; _i++) {
                    var attachItem = attachItems_1[_i];
                    socketFunctions_1.default.sendNoteAttachmentsUpdateMessage({
                        workspaceId: workspaceId,
                        noteGlobalId: attachItem.noteGlobalId,
                        globalId: attachItem.globalId
                    });
                }
            }
        });
    };
    SelectedItem.TYPE_NOTE = 'note';
    SelectedItem.TYPE_FOLDER = 'folder';
    SelectedItem.TYPE_TAG = 'tag';
    return SelectedItem;
}());
exports.default = SelectedItem;
