"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var appWindow;
var WindowInstance = (function () {
    function WindowInstance() {
    }
    WindowInstance.set = function (windowInstance) {
        appWindow = windowInstance;
    };
    WindowInstance.get = function () {
        return appWindow || null;
    };
    WindowInstance.createAttachVideoWindow = function (attachWindow, windowHtml) {
        attachWindow.once('ready-to-show', function () {
            setTimeout(function () {
                var executeJavaScript = "document.getElementsByTagName('video')[0].offsetWidth";
                attachWindow.webContents.executeJavaScript(executeJavaScript, function (width) {
                    executeJavaScript = "document.getElementsByTagName('video')[0].offsetHeight";
                    attachWindow.webContents.executeJavaScript(executeJavaScript, function (height) {
                        if (width && height) {
                            height += 30;
                            attachWindow.setSize(width, height, false);
                        }
                        attachWindow.show();
                    });
                });
            }, 100);
        });
        var file = 'data:text/html;charset=UTF-8,' + encodeURIComponent(windowHtml);
        attachWindow.loadURL(file);
    };
    return WindowInstance;
}());
exports.default = WindowInstance;
