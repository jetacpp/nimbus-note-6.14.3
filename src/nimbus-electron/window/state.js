"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../config"));
var settings_1 = __importDefault(require("../db/models/settings"));
var instance_1 = __importDefault(require("./instance"));
var WindowState = (function () {
    function WindowState() {
    }
    WindowState.get = function (callback) {
        settings_1.default.get(config_1.default.SETTINGS_APP_WINDOW_STATE_NAME, function (err, data) {
            if (err || !data) {
                return callback(null, {});
            }
            return callback(null, data);
        });
    };
    WindowState.set = function (data) {
        settings_1.default.set(config_1.default.SETTINGS_APP_WINDOW_STATE_NAME, data);
    };
    WindowState.defaultWidth = function () {
        return config_1.default.APP_WINDOW_DEFAULT_WIDTH;
    };
    WindowState.defaultHeight = function () {
        return config_1.default.APP_WINDOW_DEFAULT_HEIGHT;
    };
    WindowState.listenChangeInfo = function () {
        WindowState.get(function (err, windowState) {
            if (!windowState) {
                return false;
            }
            var appWindow = instance_1.default.get();
            if (appWindow) {
                if (windowState.isMaximized) {
                    appWindow.maximize();
                }
                if (windowState.bounds) {
                    appWindow.setSize(windowState.bounds.width, windowState.bounds.height, true);
                    appWindow.setPosition(windowState.bounds.x, windowState.bounds.y, true);
                }
            }
        });
        ['resize', 'move', 'hide'].forEach(function (event) {
            var appWindow = instance_1.default.get();
            if (appWindow) {
                appWindow.on(event, function () {
                    try {
                        WindowState.get(function (err, windowState) {
                            if (appWindow) {
                                var state = {};
                                state.isMaximized = appWindow.isMaximized();
                                state.bounds = appWindow.getBounds();
                                WindowState.set(state);
                            }
                        });
                    }
                    catch (error) {
                        if (config_1.default.SHOW_WEB_CONSOLE) {
                            console.log("App close error: ", error);
                        }
                    }
                });
            }
        });
    };
    return WindowState;
}());
exports.default = WindowState;
