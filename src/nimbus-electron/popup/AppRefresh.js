"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var Translations_1 = __importDefault(require("../translations/Translations"));
var instance_1 = __importDefault(require("../window/instance"));
var AppRefresh = (function () {
    function AppRefresh() {
    }
    AppRefresh.show = function () {
        if (instance_1.default.get()) {
            electron_1.dialog.showMessageBox(instance_1.default.get(), {
                type: "warning",
                title: electron_1.app.name,
                message: "" + Translations_1.default.get('app_refresh__warning_message'),
                detail: "" + Translations_1.default.get('app_refresh__warning_details'),
                buttons: [
                    "" + Translations_1.default.get('app_refresh__confirm_btn')
                ],
                defaultId: 0,
                cancelId: 0
            });
        }
    };
    return AppRefresh;
}());
exports.default = AppRefresh;
