"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_runtime_1 = __importDefault(require("../../config.runtime"));
var instance_1 = __importDefault(require("../window/instance"));
var settings_1 = __importDefault(require("../db/models/settings"));
var auth_1 = __importDefault(require("../auth/auth"));
var PopupRateApp = (function () {
    function PopupRateApp() {
    }
    PopupRateApp.show = function () {
        if (instance_1.default.get()) {
            if (process.platform === 'darwin') {
                instance_1.default.get().webContents.send('event:client:popup:rate:app:show:response', {
                    problemUrl: "https://nimbusweb.me/contact-us.php",
                    storeUrl: "itms-apps://itunes.apple.com/app/id" + config_runtime_1.default.APPSTORE_APP_ID,
                    modal: true
                });
            }
        }
    };
    PopupRateApp.check = function () {
        var _this = this;
        auth_1.default.getUser(function (err, authInfo) {
            if (authInfo && Object.keys(authInfo).length) {
                settings_1.default.get(PopupRateApp.APP_RATE_POPUP_SHOWN, function (err, appRatePopupShown) {
                    if (typeof (appRatePopupShown) !== "object" && appRatePopupShown) {
                        return;
                    }
                    var setInstallTimestamp = function (timestamp) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    return __generator(this, function (_a) {
                                        settings_1.default.set(PopupRateApp.APP_INSTALL_DATE, timestamp, function () {
                                            return resolve(timestamp);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    settings_1.default.get(PopupRateApp.APP_INSTALL_DATE, function (err, appInstallTimestamp) { return __awaiter(_this, void 0, void 0, function () {
                        var currentTimestamp, timeDiff;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    currentTimestamp = new Date().getTime();
                                    if (!(typeof (appInstallTimestamp) === "object" && Object.keys(appInstallTimestamp).length === 0)) return [3, 2];
                                    appInstallTimestamp = currentTimestamp;
                                    return [4, setInstallTimestamp(appInstallTimestamp)];
                                case 1:
                                    _a.sent();
                                    _a.label = 2;
                                case 2:
                                    timeDiff = currentTimestamp - appInstallTimestamp;
                                    if (timeDiff < PopupRateApp.POPUP_SHOW_TIME) {
                                        return [2];
                                    }
                                    settings_1.default.set(PopupRateApp.APP_RATE_POPUP_SHOWN, true, function () {
                                        PopupRateApp.show();
                                    });
                                    return [2];
                            }
                        });
                    }); });
                });
            }
        });
    };
    PopupRateApp.POPUP_SHOW_TIME = 345600000;
    PopupRateApp.APP_INSTALL_DATE = "appInstallDate";
    PopupRateApp.APP_RATE_POPUP_SHOWN = "appRatePopupShown";
    return PopupRateApp;
}());
exports.default = PopupRateApp;
