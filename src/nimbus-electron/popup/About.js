"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var Translations_1 = __importDefault(require("../translations/Translations"));
var config_runtime_1 = __importDefault(require("../../config.runtime"));
var About = (function () {
    function About() {
    }
    About.show = function () {
        var appVersion = electron_1.app.getVersion();
        var copyRight = "\u00A9 " + new Date().getFullYear() + " NIMBUS WEB Inc.\nAll Rights Reserved.";
        electron_1.dialog.showMessageBox({
            type: 'info',
            title: Translations_1.default.get('menu_app__about'),
            message: "Version " + appVersion + " (" + appVersion + ")\n\n" + copyRight,
            icon: config_runtime_1.default.applicationIconPath,
        });
    };
    return About;
}());
exports.default = About;
