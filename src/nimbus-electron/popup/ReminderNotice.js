"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var item_1 = __importDefault(require("../db/models/item"));
var workspace_1 = __importDefault(require("../db/models/workspace"));
var dateHandler_1 = __importDefault(require("../utilities/dateHandler"));
var instance_1 = __importDefault(require("../window/instance"));
var Translations_1 = __importDefault(require("../translations/Translations"));
var config_1 = __importDefault(require("../../config"));
var reminders;
var ReminderNotice = (function () {
    function ReminderNotice() {
    }
    ReminderNotice.init = function (workspaces) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var _i, workspaces_1, workspaceInstance, workspaceId, workspaceTitle, notesList, _a, notesList_1, noteInstance, reminderInstance, reminderData;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (reminders) {
                            return [2, resolve(false)];
                        }
                        if (!reminders) {
                            reminders = {};
                        }
                        _i = 0, workspaces_1 = workspaces;
                        _b.label = 1;
                    case 1:
                        if (!(_i < workspaces_1.length)) return [3, 5];
                        workspaceInstance = workspaces_1[_i];
                        return [4, workspace_1.default.getLocalId(workspaceInstance.workspaceId)];
                    case 2:
                        workspaceId = _b.sent();
                        workspaceTitle = workspaceInstance.title;
                        return [4, item_1.default.getNotesWithReminders(workspaceId)];
                    case 3:
                        notesList = _b.sent();
                        for (_a = 0, notesList_1 = notesList; _a < notesList_1.length; _a++) {
                            noteInstance = notesList_1[_a];
                            if (typeof (reminders[noteInstance.globalId]) !== 'undefined') {
                                continue;
                            }
                            reminderInstance = noteInstance.reminder;
                            if (reminderInstance) {
                                reminderData = {
                                    workspaceId: workspaceInstance.globalId,
                                    workspaceTitle: workspaceTitle,
                                    globalId: noteInstance.globalId,
                                    parentId: noteInstance.parentId,
                                    title: noteInstance.title,
                                    reminder: reminderInstance
                                };
                                ReminderNotice.setTimeoutByGlobalId(reminderData);
                            }
                        }
                        _b.label = 4;
                    case 4:
                        _i++;
                        return [3, 1];
                    case 5: return [2, resolve(true)];
                }
            });
        }); });
    };
    ReminderNotice.setTimeoutByGlobalId = function (reminderData) {
        var globalId = reminderData.globalId;
        ReminderNotice.clearIntervalByGlobalId(globalId);
        ReminderNotice.clearTimeoutByGlobalId(globalId);
        ReminderNotice.clearByGlobalId(globalId);
        if (typeof reminders[globalId] === 'undefined') {
            reminders[globalId] = {
                'timeout': null,
                'interval': null
            };
        }
        if (!reminderData.reminder.date) {
            return;
        }
        var reminderTimeout = reminderData.reminder.date - dateHandler_1.default.now();
        var reminderTimeoutDiff;
        if (reminderTimeout >= 0) {
            reminderTimeoutDiff = reminderTimeout * 1000;
        }
        else {
            if (!reminderData.reminder.interval) {
                return;
            }
            var pastTimeout = dateHandler_1.default.now() - reminderData.reminder.date;
            var missedIntervals = Math.floor(pastTimeout / reminderData.reminder.interval);
            if (missedIntervals >= 1) {
                reminderTimeoutDiff = (pastTimeout - (missedIntervals * reminderData.reminder.interval)) * 1000;
            }
            else {
                reminderTimeoutDiff = (pastTimeout - reminderData.reminder.interval) * 1000;
            }
            reminderTimeoutDiff = (reminderData.reminder.interval * 1000) - reminderTimeoutDiff;
        }
        if (reminderTimeoutDiff > ReminderNotice.TIMER_MAX_VALUE) {
            return;
        }
        var timeoutInstance = setTimeout(function () {
            return __awaiter(this, void 0, void 0, function () {
                var noteExist;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!this.reminderData) {
                                return [2];
                            }
                            return [4, ReminderNotice.checkIfNoteExist(this.reminderData)];
                        case 1:
                            noteExist = _a.sent();
                            if (!noteExist) {
                                return [2];
                            }
                            ReminderNotice.clearTimeoutByGlobalId(this.reminderData.globalId);
                            if (this.reminderData.reminder.interval) {
                                ReminderNotice.setIntervalByGlobalId(this.reminderData);
                            }
                            ReminderNotice.show(this.reminderData);
                            return [2];
                    }
                });
            });
        }, reminderTimeoutDiff);
        timeoutInstance['reminderData'] = reminderData;
        reminders[globalId]['timeout'] = timeoutInstance;
    };
    ReminderNotice.setIntervalByGlobalId = function (reminderData) {
        var globalId = reminderData.globalId;
        ReminderNotice.clearIntervalByGlobalId(globalId);
        if (typeof reminders[globalId] === 'undefined') {
            reminders[globalId] = {
                'timeout': null,
                'interval': null
            };
        }
        var reminderInterval = reminderData.reminder.interval * 1000;
        if (reminderInterval > ReminderNotice.TIMER_MAX_VALUE) {
            return;
        }
        var intervalInstance = setInterval(function () {
            return __awaiter(this, void 0, void 0, function () {
                var noteExist;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4, ReminderNotice.checkIfNoteExist(this.reminderData)];
                        case 1:
                            noteExist = _a.sent();
                            if (!noteExist) {
                                return [2];
                            }
                            ReminderNotice.show(this.reminderData);
                            return [2];
                    }
                });
            });
        }, reminderInterval);
        intervalInstance['reminderData'] = reminderData;
        reminders[globalId]['interval'] = intervalInstance;
    };
    ReminderNotice.clearTimeoutByGlobalId = function (globalId) {
        if (typeof reminders[globalId] !== 'undefined' && reminders[globalId]) {
            if (reminders[globalId].timeout) {
                clearTimeout(reminders[globalId].timeout);
            }
            reminders[globalId].timeout = null;
        }
    };
    ReminderNotice.clearIntervalByGlobalId = function (globalId) {
        if (typeof reminders[globalId] !== 'undefined' && reminders[globalId]) {
            if (reminders[globalId].interval) {
                clearInterval(reminders[globalId].interval);
            }
            if (reminders[globalId].timeout) {
                clearTimeout(reminders[globalId].timeout);
            }
            reminders[globalId].timeout = null;
            reminders[globalId].reminder = null;
        }
    };
    ReminderNotice.clearByGlobalId = function (globalId) {
        if (typeof reminders[globalId] !== 'undefined' && reminders[globalId]) {
            delete reminders[globalId];
        }
    };
    ReminderNotice.flushByGlobalId = function (globalId) {
        ReminderNotice.clearTimeoutByGlobalId(globalId);
        ReminderNotice.clearIntervalByGlobalId(globalId);
        ReminderNotice.clearByGlobalId(globalId);
    };
    ReminderNotice.flushAll = function () {
        if (reminders) {
            if (Object.keys(reminders).length) {
                for (var globalId in reminders) {
                    if (reminders.hasOwnProperty(globalId)) {
                        ReminderNotice.flushByGlobalId(globalId);
                    }
                }
            }
            reminders = null;
        }
    };
    ReminderNotice.show = function (reminderData) {
        if (!reminderData) {
            return;
        }
        reminderData.noteUrl = config_1.default.LOCAL_SERVER_HTTP_HOST + "/ws/" + reminderData.workspaceId + "/folder/" + reminderData.parentId + "/note/" + reminderData.globalId;
        if (instance_1.default.get()) {
            var notificationData = {
                title: reminderData.workspaceTitle.length > 100 ? reminderData.workspaceTitle.substring(0, 50) + "..." : reminderData.workspaceTitle,
                body: {
                    subtitle: reminderData.title.length > 100 ? reminderData.title.substring(0, 50) + "..." : reminderData.title,
                    body: Translations_1.default.get('reminder_bottom_text'),
                    reminderData: reminderData,
                    bundleId: config_1.default.APPBUNDLE_ID,
                    showCloseButton: true,
                    canReply: false,
                    soundName: 'default'
                }
            };
            if (process.platform !== 'darwin') {
                notificationData.body.icon = config_1.default.aboutIconPath;
            }
            instance_1.default.get().webContents.send('event:client:send:notification:response', notificationData);
        }
    };
    ReminderNotice.checkIfNoteExist = function (reminderData) {
        return __awaiter(this, void 0, void 0, function () {
            var note;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, ReminderNotice.getNoteForReminder(reminderData)];
                    case 1:
                        note = _a.sent();
                        if (note) {
                            return [2, note];
                        }
                        ReminderNotice.clearTimeoutByGlobalId(reminderData.globalId);
                        ReminderNotice.clearIntervalByGlobalId(reminderData.globalId);
                        return [2, null];
                }
            });
        });
    };
    ReminderNotice.getNoteForReminder = function (_a) {
        var _this = this;
        var workspaceId = _a.workspaceId, globalId = _a.globalId;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        if (typeof (workspaceId) === 'undefined') {
                            return [2, resolve(null)];
                        }
                        if (!globalId) {
                            return [2, resolve(null)];
                        }
                        _b = (_a = item_1.default).find;
                        _c = [{
                                type: 'note',
                                globalId: globalId,
                                rootId: { '$ne': 'trash' }
                            }];
                        _d = {};
                        if (!workspaceId) return [3, 2];
                        return [4, workspace_1.default.getLocalId(workspaceId)];
                    case 1:
                        _e = _f.sent();
                        return [3, 3];
                    case 2:
                        _e = null;
                        _f.label = 3;
                    case 3:
                        _b.apply(_a, _c.concat([(_d.workspaceId = _e,
                                _d), function (err, noteInstance) {
                                if (err || !noteInstance) {
                                    return resolve(null);
                                }
                                return resolve(noteInstance);
                            }]));
                        return [2];
                }
            });
        }); });
    };
    ReminderNotice.TIMER_MAX_VALUE = 2147483647;
    return ReminderNotice;
}());
exports.default = ReminderNotice;
