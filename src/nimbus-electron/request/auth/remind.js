"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NimbusSDK_1 = __importDefault(require("../../sync/nimbussdk/net/NimbusSDK"));
var Remind = (function () {
    function Remind() {
    }
    Remind.tryRemindPassword = function (frmData, callback) {
        if (frmData === void 0) { frmData = null; }
        if (callback === void 0) { callback = function (res) {
        }; }
        if (!frmData) {
            return callback({ errorCode: -2 });
        }
        if (!frmData.login) {
            return callback({ errorCode: -2 });
        }
        apiRestorePassword(frmData.login, function (err, repsponse) {
            if (err) {
                return callback({ errorCode: err });
            }
            return callback({});
        });
    };
    return Remind;
}());
exports.default = Remind;
function apiRestorePassword(email, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    NimbusSDK_1.default.getApi().remindPassword(email, function (err, response) {
        callback(err, response);
    });
}
