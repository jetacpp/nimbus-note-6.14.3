"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var oauth_1 = require("oauth");
exports.LOGIN_SUCCESS_URL = 'https://www.facebook.com/connect/login_success.html';
exports.LOGIN_FAILURE_URL = 'https://www.facebook.com/dialog/close';
exports.BASE_SITE_URL = '';
exports.AUTHORIZE_URL = 'https://www.facebook.com/dialog/oauth';
exports.ACCESS_TOKEN_URL = 'https://graph.facebook.com/oauth/access_token';
var Oauth = (function () {
    function Oauth(info) {
        this.info = info;
        this.oauth = new oauth_1.OAuth2(info.key, info.secret, exports.BASE_SITE_URL, exports.AUTHORIZE_URL, exports.ACCESS_TOKEN_URL);
    }
    Oauth.prototype.getAuthUrl = function () {
        return this.oauth.getAuthorizeUrl({
            redirect_uri: exports.LOGIN_SUCCESS_URL,
            scope: this.info.scope,
        });
    };
    Oauth.prototype.getTokens = function (code) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.oauth.getOAuthAccessToken(code, {
                redirect_uri: exports.LOGIN_SUCCESS_URL,
            }, function (error, _, __, result) {
                if (error) {
                    return reject(error);
                }
                resolve(result);
            });
        });
    };
    return Oauth;
}());
exports.Oauth = Oauth;
