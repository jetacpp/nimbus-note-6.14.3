"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var instance_1 = __importDefault(require("../../../window/instance"));
exports.oauthWindowParams = {
    useContentSize: true,
    center: true,
    show: false,
    resizable: true,
    alwaysOnTop: true,
    'standard-window': true,
    autoHideMenuBar: true,
    webPreferences: {
        devTools: false,
        nodeIntegration: false,
        webSecurity: false,
        plugins: true,
    },
};
exports.oauthRejectHandler = function () {
    instance_1.default.get().webContents.send('event:oauth:close', {});
};
