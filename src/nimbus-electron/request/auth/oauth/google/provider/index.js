"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var querystring_1 = require("querystring");
var googleapis_1 = __importDefault(require("googleapis"));
var node_fetch_1 = __importDefault(require("node-fetch"));
var errorHandler_1 = __importDefault(require("../../../../../utilities/errorHandler"));
var OAuth2 = googleapis_1.default.auth.OAuth2;
var FETCH_TOKEN_URL = 'https://accounts.google.com/o/oauth2/token';
var REDIRECT_URL = 'urn:ietf:wg:oauth:2.0:oob';
var getAuthenticationUrl = function (scopes, clientId, clientSecret, redirectUri) {
    if (redirectUri === void 0) { redirectUri = REDIRECT_URL; }
    var oauth2Client = new OAuth2(clientId, clientSecret, redirectUri);
    return oauth2Client.generateAuthUrl({ access_type: 'offline', scope: scopes });
};
var authorizeApp = function (url, browserWindowParams, rejectHandler) {
    if (rejectHandler === void 0) { rejectHandler = function () { }; }
    return new Promise(function (resolve, reject) {
        var win = new electron_1.BrowserWindow(browserWindowParams);
        win.webContents.on('did-finish-load', function () { return win.show(); });
        win.on('closed', function () {
            rejectHandler();
            reject(new Error('User closed the window'));
        });
        win.on('page-title-updated', function () {
            setImmediate(function () {
                var title = win.getTitle();
                if (title.startsWith('Denied')) {
                    reject(new Error(title.split(/[ =]/)[2]));
                    win.removeAllListeners('closed');
                    win.removeAllListeners('page-title-updated');
                    rejectHandler();
                    win.close();
                }
                else if (title.startsWith('Success')) {
                    resolve(title.split(/[ =]/)[2]);
                    win.removeAllListeners('closed');
                    win.removeAllListeners('page-title-updated');
                    win.close();
                }
            });
        });
        loadAuthUrl(url, win);
    });
};
var logoutApp = function (url, browserWindowParams) {
    var win = new electron_1.BrowserWindow(browserWindowParams);
    win.on('closed', function () { return console.log('Google logout success'); });
    win.on('page-title-updated', function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    errorHandler_1.default.log('fire page-title-updated?');
                    return [4, win.webContents.session.clearStorageData({
                            storages: ['cookies', 'localstorage', 'indexdb'],
                        })];
                case 1:
                    _a.sent();
                    errorHandler_1.default.log('close logout');
                    win.close();
                    return [2];
            }
        });
    }); });
    errorHandler_1.default.log('loadAuthUrl url?');
    errorHandler_1.default.log(url);
    loadAuthUrl(url, win);
};
function googleOauth(browserWindowParams, rejectHandler, httpAgent) {
    var _this = this;
    if (rejectHandler === void 0) { rejectHandler = function () { }; }
    if (httpAgent === void 0) { httpAgent = null; }
    var getAuthorizationCode = function (scopes, clientId, clientSecret, redirectUri) {
        if (redirectUri === void 0) { redirectUri = REDIRECT_URL; }
        var url = getAuthenticationUrl(scopes, clientId, clientSecret, redirectUri);
        return authorizeApp(url, browserWindowParams, rejectHandler);
    };
    var getAccessToken = function (authorizationCode, clientId, clientSecret, redirectUri) {
        if (redirectUri === void 0) { redirectUri = REDIRECT_URL; }
        return __awaiter(_this, void 0, void 0, function () {
            var data, res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = querystring_1.stringify({
                            code: authorizationCode,
                            client_id: clientId,
                            client_secret: clientSecret,
                            grant_type: 'authorization_code',
                            redirect_uri: redirectUri,
                        });
                        return [4, node_fetch_1.default(FETCH_TOKEN_URL, {
                                method: 'post',
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                body: data,
                                agent: httpAgent
                            })];
                    case 1:
                        res = _a.sent();
                        return [2, res.json()];
                }
            });
        });
    };
    var logout = function (scopes, clientId, clientSecret, redirectUri) {
        if (redirectUri === void 0) { redirectUri = REDIRECT_URL; }
        return __awaiter(_this, void 0, void 0, function () {
            var url;
            return __generator(this, function (_a) {
                errorHandler_1.default.log('start logout');
                url = getAuthenticationUrl(scopes, clientId, clientSecret, redirectUri);
                errorHandler_1.default.log('process logout url?');
                errorHandler_1.default.log(url);
                return [2, logoutApp(url, browserWindowParams)];
            });
        });
    };
    return {
        getAuthorizationCode: getAuthorizationCode,
        getAccessToken: getAccessToken,
        logout: logout,
    };
}
exports.default = googleOauth;
;
var loadAuthUrl = function (url, window) {
    window.loadURL(url, { userAgent: 'Chrome' });
};
