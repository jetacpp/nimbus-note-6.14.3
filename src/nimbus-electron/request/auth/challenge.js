"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var state_1 = __importDefault(require("../../online/state"));
var NimbusSDK_1 = __importDefault(require("../../sync/nimbussdk/net/NimbusSDK"));
var Challenge = (function () {
    function Challenge() {
    }
    Challenge.tryChallengeClient = function (frmData, callback) {
        if (frmData === void 0) { frmData = null; }
        if (callback === void 0) { callback = function (res) {
        }; }
        if (state_1.default.get()) {
            NimbusSDK_1.default.getApi().challenge(frmData, function (err, response) {
                if (err) {
                    var errorResponse = { errorCode: err };
                    if (response) {
                        if (response.challenge) {
                            errorResponse['challenge'] = response.challenge;
                        }
                        if (response.body) {
                            errorResponse['body'] = response.body;
                        }
                    }
                    return callback(errorResponse);
                }
                return callback(response);
            });
        }
        else {
            return callback({ errorCode: Challenge.ERROR_CHALLENGE_OFFLINE });
        }
    };
    Challenge.ERROR_CHALLENGE_OFFLINE = -9;
    return Challenge;
}());
exports.default = Challenge;
