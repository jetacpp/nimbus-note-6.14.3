"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../config"));
var auth_1 = __importDefault(require("../../auth/auth"));
var state_1 = __importDefault(require("../../online/state"));
var NimbusSDK_1 = __importDefault(require("../../sync/nimbussdk/net/NimbusSDK"));
var SyncManager_1 = __importDefault(require("../../sync/process/SyncManager"));
var page_1 = __importDefault(require("../page"));
var menu_1 = __importDefault(require("../../menu/menu"));
var Translations_1 = __importDefault(require("../../translations/Translations"));
var user_1 = __importDefault(require("../../db/models/user"));
var pdb_1 = __importDefault(require("../../../pdb"));
var orgs_1 = __importDefault(require("../../db/models/orgs"));
var generatorHandler_1 = __importDefault(require("../../utilities/generatorHandler"));
var Login = (function () {
    function Login() {
    }
    Login.tryAuthClient = function (login, sessionId, provider, callback) {
        var _this = this;
        if (provider === void 0) { provider = ''; }
        if (callback === void 0) { callback = function (res) {
        }; }
        if (state_1.default.get()) {
            NimbusSDK_1.default.getApi().auth(login, sessionId, function (err, response) {
                if (err || !response) {
                    return callback({ errorCode: err });
                }
                var frmData = { login: login.toLowerCase() };
                frmData = auth_1.default.prepareFormSyncData(frmData, response);
                frmData.authProvider = provider;
                user_1.default.find({ email: frmData.login }, {}, function (err, itemInstance) { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        if (itemInstance) {
                            if (itemInstance.language) {
                                frmData.language = itemInstance.language;
                            }
                            Login.offlineClientLogin(frmData, { oauth: true }, callback);
                        }
                        else {
                            frmData.password = generatorHandler_1.default.randomString(8);
                            Login.offlineClientRegister(frmData, callback);
                        }
                        return [2];
                    });
                }); });
            });
        }
    };
    Login.tryLoginClient = function (frmData, callback) {
        var _this = this;
        if (frmData === void 0) { frmData = null; }
        if (callback === void 0) { callback = function (res) {
        }; }
        if (frmData && callback) {
            if (frmData.login) {
                frmData.login = frmData.login.toLowerCase();
            }
            if (state_1.default.get()) {
                NimbusSDK_1.default.getApi().signIn(frmData.login, frmData.password, function (err, response) {
                    if (err) {
                        var errorResponse = { errorCode: err };
                        if (response) {
                            if (response.challenge) {
                                errorResponse['challenge'] = response.challenge;
                            }
                            if (response.body) {
                                errorResponse['body'] = response.body;
                            }
                        }
                        return callback(errorResponse);
                    }
                    frmData = auth_1.default.prepareFormSyncData(frmData, response);
                    frmData.authProvider = '';
                    user_1.default.find({ email: frmData.login }, {}, function (err, itemInstance) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!itemInstance) return [3, 2];
                                    if (itemInstance.language) {
                                        frmData.language = itemInstance.language;
                                    }
                                    return [4, auth_1.default.updateUserPassword(frmData)];
                                case 1:
                                    _a.sent();
                                    Login.offlineClientLogin(frmData, {}, callback);
                                    return [3, 3];
                                case 2:
                                    Login.offlineClientRegister(frmData, callback);
                                    _a.label = 3;
                                case 3: return [2];
                            }
                        });
                    }); });
                });
            }
            else {
                Login.offlineClientLogin(frmData, {}, callback);
            }
        }
        else {
            Login.offlineClientAutoLogin();
        }
    };
    Login.offlineClientLogin = function (frmData, options, callback) {
        var _this = this;
        auth_1.default.login(frmData, options, function (err, formResponse) {
            if (formResponse.errorCode) {
                return callback(formResponse);
            }
            if (frmData.language) {
                Translations_1.default.setLang(frmData.language);
                menu_1.default.update();
            }
            if (formResponse && formResponse.data && formResponse.data.email) {
                NimbusSDK_1.default.getAccountManager().setUserEmail(formResponse.data.email, function () {
                    callback(formResponse);
                    auth_1.default.getUser(function (err, authInfo) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!(authInfo && Object.keys(authInfo).length)) return [3, 3];
                                    if (pdb_1.default.getDbClientId() != authInfo.userId) {
                                        pdb_1.default.setDbClientId(authInfo.userId);
                                    }
                                    SyncManager_1.default.clearAllSyncData();
                                    if (!state_1.default.get()) return [3, 2];
                                    return [4, orgs_1.default.syncUserOrganizationsWorkspaces()];
                                case 1:
                                    _a.sent();
                                    _a.label = 2;
                                case 2: return [2, page_1.default.load(config_1.default.PAGE_NOTE_NAME)];
                                case 3: return [2, page_1.default.load(config_1.default.PAGE_AUTH_NAME)];
                            }
                        });
                    }); });
                });
            }
            else {
                return page_1.default.load(config_1.default.PAGE_AUTH_NAME);
            }
        });
    };
    Login.offlineClientRegister = function (frmData, callback) {
        var _this = this;
        auth_1.default.register(frmData, function (err, formResponse) {
            if (formResponse.errorCode) {
                return callback(formResponse);
            }
            if (formResponse.data && formResponse.data.userId) {
                if (pdb_1.default.getDbClientId() != formResponse.data.userId) {
                    pdb_1.default.setDbClientId(formResponse.data.userId);
                }
            }
            callback(null, formResponse);
            auth_1.default.getUser(function (err, authInfo) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!(authInfo && Object.keys(authInfo).length)) return [3, 3];
                            if (pdb_1.default.getDbClientId() != authInfo.userId) {
                                pdb_1.default.setDbClientId(authInfo.userId);
                            }
                            SyncManager_1.default.clearAllSyncData();
                            if (!state_1.default.get()) return [3, 2];
                            return [4, orgs_1.default.syncUserOrganizationsWorkspaces()];
                        case 1:
                            _a.sent();
                            _a.label = 2;
                        case 2: return [2, page_1.default.load(config_1.default.PAGE_NOTE_NAME)];
                        case 3: return [2, page_1.default.load(config_1.default.PAGE_AUTH_NAME)];
                    }
                });
            }); });
        });
    };
    Login.offlineClientAutoLogin = function () {
        if (config_1.default.CLIENT_AUTO_LOGIN) {
            auth_1.default.authorized(function (err, client) {
                if (client) {
                    if (client && client.email) {
                        NimbusSDK_1.default.getAccountManager().setUserEmail(client.email, function () {
                            page_1.default.load(config_1.default.PAGE_NOTE_NAME);
                        });
                    }
                }
                else {
                    page_1.default.load(config_1.default.PAGE_AUTH_NAME);
                }
            });
        }
        else {
            return page_1.default.load(config_1.default.PAGE_AUTH_NAME);
        }
    };
    Login.AUTH_PROVIDER_GOOGLE = 'google';
    Login.AUTH_PROVIDER_FACEBOOK = 'facebook';
    return Login;
}());
exports.default = Login;
