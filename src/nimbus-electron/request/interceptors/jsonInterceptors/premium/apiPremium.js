"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_1 = __importDefault(require("../../../../db/models/user"));
function apiPremium(request, callback) {
    user_1.default.getPublicPremium(request.authInfo, callback);
}
exports.default = apiPremium;
