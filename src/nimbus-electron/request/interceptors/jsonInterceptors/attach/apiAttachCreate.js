"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs-extra");
var config_runtime_1 = __importDefault(require("../../../../../config.runtime"));
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var attach_1 = __importDefault(require("../../../../db/models/attach"));
var item_1 = __importDefault(require("../../../../db/models/item"));
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
var socketFunctions_1 = __importDefault(require("../../../../sync/socket/socketFunctions"));
var urlParser_1 = __importDefault(require("../../../../utilities/urlParser"));
var pdb_1 = __importDefault(require("../../../../../pdb"));
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
var instance_1 = __importDefault(require("../../../../window/instance"));
var fetch = require('node-fetch');
var fileType = require('file-type');
var mimeTypes = require("mime-types");
function apiAttachCreate(request, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var routeParams, workspaceId, _a, body, file, mimetype, displayName, size, isBuffer, sourceRes, e_1, curDate, storedFileUUID, extension, nameData, saveData, exists;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    routeParams = urlParser_1.default.getPathParams(request.url);
                    if (!typeof (routeParams[3] !== 'undefined')) return [3, 2];
                    return [4, workspace_1.default.getLocalId(routeParams[3])];
                case 1:
                    _a = _b.sent();
                    return [3, 3];
                case 2:
                    _a = null;
                    _b.label = 3;
                case 3:
                    workspaceId = _a;
                    if (!request.body) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    body = request.body;
                    file = null;
                    mimetype = '';
                    displayName = '';
                    size = 0;
                    isBuffer = false;
                    if (!body.sourceExternalUrl) return [3, 10];
                    _b.label = 4;
                case 4:
                    _b.trys.push([4, 8, , 9]);
                    isBuffer = true;
                    displayName = body.sourceExternalUrl.split('/').pop();
                    return [4, fetch(body.sourceExternalUrl)];
                case 5:
                    sourceRes = _b.sent();
                    return [4, sourceRes.buffer()];
                case 6:
                    file = _b.sent();
                    return [4, fileType.fromBuffer(file)];
                case 7:
                    mimetype = _b.sent();
                    size = file.toString().length;
                    return [3, 9];
                case 8:
                    e_1 = _b.sent();
                    return [3, 9];
                case 9: return [3, 11];
                case 10:
                    if (request.files && request.files.attachment && request.files.attachment.length) {
                        displayName = body.displayName;
                        file = request.files.attachment[0];
                        mimetype = file.mimetype;
                        size = body.size ? parseInt(body.size) : 0;
                    }
                    else {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    _b.label = 11;
                case 11:
                    if (!file) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    if (!displayName) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    curDate = dateHandler_1.default.now();
                    storedFileUUID = attach_1.default.generateStoredFileUUID();
                    extension = "";
                    if (displayName) {
                        nameData = displayName.split(".");
                        if (nameData.length > 1) {
                            extension = nameData[nameData.length - 1];
                        }
                    }
                    if (!mimetype) {
                        mimetype = mimeTypes.extension(extension) ? "." + mimeTypes.extension(extension) : '';
                    }
                    saveData = {
                        'globalId': body.globalId,
                        'noteGlobalId': body.noteGlobalId,
                        'dateAdded': curDate,
                        'dateUpdated': curDate,
                        'displayName': displayName,
                        'inList': body.inList === "true",
                        'mime': mimetype,
                        'size': size,
                        'storedFileUUID': storedFileUUID,
                        'hashKey': body.$$hashKey,
                        "syncDate": curDate,
                        "needSync": true,
                        "isDownloaded": true,
                        "extension": extension
                    };
                    return [4, fs.exists(config_runtime_1.default.nimbusAttachmentPath)];
                case 12:
                    exists = _b.sent();
                    if (exists) {
                        prepareAttachLoad({ workspaceId: workspaceId, saveData: saveData, file: file, isBuffer: isBuffer }, callback);
                    }
                    else {
                        fs.mkdir(config_runtime_1.default.nimbusAttachmentPath, function () {
                            prepareAttachLoad({ workspaceId: workspaceId, saveData: saveData, file: file, isBuffer: isBuffer }, callback);
                        });
                    }
                    return [2];
            }
        });
    });
}
exports.default = apiAttachCreate;
function prepareAttachLoad(inputData, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var workspaceId, saveData, file, isBuffer, targetPath, exists;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    workspaceId = inputData.workspaceId, saveData = inputData.saveData, file = inputData.file, isBuffer = inputData.isBuffer;
                    if (!pdb_1.default.getClientAttachmentPath()) {
                        return [2, callback(customError_1.default.wrongAttachPath(), {})];
                    }
                    targetPath = pdb_1.default.getClientAttachmentPath() + "/" + saveData.storedFileUUID;
                    return [4, fs.exists(pdb_1.default.getClientAttachmentPath())];
                case 1:
                    exists = _a.sent();
                    if (exists) {
                        if (isBuffer) {
                            fs.writeFile(targetPath, file, function (err) {
                                if (err) {
                                    if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                        console.log(err);
                                    }
                                }
                                prepareAttachSave({ workspaceId: workspaceId, saveData: saveData }, callback);
                            });
                        }
                        else {
                            fs.rename(file.path, targetPath, function (err) {
                                if (err) {
                                    if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                        console.log(err);
                                    }
                                }
                                prepareAttachSave({ workspaceId: workspaceId, saveData: saveData }, callback);
                            });
                        }
                    }
                    else {
                        fs.mkdir(pdb_1.default.getClientAttachmentPath(), function () {
                            if (isBuffer) {
                                fs.writeFile(targetPath, file, function (err) {
                                    if (err) {
                                        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                            console.log(err);
                                        }
                                    }
                                    prepareAttachSave({ workspaceId: workspaceId, saveData: saveData }, callback);
                                });
                            }
                            else {
                                fs.rename(file.path, targetPath, function (err) {
                                    if (err) {
                                        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                            console.log('Move file error: ', err);
                                        }
                                    }
                                    prepareAttachSave({ workspaceId: workspaceId, saveData: saveData }, callback);
                                });
                            }
                        });
                    }
                    return [2];
            }
        });
    });
}
function prepareAttachSave(inputData, callback) {
    var workspaceId = inputData.workspaceId, saveData = inputData.saveData;
    var curDate = dateHandler_1.default.now();
    var itemSaveData = attach_1.default.prepareModelData(saveData);
    attach_1.default.add(syncPropsHandler_1.setUpdateProps(itemSaveData, curDate), { workspaceId: workspaceId }, function (err, attachItem) {
        if (attachItem && Object.keys(attachItem).length) {
            var curDate_1 = dateHandler_1.default.now();
            var itemQueryData_1 = { globalId: attachItem.noteGlobalId };
            var itemSaveData_1 = {
                "dateUpdated": saveData.dateUpdated,
                "updatedAt": saveData.dateUpdated,
                "dateUpdatedUser": saveData.dateUpdated,
                "syncDate": curDate_1,
                "needSync": true
            };
            item_1.default.update(itemQueryData_1, syncPropsHandler_1.setUpdateProps(itemSaveData_1, curDate_1), { workspaceId: workspaceId }, function () {
                item_1.default.find(itemQueryData_1, { workspaceId: workspaceId }, function (err, itemInstance) {
                    callback(null, attach_1.default.getResponseJson(attachItem));
                    socketUserAttachMessage({ workspaceId: workspaceId, attachItem: attachItem });
                    socketUserCounterMessage({ workspaceId: workspaceId, itemInstance: itemInstance });
                    if (itemInstance.text_version >= 2) {
                        instance_1.default.get().webContents.send('event:client:autosync:request', { workspaceId: workspaceId });
                    }
                });
            });
        }
        else {
            callback(customError_1.default.itemSaveError(), {});
        }
    });
}
function socketUserAttachMessage(inputData) {
    var workspaceId = inputData.workspaceId, attachItem = inputData.attachItem;
    if (attachItem) {
        socketFunctions_1.default.sendNoteAttachmentsUpdateMessage({
            workspaceId: workspaceId,
            noteGlobalId: attachItem.noteGlobalId,
            globalId: attachItem.globalId
        });
    }
}
function socketUserCounterMessage(inputData) {
    var workspaceId = inputData.workspaceId, itemInstance = inputData.itemInstance;
    if (itemInstance) {
        socketFunctions_1.default.sendItemsCountMessage({
            workspaceId: workspaceId,
            globalId: itemInstance.globalId,
            parentId: itemInstance.parentId,
            type: itemInstance.type
        });
    }
}
