"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var workspaceMember_1 = __importDefault(require("../../../../db/models/workspaceMember"));
var NimbusSDK_1 = __importDefault(require("../../../../sync/nimbussdk/net/NimbusSDK"));
var state_1 = __importDefault(require("../../../../online/state"));
var instance_1 = __importDefault(require("../../../../window/instance"));
var Translations_1 = __importDefault(require("../../../../translations/Translations"));
var urlParser_1 = __importDefault(require("../../../../utilities/urlParser"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
function apiWorkspaceMemberUpdate(request, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var routeParams, workspaceId, _a, globalId, role, curDate, workspaceItem, members, existMember, i, apiUpdateData;
        var _this = this;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    if (!state_1.default.get()) {
                        if (instance_1.default.get()) {
                            instance_1.default.get().webContents.send('event:client:workspace:message:response', {
                                message: Translations_1.default.get('toast__workspace__member__update__offline'),
                            });
                        }
                        return [2, callback(null, { httpStatus: 404 })];
                    }
                    routeParams = urlParser_1.default.getPathParams(request.url);
                    if (!typeof (routeParams[3] !== 'undefined')) return [3, 2];
                    return [4, workspace_1.default.getLocalId(routeParams[3])];
                case 1:
                    _a = _b.sent();
                    return [3, 3];
                case 2:
                    _a = null;
                    _b.label = 3;
                case 3:
                    workspaceId = _a;
                    globalId = typeof (routeParams[5] !== 'undefined') && routeParams[5] ? routeParams[5] : null;
                    if (!globalId) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    if (!request.body) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    role = request.body.role;
                    if (!role) {
                        role = workspaceMember_1.default.ROLE_READER;
                    }
                    curDate = dateHandler_1.default.now();
                    if (!workspaceId) return [3, 5];
                    return [4, workspace_1.default.getById(workspaceId)];
                case 4:
                    workspaceItem = _b.sent();
                    return [3, 7];
                case 5: return [4, workspace_1.default.getDefault()];
                case 6:
                    workspaceItem = _b.sent();
                    _b.label = 7;
                case 7:
                    if (!workspaceItem) {
                        return [2, callback(null, { httpStatus: 404 })];
                    }
                    members = workspaceItem.members;
                    existMember = null;
                    if (members) {
                        for (i in members) {
                            if (members[i].globalId === globalId) {
                                members[i].role = role;
                                existMember = members[i];
                                break;
                            }
                        }
                    }
                    if (!existMember) {
                        return [2, callback(null, {
                                httpStatus: 400,
                                name: 'UniqueError',
                                objectType: 'Member'
                            })];
                    }
                    apiUpdateData = {
                        memberId: globalId,
                        role: role
                    };
                    NimbusSDK_1.default.getApi().workspaceMemberUpdate({ body: apiUpdateData, skipSyncHandlers: true }, function (err, res) { return __awaiter(_this, void 0, void 0, function () {
                        var updateQuery;
                        return __generator(this, function (_a) {
                            if (err || !res) {
                                return [2, callback(customError_1.default.itemUpdateError(), {
                                        httpStatus: 400,
                                        name: 'UniqueError',
                                        objectType: 'Member'
                                    })];
                            }
                            updateQuery = { globalId: workspaceItem.globalId };
                            workspace_1.default.update(updateQuery, syncPropsHandler_1.setUpdateProps({ members: members }, curDate), {}, function (err, updateCount) {
                                if (err || !updateCount) {
                                    return callback(customError_1.default.itemUpdateError(), {
                                        httpStatus: 400,
                                        name: 'UniqueError',
                                        objectType: 'Member'
                                    });
                                }
                                callback(null, workspaceMember_1.default.getResponseJson(existMember));
                            });
                            return [2];
                        });
                    }); });
                    return [2];
            }
        });
    });
}
exports.default = apiWorkspaceMemberUpdate;
