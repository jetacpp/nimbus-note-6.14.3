"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var urlParser_1 = __importDefault(require("../../../../utilities/urlParser"));
var MoveToWorkspace_1 = __importDefault(require("../../../../workspace/MoveToWorkspace"));
var ipcFunctions_1 = __importDefault(require("../../../../sync/ipc/ipcFunctions"));
function apiWorkspaceChange(request, callback) {
    var _this = this;
    var routeParams = urlParser_1.default.getPathParams(request.url);
    var workspaceId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;
    if (!workspaceId) {
        return callback(customError_1.default.wrongInput(), []);
    }
    if (!request.body) {
        return callback(customError_1.default.wrongInput(), {});
    }
    var _a = request.body, nodesIds = _a.nodesIds, parentId = _a.parentId, isMove = _a.isMove, nodeType = _a.nodeType;
    var toWorkspaceId = request.body.workspaceId;
    if (!nodesIds || !parentId || !toWorkspaceId) {
        return callback(customError_1.default.wrongInput(), {});
    }
    if (typeof (isMove) === 'undefined') {
        return callback(customError_1.default.wrongInput(), {});
    }
    if (!nodesIds.length) {
        return callback(customError_1.default.wrongInput(), {});
    }
    var queryData = { globalId: workspaceId };
    workspace_1.default.find(queryData, {}, function (err, workspaceItem) { return __awaiter(_this, void 0, void 0, function () {
        var _i, nodesIds_1, nodesId, response, respDesc;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (err || !workspaceItem) {
                        return [2, callback(null, { httpStatus: 404 })];
                    }
                    _i = 0, nodesIds_1 = nodesIds;
                    _a.label = 1;
                case 1:
                    if (!(_i < nodesIds_1.length)) return [3, 4];
                    nodesId = nodesIds_1[_i];
                    return [4, MoveToWorkspace_1.default.process({
                            fromWorkspaceId: workspaceId,
                            toWorkspaceId: toWorkspaceId,
                            globalId: nodesId,
                            isMove: isMove
                        })];
                case 2:
                    response = _a.sent();
                    if (response.err) {
                        respDesc = '';
                        if (response.errorDesc === 'contains_encrypted_notes') {
                            respDesc = 'HAVE_ENCRYPTED_NOTE';
                        }
                        if (respDesc) {
                            return [2, callback(null, {
                                    httpStatus: 400,
                                    message: {
                                        err: response.err,
                                        name: respDesc,
                                        httpStatus: 400
                                    },
                                    name: respDesc,
                                    response: {
                                        err: response.err,
                                        name: respDesc,
                                        httpStatus: 400
                                    },
                                    status: 400
                                })];
                        }
                        if (response.errorDesc === 'too_many_notes') {
                            return [2, callback(null, {
                                    httpStatus: 403,
                                    message: "nodeWorkspaceChangeError",
                                    err: response.err,
                                    name: 'LimitError',
                                    objectType: 'NotesNumber',
                                    errorDesc: response.errorDesc,
                                    newGlobalId: response.newGlobalId,
                                    isElectron: response.isElectron,
                                })];
                        }
                        if (response.errorDesc === 'org_suspended') {
                            return [2, callback(null, {
                                    httpStatus: 403,
                                    err: response.err,
                                    name: 'AccessDenied',
                                    objectType: 'note',
                                    reason: 'NoPrivilege',
                                    nodeType: nodeType,
                                    isMove: isMove,
                                    privilegesReduced: true,
                                    errorDesc: response.errorDesc,
                                    newGlobalId: response.newGlobalId,
                                    isElectron: response.isElectron,
                                })];
                        }
                        return [2, callback(null, {
                                httpStatus: 400,
                                message: "nodeWorkspaceChangeError",
                                err: response.err,
                                errorDesc: response.errorDesc,
                                newGlobalId: response.newGlobalId,
                                isElectron: response.isElectron,
                            })];
                    }
                    _a.label = 3;
                case 3:
                    _i++;
                    return [3, 1];
                case 4:
                    ipcFunctions_1.default.requestAutoSync({ workspaceId: workspaceId });
                    return [2, callback(null, [])];
            }
        });
    }); });
}
exports.default = apiWorkspaceChange;
