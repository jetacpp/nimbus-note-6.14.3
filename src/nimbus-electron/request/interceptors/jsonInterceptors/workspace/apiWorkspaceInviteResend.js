"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var NimbusSDK_1 = __importDefault(require("../../../../sync/nimbussdk/net/NimbusSDK"));
var state_1 = __importDefault(require("../../../../online/state"));
var instance_1 = __importDefault(require("../../../../window/instance"));
var Translations_1 = __importDefault(require("../../../../translations/Translations"));
var SelectedWorkspace_1 = __importDefault(require("../../../../workspace/SelectedWorkspace"));
var urlParser_1 = __importDefault(require("../../../../utilities/urlParser"));
var workspaceInvites_1 = __importDefault(require("../../../../db/models/workspaceInvites"));
function apiWorkspaceInviteResend(request, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var routeParams, globalId, workspaceId, workspaceItem, invites, existInvite, i;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!state_1.default.get()) {
                        if (instance_1.default.get()) {
                            instance_1.default.get().webContents.send('event:client:workspace:message:response', {
                                message: Translations_1.default.get('toast__workspace__member__update__offline'),
                            });
                        }
                        return [2, callback(null, { httpStatus: 404 })];
                    }
                    routeParams = urlParser_1.default.getPathParams(request.url);
                    globalId = typeof (routeParams[3] !== 'undefined') && routeParams[3] ? parseInt(routeParams[3]) : null;
                    if (!globalId) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    if (!request.body) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    return [4, SelectedWorkspace_1.default.getGlobalId()];
                case 1:
                    workspaceId = _a.sent();
                    if (!workspaceId) return [3, 3];
                    return [4, workspace_1.default.getById(workspaceId)];
                case 2:
                    workspaceItem = _a.sent();
                    return [3, 5];
                case 3: return [4, workspace_1.default.getDefault()];
                case 4:
                    workspaceItem = _a.sent();
                    _a.label = 5;
                case 5:
                    if (!workspaceItem) {
                        return [2, callback(null, { httpStatus: 404 })];
                    }
                    invites = workspaceItem.invites;
                    existInvite = null;
                    if (invites) {
                        for (i in invites) {
                            if (invites[i].id === globalId) {
                                existInvite = invites[i];
                                break;
                            }
                        }
                    }
                    if (!existInvite) {
                        return [2, callback(null, {
                                httpStatus: 400,
                                name: 'UniqueError',
                                objectType: 'Invite'
                            })];
                    }
                    NimbusSDK_1.default.getApi().workspaceInviteResend({ globalId: globalId, skipSyncHandlers: true }, function (err, res) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            if (err || !res) {
                                if (err === workspaceInvites_1.default.ERROR_LIMIT) {
                                    return [2, callback(customError_1.default.itemUpdateError(), { name: 'LimitError', httpStatus: 400 })];
                                }
                                else if (err === workspaceInvites_1.default.ERROR_UNIQUE) {
                                    return [2, callback(customError_1.default.itemUpdateError(), { name: 'UniqueError', httpStatus: 400 })];
                                }
                                else {
                                    return [2, callback(customError_1.default.itemUpdateError(), { httpStatus: 400 })];
                                }
                            }
                            callback(null, { invite: existInvite });
                            return [2];
                        });
                    }); });
                    return [2];
            }
        });
    });
}
exports.default = apiWorkspaceInviteResend;
