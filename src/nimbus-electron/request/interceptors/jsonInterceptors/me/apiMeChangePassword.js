"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var auth_1 = __importDefault(require("../../../../auth/auth"));
var user_1 = __importDefault(require("../../../../db/models/user"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
function apiMeChangePassword(request, callback) {
    var defaultErrorResponse = {
        'name': 'WrongPassword',
        'field': 'password'
    };
    if (!request.body.oldPassword || !request.body.newPassword) {
        return callback(customError_1.default.wrongInput(), defaultErrorResponse);
    }
    if (!request.authInfo) {
        return callback(customError_1.default.wrongInput(), defaultErrorResponse);
    }
    var curDate = dateHandler_1.default.now();
    var findQuery = { login: request.authInfo.email };
    user_1.default.find(findQuery, {}, function (err, userItem) {
        if (userItem && Object.keys(userItem).length) {
            var oldPasswordHash = auth_1.default.cryptPassword(request.body.oldPassword, userItem._id);
            var newPasswordHash = auth_1.default.cryptPassword(request.body.newPassword, userItem._id);
            if (oldPasswordHash === userItem.password) {
                user_1.default.update({ "userId": userItem.userId }, syncPropsHandler_1.setUpdateProps({
                    'password': newPasswordHash
                }, curDate), {}, function (err, count) {
                    if (count) {
                        callback(null, {});
                    }
                    else {
                        callback(customError_1.default.itemSaveError(), defaultErrorResponse);
                    }
                });
            }
            else {
                callback(null, defaultErrorResponse);
            }
        }
    });
}
exports.default = apiMeChangePassword;
