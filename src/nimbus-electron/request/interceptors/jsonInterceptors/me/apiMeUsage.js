"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_1 = __importDefault(require("../../../../db/models/user"));
function apiMeUsage(request, callback) {
    var findQuery = { userId: request.authInfo.userId };
    user_1.default.find(findQuery, {}, function (err, userItem) {
        if (userItem && Object.keys(userItem).length) {
            var apiMeUsage_1 = user_1.default.getPublicUsage(userItem);
            callback(null, apiMeUsage_1);
        }
    });
}
exports.default = apiMeUsage;
