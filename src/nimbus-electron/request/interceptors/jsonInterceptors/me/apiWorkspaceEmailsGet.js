"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var user_1 = __importDefault(require("../../../../db/models/user"));
function apiWorkspaceEmailsGet(request, callback) {
    if (!request.authInfo) {
        return callback(customError_1.default.wrongInput(), {});
    }
    var findQuery = { login: request.authInfo.email };
    user_1.default.find(findQuery, {}, function (err, userItem) {
        if (userItem && Object.keys(userItem).length) {
            var response = {};
            if (userItem.notesEmail) {
                response.notesEmail = user_1.default.getNotesEmailPart(userItem.notesEmail);
            }
            if (userItem.userId) {
                response.userId = userItem.userId;
            }
            response.workspaceId = "default";
            callback(null, response);
        }
        else {
            callback(customError_1.default.itemNotFound(), {});
        }
    });
}
exports.default = apiWorkspaceEmailsGet;
