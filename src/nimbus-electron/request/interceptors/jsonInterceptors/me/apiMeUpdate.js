"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var instance_1 = __importDefault(require("../../../../window/instance"));
var menu_1 = __importDefault(require("../../../../menu/menu"));
var Translations_1 = __importDefault(require("../../../../translations/Translations"));
var user_1 = __importDefault(require("../../../../db/models/user"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
function apiMeUpdate(request, callback) {
    if (!request.body.userId) {
        return callback(customError_1.default.wrongInput(), {});
    }
    if (!request.authInfo) {
        return callback(customError_1.default.wrongInput(), {});
    }
    var curDate = dateHandler_1.default.now();
    var saveUserData = { "language": request.body.language };
    var queryData = { email: request.authInfo.email };
    user_1.default.update(queryData, syncPropsHandler_1.setUpdateProps(saveUserData, curDate), {}, function (err, count) {
        if (err || !count) {
            return callback(customError_1.default.itemSaveError(), {});
        }
        if (instance_1.default.get()) {
            Translations_1.default.setLang(saveUserData.language);
            instance_1.default.get().webContents.send('event:change:language:response', {
                language: saveUserData.language,
                contextMenu: menu_1.default.getContextMenu(),
            });
            menu_1.default.update();
        }
        user_1.default.find(queryData, {}, function (err, userItem) {
            if (userItem && Object.keys(userItem).length) {
                var apiMe = user_1.default.getPublicData(userItem);
                callback(null, apiMe);
            }
            else {
                callback(customError_1.default.itemNotFound(), {});
            }
        });
    });
}
exports.default = apiMeUpdate;
