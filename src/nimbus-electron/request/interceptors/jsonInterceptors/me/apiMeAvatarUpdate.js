"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs-extra");
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var user_1 = __importDefault(require("../../../../db/models/user"));
var NimbusSDK_1 = __importDefault(require("../../../../sync/nimbussdk/net/NimbusSDK"));
var errorHandler_1 = __importDefault(require("../../../../utilities/errorHandler"));
var state_1 = __importDefault(require("../../../../online/state"));
var instance_1 = __importDefault(require("../../../../window/instance"));
var Translations_1 = __importDefault(require("../../../../translations/Translations"));
var config_runtime_1 = __importDefault(require("../../../../../config.runtime"));
var pdb_1 = __importDefault(require("../../../../../pdb"));
var AvatarSingleDonwloader_1 = __importDefault(require("../../../../sync/downlaoder/AvatarSingleDonwloader"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
function apiMeAvatarUpdate(request, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var file, saveData, exists;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!request.file) {
                        return [2, callback(null, { httpStatus: 500 })];
                    }
                    if (!state_1.default.get()) {
                        if (instance_1.default.get()) {
                            instance_1.default.get().webContents.send('event:client:workspace:message:response', {
                                message: Translations_1.default.get('toast__user_avatar__update__offline'),
                            });
                        }
                        return [2, callback(null, { httpStatus: 404 })];
                    }
                    file = request.file;
                    saveData = {
                        email: request.authInfo.email,
                        userId: request.authInfo.userId
                    };
                    return [4, fs.exists(config_runtime_1.default.nimbusAttachmentPath)];
                case 1:
                    exists = _a.sent();
                    if (!!exists) return [3, 3];
                    return [4, fs.mkdir(config_runtime_1.default.nimbusAttachmentPath)];
                case 2:
                    _a.sent();
                    _a.label = 3;
                case 3: return [4, prepareAttachLoad({ saveData: saveData, file: file }, callback)];
                case 4:
                    _a.sent();
                    return [2];
            }
        });
    });
}
exports.default = apiMeAvatarUpdate;
function prepareAttachLoad(inputData, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var exists;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!pdb_1.default.getClientAttachmentPath()) {
                        return [2, callback(customError_1.default.wrongAttachPath(), {})];
                    }
                    return [4, fs.exists(pdb_1.default.getClientAttachmentPath())];
                case 1:
                    exists = _a.sent();
                    if (exists) {
                        prepareAttachSave(inputData, callback);
                    }
                    else {
                        fs.mkdir(pdb_1.default.getClientAttachmentPath(), function () {
                            prepareAttachSave(inputData, callback);
                        });
                    }
                    return [2];
            }
        });
    });
}
function prepareAttachSave(inputData, callback) {
    var _this = this;
    var saveData = inputData.saveData, file = inputData.file;
    var email = saveData.email;
    var fileUploadData = {
        workspaceId: null,
        tempname: file.path,
        mime: file.mimetype || '',
        extension: file.originalname ? file.originalname.split('.').pop() : ''
    };
    NimbusSDK_1.default.getApi().filesPreupload(fileUploadData, function (err, tempName) { return __awaiter(_this, void 0, void 0, function () {
        var saveUserData;
        var _this = this;
        return __generator(this, function (_a) {
            if (err || !tempName) {
                errorHandler_1.default.log({
                    err: err, response: tempName,
                    description: "Upload problem => apiMeAvatarUpdate => filesPreupload",
                    data: inputData
                });
                return [2, callback(customError_1.default.itemSaveError(), { httpStatus: 500 })];
            }
            saveUserData = { avatar: { tempFileName: tempName } };
            NimbusSDK_1.default.getApi().updateUserInfo({ body: saveUserData, skipSyncHandlers: true }, function (err, res) {
                if (err || !res) {
                    return callback(customError_1.default.itemUpdateError(), { httpStatus: 500 });
                }
                var userId = res.user_id;
                var avatar = res.avatar;
                if (!avatar) {
                    return callback(customError_1.default.itemUpdateError(), { httpStatus: 500 });
                }
                var url = avatar.url, createdAt = avatar.createdAt, updatedAt = avatar.updatedAt;
                if (!url) {
                    return callback(customError_1.default.itemUpdateError(), { httpStatus: 500 });
                }
                var storedFileUUID = url.split("/").pop();
                if (!storedFileUUID) {
                    return callback(customError_1.default.itemUpdateError(), { httpStatus: 500 });
                }
                var targetPath = pdb_1.default.getClientAttachmentPath() + "/" + storedFileUUID;
                var curDate = dateHandler_1.default.now();
                user_1.default.find({ email: email }, {}, function (err, userInstance) { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (err || !userInstance) {
                                    return [2, callback(customError_1.default.itemFindError(), { httpStatus: 500 })];
                                }
                                return [4, AvatarSingleDonwloader_1.default.removeAvatarFile(userInstance.avatar)];
                            case 1:
                                _a.sent();
                                user_1.default.update({ email: email }, syncPropsHandler_1.setUpdateProps({ avatar: avatar }, curDate), {}, function (err, count) {
                                    if (err || !count) {
                                        return callback(customError_1.default.itemSaveError(), {});
                                    }
                                    try {
                                        fs.rename(file.path, targetPath, function (err) {
                                            if (err) {
                                                if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                                    console.log(err);
                                                }
                                            }
                                            callback(null, {
                                                createdAt: createdAt,
                                                updatedAt: updatedAt,
                                                storedFileUUID: "avatar/" + storedFileUUID,
                                                userId: userId
                                            });
                                        });
                                    }
                                    catch (e) {
                                        return callback(customError_1.default.itemUpdateError(), { httpStatus: 500 });
                                    }
                                });
                                return [2];
                        }
                    });
                }); });
            });
            return [2];
        });
    }); });
}
