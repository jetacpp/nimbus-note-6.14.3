"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_1 = __importDefault(require("../../../../db/models/user"));
var NimbusSDK_1 = __importDefault(require("../../../../sync/nimbussdk/net/NimbusSDK"));
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
function apiMeAvatarDelete(request, callback) {
    var findQuery = { userId: request.authInfo.userId };
    var curDate = dateHandler_1.default.now();
    user_1.default.find(findQuery, {}, function (err, userItem) {
        if (userItem && Object.keys(userItem).length) {
            var saveUserData = { avatar: null };
            NimbusSDK_1.default.getApi().updateUserInfo({ body: saveUserData, skipSyncHandlers: true }, function (err, res) {
                if (err || !res) {
                    return callback(customError_1.default.itemUpdateError(), { httpStatus: 500 });
                }
                user_1.default.update({
                    email: userItem.email ? userItem.email.toLowerCase() : ''
                }, syncPropsHandler_1.setUpdateProps({ avatar: null }, curDate), {}, function (err, count) {
                    if (err || !count) {
                        return callback(customError_1.default.itemSaveError(), {});
                    }
                    return callback(null, {
                        isFulfilled: false,
                        isRejected: false
                    });
                });
            });
        }
        else {
            return callback(null, {
                isFulfilled: false,
                isRejected: true
            });
        }
    });
}
exports.default = apiMeAvatarDelete;
