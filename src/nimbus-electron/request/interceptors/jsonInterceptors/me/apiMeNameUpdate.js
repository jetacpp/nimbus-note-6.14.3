"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var user_1 = __importDefault(require("../../../../db/models/user"));
var NimbusSDK_1 = __importDefault(require("../../../../sync/nimbussdk/net/NimbusSDK"));
var state_1 = __importDefault(require("../../../../online/state"));
var instance_1 = __importDefault(require("../../../../window/instance"));
var Translations_1 = __importDefault(require("../../../../translations/Translations"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
function apiMeNameUpdate(request, callback) {
    if (!state_1.default.get()) {
        if (instance_1.default.get()) {
            instance_1.default.get().webContents.send('event:client:workspace:message:response', {
                message: Translations_1.default.get('toast__user_name__update__offline'),
            });
        }
        return callback(null, { httpStatus: 404 });
    }
    var saveUserData = {};
    if (typeof (request.body.firstname) !== 'undefined') {
        saveUserData.firstname = request.body.firstname.trim();
    }
    if (typeof (request.body.lastname) !== 'undefined') {
        saveUserData.lastname = request.body.lastname.trim();
    }
    if (typeof (request.body.username) !== 'undefined') {
        saveUserData.username = request.body.username.trim();
    }
    if (!Object.keys(saveUserData).length) {
        return callback(null, {});
    }
    var curDate = dateHandler_1.default.now();
    var queryData = { email: request.authInfo.email };
    NimbusSDK_1.default.getApi().updateUserInfo({ body: saveUserData, skipSyncHandlers: true }, function (err, res) {
        if (err || !res) {
            return callback(customError_1.default.itemUpdateError(), { httpStatus: 500 });
        }
        user_1.default.update(queryData, syncPropsHandler_1.setUpdateProps(saveUserData, curDate), {}, function (err, count) {
            if (err || !count) {
                return callback(customError_1.default.itemSaveError(), {});
            }
            callback(null, {});
        });
    });
}
exports.default = apiMeNameUpdate;
