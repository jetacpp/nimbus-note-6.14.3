"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs-extra");
var config_runtime_1 = __importDefault(require("../../../../../config.runtime"));
var urlParser_1 = __importDefault(require("../../../../utilities/urlParser"));
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var item_1 = __importDefault(require("../../../../db/models/item"));
var text_1 = __importDefault(require("../../../../db/models/text"));
var todo_1 = __importDefault(require("../../../../db/models/todo"));
var attach_1 = __importDefault(require("../../../../db/models/attach"));
var noteTags_1 = __importDefault(require("../../../../db/models/noteTags"));
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var socketFunctions_1 = __importDefault(require("../../../../sync/socket/socketFunctions"));
var pdb_1 = __importDefault(require("../../../../../pdb"));
function apiItemRemove(request, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var routeParams, workspaceId, _a, globalId, filterParams, getTrashItemByGlobalId, getTrashItemsByParentId_1, trashNestedItems_1, getAllNestedItems_1, trashItem, trashNestedItemsIdList, t, trashNestedItem, removeItemAssetsQuery;
        var _this = this;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    routeParams = urlParser_1.default.getPathParams(request.url);
                    if (!typeof (routeParams[3] !== 'undefined')) return [3, 2];
                    return [4, workspace_1.default.getLocalId(routeParams[3])];
                case 1:
                    _a = _b.sent();
                    return [3, 3];
                case 2:
                    _a = null;
                    _b.label = 3;
                case 3:
                    workspaceId = _a;
                    globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;
                    filterParams = urlParser_1.default.getFilterParams(request.url) || {};
                    if (!globalId) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    if (!filterParams.test) return [3, 4];
                    deleteTestData({ workspaceId: workspaceId, globalId: globalId }, callback);
                    return [3, 7];
                case 4:
                    getTrashItemByGlobalId = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, globalId;
                                    return __generator(this, function (_a) {
                                        workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                                        item_1.default.find({ rootId: 'trash', 'globalId': globalId }, { workspaceId: workspaceId }, function (err, trashItem) {
                                            if (err) {
                                                return resolve(null);
                                            }
                                            resolve(trashItem);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    getTrashItemsByParentId_1 = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, parentId;
                                    return __generator(this, function (_a) {
                                        workspaceId = inputData.workspaceId, parentId = inputData.parentId;
                                        item_1.default.findAll({ rootId: 'trash', 'parentId': parentId }, { workspaceId: workspaceId }, function (err, itemList) {
                                            if (err) {
                                                return resolve([]);
                                            }
                                            resolve(itemList);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    trashNestedItems_1 = [];
                    getAllNestedItems_1 = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, trashItem, trashItemList, _a, _b, _i, k;
                        return __generator(this, function (_c) {
                            switch (_c.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, trashItem = inputData.trashItem;
                                    if (!trashItem) return [3, 5];
                                    trashNestedItems_1.push(trashItem);
                                    return [4, getTrashItemsByParentId_1({ workspaceId: workspaceId, parentId: trashItem.globalId })];
                                case 1:
                                    trashItemList = _c.sent();
                                    _a = [];
                                    for (_b in trashItemList)
                                        _a.push(_b);
                                    _i = 0;
                                    _c.label = 2;
                                case 2:
                                    if (!(_i < _a.length)) return [3, 5];
                                    k = _a[_i];
                                    if (!trashItemList.hasOwnProperty(k)) return [3, 4];
                                    return [4, getAllNestedItems_1({ workspaceId: workspaceId, trashItem: trashItemList[k] })];
                                case 3:
                                    _c.sent();
                                    _c.label = 4;
                                case 4:
                                    _i++;
                                    return [3, 2];
                                case 5: return [2];
                            }
                        });
                    }); };
                    return [4, getTrashItemByGlobalId({ workspaceId: workspaceId, globalId: globalId })];
                case 5:
                    trashItem = _b.sent();
                    return [4, getAllNestedItems_1({ workspaceId: workspaceId, trashItem: trashItem })];
                case 6:
                    _b.sent();
                    trashNestedItemsIdList = [];
                    for (t in trashNestedItems_1) {
                        trashNestedItem = trashNestedItems_1[t];
                        if (trashNestedItem.globalId === "default") {
                            continue;
                        }
                        trashNestedItemsIdList.push(trashNestedItem.globalId);
                        if (trashNestedItem.type === "note") {
                            removeItemAssetsQuery = { noteGlobalId: trashNestedItem.globalId };
                            attach_1.default.findAll(removeItemAssetsQuery, { workspaceId: workspaceId }, function (err, attachList) {
                                if (attachList && attachList.length) {
                                    var _loop_1 = function (n) {
                                        if (attachList.hasOwnProperty(n)) {
                                            var attachItem_1 = attachList[n];
                                            var removeAttachQuery = { globalId: attachItem_1.globalId };
                                            attach_1.default.remove(removeAttachQuery, { workspaceId: workspaceId }, function () { return __awaiter(_this, void 0, void 0, function () {
                                                var targetPath, exists, e_1;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            if (!pdb_1.default.getClientAttachmentPath()) return [3, 5];
                                                            targetPath = pdb_1.default.getClientAttachmentPath() + "/" + attachItem_1.storedFileUUID;
                                                            return [4, fs.exists(targetPath)];
                                                        case 1:
                                                            exists = _a.sent();
                                                            if (!exists) return [3, 5];
                                                            _a.label = 2;
                                                        case 2:
                                                            _a.trys.push([2, 4, , 5]);
                                                            return [4, fs.unlink(targetPath)];
                                                        case 3:
                                                            _a.sent();
                                                            return [3, 5];
                                                        case 4:
                                                            e_1 = _a.sent();
                                                            if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                                                console.log("Error => apiItemRemove => remove file: ", targetPath, e_1);
                                                            }
                                                            return [3, 5];
                                                        case 5: return [2];
                                                    }
                                                });
                                            }); });
                                        }
                                    };
                                    for (var n in attachList) {
                                        _loop_1(n);
                                    }
                                }
                            });
                            todo_1.default.removeByItemId(removeItemAssetsQuery, { workspaceId: workspaceId }, function () {
                            });
                            attach_1.default.removeByItemId(removeItemAssetsQuery, { workspaceId: workspaceId }, function () {
                            });
                            noteTags_1.default.removeByItemId(removeItemAssetsQuery, { workspaceId: workspaceId }, function () {
                            });
                            text_1.default.remove(removeItemAssetsQuery, { workspaceId: workspaceId }, function () {
                            });
                        }
                        item_1.default.remove({ globalId: trashNestedItem.globalId }, { workspaceId: workspaceId }, function () {
                        });
                    }
                    if (trashItem && !trashItem.offlineOnly) {
                        setTimeout(function () {
                            ipcMessage({ workspaceId: workspaceId });
                        }, 1000);
                    }
                    socketMessage({ workspaceId: workspaceId, globalIdList: trashNestedItemsIdList });
                    callback(null, {});
                    _b.label = 7;
                case 7: return [2];
            }
        });
    });
}
exports.default = apiItemRemove;
function deleteTestData(inputData, callback) {
    var _this = this;
    var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
    item_1.default.find({ globalId: globalId }, { workspaceId: workspaceId }, function (err, itemInstance) {
        if (!itemInstance) {
            if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                console.log("Error remove item not found: " + globalId);
            }
        }
        item_1.default.remove({ globalId: globalId }, { workspaceId: workspaceId }, function () {
            if (itemInstance && itemInstance.type === "note") {
                var removeItemAssetsQuery = { noteGlobalId: itemInstance.globalId };
                text_1.default.remove(removeItemAssetsQuery, { workspaceId: workspaceId }, function () {
                });
                todo_1.default.removeByItemId(removeItemAssetsQuery, { workspaceId: workspaceId }, function () {
                });
                attach_1.default.findAll(removeItemAssetsQuery, { workspaceId: workspaceId }, function (err, attachList) {
                    if (attachList && attachList.length) {
                        var _loop_2 = function (n) {
                            if (attachList.hasOwnProperty(n)) {
                                var attachItem_2 = attachList[n];
                                var removeAttachQuery = { globalId: attachItem_2.globalId };
                                attach_1.default.remove(removeAttachQuery, { workspaceId: workspaceId }, function () { return __awaiter(_this, void 0, void 0, function () {
                                    var targetPath, exists;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                if (!pdb_1.default.getClientAttachmentPath()) return [3, 2];
                                                targetPath = pdb_1.default.getClientAttachmentPath() + "/" + attachItem_2.storedFileUUID;
                                                return [4, fs.exists(targetPath)];
                                            case 1:
                                                exists = _a.sent();
                                                if (exists) {
                                                    try {
                                                        fs.unlink(targetPath, function () {
                                                        });
                                                    }
                                                    catch (e) {
                                                        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
                                                            console.log("Error => apiItemRemove => deleteTestData => remove file: ", targetPath, e);
                                                        }
                                                    }
                                                }
                                                _a.label = 2;
                                            case 2: return [2];
                                        }
                                    });
                                }); });
                            }
                        };
                        for (var n in attachList) {
                            _loop_2(n);
                        }
                    }
                });
                attach_1.default.removeByItemId(removeItemAssetsQuery, { workspaceId: workspaceId }, function () {
                });
                noteTags_1.default.removeByItemId(removeItemAssetsQuery, { workspaceId: workspaceId }, function () {
                });
            }
            callback(null, item_1.default.getResponseJson(itemInstance));
        });
    });
}
function ipcMessage(inputData) {
}
function socketMessage(inputData) {
    socketFunctions_1.default.sendItemRemoveMessage(inputData);
}
