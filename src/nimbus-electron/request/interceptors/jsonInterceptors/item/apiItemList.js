"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var urlParser_1 = __importDefault(require("../../../../utilities/urlParser"));
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var item_1 = __importDefault(require("../../../../db/models/item"));
var attach_1 = __importDefault(require("../../../../db/models/attach"));
var todo_1 = __importDefault(require("../../../../db/models/todo"));
var tag_1 = __importDefault(require("../../../../db/models/tag"));
var noteTags_1 = __importDefault(require("../../../../db/models/noteTags"));
var text_1 = __importDefault(require("../../../../db/models/text"));
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
var state_1 = __importDefault(require("../../../../online/state"));
var NimbusSDK_1 = __importDefault(require("../../../../sync/nimbussdk/net/NimbusSDK"));
function apiItemList(request, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var routeParams, queryParams, filterParams, limitParams, orderParams, workspaceId, _a, findQuery, searchTitle, filterTitleRegexp, searchParams, query, regexp, buildExcerpts, getMatches, searchData, filterItemsByTags, tagsFindQuery;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    routeParams = urlParser_1.default.getPathParams(request.url);
                    queryParams = urlParser_1.default.getQueryParams(request.url) || {};
                    filterParams = urlParser_1.default.getFilterParams(request.url) || {};
                    limitParams = urlParser_1.default.getLimitParams(request.url) || {};
                    orderParams = urlParser_1.default.getOrderParams(request.url) || {};
                    if (!typeof (routeParams[3] !== 'undefined')) return [3, 2];
                    return [4, workspace_1.default.getLocalId(routeParams[3])];
                case 1:
                    _a = _b.sent();
                    return [3, 3];
                case 2:
                    _a = null;
                    _b.label = 3;
                case 3:
                    workspaceId = _a;
                    findQuery = {};
                    if (filterParams.globalId) {
                        if (filterParams.globalId['$like']) {
                            findQuery.globalId = { '$regex': filterParams.globalId['$like'].replace("%", ".*") };
                        }
                        else {
                            findQuery.globalId = filterParams.globalId;
                        }
                    }
                    if (filterParams.type) {
                        findQuery.type = filterParams.type;
                    }
                    if (queryParams.rootId) {
                        findQuery.rootId = queryParams.rootId;
                    }
                    if (filterParams.parentId) {
                        findQuery.parentId = filterParams.parentId;
                    }
                    if (filterParams.favorite) {
                        findQuery.favorite = true;
                    }
                    if (filterParams.title && filterParams.title.$like) {
                        searchTitle = filterParams.title.$like.replace(new RegExp('\%', 'g'), '');
                        filterTitleRegexp = new RegExp(urlParser_1.default.escapeRegExp(searchTitle), 'im');
                        findQuery.title = { '$regex': filterTitleRegexp };
                        findQuery.onlyTitleSearch = true;
                    }
                    searchParams = {
                        excerpts: {},
                        matches: {},
                        searchByStringIdList: null,
                        searchByTagIdList: null,
                    };
                    if (!(queryParams.query && queryParams.query.trim())) return [3, 5];
                    query = queryParams.query.trim();
                    regexp = new RegExp(urlParser_1.default.escapeRegExp(query), 'im');
                    findQuery.title = { '$regex': regexp };
                    if (!state_1.default.get()) return [3, 5];
                    buildExcerpts = queryParams.buildExcerpts, getMatches = queryParams.getMatches;
                    return [4, searchNotes({ workspaceId: workspaceId, query: query, buildExcerpts: buildExcerpts, getMatches: getMatches })];
                case 4:
                    searchData = _b.sent();
                    if (Object.keys(searchData).length && searchData.global_ids.length) {
                        searchParams.searchByStringIdList = searchData.global_ids;
                        searchParams.excerpts = searchData.excerpts;
                        searchParams.matches = searchData.matches;
                    }
                    else {
                        searchParams.searchByStringIdList = [];
                        searchParams.excerpts = {};
                        searchParams.matches = {};
                    }
                    _b.label = 5;
                case 5:
                    filterItemsByTags = "";
                    if (filterParams.tag) {
                        if (typeof (filterParams.tag["$in"]) !== "undefined" && filterParams.tag["$in"].length) {
                            filterItemsByTags = filterParams.tag;
                        }
                        else {
                            filterItemsByTags = { "$in": [filterParams.tag] };
                        }
                    }
                    if (filterItemsByTags) {
                        tagsFindQuery = { tag: filterItemsByTags };
                        tag_1.default.findAll(tagsFindQuery, { workspaceId: workspaceId }, function (err, tagList) {
                            var findTagIdList = [];
                            if (tagList && tagList.length) {
                                for (var d in tagList) {
                                    if (tagList.hasOwnProperty(d)) {
                                        findTagIdList.push(tagList[d].globalId);
                                    }
                                }
                                var noteTagsFindQuery = { tagGlobalId: { "$in": findTagIdList } };
                                noteTags_1.default.findAll(noteTagsFindQuery, { workspaceId: workspaceId }, function (err, noteTagsItems) {
                                    var filterNotesIdObject = {};
                                    if (noteTagsItems && noteTagsItems.length) {
                                        for (var t in noteTagsItems) {
                                            if (noteTagsItems.hasOwnProperty(t)) {
                                                filterNotesIdObject[noteTagsItems[t].noteGlobalId] = noteTagsItems[t].noteGlobalId;
                                            }
                                        }
                                    }
                                    var filterNotesIdList = [];
                                    for (var a in filterNotesIdObject) {
                                        if (filterNotesIdObject.hasOwnProperty(a)) {
                                            filterNotesIdList.push(filterNotesIdObject[a]);
                                        }
                                    }
                                    searchParams.searchByTagIdList = filterNotesIdList;
                                    if (!findQuery.title) {
                                        findQuery.globalId = { "$in": filterNotesIdList };
                                    }
                                    prepareFindItems({ workspaceId: workspaceId, queryParams: queryParams, findQuery: findQuery, limitParams: limitParams, orderParams: orderParams, searchParams: searchParams }, callback);
                                });
                            }
                            else {
                                callback(customError_1.default.itemNotFound(), []);
                            }
                        });
                    }
                    else {
                        prepareFindItems({ workspaceId: workspaceId, queryParams: queryParams, findQuery: findQuery, limitParams: limitParams, orderParams: orderParams, searchParams: searchParams }, callback);
                    }
                    return [2];
            }
        });
    });
}
exports.default = apiItemList;
function prepareFindItems(inputData, callback) {
    var workspaceId = inputData.workspaceId, queryParams = inputData.queryParams, findQuery = inputData.findQuery, limitParams = inputData.limitParams, orderParams = inputData.orderParams, searchParams = inputData.searchParams;
    var params = {};
    if (Object.keys(limitParams).length) {
        params["range"] = limitParams;
    }
    if (Object.keys(orderParams).length) {
        params["order"] = orderParams;
    }
    params = urlParser_1.default.getSortAndLimitParams(params);
    if (searchParams) {
        var searchByStringIdList = searchParams.searchByStringIdList, searchByTagIdList = searchParams.searchByTagIdList;
        if (searchByStringIdList) {
            params["searchParams"] = searchParams;
        }
        else if (searchByTagIdList) {
            params["searchParams"] = searchParams;
        }
    }
    if (findQuery.rootId === "trash" && findQuery.parentId === "root" && findQuery.parentId) {
        delete findQuery["parentId"];
    }
    if (findQuery.rootId !== "trash" && findQuery.parentId === "root") {
        delete findQuery["parentId"];
        findQuery.rootId = "root";
    }
    var curDate = dateHandler_1.default.now();
    item_1.default.find({ globalId: "default" }, { workspaceId: workspaceId }, function (err, itemInstance) {
        if (itemInstance && (Object.keys(itemInstance).length || findQuery.rootId === "trash")) {
            findItems({ workspaceId: workspaceId, queryParams: queryParams, findQuery: findQuery, params: params }, callback);
        }
        else {
            var itemDefault = item_1.default.getDefaultModel();
            item_1.default.add(syncPropsHandler_1.setUpdateProps(itemDefault, curDate), { workspaceId: workspaceId }, function (err, itemInstance) {
                if (itemInstance && Object.keys(itemInstance).length) {
                    findItems({ workspaceId: workspaceId, queryParams: queryParams, findQuery: findQuery, params: params }, callback);
                }
                else {
                    callback(customError_1.default.itemSaveError(), []);
                }
            });
        }
    });
}
function findItems(inputData, callback) {
    var _this = this;
    var workspaceId = inputData.workspaceId, queryParams = inputData.queryParams, findQuery = inputData.findQuery, params = inputData.params;
    if (findQuery && findQuery.type === "folder") {
        if (!params)
            params = {};
        params.orderField = "title";
        params.orderDirection = 1;
    }
    if (findQuery.type === "folder" && findQuery.rootId === "root") {
        findQuery.rootId = { '$ne': "trash" };
    }
    params.workspaceId = workspaceId;
    item_1.default.findAll(findQuery, params, function (err, itemList) { return __awaiter(_this, void 0, void 0, function () {
        var itemsGlobalIdList, searchParams, globalIdList, _a, i, currentItemIndex_1, prepareItemCounters_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    if (!itemList.length) {
                        return [2, callback(null, [])];
                    }
                    if (queryParams.onlyId) {
                        itemsGlobalIdList = itemList.map(function (itemInstance) {
                            return itemInstance.globalId;
                        });
                        return [2, callback(null, itemsGlobalIdList)];
                    }
                    searchParams = params.searchParams;
                    if (!(searchParams && searchParams.excerpts && searchParams.matches)) return [3, 2];
                    globalIdList = itemList.map(function (itemInstance) { return (itemInstance.globalId); });
                    _a = searchParams;
                    return [4, getShortTextList({ globalIdList: globalIdList, workspaceId: workspaceId })];
                case 1:
                    _a.texts = _b.sent();
                    _b.label = 2;
                case 2:
                    if (queryParams.counters) {
                        for (i in itemList) {
                            itemList[i].cntNotes = 0;
                            if (itemList[i].type === "note") {
                                itemList[i].todosClosedCount = 0;
                                itemList[i].todosCount = 0;
                                itemList[i].attachmentsCount = 0;
                            }
                        }
                        currentItemIndex_1 = 0;
                        prepareItemCounters_1 = function () {
                            if (typeof (itemList[currentItemIndex_1]) !== "undefined") {
                                if (itemList[currentItemIndex_1].type === "folder") {
                                    item_1.default.count({
                                        "parentId": itemList[currentItemIndex_1].globalId,
                                        "type": "note"
                                    }, { workspaceId: workspaceId }, function (err, childrenNotesCount) {
                                        itemList[currentItemIndex_1].childrenNotesCount = childrenNotesCount;
                                        currentItemIndex_1++;
                                        prepareItemCounters_1();
                                    });
                                }
                                else if (itemList[currentItemIndex_1].type === "note") {
                                    todo_1.default.count({
                                        "noteGlobalId": itemList[currentItemIndex_1].globalId,
                                        "checked": false
                                    }, { workspaceId: workspaceId }, function (err, itemTodoCount) {
                                        itemList[currentItemIndex_1].todosCount = itemTodoCount;
                                        todo_1.default.count({
                                            "noteGlobalId": itemList[currentItemIndex_1].globalId,
                                            "checked": true
                                        }, { workspaceId: workspaceId }, function (err, itemTodoClosedCount) {
                                            itemList[currentItemIndex_1].todosClosedCount = itemTodoClosedCount;
                                            var attachmentsCounterQuery = { "noteGlobalId": itemList[currentItemIndex_1].globalId };
                                            attachmentsCounterQuery.inList = true;
                                            attach_1.default.count(attachmentsCounterQuery, { workspaceId: workspaceId }, function (err, itemAttachCount) {
                                                itemList[currentItemIndex_1].attachmentsCount = itemAttachCount;
                                                currentItemIndex_1++;
                                                prepareItemCounters_1();
                                            });
                                        });
                                    });
                                }
                                else {
                                    currentItemIndex_1++;
                                    prepareItemCounters_1();
                                }
                            }
                            else {
                                return callback(null, item_1.default.getResponseListJson(itemList, searchParams));
                            }
                        };
                        prepareItemCounters_1();
                    }
                    else {
                        return [2, callback(null, item_1.default.getResponseListJson(itemList, searchParams))];
                    }
                    return [2];
            }
        });
    }); });
}
function getShortTextList(_a) {
    var globalIdList = _a.globalIdList, workspaceId = _a.workspaceId;
    return new Promise(function (resolve) {
        text_1.default.findAll({ noteGlobalId: { '$in': globalIdList } }, { workspaceId: workspaceId }, function (err, textList) {
            if (err || !textList) {
                return resolve({});
            }
            var textData = {};
            for (var _i = 0, textList_1 = textList; _i < textList_1.length; _i++) {
                var textInstance = textList_1[_i];
                textData[textInstance.noteGlobalId] = textInstance.textShort;
            }
            return resolve(textData);
        });
    });
}
function searchNotes(body) {
    return new Promise(function (resolve) {
        NimbusSDK_1.default.getApi().notesSearch(body, function (err, res) {
            if (err) {
                return resolve({});
            }
            return resolve(res);
        });
    });
}
