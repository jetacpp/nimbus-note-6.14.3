"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
var urlParser_1 = __importDefault(require("../../../../utilities/urlParser"));
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var item_1 = __importDefault(require("../../../../db/models/item"));
var text_1 = __importDefault(require("../../../../db/models/text"));
var textEditor_1 = __importDefault(require("../../../../db/models/textEditor"));
var attach_1 = __importDefault(require("../../../../db/models/attach"));
var todo_1 = __importDefault(require("../../../../db/models/todo"));
var noteTags_1 = __importDefault(require("../../../../db/models/noteTags"));
var generatorHandler_1 = __importDefault(require("../../../../utilities/generatorHandler"));
var socketFunctions_1 = __importDefault(require("../../../../sync/socket/socketFunctions"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
function apiItemDuplicate(request, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var routeParams, _a, nodesIds, parentId, suffix, workspaceId, globalId, _b, nestedItems, getItemByGlobalId, getItemsByParentId, getAllNestedItems, copyItem, copyAttachments, copyTodos, copyNoteTags, copyText, copyTextEditor, copyItemWithAssets, itemInstance, copyItemRoot, _i, nestedItems_1, nestedItem;
        var _this = this;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    routeParams = urlParser_1.default.getPathParams(request.url);
                    if (!request.body.nodesIds) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    if (!request.body.nodesIds.length) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    if (!request.body.parentId) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    if (!request.body.workspaceId) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    _a = request.body, nodesIds = _a.nodesIds, parentId = _a.parentId, suffix = _a.suffix, workspaceId = _a.workspaceId;
                    globalId = request.body.nodesIds[0];
                    if (!typeof (workspaceId !== 'undefined')) return [3, 2];
                    return [4, workspace_1.default.getLocalId(workspaceId)];
                case 1:
                    _b = _c.sent();
                    return [3, 3];
                case 2:
                    _b = null;
                    _c.label = 3;
                case 3:
                    workspaceId = _b;
                    suffix = suffix || '(copy)';
                    nestedItems = [];
                    getItemByGlobalId = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, globalId;
                                    return __generator(this, function (_a) {
                                        workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                                        item_1.default.find({ globalId: globalId }, { workspaceId: workspaceId }, function (err, itemInstance) {
                                            if (err) {
                                                return resolve(null);
                                            }
                                            resolve(itemInstance);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    getItemsByParentId = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, parentId;
                                    return __generator(this, function (_a) {
                                        workspaceId = inputData.workspaceId, parentId = inputData.parentId;
                                        item_1.default.findAll({ parentId: parentId }, { workspaceId: workspaceId }, function (err, itemList) {
                                            if (err) {
                                                return resolve([]);
                                            }
                                            resolve(itemList);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    getAllNestedItems = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, itemInstance, itemList, _a, _b, _i, k;
                        return __generator(this, function (_c) {
                            switch (_c.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, itemInstance = inputData.itemInstance;
                                    if (!itemInstance) return [3, 5];
                                    nestedItems.push(itemInstance);
                                    return [4, getItemsByParentId({ workspaceId: workspaceId, parentId: itemInstance.globalId })];
                                case 1:
                                    itemList = _c.sent();
                                    _a = [];
                                    for (_b in itemList)
                                        _a.push(_b);
                                    _i = 0;
                                    _c.label = 2;
                                case 2:
                                    if (!(_i < _a.length)) return [3, 5];
                                    k = _a[_i];
                                    if (!itemList.hasOwnProperty(k)) return [3, 4];
                                    return [4, getAllNestedItems({ workspaceId: workspaceId, itemInstance: itemList[k] })];
                                case 3:
                                    _c.sent();
                                    _c.label = 4;
                                case 4:
                                    _i++;
                                    return [3, 2];
                                case 5: return [2];
                            }
                        });
                    }); };
                    copyItem = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, copyInstance, suffix, curDate, saveItemData;
                                    return __generator(this, function (_a) {
                                        workspaceId = inputData.workspaceId, copyInstance = inputData.copyInstance, suffix = inputData.suffix;
                                        curDate = dateHandler_1.default.now();
                                        saveItemData = __assign({}, copyInstance);
                                        saveItemData._id = generatorHandler_1.default.randomString(16);
                                        saveItemData.globalId = saveItemData._id;
                                        if (suffix) {
                                            saveItemData.title = saveItemData.title + " " + suffix;
                                        }
                                        saveItemData.createdAt = curDate;
                                        saveItemData.dateAdded = curDate;
                                        saveItemData.dateUpdated = curDate;
                                        saveItemData.updatedAt = curDate;
                                        saveItemData.dateUpdatedUser = curDate;
                                        saveItemData.editnote = 1;
                                        saveItemData.text_version = 1;
                                        saveItemData.syncDate = null;
                                        saveItemData.needSync = true;
                                        item_1.default.add(syncPropsHandler_1.setUpdateProps(saveItemData, curDate), { workspaceId: workspaceId }, function (err, itemInstance) {
                                            if (err || !itemInstance) {
                                                return resolve(null);
                                            }
                                            return resolve(itemInstance);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    copyAttachments = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, noteGlobalId, newNoteGlobalId;
                                    return __generator(this, function (_a) {
                                        workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, newNoteGlobalId = inputData.newNoteGlobalId;
                                        attach_1.default.findAll({ noteGlobalId: noteGlobalId }, { workspaceId: workspaceId }, function (err, attachList) {
                                            if (attachList && attachList.length) {
                                                for (var _i = 0, attachList_1 = attachList; _i < attachList_1.length; _i++) {
                                                    var attachItem = attachList_1[_i];
                                                    var curDate = dateHandler_1.default.now();
                                                    var saveAttachData = __assign({}, attachItem);
                                                    saveAttachData._id = generatorHandler_1.default.randomString(16);
                                                    saveAttachData.globalId = saveAttachData._id;
                                                    saveAttachData.noteGlobalId = newNoteGlobalId;
                                                    saveAttachData.dateAdded = curDate;
                                                    saveAttachData.dateUpdated = curDate;
                                                    saveAttachData.syncDate = null;
                                                    saveAttachData.needSync = true;
                                                    attach_1.default.add(syncPropsHandler_1.setUpdateProps(saveAttachData, curDate), { workspaceId: workspaceId }, function () { });
                                                }
                                            }
                                            return resolve(true);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    copyTodos = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, noteGlobalId, newNoteGlobalId;
                                    return __generator(this, function (_a) {
                                        workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, newNoteGlobalId = inputData.newNoteGlobalId;
                                        todo_1.default.findAll({ noteGlobalId: noteGlobalId }, { workspaceId: workspaceId }, function (err, todoList) {
                                            if (todoList && todoList.length) {
                                                for (var _i = 0, todoList_1 = todoList; _i < todoList_1.length; _i++) {
                                                    var todoItem = todoList_1[_i];
                                                    var curDate = dateHandler_1.default.now();
                                                    var saveTodoData = __assign({}, todoItem);
                                                    saveTodoData._id = generatorHandler_1.default.randomString(16);
                                                    saveTodoData.globalId = saveTodoData._id;
                                                    saveTodoData.noteGlobalId = newNoteGlobalId;
                                                    saveTodoData.date = curDate;
                                                    saveTodoData.dateAdded = curDate;
                                                    saveTodoData.dateUpdated = curDate;
                                                    saveTodoData.syncDate = null;
                                                    saveTodoData.needSync = true;
                                                    todo_1.default.add(syncPropsHandler_1.setUpdateProps(saveTodoData, curDate), { workspaceId: workspaceId }, function () { });
                                                }
                                            }
                                            return resolve(true);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    copyNoteTags = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, noteGlobalId, newNoteGlobalId;
                                    return __generator(this, function (_a) {
                                        workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, newNoteGlobalId = inputData.newNoteGlobalId;
                                        noteTags_1.default.findAll({ noteGlobalId: noteGlobalId }, { workspaceId: workspaceId }, function (err, noteTagsList) {
                                            if (noteTagsList && noteTagsList.length) {
                                                for (var _i = 0, noteTagsList_1 = noteTagsList; _i < noteTagsList_1.length; _i++) {
                                                    var noteTagItem = noteTagsList_1[_i];
                                                    var curDate = dateHandler_1.default.now();
                                                    var saveNoteTagData = __assign({}, noteTagItem);
                                                    saveNoteTagData._id = generatorHandler_1.default.randomString(16);
                                                    saveNoteTagData.globalId = saveNoteTagData._id;
                                                    saveNoteTagData.noteGlobalId = newNoteGlobalId;
                                                    saveNoteTagData.dateAdded = curDate;
                                                    saveNoteTagData.dateUpdated = curDate;
                                                    saveNoteTagData.syncDate = null;
                                                    saveNoteTagData.needSync = true;
                                                    noteTags_1.default.add(syncPropsHandler_1.setUpdateProps(saveNoteTagData, curDate), { workspaceId: workspaceId }, function () { });
                                                }
                                            }
                                            return resolve(true);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    copyText = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, noteGlobalId, newNoteGlobalId;
                                    return __generator(this, function (_a) {
                                        workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, newNoteGlobalId = inputData.newNoteGlobalId;
                                        text_1.default.findAll({ noteGlobalId: noteGlobalId }, { workspaceId: workspaceId }, function (err, textList) {
                                            if (textList && textList.length) {
                                                for (var _i = 0, textList_1 = textList; _i < textList_1.length; _i++) {
                                                    var textItem = textList_1[_i];
                                                    var curDate = dateHandler_1.default.now();
                                                    var saveTextData = __assign({}, textItem);
                                                    saveTextData._id = generatorHandler_1.default.randomString(16);
                                                    saveTextData.globalId = saveTextData._id;
                                                    saveTextData.noteGlobalId = newNoteGlobalId;
                                                    saveTextData.dateAdded = curDate;
                                                    saveTextData.dateUpdated = curDate;
                                                    saveTextData.syncDate = null;
                                                    saveTextData.needSync = true;
                                                    text_1.default.add(syncPropsHandler_1.setUpdateProps(saveTextData, curDate), { workspaceId: workspaceId }, function () { });
                                                }
                                            }
                                            return resolve(true);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    copyTextEditor = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                                    var workspaceId, noteGlobalId, newNoteGlobalId;
                                    return __generator(this, function (_a) {
                                        workspaceId = inputData.workspaceId, noteGlobalId = inputData.noteGlobalId, newNoteGlobalId = inputData.newNoteGlobalId;
                                        textEditor_1.default.findAll({ noteGlobalId: noteGlobalId }, { workspaceId: workspaceId }, function (err, textList) {
                                            if (textList && textList.length) {
                                                for (var _i = 0, textList_2 = textList; _i < textList_2.length; _i++) {
                                                    var textItem = textList_2[_i];
                                                    var curDate = dateHandler_1.default.now();
                                                    var saveTextData = __assign({}, textItem);
                                                    saveTextData._id = generatorHandler_1.default.randomString(16);
                                                    saveTextData.noteGlobalId = newNoteGlobalId;
                                                    saveTextData.globalId = saveTextData._id;
                                                    saveTextData.dateUpdated = curDate;
                                                    saveTextData.syncDate = null;
                                                    saveTextData.needSync = true;
                                                    textEditor_1.default.add(syncPropsHandler_1.setUpdateProps(saveTextData, curDate), { workspaceId: workspaceId }, function () { });
                                                }
                                            }
                                            return resolve(true);
                                        });
                                        return [2];
                                    });
                                }); })];
                        });
                    }); };
                    copyItemWithAssets = function (inputData) { return __awaiter(_this, void 0, void 0, function () {
                        var workspaceId, copyInstance, suffix, itemInstance, copyQuery;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    workspaceId = inputData.workspaceId, copyInstance = inputData.copyInstance, suffix = inputData.suffix;
                                    return [4, copyItem({ workspaceId: workspaceId, copyInstance: copyInstance, suffix: suffix })];
                                case 1:
                                    itemInstance = _a.sent();
                                    if (!(copyInstance.type === "note")) return [3, 7];
                                    copyQuery = {
                                        workspaceId: workspaceId,
                                        noteGlobalId: copyInstance.globalId,
                                        newNoteGlobalId: itemInstance.globalId
                                    };
                                    return [4, copyAttachments(copyQuery)];
                                case 2:
                                    _a.sent();
                                    return [4, copyTodos(copyQuery)];
                                case 3:
                                    _a.sent();
                                    return [4, copyNoteTags(copyQuery)];
                                case 4:
                                    _a.sent();
                                    return [4, copyText(copyQuery)];
                                case 5:
                                    _a.sent();
                                    return [4, copyTextEditor(copyQuery)];
                                case 6:
                                    _a.sent();
                                    socketMessage({ workspaceId: workspaceId, itemInstance: itemInstance });
                                    _a.label = 7;
                                case 7: return [2, itemInstance];
                            }
                        });
                    }); };
                    return [4, getItemByGlobalId({ workspaceId: workspaceId, globalId: globalId })];
                case 4:
                    itemInstance = _c.sent();
                    if (!itemInstance) {
                        return [2, callback(customError_1.default.itemFindError(), {})];
                    }
                    return [4, getAllNestedItems({ workspaceId: workspaceId, itemInstance: itemInstance })];
                case 5:
                    _c.sent();
                    return [4, copyItemWithAssets({ workspaceId: workspaceId, copyInstance: itemInstance, suffix: suffix })];
                case 6:
                    copyItemRoot = _c.sent();
                    if (!copyItemRoot) {
                        return [2, callback(customError_1.default.itemSaveError(), {})];
                    }
                    _i = 0, nestedItems_1 = nestedItems;
                    _c.label = 7;
                case 7:
                    if (!(_i < nestedItems_1.length)) return [3, 10];
                    nestedItem = nestedItems_1[_i];
                    return [4, copyItemWithAssets({ workspaceId: workspaceId, copyInstance: nestedItem })];
                case 8:
                    _c.sent();
                    _c.label = 9;
                case 9:
                    _i++;
                    return [3, 7];
                case 10:
                    if (itemInstance && !itemInstance.offlineOnly) {
                    }
                    callback(null, {});
                    return [2];
            }
        });
    });
}
exports.default = apiItemDuplicate;
function ipcMessage(inputData) {
}
function socketMessage(inputData) {
    var workspaceId = inputData.workspaceId, itemInstance = inputData.itemInstance;
    socketFunctions_1.default.sendItemUpdateMessage({
        workspaceId: workspaceId,
        globalId: itemInstance.globalId
    });
    socketFunctions_1.default.sendItemsCountMessage({
        workspaceId: workspaceId,
        globalId: itemInstance.globalId,
        parentId: itemInstance.parentId,
        type: itemInstance.type
    });
}
