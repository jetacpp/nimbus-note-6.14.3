"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var item_1 = __importDefault(require("../../../../db/models/item"));
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
var socketFunctions_1 = __importDefault(require("../../../../sync/socket/socketFunctions"));
var urlParser_1 = __importDefault(require("../../../../utilities/urlParser"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
function apiItemUpdate(request, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var routeParams, _a, notesIds, parentId, workspaceId, _b, curDate, err, res, _i, notesIds_1, globalId, moveData;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    routeParams = urlParser_1.default.getPathParams(request.url);
                    _a = request.body, notesIds = _a.notesIds, parentId = _a.parentId;
                    if (!notesIds) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    if (!notesIds.length) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    if (!parentId) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    if (!typeof (routeParams[3] !== 'undefined')) return [3, 2];
                    return [4, workspace_1.default.getLocalId(routeParams[3])];
                case 1:
                    _b = _c.sent();
                    return [3, 3];
                case 2:
                    _b = null;
                    _c.label = 3;
                case 3:
                    workspaceId = _b;
                    curDate = dateHandler_1.default.now();
                    err = null, res = {};
                    _i = 0, notesIds_1 = notesIds;
                    _c.label = 4;
                case 4:
                    if (!(_i < notesIds_1.length)) return [3, 7];
                    globalId = notesIds_1[_i];
                    return [4, moveItemByGlobalId({
                            workspaceId: workspaceId,
                            globalId: globalId,
                            parentId: parentId,
                            curDate: curDate
                        })];
                case 5:
                    moveData = _c.sent();
                    if (!err && moveData.err) {
                        err = moveData.err;
                    }
                    res = moveData.res;
                    _c.label = 6;
                case 6:
                    _i++;
                    return [3, 4];
                case 7: return [2, callback(err, res)];
            }
        });
    });
}
exports.default = apiItemUpdate;
var getItemsByParentId = function (inputData) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2, new Promise(function (resolve) {
                var workspaceId = inputData.workspaceId, globalId = inputData.globalId, type = inputData.type;
                var queryParentData = { parentId: globalId };
                if (type) {
                    queryParentData.type = type;
                }
                item_1.default.findAll(queryParentData, { workspaceId: workspaceId }, function (err, itemInstances) {
                    if (err || !itemInstances) {
                        return resolve([]);
                    }
                    return resolve(itemInstances);
                });
            })];
    });
}); };
function moveItemByGlobalId(inputData) {
    return __awaiter(this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            return [2, new Promise(function (resolve) {
                    var workspaceId = inputData.workspaceId, globalId = inputData.globalId, parentId = inputData.parentId, curDate = inputData.curDate;
                    var saveItemData = {
                        globalId: globalId,
                        parentId: parentId,
                        "syncDate": curDate,
                        "needSync": true
                    };
                    var queryData = { globalId: saveItemData.globalId };
                    item_1.default.find(queryData, { workspaceId: workspaceId }, function (err, itemInstance) { return __awaiter(_this, void 0, void 0, function () {
                        var type;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (err || !itemInstance) {
                                        return [2, resolve({
                                                err: customError_1.default.itemFindError(),
                                                res: {}
                                            })];
                                    }
                                    type = itemInstance.type || 'note';
                                    if (parentId) {
                                        if (type === "note") {
                                            saveItemData.parentId = (parentId === 'root') ? 'default' : parentId;
                                        }
                                        else if (type === "folder") {
                                            saveItemData.parentId = saveItemData.parentId ? saveItemData.parentId : 'root';
                                        }
                                        saveItemData.rootId = (parentId === 'trash') ? 'trash' : 'root';
                                        if (parentId !== "trash") {
                                            saveItemData.rootParentId = parentId;
                                        }
                                    }
                                    if (!itemInstance) {
                                        return [2, resolve({
                                                err: customError_1.default.itemFindError(),
                                                res: {}
                                            })];
                                    }
                                    if (!Object.keys(itemInstance).length) {
                                        return [2, resolve({
                                                err: customError_1.default.itemFindError(),
                                                res: {}
                                            })];
                                    }
                                    return [4, updateSaveItemOfflineOnlyProperty({ workspaceId: workspaceId, saveItemData: saveItemData, itemInstance: itemInstance })];
                                case 1:
                                    saveItemData = _a.sent();
                                    item_1.default.update(queryData, syncPropsHandler_1.setUpdateProps(saveItemData, curDate), { workspaceId: workspaceId }, function (err, count) { return __awaiter(_this, void 0, void 0, function () {
                                        var parentFolder, updateFoldersList, getUpdateItems, _loop_1, _i, updateFoldersList_1, folderListItemGlobalId;
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    if (!count) {
                                                        return [2, resolve({
                                                                err: customError_1.default.itemSaveError(),
                                                                res: {}
                                                            })];
                                                    }
                                                    return [4, getItemByGlobalId({ workspaceId: workspaceId, globalId: itemInstance.parentId, type: 'folder' })];
                                                case 1:
                                                    parentFolder = _a.sent();
                                                    if (type === 'note' && parentFolder) {
                                                        socketMessage({ workspaceId: workspaceId, itemInstance: parentFolder });
                                                    }
                                                    updateFolderNotes({
                                                        workspaceId: workspaceId, folderGlobalId: itemInstance.globalId, updateData: {
                                                            "rootId": saveItemData.rootId,
                                                            "offlineOnly": saveItemData.offlineOnly
                                                        }
                                                    });
                                                    updateFoldersList = [];
                                                    getUpdateItems = function (itemData) { return __awaiter(_this, void 0, void 0, function () {
                                                        var childFolders, _i, childFolders_1, childFolder;
                                                        return __generator(this, function (_a) {
                                                            switch (_a.label) {
                                                                case 0: return [4, getItemsByParentId({ workspaceId: workspaceId, globalId: itemData.globalId, type: 'folder' })];
                                                                case 1:
                                                                    childFolders = _a.sent();
                                                                    if (!(childFolders && childFolders.length)) return [3, 5];
                                                                    _i = 0, childFolders_1 = childFolders;
                                                                    _a.label = 2;
                                                                case 2:
                                                                    if (!(_i < childFolders_1.length)) return [3, 5];
                                                                    childFolder = childFolders_1[_i];
                                                                    updateFoldersList.push(childFolder.globalId);
                                                                    return [4, getUpdateItems(childFolder)];
                                                                case 3:
                                                                    _a.sent();
                                                                    _a.label = 4;
                                                                case 4:
                                                                    _i++;
                                                                    return [3, 2];
                                                                case 5: return [2];
                                                            }
                                                        });
                                                    }); };
                                                    return [4, getUpdateItems(saveItemData)];
                                                case 2:
                                                    _a.sent();
                                                    _loop_1 = function (folderListItemGlobalId) {
                                                        var folderQueryData = { globalId: folderListItemGlobalId };
                                                        var folderUpdateData = {
                                                            "rootId": saveItemData.rootId,
                                                            "offlineOnly": saveItemData.offlineOnly,
                                                            "syncDate": curDate,
                                                            "needSync": true
                                                        };
                                                        item_1.default.update(folderQueryData, syncPropsHandler_1.setUpdateProps(folderUpdateData, curDate), { workspaceId: workspaceId }, function (err, count) {
                                                            if (count) {
                                                                updateFolderNotes({ workspaceId: workspaceId, folderGlobalId: folderQueryData.globalId, updateData: folderUpdateData });
                                                            }
                                                        });
                                                    };
                                                    for (_i = 0, updateFoldersList_1 = updateFoldersList; _i < updateFoldersList_1.length; _i++) {
                                                        folderListItemGlobalId = updateFoldersList_1[_i];
                                                        _loop_1(folderListItemGlobalId);
                                                    }
                                                    item_1.default.find(queryData, { workspaceId: workspaceId }, function (err, itemInstance) {
                                                        if (itemInstance && Object.keys(itemInstance).length) {
                                                            itemInstance.cntNotes = 0;
                                                            if (!itemInstance.offlineOnly) {
                                                                ipcMessage({ workspaceId: workspaceId });
                                                            }
                                                            socketMessage({ workspaceId: workspaceId, itemInstance: itemInstance });
                                                            return resolve({
                                                                err: null,
                                                                res: {}
                                                            });
                                                        }
                                                        else {
                                                            return resolve({
                                                                err: customError_1.default.itemNotFound(),
                                                                res: {}
                                                            });
                                                        }
                                                    });
                                                    return [2];
                                            }
                                        });
                                    }); });
                                    return [2];
                            }
                        });
                    }); });
                })];
        });
    });
}
function getItemByGlobalId(inputData) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2, new Promise(function (resolve) {
                    var workspaceId = inputData.workspaceId, globalId = inputData.globalId, type = inputData.type;
                    var queryParentData = { globalId: globalId };
                    if (type) {
                        queryParentData.type = type;
                    }
                    item_1.default.find(queryParentData, { workspaceId: workspaceId }, function (err, itemInstance) {
                        if (err || !itemInstance) {
                            return resolve(itemInstance);
                        }
                        return resolve(itemInstance);
                    });
                })];
        });
    });
}
function updateFolderNotes(inputData) {
    var workspaceId = inputData.workspaceId, folderGlobalId = inputData.folderGlobalId, updateData = inputData.updateData;
    var notesQueryData = {
        parentId: folderGlobalId,
        type: 'note'
    };
    item_1.default.findAll(notesQueryData, { workspaceId: workspaceId }, function (err, noteItems) {
        if (updateData && noteItems && noteItems.length) {
            var curDate = dateHandler_1.default.now();
            for (var u in noteItems) {
                if (noteItems.hasOwnProperty(u)) {
                    var globalId = noteItems[u].globalId;
                    var rootId = updateData.rootId;
                    var offlineOnly = updateData.offlineOnly;
                    if (globalId && rootId) {
                        var noteQueryData = {
                            "globalId": globalId
                        };
                        var noteSaveData = {
                            "syncDate": curDate,
                            "needSync": true,
                            "rootId": rootId,
                            "offlineOnly": offlineOnly
                        };
                        item_1.default.update(noteQueryData, syncPropsHandler_1.setUpdateProps(noteSaveData, curDate), { workspaceId: workspaceId }, function () {
                        });
                    }
                }
            }
        }
    });
}
function updateSaveItemOfflineOnlyProperty(inputData) {
    return __awaiter(this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                    var workspaceId, saveItemData, itemInstance, newParentFolder, newParentFolderId;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                workspaceId = inputData.workspaceId, saveItemData = inputData.saveItemData, itemInstance = inputData.itemInstance;
                                newParentFolder = null;
                                newParentFolderId = itemInstance.parentId;
                                if (itemInstance.type === "note") {
                                    if (typeof (saveItemData.parentId) !== "undefined") {
                                        newParentFolderId = saveItemData.parentId;
                                    }
                                }
                                if (!newParentFolderId) return [3, 3];
                                if (!(newParentFolderId === "root" || newParentFolderId === "trash")) return [3, 1];
                                saveItemData.offlineOnly = !!itemInstance.offlineOnly;
                                return [3, 3];
                            case 1: return [4, getItemByGlobalId({ workspaceId: workspaceId, globalId: newParentFolderId, type: 'folder' })];
                            case 2:
                                newParentFolder = _a.sent();
                                if (newParentFolder) {
                                    if (itemInstance.type === "note") {
                                        if (typeof (saveItemData.offlineOnly) === "undefined") {
                                            saveItemData.offlineOnly = !!newParentFolder.offlineOnly;
                                        }
                                    }
                                    else if (itemInstance.type === "folder") {
                                        if (typeof (saveItemData.offlineOnly) === "undefined") {
                                            saveItemData.offlineOnly = !!itemInstance.offlineOnly;
                                        }
                                        else {
                                            if (!saveItemData.offlineOnly) {
                                                saveItemData.offlineOnly = !!newParentFolder.offlineOnly;
                                            }
                                        }
                                    }
                                }
                                _a.label = 3;
                            case 3:
                                resolve(saveItemData);
                                return [2];
                        }
                    });
                }); })];
        });
    });
}
function ipcMessage(inputData) {
}
function socketMessage(inputData) {
    var workspaceId = inputData.workspaceId, itemInstance = inputData.itemInstance;
    socketFunctions_1.default.sendItemUpdateMessage({
        workspaceId: workspaceId,
        globalId: itemInstance.globalId
    });
    socketFunctions_1.default.sendItemsCountMessage({
        workspaceId: workspaceId,
        globalId: itemInstance.globalId,
        parentId: itemInstance.parentId,
        type: itemInstance.type
    });
}
