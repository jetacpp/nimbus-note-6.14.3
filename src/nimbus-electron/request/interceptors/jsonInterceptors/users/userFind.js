"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var urlParser_1 = __importDefault(require("./../../../../utilities/urlParser"));
var customError_1 = __importDefault(require("./../../../../utilities/customError"));
var user_1 = __importDefault(require("./../../../../db/models/user"));
function userFind(request, callback) {
    var filterParams = urlParser_1.default.getFilterParams(request.url) || {};
    if (!filterParams.email) {
        return callback(customError_1.default.wrongInput(), {});
    }
    var findQuery = { login: filterParams.email.toLowerCase() };
    user_1.default.find(findQuery, {}, function (err, userItem) {
        if (userItem && Object.keys(userItem).length) {
            callback(null, [user_1.default.getPublicData(userItem)]);
        }
        else {
            callback(customError_1.default.itemNotFound(), {});
        }
    });
}
exports.default = userFind;
