"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var user_1 = __importDefault(require("../../../../db/models/user"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
function apiUserSettingsUpdate(request, callback) {
    if (!request.body.dateTimeLocale) {
        return callback(customError_1.default.wrongInput(), {});
    }
    if (!request.authInfo) {
        return callback(customError_1.default.wrongInput(), {});
    }
    var curDate = dateHandler_1.default.now();
    var saveUserData = { dateTimeLocale: request.body.dateTimeLocale };
    var queryData = { email: request.authInfo.email };
    user_1.default.update(queryData, syncPropsHandler_1.setUpdateProps(saveUserData, curDate), {}, function (err, count) {
        if (err || !count) {
            return callback(customError_1.default.itemSaveError(), {});
        }
        callback(null, {});
    });
}
exports.default = apiUserSettingsUpdate;
