"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var urlParser_1 = __importDefault(require("../../../../utilities/urlParser"));
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var orgs_1 = __importDefault(require("../../../../db/models/orgs"));
var SelectedWorkspace_1 = __importDefault(require("../../../../workspace/SelectedWorkspace"));
var workspace_1 = __importDefault(require("../../../../db/models/workspace"));
var auth_1 = __importDefault(require("../../../../auth/auth"));
function apiUserFindById(request, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var routeParams, userId, workspace;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    routeParams = urlParser_1.default.getPathParams(request.url);
                    userId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;
                    return [4, SelectedWorkspace_1.default.get()];
                case 1:
                    workspace = _a.sent();
                    if (!request.authInfo || !userId || !workspace) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    workspace_1.default.find({ globalId: workspace.globalId }, {}, function (err, workspaceInstance) { return __awaiter(_this, void 0, void 0, function () {
                        var userInfo, orgInstance, isBusinessOrg, members, findWorkspaceMember, findUser;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (err || !workspaceInstance) {
                                        return [2, callback(customError_1.default.itemNotFound(), [])];
                                    }
                                    return [4, auth_1.default.getUserAsync()];
                                case 1:
                                    userInfo = _a.sent();
                                    return [4, orgs_1.default.getById(workspaceInstance.orgId)];
                                case 2:
                                    orgInstance = _a.sent();
                                    if (!orgInstance) {
                                        isBusinessOrg = workspaceInstance.orgId && workspaceInstance.orgId.charAt(0) === 'b';
                                        if (!isBusinessOrg && workspaceInstance.org) {
                                            orgInstance = workspaceInstance.org;
                                        }
                                    }
                                    if (!!orgInstance) return [3, 4];
                                    return [4, orgs_1.default.getDefaultOrg()];
                                case 3:
                                    orgInstance = _a.sent();
                                    _a.label = 4;
                                case 4:
                                    members = workspaceInstance.members;
                                    findWorkspaceMember = members.find(function (workspaceMember) {
                                        return workspaceMember && workspaceMember.user && workspaceMember.user.id.toString() === userId;
                                    });
                                    findUser = findWorkspaceMember ? findWorkspaceMember.user : null;
                                    if (!findUser && orgInstance && orgInstance.type === orgs_1.default.TYPE_PRIVATE) {
                                        if (orgInstance.user && orgInstance.user.email) {
                                            if (orgInstance.user.email.toLowerCase() === userInfo.email) {
                                                findUser = {
                                                    id: orgInstance.user.id ? orgInstance.user.id : userInfo.id,
                                                    username: userInfo.username ? userInfo.username : '',
                                                    displayName: userInfo.username ? userInfo.username : '',
                                                    email: userInfo.email ? userInfo.email.toLowerCase() : '',
                                                    defaultEncryptionKeyId: null,
                                                    languages: userInfo.languages ? userInfo.languages : [],
                                                    firstname: userInfo.firstname ? userInfo.firstname : '',
                                                    lastname: userInfo.lastname ? userInfo.lastname : '',
                                                    avatar: userInfo.avatar && userInfo.avatar.url ? userInfo.avatar.url : null,
                                                };
                                            }
                                            else {
                                                findUser = {
                                                    id: orgInstance.user.id ? orgInstance.user.id : '',
                                                    username: orgInstance.user.username ? orgInstance.user.username : '',
                                                    displayName: orgInstance.user.username ? orgInstance.user.username : '',
                                                    email: orgInstance.user.email ? orgInstance.user.email.toLowerCase() : '',
                                                    defaultEncryptionKeyId: null,
                                                    languages: orgInstance.user.languages ? orgInstance.user.languages : [],
                                                    firstname: orgInstance.user.firstname ? orgInstance.user.firstname : '',
                                                    lastname: orgInstance.user.lastname ? orgInstance.user.lastname : '',
                                                    avatar: orgInstance.user.avatar ? orgInstance.user.avatar : null,
                                                };
                                            }
                                        }
                                    }
                                    if (!findUser) {
                                        return [2, callback(customError_1.default.itemNotFound(), {})];
                                    }
                                    return [2, callback(null, findUser)];
                            }
                        });
                    }); });
                    return [2];
            }
        });
    });
}
exports.default = apiUserFindById;
