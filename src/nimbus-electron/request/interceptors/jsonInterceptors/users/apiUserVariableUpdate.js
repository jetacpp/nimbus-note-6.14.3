"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customError_1 = __importDefault(require("../../../../utilities/customError"));
var user_1 = __importDefault(require("../../../../db/models/user"));
var syncPropsHandler_1 = require("../../../../utilities/syncPropsHandler");
var dateHandler_1 = __importDefault(require("../../../../utilities/dateHandler"));
var auth_1 = __importDefault(require("../../../../auth/auth"));
var urlParser_1 = __importDefault(require("../../../../utilities/urlParser"));
var NimbusSDK_1 = __importDefault(require("../../../../sync/nimbussdk/net/NimbusSDK"));
function apiUserVariableUpdate(request, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var routeParams, varName, varValue, curDate, authInfo, userVariables, saveUserData, queryData;
        var _a;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    if (!request.authInfo) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    routeParams = urlParser_1.default.getPathParams(request.url);
                    varName = typeof (routeParams[4] !== 'undefined') ? decodeURI(routeParams[4]) : null;
                    if (!varName) {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    if (typeof (request.body.value) !== 'undefined') {
                        varValue = request.body.value;
                    }
                    if (typeof (varValue) === 'undefined') {
                        return [2, callback(customError_1.default.wrongInput(), {})];
                    }
                    curDate = dateHandler_1.default.now();
                    return [4, auth_1.default.fetchActualUserAsync()];
                case 1:
                    authInfo = _b.sent();
                    userVariables = authInfo && authInfo.variables ? authInfo.variables : user_1.default.getDefaultVariables();
                    saveUserData = { variables: __assign(__assign({}, userVariables), (_a = {}, _a[varName] = varValue, _a)) };
                    queryData = { email: request.authInfo.email };
                    user_1.default.update(queryData, syncPropsHandler_1.setUpdateProps(saveUserData, curDate), {}, function (err, count) {
                        if (err || !count) {
                            return callback(customError_1.default.itemSaveError(), {});
                        }
                        var apiUpdateData = { key: varName, value: varValue };
                        NimbusSDK_1.default.getApi().updateUserVariable({ body: apiUpdateData, skipSyncHandlers: true }, function (err, res) {
                            if (err || !res) {
                            }
                            callback(null, varValue);
                        });
                    });
                    return [2];
            }
        });
    });
}
exports.default = apiUserVariableUpdate;
