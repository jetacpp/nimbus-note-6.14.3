"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var apiMe_1 = __importDefault(require("./jsonInterceptors/me/apiMe"));
var apiMeUsage_1 = __importDefault(require("./jsonInterceptors/me/apiMeUsage"));
var apiUserProfession_1 = __importDefault(require("./jsonInterceptors/me/apiUserProfession"));
var apiWorkspaceInfoGet_1 = __importDefault(require("./jsonInterceptors/me/apiWorkspaceInfoGet"));
var apiWorkspaceUsageGet_1 = __importDefault(require("./jsonInterceptors/me/apiWorkspaceUsageGet"));
var apiWorkspaceEmailsGet_1 = __importDefault(require("./jsonInterceptors/me/apiWorkspaceEmailsGet"));
var apiProTrial_1 = __importDefault(require("./jsonInterceptors/me/apiProTrial"));
var apiMeNameUpdate_1 = __importDefault(require("./jsonInterceptors/me/apiMeNameUpdate"));
var apiMeAvatarUpdate_1 = __importDefault(require("./jsonInterceptors/me/apiMeAvatarUpdate"));
var apiMeUpdate_1 = __importDefault(require("./jsonInterceptors/me/apiMeUpdate"));
var apiMeChangePassword_1 = __importDefault(require("./jsonInterceptors/me/apiMeChangePassword"));
var apiMeLogout_1 = __importDefault(require("./jsonInterceptors/me/apiMeLogout"));
var apiSync_1 = __importDefault(require("./jsonInterceptors/me/apiSync"));
var apiSyncUpdate_1 = __importDefault(require("./jsonInterceptors/me/apiSyncUpdate"));
var apiAppSettings_1 = __importDefault(require("./jsonInterceptors/app/apiAppSettings"));
var apiAppSettingsUpdate_1 = __importDefault(require("./jsonInterceptors/app/apiAppSettingsUpdate"));
var apiAvatarPreview_1 = __importDefault(require("./jsonInterceptors/me/apiAvatarPreview"));
var apiMeAvatarDelete_1 = __importDefault(require("./jsonInterceptors/me/apiMeAvatarDelete"));
var userIgnoreIntro_1 = __importDefault(require("./jsonInterceptors/users/userIgnoreIntro"));
var userIgnoreSharesInfo_1 = __importDefault(require("./jsonInterceptors/users/userIgnoreSharesInfo"));
var userNoteAppearance_1 = __importDefault(require("./jsonInterceptors/users/userNoteAppearance"));
var apiUserOtp_1 = __importDefault(require("./jsonInterceptors/users/apiUserOtp"));
var apiUserOtpUpdate_1 = __importDefault(require("./jsonInterceptors/users/apiUserOtpUpdate"));
var apiUserOtpRemove_1 = __importDefault(require("./jsonInterceptors/users/apiUserOtpRemove"));
var apiUserOtpIssue_1 = __importDefault(require("./jsonInterceptors/users/apiUserOtpIssue"));
var apiMentionsWorkspace_1 = __importDefault(require("./jsonInterceptors/mention/apiMentionsWorkspace"));
var userSettings_1 = __importDefault(require("./jsonInterceptors/users/userSettings"));
var userSettingsUpdate_1 = __importDefault(require("./jsonInterceptors/users/userSettingsUpdate"));
var apiUserVariableUpdate_1 = __importDefault(require("./jsonInterceptors/users/apiUserVariableUpdate"));
var apiPremium_1 = __importDefault(require("./jsonInterceptors/premium/apiPremium"));
var apiWorkspacePremium_1 = __importDefault(require("./jsonInterceptors/premium/apiWorkspacePremium"));
var apiLimitChecker_1 = __importDefault(require("./jsonInterceptors/limit/apiLimitChecker"));
var apiOrganizationLimits_1 = __importDefault(require("./jsonInterceptors/limit/apiOrganizationLimits"));
var apiItemExport_1 = __importDefault(require("./jsonInterceptors/export/apiItemExport"));
var apiPreviewList_1 = __importDefault(require("./jsonInterceptors/preview/apiPreviewList"));
var apiPreviewRemove_1 = __importDefault(require("./jsonInterceptors/preview/apiPreviewRemove"));
var apiPreviewUpdate_1 = __importDefault(require("./jsonInterceptors/preview/apiPreviewUpdate"));
var apiShare_1 = __importDefault(require("./jsonInterceptors/share/apiShare"));
var apiShareUpdate_1 = __importDefault(require("./jsonInterceptors/share/apiShareUpdate"));
var apiClientRecent_1 = __importDefault(require("./jsonInterceptors/client/apiClientRecent"));
var apiNoteTodoList_1 = __importDefault(require("./jsonInterceptors/todo/apiNoteTodoList"));
var apiTodoOrder_1 = __importDefault(require("./jsonInterceptors/todo/apiTodoOrder"));
var apiTodoList_1 = __importDefault(require("./jsonInterceptors/todo/apiTodoList"));
var apiTodoRemove_1 = __importDefault(require("./jsonInterceptors/todo/apiTodoRemove"));
var apiTodoUpdate_1 = __importDefault(require("./jsonInterceptors/todo/apiTodoUpdate"));
var apiTodo_1 = __importDefault(require("./jsonInterceptors/todo/apiTodo"));
var apiTodoOrderUpdate_1 = __importDefault(require("./jsonInterceptors/todo/apiTodoOrderUpdate"));
var apiItemList_1 = __importDefault(require("./jsonInterceptors/item/apiItemList"));
var apiItemRemove_1 = __importDefault(require("./jsonInterceptors/item/apiItemRemove"));
var apiItemParentId_1 = __importDefault(require("./jsonInterceptors/item/apiItemParentId"));
var apiItemUpdate_1 = __importDefault(require("./jsonInterceptors/item/apiItemUpdate"));
var apiItem_1 = __importDefault(require("./jsonInterceptors/item/apiItem"));
var apiTextList_1 = __importDefault(require("./jsonInterceptors/text/apiTextList"));
var apiTextUpdate_1 = __importDefault(require("./jsonInterceptors/text/apiTextUpdate"));
var apiText_1 = __importDefault(require("./jsonInterceptors/text/apiText"));
var apiTextTokenUpdate_1 = __importDefault(require("./jsonInterceptors/text/apiTextTokenUpdate"));
var apiTagList_1 = __importDefault(require("./jsonInterceptors/tag/apiTagList"));
var apiTagRemove_1 = __importDefault(require("./jsonInterceptors/tag/apiTagRemove"));
var apiTagUpdate_1 = __importDefault(require("./jsonInterceptors/tag/apiTagUpdate"));
var apiNoteTagsList_1 = __importDefault(require("./jsonInterceptors/tag/apiNoteTagsList"));
var apiNoteTagsRemove_1 = __importDefault(require("./jsonInterceptors/tag/apiNoteTagsRemove"));
var apiNoteTagsUpdate_1 = __importDefault(require("./jsonInterceptors/tag/apiNoteTagsUpdate"));
var apiNotesTagsUpdate_1 = __importDefault(require("./jsonInterceptors/tag/apiNotesTagsUpdate"));
var apiAttachList_1 = __importDefault(require("./jsonInterceptors/attach/apiAttachList"));
var apiAttach_1 = __importDefault(require("./jsonInterceptors/attach/apiAttach"));
var apiAttachDownload_1 = __importDefault(require("./jsonInterceptors/attach/apiAttachDownload"));
var apiAttachPreview_1 = __importDefault(require("./jsonInterceptors/attach/apiAttachPreview"));
var apiAttachRemove_1 = __importDefault(require("./jsonInterceptors/attach/apiAttachRemove"));
var apiAttachUpdate_1 = __importDefault(require("./jsonInterceptors/attach/apiAttachUpdate"));
var apiAttachCreate_1 = __importDefault(require("./jsonInterceptors/attach/apiAttachCreate"));
var apiAnnotation_1 = __importDefault(require("./jsonInterceptors/annotation/apiAnnotation"));
var apiAnnotations_1 = __importDefault(require("./jsonInterceptors/annotation/apiAnnotations"));
var apiNoteRemindersList_1 = __importDefault(require("./jsonInterceptors/reminder/apiNoteRemindersList"));
var apiNoteReminderRemove_1 = __importDefault(require("./jsonInterceptors/reminder/apiNoteReminderRemove"));
var apiNoteReminderUpdate_1 = __importDefault(require("./jsonInterceptors/reminder/apiNoteReminderUpdate"));
var apiItemDuplicate_1 = __importDefault(require("./jsonInterceptors/item/apiItemDuplicate"));
var userFindById_1 = __importDefault(require("./jsonInterceptors/users/userFindById"));
var userFind_1 = __importDefault(require("./jsonInterceptors/users/userFind"));
var apiInvitesList_1 = __importDefault(require("./jsonInterceptors/workspace/apiInvitesList"));
var apiWorkspaceInviteAdd_1 = __importDefault(require("./jsonInterceptors/workspace/apiWorkspaceInviteAdd"));
var apiWorkspaceInviteResend_1 = __importDefault(require("./jsonInterceptors/workspace/apiWorkspaceInviteResend"));
var apiWorkspaceInviteUpdate_1 = __importDefault(require("./jsonInterceptors/workspace/apiWorkspaceInviteUpdate"));
var apiWorkspaceInviteRemove_1 = __importDefault(require("./jsonInterceptors/workspace/apiWorkspaceInviteRemove"));
var apiWorkspaceChange_1 = __importDefault(require("./jsonInterceptors/workspace/apiWorkspaceChange"));
var apiMembersList_1 = __importDefault(require("./jsonInterceptors/workspace/apiMembersList"));
var apiWorkspaceMemberUpdate_1 = __importDefault(require("./jsonInterceptors/workspace/apiWorkspaceMemberUpdate"));
var apiWorkspaceMemberRemove_1 = __importDefault(require("./jsonInterceptors/workspace/apiWorkspaceMemberRemove"));
var apiWorkspaceEmail_1 = __importDefault(require("./jsonInterceptors/workspace/apiWorkspaceEmail"));
var apiWorkspacesList_1 = __importDefault(require("./jsonInterceptors/workspace/apiWorkspacesList"));
var apiWorkspaceUpdate_1 = __importDefault(require("./jsonInterceptors/workspace/apiWorkspaceUpdate"));
var apiWorkspaceRemove_1 = __importDefault(require("./jsonInterceptors/workspace/apiWorkspaceRemove"));
exports.default = {
    returnEmptyJson: function (callback) {
        callback('[]');
    },
    apiMe: apiMe_1.default,
    apiMeUsage: apiMeUsage_1.default,
    apiUserProfession: apiUserProfession_1.default,
    apiWorkspaceInfoGet: apiWorkspaceInfoGet_1.default,
    apiWorkspaceUsageGet: apiWorkspaceUsageGet_1.default,
    apiWorkspaceEmailsGet: apiWorkspaceEmailsGet_1.default,
    apiProTrial: apiProTrial_1.default,
    apiMeNameUpdate: apiMeNameUpdate_1.default,
    apiMeAvatarUpdate: apiMeAvatarUpdate_1.default,
    apiMeUpdate: apiMeUpdate_1.default,
    apiMeChangePassword: apiMeChangePassword_1.default,
    apiMeLogout: apiMeLogout_1.default,
    apiSync: apiSync_1.default,
    apiSyncUpdate: apiSyncUpdate_1.default,
    apiAppSettings: apiAppSettings_1.default,
    apiAppSettingsUpdate: apiAppSettingsUpdate_1.default,
    apiAvatarPreview: apiAvatarPreview_1.default,
    apiMeAvatarDelete: apiMeAvatarDelete_1.default,
    apiUserIgnoreIntro: userIgnoreIntro_1.default,
    apiUserIgnoreSharesInfo: userIgnoreSharesInfo_1.default,
    apiUserNoteAppearance: userNoteAppearance_1.default,
    apiUserOtp: apiUserOtp_1.default,
    apiUserOtpUpdate: apiUserOtpUpdate_1.default,
    apiUserOtpRemove: apiUserOtpRemove_1.default,
    apiUserOtpIssue: apiUserOtpIssue_1.default,
    apiMentionsWorkspace: apiMentionsWorkspace_1.default,
    apiUserSettings: userSettings_1.default,
    apiUserSettingsUpdate: userSettingsUpdate_1.default,
    apiUserVariableUpdate: apiUserVariableUpdate_1.default,
    apiPremium: apiPremium_1.default,
    apiWorkspacePremium: apiWorkspacePremium_1.default,
    apiLimitChecker: apiLimitChecker_1.default,
    apiOrganizationLimits: apiOrganizationLimits_1.default,
    apiItemExport: apiItemExport_1.default,
    apiPreviewList: apiPreviewList_1.default,
    apiPreviewRemove: apiPreviewRemove_1.default,
    apiPreviewUpdate: apiPreviewUpdate_1.default,
    apiShare: apiShare_1.default,
    apiShareUpdate: apiShareUpdate_1.default,
    apiClientRecent: apiClientRecent_1.default,
    apiNoteTodoList: apiNoteTodoList_1.default,
    apiTodoOrder: apiTodoOrder_1.default,
    apiTodoList: apiTodoList_1.default,
    apiTodoRemove: apiTodoRemove_1.default,
    apiTodoUpdate: apiTodoUpdate_1.default,
    apiTodo: apiTodo_1.default,
    apiTodoOrderUpdate: apiTodoOrderUpdate_1.default,
    apiItemList: apiItemList_1.default,
    apiItemRemove: apiItemRemove_1.default,
    apiItemParentId: apiItemParentId_1.default,
    apiItemUpdate: apiItemUpdate_1.default,
    apiItem: apiItem_1.default,
    apiTextList: apiTextList_1.default,
    apiTextUpdate: apiTextUpdate_1.default,
    apiText: apiText_1.default,
    apiTextTokenUpdate: apiTextTokenUpdate_1.default,
    apiTagList: apiTagList_1.default,
    apiTagRemove: apiTagRemove_1.default,
    apiTagUpdate: apiTagUpdate_1.default,
    apiNoteTagsList: apiNoteTagsList_1.default,
    apiNoteTagsRemove: apiNoteTagsRemove_1.default,
    apiNoteTagsUpdate: apiNoteTagsUpdate_1.default,
    apiNotesTagsUpdate: apiNotesTagsUpdate_1.default,
    apiAttachList: apiAttachList_1.default,
    apiAttach: apiAttach_1.default,
    apiAttachPreview: apiAttachPreview_1.default,
    apiAttachDownload: apiAttachDownload_1.default,
    apiAttachRemove: apiAttachRemove_1.default,
    apiAttachUpdate: apiAttachUpdate_1.default,
    apiAttachCreate: apiAttachCreate_1.default,
    apiAnnotation: apiAnnotation_1.default,
    apiAnnotations: apiAnnotations_1.default,
    apiNoteRemindersList: apiNoteRemindersList_1.default,
    apiNoteReminderRemove: apiNoteReminderRemove_1.default,
    apiNoteReminderUpdate: apiNoteReminderUpdate_1.default,
    apiItemDuplicate: apiItemDuplicate_1.default,
    userFindById: userFindById_1.default,
    userFind: userFind_1.default,
    apiInvitesList: apiInvitesList_1.default,
    apiWorkspaceInviteAdd: apiWorkspaceInviteAdd_1.default,
    apiWorkspaceInviteResend: apiWorkspaceInviteResend_1.default,
    apiWorkspaceInviteUpdate: apiWorkspaceInviteUpdate_1.default,
    apiWorkspaceInviteRemove: apiWorkspaceInviteRemove_1.default,
    apiWorkspaceChange: apiWorkspaceChange_1.default,
    apiMembersList: apiMembersList_1.default,
    apiWorkspaceMemberUpdate: apiWorkspaceMemberUpdate_1.default,
    apiWorkspaceMemberRemove: apiWorkspaceMemberRemove_1.default,
    apiWorkspaceEmail: apiWorkspaceEmail_1.default,
    apiWorkspacesList: apiWorkspacesList_1.default,
    apiWorkspaceUpdate: apiWorkspaceUpdate_1.default,
    apiWorkspaceRemove: apiWorkspaceRemove_1.default,
};
