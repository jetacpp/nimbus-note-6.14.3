"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../config"));
var instance_1 = __importDefault(require("../window/instance"));
var child_1 = __importDefault(require("../window/child"));
var AutoSyncManager_1 = __importDefault(require("../sync/nimbussdk/manager/AutoSyncManager"));
var workspace_1 = __importDefault(require("../db/models/workspace"));
var loader_1 = __importDefault(require("./assets/nimbus-common/loader"));
var loader_2 = __importDefault(require("./assets/webnotes/loader"));
var loader_3 = __importDefault(require("./assets/auth/loader"));
var loader_4 = __importDefault(require("./assets/nimbus-payment/loader"));
var loader_5 = __importDefault(require("./assets/nimbus-update/loader"));
var SelectedWorkspace_1 = __importDefault(require("../workspace/SelectedWorkspace"));
var App_1 = __importDefault(require("../sync/process/application/App"));
var AppRefresh_1 = __importDefault(require("../popup/AppRefresh"));
var currentPageName;
var RequestPage = (function () {
    function RequestPage() {
    }
    RequestPage.getCurrentPageName = function () {
        return currentPageName;
    };
    RequestPage.setCurrentPageName = function (pageName) {
        currentPageName = pageName;
    };
    RequestPage.load = function (pageName, url) {
        if (url === void 0) { url = null; }
        var appWindow = instance_1.default.get();
        if (appWindow) {
            setTimeout(function () {
                switch (pageName) {
                    case config_1.default.PAGE_NOTE_NAME:
                        RequestPage.loadNote(appWindow, url);
                        break;
                    default:
                        RequestPage.loadAuth(appWindow);
                }
            }, 100);
        }
    };
    RequestPage.navToNoteUrl = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, RequestPage.reload(false, url)];
                    case 1:
                        _a.sent();
                        return [2];
                }
            });
        });
    };
    RequestPage.reload = function (checkSync, url) {
        if (checkSync === void 0) { checkSync = false; }
        if (url === void 0) { url = null; }
        return __awaiter(this, void 0, void 0, function () {
            var canRefreshApp;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!RequestPage.isAuth()) return [3, 1];
                        return [2, RequestPage.load(config_1.default.PAGE_AUTH_NAME)];
                    case 1:
                        if (!checkSync) return [3, 3];
                        return [4, RequestPage.canRefreshApp()];
                    case 2:
                        canRefreshApp = _a.sent();
                        if (!canRefreshApp) {
                            return [2];
                        }
                        _a.label = 3;
                    case 3:
                        if (child_1.default.get(child_1.default.TYPE_PURCHASE)) {
                            child_1.default.get(child_1.default.TYPE_PURCHASE).close();
                            child_1.default.set(child_1.default.TYPE_PURCHASE, null);
                        }
                        return [2, RequestPage.load(config_1.default.PAGE_NOTE_NAME, url)];
                }
            });
        });
    };
    RequestPage.canRefreshApp = function () {
        return __awaiter(this, void 0, void 0, function () {
            var selectedWorkspaceId, workspaceId, canStartSync;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, SelectedWorkspace_1.default.getGlobalId()];
                    case 1:
                        selectedWorkspaceId = _a.sent();
                        workspaceId = selectedWorkspaceId ? selectedWorkspaceId : null;
                        return [4, App_1.default.canStartNewSync({ workspaceId: workspaceId })];
                    case 2:
                        canStartSync = _a.sent();
                        if (canStartSync) {
                            return [2, true];
                        }
                        AppRefresh_1.default.show();
                        return [2, false];
                }
            });
        });
    };
    RequestPage.loadNote = function (appWindow, url) {
        if (url === void 0) { url = null; }
        if (appWindow.webContents) {
            appWindow.webContents.removeListener('did-finish-load', RequestPage.didFinishLoadAuthPage);
        }
        appWindow.webContents.removeListener('did-finish-load', RequestPage.didFinishLoadNotePage);
        appWindow.webContents.on('did-finish-load', RequestPage.didFinishLoadNotePage);
        var navUrl = url ? url : "" + config_1.default.LOCAL_SERVER_HTTP_HOST + config_1.default.nimbusAngularIndexFile;
        appWindow.loadURL(navUrl);
    };
    RequestPage.loadAuth = function (appWindow) {
        if (appWindow.webContents) {
            appWindow.webContents.removeListener('did-finish-load', RequestPage.didFinishLoadNotePage);
        }
        appWindow.webContents.removeListener('did-finish-load', RequestPage.didFinishLoadAuthPage);
        appWindow.webContents.on('did-finish-load', RequestPage.didFinishLoadAuthPage);
        appWindow.loadURL(config_1.default.LOCAL_SERVER_HTTP_HOST + config_1.default.nimbusAuthIndexFile);
    };
    RequestPage.didFinishLoadNotePage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var appWindow, currentURL, selectedWorkspace, workspaceIdFromUrl, selectedWorkspaceId, _a, availableWorkspaceList, _i, availableWorkspaceList_1, workspaceId;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        appWindow = instance_1.default.get();
                        if (!appWindow) return [3, 9];
                        return [4, loader_2.default.load(appWindow)];
                    case 1:
                        _b.sent();
                        if (!(RequestPage.getCurrentPageName() !== config_1.default.PAGE_NOTE_NAME)) return [3, 9];
                        currentURL = appWindow.webContents.getURL();
                        selectedWorkspace = undefined;
                        workspaceIdFromUrl = RequestPage.getWorkspaceIdFromUrl(currentURL);
                        if (!workspaceIdFromUrl) return [3, 3];
                        return [4, workspace_1.default.getById(workspaceIdFromUrl)];
                    case 2:
                        selectedWorkspace = _b.sent();
                        return [3, 5];
                    case 3: return [4, SelectedWorkspace_1.default.get()];
                    case 4:
                        selectedWorkspace = (_b.sent());
                        _b.label = 5;
                    case 5:
                        _a = selectedWorkspace;
                        if (!_a) return [3, 7];
                        return [4, workspace_1.default.isMainForUser(selectedWorkspace)];
                    case 6:
                        _a = !(_b.sent());
                        _b.label = 7;
                    case 7:
                        selectedWorkspaceId = _a ? selectedWorkspace.globalId : null;
                        SelectedWorkspace_1.default.setGlobalId(selectedWorkspaceId);
                        return [4, workspace_1.default.getAvailableIdList()];
                    case 8:
                        availableWorkspaceList = _b.sent();
                        for (_i = 0, availableWorkspaceList_1 = availableWorkspaceList; _i < availableWorkspaceList_1.length; _i++) {
                            workspaceId = availableWorkspaceList_1[_i];
                            if (!selectedWorkspaceId && !workspaceId) {
                                AutoSyncManager_1.default.initTimer({ workspaceId: workspaceId, initState: true });
                            }
                            if (workspaceId && (selectedWorkspaceId === workspaceId)) {
                                AutoSyncManager_1.default.initTimer({ workspaceId: workspaceId, initState: true });
                            }
                        }
                        _b.label = 9;
                    case 9:
                        RequestPage.setCurrentPageName(config_1.default.PAGE_NOTE_NAME);
                        return [2];
                }
            });
        });
    };
    RequestPage.getWorkspaceIdFromUrl = function (url) {
        if (!url) {
            return undefined;
        }
        var parts = url.split('/');
        if (typeof parts['3'] === 'undefined') {
            return undefined;
        }
        if (parts['3'] !== 'ws') {
            return undefined;
        }
        if (typeof parts['4'] === 'undefined') {
            return undefined;
        }
        if (!parts['4']) {
            return undefined;
        }
        return parts['4'];
    };
    RequestPage.didFinishLoadAuthPage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var appWindow;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        appWindow = instance_1.default.get();
                        if (!appWindow) return [3, 2];
                        return [4, loader_3.default.load(appWindow)];
                    case 1:
                        _a.sent();
                        AutoSyncManager_1.default.clearTimers();
                        _a.label = 2;
                    case 2:
                        RequestPage.setCurrentPageName(config_1.default.PAGE_AUTH_NAME);
                        return [2];
                }
            });
        });
    };
    RequestPage.isAuth = function () {
        var appWindow = instance_1.default.get();
        if (appWindow) {
            var currentUrl = appWindow.webContents.getURL();
            if (currentUrl.indexOf("/auth/web/auth/") >= 0) {
                return true;
            }
        }
        return false;
    };
    RequestPage.loadNimbusCommonAssets = loader_1.default.load;
    RequestPage.loadNimbusAngularAssets = loader_2.default.load;
    RequestPage.loadNimbusAuthAssets = loader_3.default.load;
    RequestPage.loadNimbusPaymentAssets = loader_4.default.load;
    RequestPage.loadNimbusUpdateAssets = loader_5.default.load;
    return RequestPage;
}());
exports.default = RequestPage;
