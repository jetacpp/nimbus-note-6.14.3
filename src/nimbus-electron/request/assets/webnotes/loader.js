"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../config"));
var loader_1 = __importDefault(require("../nimbus-common/loader"));
var state_1 = __importDefault(require("../../../online/state"));
var NimbusSDK_1 = __importDefault(require("../../../sync/nimbussdk/net/NimbusSDK"));
var user_1 = __importDefault(require("../../../db/models/user"));
var menu_1 = __importDefault(require("../../../menu/menu"));
var electron_1 = require("electron");
var Translations_1 = __importDefault(require("../../../translations/Translations"));
var WebNotesAssetsLoader = (function () {
    function WebNotesAssetsLoader() {
    }
    WebNotesAssetsLoader.load = function (appWindow) {
        return __awaiter(this, void 0, void 0, function () {
            var pageContent, importAssetsData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, loader_1.default.load(appWindow, config_1.default.nimbusAngularAssetsPath)];
                    case 1:
                        pageContent = _a.sent();
                        if (!pageContent) {
                            return [2];
                        }
                        importAssetsData = { appOnlineStatus: state_1.default.get() };
                        NimbusSDK_1.default.getApi().userLogin(function (err, login) {
                            user_1.default.find({ login: login }, {}, function (err, userInfo) {
                                if (userInfo) {
                                    importAssetsData.language = userInfo.language;
                                    importAssetsData.originURL = config_1.default.LOCAL_SERVER_HTTP_HOST;
                                    importAssetsData.contextMenu = menu_1.default.getContextMenu();
                                }
                                pageContent.send('event:initApp:request', importAssetsData);
                                pageContent.send('event:client:login:response', { login: login });
                            });
                        });
                        pageContent.on('context-menu', function (event, params) {
                            var menuItems = [];
                            if (params.dictionarySuggestions && params.dictionarySuggestions.length) {
                                var _loop_1 = function (suggestion) {
                                    menuItems.push({
                                        label: suggestion,
                                        click: function () { return pageContent.replaceMisspelling(suggestion); }
                                    });
                                };
                                for (var _i = 0, _a = params.dictionarySuggestions; _i < _a.length; _i++) {
                                    var suggestion = _a[_i];
                                    _loop_1(suggestion);
                                }
                                menuItems.push({
                                    type: 'separator'
                                });
                            }
                            if (params.misspelledWord) {
                                menuItems.push(new electron_1.MenuItem({
                                    label: "" + Translations_1.default.get('menu_edit__add_to_dictionary'),
                                    click: function () { return pageContent.session.addWordToSpellCheckerDictionary(params.misspelledWord); }
                                }));
                                menuItems.push({
                                    type: 'separator'
                                });
                            }
                            for (var _b = 0, _c = menu_1.default.getContextMenu(); _b < _c.length; _b++) {
                                var menuItem = _c[_b];
                                menuItems.push(menuItem);
                            }
                            var contextMenu = electron_1.Menu.buildFromTemplate(menuItems);
                            contextMenu.popup();
                        });
                        return [2];
                }
            });
        });
    };
    return WebNotesAssetsLoader;
}());
exports.default = WebNotesAssetsLoader;
