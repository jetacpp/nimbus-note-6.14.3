"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../../../config"));
var loader_1 = __importDefault(require("../nimbus-common/loader"));
var NimbusSDK_1 = __importDefault(require("../../../sync/nimbussdk/net/NimbusSDK"));
var user_1 = __importDefault(require("../../../db/models/user"));
var Translations_1 = __importDefault(require("../../../translations/Translations"));
var UpdateAssetsLoader = (function () {
    function UpdateAssetsLoader() {
    }
    UpdateAssetsLoader.load = function (appWindow) {
        loader_1.default.load(appWindow, config_1.default.nimbusUpdateAssetsPath, function () {
            NimbusSDK_1.default.getApi().userLogin(function (err, login) {
                user_1.default.find({ login: login }, {}, function (err, userInfo) {
                    var importAssetsData = {
                        trans: [],
                        language: Translations_1.default.getLang()
                    };
                    if (userInfo) {
                        importAssetsData.language = userInfo.language;
                    }
                    appWindow.webContents.send('event:initApp:request', importAssetsData);
                });
            });
        });
    };
    return UpdateAssetsLoader;
}());
exports.default = UpdateAssetsLoader;
