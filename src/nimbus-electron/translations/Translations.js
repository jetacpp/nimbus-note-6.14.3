"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var auth_1 = __importDefault(require("../auth/auth"));
var translationsData_1 = __importDefault(require("./translationsData"));
var lang = "";
var Translations = (function () {
    function Translations() {
    }
    Translations.initLang = function () {
        return new Promise(function (resolve) {
            auth_1.default.getUser(function (err, authInfo) {
                if (authInfo && Object.keys(authInfo).length && authInfo.language) {
                    Translations.setLang(authInfo.language);
                    return resolve(Translations.getLang());
                }
                Translations.setLang("en");
                return resolve(Translations.getLang());
            });
        });
    };
    Translations.setLang = function (newLang) {
        lang = newLang;
        return lang;
    };
    Translations.getLang = function () {
        if (lang) {
            return lang;
        }
        return "en";
    };
    Translations.get = function (transKey) {
        if (typeof (translationsData_1.default[transKey]) === "undefined") {
            return transKey;
        }
        if (typeof (translationsData_1.default[transKey][Translations.getLang()]) === "undefined") {
            return transKey;
        }
        return translationsData_1.default[transKey][Translations.getLang()];
    };
    return Translations;
}());
exports.default = Translations;
