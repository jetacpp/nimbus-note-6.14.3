"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var trans = {};
trans["popup_crash__title"] = {
    'en': "Oops, we found a problem :(",
    'ru': "\u041A \u0441\u043E\u0436\u0430\u043B\u0435\u043D\u0438\u044E, \u043C\u044B \u043E\u0431\u043D\u0430\u0440\u0443\u0436\u0438\u043B\u0438 \u043F\u0440\u043E\u0431\u043B\u0435\u043C\u0443 :(",
    'de': "Ups, wir haben ein Problem gefunden :("
};
trans["popup_crash__message"] = {
    'en': "We apologize for any inconvenience, please Reload Nimbus Note",
    'ru': "\u041F\u0440\u0438\u043D\u043E\u0441\u0438\u043C \u0438\u0437\u0432\u0438\u043D\u0435\u043D\u0438\u044F \u0437\u0430 \u0432\u043E\u0437\u043C\u043E\u0436\u043D\u044B\u0435 \u043D\u0435\u0443\u0434\u043E\u0431\u0441\u0442\u0432\u0430, \u043F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430, \u041F\u0435\u0440\u0435\u0437\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u0435 Nimbus Note",
    'de': "Wir entschuldigen uns f\u00FCr etwaige Unannehmlichkeiten. Bitte laden Sie Nimbus Note erneut"
};
trans["popup_crash__close_app"] = {
    'en': "Close",
    'ru': "\u0417\u0430\u043A\u0440\u044B\u0442\u044C",
    'de': "Schlie\u00DFen"
};
trans["popup_crash__refresh_app"] = {
    'en': "Reload Nimbus Note",
    'ru': "\u041F\u0435\u0440\u0435\u0437\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u044C Nimbus Note",
    'de': "Nachladen Nimbus Note"
};
trans["cache_clear__confirm_message"] = {
    'en': "Are you sure you wish to perform a clean sync? All Nimbus files stored on your device, including notes and attachments, will be erased. Any changes that have not been synced will be lost.",
    'ru': "\u0412\u044B \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043B\u044C\u043D\u043E \u0445\u043E\u0442\u0438\u0442\u0435 \u0432\u044B\u043F\u043E\u043B\u043D\u0438\u0442\u044C \u043F\u043E\u0432\u0442\u043E\u0440\u043D\u0443\u044E \u0441\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u044E? \u0412\u0441\u0435 \u0444\u0430\u0439\u043B\u044B Nimbus, \u0445\u0440\u0430\u043D\u044F\u0449\u0438\u0435\u0441\u044F \u043D\u0430 \u0432\u0430\u0448\u0435\u043C \u0443\u0441\u0442\u0440\u043E\u0439\u0441\u0442\u0432\u0435, \u0432\u043A\u043B\u044E\u0447\u0430\u044F \u0437\u0430\u043C\u0435\u0442\u043A\u0438 \u0438 \u0432\u043B\u043E\u0436\u0435\u043D\u0438\u044F, \u0431\u0443\u0434\u0443\u0442 \u0443\u0434\u0430\u043B\u0435\u043D\u044B. \u041B\u044E\u0431\u044B\u0435 \u0438\u0437\u043C\u0435\u043D\u0435\u043D\u0438\u044F, \u043A\u043E\u0442\u043E\u0440\u044B\u0435 \u043D\u0435 \u0431\u044B\u043B\u0438 \u0441\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0438\u0440\u043E\u0432\u0430\u043D\u044B, \u0431\u0443\u0434\u0443\u0442 \u043F\u043E\u0442\u0435\u0440\u044F\u043D\u044B.",
    'de': "M\u00F6chten Sie eine saubere Synchronisierung wirklich durchf\u00FChren? Alle auf Ihrem Ger\u00E4t gespeicherten Nimbus-Dateien, einschlie\u00DFlich Notizen und Anh\u00E4nge, werden gel\u00F6scht. Alle \u00C4nderungen, die nicht synchronisiert wurden, gehen verloren."
};
trans["menu_account__start_sync"] = {
    'en': "Start Sync",
    'ru': "\u0417\u0430\u043F\u0443\u0441\u0442\u0438\u0442\u044C \u0441\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u044E",
    'de': "Starten Sie die Synchronisierung"
};
trans["menu_account__traffic_info"] = {
    'en': "Account information",
    'ru': "\u0418\u043D\u0444\u043E\u0440\u043C\u0430\u0446\u0438\u044F \u043E\u0431 \u0430\u043A\u043A\u0430\u0443\u043D\u0442\u0435",
    'de': "Kontoinformationen"
};
trans["menu_account__export"] = {
    'en': "Export all notes",
    'ru': "\u042D\u043A\u0441\u043F\u043E\u0440\u0442 \u0432\u0441\u0435\u0445 \u0437\u0430\u043C\u0435\u0442\u043E\u043A",
    'de': "Export all notes"
};
trans["menu_account"] = {
    'en': "Account",
    'ru': "\u0423\u0447\u0435\u0442\u043D\u0430\u044F \u0437\u0430\u043F\u0438\u0441\u044C",
    'de': "Konto"
};
trans["menu_edit"] = {
    'en': "Edit",
    'ru': "\u0420\u0435\u0434\u0430\u043A\u0442\u0438\u0440\u043E\u0432\u0430\u0442\u044C",
    'de': "Bearbeiten"
};
trans["menu_edit__undo"] = {
    'en': "Undo",
    'ru': "\u0410\u043D\u043D\u0443\u043B\u0438\u0440\u043E\u0432\u0430\u0442\u044C",
    'de': "R\u00FCckg\u00E4ngig machen"
};
trans["menu_edit__redo"] = {
    'en': "Redo",
    'ru': "\u0412\u043E\u0441\u0441\u0442\u0430\u043D\u043E\u0432\u0438\u0442\u044C",
    'de': "Wiederholen"
};
trans["menu_edit__cut"] = {
    'en': "Cut",
    'ru': "\u0412\u044B\u0440\u0435\u0437\u0430\u0442\u044C",
    'de': "Schneiden"
};
trans["menu_edit__copy"] = {
    'en': "Copy",
    'ru': "\u041A\u043E\u043F\u0438\u0440\u043E\u0432\u0430\u0442\u044C",
    'de': "Kopieren"
};
trans["menu_edit__paste"] = {
    'en': "Paste",
    'ru': "\u0412\u0441\u0442\u0430\u0432\u0438\u0442\u044C",
    'de': "Einf\u00FCgen"
};
trans["menu_edit__lookup"] = {
    'en': "Look Up ",
    'ru': "\u041D\u0430\u0439\u0442\u0438 ",
    'de': "Schau hoch "
};
trans["menu_edit__pasteandmatchstyle"] = {
    'en': "Paste without format",
    'ru': "\u0412\u0441\u0442\u0430\u0432\u0438\u0442\u044C \u0431\u0435\u0437 \u0441\u0442\u0438\u043B\u0435\u0439",
    'de': "Einf\u00FCgen ohne Format"
};
trans["menu_edit__delete"] = {
    'en': "Delete",
    'ru': "\u0423\u0434\u0430\u043B\u0438\u0442\u044C",
    'de': "L\u00F6schen"
};
trans["menu_edit__selectall"] = {
    'en': "Select All",
    'ru': "\u0412\u044B\u0431\u0440\u0430\u0442\u044C \u0432\u0441\u0435",
    'de': "W\u00E4hlen Sie Alle"
};
trans["menu_edit__add_to_dictionary"] = {
    'en': "Add to dictionary",
    'ru': "\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u0432 \u0441\u043B\u043E\u0432\u0430\u0440\u044C",
    'de': "Zum W\u00F6rterbuch hinzuf\u00FCgen"
};
trans["menu_edit__find_notes"] = {
    'en': "Find\u2026",
    'ru': "\u041D\u0430\u0439\u0442\u0438\u2026",
    'de': "Finden\u2026"
};
trans["menu_edit__add_note"] = {
    'en': "Add note",
    'ru': "\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u0437\u0430\u043C\u0435\u0442\u043A\u0443",
    'de': "Notiz hinzuf\u00FCgen"
};
trans["menu_view"] = {
    'en': "View",
    'ru': "\u0412\u0438\u0434",
    'de': "Ansicht"
};
trans["menu_view__reload"] = {
    'en': "Refresh",
    'ru': "\u041E\u0431\u043D\u043E\u0432\u0438\u0442\u044C",
    'de': "Aktualisierung"
};
trans["menu_view__resetzoom"] = {
    'en': "Actual Size",
    'ru': "\u0424\u0430\u043A\u0442\u0438\u0447\u0435\u0441\u043A\u0438\u0439 \u0420\u0430\u0437\u043C\u0435\u0440",
    'de': "Tats\u00E4chliche Gr\u00F6\u00DFe"
};
trans["menu_view__zoomin"] = {
    'en': "Zoom In",
    'ru': "\u0423\u0432\u0435\u043B\u0438\u0447\u0438\u0442\u044C",
    'de': "Zoom zur\u00FCcksetzen"
};
trans["menu_view__zoomout"] = {
    'en': "Zoom Out",
    'ru': "\u0423\u043C\u0435\u043D\u044C\u0448\u0438\u0442\u044C",
    'de': "Rauszoomen"
};
trans["menu_view__togglefullscreen"] = {
    'en': "Toggle Full Screen",
    'ru': "\u0412\u043A\u043B\u044E\u0447\u0438\u0442\u044C \u043F\u043E\u043B\u043D\u043E\u044D\u043A\u0440\u0430\u043D\u043D\u044B\u0439 \u0440\u0435\u0436\u0438\u043C",
    'de': "Vollbild umschalten"
};
trans["menu_window"] = {
    'en': "Window",
    'ru': "\u041E\u043A\u043D\u043E",
    'de': "Fenster"
};
trans["menu_window__minimize"] = {
    'en': "Minimize",
    'ru': "\u041C\u0438\u043D\u0438\u043C\u0438\u0437\u0438\u0440\u043E\u0432\u0430\u0442\u044C",
    'de': "Minimieren"
};
trans["menu_window__maximize"] = {
    'en': "Maximize",
    'ru': "\u041C\u0430\u043A\u0441\u0438\u043C\u0438\u0437\u0438\u0440\u043E\u0432\u0430\u0442\u044C",
    'de': "Maximieren"
};
trans["menu_window__front"] = {
    'en': "Bring All to Front",
    'ru': "\u0412\u0441\u0435 \u043D\u0430 \u043F\u0435\u0440\u0435\u0434\u043D\u0438\u0439 \u043F\u043B\u0430\u043D",
    'de': "Bring All nach vorne"
};
trans["menu_window__show"] = {
    'en': "Show",
    'ru': "\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C",
    'de': "Zeigen"
};
trans["menu_help"] = {
    'en': "Help",
    'ru': "\u041F\u043E\u043C\u043E\u0449\u044C",
    'de': "Hilfe"
};
trans["menu_help__submit_bug"] = {
    'en': "Submit Bug",
    'ru': "\u041E\u0442\u043F\u0440\u0430\u0432\u0438\u0442\u044C \u043E\u0448\u0438\u0431\u043A\u0443",
    'de': "Fehler melden"
};
trans["menu_help__help"] = {
    'en': "Open Help Center",
    'ru': "\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0438\u043D\u0441\u0442\u0440\u0443\u043A\u0446\u0438\u044E",
    'de': "Anleitung anzeigen"
};
trans["menu_help__contact_us"] = {
    'en': "Contact Us",
    'ru': "\u0421\u0432\u044F\u0436\u0438\u0442\u0435\u0441\u044C \u0441 \u043D\u0430\u043C\u0438",
    'de': "Kontaktiere uns"
};
trans["menu_help__about"] = {
    'en': "About Nimbus Web",
    'ru': "\u041E Nimbus Web",
    'de': "\u00DCber Nimbus Web"
};
trans["menu_help__updates"] = {
    'en': "Check for updates...",
    'ru': "\u041F\u0440\u043E\u0432\u0435\u0440\u0438\u0442\u044C \u043E\u0431\u043D\u043E\u0432\u043B\u0435\u043D\u0438\u044F...",
    'de': "Auf Updates pr\u00FCfen..."
};
trans["menu_file"] = {
    'en': "File",
    'ru': "\u0424\u0430\u0439\u043B",
    'de': "Datei"
};
trans["menu_file__close"] = {
    'en': "Close Window",
    'ru': "\u0417\u0430\u0440\u043A\u044B\u0442\u044C \u041E\u043A\u043D\u043E",
    'de': "Fenster schlie\u00DFen"
};
trans["menu_file__logout"] = {
    'en': "Logout",
    'ru': "\u0412\u044B\u0439\u0442\u0438",
    'de': "Abmelden"
};
trans["menu_app__about"] = {
    'en': "About Nimbus Note",
    'ru': "\u041F\u0440\u043E Nimbus Note",
    'de': "\u00DCber Nimbus Note"
};
trans["menu_app__about_short"] = {
    'en': "About",
    'ru': "\u041E \u043F\u0440\u043E\u0433\u0440\u0430\u043C\u043C\u0435",
    'de': "\u00DCber"
};
trans["menu_app__preferences"] = {
    'en': "Preferences\u2026",
    'ru': "\u041D\u0430\u0441\u0442\u0440\u043E\u0439\u043A\u0438\u2026",
    'de': "Einstellungen\u2026"
};
trans["menu_app__hide"] = {
    'en': "Hide Nimbus Note",
    'ru': "\u0421\u043F\u0440\u044F\u0442\u0430\u0442\u044C Nimbus Note",
    'de': "Verbergen Nimbus Note"
};
trans["menu_app__hideothers"] = {
    'en': "Hide Others",
    'ru': "\u0421\u043A\u0440\u044B\u0442\u044C \u0434\u0440\u0443\u0433\u0438\u0435",
    'de': "Andere verstecken"
};
trans["menu_app__unhide"] = {
    'en': "Show All",
    'ru': "\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0432\u0441\u0435",
    'de': "Zeige alles"
};
trans["menu_app__quit"] = {
    'en': "Quit Nimbus Note",
    'ru': "\u0417\u0430\u043A\u0440\u044B\u0442\u044C Nimbus Note",
    'de': "Schlie\u00DFen Nimbus Note"
};
trans["menu_speech"] = {
    'en': "Speech",
    'ru': "\u0413\u043E\u043B\u043E\u0441\u043E\u0432\u043E\u0435 \u0443\u043F\u0440\u0430\u0432\u043B\u0435\u043D\u0438\u0435",
    'de': "Rede"
};
trans["menu_speech__startspeaking"] = {
    'en': "Start Speaking",
    'ru': "\u041D\u0430\u0447\u0430\u0442\u044C \u0440\u0430\u0437\u0433\u043E\u0432\u043E\u0440",
    'de': "Beginnen Sie zu sprechen"
};
trans["menu_speech__stopspeaking"] = {
    'en': "Stop Speaking",
    'ru': "\u041E\u0441\u0442\u0430\u043D\u043E\u0432\u0438\u0442\u044C \u0440\u0430\u0437\u0433\u043E\u0432\u043E\u0440",
    'de': "H\u00F6r auf zu reden"
};
trans["send_logs__confirm_details"] = {
    'en': "Send logs to Nimbus",
    'ru': "\u041E\u0442\u043F\u0440\u0430\u0432\u0438\u0442\u044C \u043B\u043E\u0433\u0438 \u0432 Nimbus",
    'de': "Protokolle an Nimbus senden"
};
trans["send_logs__confirm_message"] = {
    'en': "Send account errors logs to Nimbus team",
    'ru': "\u041E\u0442\u043F\u0440\u0430\u0432\u0438\u0442\u044C \u043B\u043E\u0433\u0438 \u043E\u0448\u0438\u0431\u043E\u043A \u0430\u043A\u043A\u0430\u0443\u043D\u0442\u0430 \u043A\u043E\u043C\u0430\u043D\u0434\u0435 Nimbus",
    'de': "Sende Konto Fehler-Logs zu Nimbus Team"
};
trans["send_logs__confirm_details"] = {
    'en': "Send logs to Nimbus",
    'ru': "\u041E\u0442\u043F\u0440\u0430\u0432\u0438\u0442\u044C \u043B\u043E\u0433\u0438 \u0432 Nimbus",
    'de': "Protokolle an Nimbus senden"
};
trans["show_sync_logs__details"] = {
    'en': "Send sync logs in console",
    'ru': "\u041E\u0442\u043F\u0440\u0430\u0432\u0438\u0442\u044C \u043B\u043E\u0433\u0438 \u0441\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u0438 \u0432 \u043A\u043E\u043D\u0441\u043E\u043B\u044C",
    'de': "Senden sync logs in der Konsole"
};
trans["send_logs__confirm_btn_submit"] = {
    'en': "Send logs",
    'ru': "\u041E\u0442\u043F\u0440\u0430\u0432\u0438\u0442\u044C \u043B\u043E\u0433\u0438",
    'de': "Protokolle senden"
};
trans["send_logs__confirm_btn_cancel"] = {
    'en': "Cancel",
    'ru': "\u041E\u0442\u043C\u0435\u043D\u0438\u0442\u044C",
    'de': "Stornieren"
};
trans["cache_clear__confirm_details"] = {
    'en': "Clean sync data",
    'ru': "\u041E\u0447\u0438\u0441\u0442\u0438\u0442\u044C \u0434\u0430\u043D\u043D\u044B\u0435 \u0441\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u0438",
    'de': "S\u00E4ubern Sie die Synchronisierungsdaten"
};
trans["cache_clear__confirm_btn_cancel"] = {
    'en': "Cancel",
    'ru': "\u041E\u0442\u043C\u0435\u043D\u0438\u0442\u044C",
    'de': "Stornieren"
};
trans["cache_clear__confirm_btn_submit"] = {
    'en': "Delete local database",
    'ru': "\u0423\u0434\u0430\u043B\u0438\u0442\u044C \u043B\u043E\u043A\u0430\u043B\u044C\u043D\u0443\u044E \u0431\u0430\u0437\u0443",
    'de': "Lokale Datenbank l\u00F6schen"
};
trans["app_refresh__warning_message"] = {
    'en': "Sorry, you can not refresh application while sync is in-progress.",
    'ru': "\u041A \u0441\u043E\u0436\u0430\u043B\u0435\u043D\u0438\u044E, \u0432\u044B \u043D\u0435 \u043C\u043E\u0436\u0435\u0442\u0435 \u043E\u0431\u043D\u043E\u0432\u0438\u0442\u044C \u043F\u0440\u0438\u043B\u043E\u0436\u0435\u043D\u0438\u0435 \u0432\u043E \u0432\u0440\u0435\u043C\u044F \u0441\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u0438.",
    'de': "Sie k\u00F6nnen die Anwendung leider nicht aktualisieren, w\u00E4hrend die Synchronisierung ausgef\u00FChrt wird."
};
trans["app_refresh__warning_details"] = {
    'en': "Please, wait until sync process complete",
    'ru': "\u041F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430, \u0434\u043E\u0436\u0434\u0438\u0442\u0435\u0441\u044C \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043D\u0438\u044F \u043F\u0440\u043E\u0446\u0435\u0441\u0441\u0430 \u0441\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u0438",
    'de': "Bitte warten Sie, bis der Synchronisierungsvorgang abgeschlossen ist"
};
trans["app_refresh__confirm_btn"] = {
    'en': "Close",
    'ru': "\u0417\u0430\u043A\u0440\u044B\u0442\u044C",
    'de': "Schlie\u00DFen"
};
trans["notice_sync_is_not_finished"] = {
    'en': "Attachment has not finished downloading. Please wait until sync is complete.",
    'ru': "\u041F\u0440\u0438\u043B\u043E\u0436\u0435\u043D\u0438\u0435 \u043D\u0435 \u0437\u0430\u043A\u043E\u043D\u0447\u0438\u043B\u043E \u0441\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u044E. \u041F\u043E\u0434\u043E\u0436\u0434\u0438\u0442\u0435, \u043F\u043E\u043A\u0430 \u0441\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u044F \u0437\u0430\u0432\u0435\u0440\u0448\u0438\u0442\u0441\u044F.",
    'de': "Der Anhang wurde nicht heruntergeladen. Bitte warten Sie, bis die Synchronisierung abgeschlossen ist."
};
trans["dock_menu__window_show"] = {
    'en': "Show Nimbus Note",
    'ru': "\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C Nimbus Note",
    'de': "Zeigen Nimbus Note"
};
trans["toast__workspace__remove__offline"] = {
    'en': "The workspace can not be removed in offline mode. Please, connect to the Internet",
    'ru': "\u041D\u0435\u0442 \u0432\u043E\u0437\u043C\u043E\u0436\u043D\u043E\u0441\u0442\u0438 \u0443\u0434\u0430\u043B\u0438\u0442\u044C \u0432\u043E\u0440\u043A\u0441\u043F\u0435\u0439\u0441 \u0432 \u043E\u0444\u0444\u043B\u0430\u0439\u043D \u0440\u0435\u0436\u0438\u043C\u0435, \u043F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430 \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0438\u0442\u0435\u0441\u044C \u043A \u0438\u043D\u0442\u0435\u0440\u043D\u0435\u0442\u0443",
    'de': "Der Arbeitsbereich kann im Offline-Modus nicht entfernt werden. Bitte verbinden Sie sich mit dem Internet"
};
trans["toast__workspace__create__offline"] = {
    'en': "The workspace can not be created in offline mode. Please, connect to the Internet",
    'ru': "\u041D\u0435\u0442 \u0432\u043E\u0437\u043C\u043E\u0436\u043D\u043E\u0441\u0442\u0438 \u0441\u043E\u0437\u0434\u0430\u0442\u044C \u0432\u043E\u0440\u043A\u0441\u043F\u0435\u0439\u0441 \u0432 \u043E\u0444\u0444\u043B\u0430\u0439\u043D \u0440\u0435\u0436\u0438\u043C\u0435, \u043F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430 \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0438\u0442\u0435\u0441\u044C \u043A \u0438\u043D\u0442\u0435\u0440\u043D\u0435\u0442\u0443",
    'de': "Der Arbeitsbereich kann nicht im Offline-Modus erstellt werden. Bitte verbinden Sie sich mit dem Internet"
};
trans["toast__workspace__update__offline"] = {
    'en': "The workspace can not be updated in offline mode. Please, connect to the Internet",
    'ru': "\u041D\u0435\u0442 \u0432\u043E\u0437\u043C\u043E\u0436\u043D\u043E\u0441\u0442\u0438 \u043E\u0431\u043D\u043E\u0432\u0438\u0442\u044C \u0432\u043E\u0440\u043A\u0441\u043F\u0435\u0439\u0441 \u0432 \u043E\u0444\u0444\u043B\u0430\u0439\u043D \u0440\u0435\u0436\u0438\u043C\u0435, \u043F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430 \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0438\u0442\u0435\u0441\u044C \u043A \u0438\u043D\u0442\u0435\u0440\u043D\u0435\u0442\u0443",
    'de': "Der Arbeitsbereich kann im Offline-Modus nicht aktualisiert werden. Bitte verbinden Sie sich mit dem Internet"
};
trans["toast__workspace__member__update__offline"] = {
    'en': "The workspace members can not be updated in offline mode. Please, connect to the Internet",
    'ru': "\u041D\u0435\u0442 \u0432\u043E\u0437\u043C\u043E\u0436\u043D\u043E\u0441\u0442\u0438 \u043E\u0431\u043D\u043E\u0432\u0438\u0442\u044C \u043F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u0435\u0439 \u0432\u043E\u0440\u043A\u0441\u043F\u0435\u0439\u0441\u0430 \u0432 \u043E\u0444\u0444\u043B\u0430\u0439\u043D \u0440\u0435\u0436\u0438\u043C\u0435, \u043F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430 \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0438\u0442\u0435\u0441\u044C \u043A \u0438\u043D\u0442\u0435\u0440\u043D\u0435\u0442\u0443",
    'de': "Die Mitglieder des Arbeitsbereichs k\u00F6nnen im Offline-Modus nicht aktualisiert werden. Bitte verbinden Sie sich mit dem Internet"
};
trans["toast__need_sync_before__share"] = {
    'en': "Please, make Sync before Share",
    'ru': "\u0421\u0434\u0435\u043B\u0430\u0439\u0442\u0435 \u0421\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u044E \u043F\u0435\u0440\u0435\u0434 \u0428\u0430\u0440\u0438\u043D\u0433\u043E\u043C",
    'de': "Bitte machen Sie Synch vor Freigabe"
};
trans["toast__can_not_share__offline"] = {
    'en': "You can not share/unshare when you are offline",
    'ru': "\u0412\u044B \u043D\u0435 \u043C\u043E\u0436\u0435\u0442\u0435 \u0441\u0434\u0435\u043B\u0430\u0442\u044C/\u043E\u0442\u043C\u0435\u043D\u0438\u0442\u044C \u0428\u0430\u0440\u0438\u043D\u0433, \u043A\u043E\u0433\u0434\u0430 \u0432\u044B \u043D\u0435 \u0432 \u0441\u0435\u0442\u0438",
    'de': "Sie k\u00F6nnen nicht freigeben / teilen, wenn Sie offline sind"
};
trans["toast__can_not_share__offline_item"] = {
    'en': "You can not share/unshare offline note or folder",
    'ru': "\u0412\u044B \u043D\u0435 \u043C\u043E\u0436\u0435\u0442\u0435 \u0441\u0434\u0435\u043B\u0430\u0442\u044C \u0428\u0430\u0440\u0438\u043D\u0433 \u0434\u043B\u044F \u043E\u0444\u0444\u043B\u0430\u0439\u043D \u0434\u0438\u0440\u0435\u043A\u0442\u043E\u0440\u0438\u0438 \u0438\u043B\u0438 \u0437\u0430\u043C\u0435\u0442\u043A\u0438",
    'de': "Sie k\u00F6nnen Offline-Notizen oder Ordner nicht freigeben / deaktivieren"
};
trans["toast__can_not_update_application__offline"] = {
    'en': "Nimbus Note cannot update while offline. Make sure you are connected to the Internet.",
    'ru': "Nimbus Note \u043D\u0435 \u043C\u043E\u0436\u0435\u0442 \u043E\u0431\u043D\u043E\u0432\u043B\u044F\u0442\u044C\u0441\u044F \u0432 \u043E\u0444\u0444\u043B\u0430\u0439\u043D \u0440\u0435\u0436\u0438\u043C\u0435. \u0423\u0431\u0435\u0434\u0438\u0442\u0435\u0441\u044C, \u0447\u0442\u043E \u0432\u044B \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0435\u043D\u044B \u043A \u0418\u043D\u0442\u0435\u0440\u043D\u0435\u0442\u0443.",
    'de': "Nimbus Note kann offline nicht aktualisiert werden. Stellen Sie sicher, dass Sie mit dem Internet verbunden sind."
};
trans["toast__can_not_sync_access_denied"] = {
    'en': "You can not make sync, you don't have permissions to sync changed data",
    'ru': "\u0412\u044B \u043D\u0435 \u043C\u043E\u0436\u0435\u0442\u0435 \u0432\u044B\u043F\u043E\u043B\u043D\u0438\u0442\u044C \u0441\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u044E, \u0443 \u0432\u0430\u0441 \u043D\u0435\u0442 \u043F\u0440\u0430\u0432 \u043D\u0430 \u0441\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u044E \u0438\u0437\u043C\u0435\u043D\u0435\u043D\u043D\u044B\u0445 \u0434\u0430\u043D\u043D\u044B\u0445",
    'de': "Sie k\u00F6nnen nicht synchronisiert machen, Sie haben keine Berechtigungen ge\u00E4nderten Daten synchronisieren"
};
trans["reminder_bottom_text"] = {
    'en': "Reminder",
    'ru': "\u041D\u0430\u043F\u043E\u043C\u0438\u043D\u0430\u043D\u0438\u0435",
    'de': "Erinnerung"
};
trans["toast__note_folder_source_workspace_not_exist"] = {
    'en': "Note or folder does not found",
    'ru': "\u0417\u0430\u043C\u0435\u0442\u043A\u0430 \u0438\u043B\u0438 \u043F\u0430\u043F\u043A\u0430 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u0430",
    'de': "Hinweis oder Ordner nicht gefunden"
};
trans["toast__can_not_move_to_workspace__item_offline"] = {
    'en': "You can not move offline note or folder",
    'ru': "\u0412\u044B \u043D\u0435 \u043C\u043E\u0436\u0435\u0442\u0435 \u043F\u0435\u0440\u0435\u043D\u0435\u0441\u0442\u0438 \u043E\u0444\u0444\u043B\u0430\u0439\u043D \u0434\u0438\u0440\u0435\u043A\u0442\u043E\u0440\u0438\u044E \u0438\u043B\u0438 \u0437\u0430\u043C\u0435\u0442\u043A\u0443",
    'de': "Sie k\u00F6nnen Offline-Notizen oder Ordner nicht verschieben"
};
trans["toast__can_not_move_to_workspace__note_encrypted"] = {
    'en': "You can not move encrypted note",
    'ru': "\u0412\u044B \u043D\u0435 \u043C\u043E\u0436\u0435\u0442\u0435 \u043F\u0435\u0440\u0435\u043D\u0435\u0441\u0442\u0438 \u0437\u0430\u0448\u0438\u0444\u0440\u043E\u0432\u0430\u043D\u043D\u0443\u044E \u0437\u0430\u043C\u0435\u0442\u043A\u0443",
    'de': "Sie k\u00F6nnen keine verschl\u00FCsselte Notiz verschieben"
};
trans["toast__can_not_move_to_workspace__item_not_sync"] = {
    'en': "Please, make Sync before move note or folder to workspace",
    'ru': "\u0421\u0434\u0435\u043B\u0430\u0439\u0442\u0435 \u0421\u0438\u043D\u0445\u0440\u043E\u043D\u0438\u0437\u0430\u0446\u0438\u044E \u043F\u0435\u0440\u0435\u0434 \u043F\u0435\u0440\u0435\u043C\u0435\u0449\u0435\u043D\u0438\u0435\u043C \u0437\u0430\u043C\u0435\u0442\u043A\u0438 \u0438\u043B\u0438 \u0434\u0438\u0440\u0435\u0442\u043A\u043E\u0440\u0438\u0438 \u0432 \u0434\u0440\u0443\u0433\u043E\u0439 \u0432\u043E\u0440\u043A\u0441\u043F\u0435\u0439\u0441",
    'de': "Bitte machen Sie die Synchronisierung, bevor Sie die Notiz oder den Ordner in den Arbeitsbereich verschieben"
};
trans["toast__can_not_move_to_workspace__quota_exceed_note_attachment"] = {
    'en': "Note contain files larger than 10mb.",
    'ru': "\u0417\u0430\u043C\u0435\u0442\u043A\u0430 \u0441\u043E\u0434\u0435\u0440\u0436\u0438\u0442 \u0444\u0430\u0439\u043B(\u044B) \u0440\u0430\u0437\u043C\u0435\u0440\u043E\u043C \u0431\u043E\u043B\u044C\u0448\u0435 10\u043C\u0431.",
    'de': "Hinweis enth\u00E4lt Dateien, die gr\u00F6\u00DFer als 10 MB sind."
};
trans["toast__can_not_move_to_workspace__quota_exceed_folder_attachments"] = {
    'en': "Folder has notes with files larger than 10mb.",
    'ru': "\u0412 \u043F\u0430\u043F\u043A\u0435 \u0435\u0441\u0442\u044C \u0437\u0430\u043C\u0435\u0442\u043A\u0438, \u0432 \u043A\u043E\u0442\u043E\u0440\u044B\u0445 \u0435\u0441\u0442\u044C \u0444\u0430\u0439\u043B\u044B \u0440\u0430\u0437\u043C\u0435\u0440\u043E\u043C \u0431\u043E\u043B\u044C\u0448\u0435 10\u043C\u0431.",
    'de': "Der Ordner enth\u00E4lt Notizen mit Dateien, die gr\u00F6\u00DFer als 10 MB sind."
};
trans["toast__can_not_move_to_workspace__quota_exceed_attachments_user"] = {
    'en': "Switch to the Nimbus Pro plan to raise the limit to 1GB.",
    'ru': "\u041F\u0435\u0440\u0435\u0439\u0434\u0438\u0442\u0435 \u043D\u0430 \u043F\u043B\u0430\u043D Nimbus Pro, \u0447\u0442\u043E\u0431\u044B \u043F\u043E\u0434\u043D\u044F\u0442\u044C \u043B\u0438\u043C\u0438\u0442 \u0434\u043E 1GB.",
    'de': "Wechseln Sie zum Nimbus Pro-Plan, um den Grenzwert auf 1 GB zu erh\u00F6hen."
};
trans["toast__can_not_move_to_workspace__quota_exceed_attachments_owner"] = {
    'en': "The project owner must switch to the Nimbus Pro plan to raise the limit on file size.",
    'ru': "\u0412\u043B\u0430\u0434\u0435\u043B\u0435\u0446 \u043F\u0440\u043E\u0435\u043A\u0442\u0430 \u0434\u043E\u043B\u0436\u0435\u043D \u043F\u0435\u0440\u0435\u0439\u0442\u0438 \u043D\u0430 \u043F\u043B\u0430\u043D Nimbus Pro, \u0447\u0442\u043E\u0431\u044B \u043F\u043E\u0434\u043D\u044F\u0442\u044C \u043B\u0438\u043C\u0438\u0442 \u043D\u0430 \u0440\u0430\u0437\u043C\u0435\u0440 \u0444\u0430\u0439\u043B\u043E\u0432.",
    'de': "Der Projektbesitzer muss zum Nimbus Pro-Plan wechseln, um die Dateigr\u00F6\u00DFe zu erh\u00F6hen."
};
trans["update_error__title"] = {
    'en': "Problem to check for updates",
    'ru': "\u041D\u0435 \u0443\u0434\u0430\u043B\u043E\u0441\u044C \u043F\u0440\u043E\u0432\u0435\u0440\u0438\u0442\u044C \u043E\u0431\u043D\u043E\u0432\u043B\u0435\u043D\u0438\u044F",
    'de': "Probleem om te controleren op updates"
};
trans["update_error__message"] = {
    'en': "Please check your Internet connection, or try again later.",
    'ru': "\u041F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430, \u043F\u0440\u043E\u0432\u0435\u0440\u044C\u0442\u0435 \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0435\u043D\u0438\u0435 \u043A \u0418\u043D\u0442\u0435\u0440\u043D\u0435\u0442\u0443 \u0438\u043B\u0438 \u043F\u043E\u0432\u0442\u043E\u0440\u0438\u0442\u0435 \u043F\u043E\u043F\u044B\u0442\u043A\u0443 \u043F\u043E\u0437\u0436\u0435.",
    'de': "Bitte \u00FCberpr\u00FCfen Sie Ihre Internetverbindung oder versuchen Sie es sp\u00E4ter erneut."
};
trans["update_not_available__title"] = {
    'en': "No Updates",
    'ru': "\u041E\u0431\u043D\u043E\u0432\u043B\u0435\u043D\u0438\u0439 \u043D\u0435\u0442",
    'de': "Keine Aktualisierungen"
};
trans["update_not_available__message"] = {
    'en': "Your current version is up-to-date.",
    'ru': "\u0412\u0430\u0448\u0430 \u0442\u0435\u043A\u0443\u0449\u0430\u044F \u0432\u0435\u0440\u0441\u0438\u044F \u0430\u043A\u0442\u0443\u0430\u043B\u044C\u043D\u0430.",
    'de': "Ihre aktuelle Version ist aktuell."
};
trans["update_downloaded__title"] = {
    'en': "Install Updates",
    'ru': "\u0423\u0441\u0442\u0430\u043D\u043E\u0432\u0438\u0442\u044C \u043E\u0431\u043D\u043E\u0432\u043B\u0435\u043D\u0438\u044F",
    'de': "Installiere Updates"
};
trans["update_downloaded__message"] = {
    'en': "Your updates have downloaded. The application will quit to install updates.",
    'ru': "\u0412\u0430\u0448\u0438 \u043E\u0431\u043D\u043E\u0432\u043B\u0435\u043D\u0438\u044F \u0437\u0430\u0433\u0440\u0443\u0436\u0435\u043D\u044B. \u041F\u0440\u0438\u043B\u043E\u0436\u0435\u043D\u0438\u0435 \u0432\u044B\u0439\u0434\u0435\u0442 \u0434\u043B\u044F \u0443\u0441\u0442\u0430\u043D\u043E\u0432\u043A\u0438 \u043E\u0431\u043D\u043E\u0432\u043B\u0435\u043D\u0438\u0439.",
    'de': "Ihre Updates wurden heruntergeladen. Die Anwendung wird beendet, um Updates zu installieren."
};
trans["toast__user_name__update__offline"] = {
    'en': "The user First Name and Last Name can not be updated in offline mode. Please, connect to the Internet",
    'ru': "\u041D\u0435\u0442 \u0432\u043E\u0437\u043C\u043E\u0436\u043D\u043E\u0441\u0442\u0438 \u043E\u0431\u043D\u043E\u0432\u0438\u0442\u044C \u0418\u043C\u044F \u0438 \u0424\u0430\u043C\u0438\u043B\u0438\u044E \u043F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u044F \u0432 \u043E\u0444\u0444\u043B\u0430\u0439\u043D \u0440\u0435\u0436\u0438\u043C\u0435, \u043F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430 \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0438\u0442\u0435\u0441\u044C \u043A \u0438\u043D\u0442\u0435\u0440\u043D\u0435\u0442\u0443",
    'de': "Der Vor- und Nachname des Benutzers kann im Offline-Modus nicht aktualisiert werden. Bitte verbinden Sie sich mit dem Internet"
};
trans["toast__user_avatar__update__offline"] = {
    'en': "The user Avatar can not be updated in offline mode. Please, connect to the Internet",
    'ru': "\u041D\u0435\u0442 \u0432\u043E\u0437\u043C\u043E\u0436\u043D\u043E\u0441\u0442\u0438 \u043E\u0431\u043D\u043E\u0432\u0438\u0442\u044C \u0410\u0432\u0430\u0442\u0430\u0440 \u043F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u044F \u0432 \u043E\u0444\u0444\u043B\u0430\u0439\u043D \u0440\u0435\u0436\u0438\u043C\u0435, \u043F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430 \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0438\u0442\u0435\u0441\u044C \u043A \u0438\u043D\u0442\u0435\u0440\u043D\u0435\u0442\u0443",
    'de': "Der Benutzer Avatar kann im Offline-Modus nicht aktualisiert werden. Bitte verbinden Sie sich mit dem Internet"
};
trans["export_notes_dialog"] = {
    'en': "Export note",
    'ru': "\u042D\u043A\u0441\u043F\u043E\u0440\u0442 \u0437\u0430\u043C\u0435\u0442\u043A\u0438",
    'de': "Export note"
};
trans["export_folder_dialog"] = {
    'en': "Export notes",
    'ru': "\u042D\u043A\u0441\u043F\u043E\u0440\u0442 \u0437\u0430\u043C\u0435\u0442\u043E\u043A",
    'de': "Export notes"
};
exports.default = trans;
