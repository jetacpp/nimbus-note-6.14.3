"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var js_md5_1 = __importDefault(require("js-md5"));
var config_1 = __importDefault(require("../../config"));
var user_1 = __importDefault(require("../db/models/user"));
var settings_1 = __importDefault(require("../db/models/settings"));
var dateHandler_1 = __importDefault(require("../utilities/dateHandler"));
var customError_1 = __importDefault(require("../utilities/customError"));
var loggedUserData;
var loggedUserInfo;
var Auth = (function () {
    function Auth() {
    }
    Auth.login = function (data, options, callback) {
        if (options === void 0) { options = { oauth: false }; }
        if (callback === void 0) { callback = function (err, res) {
        }; }
        if (!data) {
            return callback(customError_1.default.wrongInput(), { errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT });
        }
        if (!data.login) {
            return callback(customError_1.default.wrongInput(), { errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT });
        }
        if (!options.oauth && !data.password) {
            return callback(customError_1.default.wrongInput(), { errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT });
        }
        var findQuery = {
            login: data.login
        };
        user_1.default.find(findQuery, { secure: true }, function (err, doc) {
            if (!doc) {
                return callback(customError_1.default.itemNotFound(), { errorCode: Auth.errorCode.ERROR_USER_NOT_FOUND });
            }
            if (!options.oauth) {
                if (!Auth.confirmPassword(data, doc)) {
                    return callback(customError_1.default.noAuth(), { errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT });
                }
            }
            var updateFields = {
                'localSession': Auth.cryptSessionToken(doc.email, doc._id)
            };
            updateFields = Auth.prepareSyncData(updateFields, data);
            findQuery = {
                userId: doc.userId
            };
            user_1.default.update(findQuery, updateFields, {}, function (err, count) {
                if (!count) {
                    return callback(customError_1.default.itemSaveError(), { errorCode: Auth.errorCode.ERROR_USER_NOT_FOUND });
                }
                settings_1.default.set(config_1.default.SETTINGS_APP_AUTH_NAME, updateFields, function (err, list) {
                    if (!list) {
                        return callback(customError_1.default.itemSaveError(), { errorCode: Auth.errorCode.ERROR_USER_LOGIN });
                    }
                    loggedUserData = user_1.default.getPublicData(doc);
                    loggedUserInfo = user_1.default.getPublicInfo(doc);
                    return callback(null, { errorCode: Auth.errorCode.ERROR_NONE, data: loggedUserData });
                });
            });
        });
    };
    Auth.register = function (data, callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        if (!data) {
            return callback(customError_1.default.wrongInput(), { errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT });
        }
        if (!data.login || !data.password) {
            return callback(customError_1.default.wrongInput(), { errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT });
        }
        var findQuery = {
            login: data.login
        };
        user_1.default.find(findQuery, {}, function (err, doc) {
            if (doc && Object.keys(doc).length) {
                return callback(customError_1.default.wrongInput(), { errorCode: Auth.errorCode.ERROR_USER_ALREADY_EXIST });
            }
            user_1.default.create(data, { secure: true }, function (err, doc) {
                if (!doc) {
                    return callback(customError_1.default.itemSaveError(), { errorCode: Auth.errorCode.ERROR_USER_REGISTER });
                }
                var updateFields = {
                    'id': doc._id,
                    'password': Auth.cryptPassword(doc.password, doc._id),
                    'localSession': Auth.cryptSessionToken(doc.email, doc._id),
                    'username': user_1.default.prepareName(doc.email, doc._id)
                };
                updateFields = Auth.prepareSyncData(updateFields, data);
                findQuery = {
                    userId: doc.userId
                };
                user_1.default.update(findQuery, updateFields, {}, function (err, count) {
                    if (!count) {
                        return callback(customError_1.default.itemSaveError(), { errorCode: Auth.errorCode.ERROR_USER_NOT_FOUND });
                    }
                    loggedUserData = user_1.default.getPublicData(doc);
                    loggedUserData.username = updateFields.username;
                    var saveFields = { 'localSession': updateFields.localSession };
                    settings_1.default.set(config_1.default.SETTINGS_APP_AUTH_NAME, saveFields, function (err, list) {
                        if (!list) {
                            return callback(customError_1.default.itemSaveError(), { errorCode: Auth.errorCode.ERROR_USER_REGISTER });
                        }
                        return callback(null, { errorCode: Auth.errorCode.ERROR_NONE, data: loggedUserData });
                    });
                });
            });
        });
    };
    Auth.updateUserPassword = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) {
                        if (!(data.login && data.password)) {
                            return resolve(0);
                        }
                        var findQuery = {
                            login: data.login
                        };
                        user_1.default.find(findQuery, {}, function (err, userItem) {
                            if (err || !userItem) {
                                return resolve(0);
                            }
                            if (!Object.keys(userItem).length) {
                                return resolve(0);
                            }
                            var passwordHash = Auth.cryptPassword(data.password, userItem._id);
                            user_1.default.update({ 'userId': userItem.userId }, { 'password': passwordHash }, {}, function (err, count) {
                                if (!count) {
                                    customError_1.default.itemNotFound("user not found for updateUserPassword hash after successful login");
                                    return resolve(0);
                                }
                                resolve(1);
                            });
                        });
                    })];
            });
        });
    };
    Auth.authorized = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        settings_1.default.get(config_1.default.SETTINGS_APP_AUTH_NAME, function (err, data) {
            if (!data) {
                return callback(null, null);
            }
            if (!data.localSession) {
                return callback(null, null);
            }
            var findQuery = { localSession: data.localSession };
            user_1.default.find(findQuery, { useLocalSession: true }, function (err, doc) {
                if (!doc || (doc && !Object.keys(doc).length)) {
                    return callback(null, null);
                }
                loggedUserData = user_1.default.getPublicData(doc);
                return callback(null, loggedUserData);
            });
        });
    };
    Auth.logout = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        settings_1.default.remove(config_1.default.SETTINGS_APP_AUTH_NAME, function (err, status) {
            return callback(err, status);
        });
    };
    Auth.confirmPassword = function (data, doc) {
        var inputCorrectFormat = data && doc && data.password && doc.password;
        return inputCorrectFormat && Auth.cryptPassword(data.password, doc._id) === doc.password;
    };
    Auth.cryptPassword = function (password, salt) {
        return js_md5_1.default(salt + password);
    };
    Auth.cryptSessionToken = function (string, salt) {
        return js_md5_1.default(salt + string + dateHandler_1.default.now() + Math.random());
    };
    Auth.getUser = function (callback) {
        if (callback === void 0) { callback = function (err, res) {
        }; }
        Auth.authorized(function (err, loggedUserData) {
            return callback(err, loggedUserData);
        });
    };
    Auth.getUserAsync = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            Auth.getUser(function (err, userInfo) {
                                return resolve(userInfo);
                            });
                            return [2];
                        });
                    }); })];
            });
        });
    };
    Auth.fetchActualUserAsync = function () {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var authInfo, email;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, Auth.getUserAsync()];
                    case 1:
                        authInfo = _a.sent();
                        if (!authInfo) {
                            return [2, resolve(null)];
                        }
                        email = authInfo.email;
                        if (!email) {
                            return [2, resolve(null)];
                        }
                        user_1.default.find({ email: email }, {}, function (err, itemInstance) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                if (err || !itemInstance) {
                                    return [2, resolve(null)];
                                }
                                return [2, resolve(itemInstance)];
                            });
                        }); });
                        return [2];
                }
            });
        }); });
    };
    Auth.prepareFormSyncData = function (frmData, response) {
        if (response.notes_email) {
            frmData.notesEmail = response.notes_email;
        }
        if (response.premium) {
            frmData.subscribe = response.premium.active ? 1 : 0;
            frmData.paymentStartDate = response.premium.start_date;
            frmData.paymentEndDate = response.premium.end_date;
            frmData.paymentSystemCode = response.premium.source;
        }
        if (response.usage && response.usage.notes) {
            frmData.usageCurrent = response.usage.notes.current;
            frmData.usageMax = response.usage.notes.max;
        }
        if (typeof (response.days_to_quota_reset) !== "undefined") {
            frmData.dateNextQuotaReset = user_1.default.getDateNextQuotaReset(response.days_to_quota_reset);
        }
        return frmData;
    };
    Auth.prepareSyncData = function (updateFields, data) {
        if (data.notesEmail) {
            updateFields.notesEmail = data.notesEmail;
        }
        if (typeof (data.subscribe) !== "undefined") {
            updateFields.subscribe = data.subscribe;
        }
        if (data.paymentStartDate) {
            updateFields.paymentStartDate = data.paymentStartDate;
        }
        if (data.paymentEndDate) {
            updateFields.paymentEndDate = data.paymentEndDate;
        }
        if (data.paymentSystemCode) {
            updateFields.paymentSystemCode = data.paymentSystemCode;
        }
        if (data.usageCurrent) {
            updateFields.usageCurrent = data.usageCurrent;
        }
        if (data.usageMax) {
            updateFields.usageMax = data.usageMax;
        }
        if (typeof (data.dateNextQuotaReset) !== "undefined") {
            updateFields.dateNextQuotaReset = data.dateNextQuotaReset;
        }
        if (typeof (data.authProvider) !== "undefined") {
            updateFields.authProvider = data.authProvider;
        }
        return updateFields;
    };
    Auth.errorCode = {
        ERROR_NONE: 0,
        ERROR_WRONG_AUTH_INPUT: 1,
        ERROR_USER_NOT_FOUND: 2,
        ERROR_USER_ALREADY_EXIST: -4,
        ERROR_USER_REGISTER: 4,
        ERROR_USER_LOGIN: 5,
        ERROR_USER_OFFLINE: 6
    };
    return Auth;
}());
exports.default = Auth;
