"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = __importDefault(require("http"));
var express_1 = __importDefault(require("express"));
var helmet_1 = __importDefault(require("helmet"));
var consolidate_1 = __importDefault(require("consolidate"));
var body_parser_1 = __importDefault(require("body-parser"));
var multer_1 = __importDefault(require("multer"));
var config_runtime_1 = __importDefault(require("../config.runtime"));
var pdb_1 = __importDefault(require("../pdb"));
var urlParser_1 = __importDefault(require("../nimbus-electron/utilities/urlParser"));
var instance_1 = __importDefault(require("../nimbus-electron/window/instance"));
var state_1 = __importDefault(require("../nimbus-electron/online/state"));
var page_1 = __importDefault(require("../nimbus-electron/request/page"));
var jsonInterceptor_1 = __importDefault(require("../nimbus-electron/request/interceptors/jsonInterceptor"));
var NimbusSDK_1 = __importDefault(require("../nimbus-electron/sync/nimbussdk/net/NimbusSDK"));
var auth_1 = __importDefault(require("../nimbus-electron/auth/auth"));
var item_1 = __importDefault(require("../nimbus-electron/db/models/item"));
var workspace_1 = __importDefault(require("../nimbus-electron/db/models/workspace"));
var orgs_1 = __importDefault(require("../nimbus-electron/db/models/orgs"));
var Translations_1 = __importDefault(require("../nimbus-electron/translations/Translations"));
var ReminderNotice_1 = __importDefault(require("../nimbus-electron/popup/ReminderNotice"));
var errorHandler_1 = __importDefault(require("../nimbus-electron/utilities/errorHandler"));
var user_1 = __importDefault(require("../nimbus-electron/db/models/user"));
var SelectedOrganization_1 = __importDefault(require("../nimbus-electron/organization/SelectedOrganization"));
var login_1 = __importDefault(require("../nimbus-electron/request/auth/login"));
var register_1 = __importDefault(require("../nimbus-electron/request/auth/register"));
var remind_1 = __importDefault(require("../nimbus-electron/request/auth/remind"));
var google_1 = __importDefault(require("../nimbus-electron/request/auth/oauth/google"));
var facebook_1 = __importDefault(require("../nimbus-electron/request/auth/oauth/facebook"));
var challenge_1 = __importDefault(require("../nimbus-electron/request/auth/challenge"));
var syncHandler_1 = __importDefault(require("../nimbus-electron/utilities/syncHandler"));
var ERROR_WRONG_REQUEST = -2;
var ERROR_USER_ALREADY_EXISTS = -4;
var ERROR_WRONG_LOGIN_OR_PASSWORD = -6;
var ERROR_USER_NOT_FOUND = -7;
var ERROR_INTERNAL_SERVER_ERROR = -10;
var app = express_1.default();
app.use(helmet_1.default());
app.use(body_parser_1.default.json({ limit: '8mb' }));
app.set('views', [
    config_runtime_1.default.nimbusAuthTemplatesFolderPath,
    config_runtime_1.default.nimbusAngularTemplatesFolderPath,
    config_runtime_1.default.nimbusCustomTemplatesFolderPath
]);
app.engine('html', consolidate_1.default.mustache);
app.set('view engine', 'mustache');
var upload = multer_1.default({ dest: config_runtime_1.default.nimbusAttachmentPath });
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use("/" + config_runtime_1.default.NIMBUS_ANGULAR_PROJ + "/web/static", express_1.default.static(config_runtime_1.default.nimbusAngularFolderPath));
app.use("/" + config_runtime_1.default.NIMBUS_AUTH_PROJ + "/web/auth", express_1.default.static(config_runtime_1.default.nimbusAuthTemplatesFolderPath));
app.use("/" + config_runtime_1.default.NIMBUS_AUTH_PROJ + "/web", express_1.default.static(config_runtime_1.default.nimbusAuthWebFolderPath));
app.use("/custom/static", express_1.default.static(config_runtime_1.default.nimbusCustomStaticFolderPath));
app.use("/static", express_1.default.static(config_runtime_1.default.nimbusAngularStaticFolderPath));
app.use("/vendor", express_1.default.static(config_runtime_1.default.nimbusModulesPath));
var methods = {
    'GET': 'get',
    'POST': 'post',
    'PUT': 'put',
    'DELETE': 'delete'
};
var noteRequests = [
    { httpMethod: 'GET', url: '/v1/trial/proTrial', action: 'me/apiProTrial' },
    { httpMethod: 'GET', url: '/api/me', action: 'me/apiMe' },
    { httpMethod: 'POST', url: '/api/me/profile', action: 'me/apiMeNameUpdate' },
    { httpMethod: 'POST', url: '/api/me', action: 'me/apiMeUpdate' },
    { httpMethod: 'GET', url: '/v1/survey/profession*', action: 'me/apiUserProfession' },
    { httpMethod: 'GET', url: '/api/user-settings', action: 'me/apiUserSettings' },
    { httpMethod: 'POST', url: '/api/user-settings', action: 'me/apiUserSettingsUpdate' },
    { httpMethod: 'POST', url: '/v1/users/vars/*', action: 'users/apiUserVariableUpdate' },
    { httpMethod: 'GET', url: '/v1/users/*/vars/ignoreIntro', action: 'users/apiUserIgnoreIntro' },
    { httpMethod: 'GET', url: '/v1/users/*/vars/ignoreSharesInfo', action: 'users/apiUserIgnoreSharesInfo' },
    { httpMethod: 'GET', url: '/v1/users/*/vars/noteAppearance', action: 'users/apiUserNoteAppearance' },
    { httpMethod: 'GET', url: '/v1/otp/setup*', action: 'users/apiUserOtp' },
    { httpMethod: 'POST', url: '/v1/otp/setup', action: 'users/apiUserOtpUpdate' },
    { httpMethod: 'POST', url: '/v1/otp/issue', action: 'users/apiUserOtpIssue' },
    { httpMethod: 'DELETE', url: '/v1/otp/setup', action: 'otp/apiUserOtpRemove' },
    { httpMethod: 'GET', url: '/api/me/sync', action: 'me/apiSync' },
    { httpMethod: 'POST', url: '/api/me/sync', action: 'me/apiSyncUpdate' },
    { httpMethod: 'GET', url: '/api/app/settings', action: 'app/apiAppSettings' },
    { httpMethod: 'POST', url: '/api/app/settings', action: 'app/apiAppSettingsUpdate' },
    { httpMethod: 'GET', url: '/v1/workspaces/*/mentions/workspaceInfo', action: 'mention/apiMentionsWorkspace' },
    { httpMethod: 'GET', url: '/api/workspaces/*/info', action: 'me/apiWorkspaceInfoGet' },
    { httpMethod: 'GET', url: '/api/workspaces/*/usage', action: 'me/apiWorkspaceUsageGet' },
    { httpMethod: 'GET', url: '/api/workspaceEmails/*/', action: 'me/apiWorkspaceEmailsGet' },
    { httpMethod: 'POST', url: '/api/me/logout', action: 'me/apiMeLogout' },
    { httpMethod: 'DELETE', url: '/logout', action: 'me/apiMeLogout' },
    { httpMethod: 'POST', url: '/api/workspaces/*/notes/*/export*', action: 'export/apiItemExport' },
    { httpMethod: 'GET', url: '/v1/workspaces/*/previews*', action: 'preview/apiPreviewList' },
    { httpMethod: 'GET', url: '/v1/workspaces/*/previews*', action: 'preview/apiPreviewList' },
    { httpMethod: 'POST', url: '/v1/workspaces/*/previews/*', action: 'preview/apiPreviewUpdate' },
    { httpMethod: 'DELETE', url: '/v1/workspaces/*/previews/*', action: 'preview/apiPreviewRemove' },
    { httpMethod: 'GET', url: '/api/workspaces/*/notes/*/text', action: 'text/apiText' },
    { httpMethod: 'POST', url: '/api/workspaces/*/notes/*/text', action: 'text/apiTextUpdate' },
    { httpMethod: 'GET', url: '/api/workspaces/*/notes/*/tags', action: 'tag/apiNoteTagsList' },
    { httpMethod: 'POST', url: '/api/workspaces/*/notesTags', action: 'tag/apiNotesTagsUpdate' },
    { httpMethod: 'POST', url: '/api/workspaces/*/notes/*/tags/*', action: 'tag/apiNoteTagsUpdate' },
    { httpMethod: 'DELETE', url: '/api/workspaces/*/notes/*/tags/*', action: 'tag/apiNoteTagsRemove' },
    { httpMethod: 'GET', url: '/api/workspaces/*/notes/*/todos', action: 'todo/apiNoteTodoList' },
    { httpMethod: 'GET', url: '/api/workspaces/*/notes/*/todosOrder', action: 'todo/apiTodoOrder' },
    { httpMethod: 'POST', url: '/api/workspaces/*/notes/*/todosOrder', action: 'todo/apiTodoOrderUpdate' },
    { httpMethod: 'GET', url: '/api/workspaces/*/reminders', action: 'attach/apiNoteRemindersList' },
    { httpMethod: 'POST', url: '/api/workspaces/*/notes/*/reminder', action: 'attach/apiNoteReminderUpdate' },
    { httpMethod: 'DELETE', url: '/api/workspaces/*/notes/*/reminder', action: 'attach/apiNoteReminderRemove' },
    { httpMethod: 'POST', url: '/v1/workspaces/*/duplicateRequest', action: 'item/apiItemDuplicate' },
    { httpMethod: 'GET', url: '/api/workspaces/*/notes', action: 'item/apiItemList' },
    { httpMethod: 'GET', url: '/api/workspaces/*/notes*', action: 'item/apiItem' },
    { httpMethod: 'POST', url: '/api/workspaces/*/notesParentId', action: 'item/apiItemParentId' },
    { httpMethod: 'POST', url: '/api/workspaces/*/notes*', action: 'item/apiItemUpdate' },
    { httpMethod: 'DELETE', url: '/api/workspaces/*/notes*', action: 'item/apiItemRemove' },
    { httpMethod: 'GET', url: '/api/workspaces/*/texts', action: 'text/apiTextList' },
    { httpMethod: 'GET', url: '/api/workspaces/*/texts*', action: 'text/apiText' },
    { httpMethod: 'POST', url: '/api/workspaces/*/texts/*/tokens', action: 'text/apiTextTokenUpdate' },
    { httpMethod: 'GET', url: '/api/workspaces/*/tags', action: 'tag/apiTagList' },
    { httpMethod: 'GET', url: '/api/workspaces/*/tags*', action: 'tag/apiTagList' },
    { httpMethod: 'POST', url: '/api/workspaces/*/tags*', action: 'tag/apiTagUpdate' },
    { httpMethod: 'DELETE', url: '/api/workspaces/*/tags*', action: 'tag/apiTagRemove' },
    { httpMethod: 'GET', url: '/api/workspaces/*/todos', action: 'todo/apiTodoList' },
    { httpMethod: 'GET', url: '/api/workspaces/*/todos*', action: 'todo/apiTodo' },
    { httpMethod: 'POST', url: '/api/workspaces/*/todos*', action: 'todo/apiTodoUpdate' },
    { httpMethod: 'DELETE', url: '/api/workspaces/*/todos*', action: 'todo/apiTodoRemove' },
    { httpMethod: 'GET', url: '/api/workspaces/*/attachments', action: 'attach/apiAttachList' },
    { httpMethod: 'GET', url: '/api/workspaces/*/attachments*', action: 'attach/apiAttach' },
    { httpMethod: 'POST', url: '/api/workspaces/*/attachments*', action: 'attach/apiAttachUpdate' },
    { httpMethod: 'DELETE', url: '/api/workspaces/*/attachments*', action: 'attach/apiAttachRemove' },
    { httpMethod: 'GET', url: '/v1/workspaces/*/annotations/*', action: 'annotation/apiAnnotation' },
    { httpMethod: 'GET', url: '/v1/workspaces/*/annotations', action: 'annotation/apiAnnotations' },
    { httpMethod: 'GET', url: '/api/premium', action: 'premium/apiPremium' },
    { httpMethod: 'GET', url: '/v1/workspaces/*/premium', action: 'premium/apiWorkspacePremium' },
    { httpMethod: 'GET', url: '/ws/*/recent', action: 'client/apiClientRecent' },
    { httpMethod: 'GET', url: '/api/usages/limit-checker', action: 'limit/apiLimitChecker' },
    { httpMethod: 'GET', url: '/v1/organizations/*/limits', action: 'limit/apiOrganizationLimits' },
    { httpMethod: 'GET', url: '/v1/users/*', action: 'users/userFindById' },
    { httpMethod: 'GET', url: '/users', action: 'users/userFind' },
    { httpMethod: 'GET', url: '/api/invites', action: 'workspace/apiInvitesList' },
    { httpMethod: 'POST', url: '/api/workspaces/*/members', action: 'workspace/apiWorkspaceInviteAdd' },
    { httpMethod: 'POST', url: '/api/invitesById/*/send', action: 'workspace/apiWorkspaceInviteResend' },
    { httpMethod: 'POST', url: '/api/invitesById/*', action: 'workspace/apiWorkspaceInviteUpdate' },
    { httpMethod: 'DELETE', url: '/api/invitesById/*', action: 'workspace/apiWorkspaceInviteRemove' },
    { httpMethod: 'GET', url: '/v1/workspaces/*/emails*', action: 'workspace/apiWorkspaceEmail' },
    { httpMethod: 'POST', url: '/v1/workspaces/*/changeWorkspaceRequest', action: 'workspace/apiWorkspaceChange' },
    { httpMethod: 'GET', url: '/v1/workspaces/*/members', action: 'workspace/apiMembersList' },
    { httpMethod: 'POST', url: '/v1/members/*', action: 'workspace/apiWorkspaceMemberUpdate' },
    { httpMethod: 'POST', url: '/v1/workspaces/*/members/*', action: 'workspace/apiWorkspaceMemberUpdate' },
    { httpMethod: 'DELETE', url: '/v1/workspaces/*/members/*', action: 'workspace/apiWorkspaceMemberRemove' },
    { httpMethod: 'GET', url: '/v1/workspaces', action: 'workspace/apiWorkspacesList' },
    { httpMethod: 'POST', url: '/v1/workspaces', action: 'workspace/apiWorkspaceUpdate' },
    { httpMethod: 'POST', url: '/v1/workspaces/*', action: 'workspace/apiWorkspaceUpdate' },
    { httpMethod: 'DELETE', url: '/v1/workspaces/*', action: 'workspace/apiWorkspaceRemove' }
];
pdb_1.default.prepareClientDbPath();
app.get(config_runtime_1.default.nimbusAuthIndexFile, function (req, res) {
    return res.render('auth.html');
});
function getConfigUrl(_a) {
    var version = _a.version, _b = _a.publicPath, publicPath = _b === void 0 ? '/' : _b;
    return publicPath + "static/js/config.service.js?version=" + version;
}
function loadWebNotePage(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var startTime, authInfo, userVariables, trial, profile, workspacesCount, workspacesList, _a, organizations, workspaces, workspacesAccess, accountUserId, activeOrganization, configUrl, hydratedState;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    startTime = Date.now();
                    return [4, auth_1.default.fetchActualUserAsync()];
                case 1:
                    authInfo = _b.sent();
                    userVariables = authInfo && authInfo.variables ? authInfo.variables : user_1.default.getDefaultVariables();
                    if (userVariables && typeof userVariables['isPinnedLeftBlock'] === 'undefined') {
                        userVariables['isPinnedLeftBlock'] = true;
                    }
                    return [4, user_1.default.getTrialAsync(authInfo.userId)];
                case 2:
                    trial = _b.sent();
                    return [4, user_1.default.getProfileAsync(authInfo.email)];
                case 3:
                    profile = _b.sent();
                    if (authInfo && Object.keys(authInfo).length) {
                        if (pdb_1.default.getDbClientId() != authInfo.userId) {
                            pdb_1.default.setDbClientId(authInfo.userId);
                        }
                    }
                    else {
                        return [2, page_1.default.load(config_runtime_1.default.PAGE_AUTH_NAME)];
                    }
                    return [4, workspace_1.default.getUserWorkspacesCount()];
                case 4:
                    workspacesCount = _b.sent();
                    if (!!workspacesCount) return [3, 6];
                    return [4, orgs_1.default.syncUserOrganizationsWorkspaces()];
                case 5:
                    _b.sent();
                    _b.label = 6;
                case 6: return [4, workspace_1.default.findUserWorkspaces()];
                case 7:
                    workspacesList = _b.sent();
                    return [4, SelectedOrganization_1.default.getUserOrganizationsDetailsByWorkspaces(authInfo, workspacesList)];
                case 8:
                    _a = _b.sent(), organizations = _a.organizations, workspaces = _a.workspaces, workspacesAccess = _a.workspacesAccess;
                    return [4, user_1.default.getAccountUserId()];
                case 9:
                    accountUserId = _b.sent();
                    return [4, SelectedOrganization_1.default.getCurrentOrganization(authInfo)];
                case 10:
                    activeOrganization = _b.sent();
                    if (!activeOrganization) {
                        activeOrganization = { globalId: null, userId: accountUserId };
                    }
                    if (workspaces.length) {
                        ReminderNotice_1.default.init(workspaces);
                    }
                    configUrl = getConfigUrl({
                        version: config_runtime_1.default.WEBNOTES_VERSION,
                        publicPath: '/',
                    });
                    hydratedState = {
                        electronOrganization: activeOrganization,
                        organizations: organizations,
                        workspaces: workspaces,
                        workspacesAccessMap: workspacesAccess,
                        userVariables: userVariables,
                        trial: trial,
                        profile: profile,
                    };
                    return [2, res.render('index.html', {
                            loadTime: Date.now() - startTime,
                            version: config_runtime_1.default.WEBNOTES_VERSION,
                            hydratedState: JSON.stringify(hydratedState),
                            configUrl: configUrl,
                            favicon: '',
                        })];
            }
        });
    });
}
app.get("/" + config_runtime_1.default.NIMBUS_ANGULAR_PROJ + "/web/templates/index.html", loadWebNotePage);
app.get('/client', loadWebNotePage);
app.get('/ws/*/recent', loadWebNotePage);
app.get('/ws/*/recent/note/*', loadWebNotePage);
app.get('/ws/*/folder/*/note/*', loadWebNotePage);
app.get('/ws/*/folder/*', loadWebNotePage);
app.get('/ws/*/tag/*', loadWebNotePage);
app.get('/ws/*/search*', loadWebNotePage);
app.get('/ws/*/favorites*', loadWebNotePage);
app.get('/ws/*/tag/*/note/*', loadWebNotePage);
app.get('/ws/*/settings*', function () {
    if (instance_1.default.get()) {
        instance_1.default.get().webContents.send('event:client:display:settings:response');
    }
});
app.get("/" + config_runtime_1.default.NIMBUS_ANGULAR_PROJ + config_runtime_1.default.nimbusPaymentUrlPath, function (req, res) {
    return res.render('payment.html');
});
app.get("/" + config_runtime_1.default.NIMBUS_ANGULAR_PROJ + config_runtime_1.default.nimbusMessageUrlPath, function (req, res) {
    return res.render('message.html');
});
app.get("/" + config_runtime_1.default.NIMBUS_ANGULAR_PROJ + config_runtime_1.default.nimbusUpdateUrlPath, function (req, res) {
    return res.render('update.html');
});
app.get('/server-message/web', function (req, res) {
    return res.status(200).json({});
});
app.get('/api/workspaces/*/note-todo', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var noteGlobalId, getTodoList, getTodoOrder, todoList, todoOrder, unorderedTodoList, orderedTodoList;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                noteGlobalId = req.query.noteGlobalId;
                if (!noteGlobalId) {
                    return [2, res.status(404).json({})];
                }
                getTodoList = function () { return __awaiter(void 0, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2, new Promise(function (resolve) {
                                var options = {
                                    host: config_runtime_1.default.LOCAL_SERVER_HOST,
                                    port: config_runtime_1.default.LOCAL_SERVER_PORT,
                                    path: '/api/notes/' + noteGlobalId + '/todos/'
                                };
                                http_1.default.get(options, function (res) {
                                    var body = '';
                                    res.on('data', function (chunk) {
                                        body += chunk;
                                    });
                                    res.on('end', function () {
                                        resolve(body ? JSON.parse(body) : []);
                                    });
                                }).on('error', function (e) {
                                    resolve(null);
                                });
                            })];
                    });
                }); };
                getTodoOrder = function () { return __awaiter(void 0, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2, new Promise(function (resolve) {
                                var options = {
                                    host: config_runtime_1.default.LOCAL_SERVER_HOST,
                                    port: config_runtime_1.default.LOCAL_SERVER_PORT,
                                    path: '/api/notes/' + noteGlobalId + '/todoOrder/'
                                };
                                http_1.default.get(options, function (res) {
                                    var body = '';
                                    res.on('data', function (chunk) {
                                        body += chunk;
                                    });
                                    res.on('end', function () {
                                        resolve(body ? JSON.parse(body) : []);
                                    });
                                }).on('error', function (e) {
                                    resolve(null);
                                });
                            })];
                    });
                }); };
                return [4, getTodoList()];
            case 1:
                todoList = _a.sent();
                return [4, getTodoOrder()];
            case 2:
                todoOrder = _a.sent();
                unorderedTodoList = todoList.slice();
                orderedTodoList = [];
                if (todoOrder.length) {
                    todoOrder.forEach(function (todo) {
                        var found = false;
                        unorderedTodoList = unorderedTodoList.filter(function (item) {
                            if (!found && item.globalId == todo) {
                                orderedTodoList.push(item);
                                found = true;
                                return false;
                            }
                            else {
                                return true;
                            }
                        });
                    });
                }
                unorderedTodoList = unorderedTodoList.sort(function (todo1, todo2) {
                    return todo1.dateAdded - todo2.dateAdded;
                });
                orderedTodoList = orderedTodoList.concat(unorderedTodoList);
                return [2, res.status(200).json(orderedTodoList)];
        }
    });
}); });
app.post('/api/workspaces/*/note-tags-batch', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var routeParams, workspaceId, _a, tagsToAdd, tagsToDelete, _response, i, processTag;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                routeParams = urlParser_1.default.getPathParams(req.url);
                if (!typeof (routeParams[3] !== 'undefined')) return [3, 2];
                return [4, workspace_1.default.getLocalId(routeParams[3])];
            case 1:
                _a = _b.sent();
                return [3, 3];
            case 2:
                _a = null;
                _b.label = 3;
            case 3:
                workspaceId = _a;
                tagsToAdd = req.body.tagsToAdd;
                tagsToDelete = req.body.tagsToDelete;
                _response = { status: 200, message: {} };
                processTag = function (inputData) { return __awaiter(void 0, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2, new Promise(function (resolve) {
                                var workspaceId = inputData.workspaceId, requestMethod = inputData.requestMethod, noteGlobalId = inputData.noteGlobalId, tag = inputData.tag;
                                tag = encodeURIComponent(tag);
                                var options = {
                                    host: config_runtime_1.default.LOCAL_SERVER_HOST,
                                    port: config_runtime_1.default.LOCAL_SERVER_PORT,
                                    method: requestMethod,
                                    path: "/api/workspaces/" + workspaceId + "/notes/" + noteGlobalId + "/tags/" + tag
                                };
                                var tagRequest = http_1.default.request(options, function (res) {
                                    var statusCode = res.statusCode;
                                    res.on('data', function () {
                                        resolve({
                                            status: statusCode ? statusCode : 500,
                                            message: {}
                                        });
                                    });
                                }).on('error', function (err) {
                                    resolve({
                                        status: 500,
                                        message: err
                                    });
                                });
                                tagRequest.end();
                            })];
                    });
                }); };
                i = 0;
                _b.label = 4;
            case 4:
                if (!(i < tagsToAdd.length)) return [3, 7];
                if (_response.status != 200)
                    return [3, 7];
                return [4, processTag({
                        workspaceId: workspaceId,
                        requestMethod: 'POST',
                        noteGlobalId: req.body.noteGlobalId,
                        tag: tagsToAdd[i]
                    })];
            case 5:
                _response = (_b.sent());
                _b.label = 6;
            case 6:
                i++;
                return [3, 4];
            case 7:
                i = 0;
                _b.label = 8;
            case 8:
                if (!(i < tagsToDelete.length)) return [3, 11];
                if (_response.status != 200)
                    return [3, 11];
                return [4, processTag({
                        workspaceId: workspaceId,
                        requestMethod: 'DELETE',
                        noteGlobalId: req.body.noteGlobalId,
                        tag: tagsToDelete[i]
                    })];
            case 9:
                _response = (_b.sent());
                _b.label = 10;
            case 10:
                i++;
                return [3, 8];
            case 11: return [2, res.status(_response.status).json(_response.message)];
        }
    });
}); });
app.get('/attachment/*/*/preview*', function (req, res) {
    var actionReq = __assign(__assign({}, req), { action: 'attach/apiAttachPreview' });
    processRequest(actionReq, res, fileRequest);
});
app.get('/file/avatar/*', function (req, res) {
    var actionReq = __assign(__assign({}, req), { action: 'attach/apiAvatarPreview' });
    processRequest(actionReq, res, fileRequest);
});
app.get('/attachment*', function (req, res) {
    var actionReq = __assign(__assign({}, req), { action: 'attach/apiAttachDownload' });
    processRequest(actionReq, res, fileRequest);
});
app.post('/api/me/avatar', upload.single('avatar'), function (req, res) {
    var actionReq = __assign(__assign({}, req), { action: 'me/apiMeAvatarUpdate' });
    processRequest(actionReq, res, httpRequest);
});
app.delete('/api/me/avatar', function (req, res) {
    var actionReq = __assign(__assign({}, req), { action: 'me/apiMeAvatarDelete' });
    processRequest(actionReq, res, httpRequest);
});
app.put('/api/workspaces/*/attachments*', upload.fields([{ name: "attachment" }]), function (req, res) {
    var actionReq = __assign(__assign({}, req), { action: 'attach/apiAttachCreate' });
    processRequest(actionReq, res, httpRequest);
});
function apiChangePassword(oldPassword, newPassword, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    NimbusSDK_1.default.getApi().userChangePassword(oldPassword, newPassword, function (err, response) {
        callback(err, response);
    });
}
app.post('/api/me/changePassword', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var defaultErrorResponse, oldPassword, newPassword;
    return __generator(this, function (_a) {
        defaultErrorResponse = {
            'name': 'WrongPassword',
            'field': 'password'
        };
        if (!req.body.oldPassword || !req.body.newPassword) {
            return [2, res.status(400).json(defaultErrorResponse)];
        }
        oldPassword = req.body.oldPassword;
        newPassword = req.body.newPassword;
        auth_1.default.getUser(function (err, authInfo) {
            if (authInfo && Object.keys(authInfo).length) {
                apiChangePassword(oldPassword, newPassword, function (err, response) {
                    if (err || !response) {
                        return res.status(400).json(defaultErrorResponse);
                    }
                    var actionReq = __assign(__assign({}, req), { action: 'me/apiMeChangePassword', authInfo: authInfo });
                    getRequestActionPath(actionReq)(actionReq, function (err, data) {
                        if (err || !data) {
                            return res.status(400).json(defaultErrorResponse);
                        }
                        return res.status(200).json(data);
                    });
                });
            }
            else {
                return res.status(400).json(defaultErrorResponse);
            }
        });
        return [2];
    });
}); });
app.post('/api/workspaces/*/limit-checker', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        auth_1.default.getUser(function (err, authInfo) {
            if (err) {
                return res.status(404).json({});
            }
            if (authInfo && Object.keys(authInfo).length) {
                var actionReq = __assign(__assign({}, req), { action: 'limit/apiLimitChecker', body: __assign(__assign({}, (req.body || {})), { subscribe: authInfo.subscribe || 0 }) });
                getRequestActionPath(actionReq)(actionReq, function (err, data) {
                    if (err || !data) {
                        return res.status(404).json({});
                    }
                    var responseStatus = 200;
                    if (data && data.httpStatus) {
                        responseStatus = data.httpStatus;
                    }
                    try {
                        return res.status(responseStatus).json(data);
                    }
                    catch (e) {
                        errorHandler_1.default.log(e == null ? "unknown" : (e.stack || e).toString());
                    }
                });
            }
        });
        return [2];
    });
}); });
function apiShareCreate(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    var workspaceId = inputData.workspaceId, globalId = inputData.globalId, password = inputData.password;
    var globalIds = [globalId];
    NimbusSDK_1.default.getApi().shareNotes({ workspaceId: workspaceId, globalIds: globalIds, password: password }, callback);
}
function apiShareRemove(inputData, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
    var globalIds = [globalId];
    NimbusSDK_1.default.getApi().unshareNotes({ workspaceId: workspaceId, globalIds: globalIds }, function (err, response) {
        console.log('=======> err?', err);
        console.log('=======> response?', response);
        callback(err, response);
    });
}
function checkShareItemIsOffline(inputData) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2, new Promise(function (resolve) {
                    var workspaceId = inputData.workspaceId, globalId = inputData.globalId;
                    item_1.default.find({ globalId: globalId }, { workspaceId: workspaceId }, function (err, itemInstance) {
                        if (itemInstance && itemInstance.offlineOnly) {
                            return resolve(true);
                        }
                        resolve(false);
                    });
                })];
        });
    });
}
app.get('/api/workspaces/*/notes/*/share', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var routeParams, workspaceId, _a, globalId, shareItemIsOffline;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                routeParams = urlParser_1.default.getPathParams(req.url);
                if (!typeof (routeParams[3] !== 'undefined')) return [3, 2];
                return [4, workspace_1.default.getLocalId(routeParams[3])];
            case 1:
                _a = _b.sent();
                return [3, 3];
            case 2:
                _a = null;
                _b.label = 3;
            case 3:
                workspaceId = _a;
                globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;
                if (!globalId) {
                    return [2, res.status(404).json({})];
                }
                return [4, checkShareItemIsOffline({ workspaceId: workspaceId, globalId: globalId })];
            case 4:
                shareItemIsOffline = _b.sent();
                if (shareItemIsOffline) {
                    if (instance_1.default.get()) {
                        instance_1.default.get().webContents.send('event:client:workspace:message:response', {
                            message: Translations_1.default.get('toast__can_not_share__offline_item'),
                        });
                    }
                    return [2, res.status(504).json({
                            name: "OfflineError",
                            type: "share"
                        })];
                }
                auth_1.default.getUser(function (err, authInfo) {
                    if (authInfo && Object.keys(authInfo).length) {
                        var actionReq = __assign(__assign({}, req), { action: 'share/apiShare', body: __assign(__assign({}, req.body), { globalId: globalId }) });
                        getRequestActionPath(actionReq)(actionReq, function (err, data) {
                            if (err || !data) {
                                return res.status(404).json({});
                            }
                            data = {
                                accessAllowed: false,
                                dateUpdated: data.dateUpdated,
                                id: data.shareId,
                                noteGlobalId: globalId,
                                passwordRequired: data.passwordRequired,
                                securityKey: data.securityKey,
                                userId: authInfo.userId,
                                shared_url: data.shared_url || '',
                            };
                            return res.status(200).json(data);
                        });
                    }
                    else {
                        return res.status(404).json({});
                    }
                });
                return [2];
        }
    });
}); });
app.post('/api/workspaces/*/notes/*/share', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var routeParams, password, workspaceId, _a, globalId, shareItemIsOffline;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                routeParams = urlParser_1.default.getPathParams(req.url);
                password = req.body.password || null;
                if (!typeof (routeParams[3] !== 'undefined')) return [3, 2];
                return [4, workspace_1.default.getLocalId(routeParams[3])];
            case 1:
                _a = _b.sent();
                return [3, 3];
            case 2:
                _a = null;
                _b.label = 3;
            case 3:
                workspaceId = _a;
                globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;
                if (!globalId) {
                    return [2, res.status(404).json({})];
                }
                if (!state_1.default.get()) {
                    if (instance_1.default.get()) {
                        instance_1.default.get().webContents.send('event:client:workspace:message:response', {
                            message: Translations_1.default.get('toast__can_not_share__offline'),
                        });
                    }
                    return [2, res.status(504).json({
                            name: "OfflineError",
                            type: "share"
                        })];
                }
                return [4, checkShareItemIsOffline({ workspaceId: workspaceId, globalId: globalId })];
            case 4:
                shareItemIsOffline = _b.sent();
                if (shareItemIsOffline) {
                    if (instance_1.default.get()) {
                        instance_1.default.get().webContents.send('event:client:workspace:message:response', {
                            message: Translations_1.default.get('toast__can_not_share__offline_item'),
                        });
                    }
                    return [2, res.status(504).json({
                            name: "OfflineError",
                            type: "share"
                        })];
                }
                auth_1.default.getUser(function (err, authInfo) {
                    if (authInfo && Object.keys(authInfo).length) {
                        apiShareCreate({ workspaceId: workspaceId, globalId: globalId, password: password }, function (err, response) { return __awaiter(void 0, void 0, void 0, function () {
                            var shareParams, shareId, shareSecurityKey, actionReq;
                            return __generator(this, function (_a) {
                                if (err) {
                                    if (err === -19) {
                                        if (instance_1.default.get()) {
                                            instance_1.default.get().webContents.send('event:client:workspace:message:response', {
                                                message: Translations_1.default.get('toast__need_sync_before__share'),
                                            });
                                        }
                                    }
                                    return [2, res.status(404).json({
                                            name: "OfflineError",
                                            type: "share"
                                        })];
                                }
                                if (typeof (response[globalId]) === "undefined") {
                                    return [2, res.status(404).json({})];
                                }
                                shareParams = urlParser_1.default.getPathParams(response[globalId]);
                                if (shareParams.length < 3) {
                                    return [2, res.status(404).json({})];
                                }
                                shareId = typeof (shareParams[4]) !== 'undefined' ? shareParams[4] : shareParams[2];
                                shareSecurityKey = typeof (shareParams[5]) !== 'undefined' ? shareParams[5] : shareParams[3];
                                actionReq = __assign(__assign({}, req), { action: 'share/apiShareUpdate', body: __assign(__assign({}, req.body), { globalId: globalId, shared: true, passwordRequired: !!password, shareId: shareId, securityKey: shareSecurityKey, shared_url: response[globalId] }) });
                                getRequestActionPath(actionReq)(actionReq, function (err, data) {
                                    if (err || !data) {
                                        return res.status(404).json({});
                                    }
                                    data = {
                                        accessAllowed: false,
                                        dateUpdated: data.dateUpdated,
                                        id: data.shareId,
                                        noteGlobalId: globalId,
                                        passwordRequired: data.passwordRequired,
                                        securityKey: data.securityKey,
                                        userId: authInfo.userId,
                                        shared_url: data.shared_url,
                                    };
                                    return res.status(200).json(data);
                                });
                                return [2];
                            });
                        }); });
                    }
                    else {
                        return res.status(404).json({});
                    }
                });
                return [2];
        }
    });
}); });
app.delete('/api/workspaces/*/notes/*/share', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var routeParams, globalId, workspaceId, _a, shareItemIsOffline;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                routeParams = urlParser_1.default.getPathParams(req.url);
                globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;
                if (!typeof (routeParams[3] !== 'undefined')) return [3, 2];
                return [4, workspace_1.default.getLocalId(routeParams[3])];
            case 1:
                _a = _b.sent();
                return [3, 3];
            case 2:
                _a = null;
                _b.label = 3;
            case 3:
                workspaceId = _a;
                if (!globalId) {
                    return [2, res.status(404).json({})];
                }
                if (!state_1.default.get()) {
                    if (instance_1.default.get()) {
                        instance_1.default.get().webContents.send('event:client:workspace:message:response', {
                            message: Translations_1.default.get('toast__can_not_share__offline'),
                        });
                    }
                    return [2, res.status(504).json({
                            name: "OfflineError",
                            type: "share"
                        })];
                }
                return [4, checkShareItemIsOffline({ workspaceId: workspaceId, globalId: globalId })];
            case 4:
                shareItemIsOffline = _b.sent();
                if (shareItemIsOffline) {
                    if (instance_1.default.get()) {
                        instance_1.default.get().webContents.send('event:client:workspace:message:response', {
                            message: Translations_1.default.get('toast__can_not_share__offline_item'),
                        });
                    }
                    return [2, res.status(504).json({
                            name: "OfflineError",
                            type: "share"
                        })];
                }
                apiShareRemove({ workspaceId: workspaceId, globalId: globalId }, function (err, response) {
                    if (err) {
                        return res.status(404).json({});
                    }
                    var actionReq = __assign({ action: 'share/apiShareUpdate', body: __assign(__assign({}, req.body), { globalId: globalId, shared: false, passwordRequired: false, shareId: '', securityKey: '', shared_url: '' }) }, req);
                    getRequestActionPath(actionReq)(actionReq, function (err, data) {
                        return res.status(200).json({});
                    });
                });
                return [2];
        }
    });
}); });
noteRequests.forEach(function (request) {
    var requestMethod = methods[request.httpMethod];
    if (!requestMethod) {
        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
            console.error('Wrong method: ', request.httpMethod, ' for url ', request.url);
        }
        return;
    }
    if (!request.action) {
        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
            console.error('No action: ', request.httpMethod, ' for url ', request.url);
        }
        return;
    }
    app[requestMethod](request.url, function (req, res) {
        req.action = request.action;
        processRequest(req, res, httpRequest);
    });
});
function processRequest(req, res, callback) {
    auth_1.default.getUser(function (err, authInfo) {
        if (authInfo && Object.keys(authInfo).length) {
            if (pdb_1.default.getDbClientId() != authInfo.userId) {
                pdb_1.default.setDbClientId(authInfo.userId);
            }
            req.authInfo = authInfo;
            return callback(req, res);
        }
        else {
            page_1.default.reload(false);
            return res.status(401).json({});
        }
    });
}
function fileRequest(req, res) {
    getRequestActionPath(req)(req, res);
}
function httpRequest(req, res) {
    getRequestActionPath(req)(req, function (error, data) {
        var responseStatus = 200;
        if (data && data.httpStatus) {
            responseStatus = data.httpStatus;
            delete data.httpStatus;
        }
        try {
            if (data && data.textResponse) {
                delete data.textResponse;
                res.status(responseStatus).send(data.text);
            }
            else {
                res.status(responseStatus).json(data);
            }
        }
        catch (e) {
            errorHandler_1.default.log(e == null ? "unknown" : (e.stack || e).toString());
        }
    });
}
function getRequestActionPath(req) {
    var actionPath = req.action ? req.action.split('/') : '';
    var action = actionPath && actionPath.length > 1 ? actionPath[actionPath.length - 1] : req.action;
    return jsonInterceptor_1.default[action];
}
function apiSignIn(email, password, callback) {
    if (callback === void 0) { callback = function (err, res) {
    }; }
    NimbusSDK_1.default.getApi().signIn(email, password, function (err, response) {
        callback(err, response);
        if (!err) {
            NimbusSDK_1.default.getApi().userLogin(function (err, login) {
                if (instance_1.default.get()) {
                    instance_1.default.get().webContents.send('event:client:login:response', { login: login });
                }
            });
        }
    });
}
app.get('/ouath/success/:provider', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var provider, code, token, _a, status, authInfo, _b, email, sessionId;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                provider = req.params.provider;
                code = req.query.code;
                token = null;
                if (!(provider && code)) return [3, 5];
                _a = provider;
                switch (_a) {
                    case login_1.default.AUTH_PROVIDER_GOOGLE: return [3, 1];
                    case login_1.default.AUTH_PROVIDER_FACEBOOK: return [3, 3];
                }
                return [3, 5];
            case 1: return [4, google_1.default.getIdToken(code)];
            case 2:
                token = _c.sent();
                return [3, 5];
            case 3: return [4, facebook_1.default.getAccessToken(code)];
            case 4:
                token = _c.sent();
                return [3, 5];
            case 5:
                syncHandler_1.default.sendLog("=> provider: " + provider);
                syncHandler_1.default.sendLog("=> token exist: " + !!token);
                status = token ? 'Success' : 'Denied';
                if (!token) return [3, 7];
                return [4, NimbusSDK_1.default.getApi().oauth({ provider: provider, token: token })];
            case 6:
                authInfo = _c.sent();
                try {
                    _b = authInfo.body, email = _b.email, sessionId = _b.sessionId;
                    syncHandler_1.default.sendLog("=> email: " + email);
                    syncHandler_1.default.sendLog("=> sessionId exist: " + !!sessionId);
                    login_1.default.tryAuthClient(email, sessionId, provider);
                }
                catch (error) {
                    errorHandler_1.default.log(error.message);
                }
                _c.label = 7;
            case 7:
                res.send("<html lang=\"en\"><head><title>" + status + "</title></head><body></body></html>");
                return [2];
        }
    });
}); });
app.post('/auth/api/challenge', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login;
    return __generator(this, function (_a) {
        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
            console.log('/api/challenge');
        }
        login = req.body.login;
        challenge_1.default.tryChallengeClient(req.body, function (response) {
            var body = response.body;
            if (body && body.sessionId) {
                login_1.default.tryAuthClient(login, body.sessionId);
            }
            res.json(__assign({}, response));
        });
        return [2];
    });
}); });
app.post('/auth/api/auth', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
            console.log('/api/auth');
        }
        login_1.default.tryLoginClient(req.body, function (response) {
            res.json(__assign({}, response));
        });
        return [2];
    });
}); });
app.post('/auth/api/register', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
            console.log('/api/register');
        }
        register_1.default.tryRegisterClient(req.body, function (response) {
            res.json(__assign({}, response));
        });
        return [2];
    });
}); });
app.post('/auth/api/remind', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        if (config_runtime_1.default.SHOW_WEB_CONSOLE) {
            console.log('/api/remind');
        }
        remind_1.default.tryRemindPassword(req.body, function (response) {
            res.json(__assign({}, response));
        });
        return [2];
    });
}); });
module.exports = app;
