import chalk from "chalk";
import fs from "fs-extra";
import path from "path";
import PDFWindow from "electron-pdf-window";
import * as Sentry from '@sentry/electron';
import {app, BrowserWindow, shell, ipcMain, dialog, Tray} from "electron";
import {default as config} from "./config.runtime";
import {default as errorHandler} from "./nimbus-electron/utilities/errorHandler";
import {default as appWindowInstance} from "./nimbus-electron/window/instance";
import {default as selectedItem} from "./nimbus-electron/window/selectedItem";
import {default as localCache} from "./nimbus-electron/sync/cache/LocalCache";
import AttachmentHandler, {default as attachmentHandler} from "./nimbus-electron/utilities/attachmentHandler";
import {default as trans} from "./nimbus-electron/translations/Translations";

import {default as PouchDb, default as pdb} from "./pdb";
import {default as socketServer} from "./nimbus-electron/sync/socket/server";
import {default as windowInfo} from "./nimbus-electron/window/state";
import {default as menu} from "./nimbus-electron/menu/menu";
import {default as appOnlineState} from "./nimbus-electron/online/state";
import {default as appWindowPrint} from "./nimbus-electron/print/print";
import {default as childWindow} from "./nimbus-electron/window/child";
import {default as purchaseProducts} from "./nimbus-electron/online/products";

import {default as App} from "./nimbus-electron/sync/process/application/App";
import {default as NimbusSDK} from "./nimbus-electron/sync/nimbussdk/net/NimbusSDK";
import {default as appPage} from "./nimbus-electron/request/page";

import {default as SyncManager} from "./nimbus-electron/sync/process/SyncManager";
import {default as AutoSyncManager} from "./nimbus-electron/sync/nimbussdk/manager/AutoSyncManager";
import {default as AttachmentSingleDownloader} from "./nimbus-electron/sync/downlaoder/AttachmentSingleDownloader"

import {default as urlParser} from "./nimbus-electron/utilities/urlParser";
import {default as workspace} from "./nimbus-electron/db/models/workspace";
import {default as attach} from "./nimbus-electron/db/models/attach";
import {default as settingsModel} from "./nimbus-electron/db/models/settings";
import {default as orgs} from "./nimbus-electron/db/models/orgs";
import {default as apiAttachExist} from "./nimbus-electron/request/interceptors/jsonInterceptors/attach/apiAttachExist";

import {default as loginForm} from "./nimbus-electron/request/auth/login";
// import {default as registerForm} from "./nimbus-electron/request/auth/register";
import {default as logoutForm} from "./nimbus-electron/request/auth/logout";

import {default as updateUserUsage} from "./nimbus-electron/sync/process/rx/handlers/updateUserUsage";
import {default as PopupRateApp} from "./nimbus-electron/popup/PopupRateApp";
import {default as SelectedWorkspace} from "./nimbus-electron/workspace/SelectedWorkspace";
import {default as TextEditor} from "./nimbus-electron/db/TextEditor";

import {
  initHandlers,
  getUpdateInfo,
  closeUpdateWindow,
  confirmUpdateWindow
} from "./nimbus-electron/utilities/autoUpdater";
import {
  openEditorHelpLink,
  openHelpLink,
  openSubmitBugLink,
} from "./nimbus-electron/utilities/helpHandler";
import SelectedOrganization from "./nimbus-electron/organization/SelectedOrganization";
import {
  getNavigationUrlDetails,
  openLink,
  requestInternalNoteNavigation
} from "./nimbus-electron/utilities/navigationHandler";
import GoogleAuth from "./nimbus-electron/request/auth/oauth/google";
import FacebookAuth from "./nimbus-electron/request/auth/oauth/facebook";
import {openBusinessAuthLink} from "./nimbus-electron/utilities/authHandler";
import {exportCancel, exportHandler, exportOpenPath} from "./nimbus-electron/utilities/exportHandler";
import filenamify from "filenamify";

let forceQuit = false;
let tray = null;

let gotTheLock = true;
if (process.platform !== 'darwin') {
  gotTheLock = app.requestSingleInstanceLock();
}

if (!gotTheLock) {
  app.quit();
} else {
  app.on('second-instance', () => {
    if (appWindowInstance.get()) {
      if (appWindowInstance.get()) {
        if(appWindowInstance.get().isMinimized()) {
          appWindowInstance.get().restore();
          if (!appWindowInstance.get().isVisible()) {
            appWindowInstance.get().show();
          }
          return;
        }

        if (!appWindowInstance.get().isVisible()) {
          appWindowInstance.get().show();
        }
      }
    }
  });

  try {
    Sentry.init({dsn: config.SENTRY_KEY});
  } catch (e) {
    console.log(`Could not init Sentry`);
  }

  if(process.platform !== 'darwin') {
    app.setAppUserModelId(config.APPBUNDLE_ID);
  }

  /**
   * @param {Event} event
   */
  const showDialogOnWindowError = (event) => {
    const options = {
      type: 'info',
      title: `${trans.get('popup_crash__title')}`,
      message: `${trans.get('popup_crash__message')}`,
      defaultId: 0,
      cancelId: 1,
      buttons: [
        `${trans.get('popup_crash__refresh_app')}`,
        `${trans.get('popup_crash__close_app')}`
      ]
    };

    dialog.showMessageBox(options).then(({response}) => {
      if (response === 0) {
        appPage.reload();
      } else {
        forceQuit = true;
        app.quit();
      }
    });
  };

  if (!fs.existsSync(config.clientDataPath)) {
    fs.mkdirSync(config.clientDataPath);
  }

  if (!fs.existsSync(config.dataBasePath)) {
    fs.mkdirSync(config.dataBasePath);
  }

  if (process.env.NODE_ENV === "development") {
    require('electron-debug')({
      enabled: true,
      showDevTools: true
    });
  }

  if (!fs.existsSync(config.nimbusAngularWebFolderPath)) {
    if (config.SHOW_WEB_CONSOLE) {
      console.error("Folder webnotes doesn't exist");
    }
  }

  let server = require("./express-server/bin/www");

  const initAutoUpdater = () => {
    const {initHandlers} = require('./nimbus-electron/utilities/autoUpdater');
    initHandlers();
  };

  const initAutoStart = async () => {
    const needSetAutoLoad = await settingsModel.needSetAutoLoad();
    if(needSetAutoLoad) {
      const AutoLaunch = require('auto-launch');
      const autoLauncher = new AutoLaunch({name: 'Nimbus Note', isHidden: true});
      autoLauncher.isEnabled()
        .then(function(isEnabled){
          if(isEnabled){
            return;
          }
          autoLauncher.enable();
        })
        .catch(function(error){
          if (config.SHOW_WEB_CONSOLE) {
            errorHandler.log(`AutoLauncher err: ${error == null ? "unknown" : (error.stack || error).toString()}`);
          }
        });
    }
  };

  const initCustomProtocol = () => {
    if (process.defaultApp) {
      if (process.argv.length >= 2) {
        app.setAsDefaultProtocolClient('nimbus-note', process.execPath, [path.resolve(process.argv[1])]);
      }
    } else {
      app.setAsDefaultProtocolClient('nimbus-note');
    }
  };

  app.on('ready', async () => {
    if (process.platform !== 'darwin' && !config.IS_DEV_MODE) {
      initAutoUpdater();
      await initAutoStart();
    }
    initCustomProtocol();

    if (server) {
      socketServer.start(server);
    }

    await initAppWindow(createAppWindow);
  });

  const openNoteUrlHandler = (event, arg) => {
    const {noteUrl} = arg;
    if (noteUrl) {
      if (appWindowInstance.get()) {
        appWindowInstance.get().loadURL(noteUrl);
        appWindowInstance.get().show();
      }
    }
  };

  app.on('open-url', (event, url) => {
    const webNotesHost = config.WEBNOTES_BASE_URL;
    if (url.indexOf(`${webNotesHost}/ws/`) === 0) {
      const noteUrl = url.replace(webNotesHost, config.LOCAL_SERVER_HTTP_HOST);
      openNoteUrlHandler(event, {noteUrl});
    }
  });

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin')
      app.quit();
  });

// @ts-ignore
  app.on('activate', async () => {
    await initAppWindow(createAppWindow);
  });

// @ts-ignore
  pdb.prepareClientDbPath(async () => {
    await App.initSyncState();
  });

  /**
   * @param windowState Object
   */
  const createAppWindow = async (windowState) => {
    let spellCheck = await settingsModel.hasSpellChecker()
    let appWindow = new BrowserWindow({
      x: (windowState.bounds ? windowState.bounds.x : undefined),
      y: (windowState.bounds ? windowState.bounds.y : undefined),
      width: (windowState.bounds ? windowState.bounds.width : windowInfo.defaultWidth()),
      height: (windowState.bounds ? windowState.bounds.height : windowInfo.defaultHeight()),
      show: false,
      center: !!windowState.bounds,
      title: config.APP_NAME,
      icon: config.applicationIconPath,
      webPreferences: {
        nodeIntegration: true,
        allowRunningInsecureContent: false,
        plugins: false,
        preload: config.nimbusPreloadAssetsFile,
        spellcheck: !!spellCheck,
      },
    });
    if(process.platform !== 'darwin') {
      appWindow.webContents.session.setSpellCheckerLanguages(['en-US', 'ru'])
    }
    appWindowInstance.set(appWindow);

    if(process.platform !== 'darwin') {
      tray = new Tray(`${config.applicationIconPath}`);
      tray.setToolTip('Nimbus Note');
      tray.on('click', () => {
        if (appWindowInstance.get()) {
          if(appWindowInstance.get().isMinimized()) {
            appWindowInstance.get().restore();
            if (!appWindowInstance.get().isVisible()) {
              appWindowInstance.get().show();
            }
            return;
          }

          appWindowInstance.get().show();
        }
      });

      tray.setContextMenu(menu.getTrayMenu());
    }

    windowInfo.listenChangeInfo();
    appOnlineState.listenChangeStatus();
    appWindowPrint.listenPrint();

    logoutForm.listenRequest();

    appWindow.webContents.on('crashed', showDialogOnWindowError);
    appWindow.webContents.on('unresponsive', showDialogOnWindowError);

    appWindow.webContents.on('new-window', async (event, url) => {
      event.preventDefault();
      if (url.indexOf(config.nimbusAttachmentUrl) >= 0) {
        return (async () => {
          let nimbusAttachmentExist = await apiAttachExist.interceptRequestAsync({url: url});
          if (nimbusAttachmentExist) {
            return appWindow.loadURL(url);
          } else {
            AttachmentHandler.displayAttachSyncInProgressNotification();
          }
        })();
      } else if (process.platform === 'darwin' && url.indexOf(config.nimbusPaymentExternalUrl) >= 0) {
        openPaymentWindow();
        return;
      }

      const {notePageHost, reloadPage} = await getNavigationUrlDetails(url);
      if (!notePageHost) {
        return shell.openExternal(url);
      }

      const localNavUrl = url.replace(notePageHost, config.LOCAL_SERVER_HTTP_HOST);
      if(!reloadPage) {
        return requestInternalNoteNavigation(localNavUrl);
      }

      await appPage.reload(false, localNavUrl);
    });

    function closePaymentWindow() {
      if (!appWindow || !childWindow.get(childWindow.TYPE_PURCHASE)) {
        return;
      }

      childWindow.get(childWindow.TYPE_PURCHASE).close();
    }

    function openPaymentWindow() {
      if (!appWindow || childWindow.get(childWindow.TYPE_PURCHASE)) {
        return;
      }

      const purchaseWindow = new BrowserWindow({
        modal: false,
        parent: appWindow,
        show: false,
        alwaysOnTop: true,
        backgroundColor: "#3B3B3B",
        width: 925,
        height: 790,
        minWidth: 710,
        maxHeight: 790,
        center: true,
        title: "Get Nimbus Pro",
        fullscreen: false,
        minimizable: false,
        webPreferences: {
          devTools: true,
          nodeIntegration: true,
          //allowDisplayingInsecureContent: false,
          allowRunningInsecureContent: false,
          plugins: false,
          preload: config.nimbusPreloadAssetsFile
        }
      });
      purchaseWindow.once('close', function () {
        childWindow.set(childWindow.TYPE_PURCHASE, null);
      });
      childWindow.set(childWindow.TYPE_PURCHASE, purchaseWindow);

      if (childWindow.get(childWindow.TYPE_PURCHASE)) {
        childWindow.get(childWindow.TYPE_PURCHASE).webContents.on('new-window', (event, url) => {
          setTimeout(() => {
            childWindow.get(childWindow.TYPE_PURCHASE).setAlwaysOnTop(false);
          }, 1000);

          event.preventDefault();
          shell.openExternal(url);
        });

        childWindow.get(childWindow.TYPE_PURCHASE).once('ready-to-show', () => {
          appPage.loadNimbusPaymentAssets(childWindow.get(childWindow.TYPE_PURCHASE));
          setTimeout(function () {
            childWindow.get(childWindow.TYPE_PURCHASE).show();
          }, 250);
        });

        childWindow.get(childWindow.TYPE_PURCHASE).on('enter-full-screen', (event) => {
          event.preventDefault();
          childWindow.get(childWindow.TYPE_PURCHASE).maximize();
        });

        childWindow.get(childWindow.TYPE_PURCHASE).on('leave-full-screen', (event) => {
          event.preventDefault();
          childWindow.get(childWindow.TYPE_PURCHASE).unmaximize();
        });

        childWindow.get(childWindow.TYPE_PURCHASE).on('minimize', (event) => {
          event.preventDefault();
          appWindow.minimize();
        });

        childWindow.get(childWindow.TYPE_PURCHASE).once('closed', () => {
          childWindow.set(childWindow.TYPE_PURCHASE, null);
        });

        return childWindow.get(childWindow.TYPE_PURCHASE).loadURL(config.nimbusPaymentUrl);
      }
    }

    // @ts-ignore
    appWindow.webContents.on('will-navigate', async (event, url) => {
      if (url.indexOf('/client/recent') >= 0) {
        event.preventDefault();
      } else if (url.indexOf(config.nimbusAttachmentUrl) >= 0) {
        event.preventDefault();
        let routeParams = urlParser.getPathParams(url);
        let workspaceId = await SelectedWorkspace.getGlobalId();
        let globalId = typeof(routeParams[3] !== 'undefined') ? routeParams[3] : null;
        let attachmentPath = await attach.getAttachmentPath({workspaceId, globalId});
        if (attachmentPath && appWindowInstance.get()) {
          appWindowInstance.get().loadURL(url);
        }
      }
    });

    ipcMain.on('event:client:app:lang', (event) => {
      event.sender.send('event:client:app:lang');
    });

    ipcMain.on('event:client:reload:page:request', () => {
      appPage.reload();
    });

    ipcMain.on('event:client:error:log', (event, arg) => {
      errorHandler.log(arg);
    });

    ipcMain.on('event:open:note:url:handler', openNoteUrlHandler);

    ipcMain.on('event:play:video:request', (event, arg) => {
      if (arg && arg.html && arg.attach) {
        // @ts-ignore
        return (async () => {
          const {attach} = arg;
          const workspaceId = await SelectedWorkspace.getGlobalId();
          let nimbusAttachmentExist = await apiAttachExist.interceptRequestByGlobalIdAsync({
            workspaceId,
            globalId: attach.globalId
          });
          if (nimbusAttachmentExist) {
            let videoWindowInstance = <any>new BrowserWindow({
              modal: false,
              width: 640,
              height: 480,
              show: false,
              center: true,
              parent: appWindow,
              icon: `${config.applicationIconPath}`,
              backgroundColor: '#000000',
              useContentSize: true,
              fullscreen: false,
              minimizable: false,
              webPreferences: {
                devTools: false
              }
            });
            videoWindowInstance.globalId = attach.globalId;
            videoWindowInstance.workspaceId = workspaceId;

            videoWindowInstance.on('enter-full-screen', (event) => {
              event.preventDefault();
              videoWindowInstance.maximize();
            });

            videoWindowInstance.on('leave-full-screen', (event) => {
              event.preventDefault();
              videoWindowInstance.unmaximize();
            });

            videoWindowInstance.on('minimize', (event) => {
              event.preventDefault();
              appWindow.minimize();
            });

            videoWindowInstance.on('close', (event) => {
              if (event && event.sender) {
                let globalId = event.sender.globalId;
                if (globalId) {
                  childWindow.set(childWindow.TYPE_ATTACH + globalId, null);
                }
              }
            });

            appWindowInstance.createAttachVideoWindow(videoWindowInstance, arg.html);
            childWindow.set(childWindow.TYPE_ATTACH + attach.globalId, videoWindowInstance);
          } else {
            AttachmentHandler.displayAttachSyncInProgressNotification();
          }
        })();
      }
    });

    ipcMain.on('event:editor:open:attach:request', (event, arg) => {
      if (!arg) { return }
      if(!arg.attach) { return }
      AttachmentHandler.playAudioAttachment(arg)
    });

    ipcMain.on('event:close:video:request', (event, arg) => {
      if (arg && arg.globalId) {
        const globalId = arg.globalId;
        const childWindowId = childWindow.TYPE_ATTACH + globalId;
        if (childWindow.get(childWindowId)) {
          childWindow.get(childWindowId).close();
        }
      }
    });

    ipcMain.on('event:close:payment:window:requests', () => {
      closePaymentWindow();
    });

    ipcMain.on('event:open:payment:window:request', () => {
      openPaymentWindow();
    });

    // @ts-ignore
    ipcMain.on('event:sync:start:request', async (event, arg) => {
      let {workspaceId, manualSync} = arg;
      workspaceId = await workspace.getLocalId(workspaceId);
      SyncManager.startSyncOnline({workspaceId, manualSync});
    });

    // @ts-ignore
    ipcMain.on('event:web:app:export:active:note:request', async (event, arg) => {
      await exportHandler(appWindow, arg);
    });

    // @ts-ignore
    ipcMain.on('event:web:app:export:active:open:request', async (event, arg) => {
      const { path } = arg;
      await exportOpenPath(path);
    });

    // @ts-ignore
    ipcMain.on('event:web:app:export:active:cancel:request', async (event, arg) => {
      const { globalId } = arg;
      await exportCancel(globalId);
    });

    // @ts-ignore
    ipcMain.on('event:sync:queue:request', async (event, arg) => {
      let {workspaceId} = arg;
      workspaceId = await workspace.getLocalId(workspaceId);
      SyncManager.addSyncQueue({workspaceId});
    });

    // @ts-ignore
    ipcMain.on('event:client:autosync:request', async (event, arg) => {
      let {workspaceId} = arg;
      if (typeof (workspaceId) === 'undefined') {
        workspaceId = await SelectedWorkspace.getGlobalId();
      } else {
        workspaceId = await workspace.getLocalId(workspaceId);
      }
      let autoSyncEnable = await AutoSyncManager.checkAutoSyncEnable({workspaceId});
      if (autoSyncEnable) {
        event.sender.send('event:client:autosync:response', {workspaceId});
      }
    });

    ipcMain.on('event:display:productsId:list:requests', (event) => {
      event.sender.send('event:display:productsId:list:requests', {
        "productsIdList": config.PURCHASE_PRODUCTS
      });
    });

    ipcMain.on('event:display:products:requests', (event, args) => {
      let {displayError} = args;
      if (typeof(displayError) === "undefined") {
        displayError = true;
      }
      event.sender.send('event:display:products:requests', {
        products: purchaseProducts.get(),
        displayError: displayError
      });
    });

    const uploadProductRecipeHandler = (event, data) => {
      let {url, productInfo, restore} = data;
      if (url) {
        // @ts-ignore
        NimbusSDK.getApi().productRecipeUpload(url, async (err, response) => {
          if (err) {
            errorHandler.log({
              err: err, response: response,
              description: "Purchase => uploadProductRecipeHandler => productRecipeUpload",
              data: data
            });
          }
          await updateUserUsage({useForSync: false});
          await orgs.syncUserOrganizationsWorkspaces();
          event.sender.send('event:upload:products:recipe:requests', {
            response: response,
            productInfo: productInfo,
            restore: restore
          });

        });
      } else {
        errorHandler.log({
          err: null, response: null,
          description: "Purchase => uploadProductRecipeHandler",
          data: data
        });
      }
    };

    ipcMain.on('event:upload:products:recipe:requests', uploadProductRecipeHandler);

    // @ts-ignore
    ipcMain.on('event:update:user:usage:requests', async () => {
      await updateUserUsage({useForSync: false});
    });

    ipcMain.on('event:purchase:products:dialog:requests', (event, args) => {
      let {dialogOptions, closeWindow} = args;
      closeWindow = closeWindow || false;

      if (typeof(dialogOptions.title) === "undefined") {
        dialogOptions.title = "Nimbus Pro";
      }

      if (typeof(dialogOptions.detail) === "undefined") {
        dialogOptions.detail = "In-App Purchase";
      }

      if (typeof(dialogOptions.callback) === "undefined") {
        dialogOptions.callback = () => {
        };
      }

      if (childWindow.get(childWindow.TYPE_PURCHASE) && dialogOptions) {
        if (closeWindow) {
          return dialog.showMessageBox(childWindow.get(childWindow.TYPE_PURCHASE), dialogOptions).then(() => {
            setTimeout(() => {
              childWindow.get(childWindow.TYPE_PURCHASE).close();
              dialogOptions.callback();
              appPage.reload();
            }, 250);
          });
        }

        dialog.showMessageBox(childWindow.get(childWindow.TYPE_PURCHASE), dialogOptions).then(() => {
          dialogOptions.callback();
        });
      }
    });

    ipcMain.on('event:latest:update:version:requests', (event) => {
      event.sender.send('event:latest:update:version:response', getUpdateInfo());
    });

    ipcMain.on('event:close:update:window:requests', (event) => {
      closeUpdateWindow();
    });

    ipcMain.on('event:make:update:window:requests', (event) => {
      confirmUpdateWindow();
    });

    ipcMain.on('event:client:clear:sync:data:request', () => {
      localCache.requestClear();
    });

    ipcMain.on('event:web:app:remove:attachments:request', async (event, args) => {
      await attachmentHandler.eraseList(args);
    });

    // @ts-ignore
    ipcMain.on('event:client:start-single-attach-sync:request', async (event, args) => {
      let {workspaceId, globalId} = args;
      if (!workspaceId) {
        workspaceId = await SelectedWorkspace.getGlobalId();
      }
      if (globalId) {
        await AttachmentSingleDownloader.download({workspaceId, globalId});
      }
    });

    // @ts-ignore
    ipcMain.on('event:client:stop-single-attach-sync:request', async (event, args) => {
      let {workspaceId, globalId} = args;
      if (globalId) {
        await AttachmentSingleDownloader.cancel({workspaceId, globalId});
      }
    });

    // @ts-ignore
    ipcMain.on('event:client:set:active:workspace:request', async (event, args) => {
      let {workspaceId} = args;
      if (typeof (workspaceId) !== 'undefined') {
        const prevWorkspaceId = await SelectedWorkspace.getGlobalId();
        await SelectedWorkspace.set(workspaceId);

        if (prevWorkspaceId === await SelectedWorkspace.getGlobalId()) {
          return;
        }

        await SyncManager.stopSync(prevWorkspaceId);
        event.sender.send('event:client:set:active:workspace:request', {
          workspaceId: await SelectedWorkspace.getGlobalId(),
          onlineStatus: appOnlineState.get()
        });
      }
    });

    ipcMain.on('event:client:set:active:organization:request', async (event, args) => {
      const {org: orgData} = args;
      const globalId = orgData ? orgData.globalId : null;
      const prevWorkspaceId = await SelectedWorkspace.getGlobalId();
      const reloadPage = await SelectedOrganization.set(globalId);
      if(!reloadPage) {
        return;
      }
      await SyncManager.stopSync(prevWorkspaceId);
      await appPage.reload(false);
    });

    ipcMain.on('event:client:print:attachment:request', async (event, args) => {
      let {attachment } = args;
      if (typeof (attachment) !== 'undefined' && typeof(attachment['file_uuid']) !== 'undefined') {
        let targetPath = `${PouchDb.getClientAttachmentPath()}/${attachment['file_uuid']}`;
        // @ts-ignore
        let exists = await fs.exists(targetPath);
        // @ts-ignore
        if(exists) {
          if (!appWindow) {
            return;
          }

          const win = new BrowserWindow({
            width: 800,
            height: 600,
            show: true,
            parent: appWindow,
          });
          win.webContents.on('dom-ready', () => {
            win.webContents.executeJavaScript(`
             window.PDFViewerApplication.eventBus.on('pagesloaded', () => {
              setTimeout(() => {
                window.PDFViewerApplication.eventBus.dispatch('print');
              }, 500);
             });
          `);
          });
          PDFWindow.addSupport(win);
          win.loadURL(`file://${targetPath}`);
        }
      }
    });

    ipcMain.on('event:oauth:request', async (event, args) => {
      let {provider} = args;
      if(provider === 'google') {
        GoogleAuth.auth();
      } else if (provider === 'facebook') {
        FacebookAuth.auth();
      }
    });

    ipcMain.on('event:open:business:registration:request', () => {
      openBusinessAuthLink();
    });

    ipcMain.on('event:client:open:url:request', (event, args) => {
      const {url} = args;
      openLink(url);
    });

    ipcMain.on('event:client:open:help:request', () => {
      openHelpLink();
    });

    ipcMain.on('event:client:open:editor:help:request', () => {
      openEditorHelpLink();
    });

    ipcMain.on('event:client:open:submit:bug:request', () => {
      openSubmitBugLink();
    });

    // @ts-ignore
    ipcMain.on('event:editor:note:update:request', async (event, args) => {
      TextEditor.updateNote(args);
    });

    // @ts-ignore
    ipcMain.on('event:editor:attachment:sync:download:request', async (event, args) => {
      TextEditor.downloadAttach(args);
    });

    // @ts-ignore
    ipcMain.on('event:editor:sync-text-callback:data:request', async (event, args) => {
      TextEditor.updateNoteText(args);
    });

    // @ts-ignore
    ipcMain.on('event:editor:put:db:data:request', async (event, args) => {
      const res = await TextEditor.put(args);
      event.sender.send('event:editor:put:db:data:response', res);
    });

    // @ts-ignore
    ipcMain.on('event:editor:put:db:array:data:request', async (event, args) => {
      const res = await TextEditor.putArray(args);
      event.sender.send('event:editor:put:db:array:data:response', res);
    });

    // @ts-ignore
    ipcMain.on('event:editor:get:db:data:request', async (event, args) => {
      const res = await TextEditor.getFromRange(args);
      event.sender.send('event:editor:get:db:data:response', res);
    });

    // @ts-ignore
    ipcMain.on('event:editor:getFromRange:db:data:request', async (event, args) => {
      const res = await TextEditor.getFromRange(args);
      event.sender.send('event:editor:getFromRange:db:data:response', res);
    });

    // @ts-ignore
    ipcMain.on('event:editor:getKeysFromRange:db:data:request', async (event, args) => {
      const res = await TextEditor.getKeysFromRange(args);
      event.sender.send('event:editor:getKeysFromRange:db:data:response', res);
    });

    // @ts-ignore
    ipcMain.on('event:editor:delete:db:data:request', async (event, args) => {
      const res = await TextEditor.delete(args);
      event.sender.send('event:editor:delete:db:data:response', res);
    });

    ipcMain.on('event:display:products:save:requests', (event, args) => {
      if (args.products && (args.products.length)) {
        purchaseProducts.set(args.products);
      }
    });

    // @ts-ignore
    ipcMain.on('event:select:note:request', async (event, args) => {
      let {workspaceId, globalId} = args;
      if (globalId) {
        selectedItem.set({
          workspaceId: await workspace.getLocalId(workspaceId),
          itemType: selectedItem.TYPE_NOTE,
          item: globalId
        });
      }
    });

    ipcMain.on('event:display:notification:requests', (event, args) => {
      event.sender.send('event:display:notification:requests', {
        data: args.data,
        id: args.id,
        icon: `${config.applicationIconPath}`,
        settings: args.settings,
        macNotification: process.platform === 'darwin'
      });
    });

    ipcMain.on('event:close:notification:requests', (event) => {
      event.sender.send('event:close:notification:requests', {});
    });

    appWindow.on('closed', () => {
      setTimeout(() => {
        if (server) {
          server.close();
        }
        appWindow = null;
      }, 1);
    });

    let appWindowAlreadyShown = false;

    const showAppWindow = () => {
      setTimeout(() => {
        PopupRateApp.check();
        if (!appWindowAlreadyShown) {
          appWindowAlreadyShown = true;
        }
      }, appWindowAlreadyShown ? 0 : 4000);
    }

    appWindow.on('show', showAppWindow);

    appWindow.once('ready-to-show', () => {
      if(process.platform !== 'darwin') {
        const showMinimized = process.argv.indexOf('--hidden') >= 0
        if(showMinimized) { return }
      }
      appWindow.show();
    });

    app.on('before-quit', () => {
      forceQuit = true;
    });

    if (process.platform === 'darwin') {
      app.on('activate', () => {
        if (appWindow) {
          if (!appWindow.isVisible()) {
            appWindow.show();
          }
        }
      });

      appWindow.on('close', (event) => {
        if (!forceQuit) {
          event.preventDefault();
          if (!appWindow.isFullScreen()) {
            return appWindow.hide();
          }

          appWindow.setFullScreen(false);
          setTimeout(() => {
            appWindow.hide();
          }, 750);
        } else {
          appWindowInstance.set(null);
        }
      });
    } else {
      appWindow.on('close', (event) => {
        if (!forceQuit) {
          event.preventDefault();
          appWindow.hide();
        } else {
          appWindowInstance.set(null);
        }
      });
    }

    menu.add();

    loginForm.tryLoginClient();
  };

  /**
   * @param callback Function
   */
  const initAppWindow = async (callback) => {
    let appWindow = appWindowInstance.get();

    if (!appWindow) {
      await trans.initLang();
      windowInfo.get(function (err, windowState) {
        callback(windowState);
      });
    }
  };

}