import chalk from "chalk";
import crc from "crc";
import http from "http";
import express, {Request} from "express";
import helmet from "helmet";
import engines from "consolidate";
import bodyParser from "body-parser";
import multer from "multer";
import {default as config} from "../config.runtime";
import {default as pdb} from "../pdb";
import {default as urlParser} from "../nimbus-electron/utilities/urlParser";
import {default as appWindow} from "../nimbus-electron/window/instance";
import {default as appOnlineState} from "../nimbus-electron/online/state";
import {default as appPage} from "../nimbus-electron/request/page";
import {default as jsonInterceptor} from "../nimbus-electron/request/interceptors/jsonInterceptor";
import {default as NimbusSDK} from "../nimbus-electron/sync/nimbussdk/net/NimbusSDK";
import {default as auth} from "../nimbus-electron/auth/auth";
import {default as item} from "../nimbus-electron/db/models/item";
import {default as workspace} from "../nimbus-electron/db/models/workspace";
import {default as orgs} from "../nimbus-electron/db/models/orgs";
import {default as trans} from "../nimbus-electron/translations/Translations";
import {default as ReminderNotice} from "../nimbus-electron/popup/ReminderNotice";
import {default as errorHandler} from "../nimbus-electron/utilities/errorHandler";
import {default as user} from "../nimbus-electron/db/models/user";
import SelectedOrganization from "../nimbus-electron/organization/SelectedOrganization";
import Login from "../nimbus-electron/request/auth/login";
import Registration from "../nimbus-electron/request/auth/register";
import Remind from "../nimbus-electron/request/auth/remind";
import GoogleAuth from "../nimbus-electron/request/auth/oauth/google";
import FacebookAuth from "../nimbus-electron/request/auth/oauth/facebook";
import Challenge from "../nimbus-electron/request/auth/challenge";
import syncHandler from "../nimbus-electron/utilities/syncHandler";

const ERROR_WRONG_REQUEST = -2;
const ERROR_USER_ALREADY_EXISTS = -4;
const ERROR_WRONG_LOGIN_OR_PASSWORD = -6;
const ERROR_USER_NOT_FOUND = -7;
const ERROR_INTERNAL_SERVER_ERROR = -10;

let app = express();
app.use(helmet());
app.use(bodyParser.json({limit: '8mb'}));
app.set('views', [
    config.nimbusAuthTemplatesFolderPath,
    config.nimbusAngularTemplatesFolderPath,
    config.nimbusCustomTemplatesFolderPath
]);
app.engine('html', engines.mustache);
app.set('view engine', 'mustache');

let upload = multer({dest: config.nimbusAttachmentPath});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(`/${config.NIMBUS_ANGULAR_PROJ}/web/static`, express.static(config.nimbusAngularFolderPath));
app.use(`/${config.NIMBUS_AUTH_PROJ}/web/auth`, express.static(config.nimbusAuthTemplatesFolderPath));
app.use(`/${config.NIMBUS_AUTH_PROJ}/web`, express.static(config.nimbusAuthWebFolderPath));
app.use(`/custom/static`, express.static(config.nimbusCustomStaticFolderPath));
app.use(`/static`, express.static(config.nimbusAngularStaticFolderPath));
app.use(`/vendor`, express.static(config.nimbusModulesPath));

let methods = {
    'GET': 'get',
    'POST': 'post',
    'PUT': 'put',
    'DELETE': 'delete'
};

let noteRequests = [
    {httpMethod: 'GET', url: '/v1/trial/proTrial', action: 'me/apiProTrial'},

    {httpMethod: 'GET', url: '/api/me', action: 'me/apiMe'},
    {httpMethod: 'POST', url: '/api/me/profile', action: 'me/apiMeNameUpdate'},
    {httpMethod: 'POST', url: '/api/me', action: 'me/apiMeUpdate'},

    {httpMethod: 'GET', url: '/v1/survey/profession*', action: 'me/apiUserProfession'},

    {httpMethod: 'GET', url: '/api/user-settings', action: 'me/apiUserSettings'},
    {httpMethod: 'POST', url: '/api/user-settings', action: 'me/apiUserSettingsUpdate'},

    {httpMethod: 'POST', url: '/v1/users/vars/*', action: 'users/apiUserVariableUpdate'},

    {httpMethod: 'GET', url: '/v1/users/*/vars/ignoreIntro', action: 'users/apiUserIgnoreIntro'},
    {httpMethod: 'GET', url: '/v1/users/*/vars/ignoreSharesInfo', action: 'users/apiUserIgnoreSharesInfo'},
    {httpMethod: 'GET', url: '/v1/users/*/vars/noteAppearance', action: 'users/apiUserNoteAppearance'},

    {httpMethod: 'GET', url: '/v1/otp/setup*', action: 'users/apiUserOtp'},
    {httpMethod: 'POST', url: '/v1/otp/setup', action: 'users/apiUserOtpUpdate'},
    {httpMethod: 'POST', url: '/v1/otp/issue', action: 'users/apiUserOtpIssue'},
    {httpMethod: 'DELETE', url: '/v1/otp/setup', action: 'otp/apiUserOtpRemove'},

    {httpMethod: 'GET', url: '/api/me/sync', action: 'me/apiSync'},
    {httpMethod: 'POST', url: '/api/me/sync', action: 'me/apiSyncUpdate'},

    {httpMethod: 'GET', url: '/api/app/settings', action: 'app/apiAppSettings'},
    {httpMethod: 'POST', url: '/api/app/settings', action: 'app/apiAppSettingsUpdate'},

    {httpMethod: 'GET', url: '/v1/workspaces/*/mentions/workspaceInfo', action: 'mention/apiMentionsWorkspace'},

    {httpMethod: 'GET', url: '/api/workspaces/*/info', action: 'me/apiWorkspaceInfoGet'},
    {httpMethod: 'GET', url: '/api/workspaces/*/usage', action: 'me/apiWorkspaceUsageGet'},
    {httpMethod: 'GET', url: '/api/workspaceEmails/*/', action: 'me/apiWorkspaceEmailsGet'},

    {httpMethod: 'POST', url: '/api/me/logout', action: 'me/apiMeLogout'},
    {httpMethod: 'DELETE', url: '/logout', action: 'me/apiMeLogout'},

    {httpMethod: 'POST', url: '/api/workspaces/*/notes/*/export*', action: 'export/apiItemExport'},

    {httpMethod: 'GET', url: '/v1/workspaces/*/previews*', action: 'preview/apiPreviewList'},
    {httpMethod: 'GET', url: '/v1/workspaces/*/previews*', action: 'preview/apiPreviewList'},
    {httpMethod: 'POST', url: '/v1/workspaces/*/previews/*', action: 'preview/apiPreviewUpdate'},
    {httpMethod: 'DELETE', url: '/v1/workspaces/*/previews/*', action: 'preview/apiPreviewRemove'},

    {httpMethod: 'GET', url: '/api/workspaces/*/notes/*/text', action: 'text/apiText'},
    {httpMethod: 'POST', url: '/api/workspaces/*/notes/*/text', action: 'text/apiTextUpdate'},

    {httpMethod: 'GET', url: '/api/workspaces/*/notes/*/tags', action: 'tag/apiNoteTagsList'},
    {httpMethod: 'POST', url: '/api/workspaces/*/notesTags', action: 'tag/apiNotesTagsUpdate'},
    {httpMethod: 'POST', url: '/api/workspaces/*/notes/*/tags/*', action: 'tag/apiNoteTagsUpdate'},
    {httpMethod: 'DELETE', url: '/api/workspaces/*/notes/*/tags/*', action: 'tag/apiNoteTagsRemove'},

    {httpMethod: 'GET', url: '/api/workspaces/*/notes/*/todos', action: 'todo/apiNoteTodoList'},
    {httpMethod: 'GET', url: '/api/workspaces/*/notes/*/todosOrder', action: 'todo/apiTodoOrder'},
    {httpMethod: 'POST', url: '/api/workspaces/*/notes/*/todosOrder', action: 'todo/apiTodoOrderUpdate'},

    {httpMethod: 'GET', url: '/api/workspaces/*/reminders', action: 'attach/apiNoteRemindersList'},
    {httpMethod: 'POST', url: '/api/workspaces/*/notes/*/reminder', action: 'attach/apiNoteReminderUpdate'},
    {httpMethod: 'DELETE', url: '/api/workspaces/*/notes/*/reminder', action: 'attach/apiNoteReminderRemove'},

    {httpMethod: 'POST', url: '/v1/workspaces/*/duplicateRequest', action: 'item/apiItemDuplicate'},

    {httpMethod: 'GET', url: '/api/workspaces/*/notes', action: 'item/apiItemList'},
    {httpMethod: 'GET', url: '/api/workspaces/*/notes*', action: 'item/apiItem'},
    {httpMethod: 'POST', url: '/api/workspaces/*/notesParentId', action: 'item/apiItemParentId'},
    {httpMethod: 'POST', url: '/api/workspaces/*/notes*', action: 'item/apiItemUpdate'},
    {httpMethod: 'DELETE', url: '/api/workspaces/*/notes*', action: 'item/apiItemRemove'},

    {httpMethod: 'GET', url: '/api/workspaces/*/texts', action: 'text/apiTextList'},
    {httpMethod: 'GET', url: '/api/workspaces/*/texts*', action: 'text/apiText'},
    {httpMethod: 'POST', url: '/api/workspaces/*/texts/*/tokens', action: 'text/apiTextTokenUpdate'},

    {httpMethod: 'GET', url: '/api/workspaces/*/tags', action: 'tag/apiTagList'},
    {httpMethod: 'GET', url: '/api/workspaces/*/tags*', action: 'tag/apiTagList'},

    {httpMethod: 'POST', url: '/api/workspaces/*/tags*', action: 'tag/apiTagUpdate'},
    {httpMethod: 'DELETE', url: '/api/workspaces/*/tags*', action: 'tag/apiTagRemove'},

    {httpMethod: 'GET', url: '/api/workspaces/*/todos', action: 'todo/apiTodoList'},
    {httpMethod: 'GET', url: '/api/workspaces/*/todos*', action: 'todo/apiTodo'},
    {httpMethod: 'POST', url: '/api/workspaces/*/todos*', action: 'todo/apiTodoUpdate'},
    {httpMethod: 'DELETE', url: '/api/workspaces/*/todos*', action: 'todo/apiTodoRemove'},

    {httpMethod: 'GET', url: '/api/workspaces/*/attachments', action: 'attach/apiAttachList'},
    {httpMethod: 'GET', url: '/api/workspaces/*/attachments*', action: 'attach/apiAttach'},
    {httpMethod: 'POST', url: '/api/workspaces/*/attachments*', action: 'attach/apiAttachUpdate'},
    {httpMethod: 'DELETE', url: '/api/workspaces/*/attachments*', action: 'attach/apiAttachRemove'},

    {httpMethod: 'GET', url: '/v1/workspaces/*/annotations/*', action: 'annotation/apiAnnotation'},
    {httpMethod: 'GET', url: '/v1/workspaces/*/annotations', action: 'annotation/apiAnnotations'},

    {httpMethod: 'GET', url: '/api/premium', action: 'premium/apiPremium'},
    {httpMethod: 'GET', url: '/v1/workspaces/*/premium', action: 'premium/apiWorkspacePremium'},
    {httpMethod: 'GET', url: '/ws/*/recent', action: 'client/apiClientRecent'},

    {httpMethod: 'GET', url: '/api/usages/limit-checker', action: 'limit/apiLimitChecker'},
    {httpMethod: 'GET', url: '/v1/organizations/*/limits', action: 'limit/apiOrganizationLimits'},

    {httpMethod: 'GET', url: '/v1/users/*', action: 'users/userFindById'},
    {httpMethod: 'GET', url: '/users', action: 'users/userFind'},

    {httpMethod: 'GET', url: '/api/invites', action: 'workspace/apiInvitesList'},
    {httpMethod: 'POST', url: '/api/workspaces/*/members', action: 'workspace/apiWorkspaceInviteAdd'},
    {httpMethod: 'POST', url: '/api/invitesById/*/send', action: 'workspace/apiWorkspaceInviteResend'},
    {httpMethod: 'POST', url: '/api/invitesById/*', action: 'workspace/apiWorkspaceInviteUpdate'},
    {httpMethod: 'DELETE', url: '/api/invitesById/*', action: 'workspace/apiWorkspaceInviteRemove'},

    {httpMethod: 'GET', url: '/v1/workspaces/*/emails*', action: 'workspace/apiWorkspaceEmail'},
    {httpMethod: 'POST', url: '/v1/workspaces/*/changeWorkspaceRequest', action: 'workspace/apiWorkspaceChange'},

    {httpMethod: 'GET', url: '/v1/workspaces/*/members', action: 'workspace/apiMembersList'},
    {httpMethod: 'POST', url: '/v1/members/*', action: 'workspace/apiWorkspaceMemberUpdate'},
    {httpMethod: 'POST', url: '/v1/workspaces/*/members/*', action: 'workspace/apiWorkspaceMemberUpdate'},
    {httpMethod: 'DELETE', url: '/v1/workspaces/*/members/*', action: 'workspace/apiWorkspaceMemberRemove'},

    {httpMethod: 'GET', url: '/v1/workspaces', action: 'workspace/apiWorkspacesList'},
    {httpMethod: 'POST', url: '/v1/workspaces', action: 'workspace/apiWorkspaceUpdate'},
    {httpMethod: 'POST', url: '/v1/workspaces/*', action: 'workspace/apiWorkspaceUpdate'},
    {httpMethod: 'DELETE', url: '/v1/workspaces/*', action: 'workspace/apiWorkspaceRemove'}
];

pdb.prepareClientDbPath();

app.get(config.nimbusAuthIndexFile, (req, res) => {
    return res.render('auth.html');
});

function getConfigUrl({ version, publicPath = '/' }) {
    return `${ publicPath }static/js/config.service.js?version=${ version }`
}

async function loadWebNotePage(req, res) {
    const startTime = Date.now();
    const authInfo = <{userId, email, variables}>await auth.fetchActualUserAsync();
    const userVariables = authInfo && authInfo.variables ? authInfo.variables : user.getDefaultVariables();
    if(userVariables && typeof userVariables['isPinnedLeftBlock'] === 'undefined') {
        userVariables['isPinnedLeftBlock'] = true;
    }

    const trial = await user.getTrialAsync(authInfo.userId);
    const profile = await user.getProfileAsync(authInfo.email);

    if (authInfo && Object.keys(authInfo).length) {
        if (pdb.getDbClientId() != authInfo.userId) {
            pdb.setDbClientId(authInfo.userId);
        }
    } else {
        return appPage.load(config.PAGE_AUTH_NAME);
    }

    const workspacesCount = await workspace.getUserWorkspacesCount();
    if (!workspacesCount) {
        await orgs.syncUserOrganizationsWorkspaces();
    }

    const workspacesList = <[]>await workspace.findUserWorkspaces();

    const {
        organizations,
        workspaces,
        workspacesAccess,
    } = await SelectedOrganization.getUserOrganizationsDetailsByWorkspaces(authInfo, workspacesList);

    const accountUserId = await user.getAccountUserId();
    let activeOrganization = await SelectedOrganization.getCurrentOrganization(authInfo);
    if (!activeOrganization) {
        activeOrganization = {globalId: null, userId: accountUserId};
    }

    if (workspaces.length) {
        ReminderNotice.init(workspaces);
    }

    const configUrl = getConfigUrl({
        version: config.WEBNOTES_VERSION,
        publicPath: '/',
    });

    // const { host } = headers
    // const subdomain = host.split('.')[0]
    // const subdomainInfo = OrganizationsController.getSubdomainInfo(orgClient, subdomain).catch(() => [])

    const hydratedState = {
        electronOrganization: activeOrganization,
        organizations,
        workspaces,
        workspacesAccessMap: workspacesAccess,
        userVariables,
        trial,
        profile,
        // subdomain: { favicon, logo, defaultLogo },
    }

    return res.render('index.html', {
        loadTime: Date.now() - startTime,
        version: config.WEBNOTES_VERSION,
        hydratedState: JSON.stringify(hydratedState),
        configUrl,
        favicon: '',
    });
}

app.get(`/${config.NIMBUS_ANGULAR_PROJ}/web/templates/index.html`, loadWebNotePage);
app.get('/client', loadWebNotePage);
app.get('/ws/*/recent', loadWebNotePage);
app.get('/ws/*/recent/note/*', loadWebNotePage);
app.get('/ws/*/folder/*/note/*', loadWebNotePage);
app.get('/ws/*/folder/*', loadWebNotePage);
app.get('/ws/*/tag/*', loadWebNotePage);
app.get('/ws/*/search*', loadWebNotePage);
app.get('/ws/*/favorites*', loadWebNotePage);
app.get('/ws/*/tag/*/note/*', loadWebNotePage);
app.get('/ws/*/settings*', () => {
    if (appWindow.get()) {
        appWindow.get().webContents.send('event:client:display:settings:response');
    }
});

app.get(`/${config.NIMBUS_ANGULAR_PROJ}${config.nimbusPaymentUrlPath}`, (req, res) => {
    return res.render('payment.html');
});

app.get(`/${config.NIMBUS_ANGULAR_PROJ}${config.nimbusMessageUrlPath}`, (req, res) => {
    return res.render('message.html');
});

app.get(`/${config.NIMBUS_ANGULAR_PROJ}${config.nimbusUpdateUrlPath}`, (req, res) => {
    return res.render('update.html');
});

app.get('/server-message/web', (req, res) => {
    return res.status(200).json({});
});

// @ts-ignore
app.get('/api/workspaces/*/note-todo', async (req, res) => {
    let noteGlobalId = req.query.noteGlobalId;

    if (!noteGlobalId) {
        return res.status(404).json({});
    }

    // @ts-ignore
    let getTodoList = async () => {
        // @ts-ignore
        return new Promise((resolve) => {
            let options = {
                host: config.LOCAL_SERVER_HOST,
                port: config.LOCAL_SERVER_PORT,
                path: '/api/notes/' + noteGlobalId + '/todos/'
            };

            http.get(options, (res) => {
                let body = '';
                res.on('data', (chunk) => {
                    body += chunk;
                });
                res.on('end', () => {
                    resolve(body ? JSON.parse(body) : []);
                });
            }).on('error', (e) => {
                resolve(null);
            });
        });
    };

    let getTodoOrder = async () => {
        // @ts-ignore
        return new Promise((resolve) => {
            let options = {
                host: config.LOCAL_SERVER_HOST,
                port: config.LOCAL_SERVER_PORT,
                path: '/api/notes/' + noteGlobalId + '/todoOrder/'
            };

            http.get(options, (res) => {
                let body = '';
                res.on('data', (chunk) => {
                    body += chunk;
                });
                res.on('end', () => {
                    resolve(body ? JSON.parse(body) : []);
                });
            }).on('error', (e) => {
                resolve(null);
            });
        });
    };

    /**
     * @type []
     */
    let todoList = <[any]>await getTodoList();
    let todoOrder = <[any]>await getTodoOrder();

    let unorderedTodoList = todoList.slice();
    let orderedTodoList = [];

    if (todoOrder.length) {
        todoOrder.forEach((todo) => {
            let found = false;

            unorderedTodoList = unorderedTodoList.filter((item) => {
                if (!found && item.globalId == todo) {
                    orderedTodoList.push(item);
                    found = true;

                    return false;
                } else {
                    return true;
                }
            })
        })
    }

    unorderedTodoList = unorderedTodoList.sort((todo1, todo2) => {
        return todo1.dateAdded - todo2.dateAdded;
    });

    orderedTodoList = orderedTodoList.concat(unorderedTodoList);

    return res.status(200).json(orderedTodoList);
});

// @ts-ignore
app.post('/api/workspaces/*/note-tags-batch', async (req, res) => {
    let routeParams = urlParser.getPathParams(req.url);
    let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

    let tagsToAdd = req.body.tagsToAdd;
    let tagsToDelete = req.body.tagsToDelete;
    let _response = {status: 200, message: {}}, i;

    /**
     * @param {{workspaceId:string, requestMethod:string, noteGlobalId:string, tag:string}} inputData
     */
        //@ts-ignore
    let processTag = async (inputData) => {
            //@ts-ignore
            return new Promise((resolve) => {
                let {workspaceId, requestMethod, noteGlobalId, tag} = inputData;

                tag = encodeURIComponent(tag);

                let options = {
                    host: config.LOCAL_SERVER_HOST,
                    port: config.LOCAL_SERVER_PORT,
                    method: requestMethod,
                    path: `/api/workspaces/${workspaceId}/notes/${noteGlobalId}/tags/${tag}`
                };

                let tagRequest = http.request(options, (res) => {
                    let statusCode = res.statusCode;
                    res.on('data', () => {
                        resolve({
                            status: statusCode ? statusCode : 500,
                            message: {}
                        });
                    });
                }).on('error', (err) => {
                    resolve({
                        status: 500,
                        message: err
                    });
                });
                tagRequest.end();
            });
        };

    for (i = 0; i < tagsToAdd.length; i++) {
        if (_response.status != 200) break;
        _response = <any>await processTag({
            workspaceId,
            requestMethod: 'POST',
            noteGlobalId: req.body.noteGlobalId,
            tag: tagsToAdd[i]
        });
    }

    for (i = 0; i < tagsToDelete.length; i++) {
        if (_response.status != 200) break;
        _response = <any>await processTag({
            workspaceId,
            requestMethod: 'DELETE',
            noteGlobalId: req.body.noteGlobalId,
            tag: tagsToDelete[i]
        });
    }

    return res.status(_response.status).json(_response.message);
});

app.get('/attachment/*/*/preview*', (req, res) => {
    const actionReq = { ...req, action: 'attach/apiAttachPreview' };
    processRequest(actionReq, res, fileRequest);
});

app.get('/file/avatar/*', (req, res) => {
    const actionReq = { ...req, action: 'attach/apiAvatarPreview' };
    processRequest(actionReq, res, fileRequest);
});

app.get('/attachment*', function (req, res) {
    const actionReq = { ...req, action: 'attach/apiAttachDownload' };
    processRequest(actionReq, res, fileRequest);
});

app.post('/api/me/avatar', upload.single('avatar'), function (req, res) {
    const actionReq = { ...req, action: 'me/apiMeAvatarUpdate' };
    processRequest(actionReq, res, httpRequest);
});

app.delete('/api/me/avatar', function (req, res) {
    const actionReq = { ...req, action: 'me/apiMeAvatarDelete' };
    processRequest(actionReq, res, httpRequest);
});

app.put('/api/workspaces/*/attachments*', upload.fields([{name: "attachment"}]), function (req, res) {
    const actionReq = { ...req, action: 'attach/apiAttachCreate' };
    processRequest(actionReq, res, httpRequest);
});

/**
 * @param {string} oldPassword
 * @param {string} newPassword
 * @param {Function} callback
 */
function apiChangePassword(oldPassword, newPassword, callback = (err, res) => {
}) {
    NimbusSDK.getApi().userChangePassword(oldPassword, newPassword, (err, response) => {
        callback(err, response);
    });
}

app.post('/api/me/changePassword', async (req, res) => {
    let defaultErrorResponse = {
        'name': 'WrongPassword',
        'field': 'password'
    };

    if (!req.body.oldPassword || !req.body.newPassword) {
        return res.status(400).json(defaultErrorResponse);
    }

    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;

    auth.getUser((err, authInfo) => {
        if (authInfo && Object.keys(authInfo).length) {
            apiChangePassword(oldPassword, newPassword, (err, response) => {
                if (err || !response) {
                    return res.status(400).json(defaultErrorResponse);
                }

                const actionReq = {
                  ...req,
                  action: 'me/apiMeChangePassword',
                  authInfo,
                };

                getRequestActionPath(actionReq)(actionReq, (err, data) => {
                    if (err || !data) {
                        return res.status(400).json(defaultErrorResponse);
                    }

                    return res.status(200).json(data);
                });
            });

        } else {
            return res.status(400).json(defaultErrorResponse);
        }
    });
});

//@ts-ignore
app.post('/api/workspaces/*/limit-checker', async (req, res) => {
    auth.getUser((err, authInfo) => {
        if (err) {
            return res.status(404).json({});
        }

        if (authInfo && Object.keys(authInfo).length) {
            const actionReq = {
              ...req,
              action: 'limit/apiLimitChecker',
              body: {
                  ...(req.body || {}),
                  subscribe: authInfo.subscribe || 0,
              },
            };

            getRequestActionPath(actionReq)(actionReq, (err, data) => {
                if (err || !data) {
                    return res.status(404).json({});
                }

                let responseStatus = 200;
                if (data && data.httpStatus) {
                    responseStatus = data.httpStatus;
                }

                try {
                    return res.status(responseStatus).json(data);
                } catch (e) {
                    errorHandler.log(e == null ? "unknown" : (e.stack || e).toString());
                }
            });
        }
    });
});

/**
 * @param {workspaceId:string, globalId:string, password:string|null} inputData
 * @param {Function} callback
 */
function apiShareCreate(inputData, callback = (err, res) => {
}) {
    const {workspaceId, globalId, password} = inputData;
    const globalIds = [globalId];
    NimbusSDK.getApi().shareNotes({workspaceId, globalIds, password}, callback);
}

/**
 * @param {{workspaceId:string, globalId:string}} inputData
 * @param {Function} callback
 */
function apiShareRemove(inputData, callback = (err, res) => {
}) {
    const {workspaceId, globalId} = inputData;
    const globalIds = [globalId];
    NimbusSDK.getApi().unshareNotes({workspaceId, globalIds}, (err, response) => {
        console.log('=======> err?', err)
        console.log('=======> response?', response)
        callback(err, response);
    });
}

/**
 * @param {{workspaceId:string, globalId:string}} inputData
 * @return {Promise}
 */
// @ts-ignore
async function checkShareItemIsOffline(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
        const {workspaceId, globalId} = inputData;
        item.find({globalId: globalId}, {workspaceId}, (err, itemInstance) => {
            if (itemInstance && itemInstance.offlineOnly) {
                return resolve(true);
            }

            resolve(false);
        });
    });
}

app.get('/api/workspaces/*/notes/*/share', async (req, res) => {
    let routeParams = urlParser.getPathParams(req.url);
    let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
    let globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

    if (!globalId) {
        return res.status(404).json({});
    }

    let shareItemIsOffline = await checkShareItemIsOffline({workspaceId, globalId});
    if (shareItemIsOffline) {
        if (appWindow.get()) {
            appWindow.get().webContents.send('event:client:workspace:message:response', {
                message: trans.get('toast__can_not_share__offline_item'),
                //type: 'danger'
            });
        }
        return res.status(504).json({
            name: "OfflineError",
            type: "share"
        });
    }

    auth.getUser((err, authInfo) => {
        if (authInfo && Object.keys(authInfo).length) {
            const actionReq = {
              ...req,
              action: 'share/apiShare',
              body: {
                  ...req.body,
                  globalId,
              }
            };

            getRequestActionPath(actionReq)(actionReq, (err, data) => {
                if (err || !data) {
                    return res.status(404).json({});
                }

                data = {
                    accessAllowed: false,
                    dateUpdated: data.dateUpdated,
                    id: data.shareId,
                    noteGlobalId: globalId,
                    passwordRequired: data.passwordRequired,
                    securityKey: data.securityKey,
                    userId: authInfo.userId,
                    shared_url: data.shared_url || '',
                };

                return res.status(200).json(data);
            });

        } else {
            return res.status(404).json({});
        }
    });
});

app.post('/api/workspaces/*/notes/*/share', async (req, res) => {
    let routeParams = urlParser.getPathParams(req.url);
    let password = req.body.password || null;
    let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
    let globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

    if (!globalId) {
        return res.status(404).json({});
    }

    if (!appOnlineState.get()) {
        if (appWindow.get()) {
            appWindow.get().webContents.send('event:client:workspace:message:response', {
                message: trans.get('toast__can_not_share__offline'),
                //type: 'danger'
            });
        }
        return res.status(504).json({
            name: "OfflineError",
            type: "share"
        });
    }

    let shareItemIsOffline = await checkShareItemIsOffline({workspaceId, globalId});
    if (shareItemIsOffline) {
        if (appWindow.get()) {
            appWindow.get().webContents.send('event:client:workspace:message:response', {
                message: trans.get('toast__can_not_share__offline_item'),
                //type: 'danger'
            });
        }
        return res.status(504).json({
            name: "OfflineError",
            type: "share"
        });
    }

    auth.getUser((err, authInfo) => {
        if (authInfo && Object.keys(authInfo).length) {
            apiShareCreate({workspaceId, globalId, password}, async (err, response) => {
                if (err) {
                    if (err === -19) {
                        if (appWindow.get()) {
                            appWindow.get().webContents.send('event:client:workspace:message:response', {
                                message: trans.get('toast__need_sync_before__share'),
                                //type: 'danger'
                            });
                        }
                    }
                    return res.status(404).json({
                        name: "OfflineError",
                        type: "share"
                    });
                }

                if (typeof (response[globalId]) === "undefined") {
                    return res.status(404).json({});
                }

                let shareParams = urlParser.getPathParams(response[globalId]);
                if (shareParams.length < 3) {
                    return res.status(404).json({});
                }

                let shareId = typeof (shareParams[4]) !== 'undefined' ? shareParams[4] : shareParams[2];
                let shareSecurityKey = typeof (shareParams[5]) !== 'undefined' ? shareParams[5] : shareParams[3];

                const actionReq = {
                    ...req,
                    action: 'share/apiShareUpdate',
                    body: {
                        ...req.body,
                        globalId,
                        shared: true,
                        passwordRequired: !!password,
                        shareId,
                        securityKey: shareSecurityKey,
                        shared_url: response[globalId],
                    },
                };

                getRequestActionPath(actionReq)(actionReq, (err, data) => {
                    if (err || !data) {
                        return res.status(404).json({});
                    }

                    data = {
                        accessAllowed: false,
                        dateUpdated: data.dateUpdated,
                        id: data.shareId,
                        noteGlobalId: globalId,
                        passwordRequired: data.passwordRequired,
                        securityKey: data.securityKey,
                        userId: authInfo.userId,
                        shared_url: data.shared_url,
                    };

                    return res.status(200).json(data);
                });
            });
        } else {
            return res.status(404).json({});
        }
    });
});

app.delete('/api/workspaces/*/notes/*/share', async (req, res) => {
    let routeParams = urlParser.getPathParams(req.url);
    let globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;
    let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

    if (!globalId) { return res.status(404).json({}); }

    if (!appOnlineState.get()) {
        if (appWindow.get()) {
            appWindow.get().webContents.send('event:client:workspace:message:response', {
                message: trans.get('toast__can_not_share__offline'),
                //type: 'danger'
            });
        }
        return res.status(504).json({
            name: "OfflineError",
            type: "share"
        });
    }

    let shareItemIsOffline = await checkShareItemIsOffline({workspaceId, globalId});
    if (shareItemIsOffline) {
        if (appWindow.get()) {
            appWindow.get().webContents.send('event:client:workspace:message:response', {
                message: trans.get('toast__can_not_share__offline_item'),
                //type: 'danger'
            });
        }
        return res.status(504).json({
            name: "OfflineError",
            type: "share"
        });
    }

    apiShareRemove({workspaceId, globalId}, (err, response) => {
        if (err) {
            return res.status(404).json({});
        }
            const actionReq = {
                action: 'share/apiShareUpdate',
                body: {
                    ...req.body,
                    globalId,
                    shared: false,
                    passwordRequired: false,
                    shareId: '',
                    securityKey: '',
                    shared_url: '',
                },
                ...req,
            };

            getRequestActionPath(actionReq)(actionReq, (err, data) => {
                /*if (err || !data) {
                    return res.status(404).json({});
                }*/
                return res.status(200).json({});
            });
    });
});

noteRequests.forEach((request) => {
    let requestMethod = methods[request.httpMethod];

    if (!requestMethod) {
        if (config.SHOW_WEB_CONSOLE) {
            console.error('Wrong method: ', request.httpMethod, ' for url ', request.url);
        }
        return;
    }

    if (!request.action) {
        if (config.SHOW_WEB_CONSOLE) {
            console.error('No action: ', request.httpMethod, ' for url ', request.url);
        }
        return;
    }

    app[requestMethod](request.url, (req, res) => {
        req.action = request.action;
        processRequest(req, res, httpRequest);
    });
});

function processRequest(req, res, callback) {
    auth.getUser((err, authInfo) => {
        if (authInfo && Object.keys(authInfo).length) {
            if (pdb.getDbClientId() != authInfo.userId) {
                pdb.setDbClientId(authInfo.userId);
            }

            req.authInfo = authInfo;
            return callback(req, res);
        } else {
            appPage.reload(false);
            return res.status(401).json({});
        }
    });
}

function fileRequest(req, res) {
    getRequestActionPath(req)(req, res);
}

function httpRequest(req, res) {
    getRequestActionPath(req)(req, (error, data) => {
        let responseStatus = 200;
        if (data && data.httpStatus) {
            responseStatus = data.httpStatus;
            delete data.httpStatus;
        }
        try {
            if (data && data.textResponse) {
                delete data.textResponse;
                res.status(responseStatus).send(data.text);
            } else {
                res.status(responseStatus).json(data);
            }
        } catch (e) {
            errorHandler.log(e == null ? "unknown" : (e.stack || e).toString());
        }
    });
}

/**
 * @param {Request} req
 */
function getRequestActionPath(req) {
    const actionPath = req.action ? req.action.split('/') : '';
    let action = actionPath && actionPath.length > 1 ? actionPath[actionPath.length - 1] : req.action;
    return jsonInterceptor[action];
}

// auth form

/**
 * @param {string} email
 * @param {string} password
 * @param {Function} callback
 */
function apiSignIn(email, password, callback = (err, res) => {
}) {
    NimbusSDK.getApi().signIn(email, password, (err, response) => {
        callback(err, response);

        if (!err) {
            NimbusSDK.getApi().userLogin((err, login) => {
                if (appWindow.get()) {
                    appWindow.get().webContents.send('event:client:login:response', {login: login});
                }
            });
        }
    });
}

app.get('/ouath/success/:provider', async (req, res) => {
    const { provider } = req.params;
    const { code } = req.query;
    let token = null;

    if(provider && code) {
        switch (provider) {
            case Login.AUTH_PROVIDER_GOOGLE: {
                token = await GoogleAuth.getIdToken(code);
                break;
            }
            case Login.AUTH_PROVIDER_FACEBOOK: {
                token = await FacebookAuth.getAccessToken(code);
                break;
            }
        }
    }

    syncHandler.sendLog(`=> provider: ${provider}`)
    syncHandler.sendLog(`=> token exist: ${!!token}`)

    const status = token ? 'Success' : 'Denied';
    if(token) {
        const authInfo = await NimbusSDK.getApi().oauth({provider, token});

        try {
            const { email, sessionId } = authInfo.body;

            syncHandler.sendLog(`=> email: ${email}`)
            syncHandler.sendLog(`=> sessionId exist: ${!!sessionId}`)

            Login.tryAuthClient(email, sessionId, provider);
        } catch (error) {
            errorHandler.log(error.message);
        }

    }
    res.send(`<html lang="en"><head><title>${status}</title></head><body></body></html>`);
});

app.post('/auth/api/challenge', async (req, res) => {
    if (config.SHOW_WEB_CONSOLE) { console.log('/api/challenge'); }

    const { login } = req.body
    Challenge.tryChallengeClient(req.body, (response) => {
        const { body } = response;
        if(body && body.sessionId) {
            Login.tryAuthClient(login, body.sessionId);
        }
        res.json({...response});
    })
});

app.post('/auth/api/auth', async (req, res) => {
    if (config.SHOW_WEB_CONSOLE) { console.log('/api/auth'); }

    Login.tryLoginClient(req.body, (response) => {
        res.json({...response});
    });
});

app.post('/auth/api/register', async (req, res) => {
    if (config.SHOW_WEB_CONSOLE) { console.log('/api/register'); }

    Registration.tryRegisterClient(req.body, (response) => {
        res.json({...response});
    });
});

app.post('/auth/api/remind', async (req, res) => {
    if (config.SHOW_WEB_CONSOLE) { console.log('/api/remind'); }

    Remind.tryRemindPassword(req.body, (response) => {
        res.json({...response});
    });
});

module.exports = app;