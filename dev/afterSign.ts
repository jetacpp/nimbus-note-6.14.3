import fs from 'fs-extra';
import path from 'path';
import { notarize } from 'electron-notarize';
import envFilePath from './config.env';

exports.default = async function notarizing(params) {
  if (process.platform !== 'darwin') {
    return;
  }

  /*if(envFilePath.env === envFilePath.ENV_DEV) {
    console.log('afterSign skip notarize for dev build');
    return;
  }*/

  console.log('afterSign hook triggered');

  const appId = 'co.nimbusweb.nimbusnoteapp';
  const appPath = path.join(params.appOutDir, `${params.packager.appInfo.productFilename}.app`);
  if (!fs.existsSync(appPath)) {
    throw new Error(`Cannot find application at: ${appPath}`);
  }

  console.log(`Notarizing ${appId} found at ${appPath}`);

  try {
    const appleId = process.env.appleId;
    const appleIdPassword = process.env.appleIdPassword;
    const ascProvider = 'W92BW86NS4';
    await notarize({
      appBundleId: appId,
      appPath,
      appleId,
      appleIdPassword,
      ascProvider
    });
  } catch (error) {
    console.error(error);
  }

  console.log(`Done notarizing ${appId}`);
};
