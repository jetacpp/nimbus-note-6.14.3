export = 0;

import trans from "../../../../../nimbus-electron/translations/translationsData";

(function (window) {

  console.log("custom.js script load for communicate with nimbus-note-update project");

  const $ = (window as any).$;

  let lang = 'en';
  let trans = {};

  trans["update_available__title"] = {
    'en': `Found Updates`,
    'ru': `Найдено обновление`,
    'de': `Gefundene Updates`
  };

  trans["update_available__message"] = {
    'en': `Found updates, do you want update now?`,
    'ru': ` Доступно обновление, хотите обновить сейчас?`,
    'de': `Gefundene Updates, möchtest du jetzt ein Update?`
  };

  trans["update_available__progress"] = {
    'en': `Download progress`,
    'ru': `Прогресс загрузки`,
    'de': `Voortgang downloaden`
  };

  trans["update_available__yes"] = {
    'en': `Install new version`,
    'ru': `Установить новую версию`,
    'de': `Installeer een nieuwe versie`
  };

  trans["update_available__no"] = {
    'en': `No`,
    'ru': `Нет`,
    'de': `Nein`
  };

  trans["update_available__later"] = {
    'en': `Remind later`,
    'ru': `Напомнить позже`,
    'de': `Herinner later`
  };

  trans["update_available__hide"] = {
    'en': `Hide`,
    'ru': `Скрыть`,
    'de': `Verbergen`
  };

  trans["update_available__skip_version"] = {
    'en': `Skip new version`,
    'ru': `Пропустить новую версию`,
    'de': `Sla nieuwe versie over`
  };

  trans["update_in_progress"] = {
    'en': `Download in-progress...`,
    'ru': `Загрузка приложения...`,
    'de': `Download läuft...`
  };

  trans["update_in_progress__message"] = {
    'en': `Downloading new application version `,
    'ru': `Загрузка новой версии приложения `,
    'de': `Nieuwe toepassingsversie downloaden `
  };

  trans["update_description__message"] = {
    'en': `Available application version `,
    'ru': `Доступна версия приложения `,
    'de': `Beschikbare applicatieversie `
  };

  /**
   * @param {string} transKey
   * @returns {string}
   */
  let getTranslationByKey = (transKey) => {
    if(typeof (trans[transKey]) === "undefined") {
      return transKey;
    }

    if(typeof (trans[transKey][lang]) === "undefined") {
      return transKey;
    }

    return trans[transKey][lang] ? trans[transKey][lang] : transKey;
  };

  (window as any).elGetIpcRequest('event:initApp:request', (content) => {
    lang = content.language;
    $("[trans]").each(function () {
      const $self = $(this);
      const transKey = $self.attr("trans");
      $self.html(getTranslationByKey(transKey));
    });
  });

  (window as any).elGetIpcRequest('event:latest:update:version:response', (content) => {
    const version = content ? content.version : '';
    $('.update_available_version').text(version);
  });

  (window as any).elGetIpcRequest('event:latest:update:progress:response', (content) => {
    const progress = content ? content.progress : '';
    $('#update_available__progress').text(progress);
  });

  let closeWindowClickHandler = () => {
    (window as any).elSendIpcRequest('event:close:update:window:requests', {});
  };

  let updateWindowClickHandler = () => {
    const className = `#nimbus_update__title_found, #nimbus_update__description, #nimbus_update__description, 
                       .nimbus_update__button_later, .nimbus_update__button_confirm`;
    $(className).hide(0, () => {
      const $buttons = $('.nimbus_update__buttons');
      if(!$buttons.hasClass('nimbus_update__buttons_center')) {
        $buttons.addClass('nimbus_update__buttons_center');
      }
      $('#nimbus_update__title_download, #nimbus_update__progress, .nimbus_update__button_hide').show(0);
    });
    (window as any).elSendIpcRequest('event:make:update:window:requests', {});
  };

  let listenUpdateNoClick = () => {
    const events = 'click';
    const closePopupClass = '#nimbus_update__buttons__hide, #nimbus_update__buttons__no';
    $(document).off(events, closePopupClass, closeWindowClickHandler)
      .on(events, closePopupClass, closeWindowClickHandler);
  };

  let listenUpdateInstallClick = () => {
    const events = 'click';
    const closePopupClass = '#nimbus_update__buttons__yes';
    $(document).off(events, closePopupClass, updateWindowClickHandler)
      .on(events, closePopupClass, updateWindowClickHandler);
  };

  listenUpdateNoClick();
  listenUpdateInstallClick();
  (window as any).elSendIpcRequest('event:latest:update:version:requests', {});
  $('#nimbus_update__buttons__yes').focus();
})(window);
