(function (window) {

  console.log("custom.js script load for communicate with nimbus-note-auth project");

  function elInitContent() {
    elChangeOnlineStatusHandlers();
  }

  function elChangeOnlineStatusHandlers() {
    elProcessChangeOnlineStatus();

    (window as any).removeEventListener('online', elProcessChangeOnlineStatus);
    (window as any).addEventListener('online', elProcessChangeOnlineStatus);

    (window as any).removeEventListener('offline', elProcessChangeOnlineStatus);
    (window as any).addEventListener('offline', elProcessChangeOnlineStatus);
  }

  function elProcessChangeOnlineStatus() {
    let onlineStatus = (window as any).elAppIsOnline();
    (window as any).elSendIpcRequest('event:online:changed', {"online": onlineStatus});
  }

  (window as any).elGetIpcRequest('event:logout:request', () => {
    elProcessClientLogout();
  });

  function elProcessClientLogout() {
    (window as any).elSendIpcRequest('event:logout:response', {"logout": true});
  }

  elInitContent();

})(window);
