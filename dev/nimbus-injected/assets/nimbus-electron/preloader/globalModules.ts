console.log("Init Injected global");

const loadNimbusGlobalModules = () => {
  if ((window as any)) {

    if (!(window as any).$) {
      (window as any).nodeRequire = require;
      delete (window as any).require;
      // delete (window as any).exports;
      // delete (window as any).module;

      (window as any).$ = (window as any).jQuery = (window as any).nodeRequire('jquery');
    }
    if (!(window as any).EventEmitter) {
      (window as any).EventEmitter = (window as any).nodeRequire('wolfy87-eventemitter');
    }

    if (!(window as any).async) {
      (window as any).async = (window as any).nodeRequire('async');
    }
  }
};

document.addEventListener("DOMNodeInserted", loadNimbusGlobalModules);
(window as any).nodeRequire = (window as any).nodeRequire || require;

const {ipcRenderer} = (window as any).nodeRequire('electron');
ipcRenderer.on('nimbus:electron:log', function (event, data) {
  console.log(data);
});

if (process.env.NODE_ENV === "development") {
  (window as any).__devtron = {require: require, process: process};
  const devtron = (window as any).nodeRequire('devtron');
  if (devtron) {
    devtron.install();
  }
}
