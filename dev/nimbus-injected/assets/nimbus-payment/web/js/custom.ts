(function (window) {

  console.log("custom.js script load for communicate with nimbus-note-payment project");

  const $ = (window as any).$;
  const {inAppPurchase} = (window as any).nodeRequire('electron').remote;
  //const Notification = nodeRequire('node-mac-notifier');

  const MESSAGE_PAGE_KEY_PAYMENT = `nimbus-purchase`;

  const ERROR_PAYMENT_FAILED = -1;
  const ERROR_SUBSCRIPTION_NOT_UNIQUE = -2;

  let toastMessage;
  let products;

  let lang = 'en';
  let trans = {};
  trans["get_nimbus_pro"] = {
    'en': 'Get Nimbus Pro',
    'ru': 'Получить Nimbus Pro',
    'de': 'Holen Sie sich Nimbus Pro'
  };

  trans["more_monthly_uploads"] = {
    'en': 'More monthly uploads',
    'ru': 'Увеличенный лимит ежемесячных загрузок',
    'de': 'Mehr monatliche uploads'
  };

  trans["send_and_synchronize"] = {
    'en': 'Send and synchronize up to 5 GB of notes to Nimbus Note every month',
    'ru': 'Отправлять и синхронизовывать заметки до 5 ГБ в Nimbus Note каждый месяц',
    'de': 'Senden und synchronisieren Sie jeden Monat bis zu 5 GB Notizen an Nimbus Note'
  };

  trans["large_attachments"] = {
    'en': 'Large attachments',
    'ru': 'Загрузка больших файлов',
    'de': 'Große Anhänge'
  };

  trans["currently_upload_files"] = {
    'en': 'Currently you can upload files up to 10 MB',
    'ru': 'В настоящее время вы можете загружать файлы размером до 10 МБ',
    'de': 'Derzeit können Sie Dateien bis zu 10 MB hochladen'
  };

  trans["you_can_attach_files"] = {
    'en': 'With Nimbus Pro, you can attach files up to 1 GB!',
    'ru': 'С Nimbus Pro вы можете прикрепить файлы размером до 1 ГБ!',
    'de': 'Mit Nimbus Pro können Sie Dateien bis zu 1 GB anhängen!'
  };

  trans["nimbus_capture_benefits"] = {
    'en': 'Nimbus Capture benefits',
    'ru': 'Преимущества Nimbus Capture',
    'de': 'Nimbus Capture-Vorteile'
  };

  trans["you_can_get_pro_for"] = {
    'en': 'You also get a PRO account for ',
    'ru': 'Вы также получаете учетную запись PRO ',
    'de': 'Sie erhalten auch einen PRO-Account '
  };

  trans["premium_support"] = {
    'en': 'Premium Support',
    'ru': 'Премиум-поддержка',
    'de': 'Premium-Unterstützung'
  };

  trans["get_quick_answers"] = {
    'en': 'Get quick answers to your questions with priority support from our help team',
    'ru': 'Получайте быстрые ответы на свои вопросы с приоритетной поддержкой нашей команды помощи',
    'de': 'Erhalten Sie schnelle Antworten auf Ihre Fragen mit Priorität Unterstützung von unserem Hilfe-Team'
  };

  trans["price_or"] = {
    'en': 'or',
    'ru': 'или',
    'de': 'oder'
  };

  trans["price_terms_and_privacy"] = {
    'en': `Subscriptions will be charged to your credit card through your iTunes account. Subscription automatically 
    renews unless auto-renew is turned off at least 24-hours before the end of the current period. Account will be 
    charged for renewal within 24-hours prior to the end of the current period, and identify the cost of the renewal. 
    You may manage your subscriptions in Account Settings after purchase. By continuing, you accept our 
    <a href="https://nimbusweb.me/terms-and-conditions.php" target="_blank">Terms of Service</a> and 
    <a href="http://nimbus.everhelper.me/privacy.php" target="_blank">Privacy Policy</a>`,
    'ru': `Сумма будет списана с кредитной карты, привязанной к учётной записи iTunes. Подписка обновляется 
    автоматически, если автообновление не выключено за 24 часа до окончания текущего периода. Счет будет выставлен 
    в течении 24 часов до окончания текущего периода и определит стоимость обновления. Вы можете изменить свою подписку 
    в настройках аккаунта после покупки. Продолжая, Вы принимаете 
    <a href="https://nimbusweb.me/terms-and-conditions.php" target="_blank">Условия соглашения</a> и 
    <a href="http://nimbus.everhelper.me/privacy.php" target="_blank">Политику конфиденциальности</a>`,
    'de': `Abonnements werden Ihrer Kreditkarte über Ihr iTunes-Konto belastet. Das Abonnement wird automatisch 
    verlängert, es sei denn, die automatische Verlängerung ist mindestens 24 Stunden vor dem Ende des aktuellen 
    Zeitraums deaktiviert. Das Konto wird für die Verlängerung innerhalb von 24 Stunden vor dem Ende des laufenden 
    Zeitraums in Rechnung gestellt und ermittelt die Kosten für die Verlängerung. Sie können Ihre Abonnements nach dem 
    Kauf in den Kontoeinstellungen verwalten. Wenn Sie fortfahren, akzeptieren Sie unsere 
    <a href="https://nimbusweb.me/terms-and-conditions.php" target="_blank">Nutzungsbedingungen</a> und 
    <a href="http://nimbus.everhelper.me/privacy.php" target="_blank">Datenschutzbestimmungen</a>`
  };

  trans["purchase_close"] = {
    'en': 'Close window',
    'ru': 'Закрыть окно',
    'de': 'Fenster schließen '
  };

  trans["discount_save"] = {
    'en': 'Save ',
    'ru': 'Экономия ',
    'de': 'Sparen '
  };

  trans["restore_purchase"] = {
    'en': 'Restore purchase',
    'ru': 'Восстановить покупку',
    'de': 'Kauf wiederherstellen'
  };

  trans["month"] = {
    'en': 'month',
    'ru': 'месяц',
    'de': 'monat'
  };

  trans["year"] = {
    'en': 'year',
    'ru': 'год',
    'de': 'jahr'
  };

  trans["title_in_app_purchase"] = {
    'en': 'Subscription purchase:',
    'ru': 'Покупка подписки:',
    'de': 'Abonnement kaufen:'
  };

  trans["products_list_unable_to_get"] = {
    'en': 'Unable to display purchase products, please try close then open payment page.',
    'ru': 'Невозможно отобразить продукты для покупки, попробуйте закрыть, а затем откройте страницу оплаты.',
    'de': 'Die Kaufprodukte können nicht angezeigt werden. Bitte versuchen Sie, die Zahlungsseite zu schließen.'
  };

  trans["user_not_allowed_to_make_in_app_purchase"] = {
    'en': 'Sorry, you are not allowed to make in-app purchase.',
    'ru': 'Извините, нет возможности делать покупки в приложении.',
    'de': 'Entschuldigung, Sie dürfen keinen In-App-Kauf tätigen.'
  };

  trans["purchase_of_product_is_not_valid"] = {
    'en': 'Trying to buy not valid product',
    'ru': 'Попытка купить недействительный продукт',
    'de': 'Versuch, ein ungültiges produkt zu kaufen'
  };

  trans["purchase_of_product_added_to_the_queue"] = {
    'en': 'The payment has been added to the payment queue',
    'ru': 'Платеж был добавлен в очередь оплаты',
    'de': 'Die Zahlung wurde zur Zahlungswarteschlange hinzugefügt'
  };

  trans["purchase_of_product_in_progress"] = {
    'en': 'Purchasing of product',
    'ru': 'Приобретение продукта',
    'de': 'Einkauf von Produkten'
  };

  trans["purchase_of_product_has_been_done"] = {
    'en': 'Congratulations, success purchase of product',
    'ru': 'Поздравляем с успешная покупкой продукта',
    'de': 'Herzlichen Glückwunsch, Erfolg Kauf des produkts'
  };

  trans["purchase_of_product_has_been_failed"] = {
    'en': 'Failed to purchase product',
    'ru': 'Не удалось приобрести продукт',
    'de': 'Fehler beim Kauf des produkts'
  };

  trans["purchase_of_product_has_been_restored"] = {
    'en': 'The purchase of product has been restored',
    'ru': 'Покупка продукта была восстановлена',
    'de': 'Der Kauf des produkts wurde wiederhergestellt'
  };

  trans["purchase_of_product_has_been_deferred"] = {
    'en': 'The purchase has been deferred for product',
    'ru': 'Покупка была отложена для продукта',
    'de': 'Der Kauf wurde für das produkt zurückgestellt'
  };

  trans["purchase_of_product_has_been_failed_could_not_verify"] = {
    'en': 'Purchase failed, could not verify purchase for product',
    'ru': 'Покупка завершилась неудачно, не удалось проверить покупку для продукта',
    'de': 'Der Kauf ist fehlgeschlagen. Der Kauf des Produkts konnte nicht bestätigt werden'
  };

  trans["purchase_of_product_has_been_failed_detect_duplication"] = {
    'en': 'Purchase failed, detect duplication for purchase of product',
    'ru': 'Ошибка покупки, обнаруженина повтороная покупка продукта',
    'de': 'Kauf fehlgeschlagen, Duplikation beim Kauf eines Produkts erkennen'
  };

  trans["purchase_of_product_has_been_failed_internal_problem"] = {
    'en': 'Purchase failed, can not finish purchase for product',
    'ru': 'Ошибка покупки, невозможно завершить покупку продукта',
    'de': 'Der Kauf ist fehlgeschlagen, der Kauf für das Produkt kann nicht abgeschlossen werden'
  };

  /**
   * @param {string} transKey
   * @returns {string}
   */
  let getTranslationByKey = (transKey) => {
    return typeof (trans[transKey][lang]) === "undefined" ? transKey : trans[transKey][lang];
  };

  (window as any).elGetIpcRequest('event:initApp:request', (content) => {
    lang = content.language;
    $("[trans]").each(function () {
      const $self = $(this);
      const transKey = $self.attr("trans");
      $self.html(getTranslationByKey(transKey));
    });
  });

  (window as any).elGetIpcRequest('event:upload:products:recipe:requests', (content) => {
    let translationKey = '';
    if (!content.productInfo) {
      translationKey = 'purchase_of_product_has_been_failed_internal_problem';
      console.log("Upload product recipe has a problems with productInfo: ", content.productInfo);
    }

    if (!content.response) {
      translationKey = 'purchase_of_product_has_been_failed_internal_problem';
      console.log("Upload product recipe has a problems with api response: ", content.response);
    }

    if (translationKey) {
      (window as any).elSendIpcRequest('event:client:error:log', {
        err: null, response: null,
        description: "Purchase problem => upload:products:recipe:requests => BadData",
        data: {
          content: content
        }
      });

      (window as any).elSendIpcRequest('event:purchase:products:dialog:requests', {
        dialogOptions: {
          type: "error",
          message: `${getTranslationByKey(translationKey)}: ${content.productInfo}.`
        }
      });
      (window as any).elSendIpcRequest('event:close:notification:requests', {});
      return;
    }

    const productInfo = content.productInfo;
    const response = content.response;

    if (response.errorCode) {
      (window as any).elSendIpcRequest('event:client:error:log', {
        err: null, response: null,
        description: "Purchase problem => upload:products:recipe:requests => BadData",
        data: {
          response: response,
          productInfo: productInfo
        }
      });

      translationKey = 'purchase_of_product_has_been_failed_internal_problem';
      switch (response.errorCode) {
        case ERROR_PAYMENT_FAILED: {
          translationKey = 'purchase_of_product_has_been_failed_could_not_verify';
          break;
        }
        case ERROR_SUBSCRIPTION_NOT_UNIQUE: {
          translationKey = 'purchase_of_product_has_been_failed_detect_duplication';
          break;
        }
      }

      (window as any).elSendIpcRequest('event:close:notification:requests', {});
      (window as any).elSendIpcRequest('event:purchase:products:dialog:requests', {
        dialogOptions: {
          type: "error",
          message: `${getTranslationByKey(translationKey)}: ${productInfo}.`
        }
      });

    } else {

      translationKey = 'purchase_of_product_has_been_done';
      if (content.restore) {
        translationKey = 'purchase_of_product_has_been_restored';
      }

      (window as any).elSendIpcRequest('event:purchase:products:dialog:requests', {
        dialogOptions: {
          type: "info",
          message: `${getTranslationByKey(translationKey)}: ${productInfo}.`
        },
        closeWindow: true
      });
    }
  });

  // delayed products update
  (window as any).elGetIpcRequest('event:display:productsId:list:requests', (content) => {
    const productsIdList = content.productsIdList;
    if (!Array.isArray(productsIdList) || productsIdList.length <= 0) {
      console.log('Unable to get the products id list: ', content);
    }
    if ((window as any).getPurchaiseProducts) {
      (window as any).getPurchaiseProducts(productsIdList, renderPurchaiseProducts);
    }
  });

  // display pre-loaded products
  (window as any).elGetIpcRequest('event:display:products:requests', (content) => {
    products = content.products;
    let displayError = content.displayError;
    if (!Array.isArray(products) || products.length <= 0) {
      if (displayError) {
        showMessagePopup(`${getTranslationByKey('products_list_unable_to_get')}`);
      }
    }
    renderPurchaiseProducts(products);
  });

  /**
   * @param {string} productIndentifier
   * @param {string} key
   * @returns {string}
   */
  let getProductInfoByIndentifier = (productIndentifier, key) => {
    if (!products) {
      (window as any).elSendIpcRequest('event:client:error:log', {
        err: null, response: null,
        description: "Purchase problem => getProductInfoByIndentifier",
        data: {
          productIndentifier: productIndentifier,
          key: key,
          products: products
        }
      });
      return '';
    }

    let product;
    for (let i = 0; i < products.length; i++) {
      if (products[i].productIdentifier === productIndentifier) {
        product = products[i];
        break;
      }
    }

    if (!product) {
      (window as any).elSendIpcRequest('event:client:error:log', {
        err: null, response: null,
        description: "Purchase problem => getProductInfoByIndentifier",
        data: {
          productIndentifier: productIndentifier,
          key: key,
          product: product
        }
      });
      return '';
    }

    return typeof (product[key]) !== 'undefined' ? product[key] : '';
  };

  /**
   * @param {[Product]} products
   */
  let renderPurchaiseProducts = (products) => {
    const $productItems = $('.nimbus_payment__purchase_items');
    const $productItemTemplate = $('#nimbus_payment_purchase_item_template .nimbus_payment__purchase_items__item');
    const $productOrTemplate = $('#nimbus_payment_purchase_or_template .nimbus_payment__purchase_items__or');

    products.sort(function (a, b) {
      const priceA = a.price;
      const priceB = b.price;

      let comparison = 0;
      if (priceA > priceB) {
        comparison = 1;
      } else if (priceA < priceB) {
        comparison = -1;
      }
      return comparison;
    });

    $productItems.html("");

    let lessPriceProductMonthPrice = 0;
    for (let index = 0; index < products.length; index++) {
      let product = products[index];
      let $productItem = $productItemTemplate.clone();
      $productItem.attr('data-product-indetifier', product.productIdentifier);
      let $productItemTitle = $productItem.find('.nimbus_payment__purchase_items__button');
      product.period = {'title': ``, 'monthCount': ``};
      product.discount = {'percentage': 0};

      if (product.productIdentifier === 'mac.nimbusnote.subs.monthly') {
        product.period.title = `/${getTranslationByKey('month')}`;
        product.period.monthCount = 1;
        product.discount.percentage = 0;
        lessPriceProductMonthPrice = product.price;
      } else if (product.productIdentifier === 'mac.nimbusnote.subs.yearly') {
        product.period.title = `/${getTranslationByKey('year')}`;
        product.period.monthCount = 12;
        product.discount.percentage = 0;
      }

      if (product.period.monthCount > 1 && lessPriceProductMonthPrice > 0) {
        let productFullPrice = product.period.monthCount * lessPriceProductMonthPrice;
        if (productFullPrice && product.price) {
          // @ts-ignore
          let discount = parseInt(((productFullPrice / product.price) - 1) * 100);
          if (discount > 0) {
            product.discount.percentage = discount;
          }
        }
      }

      if ($productItemTitle.length) {
        $productItemTitle.html(`${product.formattedPrice}${product.period.title}`);
      }

      let $productDiscount = $productItem.find('.nimbus_payment__purchase_items__discount');
      if (product.discount.percentage > 0) {
        let $productDiscountValue = $productItem.find('.nimbus_payment__purchase_items__discount_value');
        $productDiscountValue.html(`${product.discount.percentage}%`);
      } else {
        $productDiscount.remove();
      }

      $productItems.append($productItem);

      if (index !== (products.length - 1)) {
        let $productOr = $productOrTemplate.clone();
        $productItems.append($productOr);
      }
    }

    if (products.length) {
      renderRestorePurchaseButton();
    }
  };

  let renderRestorePurchaseButton = () => {
    let $purchaseBtn = $('.nimbus_payment__restore_block__button');
    if ($purchaseBtn.length) {
      $purchaseBtn.show(0);
    }
  };

  /**
   * @param {string} productIndetifier
   * @param {number} productQuantity
   */
  let purchaseProductClick = (productIndetifier, productQuantity = 1) => {
    const isProductValid = inAppPurchase.purchaseProduct(productIndetifier, productQuantity);
    if (!isProductValid) {
      (window as any).elSendIpcRequest('event:client:error:log', {
        err: null, response: isProductValid,
        description: "Purchase problem => inAppPurchase.purchaseProduct",
        data: {
          productIndetifier: productIndetifier,
          productQuantity: productQuantity
        }
      });
      return showMessagePopup(`${getTranslationByKey('purchase_of_product_is_not_valid')}: ${productIndetifier}.`, {type: (window as any).MESSAGE_TYPE_ERROR});
    }
  };

  let listenPaymentItemsClick = () => {
    const events = 'click touchend';
    const paymentItemClass = '.nimbus_payment__purchase_items__item';
    $(document).off(events, paymentItemClass, paymentItemClickHandler)
      .on(events, paymentItemClass, paymentItemClickHandler);
  };

  let paymentItemClickHandler = function (event) {
    event.preventDefault();
    const productIndetifier = $(this).attr('data-product-indetifier');
    if (productIndetifier) {
      purchaseProductClick(productIndetifier);
    }
  };

  let listenPaymentTransactionsUpdate = () => {
    inAppPurchase.off('transactions-updated', transactionUpdateHandler)
      .on('transactions-updated', transactionUpdateHandler);
  };

  let listenPaymentRestoreClick = () => {
    $(document).on('click', '.nimbus_payment__restore_block__button', () => {
      console.log('start restore purchase');
      purchaseProductClick('mac.nimbusnote.subs.monthly');
    });
  };

  let closeWindowClickHandler = () => {
    (window as any).elSendIpcRequest('event:close:payment:window:requests', {});
  };

  let listenWindowCloseClick = () => {
    const events = 'click';
    const closePopupClass = '.nimbus_payment__close > span';
    $(document).off(events, closePopupClass, closeWindowClickHandler)
      .on(events, closePopupClass, closeWindowClickHandler);
  };

  let transactionUpdateHandler = (event, transactions) => {
    if (!Array.isArray(transactions)) {
      return;
    }

    transactions.forEach((transaction) => {
      const payment = transaction.payment;
      const paymentTitle = getProductInfoByIndentifier(payment.productIdentifier, 'localizedTitle');
      const paymentPrice = getProductInfoByIndentifier(payment.productIdentifier, 'formattedPrice');
      const paymentPeriod = getProductInfoByIndentifier(payment.productIdentifier, 'period');
      const productInfo = `${paymentTitle} ${paymentPrice}${paymentPeriod.title}`;
      let receiptURL;

      console.log("payment transaction: ", transaction);

      switch (transaction.transactionState) {
        case 'purchasing':
          showMessagePopup(`${getTranslationByKey('purchase_of_product_in_progress')}: ${productInfo} ...`, {
            type: (window as any).MESSAGE_TYPE_INFO,
            hideAfter: false
          });
          break;
        case 'purchased':
          receiptURL = decodeURI(inAppPurchase.getReceiptURL());

          console.log(`productInfo: ${productInfo}`);
          console.log(`Purchased Product receipt URL: ${receiptURL}`);
          (window as any).elSendIpcRequest('event:upload:products:recipe:requests', {
            url: receiptURL,
            productInfo: productInfo,
            transaction: transaction
          });

          inAppPurchase.finishTransactionByDate(transaction.transactionDate);
          break;
        case 'failed':
          showMessagePopup(`${getTranslationByKey('purchase_of_product_has_been_failed')}: ${productInfo}.`, {
            type: (window as any).MESSAGE_TYPE_ERROR,
            hideAfter: 15000
          });

          (window as any).elSendIpcRequest('event:client:error:log', {
            productInfo: productInfo,
            transaction: transaction
          });
          (window as any).elSendIpcRequest('event:update:user:usage:requests', {
            productInfo: productInfo
          });

          inAppPurchase.finishTransactionByDate(transaction.transactionDate);
          break;
        case 'restored':
          receiptURL = decodeURI(inAppPurchase.getReceiptURL());

          console.log(`Restored Product receipt URL: ${receiptURL}`);
          (window as any).elSendIpcRequest('event:upload:products:recipe:requests', {
            url: receiptURL,
            productInfo: productInfo,
            restore: true
          });

          inAppPurchase.finishTransactionByDate(transaction.transactionDate);
          break;
        case 'deferred':
          showMessagePopup(`${getTranslationByKey('purchase_of_product_has_been_deferred')}: ${productInfo}.`, {
            type: (window as any).MESSAGE_TYPE_WARNING,
            hideAfter: 15000
          });

          (window as any).elSendIpcRequest('event:client:error:log', {
            productInfo: productInfo,
            transaction: transaction
          });
          break;
        default:
          break;
      }
    });
  };

  (window as any).elGetIpcRequest('event:display:notification:requests', (content) => {
    /*const {data, id, settings} = content;

    if (settings.notification) {
      new Notification(data.title, {
        body: data.text,
        id: id
      });
    }*/

    if (toastMessage) {
      toastMessage.reset();
    }

    toastMessage = $.toast((window as any).getToastMessageConfig(content));
  });

  (window as any).elGetIpcRequest('event:close:notification:requests', () => {
    if (toastMessage) {
      toastMessage.reset();
    }
  });

  /**
   * @param {{title, text}|string} data
   * @param {{}} settings
   */
  let showMessagePopup = (data, settings = {}) => {
    if (typeof (data) === 'string') {
      data = {text: data};
    }

    data.title = data.title || getTranslationByKey("title_in_app_purchase");
    (window as any).elSendIpcRequest('event:display:notification:requests', {
      data: data,
      id: MESSAGE_PAGE_KEY_PAYMENT,
      settings: settings
    });
    console.log("Notification data: ", data);
  };

  listenWindowCloseClick();
  if (!(window as any).canMakePayments()) {
    showMessagePopup(getTranslationByKey('user_not_allowed_to_make_in_app_purchase'), {type: (window as any).MESSAGE_TYPE_WARNING});
  } else {
    listenPaymentItemsClick();
    listenPaymentTransactionsUpdate();
    listenPaymentRestoreClick();
    (window as any).elSendIpcRequest('event:display:products:requests', {displayError: false});
    (window as any).elSendIpcRequest('event:display:productsId:list:requests', {});
  }
})(window);
