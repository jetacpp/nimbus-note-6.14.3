(function (window) {

  console.log("custom.js script load for communicate with webnotes project");

  const $ = (window as any).$;

  let toastMessage;

  /**
   * @param {{}} texts
   * @return {string}
   */
  function getTranslation(texts) {
    if (typeof (texts[getCustomerLanguage()]) !== 'undefined') {
      return texts[getCustomerLanguage()];
    } else if (typeof (texts['en']) !== 'undefined') {
      return texts['en'];
    }

    return '';
  }

  const LAST_SYNC_TITLE = {
    'en': 'Last sync: ',
    'ru': 'Последний синк: ',
    'de': 'Letzte sync: '
  };

  const SYNC_STATUS_AUTHORIZED = {
    code: "AUTHORIZED",
    text: {
      'en': 'Authenticate...',
      'ru': 'Идентификация...',
      'de': 'Authentifizieren...'
    }
  };
  const SYNC_DOWNLOADING_META_INFO = {
    code: "DOWNLOADING_META_INFO",
    text: {
      'en': 'Downloading meta info...',
      'ru': 'Загрузка мета-данных...',
      'de': 'Meta info herunterladen...'
    }
  };

  const SYNC_REMOVE_FOLDERS = {
    code: "REMOVE_FOLDERS",
    text: {
      'en': 'Remove folder...',
      'ru': 'Удаление папок...',
      'de': 'Entfernen sie ordner...'
    }
  };
  const SYNC_REMOVE_NOTES = {
    code: "REMOVE_NOTES",
    text: {
      'en': 'Remove notes...',
      'ru': 'Удаление заметок...',
      'de': 'Notizen entfernen'
    }
  };
  const SYNC_REMOVE_ATTACHMENTS = {
    code: "REMOVE_ATTACHMENTS",
    text: {
      'en': 'Remove attachments...',
      'ru': 'Удаление аттачей...',
      'de': 'Anhänge entfernen...'
    }
  };

  const SYNC_UPDATE_CONTENT = {
    code: "UPDATE_CONTENT",
    text: {
      'en': 'Updating content...',
      'ru': 'Обновление контента...',
      'de': 'Inhalt aktualisieren...'
    }
  };
  const SYNC_DOWNLOAD_NOTES = {
    code: "DOWNLOAD_NOTES",
    text: {
      'en': 'Updating notes ',
      'ru': 'Обновление заметок ',
      'de': 'Notizen aktualisieren '
    }
  };
  const SYNC_DOWNLOAD_ATTACHMENT = {
    code: "DOWNLOAD_ATTACHMENT",
    text: {
      'en': 'Downloading attachments ',
      'ru': 'Скачивание файлов ',
      'de': 'Anhang herunterladen '
    }
  };
  const SYNC_UPLOAD_FOLDERS = {
    code: "UPLOAD_FOLDERS",
    text: {
      'en': 'Uploading folders...',
      'ru': 'Передача папок...',
      'de': 'Ordner hochladen...'
    }
  };
  const SYNC_UPLOAD_TAGS = {
    code: "UPLOAD_TAGS",
    text: {
      'en': 'Uploading tags...',
      'ru': 'Передача тегов...',
      'de': 'Tags hochladen...'
    }
  };
  const SYNC_UPLOAD_NOTES = {
    code: "UPLOAD_NOTES",
    text: {
      'en': 'Uploading notes...',
      'ru': 'Передача заметок...',
      'de': 'Notizen hochladen...'
    }
  };
  const SYNC_UPLOAD_ATTACHMENTS = {
    code: "UPLOAD_ATTACHMENTS",
    text: {
      'en': 'Uploading attachments...',
      'ru': 'Передача файлов...',
      'de': 'Anhänge hochladen...'
    }
  };

  const SYNC_TRAFFIC_LIMIT = {
    code: "TRAFFIC_LIMIT",
    text: {
      'en': 'Traffic limit. Please, get Pro Account',
      'ru': 'Лимит траффика. Пожалуйста, приобретите Pro Аккаунт',
      'de': 'Traffic-Limit. Bitte erhalten Pro Account'
    }
  };

  const SYNC_STATUS_NOT_AVAILABLE = {
    code: "NOT_AVAILABLE",
    text: {
      'en': 'Not available',
      'ru': 'Не доступно',
      'de': 'Nicht verfügbar'
    }
  };
  const SYNC_STATUS_PAUSED = {
    code: "PAUSED",
    text: {
      'en': 'Sync paused',
      'ru': 'Пауза синхронизации',
      'de': 'Sync pausiert'
    }
  };
  const SYNC_STATUS_CANCELED = {
    code: "CANCELED",
    text: {
      'en': 'Sync failed',
      'ru': 'Ошибка синхронизации',
      'de': 'Zeitfehler'
    }
  };
  const SYNC_STATUS_READY = {
    code: "READY",
    text: {
      'en': 'Start Sync',
      'ru': 'Запустить синхронизацию',
      'de': 'Starten Sie die Synchronisierung'
    },
    cursor: 'pointer'
  };
  const SYNC_STATUS_DONE = {
    code: "DONE",
    text: {
      'en': 'Sync complete',
      'ru': 'Синхронизация завершена',
      'de': 'Synch abgeschlossen'
    },
  };
  const NEED_SYNC_BEFORE_SHARE = {
    code: "NEED_SYNC_BEFORE_SHARE",
    text: {
      'en': 'Please, make Sync before Share',
      'ru': 'Сделайте Синхронизацию перед Шарингом',
      'de': 'Bitte machen Sie Synch vor Freigabe'
    },
    cursor: 'pointer'
  };

  let syncTimerForDone = null;

  function setCustomerLanguage(language) {
    (window as any).customerLanguage = language;
  }

  function setEventOriginURL(originURL) {
    (window as any).originURL = originURL;
  }

  function getCustomerLanguage() {
    return (window as any).customerLanguage || 'en';
  }

  function getEventOriginURL() {
    return (window as any).originURL || '';
  }

  (window as any).elGetIpcRequest('event:initApp:request', (content) => {
    if(content.contextMenu) {
      (window as any).setAppContextMenu(content.contextMenu);
    }
    if (content.language) {
      setCustomerLanguage(content.language);
    }
    if (content.originURL) {
      setEventOriginURL(content.originURL);
    }
    elInitContent(content);
  });

  (window as any).elGetIpcRequest('event:client:autosync:request', (content) => {
    const {workspaceId} = content;
    elSendAutoSyncRequest({workspaceId});
  });

  (window as any).elGetIpcRequest('event:change:language:response', (content) => {
    if(content.contextMenu) {
      (window as any).setAppContextMenu(content.contextMenu);
    }
    if (content.language) {
      setCustomerLanguage(content.language);
    }
    requestSyncPanelReload();
  });

  (window as any).elGetIpcRequest('event:logout:request', () => {
    elProcessClientLogout();
  });

  (window as any).elGetIpcRequest('event:client:display:settings:response', () => {
    elDisplaySettings();
  });

  (window as any).elGetIpcRequest('event:client:traffic:info:response', () => {
    elTrafficInfo();
  });

  (window as any).elGetIpcRequest('event:client:update:inline:attach:response', (content) => {
    elUpdateInlineAttach(content);
  });

  (window as any).elGetIpcRequest('event:client:set:active:workspace:request', (content) => {
    const {workspaceId, onlineStatus} = content;
    toggleClientSync({workspaceId, show: onlineStatus});

    let workspaceSelector = '';
    if (typeof (workspaceId) !== 'undefined') {
      workspaceSelector = `[data-workspace-id=${workspaceId}]`;
      if (!workspaceId) {
        workspaceSelector = `[data-workspace-is-default=true]`;
      }
    }
    let $syncBar = $('.workspace-sync');
    let $syncEl = $syncBar.find(`.workspace-sync--action${workspaceSelector}`);
    if ($syncEl.length) {
      if ($syncBar.hasClass('hidden')) {
        $syncBar.removeClass('hidden');
      }
    }
  });

  (window as any).elGetIpcRequest('event:client:add:note:response', () => {
    elAddNote();
  });

  (window as any).elGetIpcRequest('event:client:find:notes:response', () => {
    elFindNotes();
  });

  (window as any).elGetIpcRequest('event:client:send:notification:response', (content) => {
    (window as any).elSendNotification(content);
  });

  (window as any).elGetIpcRequest('event:client:remove:workspace:response', (content) => {
    elRemoveWorkspaceForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:remove:organization:response', (content) => {
    elRemoveOrgForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:update:workspace:members:response', (content) => {
    elUpdateWorkspaceMembersForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:update:workspace:invites:response', (content) => {
    elUpdateWorkspaceInvitesForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:update:workspaces:access:response', (content) => {
    elUpdateWorkspacesAccessForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:update:workspaces:premium:response', (content) => {
    elUpdateWorkspacesPremiumForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:update:workspace:response', (content) => {
    elUpdateWorkspaceForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:update:organization:response', (content) => {
    elUpdateOrgForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:workspace:message:response', (content) => {
    elDisplayMessageForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:me:name:update:response', (content) => {
    elUpdateUserName(content);
  });

  (window as any).elGetIpcRequest('event:client:me:avatar:remove:response', (content) => {
    elRemoveUserAvatar(content);
  });

  (window as any).elGetIpcRequest('event:client:popup:too_many_notes:response', (content) => {
    elDisplayNotesCountLimitPopup(content);
  });

  (window as any).elGetIpcRequest('event:client:note:text:update:response', (content) => {
    updateNoteTextForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:popup:limits:show:response', (content) => {
    elDisplayLimitNoticeForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:popup:rate:app:show:response', (content) => {
    elDisplayRateNoticeForAccount(content);
  });

  (window as any).elGetIpcRequest('event:client:login:response', (content) => {
    elDisplayClientLogin(content.login);
  });

  (window as any).elGetIpcRequest('event:client:autosync:response', (content) => {
    const {workspaceId} = content;
    elClientSyncHandler({workspaceId});
  });

  (window as any).elGetIpcRequest('event:client:sync:response', (content) => {
    elDisplayClientSync(content);
  });

  (window as any).elGetIpcRequest('event:display:angular:notification:requests', (content) => {
    if (toastMessage) {
      toastMessage.reset();
    }

    content.settings.transparent = 0.5;
    toastMessage = $.toast((window as any).getToastMessageConfig(content));
  });

  // pre-load purchaise products
  (window as any).elGetIpcRequest('event:display:productsId:list:requests', (content) => {
    const productsIdList = content.productsIdList;
    if (!Array.isArray(productsIdList) || productsIdList.length <= 0) {
      console.log('Unable to get the products id list: ', content);
    }
    if ((window as any).getPurchaiseProducts) {
      (window as any).getPurchaiseProducts(productsIdList, (productsList) => {
        productsList = [];
        if (productsList && !productsList.length) {
          (window as any).getPurchaiseProducts(productsIdList);
        }
      });
    }
  });

  (window as any).elGetIpcRequest('event:editor:update:synced-status:request', (content) => {
    handleEditorUpdateSyncedStatusRequest(content);
  });

  (window as any).elGetIpcRequest('event:editor:bg:note-sync:request', (content) => {
    handleBgNoteSync(content);
  });

  (window as any).elGetIpcRequest('event:editor:update:attachment:request', (content) => {
    handleEditorUpdateAttachment(content);
  });

  (window as any).elGetIpcRequest('event:client:update:editor:downloaded:attach:response', (content) => {
    handleEditorUpdateDownloadedAttachment(content);
  });

  (window as any).elGetIpcRequest('event:navigate:to:internal:note:requests', (content) => {
    handleEditorNavigateToInternalNote(content);
  });

  (window as any).elGetIpcRequest('event:export:set:status:requests', (content) => {
    elSetExportStatus(content);
  });

  (window as any).elGetIpcRequest('event:export:set:progress:requests', (content) => {
    elSetExportProgress(content);
  });

  (window as any).elGetIpcRequest('event:client:export:workspace:notes:response', () => {
    elExportWorkspaceNotes();
  });

  (window as any).elSendIpcRequest('event:display:productsId:list:requests', {});

  if (window) {
    (window as any).addEventListener('message', (event) => {
      if (event.origin !== getEventOriginURL()) return;
      const {eventName} = event.data;
      switch (eventName) {
        case 'event:web:app:reload:request': {
          requestSyncPanelReload();
          break;
        }
        case 'event:web:app:print:request': {
          const {htmlContents} = event.data;
          requestPrintNote(htmlContents);
          break;
        }
        case 'event:web:app:playVideoTab:request': {
          const {htmlContents, attachment} = event.data;
          requestPlayVideoTab(htmlContents, attachment);
          break;
        }
        case 'event:web:app:closeVideoTab:request': {
          const {globalId} = event.data;
          requestCloseVideoTab(globalId);
          break;
        }
        case 'event:web:app:handleSaveNote:request': {
          const {note} = event.data;
          handleSaveNote(note);
          break;
        }
        case 'event:web:app:handleSelectNote:request': {
          const {workspaceId, globalId} = event.data;
          handleSelectNote({workspaceId, globalId});
          break;
        }
        case 'event:client:clear:sync:data:request': {
          handleClearSyncData();
          break;
        }
        case 'event:web:app:start-sync-attach:request': {
          const {attachment} = event.data;
          handleStartSingleAttachSync(attachment);
          break;
        }
        case 'event:web:app:stop-sync-attach:request': {
          const {attachment} = event.data;
          handleStopSingleAttachSync(attachment);
          break;
        }
        case 'event:web:app:set:active:workspace:request': {
          const {workspaceId} = event.data;
          handleSetActiveWorkspace(workspaceId);
          break;
        }
        case 'event:web:app:set:active:organization:request': {
          const {org} = event.data;
          handleSetActiveOrganization(org);
          break;
        }
        case 'event:web:app:print:attachment:request': {
          const {attachment} = event.data;
          handlePrintAttachment(attachment);
          break;
        }
        case 'event:web:app:copy:text:to:clipboard:request': {
          const {text} = event.data;
          handleCopyTextToClipBoard(text);
          break;
        }
        case 'event:web:editor:note:update:request': {
          handleEditorUpdateNote(event.data);
          break;
        }
        case 'event:web:app:handleEditorSyncDownload:request': {
          handleEditorSyncDownload(event.data);
          break;
        }
        case 'event:web:app:handleOpenUrl:request': {
          const {url} = event.data;
          handleOpenUrl(url);
          break;
        }
        case 'event:web:app:handleOpenHelp:request': {
          handleOpenHelp();
          break;
        }
        case 'event:web:app:handleOpenEditorHelp:request': {
          handleOpenEditorHelp();
          break;
        }
        case 'event:web:app:handleOpenSubmitBug:request': {
          handleOpenSubmitBug();
          break;
        }
        case 'event:web:app:remove:attachments:request': {
          handleRemoveAttachments(event.data);
          break;
        }
        case 'event:web:app:export:active:note:request': {
          handleExportNote(event.data);
          break;
        }
        case 'event:web:app:open:export:path': {
          handleOpenExportPath(event.data);
          break;
        }
        case 'event:web:app:cancel:export': {
          handleCancelExport(event.data);
          break;
        }
      }
    }, false);
  }

  /**
   * @param content Object
   */
  function elInitContent(content) {
    if ($) {
      elInitTemplateEventsHandlers();
      elChangeOnlineStatusHandlers();
    }
  }

  function elChangeOnlineStatusHandlers() {
    elProcessChangeOnlineStatus();
    (window as any).removeEventListener('online', elProcessChangeOnlineStatus);
    (window as any).addEventListener('online', elProcessChangeOnlineStatus);

    (window as any).removeEventListener('offline', elProcessChangeOnlineStatus);
    (window as any).addEventListener('offline', elProcessChangeOnlineStatus);
  }

  function elProcessChangeOnlineStatus(event = null) {
    let onlineStatus;
    if (event) {
      onlineStatus = (event.type === "online");
    } else {
      onlineStatus = (window as any).elAppIsOnline();
    }
    (window as any).elSendIpcRequest('event:online:changed', {"online": onlineStatus});
    toggleClientSync({show: onlineStatus});
  }

  function syncAcceleratorHandler(event) {
    if ((event.ctrlKey || event.metaKey) && event.which === 83) {
      event.preventDefault();
      elSendAutoSyncRequest({});
      return false;
    }
  }

  function elInitTemplateEventsHandlers() {
    $(document).off("keydown", syncAcceleratorHandler).on("keydown", syncAcceleratorHandler);
  }

  function elProcessClientLogout() {
    (window as any).elSendIpcRequest('event:logout:response', {"logout": true});
  }

  /**
   * @param {string|null} login
   */
  function elDisplayClientLogin(login) {
    let $syncBar = $('.workspace-sync');
    if ($syncBar) {
      if (login) {
        elDisplayClientSync({
          status: SYNC_STATUS_READY.code
        });
      } else {
        elDisplayClientSync({
          status: SYNC_STATUS_NOT_AVAILABLE.code
        })
      }
    }
  }

  /**
   * @param {{workspaceId:string, status:boolean}} syncData
   */
  function elDisplayClientSync(syncData) {
    if (syncData) {
      let {workspaceId, status} = syncData;
      let syncDisplayData = <any>{};

      if (syncTimerForDone && status !== SYNC_STATUS_DONE.code) {
        clearTimeout(syncTimerForDone);
        syncTimerForDone = null;
      }

      let workspaceSelector = '';
      if (typeof (workspaceId) !== 'undefined') {
        workspaceSelector = `[data-workspace-id=${workspaceId}]`;
        if (!workspaceId) {
          workspaceSelector = `[data-workspace-is-default=true]`;
        }
      }
      let $syncBar = $('.workspace-sync');
      let $syncEl = $syncBar.find(`.workspace-sync--action${workspaceSelector}`);

      if ($syncEl.length) {
        if ($syncBar.hasClass('hidden')) {
          $syncBar.removeClass('hidden');
        }
      }

      if (status) {
        syncDisplayData.status = status;
        switch (status) {
          case SYNC_STATUS_READY.code: {
            syncDisplayData.statusText = getTranslation(SYNC_STATUS_READY.text);
            syncDisplayData.cursor = SYNC_STATUS_READY.cursor;
            break;
          }

          case SYNC_STATUS_AUTHORIZED.code: {
            syncDisplayData.statusText = getTranslation(SYNC_STATUS_AUTHORIZED.text);
            break;
          }
          case SYNC_DOWNLOADING_META_INFO.code: {
            syncDisplayData.statusText = getTranslation(SYNC_DOWNLOADING_META_INFO.text);
            break;
          }
          case SYNC_REMOVE_FOLDERS.code: {
            syncDisplayData.statusText = getTranslation(SYNC_REMOVE_FOLDERS.text);
            break;
          }
          case SYNC_REMOVE_NOTES.code: {
            syncDisplayData.statusText = getTranslation(SYNC_REMOVE_NOTES.text);
            break;
          }
          case SYNC_REMOVE_ATTACHMENTS.code: {
            syncDisplayData.statusText = getTranslation(SYNC_REMOVE_ATTACHMENTS.text);
            break;
          }
          case SYNC_UPDATE_CONTENT.code: {
            syncDisplayData.statusText = getTranslation(SYNC_UPDATE_CONTENT.text);
            break;
          }
          case SYNC_DOWNLOAD_NOTES.code: {
            let statusText = getTranslation(SYNC_DOWNLOAD_NOTES.text);
            if (typeof (syncData.current) !== "undefined" && syncData.current) {
              if (typeof (syncData.total) !== "undefined" && syncData.total) {
                statusText += (syncData.current + "/" + syncData.total);
              }
            }
            syncDisplayData.statusText = statusText;
            break;
          }
          case SYNC_DOWNLOAD_ATTACHMENT.code: {
            let statusText = getTranslation(SYNC_DOWNLOAD_ATTACHMENT.text);
            if (typeof (syncData.current) !== "undefined" && syncData.current) {
              if (typeof (syncData.total) !== "undefined" && syncData.total) {
                statusText += (syncData.current + "/" + syncData.total);
              }
            }
            syncDisplayData.statusText = statusText;
            break;
          }
          case SYNC_UPLOAD_FOLDERS.code: {
            syncDisplayData.statusText = getTranslation(SYNC_UPLOAD_FOLDERS.text);
            break;
          }
          case SYNC_UPLOAD_TAGS.code: {
            syncDisplayData.statusText = getTranslation(SYNC_UPLOAD_TAGS.text);
            break;
          }
          case SYNC_UPLOAD_NOTES.code: {
            syncDisplayData.statusText = getTranslation(SYNC_UPLOAD_NOTES.text);
            break;
          }
          case SYNC_UPLOAD_ATTACHMENTS.code: {
            syncDisplayData.statusText = getTranslation(SYNC_UPLOAD_ATTACHMENTS.text);
            break;
          }
          case SYNC_TRAFFIC_LIMIT.code: {
            syncDisplayData.statusText = getTranslation(SYNC_TRAFFIC_LIMIT.text);
            break;
          }
          case NEED_SYNC_BEFORE_SHARE.code: {
            syncDisplayData.statusText = getTranslation(NEED_SYNC_BEFORE_SHARE.text);
            syncDisplayData.cursor = NEED_SYNC_BEFORE_SHARE.cursor;
            syncTimerForDone = setTimeout(() => {
              if ($syncEl.length) {
                let status = $syncEl.attr("data-sync-state");
                if (status === NEED_SYNC_BEFORE_SHARE.code) {
                  // TODO: display popup to make sync before share
                  //elDisplayClientSync({status: SYNC_STATUS_READY.code});
                }
              }
            }, 500);
            break;
          }
          case SYNC_STATUS_CANCELED.code: {
            syncDisplayData.statusText = getTranslation(SYNC_STATUS_CANCELED.text);
            syncTimerForDone = setTimeout(() => {
              if ($syncEl.length) {
                let status = $syncEl.attr("data-sync-state");
                if (status === SYNC_STATUS_CANCELED.code) {
                  //elDisplayClientSync({status: SYNC_STATUS_READY.code});
                }
              }
            }, 1000);
            break;
          }
          case SYNC_STATUS_PAUSED.code: {
            syncDisplayData.statusText = getTranslation(SYNC_STATUS_PAUSED.text);
            syncTimerForDone = setTimeout(() => {
              if ($syncEl.length) {
                let status = $syncEl.attr("data-sync-state");
                if (status === SYNC_STATUS_DONE.code) {
                  //elDisplayClientSync({status: SYNC_STATUS_READY.code});
                }
              }
            }, 1000);
            break;
          }
          case SYNC_STATUS_DONE.code: {
            syncDisplayData.statusText = getTranslation(SYNC_STATUS_DONE.text);
            if ($syncEl.length) {
              let status = $syncEl.attr("data-sync-state");
              if (status === SYNC_STATUS_DONE.code) {
                elDisplayClientSync({status: SYNC_STATUS_READY.code});
              }
            }
            break;
          }
          default: {
            syncDisplayData.status = SYNC_STATUS_NOT_AVAILABLE.code;
            syncDisplayData.statusText = getTranslation(SYNC_STATUS_NOT_AVAILABLE.text);
          }
        }
      }

      if ($syncEl.length) {
        let $statusIcon = $syncEl.find('.workspace-sync--action__icon[icon=workspace-sync]');
        let $errorIcon = $syncBar.find(".workspace-sync--action__icon[icon=workspace-sync-problem]");
        if ($statusIcon.length && $errorIcon.length) {
          let statusText = syncDisplayData.statusText;
          $syncEl.attr("data-sync-state", syncDisplayData.status);
          $syncEl.attr("data-cursor", syncDisplayData.cursor ? syncDisplayData.cursor : 'default');

          let errorLastSyncStatusList = [
            SYNC_STATUS_PAUSED.code,
            SYNC_STATUS_CANCELED.code,
            SYNC_TRAFFIC_LIMIT.code
          ];

          if (errorLastSyncStatusList.indexOf(syncDisplayData.status) >= 0) {
            if ($errorIcon.hasClass('hidden')) {
              $errorIcon.removeClass('hidden');
            }
          } else {
            if (!$errorIcon.hasClass('hidden')) {
              $errorIcon.addClass('hidden');
            }
          }

          let updateLastSyncStatusList = [
            SYNC_STATUS_DONE.code,
            SYNC_STATUS_PAUSED.code,
            SYNC_STATUS_CANCELED.code,
            SYNC_TRAFFIC_LIMIT.code,
            SYNC_STATUS_NOT_AVAILABLE.code
          ];
          if (updateLastSyncStatusList.indexOf(syncDisplayData.status) >= 0) {
            let today = new Date();
            let month = <any>(today.getMonth() + 1);
            if (month < 10) {
              month = "0" + month;
            }
            let day = <any>(today.getDate());
            if (day < 10) {
              day = "0" + day;
            }
            let minutes = <any>(today.getMinutes());
            if (minutes < 10) {
              minutes = "0" + minutes;
            }
            let seconds = <any>(today.getSeconds());
            if (seconds < 10) {
              seconds = "0" + seconds;
            }
            let date = today.getFullYear() + '-' + month + '-' + day;
            let time = today.getHours() + ":" + minutes + ":" + seconds;
            let dateTime = date + " " + time;

            $syncEl.attr('title', getTranslation(LAST_SYNC_TITLE) + dateTime + ' (' + statusText + ')');
            if ($statusIcon.hasClass('rotating')) {
              $statusIcon.removeClass('rotating');
            }
          } else if (syncDisplayData.status !== SYNC_STATUS_READY.code) {
            $syncEl.attr('title', statusText);
            if (!$statusIcon.hasClass('rotating')) {
              $statusIcon.addClass('rotating');
            }
          } else {
            $syncEl.attr('title', statusText);
            if ($statusIcon.hasClass('rotating')) {
              $statusIcon.removeClass('rotating');
            }
          }
        }
      }
    }
  }

  /**
   * @param {{workspaceId:string, show:boolean}} inputData
   */
  function toggleClientSync(inputData) {
    let {workspaceId, show} = inputData;
    show = show || false;
    let $syncBar = $('.workspace-sync');
    if ($syncBar.length) {
      let workspaceSelector = '';
      if (typeof (workspaceId) !== 'undefined') {
        workspaceSelector = `[data-workspace-id=${workspaceId}]`;
        if (!workspaceId) {
          workspaceSelector = `[data-workspace-is-default=true]`;
        }
      }

      let $syncEl = $syncBar.find(`.workspace-sync--action${workspaceSelector} > [icon=workspace-sync]`);
      let $syncErrorEl = $syncBar.find(`.workspace-sync--action${workspaceSelector} > [icon=workspace-sync-error]`);

      if ($syncEl.length && $syncErrorEl.length) {
        if (show) {
          elDisplayClientSync({
            workspaceId,
            status: SYNC_STATUS_READY.code
          });

          $syncErrorEl.hide(0, () => {
            $syncEl.show(0);
          });
        } else {
          $syncEl.hide(0, () => {
            $syncErrorEl.show(0);
          });
          elDisplayClientSync({
            workspaceId,
            status: SYNC_STATUS_NOT_AVAILABLE.code
          });
        }
      }
    }
  }

  $(function () {
    $(document).off("click", ".attach_single_name a")
      .on("click", ".attach_single_name a", function () {
        let $self = $(this);
        if ($self.attr('attachment-id') && $self.data('href')) {
          (window as any).location = $self.data('href');
        }
      });

    $(document).off("click", ".workspace-sync--action > [icon=workspace-sync]")
      .on("click", ".workspace-sync--action > [icon=workspace-sync]", function (event) {
        event.preventDefault();
        const workspaceId = $(this).closest('.workspace-sync--action').attr('data-workspace-id');
        elClientSyncHandler({workspaceId, manualSync: true});
      });
  });

  /**
   * @param {{workspaceId:string, timer:int}} inputData
   */
  function elSendAutoSyncRequest(inputData) {
    let {workspaceId, timer} = inputData;
    timer = timer || 100;
    setTimeout(() => {
      (window as any).elSendIpcRequest('event:client:autosync:request', {workspaceId});
    }, timer);
  }

  /**
   * @param {{workspaceId:string, manualSync:boolean}} inputData
   */
  function elClientSyncHandler(inputData) {
    const {workspaceId, manualSync} = inputData;
    let $syncStatus = $(".workspace-sync--action");
    if ($syncStatus.length) {
      let status = $syncStatus.attr("data-sync-state") ? $syncStatus.attr("data-sync-state").trim() : '';
      if (!status) {
        status = SYNC_STATUS_READY.code;
        $syncStatus.attr("data-sync-state", status);
      }

      let syncAvailableStatuses = [
        SYNC_STATUS_READY.code,
        NEED_SYNC_BEFORE_SHARE.code,
        SYNC_STATUS_PAUSED.code,
        SYNC_STATUS_CANCELED.code,
        SYNC_TRAFFIC_LIMIT.code,
        SYNC_STATUS_DONE.code
      ];

      if (status === SYNC_TRAFFIC_LIMIT.code) {
        // return (window as any).elSendIpcRequest('event:open:payment:window:request');
      }

      if (syncAvailableStatuses.indexOf(status) >= 0) {
        return (window as any).elSendIpcRequest('event:sync:start:request', {workspaceId, status, manualSync});
      }
      (window as any).elSendIpcRequest('event:sync:queue:request', {workspaceId, status});
    }
  }

  function requestSyncPanelReload() {
    setTimeout(() => {
      elInitContent({
        language: getCustomerLanguage()
      });
    }, 350);
  }

  /**
   * @param {string} htmlContents
   */
  function requestPrintNote(htmlContents) {
    if (!htmlContents) return;
    (window as any).elSendIpcRequest('event:print', {"template": htmlContents});
  }

  /**
   * @param {string} htmlContents
   * @param {{}} attachment
   */
  function requestPlayVideoTab(htmlContents, attachment) {
    if (!htmlContents) return;
    (window as any).elSendIpcRequest('event:play:video:request', {"html": htmlContents, "attach": attachment});
  }

  /**
   * @param {string} globalId
   */
  function requestCloseVideoTab(globalId) {
    if (!globalId) return;
    (window as any).elSendIpcRequest('event:close:video:request', {"globalId": globalId});
  }

  /**
   * @param {{}} note
   */
  function handleSaveNote(note) {
    if (note && !note.isOfflineOnly) {
      elSendAutoSyncRequest({});
    }
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  function handleSelectNote(inputData) {
    (window as any).elSendIpcRequest('event:select:note:request', inputData);
  }

  function handleClearSyncData() {
    (window as any).elSendIpcRequest('event:client:clear:sync:data:request');
  }

  /**
   * @param {{}} attachment
   */
  function handleStartSingleAttachSync(attachment) {
    if (attachment) {
      (window as any).elSendIpcRequest('event:client:start-single-attach-sync:request', {"globalId": attachment.globalId});
    }
  }

  /**
   * @param {{}} attachment
   */
  function handleStopSingleAttachSync(attachment) {
    if (attachment) {
      (window as any).elSendIpcRequest('event:client:stop-single-attach-sync:request', {"globalId": attachment.globalId});
    }
  }

  function handleOpenUrl(url) {
    (window as any).elSendIpcRequest('event:client:open:url:request', {url});
  }

  function handleOpenHelp() {
    (window as any).elSendIpcRequest('event:client:open:help:request', {});
  }

  function handleOpenEditorHelp() {
    (window as any).elSendIpcRequest('event:client:open:editor:help:request', {});
  }

  function handleOpenSubmitBug() {
    (window as any).elSendIpcRequest('event:client:open:submit:bug:request', {});
  }

  function handleRemoveAttachments({workspaceId, noteId, attachments}) {
    (window as any).elSendIpcRequest('event:web:app:remove:attachments:request', {workspaceId, noteId, attachments});
  }

  function handleExportNote(data) {
    (window as any).elSendIpcRequest('event:web:app:export:active:note:request', data);
  }

  function handleOpenExportPath(data) {
    (window as any).elSendIpcRequest('event:web:app:export:active:open:request', data);
  }

  function handleCancelExport(data) {
    (window as any).elSendIpcRequest('event:web:app:export:active:cancel:request', data);
  }

  /**
   * @param {string} workspaceId
   */
  function handleSetActiveWorkspace(workspaceId) {
    (window as any).elSendIpcRequest('event:client:set:active:workspace:request', {workspaceId});
  }

  /**
   * @param {{}} org
   */
  function handleSetActiveOrganization(org) {
    (window as any).elSendIpcRequest('event:client:set:active:organization:request', {org});
  }

  function handlePrintAttachment(attachment) {
    (window as any).elSendIpcRequest('event:client:print:attachment:request', {attachment});
  }

  /**
   * @param {string} text
   */
  function handleCopyTextToClipBoard(text) {
    (window as any).writeTextToClipboard(text);
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  function handleEditorUpdateNote(inputData) {
    (window as any).elSendIpcRequest('event:editor:note:update:request', inputData);
  }

  function handleEditorSyncDownload(inputData) {
    (window as any).elSendIpcRequest('event:editor:attachment:sync:download:request', inputData);
  }

  function handleEditorPutDbRequest(data) {
    // @ts-ignore
    return new Promise(resolve => {
      (window as any).elSendIpcRequest('event:editor:put:db:data:request', data);
      (window as any).elGetOnceIpcRequest('event:editor:put:db:data:response', (data) => {
        return resolve(data);
      });
    });
  }

  function handleEditorPutArrayDbRequest(data) {
    // @ts-ignore
    return new Promise(resolve => {
      (window as any).elSendIpcRequest('event:editor:put:db:array:data:request', data);
      (window as any).elGetOnceIpcRequest('event:editor:put:db:array:data:response', (data) => {
        return resolve(data);
      });
    });
  }

  function handleEditorGetDbRequest(data) {
    // @ts-ignore
    return new Promise(resolve => {
      (window as any).elSendIpcRequest('event:editor:get:db:data:request', data);
      (window as any).elGetOnceIpcRequest('event:editor:get:db:data:response', (data) => {
        return resolve(data);
      });
    });
  }

  function handleEditorGetFromRangeDbRequest(data) {
    // @ts-ignore
    return new Promise(resolve => {
      (window as any).elSendIpcRequest('event:editor:getFromRange:db:data:request', data);
      (window as any).elGetOnceIpcRequest('event:editor:getFromRange:db:data:response', (data) => {
        return resolve(data);
      });
    });
  }

  function handleEditorGetKeysFromRangeRequest(data) {
    // @ts-ignore
    return new Promise(resolve => {
      (window as any).elSendIpcRequest('event:editor:getKeysFromRange:db:data:request', data);
      (window as any).elGetOnceIpcRequest('event:editor:getKeysFromRange:db:data:response', (data) => {
        return resolve(data);
      });
    });
  }

  function handleEditorDeleteRequest(data) {
    // @ts-ignore
    return new Promise(resolve => {
      (window as any).elSendIpcRequest('event:editor:delete:db:data:request', data);
      (window as any).elGetOnceIpcRequest('event:editor:delete:db:data:response', (data) => {
        return resolve(data);
      });
    });
  }

  function handleEditorSyncCallbackRequest(data) {
    (window as any).elSendIpcRequest('event:editor:sync-callback:data:request', data);
  }

  function handleEditorSyncTextCallbackRequest(data) {
    (window as any).elSendIpcRequest('event:editor:sync-text-callback:data:request', data);
  }

  function handleEditorOpenAttachment(data) {
    (window as any).elSendIpcRequest('event:editor:open:attach:request', data);
  }

  function handleEditorShowError(data) {
    const { message } = data
    if(!message) { return }
    (window as any).electron.showError({...data, electron: true});
  }

  function handleEditorUpdateSyncedStatusRequest(data) {
    if((window as any).textEditor && (window as any).textEditor.setNoteSynced) {
      (window as any).textEditor.setNoteSynced(data, true);
    }
    if((window as any).textEditor && (window as any).textEditor.setBgNoteSynced) {
      (window as any).textEditor.setBgNoteSynced(data, true);
    }
  }

  function handleBgNoteSync(data) {
    if((window as any).electron) {
      if((window as any).electron.makeBgNoteSync) {
        (window as any).electron.makeBgNoteSync(data);
      }
    }
  }

  function handleEditorUpdateAttachment(data) {
    if((window as any).electron) {
      if((window as any).electron.updateEditorAttachment) {
        (window as any).electron.updateEditorAttachment(data);
      }
    }
  }

  function handleEditorNavigateToInternalNote(data) {
    if((window as any).electron) {
      if((window as any).electron.processNavigation) {
        (window as any).electron.processNavigation(data);
      }
    }
  }

  function handleEditorUpdateDownloadedAttachment(data) {
    if((window as any).electron) {
      if((window as any).electron.updateEditorDownloadedAttach) {
        (window as any).electron.updateEditorDownloadedAttach(data);
      }
    }
  }

  if(!(window as any).textEditor) {
    (window as any).textEditor = {};
  }
  if(!(window as any).textEditor.put) {
    (window as any).textEditor.put = handleEditorPutDbRequest;
  }
  if(!(window as any).textEditor.putArray) {
    (window as any).textEditor.putArray = handleEditorPutArrayDbRequest;
  }
  if(!(window as any).textEditor.get) {
    (window as any).textEditor.get = handleEditorGetDbRequest;
  }
  if(!(window as any).textEditor.getFromRange) {
    (window as any).textEditor.getFromRange = handleEditorGetFromRangeDbRequest;
  }
  if(!(window as any).textEditor.getKeysFromRange) {
    (window as any).textEditor.getKeysFromRange = handleEditorGetKeysFromRangeRequest;
  }
  if(!(window as any).textEditor.delete) {
    (window as any).textEditor.delete = handleEditorDeleteRequest;
  }
  if(!(window as any).textEditor.syncCb) {
    (window as any).textEditor.syncCb = handleEditorSyncCallbackRequest;
  }
  if(!(window as any).textEditor.syncTextCb) {
    (window as any).textEditor.syncTextCb = handleEditorSyncTextCallbackRequest;
  }
  if(!(window as any).textEditor.openAttachment) {
    (window as any).textEditor.openAttachment = handleEditorOpenAttachment;
  }
  if(!(window as any).textEditor.showError) {
    (window as any).textEditor.showError = handleEditorShowError;
  }

  function elDisplaySettings() {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.showSettings) {
      (window as any).electron.showSettings();
    }
  }

  function elTrafficInfo() {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.showTrafficInfo) {
      (window as any).electron.showTrafficInfo();
    }
  }

  function elUpdateInlineAttach(attachData) {
    let $attach = $("img[data-attachment-id=" + attachData.globalId + "][data-note-id=" + attachData.noteGlobalId + "]");
    if ($attach.length) {
      $attach.attr('src', $attach.attr('src') + '&' + new Date().getTime());
    }
  }

  /**
   * @param {{}} errorData
   */
  function elDisplayLimitNoticeForAccount(errorData) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.showLimitNoticeForAccount) {
      (window as any).electron.showLimitNoticeForAccount(errorData);
    }
  }

  /**
   * @param {{}} storeData
   */
  function elDisplayRateNoticeForAccount(storeData) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.showRateNoticeForAccount) {
      (window as any).electron.showRateNoticeForAccount(storeData);
    }
  }

  /**
   * @param {{}} data
   */
  function elRemoveWorkspaceForAccount(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.removeWorkspaceForAccount) {
      (window as any).electron.removeWorkspaceForAccount(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elRemoveOrgForAccount(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.removeOrgForAccount) {
      (window as any).electron.removeOrgForAccount(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elUpdateWorkspaceForAccount(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.updateWorkspaceForAccount) {
      (window as any).electron.updateWorkspaceForAccount(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elUpdateOrgForAccount(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.updateOrgForAccount) {
      (window as any).electron.updateOrgForAccount(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elUpdateWorkspaceMembersForAccount(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.updateWorkspaceMembersForAccount) {
      (window as any).electron.updateWorkspaceMembersForAccount(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elUpdateWorkspaceInvitesForAccount(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.updateWorkspaceInvitesForAccount) {
      (window as any).electron.updateWorkspaceInvitesForAccount(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elUpdateWorkspacesAccessForAccount(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.updateWorkspacesAccessForAccount) {
      (window as any).electron.updateWorkspacesAccessForAccount(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elUpdateWorkspacesPremiumForAccount(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.fetchWorkspacePremiumForAccount) {
      (window as any).electron.fetchWorkspacePremiumForAccount(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elDisplayMessageForAccount(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.displayMessageForAccount) {
      (window as any).electron.displayMessageForAccount(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elUpdateUserName(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.updateUserNameForAccount) {
      (window as any).electron.updateUserNameForAccount(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elRemoveUserAvatar(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.removeUserAvatarForAccount) {
      (window as any).electron.removeUserAvatarForAccount(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elDisplayNotesCountLimitPopup(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.displayNotesCountLimitPopup) {
      (window as any).electron.displayNotesCountLimitPopup(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elSetExportStatus(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.setExportStatus) {
      (window as any).electron.setExportStatus(data);
    }
  }

  /**
   * @param {{}} data
   */
  function elSetExportProgress(data) {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.setExportProgress) {
      (window as any).electron.setExportProgress(data);
    }
  }

  function elExportWorkspaceNotes() {
    if (typeof ((window as any).electron) !== "undefined" && (window as any).electron.exportWorkspaceNotes) {
      (window as any).electron.exportWorkspaceNotes();
    }
  }

  /**
   * @param {{}} data
   */
  function updateNoteTextForAccount(data) {
    const {globalId} = data;
    if (!globalId) {
      return;
    }

    let $inlineImg = $(`img[data-attachment-id=${globalId}]`);
    if (!$inlineImg.length) {
      $inlineImg = $(`.note-text-editor img[src*='/${globalId}/']`)
    }

    if (!$inlineImg.length) {
      return;
    }

    $inlineImg.attr('src', `${$inlineImg.attr('src')}?t=${new Date().getTime()}`);
  }

  function elFindNotes() {
    $(".dialog-input--search").focus();
  }

  function elAddNote() {
    $(".button-add-circle").get(0).click();
  }
})(window);