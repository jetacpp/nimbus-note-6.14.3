(function (window) {

  console.log("common.js script load for communicate with auth/webnotes/update project");

  const $ = (window as any).$;
  const {ipcRenderer} = (window as any).nodeRequire('electron');
  const remote = (window as any).nodeRequire('electron').remote;
  const {clipboard, Menu} = remote;
  let menu;
  let inAppPurchase;
  if(remote.process.platform === 'darwin') {
    inAppPurchase = remote.inAppPurchase;
  }

  /**
   * @param {string} key
   * @param {*} val
   */
  function elSendIpcRequest(key, val = null) {
    if (ipcRenderer) {
      ipcRenderer.send(key, val);
    }
  }

  /**
   * @param {string} key
   * @param {Function} callback
   */
  function elGetIpcRequest(key, callback = (res) => {
  }) {
    if (ipcRenderer)
      ipcRenderer.on(key, function (event, arg) {
        callback(arg);
      });
  }

  /**
   * @param {string} key
   * @param {Function} callback
   */
  function elGetOnceIpcRequest(key, callback = (res) => {
  }) {
    if (ipcRenderer)
      ipcRenderer.once(key, function (event, arg) {
        callback(arg);
      });
  }

  /**
   * @param {{}} content
   */
  function elSendNotification(content) {
    let {title, body} = content;
    body.body = body.subtitle;
    const notification = new (window as any).Notification(title, body);
    if (body.reminderData && notification) {
      notification.addEventListener('click', () => {
        const noteUrl = body.reminderData.noteUrl;
        if (noteUrl) {
          (window as any).elSendIpcRequest('event:open:note:url:handler', {noteUrl});
        }
      });
    }
  }

  /**
   * @param content mixed
   */
  function elLogContent(content) {
    console.log("Log info: ", content);
  }

  /**
   * @returns {boolean}
   */
  function elAppIsOnline() {
    return navigator.onLine;
  }

  if ($) {
    $.fn.serializeObject = function () {
      let o = {};
      let a = this.serializeArray();
      $.each(a, function () {
        if (o[this.name]) {
          if (!o[this.name].push) {
            o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value || '');
        } else {
          o[this.name] = this.value || '';
        }
      });
      return o;
    };
  }

  /**
   * @param {[string]} productIdList
   * @param {Function} callback
   */
  let getPurchaiseProducts = (productIdList, callback) => {
    callback = callback || function () {
    };

    if (!(window as any).canMakePayments()) { return callback([]); }
    if(!inAppPurchase) { return callback([]); }
    inAppPurchase.getProducts(productIdList).then(products => {
      if (!Array.isArray(products) || products.length <= 0) {
        console.log('Unable to retrieve the product information.', products);
      }

      (window as any).elSendIpcRequest('event:display:products:save:requests', {
        'products': products
      });

      if (callback) {
        return callback(products);
      }
    });
  };

  let canMakePayments = () => {
    if(!inAppPurchase) { return false; }
    return inAppPurchase.canMakePayments();
  };

  let writeTextToClipboard = (text) => {
    clipboard.writeText(text);
  };

  let getToastMessageConfig = (content) => {
    const {data, settings} = content;

    const toastMessage = <any>{
      text: data.text,
      showHideTransition: 'fade',
      textColor: '#fff',
      allowToastClose: true,
      hideAfter: typeof (settings.hideAfter) !== "undefined" ? settings.hideAfter : 10000,
      stack: 5,
      textAlign: 'left',
      position: typeof (settings.position) !== "undefined" ? settings.position : 'top-right',
      transparent: typeof (settings.transparent) !== "undefined" ? settings.transparent : 0.3
    };

    switch (settings.type) {
      case (window as any).MESSAGE_TYPE_INFO: {
        toastMessage.bgColor = `rgba(0, 0, 255, ${toastMessage.transparent})`;
        break;
      }
      case (window as any).MESSAGE_TYPE_WARNING: {
        toastMessage.bgColor = `rgba(255, 255, 0, ${toastMessage.transparent})`;
        break;
      }
      case (window as any).MESSAGE_TYPE_ERROR: {
        toastMessage.bgColor = `rgba(255, 0, 0, ${toastMessage.transparent})`;
        break;
      }
      case (window as any).MESSAGE_TYPE_SUCCESS: {
        toastMessage.bgColor = `rgba(0, 255, 0, ${toastMessage.transparent})`;
        break;
      }
      default: {
        toastMessage.bgColor = `rgba(0, 0, 0, ${toastMessage.transparent})`;
      }
    }

    return toastMessage;
  };

  const reloadPageHandler = () => {
    elSendIpcRequest('event:client:reload:page:request', {});
  };


  const contextMenuHandler = (event) => {
    event.preventDefault();
    const tagNameCorrect = [
      'input',
      'textarea',
    ].indexOf(event.target.tagName.toLowerCase()) >= 0;
    const parentCorrect = event.target.closest([
      `.note-body-panel`,
    ].join(','));
    const parentTable = parentCorrect ? event.target.closest('.table-wrapper') : null;
    const parentMenu = parentCorrect ? event.target.closest('.popup-menu') : null;
    const editorBlockCorrect = parentCorrect && !parentTable && !parentMenu;
    if((tagNameCorrect || editorBlockCorrect)  && menu) {
      menu.popup();
    }
  };

  const setAppContextMenu = (items = []) => {
    // menu = Menu.buildFromTemplate(items);
  };

  (window as any).elSendIpcRequest = elSendIpcRequest;
  (window as any).elGetIpcRequest = elGetIpcRequest;
  (window as any).elGetOnceIpcRequest = elGetOnceIpcRequest;
  (window as any).elAppIsOnline = elAppIsOnline;
  (window as any).elLogContent = elLogContent;
  (window as any).getPurchaiseProducts = getPurchaiseProducts;
  (window as any).canMakePayments = canMakePayments;
  (window as any).getToastMessageConfig = getToastMessageConfig;
  (window as any).writeTextToClipboard = writeTextToClipboard;
  (window as any).elSendNotification = elSendNotification;
  (window as any).setAppContextMenu = setAppContextMenu;

  (window as any).MESSAGE_TYPE_INFO = 'info';
  (window as any).MESSAGE_TYPE_ERROR = 'error';
  (window as any).MESSAGE_TYPE_WARNING = 'warning';
  (window as any).MESSAGE_TYPE_SUCCESS = 'success';

  // (window as any).addEventListener('contextmenu', contextMenuHandler, false);
})(window);
