import chalk from "chalk";
import fs from "fs-extra";
import DataStore from "nedb";
import {default as config} from "./config.runtime";
import {default as auth} from "./nimbus-electron/auth/auth";
import {default as error} from "./nimbus-electron/utilities/customError";
import {default as workspace} from "./nimbus-electron/db/models/workspace";

let dbConnections = {};
let dbClientId = null;

export default class AppDatabase {
  /**
   * @param {Function} callback
   */
  static prepareClientDbPath(callback = (err, res) => {
  }) {
    auth.getUser((err, authInfo) => {
      if (authInfo && Object.keys(authInfo).length) {
        if (AppDatabase.getDbClientId() !== authInfo.userId) {
          AppDatabase.setDbClientId(authInfo.userId);
        }
        callback(null, authInfo);
      } else {
        callback(null, null);
      }
    });
  }

  /**
   * @param {string} clientId
   */
  static setDbClientId(clientId) {
    dbClientId = clientId;
    AppDatabase.setClientDbPath(clientId);
    AppDatabase.setWorkspaceDbPath(config.DEFAULT_WORKSPACE_ID);
    AppDatabase.setClientAttachmentPath(clientId);
    AppDatabase.setClientTempAttachmentPath(clientId);
    AppDatabase.setClientExportPath(clientId);
    dbConnections = {};
  }

  /**
   * @return {string|null}
   */
  static getDbClientId() {
    return dbClientId;
  }

  static clearDbConnections() {
    for (let i in dbConnections) {
      if (dbConnections.hasOwnProperty(i)) {
        delete dbConnections[i];
      }
    }
  }

  /**
   * @param {string} clientId
   */
  static setClientDbPath(clientId) {
    if (clientId) {
      config.clientDataBasePath = `${config.dataBasePath}/${clientId.toString()}`;
      if (!fs.existsSync(config.clientDataBasePath)) {
        fs.mkdirSync(config.clientDataBasePath);
      }
    }
  }

  /**
   * @param {string} workspaceId
   */
  static setWorkspaceDbPath(workspaceId) {
    if (!workspaceId) {
      if (config.SHOW_WEB_CONSOLE) {
        console.log("Client workspaceId is empty", workspaceId);
      }
    }

    if (!config.clientDataBasePath) {
      if (config.SHOW_WEB_CONSOLE) {
        console.log("Client database path is empty", config.clientDataBasePath, workspaceId);
      }
      return;
    }

    let workspaceDbPath = `${config.clientDataBasePath}/${workspaceId}`;
    if (!fs.existsSync(workspaceDbPath)) {
      fs.mkdirSync(workspaceDbPath);
    }
  }

  /**
   * @return {string}
   */
  static getClientDbPath() {
    return config.clientDataBasePath || "";
  }

  /**
   * @param {string} clientId
   */
  static setClientAttachmentPath(clientId) {
    if (clientId) {
      config.clientAttachmentBasePath = `${config.nimbusAttachmentPath}/${clientId.toString()}`;
      if (!fs.existsSync(config.clientAttachmentBasePath)) {
        fs.mkdirSync(config.clientAttachmentBasePath);
      }
    }
  }

  /**
   * @param {string} clientId
   */
  static setClientTempAttachmentPath(clientId) {
    if (clientId) {
      if (!fs.existsSync(config.nimbusTempAttachmentPath)) {
        fs.mkdirSync(config.nimbusTempAttachmentPath);
      }

      config.clientTempAttachmentBasePath = `${config.nimbusTempAttachmentPath}/${clientId.toString()}`;
      if (!fs.existsSync(config.clientTempAttachmentBasePath)) {
        fs.mkdirSync(config.clientTempAttachmentBasePath);
      }
    }
  }

  /**
   * @param {string} clientId
   */
  static setClientExportPath(clientId) {
    if (clientId) {
      if (!fs.existsSync(config.nimbusExportPath)) {
        fs.mkdirSync(config.nimbusExportPath);
      }
      config.clientExportBasePath = `${config.nimbusExportPath}/${clientId.toString()}`;
      if (!fs.existsSync(config.clientExportBasePath)) {
        fs.mkdirSync(config.clientExportBasePath);
      }
    }
  }

  /**
   * @return {string}
   */
  static getClientAttachmentPath() {
    return config.clientAttachmentBasePath || "";
  }

  /**
   * @return {string}
   */
  static getClientTempAttachmentPath() {
    return config.clientTempAttachmentBasePath || "";
  }

  /**
   * @return {string}
   */
  static getClientExportPath() {
    return config.clientExportBasePath || "";
  }

  /**
   * @param {string} clientId
   */
  static setClientAttachmentUrl(clientId) {
    if (clientId) {
      config.clientAttachmentBaseUrl = `${config.nimbusAttachmentUrl}/${clientId.toString()}`;
    }
  }

  /**
   * @param {{workspaceId:string, dbName:string, config:{}}} inputData
   * @return {{}|null}
   */
  static getInstance(inputData) {
    const {dbName} = inputData;
    let {workspaceId} = inputData;

    if (!dbName) {
      return null;
    }

    if (!config) {
      return null;
    }

    if (!workspaceId) {
      workspaceId = workspace.DEFAULT_NAME;
    }

    let dbFullName;
    if (dbName === config.DB_USER_MODEL_NAME || dbName === config.DB_SETTINGS_MODEL_NAME) {
      dbFullName = dbName;
    } else if (dbName === config.DB_WORKSPACE_MODEL_NAME || dbName === config.DB_USER_SETTINGS_MODEL_NAME) {
      dbFullName = dbName;
    } else if (config.clientDataBasePath && workspaceId) {
      dbFullName = workspaceId + "/" + dbName;
    }

    if (!dbFullName) {
      if (config.SHOW_WEB_CONSOLE) {
        console.log("Client db full name is empty", config.clientDataBasePath, workspaceId, dbName);
      }
    }

    if (typeof (dbConnections[dbFullName]) === "undefined") {
      let dbPath = `${config.dataBasePath}/${dbName}`;
      if (dbName === config.DB_USER_MODEL_NAME || dbName === config.DB_SETTINGS_MODEL_NAME) {
        dbPath = `${config.dataBasePath}/${dbName}`;
      } else if (config.clientDataBasePath) {
        if (dbName === config.DB_WORKSPACE_MODEL_NAME || dbName === config.DB_USER_SETTINGS_MODEL_NAME) {
          dbPath = `${config.clientDataBasePath}/${dbName}`;
        } else {
          dbPath = `${config.clientDataBasePath}/${workspaceId}/${dbName}`;
        }
      } else {
        if (config.SHOW_WEB_CONSOLE) {
          console.log("Client db path is empty", config.clientDataBasePath, workspaceId, dbName);
        }
        return null;
      }

      dbConnections[dbFullName] = new DataStore({
        filename: dbPath,
        autoload: true
      });
    }
    return dbConnections[dbFullName];
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    const workspaceId = options.workspaceId;
    const dbName = options.dbName;
    const pdb = AppDatabase.getInstance({workspaceId, dbName});
    if (!pdb) {
      console.log("No database connection", dbName)
      // console.log(error.noDbConnection(dbName))
      return callback(null, null);
    }

    data = data.findParams ? data.findParams : data;
    try {
      pdb.findOne(AppDatabase.getFindQuery(data, options), (err, doc) => {
        if (err) {
          if (config.SHOW_WEB_CONSOLE) {
            console.log("Problem to find objects doc & err: ", doc, err);
          }
          return callback(error.itemFindError(), null);
        }
        callback(null, doc);
      });
    } catch (err) {
      return callback(error.itemFindError(), null);
    }
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    const workspaceId = options.workspaceId;
    const dbName = options.dbName;
    let pdb = AppDatabase.getInstance({workspaceId, dbName});
    if (!pdb) {
      console.log("No database connection", dbName)
      // console.log(error.noDbConnection(dbName))
      return callback(null, []);
    }

    let findQuery = options.findParams ? options.findParams : {selector: data};
    let query = pdb.find(AppDatabase.getFindQuery(findQuery.selector, options));
    let sortQuery = {};
    if (typeof (findQuery.sort) !== "undefined") {
      sortQuery = {};
      for (let i in findQuery.sort) {
        if (findQuery.sort.hasOwnProperty(i)) {
          for (let j in findQuery.sort[i]) {
            if (findQuery.sort[i].hasOwnProperty(j)) {
              sortQuery[j] = findQuery.sort[i][j] === "desc" ? -1 : 1;
            }
          }
        }
      }

      if (Object.keys(sortQuery)) {
        query.sort(sortQuery);
      }
    }

    if (typeof (findQuery.skip) !== "undefined") {
      query.skip(findQuery.skip);
    }
    if (typeof(findQuery.limit) !== "undefined") {
      query.limit(findQuery.limit);
    }

    try {
      query.exec((err, docs) => {
        if (err) {
          if (config.SHOW_WEB_CONSOLE) {
            console.log("Problem to findAll object", data, err);
          }
          return callback(error.itemFindAllError(), []);
        }
        callback(null, docs);
      });
    } catch (err) {
      return callback(error.itemFindAllError(), []);
    }
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static create(data, options, callback = (err, res) => {
  }) {
    const workspaceId = options.workspaceId;
    const dbName = options.dbName;
    let pdb = AppDatabase.getInstance({workspaceId, dbName});

    if (!pdb) {
      return callback(error.noDbConnection(options.dbName), null);
    }

    let newData = options.prepareModelData(data);
    try {
      pdb.insert(newData, (err, newDoc) => {
        if (err || !newDoc) {
          if (err.errorType === 'uniqueViolated' && err.key === 'default') {
            return callback(null, newDoc);
          }
          return callback(error.itemSaveError(), null);
        }

        callback(null, newDoc);
      });
    } catch (err) {
      return callback(error.itemSaveError(), null);
    }
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, options, callback = (err, res) => {
  }) {
    const workspaceId = options.workspaceId;
    const dbName = options.dbName;
    let pdb = AppDatabase.getInstance({workspaceId, dbName});
    if (!pdb) {
      return callback(error.noDbConnection(dbName), 0);
    }

    AppDatabase.find(data, options, (err, doc) => {
      if (err || !doc) {
        if (config.SHOW_WEB_CONSOLE) {
          console.log("No db objects for update: ", data, options);
        }
        return callback(null, 0);
      }

      for (let prop in options.updateFields) {
        if (options.updateFields.hasOwnProperty(prop)) {
          doc[prop] = options.updateFields[prop];
        }
      }

      try {
        pdb.update({_id: doc._id}, doc, {upsert: true}, (err, numReplaced, upsert) => {
          if (err) {
            return callback(error.itemUpdateError(), 0);
          }

          callback(null, numReplaced);
        });
      } catch (err) {
        return callback(error.itemUpdateError(), 0);
      }
    });
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    const workspaceId = options.workspaceId;
    const dbName = options.dbName;
    let pdb = AppDatabase.getInstance({workspaceId, dbName});

    if (!pdb) {
      return callback(error.noDbConnection(options.dbName), 0);
    }

    if (typeof (data.erised) === "undefined") {
      options.ignoreErised = true;
    }

    let findQuery = options.findParams ? options.findParams : {selector: data};
    try {
      pdb.remove(AppDatabase.getFindQuery(findQuery.selector, options), {multi: true}, (err, numRemoved) => {
        if (err) {
          return callback(err, 0);
        }

        callback(null, numRemoved);
      });
    } catch (err) {
      return callback(err, 0);
    }
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static count(data, options, callback = (err, res) => {
  }) {
    const workspaceId = options.workspaceId;
    const dbName = options.dbName;
    let pdb = AppDatabase.getInstance({workspaceId, dbName});

    if (!pdb) {
      console.log("No database connection", dbName)
      // console.log(error.noDbConnection(dbName))
      return callback(null, 0);
    }

    let findQuery = options.findParams ? options.findParams : {selector: data};
    try {
      pdb.count(AppDatabase.getFindQuery(findQuery.selector, options), (err, count) => {
        if (err) {
          if (config.SHOW_WEB_CONSOLE) {
            console.log("Problem to get db count err & count: ", err, count);
          }
        }
        callback(null, count);
      });
    } catch (err) {
      if (config.SHOW_WEB_CONSOLE) {
        console.log("Problem to get db count err & count: ", err, 0);
      }
      callback(null, 0);
    }
  }

  /**
   * @param {{}} selector
   * @param {{}} options
   */
  static getFindQuery(selector, options) {
    if (selector) {
      let queryMissErisedValue = typeof (selector.erised) === "undefined";
      let queryMissErisedIgnoreValue = typeof (options.ignoreErised) === "undefined";
      if (queryMissErisedValue && queryMissErisedIgnoreValue) {
        selector.erised = {'$ne': true};
      }
    }
    return selector;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    if (data._id) {
      item._id = data._id;
    }

    if (data._rev) {
      item._rev = data._rev;
    }

    return item;
  }

  /**
   * @return {{}}
   */
  static getErasedUpdateFields() {
    return {
      erised: true,
      needSync: true
    };
  }
}
