import fs = require('fs-extra');
import {default as config} from "../../config";

export async function clearPoint(pointPath) {
    try {
        let pointStat = await fs.lstat(pointPath);
        if (pointStat.isFile()) {
            await fs.unlink(pointPath);
        } else if (pointStat.isDirectory()) {
            await clearDirectory(pointPath);
            await fs.rmdir(pointPath);
        }
    } catch (e) {
        if (config.SHOW_WEB_CONSOLE) {
            console.log("Problem: clearPoint => remove file or directory: ", pointPath, e);
        }
    }
}

/**
 * @param {string} dirPath
 */
export function clearDirectory(dirPath) {
    return new Promise(async (resolve) => {
        let files;
        try {
            files = fs.readdirSync(dirPath);
        } catch (e) {
            if (config.SHOW_WEB_CONSOLE) {
                console.log("Error => clearDirectory => clearDirectory: ", dirPath, e);
            }
            return resolve(false);
        }

        if (files.length > 0) {
            for (let i = 0; i < files.length; i++) {
                let filePath = dirPath + '/' + files[i].split('/')[0].split('\\')[0];
                let isFile = false;
                try {
                    isFile = fs.lstatSync(filePath).isFile()
                } catch (e) {
                    console.log("Error => clearDirectory => isFile: ", filePath, e);
                }
                if (isFile) {
                    try {
                        fs.unlinkSync(filePath);
                    } catch (e) {
                        if (config.SHOW_WEB_CONSOLE) {
                            console.log("Error => clearDirectory => remove file: ", filePath, e);
                        }
                        return resolve(true);
                    }
                }
            }
        }

        return resolve(true);
    });
}
