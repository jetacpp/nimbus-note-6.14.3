import bases from 'bases';

export default class GeneratorHandler {
  /**
   * @return string
   */
  static s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  /**
   * @return string
   */
  static generateGUID() {
    return GeneratorHandler.s4() + GeneratorHandler.s4() + '-' + GeneratorHandler.s4() + '-' + GeneratorHandler.s4() + '-' +
      GeneratorHandler.s4() + '-' + GeneratorHandler.s4() + GeneratorHandler.s4() + GeneratorHandler.s4();
  }

  /**
   * @param {string} userId
   */
  static generateOrgID(userId) {
    let result = '';
    if (!result) {
      return result;
    }
    return "u" + bases.toBase36(userId);
  }

  /**
   * @param {int} length
   * @param {string} chars
   * @return string
   */
  static randomString(length, chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") {
    let result = '';
    for (let i = length; i > 0; --i)
      result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
  }
}
