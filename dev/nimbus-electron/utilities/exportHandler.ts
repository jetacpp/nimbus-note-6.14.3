import {app, dialog, shell} from "electron";
import {default as workspace} from "../db/models/workspace";
import * as exportNote from "./export/exportNote";
import * as compressNote from "./export/compressNote";
import syncHandler from "./syncHandler";
import {default as trans} from "../translations/Translations";
import * as path from "path";
import {getFolder, getFolderNotes, getNote} from "./export/utils/find";
import {makeDir, makeTempDir} from "./export/note/process";
import filenamify from 'filenamify';
import {
    clearTitle,
    getExportFileName,
    getItemPath,
    getItemsPath,
    getUniqueTitle,
    getUniqueTitles,
    removePath
} from "./export/utils/file";
import {default as appWindow} from "../window/instance";
import {default as PouchDb} from "../../pdb";
import fs = require('fs-extra');

export enum EXPORT_TYPE {
    NOTE,
    FOLDER,
}

export enum EXPORT_STATUS {
    START,
    PROGRESS,
    FINISH,
    CANCEL,
    ERROR,
}

const DEFAULT_TITLE = {
    'folder': 'New folder',
    'note': 'New note',
}

const activeExport = {};

export const exportHandler = async (appWindow, payload) => {
    const id = payload.id;

    const noteId = payload.noteGlobalId;
    const folderId = payload.folderGlobalId;
    const format = payload.format;

    payload.extension = format === 'html' ? 'zip' : 'pdf';

    try {
        if (noteId) {
            await exportNoteHandler(appWindow, payload);
        } else if (folderId) {
            await exportFolderHandler(appWindow, payload);
        }
    } catch (error) {
        syncHandler.sendLog(error.message);
        clearActiveExport(folderId);
        setExportStatus(folderId, EXPORT_STATUS.ERROR);
    }

    return id;
}

async function exportFolderHandler(appWindow, payload) {
    const id = payload.id;
    const format = payload.format;
    const workspaceId = payload.workspaceId;
    const workspaceGlobalId = await workspace.getLocalId(payload.workspaceId);
    const extension = payload.extension;
    const folderId = payload.folderGlobalId;
    const folders: any[] = payload.folders ? Object.values(payload.folders) : [];

    if (!payload.format || !payload.folderGlobalId) {
        throw new Error(`Problem to make export: bad payload provided`);
    }

    const userTempPath = PouchDb.getClientExportPath()

    let folder;
    if (folderId === 'root') {
        folder = {
            globalId: 'root',
            parentId: null,
            title: 'All Notes',
            childrenNotesCount: 0,
        }
    } else {
        folder = await getFolder(workspaceGlobalId, folderId)
    }

    if (!folder) {
        throw new Error(`Problem to get export folder with folderId: ${folderId} and workspaceId: ${workspaceGlobalId}`);
    }

    let {title} = <{ title: string }>folder;

    title = filenamify(title);
    const {canceled, userSelectedPath} = await exportFolderSaveDialog(appWindow, {title});
    if (canceled || !userSelectedPath) {
        return null;
    }

    const language = payload.language;
    const size = payload.size;
    const style = payload.style;
    const timezone = payload.timezone;

    const totalNotes = folders.reduce((a, b) => {
        return a + (b.childrenNotesCount || 0);
    }, 0);

    let progress = 0;

    setExportStatus(folderId, EXPORT_STATUS.PROGRESS);
    setExportProgress(folderId, totalNotes, progress);

    const exportDir = await makeTempDir(userTempPath, true);
    if (!exportDir) {
        throw new Error(`Problem: can not create export temp dir for path: ${userSelectedPath}`);
    }

    if (typeof activeExport[folderId] !== "undefined") {
        clearActiveExport(folderId)
    }
    activeExport[folderId] = {
        status: EXPORT_STATUS.PROGRESS,
        interval: setInterval(() => {
            setExportProgress(folderId, totalNotes, progress);
        }, 250),
        path: exportDir,
        readStream: null,
        writeStream: null,
    }

    const folderTitles = getUniqueTitles(folders, DEFAULT_TITLE.folder, extension);
    const folderPaths = getItemsPath(folders, folderTitles);

    let baseDirName = '';
    let doneNotes = 0;

    for (const folder of folders) {
        if (!isExportActive(folderId)) {
            return null
        }

        const folderName = getUniqueTitle(folderTitles, folder);
        if (!folderName) {
            throw new Error(`Problem: can not find export dir name: ${folderName} for folder: ${folder.globalId} with title: ${folder.title}`);
        }

        const folderPath = getItemPath(folderPaths, folder);
        if (!isExportActive(folderId)) {
            return null
        }

        const {
            destinationName: folderDirName,
            destinationPath: folderDir,
        } = await makeDir(folderPath, exportDir);

        if (folder.globalId === folderId) {
            baseDirName = folderDirName;
        }

        if (!folderDir) {
            throw new Error(`Problem: can not create export dir: ${folderName} for folder: ${folder.globalId} with title: ${folder.title}`);
        }

        // if(!folder.childrenNotesCount) { continue }
        if (!isExportActive(folderId)) {
            return null
        }

        const notes = <any[]>await getFolderNotes(workspaceGlobalId, folder.globalId);
        const noteTitles = getUniqueTitles(notes, DEFAULT_TITLE.note);

        for (let note of notes) {
            if (!isExportActive(folderId)) {
                return null
            }

            const noteName = getUniqueTitle(noteTitles, note);
            if (!folderName) {
                throw new Error(`Problem: can not find export note name: ${noteName} for note: ${note.globalId} with title: ${note.title}`);
            }

            const noteId = note.globalId;
            const textVersion = note.text_version;

            if (!isExportActive(folderId)) {
                return null
            }

            const dir = await processNote(appWindow, {
                id,
                saveFilePath: folderDir,
                saveFileName: `${noteName}.${extension}`,
                workspaceId,
                noteId,
                format,
                language,
                size,
                style,
                timezone,
                globalId: folderId,
                exportType: EXPORT_TYPE.FOLDER,
                textVersion,
                workspaceGlobalId,
                folderExportDir: exportDir,
            });

            if (!isExportActive(folderId)) {
                return null
            }

            if (!dir) {
                throw new Error(`Problem: processNote return empty dir for note name: ${noteName} for note globalId: ${note.globalId}`);
            }

            doneNotes++;
            progress = totalNotes ? Math.floor(doneNotes * 100 / totalNotes) : 100;
        }
    }

    if (!isExportActive(folderId)) {
        return null
    }

    clearActiveExport(folderId);
    const dirName = exportDir.split(path.sep).pop();
    const outputPath = path.join(userSelectedPath, dirName);

    try {
        // @ts-ignore
        const outputExist = await fs.exists(outputPath)
        if(outputExist) {
            syncHandler.sendLog(`Clear exist ${outputPath}`)
            await clearExportDir(outputPath)
        }
    } catch (error) {
        syncHandler.sendLog(`Problem: to clear ${outputPath} - ${error.message}`)
        syncHandler.sendLog(error)
    }

    syncHandler.sendLog(`Move ${exportDir} into ${outputPath}`);
    try {
        await fs.move(exportDir, outputPath);
    } catch (error) {
        syncHandler.sendLog(`Problem: to move ${exportDir} into ${outputPath} - ${error.message}`);
        clearExportDir(exportDir);
    }
    setExportProgress(folderId, totalNotes, 100, outputPath);
    setExportStatus(folderId, EXPORT_STATUS.FINISH);

    return exportDir;
}

async function exportNoteHandler(appWindow, payload) {
    const id = payload.id;
    const format = payload.format;
    const workspaceId = payload.workspaceId;
    const workspaceGlobalId = await workspace.getLocalId(payload.workspaceId);
    const noteId = payload.noteGlobalId;
    const extension = payload.extension;

    if (!payload.format || !payload.noteGlobalId) {
        throw new Error(`Problem to make export: bad payload provided`);
    }

    const note = await getNote(workspaceGlobalId, noteId);
    if (!note) {
        throw new Error(`Problem to get export note for noteId: ${noteId} and workspaceId: ${workspaceGlobalId}`);
    }

    let {globalId, title, text_version: textVersion} = <{ globalId: string, title: string, text_version: number }>note;
    title = clearTitle(title, DEFAULT_TITLE.note);

    const {canceled, userSelectedPath} = await exportNoteSaveDialog(appWindow, {title, extension});
    if (canceled || !userSelectedPath) {
        return null;
    }

    const userTempPath = PouchDb.getClientExportPath()

    const language = payload.language;
    const size = payload.size;
    const style = payload.style;
    const timezone = payload.timezone;

    const totalNotes = 1;
    let progress = 0;

    setExportStatus(noteId, EXPORT_STATUS.PROGRESS);
    setExportProgress(noteId, totalNotes, 0, '');

    if (typeof activeExport[noteId] !== "undefined") {
        clearActiveExport(noteId)
    }
    activeExport[noteId] = {
        status: EXPORT_STATUS.PROGRESS,
        interval: setInterval(() => {
            setExportProgress(noteId, totalNotes, progress);
        }, 250),
        readStream: null,
        writeStream: null,
    }

    if (!isExportActive(noteId)) {
        return null
    }

    let noteName = userSelectedPath.split(path.sep).pop();
    if(!noteName) { noteName = getExportFileName(title) }
    const saveFileName = `${noteName}.${extension}`

    const exportPath = await processNote(appWindow, {
        id,
        saveFilePath: userTempPath,
        saveFileName,
        workspaceId,
        noteId,
        format,
        language,
        size,
        style,
        timezone,
        globalId: noteId,
        exportType: EXPORT_TYPE.NOTE,
        textVersion,
        workspaceGlobalId,
    });

    if (!isExportActive(noteId, exportPath)) {
        return null
    }

    if (!exportPath) {
        clearExportDir(exportPath);
        throw new Error(`Problem: processNote return empty dir for for note name: ${title} with note globalId: ${globalId}`);
    }

    clearActiveExport(noteId);

    try {
        // @ts-ignore
        const outputExist = await fs.exists(userSelectedPath)
        if(outputExist) {
            syncHandler.sendLog(`Clear exist ${userSelectedPath}`);
            await fs.unlink(userSelectedPath)
        }
    } catch (error) {
        syncHandler.sendLog(`Problem: to clear ${userSelectedPath} - ${error.message}`)
        syncHandler.sendLog(error)
    }

    syncHandler.sendLog(`Move ${exportPath} into ${userSelectedPath}`);
    try {
        await fs.move(exportPath, userSelectedPath);
    } catch (error) {
        syncHandler.sendLog(`Problem: to move ${exportPath} into ${userSelectedPath} - ${error.message}`)
        syncHandler.sendLog(error)
        fs.unlink(exportPath)
    }
    setExportProgress(noteId, totalNotes, 100, userSelectedPath);
    setExportStatus(noteId, EXPORT_STATUS.FINISH);

    return exportPath;
}

async function processNote(appWindow, payload) {
    const {
        id,
        saveFilePath,
        saveFileName,
        workspaceId,
        noteId,
        format,
        language,
        size,
        style,
        timezone,
        globalId,
        exportType,
        textVersion,
        workspaceGlobalId,
        folderExportDir,
    } = payload;

    const exportDir = await makeTempDir(saveFilePath);
    if (!exportDir) {
        throw new Error(`Problem: can not create export temp dir for path: ${saveFilePath}`);
    }

    const exportBaseDir = exportType === EXPORT_TYPE.NOTE ? exportDir : '';
    if (!isExportActive(globalId, exportBaseDir)) {
        return null
    }

    const workerData = await exportNote.run({
        id,
        dir: exportDir,
        workspaceId,
        noteId,
        format,
        language,
        size,
        style,
        timezone,
        globalId,
        exportType,
        textVersion,
        workspaceGlobalId,
    });

    if (!isExportActive(globalId, exportBaseDir)) {
        return null
    }

    if (!workerData) {
        throw new Error(`Problem to generate html container for export with workspaceId: ${workspaceId} and noteId: ${noteId} and format: ${format}`);
    }

    const {dir} = workerData;
    if (!dir) {
        throw new Error(`Problem to get dir from workerData for noteId: ${noteId}`);
    }

    if (exportType === EXPORT_TYPE.NOTE) {
        syncHandler.sendLog(`Export dir: ${dir}`);
    }

    const compressData = await compressNote.run({
        ...workerData,
        saveFilePath,
        saveFileName,
        format,
        globalId,
        folderExportDir: folderExportDir ? folderExportDir : exportDir,
    });

    if (!isExportActive(globalId)) {
        return null
    }

    if (!compressData) {
        clearExportDir(dir);
        throw new Error(`Problem to compress container for export with workspaceId: ${workspaceId} and noteId: ${noteId} and format: ${format}`);
    }

    const { outputFile } = compressData

    await clearExportDir(dir);
    return outputFile;
}

const exportNoteSaveDialog = async (appWindow, options) => {
    const {title, extension} = options
    const filePath = extension ? `${filenamify(title)}.${extension}` : filenamify(title)
    const saveFilePath = `${app.getPath('documents')}/${filePath}`;
    const {canceled, filePath: userSelectedPath} = await dialog.showSaveDialog(appWindow, {
        title: `${trans.get('export_notes_dialog')}`,
        defaultPath: saveFilePath,
        filters: [
            {name: extension, extensions: [extension]},
            {name: 'All Files', extensions: ['*']},
        ],
        showsTagField: false,
        properties: ['createDirectory']
    });
    return {canceled, userSelectedPath}
}

const exportFolderSaveDialog = async (appWindow, options) => {
    let {canceled, filePaths} = await dialog.showOpenDialog(appWindow, {
        title: `${trans.get('export_folder_dialog')}`,
        properties: ['openDirectory', 'createDirectory']
    });
    const userSelectedPath = filePaths.length ? filePaths[0] : ''
    return {canceled, userSelectedPath}
}

export const setExportStatus = (globalId, status) => {
    if (appWindow.get()) {
        appWindow.get().webContents.send('event:export:set:status:requests', {globalId, status});
    }
}

export const setExportProgress = (globalId, total, progress, path = '') => {
    if (appWindow.get()) {
        appWindow.get().webContents.send('event:export:set:progress:requests', {
            globalId,
            total,
            progress,
            path,
        });
    }
}

const clearActiveExport = (globalId) => {
    if (typeof activeExport[globalId] === 'undefined') { return }

    clearInterval(activeExport[globalId].interval);

    if(activeExport[globalId].readStream) {
        syncHandler.sendLog('=> close export read stream')
        activeExport[globalId].readStream.close();
    }

    if(activeExport[globalId].writeStream) {
        syncHandler.sendLog('=> close export write stream')
        activeExport[globalId].writeStream.close();
    }

    if(activeExport[globalId].compressReadStream) {
        syncHandler.sendLog('=> close export compress read stream')
        const compressReadStreamPath = activeExport[globalId].compressReadStreamPath
        activeExport[globalId].compressReadStream.abort();
        if(compressReadStreamPath) {
            try { clearExportDir(compressReadStreamPath) } catch (e) {}
        }
    }

    if(activeExport[globalId].compressWriteStream) {
        syncHandler.sendLog('=> close export compress write stream')
        const compressWritePath = activeExport[globalId].compressWriteStreamPath
        activeExport[globalId].compressWriteStream.close();
        if(compressWritePath) {
            try { clearExportDir(compressWritePath) } catch (e) {}
        }

        const folderExportDir = activeExport[globalId].compressFolderExportDir
        if(folderExportDir) {
            try { clearExportDir(folderExportDir) } catch (e) {}
        }
    }

    clearActiveExportStream(globalId);
    clearActiveCompressStream(globalId);

    delete activeExport[globalId];
}

export const setActiveExportStream = (globalId, readStream, writeStream) => {
    if (typeof activeExport[globalId] === 'undefined') { return }
    activeExport[globalId].readStream = readStream;
    activeExport[globalId].writeStream = writeStream;
}

export const clearActiveExportStream = (globalId) => {
    if (typeof activeExport[globalId] === 'undefined') { return }
    activeExport[globalId].readStream = null;
    activeExport[globalId].writeStream = null;
}

export const setActiveCompressStream = (globalId: string, readStream: any, readStreamPath: string, writeStream: any, writeStreamPath: string, folderExportDir: string) => {
    if (typeof activeExport[globalId] === 'undefined') { return }
    activeExport[globalId].compressReadStream = readStream;
    activeExport[globalId].compressReadStreamPath= readStreamPath;
    activeExport[globalId].compressWriteStream = writeStream;
    activeExport[globalId].compressWriteStreamPath = writeStreamPath;
    if(folderExportDir) {
        activeExport[globalId].compressFolderExportDir = folderExportDir;
    }
}

export const clearActiveCompressStream = (globalId) => {
    if (typeof activeExport[globalId] === 'undefined') { return }
    activeExport[globalId].compressReadStream = null;
    activeExport[globalId].compressWriteStream = null;
    activeExport[globalId].compressReadStreamPath = null;
    activeExport[globalId].compressWriteStreamPath = null;
    activeExport[globalId].compressFolderExportDir = null;
}

export const isExportActive = (globalId, dir = '') => {
    if (typeof activeExport[globalId] === 'undefined') {
        return false
    }
    const {cancel, path} = activeExport[globalId];
    if (cancel) {
        clearActiveExport(globalId);
        const exportDir = dir || path
        if (exportDir) {
            setTimeout(() => clearExportDir(exportDir), 300)
        }
    }
    return !cancel;
}

export const exportCancel = (globalId) => {
    if (!globalId) { return }
    if (typeof activeExport[globalId] === 'undefined') { return }
    activeExport[globalId].cancel = true;
}

export const exportOpenPath = async (path) => {
    await shell.showItemInFolder(path);
}

export const clearExportDir = async dir => {
    await removePath(dir);
}
