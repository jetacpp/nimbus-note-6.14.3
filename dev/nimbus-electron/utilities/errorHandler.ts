import chalk from "chalk";
import {app, dialog} from "electron";
import os = require('os');
import fs = require("fs-extra");
import util = require('util');
import * as Sentry from '@sentry/electron';
import * as readLastLines from 'read-last-lines';
import {default as config} from "../../config";
import {default as NimbusSDK} from "../sync/nimbussdk/net/NimbusSDK";
import {default as appWindowInstance} from "../window/instance";
import {default as dateHandler} from "./dateHandler";
import {default as trans} from "../translations/Translations";
import {default as appWindow} from "../window/instance";
import {default as PouchDb} from "../../pdb";
import {default as API} from "../sync/nimbussdk/net/API";

let logStream;
let stopSaveLog;

export default class ErrorHandler {
  /**
   * @param {Error} err
   */
  static log(err) {
    if (err) {
      if (config.SHOW_WEB_CONSOLE) {
        console.log(err);
      }
    }

    let appWindow = appWindowInstance.get();
    if (appWindow && appWindow.webContents) {
      appWindow.webContents.send('nimbus:electron:log', err);
    }

    if(stopSaveLog) {
      return;
    }

    ErrorHandler.writeSessionLogsToFile(err);
  }

  static async writeSessionLogsToFile(data) {
    if (!data) {
      return;
    }

    if (typeof (data) === "object") {
      data = JSON.stringify(data);
    }

    if (!PouchDb.getClientAttachmentPath()) {
      return;
    }

    const targetPath = `${PouchDb.getClientAttachmentPath()}/${config.logsReportName}`;
    try {
      // @ts-ignore
      if (!await fs.exists(targetPath)) {
        if(logStream) {
          logStream.end();
        }
        logStream = fs.createWriteStream(targetPath, {flags: 'a'});
      }

      if(!logStream) {
        logStream = fs.createWriteStream(targetPath, {flags: 'a'});
      }

      const dateTime = dateHandler.dateYmdHis();
      logStream.write(dateTime + '  ' + util.format(data) + '\n');
    } catch (e) {
      if (config.SHOW_WEB_CONSOLE) {
        console.log("Problem to write into log file", e);
      }
    }
  }

  static async clearSessionLogsStream() {
    ErrorHandler.stopSyncLog();
    const targetPath = `${PouchDb.getClientAttachmentPath()}/${config.logsReportName}`;
    // @ts-ignore
    if (await fs.exists(targetPath)) {
      try {
        await fs.unlink(targetPath);
      } catch (e) {
        if (config.SHOW_WEB_CONSOLE) {
          console.log("Error => clearSessionLogsStream => remove file: ", targetPath, e);
        }
      }
    }
  }

  static stopSyncLog() {
    stopSaveLog = true;
    setTimeout(() => stopSaveLog = false, 5000);
  }

  static sendSessionLogsToNimbus() {
    NimbusSDK.getApi().userLogin(async (err, login) => {
      if (!PouchDb.getClientAttachmentPath()) {
        return;
      }

      let appWindow = appWindowInstance.get();
      const targetPath = `${PouchDb.getClientAttachmentPath()}/${config.logsReportName}`;
      // @ts-ignore
      const targetExist = await fs.exists(targetPath);
      if (appWindow && appWindow.webContents) {
        appWindow.webContents.send('nimbus:electron:log', {
          "logs:path": targetPath,
          "exist": targetExist
        });
      }

      // @ts-ignore
      if (!targetExist) {
        return;
      }

      const email = login ? login : "not authorized";
      try {
        readLastLines.read(targetPath, 100)
          .then((lines) => {
            Sentry.configureScope((scope) => {
              scope.clear();
              scope.setTag('event_id', 'customer:log');
              Sentry.captureMessage(`Electron logs from customer: ${email}`);
              const linesData = lines.split('\n');
              for(let line of linesData) {
                Sentry.addBreadcrumb({
                  category: 'log',
                  message: line,
                  level: Sentry.Severity.Info
                });
              }
            });
          })
          .catch((e) => {
            if (config.SHOW_WEB_CONSOLE) {
              console.log("Error => sendSessionLogsToNimbus => read logs file: ", targetPath, e);
            }
          });
      } catch (e) {
        if (config.SHOW_WEB_CONSOLE) {
          console.log("Error => sendSessionLogsToNimbus => read logs file: ", targetPath, e);
        }
      }
    });
  }

  static requestSendSessionLogs() {
    const dialogOptions = {
      type: "question",
      title: app.name,
      message: `${trans.get('send_logs__confirm_message')}`,
      detail: `${trans.get('send_logs__confirm_details')}`,
      buttons: [
        `${trans.get('send_logs__confirm_btn_cancel')}`,
        `${trans.get('send_logs__confirm_btn_submit')}`
      ],
      defaultId: 1,
      cancelId: 0
    };

    if (appWindow.get()) {
      dialog.showMessageBox(appWindow.get(), dialogOptions).then(({response}) => {
        if (response) {
          ErrorHandler.sendSessionLogsToNimbus();
        }
      });
    }
  }

  static displayErrorPopup(inputData) {
    const {err, response} = inputData;
    if(err === API.ERROR_ACCESS_DENIED) {
      if (appWindow.get()) {
        appWindow.get().webContents.send('event:client:workspace:message:response', {
          message: trans.get('toast__can_not_sync_access_denied'),
          //type: 'danger'
        });
      }
    } else if(response && err === API.ERROR_QUOTA_EXCEED) {
      let translationKey = '';
      let { body } = response;
      if(!body) {
        return;
      }
      const { _errorDesc } = body;
      if(_errorDesc) {
        switch (_errorDesc) {
          case API.DESC_TOO_MANY_NOTES: {
            translationKey = API.DESC_TOO_MANY_NOTES;
            break;
          }
        }
      }

      if(translationKey) {
        appWindow.get().webContents.send('event:client:popup:too_many_notes:response', {});
      }
    }
  }
}
