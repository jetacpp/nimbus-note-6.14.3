import $ = require('cheerio');

export default class TextHandler {
  /**
   * @param {string} str
   * @return string
   */
  static stripTags(str) {
    if (str) {
      str = $.load(str).text().trim();
    }
    return str ? str : '';
  }
}
