import {default as config} from "../../config.runtime";

export default class CustomError {
  /**
   * @param {string} errorMessage
   * @return Error
   */
  static noAuth(errorMessage = "") {
    return CustomError.getError("User not authorized", errorMessage);
  }

  /**
   * @param {string} errorMessage
   * @return Error
   */
  static wrongInput(errorMessage = "") {
    return CustomError.getError("Wrong input data", errorMessage);
  }

  /**
   * @param {string} errorMessage
   * @return Error
   */
  static itemNotFound(errorMessage = "") {
    return CustomError.getError("Model item not found", errorMessage);
  }

  /**
   * @param {string} errorMessage
   * @return Error
   */
  static itemFindError(errorMessage = "") {
    return CustomError.getError("Model item find problem", errorMessage);
  }

  /**
   * @param {string} errorMessage
   * @return Error
   */
  static itemFindAllError(errorMessage = "") {
    return CustomError.getError("Model item find all problem", errorMessage);
  }

  /**
   * @param {string} errorMessage
   * @return Error
   */
  static noDbConnection(errorMessage = "") {
    return CustomError.getError("No database connection", errorMessage);
  }

  /**
   * @param {string} errorMessage
   * @return Error
   */
  static itemSaveError(errorMessage = "") {
    return CustomError.getError("Model item save problem", errorMessage);
  }

  /**
   * @param {string} errorMessage
   * @return Error
   */
  static itemUpdateError(errorMessage = "") {
    return CustomError.getError("Model item update problem", errorMessage);
  }

  /**
   * @param {string} errorMessage
   * @return Error
   */
  static itemRemoveError(errorMessage = "") {
    return CustomError.getError("Model item remove problem", errorMessage);
  }

  /**
   * @param {string} errorMessage
   * @return Error
   */
  static wrongAttachPath(errorMessage = "") {
    return CustomError.getError("Wrong attach path", errorMessage);
  }

  /**
   * @return null
   */
  static noError() {
    return null;
  }

  /**
   * @param {string} errorPrefix
   * @param {string} errorMessage
   * @return Error
   */
  static getError(errorPrefix = "", errorMessage = "") {
    let text = errorMessage ? errorPrefix + ": " + errorMessage : errorPrefix;
    let error = new Error(text);

    if (config.SHOW_WEB_CONSOLE) {
      console.log(error);
    }

    return error;
  }
}
