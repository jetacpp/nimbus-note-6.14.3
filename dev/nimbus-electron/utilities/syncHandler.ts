import {default as config} from "../../config";
import {default as appWindowInstance} from "../window/instance";

let logs = [];

export default class syncHandler {
    static init() {
        logs = [];
    }

    static getLogs() {
        return logs;
    }

    static setLog(log) {
        logs.push({
            date: new Date().toISOString(),
            log,
        });
    }

    static showLogs() {
        let appWindow = appWindowInstance.get();
        if (appWindow && appWindow.webContents) {
            appWindow.webContents.send('nimbus:electron:log', 'Last sync logs:');
            appWindow.webContents.send('nimbus:electron:log', syncHandler.getLogs());
        }
    }

    /**
     * @param {string} log
     * @param {Error} payload
     */
    static log(log, payload = {}) {
        if (!log) { return }

        if (config.SHOW_WEB_CONSOLE) {
            console.log(log);
        }

        syncHandler.setLog(log);
    }

    static sendLog(log) {
        let appWindow = appWindowInstance.get();
        if (appWindow && appWindow.webContents) {
            appWindow.webContents.send('nimbus:electron:log', log);
        }
        if (config.SHOW_WEB_CONSOLE) {
            console.log(log);
        }
    }
}
