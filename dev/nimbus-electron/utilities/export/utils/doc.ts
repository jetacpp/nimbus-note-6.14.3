import * as Y from "yjs";
// @ts-ignore
import * as syncProtocol from 'y-protocols/dist/sync.js';
// @ts-ignore
import * as encoding from 'lib0/dist/encoding.js';
// @ts-ignore
import * as decoding from 'lib0/dist/decoding.js';
import {default as textEditor} from "../../../db/models/textEditor";
import {default as workspace} from "../../../db/models/workspace";

export function getDocName(workspaceId: string, noteGlobalId: string) {
    return workspaceId + '_' + noteGlobalId;
}

export function readAllEntries(docName: string):Promise<[]> {
    return new Promise(async resolve => {
        const data = docName.split('_');
        let workspaceId = data[0];
        let noteGlobalId = data[1];

        workspaceId = workspaceId ? await workspace.getLocalId(workspaceId) : null;

        textEditor.findAll({noteGlobalId}, {workspaceId}, (err, res) => {
            if (err || !res) { return resolve([]); }
            return resolve(res.filter(textInstance => !!textInstance.text).map(textInstance => textInstance.text));
        });
    });
}

export async function getDoc(docName: string):Promise<Y.Doc> {
    const doc = new Y.Doc();
    const buffers = await readAllEntries(docName);
    for (const buf of buffers) {
        syncProtocol.readSyncMessage(decoding.createDecoder(new Uint8Array(_deSerializeBuffer(buf))), encoding.createEncoder(), doc);
    }
    return doc;
}

function _deSerializeBuffer(str) {
    return Buffer.from(str, 'base64')
}
