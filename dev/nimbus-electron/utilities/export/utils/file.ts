import filenamify from "filenamify";
import fs = require('fs-extra');
import syncHandler from "../../syncHandler";
import {default as generator} from "../../generatorHandler";

export const getExportFileName = (title: string): string => {
    return filenamify(title) ? filenamify(title) : generator.randomString(8)
}

export const getUniqueTitles = (items, defaultTitle, escapeExt = '') => {
    const parentToChildren = {}
    for(const item of items) {
        if(typeof parentToChildren[item.parentId] === 'undefined') {
            parentToChildren[item.parentId] = {}
        }

        let itemTitle = item.title
        if(escapeExt) {
            let titleExt = ''
            if(itemTitle.lastIndexOf('.') >= 0) {
                titleExt = itemTitle.substr(itemTitle.lastIndexOf('.') + 1);
            }
            if(titleExt && titleExt === escapeExt) {
                itemTitle += '_';
            }
        }

        let title = clearTitle(itemTitle, defaultTitle);
        parentToChildren[item.parentId][item.globalId] = {
            title,
            counter: 0,
        };
    }

    const childrenName = {}
    for(const parentId in parentToChildren) {
        if(!parentToChildren.hasOwnProperty(parentId)) { continue }

        if(typeof childrenName[parentId] === 'undefined') {
            childrenName[parentId] = {}
        }

        const children = parentToChildren[parentId]
        for(const globalId in children) {
            if(!children.hasOwnProperty(globalId)) { continue }

            const child = children[globalId]
            if(typeof childrenName[parentId][child.title] === 'undefined') {
                childrenName[parentId][child.title] = 0
                continue
            }
            childrenName[parentId][child.title] += 1

            const itemName = `${parentToChildren[parentId][globalId].title}_${childrenName[parentId][child.title]}`
            parentToChildren[parentId][globalId] = {
                title: itemName,
                counter: childrenName[parentId][child.title],
            }
        }
    }

    return parentToChildren
}

export const getItemsPath = (items, titles = {}) => {
    const objs = {}
    if(!items.length) { return objs }

    const list = [...items];

    const root = list.shift();
    if(!root) { return objs }

    const rootTitle = getUniqueTitle(titles, root);
    objs[root.globalId] = {
        ...root,
        path: rootTitle,
    }

    for(const item of list) {
        const parentPath = typeof objs[item.parentId] === 'undefined' ? undefined : objs[item.parentId].path
        objs[item.globalId] = {
            ...item,
            path: `${parentPath}/${getUniqueTitle(titles, item)}`,
        }
    }

    return objs;
}

export const getUniqueTitle = (titles, item) => {
    if(typeof titles[item.parentId] === 'undefined') {
        return ''
    }

    if(typeof titles[item.parentId][item.globalId] === 'undefined') {
        return ''
    }

    if(typeof titles[item.parentId][item.globalId].title === 'undefined') {
        return ''
    }

    return titles[item.parentId][item.globalId].title
}

export const getItemPath = (itemsPath, item) => {
    if(typeof itemsPath[item.globalId] === 'undefined') {
        return ''
    }

    if(typeof itemsPath[item.globalId].path === 'undefined') {
        return ''
    }

    return itemsPath[item.globalId].path
}

export const clearTitle = (title, defaultTitle) => {
    let result = title
    if(result) { result = result.trim() }
    if(result) { result = filenamify(result) }
    if(!result) { result = defaultTitle }
    return result
}

export const removePath = path =>
    new Promise(resolve => {
        try {
            // @ts-ignore
            fs.rmdir(path, { recursive: true }, (error) => {
                if(error) {
                    syncHandler.sendLog(error)
                    return resolve(false)
                }
                resolve(true)
            })
        } catch (error) {
            syncHandler.sendLog(`Problem: can not remove path: ${path} - ${error.message}`)
            resolve(false)
        }
    })

export const ensureDirExistsAndWritable = async dir => {
    // @ts-ignore
    const dirExist = await fs.exists(dir)
    // @ts-ignore
    if (dirExist) {
        try {
            await fs.access(dir, fs.constants.W_OK)
        } catch (error) {
            syncHandler.sendLog(`Cannot access directory ${dir} ${error.message}`)
            return false
        }
    } else {
        try {
            await fs.mkdir(dir)
        } catch (error) {
            if (error.code == 'EACCES') {
                syncHandler.sendLog(`Cannot create directory: ${dir} ${error.message}`)
            } else {
                syncHandler.sendLog(`Cannot create directory: ${error.code} ${error.message}`)
            }
            return false
        }
    }
    return true
}
