export const getFormatDate = () => {
    let date = new Date();
    let year = date.getFullYear();
    let month:string|number = date.getMonth() + 1;
    let day:string|number = date.getDate();
    let hours = date.getHours();
    let minutes:string|number = date.getMinutes();
    let seconds:string|number = date.getSeconds();

    if(month < 10) { month = `0${month}` }
    if(day < 10) { day = `0${month}` }
    if(minutes < 10) { minutes = `0${month}` }
    if(seconds < 10) { seconds = `0${month}` }

    return `${year}-${month}-${day}_${hours}-${minutes}-${seconds}`
}