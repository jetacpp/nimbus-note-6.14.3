import {default as workspace} from "../../../db/models/workspace";
import {default as orgs} from "../../../db/models/orgs";
import {default as item} from "../../../db/models/item";
import {default as attach} from "../../../db/models/attach";

export async function getWorkspace(workspaceId:string) {
    if(workspaceId) {
        return await workspace.getById(workspaceId);
    }
    return await workspace.getDefault();
}

export async function getOrg(orgId:string) {
    return await orgs.getById(orgId);
}

export async function getNote(workspaceId, globalId) {
    return new Promise(resolve => {
        item.find({globalId, type: 'note'}, {workspaceId}, (err, itemInstance) => {
            if (err || !itemInstance) { return resolve(null); }
            return resolve(itemInstance);
        });
    });
}

export async function getFolder(workspaceId, globalId) {
    return new Promise(resolve => {
        item.find({globalId, type: 'folder'}, {workspaceId}, (err, itemInstance) => {
            if (err || !itemInstance) { return resolve(null); }
            return resolve(itemInstance);
        });
    });
}

export async function getFolderNotes(workspaceId, folderId) {
    return new Promise(resolve => {
        item.findAll({parentId: folderId, type: 'note'}, {workspaceId}, (err, itemList) => {
            if (err || !itemList) { return resolve([]); }
            return resolve(itemList);
        });
    });
}

export async function getAttachments(workspaceId, noteGlobalId) {
    return new Promise(resolve => {
        attach.findAll({noteGlobalId}, {workspaceId}, (err, attachments) => {
            if (err || !attachments) { return resolve([]); }
            return resolve(attachments);
        });
    });
}
