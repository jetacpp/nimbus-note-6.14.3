import archiver from 'archiver';
import fs = require('fs-extra');
import syncHandler from "../syncHandler";
import {default as appWindow} from "../../window/instance";
import {BrowserWindow} from "electron";
import {default as config} from "../../../config.runtime";
import {default as childWindow} from "../../window/child";
import * as path from "path";
import {clearActiveCompressStream, isExportActive, setActiveCompressStream} from "../exportHandler";
import {writeExportNoteContent} from "./note/process";

export interface ICompressResponses {
    error: Error|null
    saveFilePath:string
    saveFileName:string
    outputFile: string
    saveFileMime:string
}

export async function run(workerData:any):Promise<ICompressResponses> {
    const { format, dir, title, globalId } = workerData;

    if(!dir) {
        throw new Error(`Problem: no export compress dir path: ${dir}`);
    }

    if(!title) {
        throw new Error(`Problem: no export compress dir name: ${title}`);
    }

    if(!isExportActive(globalId)) {
        return null
    }

    switch (format) {
        case 'html': return await compressToZip(workerData);
        case 'pdf':  return await compressToPdf(workerData);
    }

    return null;
}

async function compressToZip(workerData):Promise<ICompressResponses> {
    const { dir, title, saveFilePath, saveFileName } = workerData;

    syncHandler.sendLog(`=> compressToZip => dir: ${dir} and title: ${title}`);

    let outputFile, error
    try {
        outputFile = await zipNote(workerData);
    } catch (err) { error = err }

    return {
        error,
        saveFilePath,
        saveFileName,
        outputFile,
        saveFileMime: 'application/zip',
    }
}

async function zipNote(workerData):Promise<string> {
    return new Promise((resolve, reject) => {
        const { dir, saveFilePath, saveFileName, globalId, folderExportDir } = workerData;
        const archive = archiver('zip', { zlib: { level: 9 } });
        archive.directory(dir, false);

        const outputFile = path.join(saveFilePath, saveFileName);
        const output = fs.createWriteStream(outputFile);

        output.on('close', () => {
            clearActiveCompressStream(globalId);
            resolve(outputFile)
        });
        archive.on('error', error => {
            if(error.message !== 'archive was aborted') {
                archive.removeAllListeners();
                try {
                    fs.unlink(outputFile);
                } catch (error) { syncHandler.sendLog(error) }
                return resolve(null);
            } else {
                archive.abort();
            }

            syncHandler.sendLog(error);
            reject(error);
        });

        setActiveCompressStream(globalId, archive, dir, output, outputFile, folderExportDir);

        archive.pipe(output);
        archive.finalize();
    })
}

function compressToPdf(workerData):Promise<ICompressResponses> {
    return new Promise(async (resolve, reject) => {
        const { dir, saveFilePath, saveFileName, globalId } = workerData;

        syncHandler.sendLog(`=> compressToPdf => dir: ${dir} and saveFilePath: ${saveFilePath}`);

        const noteFileUrl = `${dir}/note.html`;

        const exportWindow = getExportWindow();
        const filePath = `file://${noteFileUrl}`;
        await exportWindow.loadURL(filePath);

        let buf = await exportWindow.webContents.printToPDF({
            printBackground: false,
            printSelectionOnly: false,
            landscape: false,
            pageSize: 'A4',
        });

        setTimeout(() => {
            exportWindow.close();
        }, 750);

        if(!isExportActive(globalId)) {
            buf = null;
            return resolve(null);
        }

        const outputFile = path.join(saveFilePath, saveFileName);

        const response = {
            saveFilePath,
            saveFileName,
            outputFile,
            saveFileMime: 'application/pdf',
        }

        try {
            await writeExportNoteContent(globalId, outputFile, buf);
            return resolve({
                error: null,
                ...response,
            });
        } catch (error) {
            syncHandler.sendLog(error);
            return reject(error);
        }
    });
}

function getExportWindow() {
    let exportWindow = new BrowserWindow({
        parent: appWindow.get(),
        show: false,
        width: 600,
        height: 600,
        x: 0,
        y: 0,
        useContentSize: true,
        icon: `${config.applicationIconPath}`,
        resizable:  false,
        minimizable: false,
        skipTaskbar: true,
        webPreferences: {
            devTools: false,
            nodeIntegration: false,
            allowRunningInsecureContent: false,
            plugins: false,
        }
    });
    exportWindow.setMenu(null);

    childWindow.set(childWindow.TYPE_EXPORT, exportWindow);
    exportWindow.once('close', () => {
        childWindow.set(childWindow.TYPE_EXPORT, null);
    });

    return exportWindow;
}
