import * as Y from "yjs";
import path = require('path');
import fs = require('fs-extra');
import mime = require('mime');
import {getAttachments, getNote, getOrg, getWorkspace} from "../utils/find";
import {default as PouchDb} from "../../../../pdb";
import config from "../../../../config";
import {default as workspace} from "../../../db/models/workspace";
import {getNoteHtml} from "./html";
import {getFormatDate} from "../utils/date";
import syncHandler from "../../syncHandler";
import {clearActiveExportStream, isExportActive, setActiveExportStream} from "../../exportHandler";
import {default as generator} from "../../generatorHandler";
import {ensureDirExistsAndWritable} from "../utils/file";
import {mkdir} from "fs-extra";

export async function processDoc(doc: Y.Doc, settings: any) {
    const {
        dir,
        workspaceId,
        noteId,
        textVersion,
        globalId,
    } = settings;

    const workspaceGlobalId = await workspace.getLocalId(workspaceId);

    const workspaceInstance = <any>await getWorkspace(workspaceGlobalId);
    const orgInfo = <any>await getOrg(workspaceInstance.orgId);
    const note = <any>await getNote(workspaceGlobalId, noteId);
    const attachments = <[any]>await getAttachments(workspaceGlobalId, noteId);

    const domain = orgInfo && orgInfo.domain ? orgInfo.domain : config.WEBNOTES_DOMAIN;
    const title = note.title;

    const attachmentsByGlobalId = {};
    for (const attach of attachments) {
        attach.global_id = attach.globalId;
        attach.display_name = attach.displayName;
        attachmentsByGlobalId[attach.globalId] = attach;
    }

    const { html, context } = await getNoteHtml({
        ...settings,
        title,
        domain,
        workspaceId,
    }, doc, attachmentsByGlobalId);

    const distPath = path.join(config.ASAR_APP_DIR, '/node_modules/@nimbus/editor-electron/dist');

    await writeExportNoteContent(globalId, path.join(dir, '/note.html'), html);

    if(textVersion > 1) {
        try {
            await fs.copy(path.join(distPath, '/fonts'), path.join(dir, '/assets/fonts'));
            await fs.copy(path.join(distPath, '/export_theme.css'), path.join(dir, '/assets/theme.css'));
        } catch (err) {
            syncHandler.sendLog(`Problem: export assets fonts + theme copy error: ${err.message}`);
        }
    } else {
        if(Object.keys(context.attachments).length) {
            await mkdir(path.join(dir, '/assets'));
        }
    }

    console.info('Count editor attachments to copy %d', Object.keys(context.attachments).length);

    if(!context.attachments) { context.attachments = {} }
    for(let attachment of attachments) {
        if(!attachment.inList) { continue }
        context.attachments[attachment.globalId] = attachment.globalId
    }

    for (const attachmentGlobalId of Object.keys(context.attachments)) {
        if(!isExportActive(globalId)) {
            return {
                dir,
                title,
            }
        }

        if(typeof attachmentsByGlobalId[attachmentGlobalId] === 'undefined') { continue }
        console.info('Copy attach %s', attachmentGlobalId);
        const attach = attachmentsByGlobalId[attachmentGlobalId];
        if (attach) {
            const storedFileUUID = attach.storedFileUUID;
            let extension = mime.getExtension(attach.mime);
            extension = extension ? `.${extension}` : '';
            const attachDestPath = `${dir}/assets/${attach.global_id}${extension}`;
            await copyAttach(globalId, storedFileUUID, attachDestPath);
        }
    }

    return {
        dir,
        title,
    };
}

const writeStreamResolveFn = (globalId, resolve) => {
    clearActiveExportStream(globalId);
    resolve();
}

const copyAttach = async (globalId: string, storedFileUUID:string, toPath:string) => {
    const fromPath = `${PouchDb.getClientAttachmentPath()}/${storedFileUUID}`;
    return await copyExportNoteFile(globalId, fromPath, toPath);
}

const copyExportNoteFile = async (globalId: string, fromPath: string, toPath:string) => {
    return new Promise(resolve => {
        const input = fs.createReadStream(fromPath);
        const output = fs.createWriteStream(toPath);

        input.on('error', () => writeStreamResolveFn(globalId, resolve));
        output.on('error', () => writeStreamResolveFn(globalId, resolve));
        output.on('close', () => writeStreamResolveFn(globalId, resolve));

        setActiveExportStream(globalId, input, output);

        input.pipe(output);
    });
}

export const writeExportNoteContent = async (globalId: string, toPath:string, content: string | Buffer) => {
    return new Promise(resolve => {
        const output = fs.createWriteStream(toPath);

        output.on('error', () => writeStreamResolveFn(globalId, resolve));
        output.on('close', () => writeStreamResolveFn(globalId, resolve));

        setActiveExportStream(globalId, null, output);

        output.write(content);
        output.end();
    });
}

export const makeDir = async (title:string, exportDir = null) => {
    const clientPath = PouchDb.getClientExportPath();
    const dirPath = exportDir ? exportDir : clientPath;
    const destinationName = title;
    const destinationPath = path.join(dirPath, destinationName);
    try {
        await fs.mkdir(destinationPath);
        return {
            destinationName,
            destinationPath,
        };
    } catch (error) {
        syncHandler.sendLog(error);
        return {
            destinationName: '',
            destinationPath: null,
        };
    }
}

export const makeTempDir = async (basePath = '', folder = false) => {
    basePath = folder ? basePath : path.dirname(basePath);
    const clientPath = basePath ? basePath : PouchDb.getClientExportPath();

    const isWriteable = await ensureDirExistsAndWritable(clientPath);
    if(!isWriteable) {
        return null;
    }

    try {
        if(folder) {
            const name = `export-${getFormatDate()}`;
            const destinationPath = path.join(clientPath, name);
            await fs.mkdir(destinationPath);
            return destinationPath;
        }
        const name = `export-${generator.randomString(8)}`;
        const destinationPath = path.join(clientPath, name);
        await fs.mkdir(destinationPath);
        return destinationPath;
    } catch (error) {
        syncHandler.sendLog(error);
        return null;
    }
}
