import * as Y from "yjs";
import jsdom = require('jsdom');
import Storage = require('dom-storage');
import mime = require('mime');
// @ts-ignore
import {AttachmentNotFoundError, initEditor, ToHTMLContext} from "@nimbus/editor-electron/dist/editor-node";
import {default as text} from "../../../db/models/text";

const googleFonts = ['IBMPlexSans-Roboto', 'RobotoSlab', 'Caveat', 'AnonymousPro', 'Inconsolata'];

class ResizeObserver {
    observe () {}
    disconnect () {}
}

export const getNoteHtml = async (settings, doc, attachmentsByGlobalId) => {
    const { textVersion } = settings;
    if(textVersion > 1) {
        return await getNewEditorHtml(settings, doc, attachmentsByGlobalId);
    }
    return await getOldEditorHtml(settings, attachmentsByGlobalId);
}

const getHost = (url: string) => url.replace(/https?:\/\//i, '').split('/')[0]
const httpGetRequest = (url: string) => {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url)
        xhr.onloadend = (e) => {
            try {
                let response = JSON.parse(xhr.response);
                resolve(response.body)
            } catch (error) {
                reject(error)
            }
        }
        xhr.onerror = (error) => { reject(error) }
        xhr.send(null)
    })
}

const getNewEditorHtml = async (settings, doc, attachmentsByGlobalId) => {
    const {
        language,
        timezone,
        style,
        size,

        pdf,

        noteUrlTemplate,
        folderUrlTemplate,
        workspaceUrlTemplate,

        title,
        domain,
        workspaceId,
    } = settings;

    const { JSDOM } = jsdom;

    const dom = new JSDOM(`<!DOCTYPE html><div id="note-editor"></div>`);
    const { window } = dom;

    // @ts-ignore
    global['navigator'] = {
        userAgent: 'Chrome'
    };
    global['document'] = dom.window.document;
    global['window'] = dom.window;
    const setToGlobal = [
        'Node', 'Text', 'HTMLElement', 'MutationObserver', 'Image'
    ];
    for (const k of setToGlobal) {
        global[k] = window[k];
    }
    global['localStorage'] = new Storage(null, {strict: false});
    global['ResizeObserver'] = ResizeObserver

    const editor = await initEditor({
        y: doc,
        Y,
        mode: 'export',
        plugins: {
            i18n: {
                language
            },
            blockMenu: {
                enabled: false
            },
            embeddedMenu: {
                enabled: false
            },
            attachmentManager: {
                resolve: (attachmentGlobalId:string) => {
                    console.info('Resolving attach %s', attachmentGlobalId)
                    if (!attachmentsByGlobalId[attachmentGlobalId]) {
                        return Promise.reject(new AttachmentNotFoundError('attachment ' + attachmentGlobalId + ' not found'))
                    }
                    return Promise.resolve(attachmentsByGlobalId[attachmentGlobalId])
                }
            },
            bookmarkManager: {
                getPreviews: url => {
                    return httpGetRequest(`https://everhelper.me/sdpreviews/listing.php?p=0&order=best&host=${getHost(url)}&ag=1`).catch(err => {
                        return 'bookmark.error.unknown_error'
                    })
                },
                getFavicon: url => {
                    return httpGetRequest(`https://www.google.com/s2/favicons?domain=${url}`).catch(err => {
                        return 'bookmark.error.unknown_error'
                    })
                },
                iframelyHost: 'https://iframely.nimbusweb.me',
                iframelyPort: null,
                extendedMetaRequest: true
            }
        },
        blots: {
            date: {
                timezone,
            }
        },
        editorStyle: { style, size }
    });

    const mentionUrlTemplates = {
        note: noteUrlTemplate({
            domain,
            workspaceId,
        }),
        folder: folderUrlTemplate({
            domain,
            workspaceId,
        }),
        workspace: workspaceUrlTemplate({
            domain,
        }),
    };

    const context = new ToHTMLContext({mentionUrlTemplates, pdf});
    let html = editor.toHTML(context);

    editor.destroy();

    if(typeof global['document'] !== 'undefined') {
        delete global['document']
    }

    if(typeof global['window'] !== 'undefined') {
        delete global['window']
    }

    if(typeof global['navigator'] !== 'undefined') {
        delete global['navigator']
    }

    if(typeof global['localStorage'] !== 'undefined') {
        delete global['localStorage']
    }

    if(typeof global['ResizeObserver'] !== 'undefined') {
        delete global['ResizeObserver']
    }

    if(pdf) {
        html = html.replace(new RegExp("<iframe[^>]+>.*?</iframe>", "g"), "");
    }

    html = `<!doctype html>
    <html lang="en">
        <head>
            <title>${title}</title>
            <meta charset="utf-8">
            <link rel="stylesheet" type="text/css" href="./assets/theme.css">
            <link rel="stylesheet" type="text/css" href="./assets/fonts/fonts.css">
            ${googleFonts.map(font => `<link rel="stylesheet" href="./assets/fonts/google-fonts/${font}.css">`).join('\n')}
        </head>
        <body>
            ${html}
        </body>
    </html>`;

    return {
        html,
        context,
    };
}

const getOldEditorHtml = async (settings, attachmentsByGlobalId) => {
    const {
        noteId,

        title,
        workspaceGlobalId,
    } = settings;

    let html = <string>await text.getTextAsync(noteId, workspaceGlobalId);
    let regexp = new RegExp('#attacheloc:(.*)#', 'ig');
    let attachIdList = html.match(regexp);
    if(!attachIdList) { attachIdList = [] }

    const attachments = {}
    for(let attachId of attachIdList) {
        const globalId = attachId.substring(12, attachId.length - 1)
        if(typeof attachmentsByGlobalId[globalId] === 'undefined') { continue }

        const attach = attachmentsByGlobalId[globalId];
        let extension = mime.getExtension(attach.mime);
        extension = extension ? `.${extension}` : '';
        const attachPath = `assets/${attach.globalId}${extension}`;
        html = html.replace(attachId, attachPath);

        attachments[globalId] = globalId;
    }

    html = (""+html).replace(/<a\s+href=/gi, '<a target="_blank" href=');

    html = `<!doctype html>
    <html lang="en">
        <head>
            <title>${title}</title>
            <meta charset="utf-8">
        </head>
        <body>
            ${html}
        </body>
    </html>`;

    return {
        html,
        context: {
            attachments,
        },
    };
}
