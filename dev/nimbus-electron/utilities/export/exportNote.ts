import {getDoc, getDocName} from "./utils/doc";
import {processDoc} from "./note/process";
import template from "lodash.template";
import syncHandler from "../syncHandler";

const config = {
    export: {
        noteUrlTemplate: 'https://<%= domain %>/ws/<%= workspaceId %>/{id}',
        folderUrlTemplate: 'https://<%= domain %>/ws/<%= workspaceId %>/folder/{id}',
        workspaceUrlTemplate: 'https://<%= domain %>/ws/{id}',
        chromeHost: 'localhost',
        sharedDir: '/shared-data'
    },
};

export async function run(workerData:any) {
    let { language, timezone, style, size } = workerData;
    const { dir, workspaceId, noteId, format } = workerData;
    const { globalId, exportType, textVersion, workspaceGlobalId, exportDir } = workerData;

    syncHandler.sendLog(`=> export => note globalId: ${globalId} and textVersion: ${textVersion}`)

    language = language || 'en';
    timezone = !isNaN(parseInt(timezone)) ? parseInt(timezone) : new Date().getTimezoneOffset();
    style = style || 'default';
    size = size || 'normal';

    const pdf = format === 'pdf';

    const noteUrlTemplate = template(config.export.noteUrlTemplate);
    const folderUrlTemplate = template(config.export.folderUrlTemplate);
    const workspaceUrlTemplate = template(config.export.workspaceUrlTemplate);

    const docName = getDocName(workspaceId, noteId);
    const doc = textVersion > 1 ? await getDoc(docName) : null;

    const settings = {
        dir,
        workspaceId,
        noteId,

        language,
        timezone,
        style,
        size,

        pdf,

        noteUrlTemplate,
        folderUrlTemplate,
        workspaceUrlTemplate,

        globalId,
        exportType,
        textVersion,
        workspaceGlobalId,
        exportDir,
    };

    return await processDoc(doc, settings);
}
