import parsed = require('url-parse');

export default class UrlParser {
  /**
   * @param url String
   * @return {}
   */
  static getDetails(url) {
    return parsed(url.trim().replace(/\+/gi, " ").replace(/%2B/gi, "+"), true, null);
  }

  /**
   * @param {string} url
   * @return string
   */
  static getAttachmentRemoteUri(url) {
    if (!url) {
      return "";
    }

    let lastSlashIndex = url.lastIndexOf("/");
    let path = url.slice(0, lastSlashIndex + 1);
    if (!path) {
      return "";
    }

    let fileName = url.slice(lastSlashIndex + 1);
    if (!fileName) {
      return "";
    }

    return path + encodeURIComponent(fileName);
  }

  /**
   * @param {string} str
   */
  static escapeRegExp(str) {
    return str.trim().replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
  }

  /**
   * @param {string} url
   * @return {}
   */
  static getQueryParams(url) {
    let urlData = UrlParser.getDetails(url);
    return urlData && urlData.query ? urlData.query : {};
  }

  /**
   * @param {string} url
   * @return {}
   */
  static getFilterParams(url) {
    let result = {};

    let query = UrlParser.getQueryParams(url);
    if (query && Object.keys(query).length && query.filter) {
      let filterQuery = JSON.parse(query.filter);
      if (filterQuery && Object.keys(filterQuery).length)
        result = filterQuery;
    }

    return result;
  }

  /**
   * @param {string} url
   * @return {}
   */
  static getLimitParams(url) {
    let result = {};

    let query = UrlParser.getQueryParams(url);
    if (query && Object.keys(query).length && query.range) {
      let rangeQuery = JSON.parse(query.range);
      if (rangeQuery && Object.keys(rangeQuery).length)
        result = rangeQuery;
    }

    return result;
  }

  /**
   * @param {string} url
   * @return {}
   */
  static getOrderParams(url) {
    let result = {};

    let query = UrlParser.getQueryParams(url);
    if (!query) {
      return result;
    }

    if (!Object.keys(query).length) {
      return result;
    }

    if (!query.order) {
      return result;
    }

    let orderQuery = JSON.parse(query.order);
    if (!orderQuery) {
      return result;
    }

    if ((orderQuery) instanceof Array) {
      if (!orderQuery.length) {
        return result;
      }

      let orderParams = orderQuery[0];
      if (!orderParams.length) {
        return result;
      }

      return orderParams;
    } else if ((orderQuery) instanceof Object) {
      if (!Object.keys(orderQuery).length) {
        return result;
      }

      result = orderQuery;
    }

    return result;
  }

  /**
   * @param {string} url
   * @return []
   */
  static getPathParams(url) {
    let urlData = UrlParser.getDetails(url);
    return urlData.pathname.split('/');
  }

  /**
   * @param {{}} options
   * @returns {}
   */
  static getSortAndLimitParams(options) {
    options = options || {};

    let orderField = "";
    let orderDirection = 1;
    let offsetValue = 0;
    let limitValue = 0;

    if (options.order instanceof Array) {
      if (typeof(options.order[0]) !== "undefined" && options.order[0])
        orderField = options.order[0];
      if (typeof(options.order[1]) !== "undefined" && options.order[1] === "DESC")
        orderDirection = -1;
    }

    if (options.range) {
      if (typeof(options.range.offset) !== "undefined" && options.range.offset > 0)
        offsetValue = options.range.offset;
      if (typeof(options.range.limit) !== "undefined" && options.range.limit > 0)
        limitValue = options.range.limit;
    }

    let sortQuery = {};
    if (orderField && orderDirection)
      sortQuery[orderField] = orderDirection;

    return {
      'orderField': orderField,
      'orderDirection': orderDirection,
      'offsetValue': offsetValue,
      'limitValue': limitValue,
      'sortQuery': sortQuery
    };
  }

  static getWorkspaceIdFromUrl(url) {
    if(!url) { return null; }
    const data = url.split('/');
    return typeof (data[5]) !== 'undefined' && data[5] ? data[5] : null;
  }
}
