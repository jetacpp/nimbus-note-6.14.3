export default class DateHandler {
  /**
   * @param {int} date
   * @returns {number}
   */
  static now(date = null) {
    return Math.floor((date ? new Date(date) : new Date()).getTime() / 1000);
  }

  /**
   * @param {int} date
   * @returns {number}
   */
  static msec(date = null) {
    return Math.floor((date ? new Date(date) : new Date()).getTime());
  }

  /**
   * @return string
   */
  static dateYmdHis() {
    const today = new Date();
    let dd = <any>today.getDate();
    let mm = <any>today.getMonth() + 1;
    const yyyy = today.getFullYear();

    let hh = <any>today.getHours();
    let mn = <any>today.getMinutes();
    let ss = <any>today.getSeconds();

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }

    if (hh < 10) {
      hh = '0' + hh
    }

    if (mn < 10) {
      mn = '0' + mn
    }

    if (ss < 10) {
      ss = '0' + ss
    }

    return yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + mn + ':' + ss;
  }

  static setBigTimeout(callback, delay) {
    let maxDelay = Math.pow(2,31)-1;

    if (delay > maxDelay) {
      let args = <any>arguments;
      args[1] -= maxDelay;

      return setTimeout(() => {
        DateHandler.setBigTimeout.apply(undefined, args);
      }, maxDelay);
    }

    return setTimeout.apply(undefined, <any>arguments);
  }
}

