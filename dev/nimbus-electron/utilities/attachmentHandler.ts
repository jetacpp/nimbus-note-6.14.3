import fs = require('fs-extra');
import mimeTypes = require("mime-types");
import {default as attach} from "../db/models/attach";
import {default as PouchDb} from "../../pdb";
import {default as workspace} from "../db/models/workspace";
import syncHandler from "./syncHandler";
import {default as SelectedWorkspace} from "../workspace/SelectedWorkspace";
import {default as apiAttachExist} from "../request/interceptors/jsonInterceptors/attach/apiAttachExist";
import filenamify from "filenamify";
import {default as errorHandler} from "./errorHandler";
import {shell} from "electron";
import path from "path";
import {default as appWindowInstance} from "../window/instance";
import {default as trans} from "../translations/Translations";

const ALLOWED_AUDIO_EXTENSIONS = [
    ".3gp", ".aa", ".aac", ".aax", ".act", ".aiff", ".alac", ".amr", ".ape", ".au", ".awb", ".dct", ".dss", ".dvf", ".flac",
    ".gsm", ".iklax", ".ivs", ".m4a", ".m4b", ".m4p", ".mmf", ".mp3", ".mpc", ".msv", ".nmf", ".ogg", ".oga", ".mogg",
    ".opus", ".ra, .rm", ".raw", ".rf64", ".sln", ".tta", ".voc", ".vox", ".wav", ".wma", ".wv", ".webm", ".8svx", ".cda",
]

export default class AttachmentHandler {
    static async eraseList(inputData) {
        let { workspaceId, noteId, attachments } = inputData;

        if(typeof (workspaceId) === 'undefined') { return }
        if(!noteId) { return }
        if(!attachments) { return }
        if(!attachments.length) { return }
        workspaceId = workspaceId ? await workspace.getLocalId(workspaceId) : null;

        attach.findAll({
            globalId: {$in: attachments},
            noteGlobalId: noteId,
        }, {workspaceId}, async (err, attachItems) => {
            if(err || !attachItems) { return }

            for(let attachItem of attachItems) {
                attach.count({globalId: attachItem.globalId}, {workspaceId}, async (err, count) => {
                    if(count === 1) {
                        attach.erase({globalId: attachItem.globalId}, {workspaceId}, async () => {
                            if (!PouchDb.getClientAttachmentPath()) { return }
                            const targetPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
                            syncHandler.log('=> remove attach targetPath?', targetPath);

                            // @ts-ignore
                            let exists = await fs.exists(targetPath);
                            // @ts-ignore
                            if (exists) {
                                try {
                                    fs.unlink(targetPath, () => {
                                    });
                                } catch (e) {}
                            }

                            const previewPath = `${targetPath}-preview`;
                            // @ts-ignore
                            let previewExist = await fs.exists(previewPath);
                            // @ts-ignore
                            if (previewExist) {
                                try {
                                    fs.unlink(previewPath, () => {
                                    });
                                } catch (e) {}
                            }
                        });
                    }
                });
            }
        });
    }

    static async playAudioAttachment(inputData) {
        let { attach } = inputData;

        const workspaceId = await SelectedWorkspace.getGlobalId();
        let nimbusAttachmentExist = await apiAttachExist.interceptRequestByGlobalIdAsync({
            workspaceId,
            globalId: attach.globalId
        })

        if (!nimbusAttachmentExist) {
            return AttachmentHandler.displayAttachSyncInProgressNotification();
        }

        if(!PouchDb.getClientAttachmentPath()) { return }
        if(!PouchDb.getClientTempAttachmentPath()) { return }

        let mime = attach.mime.split('/')
        let ext = mime.length >= 2 ? mime[1] : ''
        let displayName = filenamify(attach.displayName)


        const nameExtParts = displayName ? displayName.split('.') : []
        const nameExt = nameExtParts.length >= 2 ? `.${nameExtParts[nameExtParts.length - 1]}` : ''
        const nameContainAllowedExt = nameExt && ALLOWED_AUDIO_EXTENSIONS.indexOf(nameExt) >= 0
        if(nameContainAllowedExt) {
            displayName = `${displayName}`
        }

        const findExtByMime = mimeTypes.extension(ext) ? `.${mimeTypes.extension(ext)}` : ''
        const mimeContainExt = findExtByMime && ALLOWED_AUDIO_EXTENSIONS.indexOf(findExtByMime) >= 0
        if(!nameContainAllowedExt && mimeContainExt) {
            displayName = `${displayName}.${findExtByMime}`
        }

        const nameContainExtAsMime = displayName.lastIndexOf(`.${ext}`) === (displayName.length - (ext.length + 1))
        if(!nameContainAllowedExt && !mimeContainExt && !nameContainExtAsMime) {
            displayName = `${displayName}.${ext}`
        }

        let targetPath = `${PouchDb.getClientAttachmentPath()}/${attach.storedFileUUID}`
        let destPath = displayName && ext ? `${PouchDb.getClientTempAttachmentPath()}/${displayName}` : null

        // @ts-ignore
        let exists = await fs.exists(targetPath);
        // @ts-ignore
        if (!exists) {
            errorHandler.log(`event:editor:open:attach:request => file not exist by path: ${targetPath}`)
            return
        }

        if(!destPath) {
            errorHandler.log(`event:editor:open:attach:request => file has problem with name: '${displayName}' or mime type: ${attach.mime} and can not be open by path ${targetPath}`)
            return
        }

        let filenames = []
        try {
            filenames = await fs.readdir(PouchDb.getClientTempAttachmentPath())
        } catch (err) {
            errorHandler.log(err)
        }

        try {
            await fs.copy(targetPath, destPath)
            shell.openItem(destPath)

            setTimeout(() => {
                filenames.map(filename => {
                    if(filename.indexOf('.') > 0 && filename !== displayName) {
                        fs.unlink(path.join(PouchDb.getClientTempAttachmentPath(), filename))
                    }
                })
            }, 1000)
        } catch (err) {
            errorHandler.log(err)
        }
    }

    static displayAttachSyncInProgressNotification() {
        if (appWindowInstance.get()) {
            appWindowInstance.get().webContents.send('event:display:angular:notification:requests', {
                data: {
                    text: trans.get('notice_sync_is_not_finished')
                },
                id: 'nimbus_angular_notice',
                settings: {
                    position: 'bottom-right'
                }
            });
        }
    };
}
