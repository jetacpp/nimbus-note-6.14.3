import chalk from "chalk";

export function setUpdateProps(saveProps, curDate) {
  for(let saveKey in saveProps) {
    if(!saveProps.hasOwnProperty(saveKey)) { continue }
    if(saveKey && saveKey.indexOf('UpdateTime') > 0) { continue }
    saveProps[`${saveKey}UpdateTime`] = curDate;
  }
  return saveProps;
}

export function propOutDate(props, existObj, fieldsUpdateDates) {
  const { existNoteObjProp, serverNoteObjProp } = props;
  if(!existObj || !fieldsUpdateDates || !existNoteObjProp || !serverNoteObjProp) {
    return true;
  }

  const propName = `${existNoteObjProp}UpdateTime`;
  if(typeof (existObj[propName]) === 'undefined') {
    let noteFieldsUpdateDatesPropExist = true;
    if(!fieldsUpdateDates) {
      noteFieldsUpdateDatesPropExist = false;
    }

    if(noteFieldsUpdateDatesPropExist && typeof (fieldsUpdateDates[serverNoteObjProp]) === 'undefined') {
      noteFieldsUpdateDatesPropExist = false;
    }

    if(noteFieldsUpdateDatesPropExist && !fieldsUpdateDates[serverNoteObjProp]) {
      noteFieldsUpdateDatesPropExist = false;
    }

    if(noteFieldsUpdateDatesPropExist && typeof (fieldsUpdateDates[serverNoteObjProp]['updatedAt']) === 'undefined') {
      noteFieldsUpdateDatesPropExist = false;
    }

    if(noteFieldsUpdateDatesPropExist && !fieldsUpdateDates[serverNoteObjProp]['updatedAt']) {
      noteFieldsUpdateDatesPropExist = false;
    }

    return noteFieldsUpdateDatesPropExist;
  }

  const existPropDate = existObj[propName];
  if(!existPropDate) {
    return true;
  }

  if(typeof (fieldsUpdateDates[serverNoteObjProp]) === 'undefined') {
    return false;
  }

  if(typeof (fieldsUpdateDates[serverNoteObjProp]['updatedAt']) === 'undefined') {
    return false;
  }

  const serverPropDate = fieldsUpdateDates[serverNoteObjProp]['updatedAt'];
  if(!serverPropDate) {
    return false;
  }

  return existPropDate < serverPropDate;
}

export function propPreviewOutDate(props, existObj, note) {
  const { existNoteObjProp } = props;

  if(!existObj || !note || !existNoteObjProp) {
    return true;
  }

  const propName = `${existNoteObjProp}UpdateTime`;
  if(typeof (existObj[propName]) === 'undefined') {
    return true;
  }

  const existPropDate = existObj[propName];
  if(!existPropDate) {
    return true;
  }

  const serverPropDate = note.preview && note.preview.date_updated ? note.preview.date_updated : note.date_updated;
  if(!serverPropDate) {
    return true;
  }

  return existPropDate < serverPropDate;
}

export function propReminderOutDate(props, existObj, note) {
  const { existNoteObjProp } = props;

  if(!existObj || !note || !existNoteObjProp) {
    return true;
  }

  const propName = `${existNoteObjProp}UpdateTime`;
  if(typeof (existObj[propName]) === 'undefined') {
    return true;
  }

  const existPropDate = existObj[propName];
  if(!existPropDate) {
    return true;
  }

  const serverPropDate = note.date_updated;
  if(!serverPropDate) {
    return true;
  }

  return existPropDate < serverPropDate;
}
