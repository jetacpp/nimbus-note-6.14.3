import chalk from "chalk";
import {dialog, BrowserWindow} from 'electron';
import {autoUpdater} from 'electron-updater';
import {default as errorHandler} from "./errorHandler";
import {default as trans} from "../translations/Translations";
import {default as appOnlineState} from "../online/state";
import {default as config} from "../../config.runtime";
import {default as appWindow} from "../window/instance";
import {default as childWindow} from "../window/child";
import {default as appPage} from "../request/page";

let updater;
let updateInfo;
let showError;
autoUpdater.autoDownload = false;

/**
 * @param {{event: string, error: Error, text: string, info:{}, progress:{}}} data
 */
function showUpdatePopup(data) {
  let updateWindow = new BrowserWindow({
    parent: appWindow.get(),
    modal: true,
    show: false,
    alwaysOnTop: true,
    width: 400,
    height: 175,
    icon: `${config.applicationIconPath}`,
    resizable:  false,
    minimizable: false,
    skipTaskbar: true,
    webPreferences: {
      devTools: process.env.NODE_ENV === "development",
      nodeIntegration: true,
      allowRunningInsecureContent: false,
      plugins: false,
      preload: config.nimbusPreloadAssetsFile
    }
  });
  updateWindow.setMenu(null);

  childWindow.set(childWindow.TYPE_UPDATE, updateWindow);

  updateWindow.once('close', () => {
    childWindow.set(childWindow.TYPE_UPDATE, null);
    if(updater) {
      updater.enabled = true;
    }
  });

  childWindow.get(childWindow.TYPE_UPDATE).once('ready-to-show', () => {
    appPage.loadNimbusUpdateAssets(childWindow.get(childWindow.TYPE_UPDATE));
    setTimeout(() => {
      childWindow.get(childWindow.TYPE_UPDATE).show();
    }, 250);
  });

  updateWindow.loadURL(config.nimbusUpdateUrl);
}

/**
 * @param {{event: string, error: Error, text: string, info:{}, progress:{}}} data
 */
function sendStatusToWindow(data) {
  const {event, error, text, info, progress} = data;

  if(text) {
    errorHandler.log(text);
  }

  if(info) {
    updateInfo = info;
    errorHandler.log(info);
  }

  if(error) {
    if (config.SHOW_WEB_CONSOLE) {
      console.log(error);
    }
    errorHandler.log(error);
    if(updater) {
      updater.enabled = true;
      updater = null;
    }

    return dialog.showErrorBox(trans.get('update_error__title'), trans.get('update_error__message'));
  }

  switch (event) {
    case 'update-available': {
      showUpdatePopup(data);
      break;
    }
    case 'download-progress': {
      if (childWindow.get(childWindow.TYPE_UPDATE)) {
        childWindow.get(childWindow.TYPE_UPDATE).webContents.send('event:latest:update:progress:response', {
          progress: `${Math.round(progress.percent + 0.0001)}%`
        });
      }
      break;
    }
    case 'update-not-available': {
      closeUpdateWindow();
      if(showError) {
        dialog.showMessageBox({
          title: trans.get('update_not_available__title'),
          message: trans.get('update_not_available__message'),
          icon: config.applicationIconPath,
        }).then(() => {
          if(updater) {
            updater.enabled = true;
            updater = null;
          }
          showError = false;
        });
      }
      break;
    }
    case 'update-downloaded': {
      closeUpdateWindow();
      dialog.showMessageBox({
        title: trans.get('update_downloaded__title'),
        message: trans.get('update_downloaded__message'),
        icon: config.applicationIconPath,
      }).then(() => {
        if(updater) {
          updater.enabled = true;
          updater = null;
        }
        setImmediate(() => autoUpdater.quitAndInstall());
      });
      break;
    }
    case 'error': {
      if(updater) {
        updater.enabled = true;
        updater = null;
      }
      closeUpdateWindow();
      if(showError) {
        sendStatusToWindow({error});
      }
      break;
    }
  }
}

export function closeUpdateWindow() {
  if(childWindow.get(childWindow.TYPE_UPDATE)) {
    childWindow.get(childWindow.TYPE_UPDATE).close();
  }

  if(updater) {
    updater.enabled = true;
    updater = null;
  }
}

export function confirmUpdateWindow() {
  try {
    autoUpdater.downloadUpdate().catch((error) => {
      if (isNetworkError(error)) {
        errorHandler.log('Network Error');
      } else {
        errorHandler.log('Unknown Error');
        errorHandler.log(error == null ? "unknown" : (error.stack || error).toString());
      }
    });
  } catch (error) {
    sendStatusToWindow({text: 'downloadUpdate error', error});
  }
}

function isNetworkError(errorObject) {
  return errorObject.message === "net::ERR_INTERNET_DISCONNECTED" ||
    errorObject.message === "net::ERR_PROXY_CONNECTION_FAILED" ||
    errorObject.message === "net::ERR_CONNECTION_RESET" ||
    errorObject.message === "net::ERR_CONNECTION_CLOSE" ||
    errorObject.message === "net::ERR_NAME_NOT_RESOLVED" ||
    errorObject.message === "net::ERR_CONNECTION_TIMED_OUT";
}

export function getUpdateInfo() {
  return updateInfo;
}

export function autoCheck() {
  if(updater) {
    updater.enabled = true;
    updater = null;
  }

  showError = false;
  setTimeout(() => {
    if(appOnlineState.get()) {
      try {
        if(!config.USE_DEV_SYNC) {
          autoUpdater.checkForUpdates();
        }
      } catch (error) {
        if(showError) {
          sendStatusToWindow({text: 'autoCheck => checkForUpdates => error', error});
        }
        updater.enabled = true;
      }
    }
  }, 5000);
}

/**
 * @param {MenuItem} menuItem
 */
export function menuCheck(menuItem) {
  if(!appOnlineState.get()) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:workspace:message:response', {
        message: trans.get('toast__can_not_update_application__offline'),
        //type: 'danger'
      });
    }
    return;
  }

  updater = menuItem;
  updater.enabled = false;
  showError = true;
  try {
    if(!config.USE_DEV_SYNC) {
      autoUpdater.checkForUpdates();
    }
  } catch (error) {
    if(showError) {
      sendStatusToWindow({text: 'menuCheck => checkForUpdates => error', error});
    }
    updater.enabled = true;
  }
}

export function initHandlers() {
  autoUpdater.on('checking-for-update', () => {
    sendStatusToWindow({event: 'checking-for-update', text: 'Checking for update...'});
  });
  autoUpdater.on('update-available', (info) => {
    sendStatusToWindow({event: 'update-available', text: 'Update available.', info});
  });
  autoUpdater.on('update-not-available', (info) => {
    sendStatusToWindow({event: 'update-not-available', text: 'Update not available.', info});
  });
  autoUpdater.on('error', (error) => {
    if(showError) {
      sendStatusToWindow({event: 'update-not-available', text: 'Error in auto-updater.', error});
    }
  });
  autoUpdater.on('download-progress', (progressObj) => {
    showError = true;
    let text = "Download speed: " + progressObj.bytesPerSecond;
    text = text + ' - Downloaded ' + progressObj.percent + '%';
    text = text + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
    sendStatusToWindow({event: 'download-progress', text, progress: progressObj});
  });
  autoUpdater.on('update-downloaded', (info) => {
    sendStatusToWindow({event: 'update-downloaded', text: 'Update downloaded', info});
  });

  setInterval(() => {
    autoCheck();
  }, 86400000);
  setTimeout(() => {
    autoCheck();
  }, 1000);
}
