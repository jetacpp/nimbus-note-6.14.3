import config from "../../config";

export function openBusinessAuthLink() {
    require('electron').shell.openExternal(`https://nimbusweb.me/auth/?f=register&rt=business&client=${config.CLIENT_NAME}`)
}
