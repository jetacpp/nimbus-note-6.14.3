import {shell} from "electron";
import {default as auth} from "../auth/auth";
import {default as config} from "../../config.runtime";
import {default as workspace} from "../db/models/workspace";
import {default as urlParser} from "./urlParser";
import SelectedOrganization from "../organization/SelectedOrganization";
import {default as appWindowInstance} from "../window/instance";

export function openLink(url) {
    if(!url) { return }
    shell.openExternal(url)
}

export async function getNavigationUrlDetails(url) {
    const authInfo = <{userId}>await auth.getUserAsync();
    const defaultNavigation = {
        notePageHost: '',
        organizationId: null,
        workspaceId: null,
        reloadPage: false,
    };

    const personalOrgNavigation = await parseNavigationUrlByOrgHost({
        url,
        host: config.WEBNOTES_DOMAIN,
        orgId: null,
        defaultNavigation,
    });
    if(personalOrgNavigation.notePageHost) {
       return personalOrgNavigation;
    }

    const workspacesList = <[]>await workspace.findUserWorkspaces();
    const {organizations} = await SelectedOrganization.getUserOrganizationsDetailsByWorkspaces(authInfo, workspacesList);
    for(let organization of organizations) {
        const {globalId, domain, sub, electronHost} =  organization;
        if(!electronHost || electronHost === config.WEBNOTES_DOMAIN) {continue;}

        let businessNavigation = defaultNavigation;
        if(domain) {
            businessNavigation = await parseNavigationUrlByOrgHost({
                url,
                host: domain,
                orgId: globalId,
                defaultNavigation,
            });
        }

        if(sub) {
            businessNavigation = await parseNavigationUrlByOrgHost({
                url,
                host: `${organization.sub}.${config.authApiServiceDomain}`,
                orgId: globalId,
                defaultNavigation,
            });
        }

        if(businessNavigation.notePageHost) {
            return businessNavigation;
        }
    }

    return defaultNavigation;
}

/**
 * @param {{url:string, host:string, orgId:string, defaultNavigation:{}}}
 */
export async function parseNavigationUrlByOrgHost({url, host, orgId, defaultNavigation}) {
    let {notePageHost, organizationId, workspaceId, reloadPage} = defaultNavigation;

    if(!url || !host) { return defaultNavigation; }
    if(typeof (notePageHost) === 'undefined') { notePageHost = ''; }
    if(typeof (organizationId) === 'undefined') { organizationId = null; }
    if(typeof (workspaceId) === 'undefined') { workspaceId = null; }
    if(typeof (workspaceId) === 'undefined') { reloadPage = false; }

    const searchHostHttp = `http://${host}`;
    const searchHostHttps = `https://${host}`;

    if(url.indexOf(`${searchHostHttp}/ws/`) >= 0) {
        notePageHost = searchHostHttp;
    }

    if(url.indexOf(`${searchHostHttps}/ws/`) >= 0) {
        notePageHost = searchHostHttps;
    }

    if(notePageHost) {
        organizationId = orgId;
        workspaceId = await workspace.getLocalId(urlParser.getWorkspaceIdFromUrl(url));
        reloadPage = await SelectedOrganization.set(organizationId, workspaceId);
    }

    return {
        notePageHost,
        organizationId,
        workspaceId,
        reloadPage,
    };
}

export function requestInternalNoteNavigation(url) {
    if (appWindowInstance.get()) {
        return appWindowInstance.get().webContents.send('event:navigate:to:internal:note:requests', {
            url
        });
    }
}
