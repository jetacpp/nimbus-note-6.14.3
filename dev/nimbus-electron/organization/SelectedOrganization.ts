import {default as auth} from "../auth/auth";
import {default as orgs} from "../db/models/orgs";
import {default as workspace} from "../db/models/workspace";
import {default as user} from "../db/models/user";
import SelectedWorkspace from "../workspace/SelectedWorkspace";
import {default as errorHandler} from "../utilities/errorHandler";

let userOrganizations = null;

export default class SelectedOrganization {
    static async set(globalId, workspaceId = null) {
        const userInfo = await auth.getUserAsync();
        if(!userInfo) {
            return false;
        }

        if(!globalId) { // is private org
            const prevOrgData  = await SelectedOrganization.getCurrentOrganization(userInfo);
            if(!prevOrgData) {
                return false;
            }

            await SelectedOrganization.setCurrentOrganization(userInfo, null);
            await SelectedWorkspace.setUserWorkspaceForSelectedOrganization(userInfo, workspaceId);
            return true;
        }

        const orgInstance = await orgs.getById(globalId);
        if(!orgInstance) {
            return false;
        }

        const prevOrgData  = await SelectedOrganization.getCurrentOrganization(userInfo);
        if(prevOrgData && prevOrgData.globalId === globalId) {
            return false;
        }

        await SelectedOrganization.setCurrentOrganization(userInfo, globalId);
        await SelectedWorkspace.setUserWorkspaceForSelectedOrganization(userInfo, workspaceId);
        return true;
    }

    static async setCurrentOrganization(authInfo, globalId) {
        return await user.saveUserSettings(authInfo, 'currentOrganization', globalId);
    }

    static async getCurrentOrganization(authInfo) {
        const settings = <{currentOrganization}>await user.getUserSettings(authInfo);

        if(!settings) {
            return null;
        }

        if(!settings.currentOrganization) {
            return null;
        }

        const globalId = settings.currentOrganization || null;

        const workspacesList = <[]>await workspace.findUserWorkspaces();
        await SelectedOrganization.getUserOrganizationsDetailsByWorkspaces(authInfo, workspacesList);

        const allUserOrganizationsDetails = SelectedOrganization.getUserOrganizationsDetails();
        if(!allUserOrganizationsDetails) {
            return null;
        }

        const {organizations} = allUserOrganizationsDetails;
        if(!organizations) {
            return null;
        }

        return organizations.find(organization => organization.globalId === globalId);
    }

    static getUserOrganizationsDetails() {
        return userOrganizations;
    }

    static async getUserOrganizationsDetailsByWorkspaces(authInfo, workspacesList = []) {
        const workspaces = <[{orgId, owner, globalId, access}]>await workspace.getResponseListJson(workspacesList);

        let workspacesAccess = workspaces
            .filter((workspaceInstance) => {
                const ownerUnknown = !workspaceInstance.owner;
                const userIsNotOwner = workspaceInstance.owner && workspaceInstance.owner.email &&
                    (workspaceInstance.owner.email.toLowerCase() !== authInfo.email.toLowerCase());
                const isBusinessOrg = workspaceInstance.orgId && workspaceInstance.orgId.charAt(0) === 'b';
                return  ownerUnknown || userIsNotOwner || isBusinessOrg;
            })
            .map((workspaceInstance) => {
                if(!workspaceInstance.access) {
                    return workspace.getDefaultAccess();
                }
                return workspaceInstance.access;
            });

        const orgsList = await orgs.getAllByQuery({type: orgs.TYPE_BUSINESS});
        const organizations = await orgs.getResponseListJson(orgsList);

        userOrganizations = {
            organizations,
            workspaces,
            workspacesAccess,
        };

        return userOrganizations;
    }
}
