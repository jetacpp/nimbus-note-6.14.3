// legacy endpoints
import {default as apiMe} from "./jsonInterceptors/me/apiMe";
import {default as apiMeUsage} from "./jsonInterceptors/me/apiMeUsage";
import {default as apiUserProfession} from "./jsonInterceptors/me/apiUserProfession";

// workspace endpoints
import {default as apiWorkspaceInfoGet} from "./jsonInterceptors/me/apiWorkspaceInfoGet";
import {default as apiWorkspaceUsageGet} from "./jsonInterceptors/me/apiWorkspaceUsageGet";
import {default as apiWorkspaceEmailsGet} from "./jsonInterceptors/me/apiWorkspaceEmailsGet";

import {default as apiProTrial} from "./jsonInterceptors/me/apiProTrial";
import {default as apiMeNameUpdate} from "./jsonInterceptors/me/apiMeNameUpdate";
import {default as apiMeAvatarUpdate} from "./jsonInterceptors/me/apiMeAvatarUpdate";
import {default as apiMeUpdate} from "./jsonInterceptors/me/apiMeUpdate";
import {default as apiMeChangePassword} from "./jsonInterceptors/me/apiMeChangePassword";
import {default as apiMeLogout} from "./jsonInterceptors/me/apiMeLogout";
import {default as apiSync} from "./jsonInterceptors/me/apiSync";
import {default as apiSyncUpdate} from "./jsonInterceptors/me/apiSyncUpdate";
import {default as apiAppSettings} from "./jsonInterceptors/app/apiAppSettings";
import {default as apiAppSettingsUpdate} from "./jsonInterceptors/app/apiAppSettingsUpdate";
import {default as apiAvatarPreview} from "./jsonInterceptors/me/apiAvatarPreview";
import {default as apiMeAvatarDelete} from "./jsonInterceptors/me/apiMeAvatarDelete";

import {default as apiUserIgnoreIntro} from "./jsonInterceptors/users/userIgnoreIntro";
import {default as apiUserIgnoreSharesInfo} from "./jsonInterceptors/users/userIgnoreSharesInfo";
import {default as apiUserNoteAppearance} from "./jsonInterceptors/users/userNoteAppearance";

import {default as apiUserOtp} from "./jsonInterceptors/users/apiUserOtp";
import {default as apiUserOtpUpdate} from "./jsonInterceptors/users/apiUserOtpUpdate";
import {default as apiUserOtpRemove} from "./jsonInterceptors/users/apiUserOtpRemove";
import {default as apiUserOtpIssue} from "./jsonInterceptors/users/apiUserOtpIssue";

import {default as apiMentionsWorkspace} from "./jsonInterceptors/mention/apiMentionsWorkspace";

import {default as apiUserSettings} from "./jsonInterceptors/users/userSettings";
import {default as apiUserSettingsUpdate} from "./jsonInterceptors/users/userSettingsUpdate";

import {default as apiUserVariableUpdate} from "./jsonInterceptors/users/apiUserVariableUpdate";

import {default as apiPremium} from "./jsonInterceptors/premium/apiPremium";
import {default as apiWorkspacePremium} from "./jsonInterceptors/premium/apiWorkspacePremium";
import {default as apiLimitChecker} from "./jsonInterceptors/limit/apiLimitChecker";
import {default as apiOrganizationLimits} from "./jsonInterceptors/limit/apiOrganizationLimits";

import {default as apiItemExport} from "./jsonInterceptors/export/apiItemExport";

import {default as apiPreviewList} from "./jsonInterceptors/preview/apiPreviewList";
import {default as apiPreviewRemove} from "./jsonInterceptors/preview/apiPreviewRemove";
import {default as apiPreviewUpdate} from "./jsonInterceptors/preview/apiPreviewUpdate";

import {default as apiShare} from "./jsonInterceptors/share/apiShare";
import {default as apiShareUpdate} from "./jsonInterceptors/share/apiShareUpdate";

import {default as apiClientRecent} from "./jsonInterceptors/client/apiClientRecent";

import {default as apiNoteTodoList} from "./jsonInterceptors/todo/apiNoteTodoList";
import {default as apiTodoOrder} from "./jsonInterceptors/todo/apiTodoOrder";
import {default as apiTodoList} from "./jsonInterceptors/todo/apiTodoList";
import {default as apiTodoRemove} from "./jsonInterceptors/todo/apiTodoRemove";
import {default as apiTodoUpdate} from "./jsonInterceptors/todo/apiTodoUpdate";
import {default as apiTodo} from "./jsonInterceptors/todo/apiTodo";
import {default as apiTodoOrderUpdate} from "./jsonInterceptors/todo/apiTodoOrderUpdate";

import {default as apiItemList} from "./jsonInterceptors/item/apiItemList";
import {default as apiItemRemove} from "./jsonInterceptors/item/apiItemRemove";
import {default as apiItemParentId} from "./jsonInterceptors/item/apiItemParentId";
import {default as apiItemUpdate} from "./jsonInterceptors/item/apiItemUpdate";
import {default as apiItem} from "./jsonInterceptors/item/apiItem";

import {default as apiTextList} from "./jsonInterceptors/text/apiTextList";
import {default as apiTextUpdate} from "./jsonInterceptors/text/apiTextUpdate";
import {default as apiText} from "./jsonInterceptors/text/apiText";
import {default as apiTextTokenUpdate} from "./jsonInterceptors/text/apiTextTokenUpdate";

import {default as apiTagList} from "./jsonInterceptors/tag/apiTagList";
import {default as apiTagRemove} from "./jsonInterceptors/tag/apiTagRemove";
import {default as apiTagUpdate} from "./jsonInterceptors/tag/apiTagUpdate";

import {default as apiNoteTagsList} from "./jsonInterceptors/tag/apiNoteTagsList";
import {default as apiNoteTagsRemove} from "./jsonInterceptors/tag/apiNoteTagsRemove";
import {default as apiNoteTagsUpdate} from "./jsonInterceptors/tag/apiNoteTagsUpdate";
import {default as apiNotesTagsUpdate} from "./jsonInterceptors/tag/apiNotesTagsUpdate";

import {default as apiAttachList} from "./jsonInterceptors/attach/apiAttachList";
import {default as apiAttach} from "./jsonInterceptors/attach/apiAttach";
import {default as apiAttachDownload} from "./jsonInterceptors/attach/apiAttachDownload";
import {default as apiAttachPreview} from "./jsonInterceptors/attach/apiAttachPreview";
import {default as apiAttachRemove} from "./jsonInterceptors/attach/apiAttachRemove";
import {default as apiAttachUpdate} from "./jsonInterceptors/attach/apiAttachUpdate";
import {default as apiAttachCreate} from "./jsonInterceptors/attach/apiAttachCreate";

import {default as apiAnnotation} from "./jsonInterceptors/annotation/apiAnnotation";
import {default as apiAnnotations} from "./jsonInterceptors/annotation/apiAnnotations";

import {default as apiNoteRemindersList} from "./jsonInterceptors/reminder/apiNoteRemindersList";
import {default as apiNoteReminderRemove} from "./jsonInterceptors/reminder/apiNoteReminderRemove";
import {default as apiNoteReminderUpdate} from "./jsonInterceptors/reminder/apiNoteReminderUpdate";

import {default as apiItemDuplicate} from "./jsonInterceptors/item/apiItemDuplicate";

import {default as userFindById} from "./jsonInterceptors/users/userFindById";
import {default as userFind} from "./jsonInterceptors/users/userFind";

import {default as apiInvitesList} from "./jsonInterceptors/workspace/apiInvitesList";
import {default as apiWorkspaceInviteAdd} from "./jsonInterceptors/workspace/apiWorkspaceInviteAdd";
import {default as apiWorkspaceInviteResend} from "./jsonInterceptors/workspace/apiWorkspaceInviteResend";
import {default as apiWorkspaceInviteUpdate} from "./jsonInterceptors/workspace/apiWorkspaceInviteUpdate";
import {default as apiWorkspaceInviteRemove} from "./jsonInterceptors/workspace/apiWorkspaceInviteRemove";

import {default as apiWorkspaceChange} from "./jsonInterceptors/workspace/apiWorkspaceChange";

import {default as apiMembersList} from "./jsonInterceptors/workspace/apiMembersList";
import {default as apiWorkspaceMemberUpdate} from "./jsonInterceptors/workspace/apiWorkspaceMemberUpdate";
import {default as apiWorkspaceMemberRemove} from "./jsonInterceptors/workspace/apiWorkspaceMemberRemove";

import {default as apiWorkspaceEmail} from "./jsonInterceptors/workspace/apiWorkspaceEmail";
import {default as apiWorkspacesList} from "./jsonInterceptors/workspace/apiWorkspacesList";
import {default as apiWorkspaceUpdate} from "./jsonInterceptors/workspace/apiWorkspaceUpdate";
import {default as apiWorkspaceRemove} from "./jsonInterceptors/workspace/apiWorkspaceRemove";

export default {
  /**
   * @param callback
   */
  returnEmptyJson: (callback) => {
    callback('[]');
  },

  // precessed json request
  apiMe,
  apiMeUsage,
  apiUserProfession,
  apiWorkspaceInfoGet,
  apiWorkspaceUsageGet,
  apiWorkspaceEmailsGet,
  apiProTrial,
  apiMeNameUpdate,
  apiMeAvatarUpdate,
  apiMeUpdate,
  apiMeChangePassword,
  apiMeLogout,
  apiSync,
  apiSyncUpdate,
  apiAppSettings,
  apiAppSettingsUpdate,
  apiAvatarPreview,
  apiMeAvatarDelete,

  apiUserIgnoreIntro,
  apiUserIgnoreSharesInfo,
  apiUserNoteAppearance,

  apiUserOtp,
  apiUserOtpUpdate,
  apiUserOtpRemove,
  apiUserOtpIssue,

  apiMentionsWorkspace,

  apiUserSettings,
  apiUserSettingsUpdate,

  apiUserVariableUpdate,

  apiPremium,
  apiWorkspacePremium,
  apiLimitChecker,
  apiOrganizationLimits,

  apiItemExport,

  apiPreviewList,
  apiPreviewRemove,
  apiPreviewUpdate,

  apiShare,
  apiShareUpdate,

  apiClientRecent,

  apiNoteTodoList,
  apiTodoOrder,
  apiTodoList,
  apiTodoRemove,
  apiTodoUpdate,
  apiTodo,
  apiTodoOrderUpdate,

  apiItemList,
  apiItemRemove,
  apiItemParentId,
  apiItemUpdate,
  apiItem,

  apiTextList,
  apiTextUpdate,
  apiText,
  apiTextTokenUpdate,

  apiTagList,
  apiTagRemove,
  apiTagUpdate,

  apiNoteTagsList,
  apiNoteTagsRemove,
  apiNoteTagsUpdate,
  apiNotesTagsUpdate,

  apiAttachList,
  apiAttach,
  apiAttachPreview,
  apiAttachDownload,
  apiAttachRemove,
  apiAttachUpdate,
  apiAttachCreate,

  apiAnnotation,
  apiAnnotations,

  apiNoteRemindersList,
  apiNoteReminderRemove,
  apiNoteReminderUpdate,

  apiItemDuplicate,

  userFindById,
  userFind,

  apiInvitesList,
  apiWorkspaceInviteAdd,
  apiWorkspaceInviteResend,
  apiWorkspaceInviteUpdate,
  apiWorkspaceInviteRemove,

  apiWorkspaceChange,

  apiMembersList,
  apiWorkspaceMemberUpdate,
  apiWorkspaceMemberRemove,

  apiWorkspaceEmail,
  apiWorkspacesList,
  apiWorkspaceUpdate,
  apiWorkspaceRemove,
}
