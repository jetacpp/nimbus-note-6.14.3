import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as user} from "../../../../db/models/user";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiUserOtp(request, callback) {
    if (!request.authInfo) {
        return callback(error.wrongInput(), {});
    }

    let findQuery = {login: request.authInfo.email};
    // @ts-ignore
    user.find(findQuery, {}, async (err, userItem) => {
        if (userItem && Object.keys(userItem).length) {
            // const response = userItem.otpSetup || '';

            const response = {
                textResponse: true,
                text: '',
            };

            callback(null, response);
        } else {
            callback(error.itemNotFound(), {});
        }
    });
}
