import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as orgs} from "../../../../db/models/orgs";
import SelectedWorkspace from "../../../../workspace/SelectedWorkspace";
import {default as workspaceModel} from "../../../../db/models/workspace";
import {default as auth} from "../../../../auth/auth";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiUserFindById(request, callback) {
    let routeParams = urlParser.getPathParams(request.url);
    let userId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;
    let workspace = <{ globalId: string }>await SelectedWorkspace.get();

    if (!request.authInfo || !userId || !workspace) {
        return callback(error.wrongInput(), {});
    }

    workspaceModel.find({globalId: workspace.globalId}, {}, async (err, workspaceInstance) => {
        if (err || !workspaceInstance) {
            return callback(error.itemNotFound(), []);
        }

        const userInfo = <any>await auth.getUserAsync();
        let orgInstance = <any>await orgs.getById(workspaceInstance.orgId);

        if (!orgInstance) {
            const isBusinessOrg = workspaceInstance.orgId && workspaceInstance.orgId.charAt(0) === 'b';
            if (!isBusinessOrg && workspaceInstance.org) {
                orgInstance = workspaceInstance.org;
            }
        }

        if (!orgInstance) {
            orgInstance = await orgs.getDefaultOrg();
        }

        const members = workspaceInstance.members;
        const findWorkspaceMember = members.find((workspaceMember) => {
            return workspaceMember && workspaceMember.user && workspaceMember.user.id.toString() === userId;
        });

        let findUser = findWorkspaceMember ? findWorkspaceMember.user : null;
        if (!findUser && orgInstance && orgInstance.type === orgs.TYPE_PRIVATE) {
            if (orgInstance.user && orgInstance.user.email) {
                if(orgInstance.user.email.toLowerCase() === userInfo.email) {
                    findUser = {
                        id: orgInstance.user.id ? orgInstance.user.id : userInfo.id,
                        username: userInfo.username ? userInfo.username : '',
                        displayName: userInfo.username ? userInfo.username : '',
                        email: userInfo.email ? userInfo.email.toLowerCase() : '',
                        defaultEncryptionKeyId: null,
                        languages: userInfo.languages ? userInfo.languages : [],
                        firstname: userInfo.firstname ? userInfo.firstname : '',
                        lastname: userInfo.lastname ? userInfo.lastname : '',
                        avatar: userInfo.avatar && userInfo.avatar.url ? userInfo.avatar.url : null,
                    };
                } else {
                    findUser = {
                        id: orgInstance.user.id ? orgInstance.user.id : '',
                        username: orgInstance.user.username ? orgInstance.user.username : '',
                        displayName: orgInstance.user.username ? orgInstance.user.username : '',
                        email: orgInstance.user.email ? orgInstance.user.email.toLowerCase() : '',
                        defaultEncryptionKeyId: null,
                        languages: orgInstance.user.languages ? orgInstance.user.languages : [],
                        firstname: orgInstance.user.firstname ? orgInstance.user.firstname : '',
                        lastname: orgInstance.user.lastname ? orgInstance.user.lastname : '',
                        avatar: orgInstance.user.avatar ? orgInstance.user.avatar : null,
                    };
                }
            }
        }

        if (!findUser) {
            return callback(error.itemNotFound(), {});
        }

        return callback(null, findUser);
    });
}
