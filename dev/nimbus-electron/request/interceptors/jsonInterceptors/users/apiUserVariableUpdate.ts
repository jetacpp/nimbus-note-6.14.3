
import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as user} from "../../../../db/models/user";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";
import {default as auth} from "../../../../auth/auth";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiUserVariableUpdate(request, callback) {
    if (!request.authInfo) {
        return callback(error.wrongInput(), {});
    }

    let routeParams = urlParser.getPathParams(request.url);
    let varName = typeof (routeParams[4] !== 'undefined') ? decodeURI(routeParams[4]) : null;
    let varValue;

    if(!varName) {
        return callback(error.wrongInput(), {});
    }

    if(typeof (request.body.value) !== 'undefined') {
        varValue = request.body.value;
    }

    if(typeof(varValue) === 'undefined') {
        return callback(error.wrongInput(), {});
    }

    let curDate = date.now();

    const authInfo = <{variables}>await auth.fetchActualUserAsync();
    const userVariables = authInfo && authInfo.variables ? authInfo.variables : user.getDefaultVariables();

    let saveUserData = {variables: {...userVariables, [varName]: varValue}};
    let queryData = {email: request.authInfo.email};

    user.update(queryData, setUpdateProps(saveUserData, curDate), {}, (err, count) => {
        if (err || !count) {
            return callback(error.itemSaveError(), {});
        }

        const apiUpdateData = { key: varName, value: varValue };
        NimbusSDK.getApi().updateUserVariable({body: apiUpdateData, skipSyncHandlers: true}, (err, res) => {
            if (err || !res) {
                //return callback(error.itemUpdateError(), {httpStatus: 500});
            }

            callback(null, varValue);
        });
    });
}
