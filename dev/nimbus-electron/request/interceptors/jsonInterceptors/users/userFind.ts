import chalk from "chalk";
import {default as urlParser} from "./../../../../utilities/urlParser";
import {default as error} from "./../../../../utilities/customError";
import {default as user} from "./../../../../db/models/user";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function userFind(request, callback) {
  let filterParams = urlParser.getFilterParams(request.url) || <any>{};

  if (!filterParams.email) {
    return callback(error.wrongInput(), {});
  }

  let findQuery = {login: filterParams.email.toLowerCase()};
  user.find(findQuery, {}, (err, userItem) => {
    if (userItem && Object.keys(userItem).length) {
      callback(null, [user.getPublicData(userItem)]);
    } else {
      callback(error.itemNotFound(), {});
    }
  });
}
