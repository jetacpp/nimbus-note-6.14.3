import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as user} from "../../../../db/models/user";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiUserSettingsUpdate(request, callback) {
  if (!request.body.dateTimeLocale) {
    return callback(error.wrongInput(), {});
  }

  if (!request.authInfo) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();

  let saveUserData = {dateTimeLocale: request.body.dateTimeLocale};
  let queryData = {email: request.authInfo.email};

  user.update(queryData, setUpdateProps(saveUserData, curDate), {}, (err, count) => {
    if (err || !count) {
      return callback(error.itemSaveError(), {});
    }
    callback(null, {});
  });
}
