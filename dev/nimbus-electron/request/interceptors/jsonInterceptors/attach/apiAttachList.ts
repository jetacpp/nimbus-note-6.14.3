import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as attach} from "../../../../db/models/attach";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiAttachList(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let filterParams = <any>urlParser.getFilterParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

  if (!filterParams.noteGlobalId) {
    return callback(error.wrongInput(), []);
  }

  let findQuery = {noteGlobalId: filterParams.noteGlobalId};
  attach.findAll(findQuery, {workspaceId}, (err, attachList) => {
    callback(null, attach.getResponseListJson(attachList));
  });
}
