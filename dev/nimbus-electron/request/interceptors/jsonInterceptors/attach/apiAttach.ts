import chalk from "chalk";
import {default as urlParser} from "./../../../../utilities/urlParser";
import {default as error} from "./../../../../utilities/customError";
import {default as attach} from "./../../../../db/models/attach";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiAttach(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

  if (!globalId) {
    return callback(error.wrongInput(), {});
  }

  let queryData = {globalId: globalId};
  attach.find(queryData, {workspaceId}, (err, attachItem) => {
    callback(null, attach.getResponseJson(attachItem));
  });
}
