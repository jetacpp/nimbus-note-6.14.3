import chalk from "chalk";
import fs = require('fs-extra');
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as attach} from "../../../../db/models/attach";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as date} from "../../../../utilities/dateHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {default as PouchDb} from "../../../../../pdb";
import {default as config} from "../../../../../config.runtime";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiAttachRemove(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

  if (!globalId) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();
  let queryData = {globalId: globalId};
  attach.find(queryData, {workspaceId}, (err, attachItem) => {
    if (attachItem && Object.keys(attachItem).length) {
      attach.remove(queryData, {workspaceId}, async () => {
        if (!PouchDb.getClientAttachmentPath()) {
          return callback(error.wrongAttachPath(), {});
        }

        let targetPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
        // @ts-ignore
        let exists = await fs.exists(targetPath);
        // @ts-ignore
        if (exists) {
          try {
            fs.unlink(targetPath, () => {
            });
          } catch (e) {
            if (config.SHOW_WEB_CONSOLE) {
              console.log("Error => apiAttachRemove => remove file: ", targetPath, e);
            }
          }
        }

        let itemQueryData = {globalId: attachItem.noteGlobalId};
        let itemSaveData = {
          "dateUpdated": curDate,
          "updatedAt": curDate,
          "dateUpdatedUser": curDate,
          "syncDate": curDate,
          "needSync": true
        };
        item.update(itemQueryData, setUpdateProps(itemSaveData, curDate), {workspaceId}, () => {
          item.find(itemQueryData, {workspaceId}, (err, itemInstance) => {
            callback(null, {});
            socketUserAttachMessage({workspaceId, attachItem});
            socketUserCounterMessage({workspaceId, itemInstance});
          });
        });
      });
    } else {
      callback(error.itemNotFound(), {});
    }
  });
}

/**
 * @param {{workspaceId:string, attachItem:attach}} inputData
 */
function socketUserAttachMessage(inputData) {
  const {workspaceId, attachItem} = inputData;
  if (attachItem) {
    socketConnection.sendNoteAttachmentsRemoveMessage({
      workspaceId,
      noteGlobalId: attachItem.noteGlobalId,
      globalId: attachItem.globalId
    });
  }
}

/**
 * @param {{workspaceId:string, itemInstance:item}} inputData
 */
function socketUserCounterMessage(inputData) {
  const {workspaceId, itemInstance} = inputData;
  if (itemInstance) {
    socketConnection.sendItemsCountMessage({
      workspaceId,
      globalId: itemInstance.globalId,
      parentId: itemInstance.parentId,
      type: itemInstance.type
    });
  }
}
