import chalk from "chalk";
import fs = require('fs-extra');
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as attach} from "../../../../db/models/attach";
import {default as SelectedWorkspace} from "../../../../workspace/SelectedWorkspace";
import {default as PouchDb} from "../../../../../pdb";

/**
 * @param {Request} request
 * @param {Response} response
 */
export default async function apiAttachDownload(request, response) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = await SelectedWorkspace.getGlobalId();
  let globalId = typeof (routeParams[2] !== 'undefined') ? routeParams[2] : null;

  let queryData = {globalId: globalId};
  attach.find(queryData, {workspaceId}, async (err, attachItem) => {
    if (attachItem && Object.keys(attachItem).length) {
      if (!PouchDb.getClientAttachmentPath()) {
        return response.status(404).send();
      }

      let targetPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
      let targetFileName = attachItem.displayName;

      // @ts-ignore
      let exists = await fs.exists(targetPath);
      errorHandler.log("returnApiAttachDownloadJson => file exist by path: " + targetPath);
      // @ts-ignore
      if (!exists) {
        errorHandler.log("returnApiAttachDownloadJson => file not exist by path: " + targetPath);
        return response.status(404).send();
      }

      response.download(targetPath, targetFileName);
    }
  });
}
