import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as date} from "../../../../utilities/dateHandler";
import {default as error} from "../../../../utilities/customError";
import {default as attach} from "../../../../db/models/attach";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiAttachUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

  if (!globalId || !request.body.displayName) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();
  let saveTodoData = <any>{
    "displayName": request.body.displayName,
    "dateUpdated": curDate,
    "syncDate": curDate,
    "needSync": true
  };

  let queryData = {globalId: globalId};
  attach.find(queryData, {workspaceId}, (err, attachItem) => {
    if (attachItem && Object.keys(attachItem).length) {
      saveTodoData.oldDisplayName = attachItem.displayName;
      attach.update(queryData, setUpdateProps(saveTodoData, curDate), {workspaceId}, (err, count) => {
        if (!count) {
          return callback(error.itemSaveError(), {});
        }

        let itemQueryData = {globalId: attachItem.noteGlobalId};
        let itemSaveData = {
          "dateUpdated": curDate,
          "updatedAt": curDate,
          "dateUpdatedUser": curDate,
          "syncDate": curDate,
          "needSync": true
        };

        item.update(itemQueryData, setUpdateProps(itemSaveData, curDate), {workspaceId}, () => {
          item.find(itemQueryData, {workspaceId}, (err, itemInstance) => {
            attach.find(queryData, {workspaceId}, (err, attachItem) => {
              callback(null, attach.getResponseJson(attachItem));
              socketUserAttachMessage({workspaceId, attachItem});
              socketUserCounterMessage({workspaceId, itemInstance});
            });
          });
        });
      });
    } else {
      callback({});
    }
  });
}

/**
 * @param {{workspaceId:string, attachItem:attach}} inputData
 */
function socketUserAttachMessage(inputData) {
  const {workspaceId, attachItem} = inputData;
  if (attachItem) {
    socketConnection.sendNoteAttachmentsUpdateMessage({
      workspaceId,
      noteGlobalId: attachItem.noteGlobalId,
      globalId: attachItem.globalId
    });
  }
}

/**
 * @param {{workspaceId:string, itemInstance:string}} inputData
 */
function socketUserCounterMessage(inputData) {
  const {workspaceId, itemInstance} = inputData;
  if (itemInstance) {
    socketConnection.sendItemsCountMessage({
      workspaceId,
      globalId: itemInstance.globalId,
      parentId: itemInstance.parentId,
      type: itemInstance.type
    });
  }
}
