import chalk from "chalk";
import fs = require('fs-extra');
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as attach} from "../../../../db/models/attach";
import {default as SelectedWorkspace} from "../../../../workspace/SelectedWorkspace";
import {default as PouchDb} from "../../../../../pdb";

export default class ApiAttachExist {
  /**
   * @param {Request} request
   * @param {Function} callback
   */
  static async interceptRequest(request, callback) {
    let routeParams = urlParser.getPathParams(request.url);
    let workspaceId = await SelectedWorkspace.getGlobalId();
    let globalId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;
    return ApiAttachExist.apiAttachExist({workspaceId, globalId}, callback);
  }

  static interceptRequestAsync(request) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      await ApiAttachExist.interceptRequest(request, (err, response) => {
        return resolve(response);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async interceptRequestByGlobalIdAsync(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      ApiAttachExist.apiAttachExist(inputData, (err, response) => {
        return resolve(response);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static apiAttachExist(inputData, callback) {
    const {workspaceId, globalId} = inputData;
    if (!globalId) {
      return callback(null, false);
    }

    let queryData = {globalId: globalId};
    attach.find(queryData, {workspaceId}, async (err, attachItem) => {
      if (attachItem && Object.keys(attachItem).length) {
        if (!attachItem.isDownloaded) {
          return callback(err, false);
        }

        if (!PouchDb.getClientAttachmentPath()) {
          return callback(err, false);
        }

        let targetPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
        // @ts-ignore
        let exists = await fs.exists(targetPath);
        // @ts-ignore
        if (!exists) {
          errorHandler.log("apiAttachExist => file not exist by path: " + targetPath);
          return callback(null, false);
        }

        callback(null, true);
      } else {
        callback(null, false);
      }
    });
  }
}
