import chalk from "chalk";
import fs = require('fs-extra');
import {default as config} from "../../../../../config.runtime";
import {default as error} from "../../../../utilities/customError";
import {default as attach} from "../../../../db/models/attach";
import {default as item} from "../../../../db/models/item";
import {default as date} from "../../../../utilities/dateHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as PouchDb} from "../../../../../pdb";
import {default as workspace} from "../../../../db/models/workspace";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as appWindow} from "../../../../window/instance";
const fetch = require('node-fetch');
const fileType = require('file-type');
import mimeTypes = require("mime-types");

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiAttachCreate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  let body = request.body;
  let file = null;
  let mimetype = ''
  let displayName = ''
  let size = 0
  let isBuffer = false
  if(body.sourceExternalUrl) {
    try {
      isBuffer = true
      displayName = body.sourceExternalUrl.split('/').pop()
      const sourceRes = await fetch(body.sourceExternalUrl)
      file = await sourceRes.buffer()
      mimetype = await fileType.fromBuffer(file)
      size = file.toString().length
    } catch (e) {}
  } else if(request.files && request.files.attachment && request.files.attachment.length) {
    displayName = body.displayName
    file = request.files.attachment[0];
    mimetype = file.mimetype
    size = body.size ? parseInt(body.size) : 0
  } else {
    return callback(error.wrongInput(), {});
  }

  if(!file) {
    return callback(error.wrongInput(), {});
  }

  if(!displayName) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();

  let storedFileUUID = attach.generateStoredFileUUID();

  let extension = "";
  if (displayName) {
    let nameData = displayName.split(".");
    if (nameData.length > 1) {
      extension = nameData[nameData.length - 1];
    }
  }

  if(!mimetype) {
    mimetype = mimeTypes.extension(extension) ? `.${mimeTypes.extension(extension)}` : ''
  }

  let saveData = {
    'globalId': body.globalId,
    'noteGlobalId': body.noteGlobalId,
    'dateAdded': curDate,
    'dateUpdated': curDate,
    'displayName': displayName,
    'inList': body.inList === "true",
    'mime': mimetype,
    'size': size,
    'storedFileUUID': storedFileUUID,
    'hashKey': body.$$hashKey,
    "syncDate": curDate,
    "needSync": true,
    "isDownloaded": true,
    "extension": extension
  };

  // @ts-ignore
  let exists = await fs.exists(config.nimbusAttachmentPath);
  // @ts-ignore
  if (exists) {
    prepareAttachLoad({workspaceId, saveData, file, isBuffer}, callback);
  } else {
    fs.mkdir(config.nimbusAttachmentPath, () => {
      prepareAttachLoad({workspaceId, saveData, file, isBuffer}, callback);
    });
  }
}

/**
 * @param {{ workspaceId: string, saveData: {}, file, isBuffer: boolean }} inputData
 * @param {Function} callback
 */
async function prepareAttachLoad(inputData, callback) {
  const {workspaceId, saveData, file, isBuffer} = inputData;
  if (!PouchDb.getClientAttachmentPath()) {
    return callback(error.wrongAttachPath(), {});
  }

  let targetPath = `${PouchDb.getClientAttachmentPath()}/${saveData.storedFileUUID}`;
  // @ts-ignore
  let exists = await fs.exists(PouchDb.getClientAttachmentPath());
  // @ts-ignore
  if (exists) {
    if(isBuffer) {
      fs.writeFile(targetPath, file, (err) => {
        if (err) {
          if (config.SHOW_WEB_CONSOLE) { console.log(err); }
        }
        prepareAttachSave({workspaceId, saveData}, callback);
      })
    } else {
      fs.rename(file.path, targetPath, (err) => {
        if (err) {
          if (config.SHOW_WEB_CONSOLE) { console.log(err); }
        }
        prepareAttachSave({workspaceId, saveData}, callback);
      });
    }
  } else {
    fs.mkdir(PouchDb.getClientAttachmentPath(), () => {
      if(isBuffer) {
        fs.writeFile(targetPath, file, (err) => {
          if (err) {
            if (config.SHOW_WEB_CONSOLE) { console.log(err); }
          }
          prepareAttachSave({workspaceId, saveData}, callback);
        })
      } else {
        fs.rename(file.path, targetPath, (err) => {
          if (err) {
            if (config.SHOW_WEB_CONSOLE) {
              console.log('Move file error: ', err);
            }
          }
          prepareAttachSave({workspaceId, saveData}, callback);
        });
      }
    });
  }
}

/**
 * @param {{workspaceId:string, saveData:{}}} inputData
 * @param callback Function
 */
function prepareAttachSave(inputData, callback) {
  const {workspaceId, saveData} = inputData;
  let curDate = date.now();
  let itemSaveData = attach.prepareModelData(saveData);
  attach.add(setUpdateProps(itemSaveData, curDate), {workspaceId}, (err, attachItem) => {
    if (attachItem && Object.keys(attachItem).length) {
      let curDate = date.now();
      let itemQueryData = {globalId: attachItem.noteGlobalId};
      let itemSaveData = {
        "dateUpdated": saveData.dateUpdated,
        "updatedAt": saveData.dateUpdated,
        "dateUpdatedUser": saveData.dateUpdated,
        "syncDate": curDate,
        "needSync": true
      };

      item.update(itemQueryData, setUpdateProps(itemSaveData, curDate), {workspaceId}, () => {
        item.find(itemQueryData, {workspaceId}, (err, itemInstance) => {
          callback(null, attach.getResponseJson(attachItem));
          socketUserAttachMessage({workspaceId, attachItem});
          socketUserCounterMessage({workspaceId, itemInstance});
          if(itemInstance.text_version >= 2) {
            appWindow.get().webContents.send('event:client:autosync:request', {workspaceId});
          }
        });
      });
    } else {
      callback(error.itemSaveError(), {});
    }
  });
}

/**
 * @param {{workspaceId:string, attachItem:attach}} inputData
 */
function socketUserAttachMessage(inputData) {
  const {workspaceId, attachItem} = inputData;
  if (attachItem) {
    socketConnection.sendNoteAttachmentsUpdateMessage({
      workspaceId,
      noteGlobalId: attachItem.noteGlobalId,
      globalId: attachItem.globalId
    });
  }
}

/**
 * @param {{workspaceId:string, itemInstance:{}}} inputData
 */
function socketUserCounterMessage(inputData) {
  const {workspaceId, itemInstance} = inputData;
  if (itemInstance) {
    socketConnection.sendItemsCountMessage({
      workspaceId,
      globalId: itemInstance.globalId,
      parentId: itemInstance.parentId,
      type: itemInstance.type
    });
  }
}
