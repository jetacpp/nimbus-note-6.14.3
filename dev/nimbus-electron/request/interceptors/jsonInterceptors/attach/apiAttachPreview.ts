import chalk from "chalk";
import fs = require('fs-extra');
import Jimp from "jimp";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as attach} from "../../../../db/models/attach";
import {default as PouchDb} from "../../../../../pdb";
import {default as workspace} from "../../../../db/models/workspace";
import config from "../../../../../config";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param {Request} request
 * @param {Response} response
 */
// @ts-ignore
export default async function apiAttachPreview(request, response) {
  const {height} = request.query;
  let routeParams = urlParser.getPathParams(request.url);
  let isDownloadAction = request.url.indexOf(`?dw`) > 0;
  let workspaceId = typeof (routeParams[2] !== 'undefined') ? await workspace.getLocalId(routeParams[2]) : null;
  let globalId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;

  let queryData = {globalId: globalId};
  attach.find(queryData, {workspaceId}, async (err, attachItem) => {
    if (attachItem && Object.keys(attachItem).length) {
      if (!PouchDb.getClientAttachmentPath()) {
        return response.status(404).send();
      }

      let originalPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
      let previewPath = `${originalPath}-preview`;

      if (isDownloadAction) {
        // @ts-ignore
        let fileExist = await fs.exists(originalPath);
        // @ts-ignore
        if (!fileExist) {
          errorHandler.log("apiAttachPreview => file not exist by path: " + originalPath);
          return response.status(404).send();
        }

        let targetFileName = attachItem.displayName;
        return response.download(originalPath, targetFileName);
      }

      if(height) {
        // @ts-ignore
        let previewFileExist = await fs.exists(previewPath);
        // @ts-ignore
        if(!previewFileExist) {
          // @ts-ignore
          let fileExist = await fs.exists(originalPath);
          // @ts-ignore
          if (!fileExist) {
            errorHandler.log("apiAttachPreview => file not exist by path: " + originalPath);
            return response.status(404).send();
          }

          try {
            await makeThumbnail(originalPath, previewPath);
            return response.sendFile(previewPath);
          } catch (error) {
            errorHandler.log(`Problem to generate preview`);
            errorHandler.log(error.message);
            return response.status(404).send();
          }
        }

        return response.sendFile(previewPath);
      }

      response.sendFile(originalPath);
    }
  });
}

export async function makeThumbnail(inputPath: string, outputPath:string): Promise<any> {
  return new Promise(async resolve => {
    try {
      const file = await Jimp.read(inputPath);
      const mime = file.getMIME();
      if(mime === Jimp.MIME_GIF) {
        const gifInput = await fs.readFile(inputPath);
        fs.writeFile(outputPath, gifInput, {}, () => {
          resolve(outputPath);
        });
      } else {
        file.resize(Jimp.AUTO, config.NOTE_PREVIEW_SCALE_SIZE).quality(100).write(outputPath, () => resolve(outputPath));
      }
    } catch (error) {
      syncHandler.sendLog(`Preview image error: ${error.message}`);
      resolve(null);
    }
  });
}
