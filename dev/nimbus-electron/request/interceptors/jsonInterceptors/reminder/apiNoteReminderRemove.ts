import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as date} from "../../../../utilities/dateHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {default as ReminderNotice} from "../../../../popup/ReminderNotice";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiNoteReminderRemove(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let noteGlobalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

  if (!noteGlobalId) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();

  let itemQueryData = {globalId: noteGlobalId};
  let itemSaveData = {
    // "dateUpdated": curDate,
    // "updatedAt": curDate,
    // "dateUpdatedUser": curDate,
    "reminder": null,
    "syncDate": curDate,
    "needSync": true
  };

  item.find(itemQueryData, {workspaceId}, (err, itemInstance) => {
    if (itemInstance && Object.keys(itemInstance).length) {
      // @ts-ignore
      item.update(itemQueryData, setUpdateProps(itemSaveData, curDate), {workspaceId}, async () => {
        callback(null, {});
        ReminderNotice.flushByGlobalId(itemInstance.globalId);
        socketNoteReminderMessage({workspaceId, noteGlobalId});
      });
    } else {
      callback(error.itemNotFound(), {});
    }
  });
}

/**
 * @param {{workspaceId:string, noteGlobalId:string}} inputData
 */
function socketNoteReminderMessage(inputData) {
  const {workspaceId, noteGlobalId} = inputData;
  if (noteGlobalId) {
    // socketConnection.sendNoteTagsMessage(inputData);
  }
}
