import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as reminder} from "../../../../db/models/reminder";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiNoteRemindersList(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

  item.findAll({type: 'note', reminder: {'$ne': null}}, {workspaceId}, (err, itemList) => {
    if (err || !itemList) {
      return callback(null, []);
    }

    callback(null, reminder.getResponseListJson(itemList.filter((itemInstance) => (itemInstance.reminder))));
  });
}
