import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as reminder} from "../../../../db/models/reminder";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as date} from "../../../../utilities/dateHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {default as ReminderNotice} from "../../../../popup/ReminderNotice";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiNoteReminderUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let noteGlobalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

  if (!noteGlobalId) {
    return callback(error.wrongInput(), {});
  }

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  if (!request.body.date) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();

  let saveReminderData = <any>{
    "date": request.body.date
  };

  if (typeof (request.body.interval) !== "undefined") {
    saveReminderData.interval = request.body.interval;
  }

  let itemQueryData = {globalId: noteGlobalId};
  let itemSaveData = <any>{
    "reminder": saveReminderData,
    // "dateUpdated": curDate,
    // "updatedAt": curDate,
    // "dateUpdatedUser": curDate,
    "syncDate": curDate,
    "needSync": true
  };

  item.find(itemQueryData, {workspaceId}, (err, itemInstance) => {
    if (itemInstance && Object.keys(itemInstance).length) {
      if (!itemInstance.reminder) {
        itemSaveData.reminder = reminder.prepareModelData(saveReminderData);
      } else {
        for (let i in saveReminderData) {
          itemSaveData.reminder[i] = saveReminderData[i];
        }
      }

      // @ts-ignore
      item.update(itemQueryData, setUpdateProps(itemSaveData, curDate), {workspaceId}, async () => {
        callback(null, {
          date: itemSaveData.reminder.date,
          interval: itemSaveData.reminder.interval,
          dateUpdated: curDate,
          noteGlobalId: noteGlobalId,
          priority: itemSaveData.reminder.priority
        });

        if (typeof (routeParams[3]) !== 'undefined') {
          const workspaceInstance = <{globalId:string, title:string}>await workspace.getById(routeParams[3]);
          if (workspaceInstance) {
            const reminderData = {
              workspaceId: workspaceInstance.globalId,
              workspaceTitle: workspaceInstance.title,
              globalId: itemInstance.globalId,
              parentId: itemInstance.parentId,
              title: itemInstance.title,
              reminder: itemSaveData.reminder
            };
            ReminderNotice.flushByGlobalId(itemInstance.globalId);
            ReminderNotice.setTimeoutByGlobalId(reminderData);
          }
        }
        socketNoteReminderMessage({workspaceId, noteGlobalId});
      });
    } else {
      callback(error.itemNotFound(), {});
    }
  });
}

/**
 * @param {{workspaceId:string, noteGlobalId:string}} inputData
 */
function socketNoteReminderMessage(inputData) {
  const {workspaceId, noteGlobalId} = inputData;
  if (noteGlobalId) {
    // socketConnection.sendNoteTagsMessage(inputData);
  }
}
