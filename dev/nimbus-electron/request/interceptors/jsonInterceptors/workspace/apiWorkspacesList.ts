import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiWorkspacesList(request, callback) {
  workspace.findAll({}, {}, async (err, workspacesList) => {
    if (err || !workspacesList) {
      return callback(error.itemNotFound(), []);
    }

    if (err || !workspacesList.length) {
      const defaultWorkspace = await workspace.getDefault();
      workspacesList = [defaultWorkspace];
    }
    const workspaces = await workspace.getResponseListJson(workspacesList);

    return callback(null, workspaces);
  });
}
