import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiInvitesList(request, callback) {
  let filterParams = urlParser.getFilterParams(request.url) || <any>{};

  if (!filterParams.workspaceId) {
    return callback(error.wrongInput(), []);
  }

  let workspaceId = filterParams.workspaceId;
  workspace.find({globalId: workspaceId}, {}, (err, workspaceInstance) => {
    if (err || !workspaceInstance) {
      return callback(error.itemNotFound(), []);
    }
    const invites = workspaceInstance.invites;
    const members = workspaceInstance.members;

    if (!invites) {
      return callback(null, []);
    }

    const membersEmailList = members.map((memberItem) => {
      return memberItem && memberItem.user ? memberItem.user.email : '';
    });

    const invitesFilter = invites.filter((inviteItem) => {
      return membersEmailList.indexOf(inviteItem.email.toLowerCase()) < 0;
    });

    return callback(null, invitesFilter);
  });
}
