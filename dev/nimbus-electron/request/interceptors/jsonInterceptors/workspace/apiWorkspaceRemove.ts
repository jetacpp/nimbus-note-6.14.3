import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as workspaceModel, default as workspace} from "../../../../db/models/workspace";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as appOnlineState} from "../../../../online/state";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import WorkspaceObjRepository from "../../../../sync/process/repositories/WorkspaceObjRepository";
import {default as appWindow} from "../../../../window/instance";
import {default as trans} from "../../../../translations/Translations";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiWorkspaceRemove(request, callback) {
  if (!appOnlineState.get()) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:workspace:message:response', {
        message: trans.get('toast__workspace__remove__offline'),
        //type: 'danger'
      });
    }
    return callback(null, {httpStatus: 404});
  }

  let routeParams = urlParser.getPathParams(request.url);
  let globalId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;

  if (!globalId) {
    return callback(error.wrongInput(), {});
  }

  let queryData = {globalId: globalId};
  workspace.find(queryData, {}, async (err, workspaceItem) => {
    if (workspaceItem && Object.keys(workspaceItem).length) {
      if (await workspace.isMainForUser(workspaceItem)) {
        return callback(null, {httpStatus: 404});
      }

      await WorkspaceObjRepository.removeWorkspaceData({item: workspaceItem});

      const removeQueryData = {
        globalId: globalId,
        erised: {"$in": [true, false]}
      };
      workspace.erase(removeQueryData, {}, () => {
        NimbusSDK.getApi().workspaceDelete({
          globalId: workspaceItem.globalId,
          skipSyncHandlers: true
        }, (err, res) => {
          if (err || !res) {
            socketUserWorkspaceMessage({workspaceItem});
            return callback(error.itemRemoveError(), {});
          }

          socketUserWorkspaceMessage({workspaceItem});
          callback(null, {});
        });
      });
    } else {
      callback(error.itemNotFound(), {});
    }
  });
}

/**
 * @param {{workspaceItem:workspace}} inputData
 */
function socketUserWorkspaceMessage(inputData) {
  const {workspaceItem} = inputData;
  if (workspaceItem) {
    //socketConnection.sendWorkspaceRemoveMessage(workspaceItem.globalId);
  }
}
