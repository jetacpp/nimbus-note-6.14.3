import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as workspace} from "../../../../db/models/workspace";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as appOnlineState} from "../../../../online/state";
import {default as appWindow} from "../../../../window/instance";
import {default as trans} from "../../../../translations/Translations";
import SelectedWorkspace from "../../../../workspace/SelectedWorkspace";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiWorkspaceInviteRemove(request, callback) {
  if (!appOnlineState.get()) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:workspace:message:response', {
        message: trans.get('toast__workspace__member__update__offline'),
        //type: 'danger'
      });
    }
    return callback(null, {httpStatus: 404});
  }

  let routeParams = urlParser.getPathParams(request.url);
  let globalId = typeof (routeParams[3] !== 'undefined') && routeParams[3] ? parseInt(routeParams[3]) : null;
  let workspaceId = await SelectedWorkspace.getGlobalId();

  if (!globalId) {
    return callback(error.wrongInput(), {});
  }

  let workspaceItem;
  if (workspaceId) {
    workspaceItem = await workspace.getById(workspaceId);
  } else {
    workspaceItem = await workspace.getDefault();
  }

  if (!workspaceItem) {
    return callback(error.itemNotFound(), {});
  }

  let curDate = date.now();

  const invites = workspaceItem.invites;

  let userExistInInvites = false;
  if (invites) {
    for (let i in invites) {
      if (invites[i].id === globalId) {
        userExistInInvites = true;
        delete invites[i];
        break;
      }
    }
  }

  if (!userExistInInvites) {
    return callback(null, {
      httpStatus: 400,
      name: 'UniqueError',
      objectType: 'Invite'
    });
  }

  NimbusSDK.getApi().workspaceInviteDelete({globalId, skipSyncHandlers: true}, (err, res) => {
    if (err || !res) {
      return callback(error.itemRemoveError(), {});
    }

    const updateQuery = {globalId: workspaceItem.globalId};
    workspace.update(updateQuery, setUpdateProps({invites}, curDate), {}, (err, updateCount) => {
      if (err || !updateCount) {
        return callback(error.itemRemoveError(), {
          httpStatus: 400,
          name: 'UniqueError',
          objectType: 'Invite'
        });
      }

      callback(null, {});
    });
  });
}
