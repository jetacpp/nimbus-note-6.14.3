import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as workspace} from "../../../../db/models/workspace";
import {default as orgs} from "../../../../db/models/orgs";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as appOnlineState} from "../../../../online/state";
import {default as appWindow} from "../../../../window/instance";
import {default as trans} from "../../../../translations/Translations";
import WorkspaceObjRepository from "../../../../sync/process/repositories/WorkspaceObjRepository";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";
import SelectedOrganization from "../../../../organization/SelectedOrganization";
import {default as appPage} from "../../../page";
import SelectedWorkspace from "../../../../workspace/SelectedWorkspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiWorkspaceMemberRemove(request, callback) {
  if (!appOnlineState.get()) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:workspace:message:response', {
        message: trans.get('toast__workspace__member__update__offline'),
        //type: 'danger'
      });
    }
    return callback(null, {httpStatus: 404});
  }

  if (!request.authInfo) {
    return callback(error.wrongInput(), {});
  }

  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;
  let globalId = typeof (routeParams[5] !== 'undefined') && routeParams[5] ? routeParams[5] : null;

  if (!globalId) {
    return callback(error.wrongInput(), {});
  }

  let workspaceItem;
  if (workspaceId) {
    workspaceItem = await workspace.getById(workspaceId);
  } else {
    workspaceItem = await workspace.getDefault();
  }

  if (!workspaceItem) {
    return callback(error.itemNotFound(), {});
  }

  let curDate = date.now();

  const members = workspaceItem.members;

  let userExistInMembers = false;
  let userEmailInMembers = '';
  if (members) {
    for (let i in members) {
      if (members[i].globalId === globalId) {
        if (members[i].user && members[i].user.email) {
          userEmailInMembers = members[i].user.email.toLowerCase();
        }
        userExistInMembers = true;
        delete members[i];
        break;
      }
    }
  }

  if (!userExistInMembers) {
    return callback(null, {
      httpStatus: 400,
      name: 'UniqueError',
      objectType: 'Member'
    });
  }

  NimbusSDK.getApi().workspaceMemberDelete({memberId: globalId, skipSyncHandlers: true}, async (err, res) => {
    if (err || !res) {
      return callback(error.itemRemoveError(), {});
    }

    if (request.authInfo.email.toLowerCase() === userEmailInMembers.toLowerCase()) {
      if (await workspace.isMainForUser(workspaceItem)) {
        return callback(error.itemRemoveError(), {httpStatus: 400});
      } else {
        await WorkspaceObjRepository.removeWorkspaceData({item: workspaceItem});
        const removeQueryData = {globalId: workspaceItem.globalId, erised: {"$in": [true, false]}};

        const org = <{type}>await orgs.getByWorkspaceId(workspaceId);
        let isBusinessOrg = org && org.type === orgs.TYPE_BUSINESS;
        if(!org) { isBusinessOrg = workspaceItem.orgId && workspaceItem.orgId.charAt(0) === 'b'; }

        workspace.erase(removeQueryData, {}, async () => {
          socketUserWorkspaceMessage({workspaceItem});

          if(isBusinessOrg) {
            const countWorkspaces = await workspace.countUserWorkspacesByOrgId(workspaceItem.orgId);
            const selectedWorkspaceId = await SelectedWorkspace.getGlobalId();

            if(countWorkspaces) {
              await SelectedOrganization.set(workspaceItem.orgId);
            } else {
              await SelectedOrganization.set(null);
            }

            if(!countWorkspaces || (selectedWorkspaceId === workspaceItem.globalId)) {
              await appPage.reload(false);
            }
          }

          callback(null, {});
        });
      }
    } else {
      const itemQuery = {globalId: workspaceItem.globalId};
      let {countMembers} = workspaceItem;
      if(countMembers) {
        countMembers = countMembers - 1;
      }
      let updateData = {members, countMembers};
      workspace.update(itemQuery, setUpdateProps(updateData, curDate), {workspaceId}, (err, updateCount) => {
        if (err || !updateCount) {
          return callback(error.itemRemoveError(), {
            httpStatus: 400,
            name: 'UniqueError',
            objectType: 'Member'
          });
        }

        callback(null, {});
      });
    }
  });
}

/**
 * @param {{workspaceItem:workspace}} inputData
 */
function socketUserWorkspaceMessage(inputData) {
  const {workspaceItem} = inputData;
  if (workspaceItem) {
    //socketConnection.sendWorkspaceRemoveMessage(workspaceItem.globalId);
  }
}
