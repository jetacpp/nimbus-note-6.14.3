import chalk from "chalk";
import {default as workspace} from "../../../../db/models/workspace";
import {default as error} from "../../../../utilities/customError";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as MoveToWorkspace} from "../../../../workspace/MoveToWorkspace";
import {default as AutoSyncRequest} from "../../../../sync/ipc/ipcFunctions";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiWorkspaceChange(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;
  if (!workspaceId) {
    return callback(error.wrongInput(), []);
  }

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  const {nodesIds, parentId, isMove, nodeType} = request.body;
  const toWorkspaceId = request.body.workspaceId;

  if (!nodesIds || !parentId || !toWorkspaceId) {
    return callback(error.wrongInput(), {});
  }

  if(typeof (isMove) === 'undefined') {
    return callback(error.wrongInput(), {});
  }

  if (!nodesIds.length) {
    return callback(error.wrongInput(), {});
  }

  let queryData = {globalId: workspaceId};
  workspace.find(queryData, {}, async (err, workspaceItem) => {
    if (err || !workspaceItem) {
      return callback(null, {httpStatus: 404});
    }

    for(let nodesId of nodesIds) {
      const response = <{err:any, errorDesc:string, newGlobalId:string, isElectron?: boolean}>await MoveToWorkspace.process({
        fromWorkspaceId: workspaceId,
        toWorkspaceId,
        globalId: nodesId,
        isMove
      });


      if (response.err) {
        let respDesc = '';
        if(response.errorDesc === 'contains_encrypted_notes') {
          respDesc = 'HAVE_ENCRYPTED_NOTE';
        }

        if(respDesc) {
          return callback(null, {
            httpStatus: 400,
            message: {
              err: response.err,
              name: respDesc,
              httpStatus: 400
            },
            name: respDesc,
            response: {
              err: response.err,
              name: respDesc,
              httpStatus: 400
            },
            status: 400
          });
        }

        if(response.errorDesc === 'too_many_notes') {
          return callback(null, {
            httpStatus: 403,
            message: "nodeWorkspaceChangeError",
            err: response.err,
            name: 'LimitError',
            objectType: 'NotesNumber',
            errorDesc: response.errorDesc,
            newGlobalId: response.newGlobalId,
            isElectron: response.isElectron,
          });
        }

        if(response.errorDesc === 'org_suspended') {
          return callback(null, {
            httpStatus: 403,
            err: response.err,
            name: 'AccessDenied',
            objectType: 'note',
            reason: 'NoPrivilege',
            nodeType,
            isMove,
            privilegesReduced: true,
            errorDesc: response.errorDesc,
            newGlobalId: response.newGlobalId,
            isElectron: response.isElectron,
          });
        }

        return callback(null, {
          httpStatus: 400,
          message: "nodeWorkspaceChangeError",
          err: response.err,
          errorDesc: response.errorDesc,
          newGlobalId: response.newGlobalId,
          isElectron: response.isElectron,
        });
      }
    }

    AutoSyncRequest.requestAutoSync({workspaceId});

    return callback(null, []);
  });
}
