import chalk from "chalk";
import {default as workspace} from "../../../../db/models/workspace";
import {default as error} from "../../../../utilities/customError";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as appOnlineState} from "../../../../online/state";
import {default as appWindow} from "../../../../window/instance";
import {default as trans} from "../../../../translations/Translations";
import SelectedWorkspace from "../../../../workspace/SelectedWorkspace";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as workspaceInvites} from "../../../../db/models/workspaceInvites";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiWorkspaceInviteResend(request, callback) {
  if (!appOnlineState.get()) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:workspace:message:response', {
        message: trans.get('toast__workspace__member__update__offline'),
        //type: 'danger'
      });
    }
    return callback(null, {httpStatus: 404});
  }

  let routeParams = urlParser.getPathParams(request.url);
  let globalId = typeof (routeParams[3] !== 'undefined') && routeParams[3] ? parseInt(routeParams[3]) : null;
  if (!globalId) {
    return callback(error.wrongInput(), {});
  }

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  let workspaceId = await SelectedWorkspace.getGlobalId();

  let workspaceItem;
  if (workspaceId) {
    workspaceItem = await workspace.getById(workspaceId);
  } else {
    workspaceItem = await workspace.getDefault();
  }

  if (!workspaceItem) {
    return callback(null, {httpStatus: 404});
  }

  const invites = workspaceItem.invites;

  let existInvite = null;
  if (invites) {
    for (let i in invites) {
      if (invites[i].id === globalId) {
        existInvite = invites[i];
        break;
      }
    }
  }

  if (!existInvite) {
    return callback(null, {
      httpStatus: 400,
      name: 'UniqueError',
      objectType: 'Invite'
    });
  }

  NimbusSDK.getApi().workspaceInviteResend({globalId, skipSyncHandlers: true}, async (err, res) => {
    if (err || !res) {
      if(err === workspaceInvites.ERROR_LIMIT) {
        return callback(error.itemUpdateError(), {name: 'LimitError', httpStatus: 400});
      } else if(err === workspaceInvites.ERROR_UNIQUE) {
        return callback(error.itemUpdateError(), {name: 'UniqueError', httpStatus: 400});
      } else {
        return callback(error.itemUpdateError(), {httpStatus: 400});
      }
    }

    callback(null, {invite: existInvite});
  });
}
