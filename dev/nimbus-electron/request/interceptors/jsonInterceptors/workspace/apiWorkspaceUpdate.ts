import chalk from "chalk";
import {default as workspace} from "../../../../db/models/workspace";
import {default as orgs} from "../../../../db/models/orgs";
import {default as date} from "../../../../utilities/dateHandler";
import {default as error} from "../../../../utilities/customError";
import {default as user} from "../../../../db/models/user";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as appOnlineState} from "../../../../online/state";
import {default as appWindow} from "../../../../window/instance";
import {default as trans} from "../../../../translations/Translations";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as WorkspaceObjRepository} from "../../../../sync/process/repositories/WorkspaceObjRepository";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiWorkspaceUpdate(request, callback) {
  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  if (!request.body.title) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();
  let saveData = <any>{
    "globalId": request.body.globalId || null,
    "title": request.body.title,
    "updatedAt": request.body.updatedAt ? request.body.updatedAt : curDate,
    "syncDate": curDate,
    "needSync": false
  };

  let queryData = {globalId: saveData.globalId};
  // @ts-ignore
  workspace.find(queryData, {}, async (err, workspaceItem) => {
    if (workspaceItem && Object.keys(workspaceItem).length) {
      if (!appOnlineState.get()) {
        if (appWindow.get()) {
          appWindow.get().webContents.send('event:client:workspace:message:response', {
            message: trans.get('toast__workspace__update__offline'),
            //type: 'danger'
          });
        }
        return callback(null, {httpStatus: 404});
      }

      let apiUpdateData = {
        globalId: workspaceItem.globalId,
        title: saveData.title
      };

      NimbusSDK.getApi().workspaceUpdate({body: apiUpdateData, skipSyncHandlers: true}, (err, res) => {
        if (err || !res) {
          return callback(error.itemUpdateError(), {httpStatus: 500});
        }

        workspace.update(queryData, setUpdateProps(saveData, curDate), {}, (err, count) => {
          if (!count) {
            return callback(error.itemUpdateError(), {});
          }
          // @ts-ignore
          workspace.find(queryData, {}, async (err, workspaceItem) => {
            if (!workspaceItem) {
              return callback(error.itemNotFound(), workspaceItem);
            }

            callback(null, await workspace.getResponseJson(workspaceItem));
            socketUserWorkspaceMessage({workspaceItem, isCreate: false});
          });
        });
      });
    } else {
      if (!appOnlineState.get()) {
        if (appWindow.get()) {
          appWindow.get().webContents.send('event:client:workspace:message:response', {
            message: trans.get('toast__workspace__create__offline'),
            //type: 'danger'
          });
        }
        return callback(null, {httpStatus: 404});
      }

      const orgInstance = <any>await orgs.getActive();
      if (!orgInstance) {
        return callback(error.itemSaveError(), {httpStatus: 400});
      }

      saveData.orgId = orgInstance.id;
      let findQuery = {login: request.authInfo.email};
      user.find(findQuery, {}, (err, userItem) => {

        saveData.userId = orgInstance && orgInstance.user && orgInstance.user.id ? orgInstance.user.id : null;
        if(!saveData.userId) {
          saveData.userId = userItem ? userItem.userId : null;
        }
        saveData.isDefault = false;
        saveData.defaultEncryptionKeyId = null;
        saveData.isNotesLimited = true;

        let apiSaveData = <any>{title: saveData.title};
        if (saveData.orgId) {
          apiSaveData.orgId = saveData.orgId;
        }

        /**
         * @param {string} orgId
         */
          // @ts-ignore
        const getUserWorkspacesCount = async (orgId) => {
            // @ts-ignore
            return new Promise(async (resolve) => {
              workspace.count({orgId}, {}, (err, count) => {
                if (err || !count) {
                  return resolve(0);
                }
                resolve(count);
              });
            });
          };

        NimbusSDK.getApi().workspaceCreate({body: apiSaveData, skipSyncHandlers: true}, async (err, res) => {
          if (err === workspace.ERROR_QUOTA_EXCEED) {
            return callback(error.itemSaveError(), {
              httpStatus: 400,
              limitSize: orgInstance.limits.workspaces,
              name: "LimitError",
              objectGlobalId: orgInstance.id,
              objectSize: await getUserWorkspacesCount(orgInstance.id),
              objectType: "Workspace",
              premiumSize: null
            });
          }

          if (err || !res) {
            return callback(error.itemSaveError(), {httpStatus: 500});
          }

          saveData.globalId = res.workspace.globalId;
          saveData.access = workspace.getDefaultAccess();

          // @ts-ignore
          workspace.add(setUpdateProps(saveData, curDate), {}, async (err, workspaceItem) => {
            if (workspaceItem && Object.keys(workspaceItem).length) {
              callback(null, await workspace.getResponseJson(workspaceItem));
              socketUserWorkspaceMessage({workspaceItem, isCreate: true});
            } else {
              callback(null, {});
            }
          });
        });
      });
    }
  });
}

/**
 * @param {{workspaceItem:workspace}} inputData
 */
function socketUserWorkspaceMessage(inputData) {
  const {workspaceItem, isCreate} = inputData;
  if (workspaceItem) {
    WorkspaceObjRepository.sendSocketEventOnUpdate(workspaceItem.globalId, isCreate)
  }
}
