import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as workspace} from "../../../../db/models/workspace";
import {default as workspaceMember} from "../../../../db/models/workspaceMember";
import {default as error} from "../../../../utilities/customError";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiMembersList(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;
  if (!workspaceId) {
    return callback(error.wrongInput(), []);
  }

  workspace.find({globalId: workspaceId}, {}, (err, workspaceInstance) => {
    if (err || !workspaceInstance) {
      return callback(error.itemNotFound(), []);
    }

    if (!workspaceInstance.members) {
      return callback(null, []);
    }

    return callback(null, workspaceMember.getResponseListJson(workspaceInstance.members));
  });
}
