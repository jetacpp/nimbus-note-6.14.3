import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as user} from "../../../../db/models/user";
import {default as orgs} from "../../../../db/models/orgs";
import workspace from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiWorkspaceEmail(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;

  let originalWorkspaceId =  typeof (routeParams[3] !== 'undefined') ? routeParams[3] : '';

  if (!request.authInfo) {
    return callback(error.wrongInput(), {});
  }

  const workspaceInstance = <{notesEmail}>await workspace.getById(workspaceId);
  if(!workspaceInstance) {
    return callback(error.itemNotFound(), {});
  }

  if(!workspaceInstance.notesEmail) {
    return callback(error.itemNotFound(), {});
  }

  const { notesEmail } = workspaceInstance;
  let findQuery = {login: request.authInfo.email};
  // @ts-ignore
  user.find(findQuery, {}, async (err, userItem) => {
    if (userItem && Object.keys(userItem).length) {
      let org = <any>await orgs.getActive();
      let response = {
        notesEmail: notesEmail ? user.getNotesEmailPart(notesEmail) : '',
        userId: org && org.user ? org.user.id : userItem.userId,
        workspaceId: originalWorkspaceId,
      };
      return callback(null, response);
    } else {
      return callback(error.itemNotFound(), {});
    }
  });
}
