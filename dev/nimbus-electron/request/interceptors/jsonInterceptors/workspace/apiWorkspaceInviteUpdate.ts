import chalk from "chalk";
import {default as workspace} from "../../../../db/models/workspace";
import {default as error} from "../../../../utilities/customError";
import {default as workspaceMember} from "../../../../db/models/workspaceMember";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as appOnlineState} from "../../../../online/state";
import {default as appWindow} from "../../../../window/instance";
import {default as trans} from "../../../../translations/Translations";
import SelectedWorkspace from "../../../../workspace/SelectedWorkspace";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as WorkspaceObjRepository} from "../../../../sync/process/repositories/WorkspaceObjRepository";
import {default as workspaceInvites} from "../../../../db/models/workspaceInvites";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiWorkspaceInviteUpdate(request, callback) {
  if (!appOnlineState.get()) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:workspace:message:response', {
        message: trans.get('toast__workspace__member__update__offline'),
        //type: 'danger'
      });
    }
    return callback(null, {httpStatus: 404});
  }

  let routeParams = urlParser.getPathParams(request.url);
  let globalId = typeof (routeParams[3] !== 'undefined') && routeParams[3] ? parseInt(routeParams[3]) : null;
  if (!globalId) {
    return callback(error.wrongInput(), {});
  }

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  let workspaceId = await SelectedWorkspace.getGlobalId();

  let role = request.body.role;
  if (!role) {
    role = workspaceMember.ROLE_READER;
  }

  let curDate = date.now();

  let workspaceItem;
  if (workspaceId) {
    workspaceItem = await workspace.getById(workspaceId);
  } else {
    workspaceItem = await workspace.getDefault();
  }

  if (!workspaceItem) {
    return callback(null, {httpStatus: 404});
  }

  const invites = workspaceItem.invites;

  let existInvite = false;
  if (invites) {
    for (let i in invites) {
      if (invites[i].id === globalId) {
        invites[i].role = role;
        existInvite = invites[i];
        break;
      }
    }
  }

  if (!existInvite) {
    return callback(null, {
      httpStatus: 400,
      objectType: 'Invite'
    });
  }

  let apiUpdateData = {
    id: globalId,
    role: role,
    encryptRole: 'encryptionDeny'
  };

  NimbusSDK.getApi().workspaceInviteUpdate({body: apiUpdateData, skipSyncHandlers: true}, async (err, res) => {
    if (err || !res) {
      if(err === workspaceInvites.ERROR_LIMIT) {
        return callback(error.itemUpdateError(), {name: 'LimitError', httpStatus: 400, objectType: 'Invite'});
      } else if(err === workspaceInvites.ERROR_UNIQUE) {
        return callback(error.itemUpdateError(), {name: 'UniqueError', httpStatus: 400, objectType: 'Invite'});
      } else {
        return callback(error.itemUpdateError(), {httpStatus: 400, objectType: 'Invite'});
      }
    }

    const updateQuery = {globalId: workspaceItem.globalId};
    workspace.update(updateQuery, setUpdateProps({invites}, curDate), {}, (err, updateCount) => {
      if (err || !updateCount) {
        return callback(error.itemUpdateError(), {httpStatus: 400, objectType: 'Invite'});
      }

      callback(null, existInvite);
      socketUserWorkspaceInviteMessage({workspaceId: workspaceItem.globalId});
    });
  });
}

/**
 * @param {{workspaceId:string}} inputData
 */
function socketUserWorkspaceInviteMessage(inputData) {
  const {workspaceId} = inputData;
  if (workspaceId) {
    WorkspaceObjRepository.sendSocketEventOnUpdateInvites(workspaceId);
  }
}
