import chalk from "chalk";
import {default as workspace} from "../../../../db/models/workspace";
import {default as error} from "../../../../utilities/customError";
import {default as workspaceMember} from "../../../../db/models/workspaceMember";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as appOnlineState} from "../../../../online/state";
import {default as appWindow} from "../../../../window/instance";
import {default as trans} from "../../../../translations/Translations";
import SelectedWorkspace from "../../../../workspace/SelectedWorkspace";
import {default as urlParser} from "../../../../utilities/urlParser";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiWorkspaceMemberUpdate(request, callback) {
  if (!appOnlineState.get()) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:workspace:message:response', {
        message: trans.get('toast__workspace__member__update__offline'),
        //type: 'danger'
      });
    }
    return callback(null, {httpStatus: 404});
  }

  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let globalId = typeof (routeParams[5] !== 'undefined') && routeParams[5] ? routeParams[5] : null;
  if (!globalId) {
    return callback(error.wrongInput(), {});
  }

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  let role = request.body.role;
  if (!role) {
    role = workspaceMember.ROLE_READER;
  }

  let curDate = date.now();

  let workspaceItem;
  if (workspaceId) {
    workspaceItem = await workspace.getById(workspaceId);
  } else {
    workspaceItem = await workspace.getDefault();
  }

  if (!workspaceItem) {
    return callback(null, {httpStatus: 404});
  }

  const members = workspaceItem.members;

  let existMember = null;
  if (members) {
    for (let i in members) {
      if (members[i].globalId === globalId) {
        members[i].role = role;
        existMember = members[i];
        break;
      }
    }
  }

  if (!existMember) {
    return callback(null, {
      httpStatus: 400,
      name: 'UniqueError',
      objectType: 'Member'
    });
  }

  let apiUpdateData = {
    memberId: globalId,
    role: role
  };

  NimbusSDK.getApi().workspaceMemberUpdate({body: apiUpdateData, skipSyncHandlers: true}, async (err, res) => {
    if (err || !res) {
      return callback(error.itemUpdateError(), {
        httpStatus: 400,
        name: 'UniqueError',
        objectType: 'Member'
      });
    }

    const updateQuery = {globalId: workspaceItem.globalId};
    workspace.update(updateQuery, setUpdateProps({members}, curDate), {}, (err, updateCount) => {
      if (err || !updateCount) {
        return callback(error.itemUpdateError(), {
          httpStatus: 400,
          name: 'UniqueError',
          objectType: 'Member'
        });
      }

      callback(null, workspaceMember.getResponseJson(existMember));
    });
  });
}
