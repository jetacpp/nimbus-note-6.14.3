import chalk from "chalk";
import {default as workspace} from "../../../../db/models/workspace";
import {default as workspaceInvites} from "../../../../db/models/workspaceInvites";
import {default as error} from "../../../../utilities/customError";
import {default as workspaceMember} from "../../../../db/models/workspaceMember";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as appOnlineState} from "../../../../online/state";
import {default as appWindow} from "../../../../window/instance";
import {default as trans} from "../../../../translations/Translations";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiWorkspaceInviteAdd(request, callback) {
  if (!appOnlineState.get()) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:workspace:message:response', {
        message: trans.get('toast__workspace__member__update__offline'),
        //type: 'danger'
      });
    }
    return callback(null, {httpStatus: 404});
  }

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  let workspaceId = request.body.workspaceId;
  if (!workspaceId) {
    return callback(error.wrongInput(), {});
  }

  let email = request.body.email ? request.body.email.toLowerCase() : '';
  if (!email) {
    return callback(error.wrongInput(), {});
  }

  let role = request.body.role;
  if (!role) {
    role = workspaceMember.ROLE_READER;
  }

  let curDate = date.now();

  let queryData = {globalId: workspaceId};
  // @ts-ignore
  workspace.find(queryData, {}, (err, workspaceItem) => {
    if (err || !workspaceItem) {
      return callback(null, {httpStatus: 404});
    }

    const members = workspaceItem.members;
    const invites = workspaceItem.invites;

    let userExistInInvites = false;
    if (invites) {
      for (let invite of invites) {
        if (invite.email.toLowerCase() === email.toLowerCase()) {
          userExistInInvites = true;
          break;
        }
      }
    }

    if (userExistInInvites) {
      // return callback(error.itemUpdateError(), {name: 'UniqueError', httpStatus: 400});
    }

    let userExistInMembers = false;
    if (members) {
      for (let member of members) {
        if (member.user && member.user.email && (member.user.email.toLowerCase() === email.toLowerCase())) {
          userExistInMembers = true;
          break;
        }
      }
    }

    if (userExistInInvites || userExistInMembers) {
      // return callback(error.itemUpdateError(), {name: 'UniqueError', httpStatus: 400});
    }

    let apiUpdateData = {
      workspaceId: workspaceItem.globalId,
      role: role,
      email: email
    };

    NimbusSDK.getApi().workspaceMemberAdd({body: apiUpdateData, skipSyncHandlers: true}, async (err, res) => {
      if (err || !res) {
        if(err === workspaceInvites.ERROR_LIMIT) {
          return callback(error.itemUpdateError(), {name: 'LimitError', httpStatus: 400});
        } else if(err === workspaceInvites.ERROR_UNIQUE) {
          return callback(error.itemUpdateError(), {name: 'UniqueError', httpStatus: 400});
        } else {
          return callback(error.itemUpdateError(), {httpStatus: 400});
        }
      }

      invites.push(res.invite);

      const updateQuery = {globalId: workspaceItem.globalId};
      workspace.update(updateQuery, setUpdateProps({invites}, curDate), {}, (err, updateCount) => {
        if (err || !updateCount) {
          return callback(error.itemUpdateError(), {httpStatus: 400});
        }

        callback(null, {
          invite: {
            ...res.invite,
            result: res.result
          }
        });
      });
    });
  });
}
