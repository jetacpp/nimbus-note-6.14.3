import chalk from "chalk";
import fs = require('fs-extra');
import {default as config} from "../../../../../config.runtime";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as item} from "../../../../db/models/item";
import {default as text} from "../../../../db/models/text";
import {default as todo} from "../../../../db/models/todo";
import {default as attach} from "../../../../db/models/attach";
import {default as noteTags} from "../../../../db/models/noteTags";
import {default as workspace} from "../../../../db/models/workspace";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {default as PouchDb} from "../../../../../pdb";
//import {default as ipcConnection} from "./../../../../sync/ipc/ipcFunctions";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiItemRemove(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;
  let filterParams = urlParser.getFilterParams(request.url) || <any>{};

  if (!globalId) {
    return callback(error.wrongInput(), {});
  }

  if (filterParams.test) {
    deleteTestData({workspaceId, globalId}, callback);
  } else {

    /**
     * @param {{workspaceId:string, globalId:string}} inputData
     */
    let getTrashItemByGlobalId = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, globalId} = inputData;
        item.find({rootId: 'trash', 'globalId': globalId}, {workspaceId}, (err, trashItem) => {
          if (err) {
            return resolve(null);
          }
          resolve(trashItem);
        });
      });
    };

    /**
     * @param {{workspaceId:string, parentId:string}} inputData
     */
    let getTrashItemsByParentId = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, parentId} = inputData;
        item.findAll({rootId: 'trash', 'parentId': parentId}, {workspaceId}, (err, itemList) => {
          if (err) {
            return resolve([]);
          }
          resolve(itemList);
        });
      });
    };

    let trashNestedItems = [];

    /**
     * @param {{workspaceId:string, trashItem:item}} inputData
     */
      // @ts-ignore
    let getAllNestedItems = async (inputData) => {
        const {workspaceId, trashItem} = inputData;
        if (trashItem) {
          trashNestedItems.push(trashItem);
          let trashItemList = <{}>await getTrashItemsByParentId({workspaceId, parentId: trashItem.globalId});
          for (let k in trashItemList) {
            if (trashItemList.hasOwnProperty(k)) {
              await getAllNestedItems({workspaceId, trashItem: trashItemList[k]});
            }
          }
        }
      };

    let trashItem = <{offlineOnly:boolean}>await getTrashItemByGlobalId({workspaceId, globalId});
    await getAllNestedItems({workspaceId, trashItem});

    let trashNestedItemsIdList = [];
    for (let t in trashNestedItems) {
      let trashNestedItem = trashNestedItems[t];

      if (trashNestedItem.globalId === "default") {
        continue;
      }

      trashNestedItemsIdList.push(trashNestedItem.globalId);

      if (trashNestedItem.type === "note") {
        let removeItemAssetsQuery = {noteGlobalId: trashNestedItem.globalId};
        attach.findAll(removeItemAssetsQuery, {workspaceId}, (err, attachList) => {
          if (attachList && attachList.length) {
            for (let n in attachList) {
              if (attachList.hasOwnProperty(n)) {
                let attachItem = attachList[n];
                let removeAttachQuery = {globalId: attachItem.globalId};
                attach.remove(removeAttachQuery, {workspaceId}, async () => {
                  if (PouchDb.getClientAttachmentPath()) {
                    let targetPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
                    // @ts-ignore
                    let exists = await fs.exists(targetPath);
                    // @ts-ignore
                    if (exists) {
                      try {
                        await fs.unlink(targetPath);
                      } catch (e) {
                        if (config.SHOW_WEB_CONSOLE) {
                          console.log("Error => apiItemRemove => remove file: ", targetPath, e);
                        }
                      }
                    }
                  }
                });
              }
            }
          }
        });
        todo.removeByItemId(removeItemAssetsQuery, {workspaceId}, () => {
        });
        attach.removeByItemId(removeItemAssetsQuery, {workspaceId}, () => {
        });
        noteTags.removeByItemId(removeItemAssetsQuery, {workspaceId}, () => {
        });
        text.remove(removeItemAssetsQuery, {workspaceId}, () => {
        });
      }

      item.remove({globalId: trashNestedItem.globalId}, {workspaceId}, () => {
      });
    }

    if (trashItem && !trashItem.offlineOnly) {
      setTimeout(() => {
        ipcMessage({workspaceId});
      }, 1000);
    }
    socketMessage({workspaceId, globalIdList: trashNestedItemsIdList});
    callback(null, {});
  }
}

/**
 * @param {{workspaceId:string, globalId:string}} inputData
 * @param {Function} callback
 */
function deleteTestData(inputData, callback) {
  const {workspaceId, globalId} = inputData;
  item.find({globalId: globalId}, {workspaceId}, (err, itemInstance) => {
    if (!itemInstance) {
      if (config.SHOW_WEB_CONSOLE) {
        console.log("Error remove item not found: " + globalId);
      }
    }

    item.remove({globalId: globalId}, {workspaceId}, () => {
      if (itemInstance && itemInstance.type === "note") {
        let removeItemAssetsQuery = {noteGlobalId: itemInstance.globalId};
        text.remove(removeItemAssetsQuery, {workspaceId}, () => {
        });
        todo.removeByItemId(removeItemAssetsQuery, {workspaceId}, () => {
        });
        attach.findAll(removeItemAssetsQuery, {workspaceId}, (err, attachList) => {
          if (attachList && attachList.length) {
            for (let n in attachList) {
              if (attachList.hasOwnProperty(n)) {
                let attachItem = attachList[n];
                let removeAttachQuery = {globalId: attachItem.globalId};
                attach.remove(removeAttachQuery, {workspaceId}, async () => {
                  if (PouchDb.getClientAttachmentPath()) {
                    let targetPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
                    // @ts-ignore
                    let exists = await fs.exists(targetPath);
                    // @ts-ignore
                    if (exists) {
                      try {
                        fs.unlink(targetPath, () => {
                        });
                      } catch (e) {
                        if (config.SHOW_WEB_CONSOLE) {
                          console.log("Error => apiItemRemove => deleteTestData => remove file: ", targetPath, e);
                        }
                      }
                    }
                  }
                });
              }
            }
          }
        });
        attach.removeByItemId(removeItemAssetsQuery, {workspaceId}, () => {
        });
        noteTags.removeByItemId(removeItemAssetsQuery, {workspaceId}, () => {
        });
      }

      callback(null, item.getResponseJson(itemInstance));
    });
  });
}

/**
 * @param {{workspaceId:string}} inputData
 */
function ipcMessage(inputData) {
  //ipcConnection.requestAutoSync(inputData);
}

/**
 * @param {{workspaceId:string, globalIdList:[string]}} inputData
 */
function socketMessage(inputData) {
  socketConnection.sendItemRemoveMessage(inputData);
}

