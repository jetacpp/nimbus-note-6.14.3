import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as date} from "../../../../utilities/dateHandler";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as workspace} from "../../../../db/models/workspace";
import {default as item} from "../../../../db/models/item";
import {default as text} from "../../../../db/models/text";
import {default as textEditor} from "../../../../db/models/textEditor";
import {default as attach} from "../../../../db/models/attach";
import {default as todo} from "../../../../db/models/todo";
import {default as noteTags} from "../../../../db/models/noteTags";
import {default as generator} from "../../../../utilities/generatorHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
// import {default as ipcConnection} from "./../../../../sync/ipc/ipcFunctions";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiItemDuplicate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);

  if (!request.body.nodesIds) {
    return callback(error.wrongInput(), {});
  }

  if(!request.body.nodesIds.length) {
    return callback(error.wrongInput(), {});
  }

  if(!request.body.parentId) {
    return callback(error.wrongInput(), {});
  }

  if(!request.body.workspaceId) {
    return callback(error.wrongInput(), {});
  }

  let {nodesIds, parentId, suffix, workspaceId} = request.body;
  const globalId = request.body.nodesIds[0];
  workspaceId = typeof (workspaceId !== 'undefined') ? await workspace.getLocalId(workspaceId) : null;
  suffix = suffix || '(copy)';

  // response note details
  let nestedItems = [];

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  let getItemByGlobalId = async (inputData) => {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, globalId} = inputData;
      item.find({globalId}, {workspaceId}, (err, itemInstance) => {
        if (err) {
          return resolve(null);
        }
        resolve(itemInstance);
      });
    });
  };

  /**
   * @param {{workspaceId:string, parentId:string}} inputData
   */
  let getItemsByParentId = async (inputData) => {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, parentId} = inputData;
      item.findAll({parentId}, {workspaceId}, (err, itemList) => {
        if (err) {
          return resolve([]);
        }
        resolve(itemList);
      });
    });
  };

  /**
   * @param {{workspaceId:string, itemInstance:item}} inputData
   */
    // @ts-ignore
  let getAllNestedItems = async (inputData) => {
      const {workspaceId, itemInstance} = inputData;
      if (itemInstance) {
        nestedItems.push(itemInstance);
        let itemList = <{}>await getItemsByParentId({workspaceId, parentId: itemInstance.globalId});
        for (let k in itemList) {
          if (itemList.hasOwnProperty(k)) {
            await getAllNestedItems({workspaceId, itemInstance: itemList[k]});
          }
        }
      }
    };

  /**
   * @param {{workspaceId:string, copyInstance:item, suffix:string}} inputData
   */
    // @ts-ignore
  let copyItem = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, copyInstance, suffix} = inputData;
        let curDate = date.now();
        let saveItemData = {...copyInstance};
        saveItemData._id = generator.randomString(16);
        saveItemData.globalId = saveItemData._id;
        if(suffix) {
          saveItemData.title = `${saveItemData.title} ${suffix}`;
        }
        saveItemData.createdAt = curDate;
        saveItemData.dateAdded = curDate;
        saveItemData.dateUpdated = curDate;
        saveItemData.updatedAt = curDate;
        saveItemData.dateUpdatedUser = curDate;
        saveItemData.editnote = 1;
        saveItemData.text_version = 1;
        saveItemData.syncDate = null;
        saveItemData.needSync = true;
        item.add(setUpdateProps(saveItemData, curDate), {workspaceId}, (err, itemInstance) => {
          if(err || !itemInstance) {
            return resolve(null);
          }
          return resolve(itemInstance);
        });
      });
    };

  /**
   * @param {{workspaceId:string, noteGlobalId:string, newNoteGlobalId:string}} inputData
   */
    // @ts-ignore
  let copyAttachments = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, noteGlobalId, newNoteGlobalId} = inputData;
        attach.findAll({noteGlobalId}, {workspaceId}, (err, attachList) => {
          if (attachList && attachList.length) {
            for (let attachItem of attachList) {
              let curDate = date.now();
              let saveAttachData = {...attachItem};
              saveAttachData._id = generator.randomString(16);
              saveAttachData.globalId = saveAttachData._id;
              saveAttachData.noteGlobalId = newNoteGlobalId;
              saveAttachData.dateAdded = curDate;
              saveAttachData.dateUpdated = curDate;
              saveAttachData.syncDate = null;
              saveAttachData.needSync = true;
              // TODO: wait until save note attach
              attach.add(setUpdateProps(saveAttachData, curDate), {workspaceId}, () => {});
            }
          }
          return resolve(true);
        });
      });
    };

  /**
   * @param {{workspaceId:string, noteGlobalId:string, newNoteGlobalId:string}} inputData
   */
    // @ts-ignore
  let copyTodos = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, noteGlobalId, newNoteGlobalId} = inputData;
        todo.findAll({noteGlobalId}, {workspaceId}, (err, todoList) => {
          if (todoList && todoList.length) {
            for (let todoItem of todoList) {
              let curDate = date.now();
              let saveTodoData = {...todoItem};
              saveTodoData._id = generator.randomString(16);
              saveTodoData.globalId = saveTodoData._id;
              saveTodoData.noteGlobalId = newNoteGlobalId;
              saveTodoData.date = curDate;
              saveTodoData.dateAdded = curDate;
              saveTodoData.dateUpdated = curDate;
              saveTodoData.syncDate = null;
              saveTodoData.needSync = true;
              // TODO: wait until save note todos
              todo.add(setUpdateProps(saveTodoData, curDate), {workspaceId}, () => {});
            }
          }
          return resolve(true);
        });
      });
    };

  /**
   * @param {{workspaceId:string, noteGlobalId:string, newNoteGlobalId:string}} inputData
   */
    // @ts-ignore
  let copyNoteTags = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, noteGlobalId, newNoteGlobalId} = inputData;
        noteTags.findAll({noteGlobalId}, {workspaceId}, (err, noteTagsList) => {
          if (noteTagsList && noteTagsList.length) {
            for (let noteTagItem of noteTagsList) {
              let curDate = date.now();
              let saveNoteTagData = {...noteTagItem};
              saveNoteTagData._id = generator.randomString(16);
              saveNoteTagData.globalId = saveNoteTagData._id;
              saveNoteTagData.noteGlobalId = newNoteGlobalId;
              saveNoteTagData.dateAdded = curDate;
              saveNoteTagData.dateUpdated = curDate;
              saveNoteTagData.syncDate = null;
              saveNoteTagData.needSync = true;
              // TODO: wait until save note tags
              noteTags.add(setUpdateProps(saveNoteTagData, curDate), {workspaceId}, () => {});
            }
          }
          return resolve(true);
        });
      });
    };

  /**
   * @param {{workspaceId:string, noteGlobalId:string, newNoteGlobalId:string}} inputData
   */
    // @ts-ignore
  let copyText = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, noteGlobalId, newNoteGlobalId} = inputData;
        text.findAll({noteGlobalId}, {workspaceId}, (err, textList) => {
          if (textList && textList.length) {
            for (let textItem of textList) {
              let curDate = date.now();
              let saveTextData = {...textItem};
              saveTextData._id = generator.randomString(16);
              saveTextData.globalId = saveTextData._id;
              saveTextData.noteGlobalId = newNoteGlobalId;
              saveTextData.dateAdded = curDate;
              saveTextData.dateUpdated = curDate;
              saveTextData.syncDate = null;
              saveTextData.needSync = true;
              // TODO: wait until save text for old editor
              text.add(setUpdateProps(saveTextData, curDate), {workspaceId}, () => {});
            }
          }
          return resolve(true);
        });
      });
    };

  /**
   * @param {{workspaceId:string, noteGlobalId:string, newNoteGlobalId:string}} inputData
   */
    // @ts-ignore
  let copyTextEditor = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, noteGlobalId, newNoteGlobalId} = inputData;
        textEditor.findAll({noteGlobalId}, {workspaceId}, (err, textList) => {
          if (textList && textList.length) {
            for (let textItem of textList) {
              let curDate = date.now();
              let saveTextData = {...textItem};
              saveTextData._id = generator.randomString(16);
              saveTextData.noteGlobalId = newNoteGlobalId;
              saveTextData.globalId = saveTextData._id;
              saveTextData.dateUpdated = curDate;
              saveTextData.syncDate = null;
              saveTextData.needSync = true;
              // TODO: wait until save text for new editor
              textEditor.add(setUpdateProps(saveTextData, curDate), {workspaceId}, () => {});
            }
          }
          return resolve(true);
        });
      });
    };

  /**
   * @param {{workspaceId:string, copyInstance:item, suffix:string}} inputData
   */
  let copyItemWithAssets = async (inputData) => {
    let {workspaceId, copyInstance, suffix} = inputData;
    let itemInstance = <any>await copyItem({workspaceId, copyInstance, suffix});
    if (copyInstance.type === "note") {
      let copyQuery = {
        workspaceId,
        noteGlobalId: copyInstance.globalId,
        newNoteGlobalId: itemInstance.globalId
      };

      await copyAttachments(copyQuery);
      await copyTodos(copyQuery);
      await copyNoteTags(copyQuery);
      await copyText(copyQuery);
      await copyTextEditor(copyQuery);

      socketMessage({workspaceId, itemInstance});
    }
    return itemInstance;
  };

  let itemInstance = <{offlineOnly:boolean}>await getItemByGlobalId({workspaceId, globalId});
  if(!itemInstance) {
    return callback(error.itemFindError(), {});
  }
  await getAllNestedItems({workspaceId, itemInstance});

  let copyItemRoot = await copyItemWithAssets({workspaceId, copyInstance: itemInstance, suffix});
  if(!copyItemRoot) {
    return callback(error.itemSaveError(), {});
  }

  for (let nestedItem of nestedItems) {
    await copyItemWithAssets({workspaceId, copyInstance: nestedItem});
  }

  if (itemInstance && !itemInstance.offlineOnly) {
      // ipcMessage({workspaceId});
  }

  // TODO: prepare response data for note or folder
  callback(null, {});
}

/**
 * @param {{workspaceId:string}} inputData
 */
function ipcMessage(inputData) {
  //ipcConnection.requestAutoSync(inputData);
}

/**
 * @param {{workspaceId:string, globalIdList:[string]}} inputData
 */
function socketMessage(inputData) {
  const {workspaceId, itemInstance} = inputData;
  socketConnection.sendItemUpdateMessage({
    workspaceId,
    globalId: itemInstance.globalId
  });
  socketConnection.sendItemsCountMessage({
    workspaceId,
    globalId: itemInstance.globalId,
    parentId: itemInstance.parentId,
    type: itemInstance.type
  });
}
