import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiItem(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;
  item.getApiNote(globalId, {workspaceId}, callback);
}
