import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as item} from "../../../../db/models/item";
import {default as text} from "../../../../db/models/text";
import {default as workspace} from "../../../../db/models/workspace";
import {default as date} from "../../../../utilities/dateHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {default as urlParser} from "../../../../utilities/urlParser";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
//import {default as ipcConnection} from "./../../../../sync/ipc/ipcFunctions";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiItemUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let queryParams = urlParser.getQueryParams(request.url);

  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let curDate = date.now();
  let defaultTitle = "Unnamed Note";

  let requestBody = request.body;
  if(!requestBody) { return callback(error.wrongInput(), {}); }

  if(requestBody instanceof Array) { // process multiple item update
    const responseData = [];
    for(const requestBodyItem of requestBody) {
      const saveItemRespItem = await prepareAndUpdateItem({
        requestBody: requestBodyItem,
        curDate,
        queryParams,
        routeParams,
        workspaceId,
        defaultTitle,
      });
      if(!saveItemRespItem) { return callback(error.itemSaveError(), {}); }
      if(saveItemRespItem.error) { return callback(error.itemSaveError(), {}); }
      responseData.push(saveItemRespItem.data);
    }
    return callback(null, responseData);
  }

  // process one item update

  if (!requestBody.globalId) { return callback(error.wrongInput(), {}); }
  const saveItemResp = await prepareAndUpdateItem({
    requestBody,
    curDate,
    queryParams,
    routeParams,
    workspaceId,
    defaultTitle,
  });
  if(!saveItemResp) { return callback(error.itemSaveError(), {}); }
  if(saveItemResp.error) { return callback(error.itemSaveError(), {}); }
  return callback(saveItemResp.error, saveItemResp.data);
}

async function prepareAndUpdateItem(inputData) {
  const {
    requestBody,
    curDate,
    queryParams,
    routeParams,
    workspaceId,
    defaultTitle,
  } = inputData;

  let saveItemData = prepareSaveItemData({
    requestBody,
    curDate,
  });

  return <{error: Error, data: {}}>await updateItem({
    queryParams,
    routeParams,
    requestBody,
    saveItemData,
    workspaceId,
    defaultTitle,
    curDate,
  });
}

let prepareSaveItemData = (inputData) => {
  let {requestBody, curDate} = inputData;

  let {globalId} = requestBody;
  let saveItemData = <any>{
    "globalId": globalId,
    "syncDate": curDate,
    "needSync": true
  };

  if (requestBody.parentId) {
    saveItemData.parentId = requestBody.parentId;
  }

  if (requestBody.title) {
    saveItemData.title = requestBody.title;
  }

  if (requestBody.type) {
    saveItemData.type = requestBody.type;
  }

  if (typeof (requestBody.isOfflineOnly) !== "undefined") {
    saveItemData.offlineOnly = requestBody.isOfflineOnly;
  }

  if (typeof (requestBody.color) !== 'undefined') {
    saveItemData.color = requestBody.color;
  }

  if (requestBody.date) {
    saveItemData.date = requestBody.date;
  }

  if (requestBody.createdAt) {
    saveItemData.createdAt = requestBody.createdAt;
  }

  if (requestBody.dateAdded) {
    saveItemData.dateAdded = requestBody.dateAdded;
  }

  if (requestBody.dateCreated) {
    saveItemData.dateCreated = requestBody.dateCreated;
  }

  if (requestBody.dateUpdated) {
    saveItemData.dateUpdated = requestBody.dateUpdated;
  }

  if (requestBody.dateUpdatedUser) {
    saveItemData.dateUpdatedUser = requestBody.dateUpdatedUser;
  }

  if (typeof (requestBody.role) !== "undefined") {
    saveItemData.role = requestBody.role;
  }

  if (typeof (requestBody.url) !== "undefined") {
    saveItemData.url = requestBody.url;
  }

  if (typeof (requestBody.locationLat) !== "undefined") {
    saveItemData.locationLat = requestBody.locationLat;
  }

  if (typeof (requestBody.locationLng) !== "undefined") {
    saveItemData.locationLng = requestBody.locationLng;
  }

  if (typeof (requestBody.shared) !== "undefined") {
    saveItemData.shared = requestBody.shared;
  }

  if (typeof (requestBody.favorite) !== "undefined") {
    saveItemData.favorite = requestBody.favorite;
  }

  if (typeof (requestBody.size) !== "undefined") {
    saveItemData.size = requestBody.size;
  }

  if (typeof (requestBody.editnote) !== "undefined") {
    saveItemData.editnote = (requestBody.editnote ? 1 : 0);
    saveItemData.editnoteUpdateTime = curDate;
  }

  if (typeof (requestBody.isFullwidth) !== "undefined") {
    saveItemData.isFullwidth = (requestBody.isFullwidth ? 1 : 0);
    saveItemData.isFullwidthUpdateTime = curDate;
  }

  if (typeof (requestBody.isEncrypted) !== "undefined") {
    saveItemData.isEncrypted = requestBody.isEncrypted;
  }

  if (requestBody.lastChangeBy) {
    saveItemData.lastChangeBy = requestBody.lastChangeBy;
  }

  if (typeof (requestBody.folderDepth) !== "undefined") {
    saveItemData.folderDepth = requestBody.folderDepth;
  }

  if (typeof (requestBody.folderList) !== "undefined") {
    saveItemData.folderList = requestBody.folderList;
  }

  if (typeof (requestBody.titleTree) !== "undefined") {
    saveItemData.titleTree = requestBody.titleTree;
  }

  if (typeof (requestBody.parentId) === 'undefined') {
    if (saveItemData.type === 'folder') {
      saveItemData.parentId = 'root';
    }
  }

  return saveItemData;
};

/**
 * @param {{workspaceId:string, globalId:string, type:string}} inputData
 * @return {Promise<{}>}
 */
function getItemsByParentId(inputData) {
  return new Promise((resolve) => {
    const {workspaceId, globalId, type} = inputData;
    let queryParentData = <any>{parentId: globalId};
    if (type) {
      queryParentData.type = type;
    }
    item.findAll(queryParentData, {workspaceId}, (err, itemInstances) => {
      if (err || !itemInstances) {
        return resolve([]);
      }
      return resolve(itemInstances);
    });
  });
}

/**
 * @param {{}} inputData
 * @return {Promise<{error: {}, data: {}}>}
 */
function updateItem(inputData) {
  return new Promise((resolve) => {
    let { queryParams, routeParams, requestBody, saveItemData, workspaceId, defaultTitle, curDate } = inputData;
    let queryData = {globalId: saveItemData.globalId};

    item.find(queryData, {workspaceId}, async (err, itemInstance) => {
      if (!saveItemData.type && itemInstance) {
        saveItemData.type = itemInstance.type;
      }

      if (typeof (saveItemData.title) !== "undefined") {
        if (saveItemData.type === "note") {
          saveItemData.title = item.prepareNoteTitle(saveItemData.title);
        }

        let clearTitle = saveItemData.title.replace(/[\n\r]/g, '');
        if (!clearTitle) {
          saveItemData.title = defaultTitle;
        }
      }

      if (requestBody.parentId) {
        if (saveItemData.type === "note") {
          saveItemData.parentId = (requestBody.parentId === 'root') ? 'default' : requestBody.parentId;
        } else if (saveItemData.type === "folder") {
          saveItemData.parentId = saveItemData.parentId ? saveItemData.parentId : 'root';
        }

        saveItemData.rootId = (requestBody.parentId === 'trash') ? 'trash' : 'root';

        if (requestBody.parentId !== "trash") {
          saveItemData.rootParentId = requestBody.parentId;
        }
      }

      if (itemInstance && Object.keys(itemInstance).length) {
        saveItemData = await updateSaveItemOfflineOnlyProperty({workspaceId, saveItemData, itemInstance});
        item.update(queryData, setUpdateProps(saveItemData, curDate), {workspaceId}, async (err, count) => {
          if (!count) {
            return resolve({
              error: error.itemSaveError(),
              data: {},
            });
          }

          let parentFolder = await getItemByGlobalId({workspaceId, globalId: itemInstance.parentId, type: 'folder'});
          if (saveItemData.type === "note" && parentFolder) {
            socketMessage({workspaceId, itemInstance: parentFolder});
          }

          updateFolderNotes({
            workspaceId, folderGlobalId: itemInstance.globalId, updateData: {
              "rootId": saveItemData.rootId,
              "offlineOnly": saveItemData.offlineOnly
            }
          });

          let updateFoldersList = [];
          // @ts-ignore
          let getUpdateItems = async (itemData) => {
            let childFolders = <[{globalId:string}]>await getItemsByParentId({workspaceId, globalId: itemData.globalId, type: 'folder'});
            if (childFolders && childFolders.length) {
              for (let childFolder of childFolders) {
                updateFoldersList.push(childFolder.globalId);
                await getUpdateItems(childFolder);
              }
            }
          };
          await getUpdateItems(saveItemData);

          for (let folderListItemGlobalId of updateFoldersList) {
            let folderQueryData = {globalId: folderListItemGlobalId};
            let folderUpdateData = {
              "rootId": saveItemData.rootId,
              "offlineOnly": saveItemData.offlineOnly,
              "syncDate": curDate,
              "needSync": true
            };

            item.update(folderQueryData, setUpdateProps(folderUpdateData, curDate), {workspaceId}, (err, count) => {
              if (count) {
                updateFolderNotes({workspaceId, folderGlobalId: folderQueryData.globalId, updateData: folderUpdateData});
              }
            });
          }

          item.find(queryData, {workspaceId}, (err, itemInstance) => {
            if (itemInstance && Object.keys(itemInstance).length) {
              // TODO: calculate count of notes for folder item
              itemInstance.cntNotes = 0;
              if (!itemInstance.offlineOnly) {
                ipcMessage({workspaceId});
              }
              socketMessage({workspaceId, itemInstance});
              return resolve({
                error: null,
                data: item.getResponseJson(itemInstance),
              });
            } else {
              return resolve({
                error: error.itemNotFound(),
                data: {},
              });
            }
          });
        });
      } else {
        saveItemData = await addSaveItemOfflineOnlyProperty({workspaceId, saveItemData});
        saveItemData.editnote = 1;
        saveItemData.text_version = queryParams.textVersion ? parseInt(queryParams.textVersion) : 2;

        if (saveItemData.type === "note") {
          const {
            canCreateNote,
            currentNotesCount,
            notesPerWorkspace,
            notesPerWorkspacePro
          } = <any>await item.validateNotesCountPerWorkspace({workspaceId});

          if (!canCreateNote) {
            let objectGlobalId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;
            return resolve({
              error: null,
              data: {
                httpStatus: 403,
                limitSize: notesPerWorkspace,
                name: "LimitError",
                objectGlobalId: objectGlobalId,
                objectSize: currentNotesCount,
                objectType: "NotesNumber",
                premiumSize: notesPerWorkspacePro,
              }
            });
          }
        }

        item.add(setUpdateProps(saveItemData, curDate), {workspaceId}, async (err, itemInstance) => {
          if (itemInstance && Object.keys(itemInstance).length) {
            // TODO: calculate count of notes for folder item
            itemInstance.cntNotes = 0;
            if (saveItemData.type === "note") {
              saveItemData = text.getDefaultModel(itemInstance.globalId);
              saveItemData.text_version = queryParams.textVersion ? parseInt(queryParams.textVersion) : 2;
              text.add(setUpdateProps(saveItemData, curDate), {workspaceId}, (err, textInstance) => {
                if (textInstance && Object.keys(textInstance).length) {
                  if (!itemInstance.offlineOnly) {
                    ipcMessage({workspaceId});
                  }
                  socketMessage({workspaceId, itemInstance});
                  return resolve({
                    error: null,
                    data: item.getResponseJson(itemInstance),
                  });
                } else {
                  return resolve({
                    error: error.itemSaveError(),
                    data: {},
                  });
                }
              });
            } else if (saveItemData.type === "folder") {
              if (!itemInstance.offlineOnly) {
                ipcMessage({workspaceId});
              }
              return resolve({
                error: null,
                data: item.getResponseJson(itemInstance),
              });
            }
          } else {
            return resolve({
              error: error.itemNotFound(),
              data: {},
            });
          }
        });
      }
    });
  });
}

/**
 * @param {{workspaceId:string, globalId:string, type:string}} inputData
 * @return {Promise<any>}
 */
function getItemByGlobalId(inputData) {
  return new Promise((resolve) => {
    const {workspaceId, globalId, type} = inputData;
    let queryParentData = <{globalId:string, type:string}>{globalId};
    if (type) {
      queryParentData.type = type;
    }
    item.find(queryParentData, {workspaceId}, (err, itemInstance) => {
      if (err || !itemInstance) {
        return resolve(itemInstance);
      }
      return resolve(itemInstance);
    });
  });
}

/**
 * @param {{workspaceId:string, folderGlobalId:string, updateData:{}}} inputData
 */
function updateFolderNotes(inputData) {
  const {workspaceId, folderGlobalId, updateData} = inputData;
  let notesQueryData = {
    parentId: folderGlobalId,
    type: 'note'
  };
  item.findAll(notesQueryData, {workspaceId}, (err, noteItems) => {
    if (updateData && noteItems && noteItems.length) {
      let curDate = date.now();
      for (let u in noteItems) {
        if (noteItems.hasOwnProperty(u)) {
          let globalId = noteItems[u].globalId;
          let rootId = updateData.rootId;
          let offlineOnly = updateData.offlineOnly;
          if (globalId && rootId) {
            let noteQueryData = {
              "globalId": globalId
            };
            let noteSaveData = {
              "syncDate": curDate,
              "needSync": true,
              "rootId": rootId,
              "offlineOnly": offlineOnly
            };
            item.update(noteQueryData, setUpdateProps(noteSaveData, curDate), {workspaceId}, () => {
            });
          }
        }
      }
    }
  });
}

/**
 * @param {{workspaceId:string, saveItemData:{}, itemInstance:item}} inputData
 * @return {Promise<void>}
 */
async function updateSaveItemOfflineOnlyProperty(inputData) {
  // @ts-ignore
  return new Promise(async (resolve) => {
    const {workspaceId, saveItemData, itemInstance} = inputData;
    let newParentFolder = null;
    let newParentFolderId = itemInstance.parentId;
    if (itemInstance.type === "note") {
      if (typeof (saveItemData.parentId) !== "undefined") {
        newParentFolderId = saveItemData.parentId;
      }
    }

    if (newParentFolderId) {
      if (newParentFolderId === "root" || newParentFolderId === "trash") {
        saveItemData.offlineOnly = !!itemInstance.offlineOnly;
      } else {
        newParentFolder = await getItemByGlobalId({workspaceId, globalId: newParentFolderId, type: 'folder'});
        if (newParentFolder) {
          if (itemInstance.type === "note") {
            if (typeof (saveItemData.offlineOnly) === "undefined") {
              saveItemData.offlineOnly = !!newParentFolder.offlineOnly;
            }
          } else if (itemInstance.type === "folder") {
            if (typeof (saveItemData.offlineOnly) === "undefined") {
              saveItemData.offlineOnly = !!itemInstance.offlineOnly;
            } else {
              if (!saveItemData.offlineOnly) {
                saveItemData.offlineOnly = !!newParentFolder.offlineOnly;
              }
            }
          }
        }
      }
    }

    resolve(saveItemData);
  });
}

/**
 * @param {{workspaceId:string, saveItemData:{}}} inputData
 * @return {Promise<void>}
 */
async function addSaveItemOfflineOnlyProperty(inputData) {
  // @ts-ignore
  return new Promise(async (resolve) => {
    const {workspaceId, saveItemData} = inputData;
    if (saveItemData.type === "note") {
      if (saveItemData.parentId) {
        if (saveItemData.parentId === "root" || saveItemData.parentId === "trash") {
          saveItemData.offlineOnly = !!saveItemData.offlineOnly;
        } else {
          let parentFolder = <{offlineOnly:boolean}>await getItemByGlobalId({workspaceId, globalId: saveItemData.parentId, type: 'folder'});
          if (parentFolder) {
            if (typeof (saveItemData.offlineOnly) === "undefined") {
              saveItemData.offlineOnly = !!parentFolder.offlineOnly;
            }
          }
        }
      }
    } else if (saveItemData.type === "folder") {
      saveItemData.offlineOnly = !!saveItemData.offlineOnly;
    }
    resolve(saveItemData);
  });
}

/**
 * @param {{workspaceId:string}} inputData
 */
function ipcMessage(inputData) {
  //ipcConnection.requestAutoSync(inputData);
}

/**
 * @param {{workspaceId:string, itemInstance:item}} inputData
 */
function socketMessage(inputData) {
  const {workspaceId, itemInstance} = inputData;
  socketConnection.sendItemUpdateMessage({
    workspaceId,
    globalId: itemInstance.globalId
  });
  socketConnection.sendItemsCountMessage({
    workspaceId,
    globalId: itemInstance.globalId,
    parentId: itemInstance.parentId,
    type: itemInstance.type
  });
}
