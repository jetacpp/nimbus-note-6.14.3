import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as date} from "../../../../utilities/dateHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {default as urlParser} from "../../../../utilities/urlParser";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
//import {default as ipcConnection} from "./../../../../sync/ipc/ipcFunctions";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiItemUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let {notesIds, parentId} = request.body;

  if (!notesIds) {
    return callback(error.wrongInput(), {});
  }

  if (!notesIds.length) {
    return callback(error.wrongInput(), {});
  }

  if (!parentId) {
    return callback(error.wrongInput(), {});
  }

  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let curDate = date.now();

  let err= null, res = {};
  for(let globalId of notesIds) {
    const moveData = <{err:any, res:any}>await moveItemByGlobalId({
      workspaceId,
      globalId,
      parentId,
      curDate
    });

    if(!err && moveData.err) {
      err = moveData.err;
    }

    res = moveData.res;
  }

  return callback(err, res);
}

/**
 * @param {{workspaceId:string, globalId:string, type:string}} inputData
 * @return {Promise<any>}
 */
const getItemsByParentId = async (inputData) => {
  // @ts-ignore
  return new Promise((resolve) => {
    const {workspaceId, globalId, type} = inputData;
    let queryParentData = <any>{parentId: globalId};
    if (type) {
      queryParentData.type = type;
    }
    item.findAll(queryParentData, {workspaceId}, (err, itemInstances) => {
      if (err || !itemInstances) {
        return resolve([]);
      }
      return resolve(itemInstances);
    });
  });
};

/**
 * @param {{workspaceId:string, globalId:string, parentId:string, curDate:int}} inputData
 */
async function moveItemByGlobalId(inputData) {
  // @ts-ignore
  return new Promise((resolve) => {
    const {workspaceId, globalId, parentId, curDate} = inputData;
    let saveItemData = <any>{
      globalId,
      parentId,
      "syncDate": curDate,
      "needSync": true
    };

    let queryData = {globalId: saveItemData.globalId};

    // @ts-ignore
    item.find(queryData, {workspaceId}, async (err, itemInstance) => {
      if(err || !itemInstance) {
        return resolve({
          err: error.itemFindError(),
          res: {}
        });
      }

      const type = itemInstance.type || 'note';

      if (parentId) {
        if (type === "note") {
          saveItemData.parentId = (parentId === 'root') ? 'default' : parentId;
        } else if (type === "folder") {
          saveItemData.parentId = saveItemData.parentId ? saveItemData.parentId : 'root';
        }

        saveItemData.rootId = (parentId === 'trash') ? 'trash' : 'root';

        if (parentId !== "trash") {
          saveItemData.rootParentId = parentId;
        }
      }

      if (!itemInstance) {
        return resolve({
          err: error.itemFindError(),
          res: {}
        });
      }

      if(!Object.keys(itemInstance).length) {
        return resolve({
          err: error.itemFindError(),
          res: {}
        });
      }

      saveItemData = await updateSaveItemOfflineOnlyProperty({workspaceId, saveItemData, itemInstance});
      item.update(queryData, setUpdateProps(saveItemData, curDate), {workspaceId}, async (err, count) => {
        if (!count) {
          return resolve({
            err: error.itemSaveError(),
            res: {}
          });
        }

        let parentFolder = await getItemByGlobalId({workspaceId, globalId: itemInstance.parentId, type: 'folder'});
        if (type === 'note' && parentFolder) {
          socketMessage({workspaceId, itemInstance: parentFolder});
        }

        updateFolderNotes({
          workspaceId, folderGlobalId: itemInstance.globalId, updateData: {
            "rootId": saveItemData.rootId,
            "offlineOnly": saveItemData.offlineOnly
          }
        });

        let updateFoldersList = [];
        // @ts-ignore
        let getUpdateItems = async (itemData) => {
          let childFolders = <[{globalId:string}]>await getItemsByParentId({workspaceId, globalId: itemData.globalId, type: 'folder'});
          if (childFolders && childFolders.length) {
            for (let childFolder of childFolders) {
              updateFoldersList.push(childFolder.globalId);
              await getUpdateItems(childFolder);
            }
          }
        };
        await getUpdateItems(saveItemData);

        for (let folderListItemGlobalId of updateFoldersList) {
          let folderQueryData = {globalId: folderListItemGlobalId};
          let folderUpdateData = {
            "rootId": saveItemData.rootId,
            "offlineOnly": saveItemData.offlineOnly,
            "syncDate": curDate,
            "needSync": true
          };
          item.update(folderQueryData, setUpdateProps(folderUpdateData, curDate), {workspaceId}, (err, count) => {
            if (count) {
              updateFolderNotes({workspaceId, folderGlobalId: folderQueryData.globalId, updateData: folderUpdateData});
            }
          });
        }

        item.find(queryData, {workspaceId}, (err, itemInstance) => {
          if (itemInstance && Object.keys(itemInstance).length) {
            // TODO: calculate count of notes for folder item
            itemInstance.cntNotes = 0;
            if (!itemInstance.offlineOnly) {
              ipcMessage({workspaceId});
            }
            socketMessage({workspaceId, itemInstance});
            return resolve({
              err: null,
              res: {}
            });
          } else {
            return resolve({
              err: error.itemNotFound(),
              res: {}
            });
          }
        });
      });

    });
  });
}

/**
 * @param {{workspaceId:string, globalId:string, type:string}} inputData
 * @return {Promise<any>}
 */
async function getItemByGlobalId(inputData) {
  // @ts-ignore
  return new Promise((resolve) => {
    const {workspaceId, globalId, type} = inputData;
    let queryParentData = <any>{globalId: globalId};
    if (type) {
      queryParentData.type = type;
    }
    item.find(queryParentData, {workspaceId}, (err, itemInstance) => {
      if (err || !itemInstance) {
        return resolve(itemInstance);
      }
      return resolve(itemInstance);
    });
  });
}

/**
 * @param {{workspaceId:string, folderGlobalId:string, updateData:{}}} inputData
 */
function updateFolderNotes(inputData) {
  const {workspaceId, folderGlobalId, updateData} = inputData;
  let notesQueryData = {
    parentId: folderGlobalId,
    type: 'note'
  };
  item.findAll(notesQueryData, {workspaceId}, (err, noteItems) => {
    if (updateData && noteItems && noteItems.length) {
      let curDate = date.now();
      for (let u in noteItems) {
        if (noteItems.hasOwnProperty(u)) {
          let globalId = noteItems[u].globalId;
          let rootId = updateData.rootId;
          let offlineOnly = updateData.offlineOnly;
          if (globalId && rootId) {
            let noteQueryData = {
              "globalId": globalId
            };
            let noteSaveData = {
              "syncDate": curDate,
              "needSync": true,
              "rootId": rootId,
              "offlineOnly": offlineOnly
            };
            item.update(noteQueryData, setUpdateProps(noteSaveData, curDate), {workspaceId}, () => {
            });
          }
        }
      }
    }
  });
}

/**
 * @param {{workspaceId:string, saveItemData:{}, itemInstance:item}} inputData
 * @return {Promise<void>}
 */
async function updateSaveItemOfflineOnlyProperty(inputData) {
  // @ts-ignore
  return new Promise(async (resolve) => {
    const {workspaceId, saveItemData, itemInstance} = inputData;
    let newParentFolder = null;
    let newParentFolderId = itemInstance.parentId;
    if (itemInstance.type === "note") {
      if (typeof (saveItemData.parentId) !== "undefined") {
        newParentFolderId = saveItemData.parentId;
      }
    }

    if (newParentFolderId) {
      if (newParentFolderId === "root" || newParentFolderId === "trash") {
        saveItemData.offlineOnly = !!itemInstance.offlineOnly;
      } else {
        newParentFolder = await getItemByGlobalId({workspaceId, globalId: newParentFolderId, type: 'folder'});
        if (newParentFolder) {
          if (itemInstance.type === "note") {
            if (typeof (saveItemData.offlineOnly) === "undefined") {
              saveItemData.offlineOnly = !!newParentFolder.offlineOnly;
            }
          } else if (itemInstance.type === "folder") {
            if (typeof (saveItemData.offlineOnly) === "undefined") {
              saveItemData.offlineOnly = !!itemInstance.offlineOnly;
            } else {
              if (!saveItemData.offlineOnly) {
                saveItemData.offlineOnly = !!newParentFolder.offlineOnly;
              }
            }
          }
        }
      }
    }

    resolve(saveItemData);
  });
}

/**
 * @param {{workspaceId:string}} inputData
 */
function ipcMessage(inputData) {
  //ipcConnection.requestAutoSync(inputData);
}

/**
 * @param {{workspaceId:string, itemInstance:item}} inputData
 */
function socketMessage(inputData) {
  const {workspaceId, itemInstance} = inputData;
  socketConnection.sendItemUpdateMessage({
    workspaceId,
    globalId: itemInstance.globalId
  });
  socketConnection.sendItemsCountMessage({
    workspaceId,
    globalId: itemInstance.globalId,
    parentId: itemInstance.parentId,
    type: itemInstance.type
  });
}
