import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as item} from "../../../../db/models/item";
import {default as attach} from "../../../../db/models/attach";
import {default as todo} from "../../../../db/models/todo";
import {default as tag} from "../../../../db/models/tag";
import {default as noteTags} from "../../../../db/models/noteTags";
import {default as text} from "../../../../db/models/text";
import {default as workspace} from "../../../../db/models/workspace";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";
import {default as appOnlineState} from "../../../../online/state";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiItemList(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let queryParams = urlParser.getQueryParams(request.url) || {};
  let filterParams = urlParser.getFilterParams(request.url) || <any>{};
  let limitParams = urlParser.getLimitParams(request.url) || {};
  let orderParams = urlParser.getOrderParams(request.url) || {};
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

  let findQuery = <any>{};

  if (filterParams.globalId) {
    if (filterParams.globalId['$like']) {
      findQuery.globalId = {'$regex': filterParams.globalId['$like'].replace("%", ".*")};
    } else {
      findQuery.globalId = filterParams.globalId;
    }
  }

  if (filterParams.type) {
    findQuery.type = filterParams.type;
  }

  if (queryParams.rootId) {
    findQuery.rootId = queryParams.rootId;
  }

  if (filterParams.parentId) {
    findQuery.parentId = filterParams.parentId;
  }

  if (filterParams.favorite) {
    findQuery.favorite = true;
  }

  if (filterParams.title && filterParams.title.$like) {
    let searchTitle = filterParams.title.$like.replace(new RegExp('\%', 'g'), '');
    let filterTitleRegexp = new RegExp(urlParser.escapeRegExp(searchTitle), 'im');
    findQuery.title = {'$regex': filterTitleRegexp};
    findQuery.onlyTitleSearch = true;
  }

  let searchParams = {
    excerpts: {},
    matches: {},
    searchByStringIdList: null,
    searchByTagIdList: null,
  };
  if (queryParams.query && queryParams.query.trim()) {
    const query = queryParams.query.trim();
    let regexp = new RegExp(urlParser.escapeRegExp(query), 'im');
    findQuery.title = {'$regex': regexp};

    if(appOnlineState.get()) {
      const {buildExcerpts, getMatches} = queryParams;
      const searchData = <any>await searchNotes({workspaceId, query, buildExcerpts, getMatches});
      if(Object.keys(searchData).length && searchData.global_ids.length) {
        searchParams.searchByStringIdList = searchData.global_ids;
        searchParams.excerpts = searchData.excerpts;
        searchParams.matches = searchData.matches;
      } else {
        searchParams.searchByStringIdList = [];
        searchParams.excerpts = {};
        searchParams.matches = {};
      }
    }
  }

  let filterItemsByTags = <any>"";
  if (filterParams.tag) {
    if (typeof (filterParams.tag["$in"]) !== "undefined" && filterParams.tag["$in"].length) {
      filterItemsByTags = filterParams.tag;
    } else {
      filterItemsByTags = {"$in": [filterParams.tag]};
    }
  }

  if (filterItemsByTags) {
    let tagsFindQuery = {tag: filterItemsByTags};
    tag.findAll(tagsFindQuery, {workspaceId}, (err, tagList) => {
      let findTagIdList = [];
      if (tagList && tagList.length) {
        for (let d in tagList) {
          if (tagList.hasOwnProperty(d)) {
            findTagIdList.push(tagList[d].globalId);
          }
        }

        let noteTagsFindQuery = {tagGlobalId: {"$in": findTagIdList}};
        noteTags.findAll(noteTagsFindQuery, {workspaceId}, (err, noteTagsItems) => {
          let filterNotesIdObject = {};
          if (noteTagsItems && noteTagsItems.length) {
            for (let t in noteTagsItems) {
              if (noteTagsItems.hasOwnProperty(t)) {
                filterNotesIdObject[noteTagsItems[t].noteGlobalId] = noteTagsItems[t].noteGlobalId;
              }
            }
          }

          let filterNotesIdList = [];
          for (let a in filterNotesIdObject) {
            if (filterNotesIdObject.hasOwnProperty(a)) {
              filterNotesIdList.push(filterNotesIdObject[a]);
            }
          }

          searchParams.searchByTagIdList = filterNotesIdList;
          if(!findQuery.title) {
            findQuery.globalId = {"$in": filterNotesIdList};
          }

          prepareFindItems({workspaceId, queryParams, findQuery, limitParams, orderParams, searchParams}, callback);
        });
      } else {
        callback(error.itemNotFound(), []);
      }
    });
  } else {
    prepareFindItems({workspaceId, queryParams, findQuery, limitParams, orderParams, searchParams}, callback);
  }
}

/**
 * @param {{workspaceId:string, queryParams:{}, findQuery:{}, limitParams:{}, orderParams:{}, searchParams:{}}} inputData
 * @param {Function} callback
 */
function prepareFindItems(inputData, callback) {
  const {workspaceId, queryParams, findQuery, limitParams, orderParams, searchParams} = inputData;
  let params = {};

  if (Object.keys(limitParams).length) {
    params["range"] = limitParams;
  }

  if (Object.keys(orderParams).length) {
    params["order"] = orderParams;
  }

  params = urlParser.getSortAndLimitParams(params);

  if(searchParams) {
    const { searchByStringIdList, searchByTagIdList } = searchParams;
    if(searchByStringIdList) {
      params["searchParams"] = searchParams;
    } else if(searchByTagIdList) {
      params["searchParams"] = searchParams;
    }
  }

  if (findQuery.rootId === "trash" && findQuery.parentId === "root" && findQuery.parentId) {
    delete findQuery["parentId"];
  }

  if (findQuery.rootId !== "trash" && findQuery.parentId === "root") {
    delete findQuery["parentId"];
    findQuery.rootId = "root";
  }

  let curDate = date.now();

  item.find({globalId: "default"}, {workspaceId}, (err, itemInstance) => {
    if (itemInstance && (Object.keys(itemInstance).length || findQuery.rootId === "trash")) {
      findItems({workspaceId, queryParams, findQuery, params}, callback);
    } else {
      let itemDefault = item.getDefaultModel();
      item.add(setUpdateProps(itemDefault, curDate), {workspaceId}, (err, itemInstance) => {
        if (itemInstance && Object.keys(itemInstance).length) {
          findItems({workspaceId, queryParams, findQuery, params}, callback);
        } else {
          callback(error.itemSaveError(), []);
        }
      });
    }
  });
}

/**
 * @param {{workspaceId:string, queryParams:{}, findQuery:{}, params:{}}} inputData
 * @param {Function} callback
 */
function findItems(inputData, callback) {
  let {workspaceId, queryParams, findQuery, params} = inputData;
  if (findQuery && findQuery.type === "folder") {
    if (!params) params = {};
    params.orderField = "title";
    params.orderDirection = 1;
  }

  if (findQuery.type === "folder" && findQuery.rootId === "root") {
    findQuery.rootId = {'$ne': "trash"};
  }

  params.workspaceId = workspaceId;
  item.findAll(findQuery, params, async (err, itemList) => {
    if (!itemList.length) {
      return callback(null, []);
    }

    if (queryParams.onlyId) {
      let itemsGlobalIdList = itemList.map((itemInstance) => {
        return itemInstance.globalId;
      });
      return callback(null, itemsGlobalIdList);
    }

    let {searchParams} = params;
    if(searchParams && searchParams.excerpts && searchParams.matches) {
      const globalIdList = itemList.map((itemInstance) => (itemInstance.globalId));
      searchParams.texts = await getShortTextList({globalIdList, workspaceId});
    }

    if (queryParams.counters) {
      for (let i in itemList) {
        itemList[i].cntNotes = 0;
        if (itemList[i].type === "note") {
          itemList[i].todosClosedCount = 0;
          itemList[i].todosCount = 0;
          itemList[i].attachmentsCount = 0;
        }
      }


      let currentItemIndex = 0;
      let prepareItemCounters = () => {
        if (typeof (itemList[currentItemIndex]) !== "undefined") {
          if (itemList[currentItemIndex].type === "folder") {
            item.count({
              "parentId": itemList[currentItemIndex].globalId,
              "type": "note"
            }, {workspaceId}, (err, childrenNotesCount) => {
              itemList[currentItemIndex].childrenNotesCount = childrenNotesCount;

              currentItemIndex++;
              prepareItemCounters();
            });
          } else if (itemList[currentItemIndex].type === "note") {
            todo.count({
              "noteGlobalId": itemList[currentItemIndex].globalId,
              "checked": false
            }, {workspaceId}, (err, itemTodoCount) => {
              itemList[currentItemIndex].todosCount = itemTodoCount;

              todo.count({
                "noteGlobalId": itemList[currentItemIndex].globalId,
                "checked": true
              }, {workspaceId}, (err, itemTodoClosedCount) => {
                itemList[currentItemIndex].todosClosedCount = itemTodoClosedCount;

                let attachmentsCounterQuery = <any>{"noteGlobalId": itemList[currentItemIndex].globalId};
                    attachmentsCounterQuery.inList = true;
                attach.count(attachmentsCounterQuery, {workspaceId}, (err, itemAttachCount) => {
                  itemList[currentItemIndex].attachmentsCount = itemAttachCount;

                  currentItemIndex++;
                  prepareItemCounters();
                });
              });
            });
          } else {
            currentItemIndex++;
            prepareItemCounters();
          }
        } else {
          return callback(null, item.getResponseListJson(itemList, searchParams));
        }
      };
      prepareItemCounters();
    } else {
      return callback(null, item.getResponseListJson(itemList, searchParams));
    }
  });
}

function getShortTextList({globalIdList, workspaceId}) {
  // @ts-ignore
  return new Promise((resolve) => {
    text.findAll({noteGlobalId: {'$in': globalIdList}}, {workspaceId}, (err, textList) => {
      if(err || !textList) {
        return resolve({});
      }

      let textData = {};
      for(let textInstance of textList) {
        textData[textInstance.noteGlobalId] = textInstance.textShort;
      }

      return resolve(textData);
    });
  });
}

function searchNotes(body) {
  // @ts-ignore
  return new Promise((resolve) => {
    NimbusSDK.getApi().notesSearch(body, (err, res) => {
      if(err) {
        return resolve({});
      }
      return resolve(res);
    });
  });
}
