import chalk from "chalk";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiClientRecent(request, callback) {
  return callback(null, {});
}
