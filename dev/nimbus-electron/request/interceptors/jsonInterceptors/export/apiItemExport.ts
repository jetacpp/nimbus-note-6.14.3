import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as generator} from "../../../../utilities/generatorHandler";
import {exportHandler} from "../../../../utilities/exportHandler";
import {default as appWindow} from "../../../../window/instance";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiItemExport(request, callback) {
    let requestBody = request.body;
    if(!requestBody) { return callback(error.wrongInput(), {}); }

    const id = generator.randomString(16);
    if(appWindow.get()) {
        exportHandler(appWindow.get(), {...requestBody, id});
    }

    return callback(null, { id });
}
