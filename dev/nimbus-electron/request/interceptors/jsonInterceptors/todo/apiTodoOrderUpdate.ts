import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as todo} from "../../../../db/models/todo";
import {default as workspace} from "../../../../db/models/workspace";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiTodoOrderUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let noteGlobalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

  if (!noteGlobalId) {
    return callback(error.wrongInput(), []);
  }

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  if (!request.body.length) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();

  let findQuery = {globalId: {"$in": request.body}};
  let options = <any>{workspaceId};

  options.findParams = {
    selector: findQuery,
    sort: [{"orderNumber": "asc"}]
  };

  todo.findAll(findQuery, options, async (err, todoList) => {
    if (!todoList.length) {
      return callback(null, []);
    }

    let updateOrderNum = async (todoInstance, orderNum) => {
      // @ts-ignore
      return new Promise((resolve) => {
        let findQuery = {noteGlobalId: todoInstance.noteGlobalId, globalId: todoInstance.globalId};
        todo.update(findQuery, setUpdateProps({orderNumber: orderNum}, curDate), {workspaceId}, (err, response) => {
          if (err || !response) {
            return resolve(false);
          }
          resolve(true);
        });
      });
    };

    for (let todoInstance of todoList) {
      let orderNum = request.body.indexOf(todoInstance.globalId);
      if (orderNum >= 0) {
        await updateOrderNum(todoInstance, ++orderNum);
      }
    }

    callback(null, request.body);
  });
}
