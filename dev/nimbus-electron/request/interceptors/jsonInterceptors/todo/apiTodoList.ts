import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as todo} from "../../../../db/models/todo";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiTodoList(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let filterParams = <any>urlParser.getFilterParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

  let findQuery = {noteGlobalId: filterParams.noteGlobalId};
  let options = <any>{workspaceId};
  options.findParams = {selector: findQuery};
  options.findParams.sort = [{"orderNumber": "asc"}, {"date": "asc"}];
  todo.findAll(findQuery, options, (err, todoList) => {
    callback(null, todo.getResponseListJson(todoList));
  });
}
