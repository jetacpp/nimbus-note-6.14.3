import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as todo} from "../../../../db/models/todo";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiNoteTodoList(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let noteGlobalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

  if (!noteGlobalId) {
    return callback(error.wrongInput(), []);
  }

  let findQuery = {noteGlobalId: noteGlobalId};
  let options = <any>{workspaceId};
  options.findParams = {selector: findQuery};
  options.findParams.sort = [{"orderNumber": "asc"}];
  todo.findAll(findQuery, options, (err, todoList) => {
    callback(null, todo.getResponseListJson(todoList));
  });
}
