import chalk from "chalk";
import {default as todo} from "../../../../db/models/todo";
import {default as item} from "../../../../db/models/item";
import {default as date} from "../../../../utilities/dateHandler";
import {default as error} from "../../../../utilities/customError";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as workspace} from "../../../../db/models/workspace";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiTodoUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  if (!request.body.noteGlobalId || !request.body.label) {
    return callback(error.wrongInput(), {});
  }

  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let curDate = date.now();
  let saveTodoData = <any>{
    "globalId": request.body.globalId,
    "noteGlobalId": request.body.noteGlobalId,
    "label": request.body.label,
    "checked": request.body.checked,
    "date": request.body.date ? request.body.date : curDate,
    "dateUpdated": request.body.dateUpdated ? request.body.dateUpdated : curDate,
    "syncDate": curDate,
    "needSync": true
  };

  let itemQueryData = {globalId: saveTodoData.noteGlobalId};
  let itemSaveData = {
    "dateUpdated": curDate,
    "updatedAt": curDate,
    "dateUpdatedUser": curDate,
    "syncDate": curDate,
    "needSync": true
  };

  let queryData = {globalId: saveTodoData.globalId, noteGlobalId: saveTodoData.noteGlobalId};
  todo.find(queryData, {workspaceId}, (err, todoItem) => {
    if (todoItem && Object.keys(todoItem).length) {
      todo.update(queryData, setUpdateProps(saveTodoData, curDate), {workspaceId}, (err, count) => {
        if (!count) {
          return callback(error.itemSaveError(), {});
        }

        item.update(itemQueryData, setUpdateProps(itemSaveData, curDate), {workspaceId}, () => {
          item.find(itemQueryData, {workspaceId}, (err, itemInstance) => {
            todo.find(queryData, {workspaceId}, (err, todoItem) => {
              if (!todoItem) {
                return callback(error.itemNotFound(), todoItem);
              }

              callback(null, todo.getResponseJson(todoItem));
              socketUserNoteTodoMessage({workspaceId, todoItem});
              socketUserCounterMessage({workspaceId, itemInstance});
            });
          });
        });
      });
    } else {
      let findQuery = {noteGlobalId: saveTodoData.noteGlobalId};
      let options = <any>{workspaceId};

      options.findParams = {
        selector: findQuery,
        sort: [{"orderNumber": "asc"}]
      };
      todo.findAll(findQuery, options, (err, todoList) => {
        if (err) {
          return callback(error.itemNotFound(), todoItem);
        }

        saveTodoData.orderNumber = todoList && todoList.length ? (todoList.length + 1) : 1;

        todo.add(setUpdateProps(saveTodoData, curDate), {workspaceId}, (err, todoItem) => {
          if (todoItem && Object.keys(todoItem).length) {
            item.update(itemQueryData, setUpdateProps(itemSaveData, curDate), {workspaceId}, () => {
              item.find(itemQueryData, {workspaceId}, (err, itemInstance) => {
                if (!itemInstance) {
                  return callback(error.itemNotFound(), todoItem);
                }

                callback(null, todo.getResponseJson(todoItem));
                socketUserNoteTodoMessage({workspaceId, todoItem});
                socketUserCounterMessage({workspaceId, itemInstance});
              });
            });
          } else {
            callback(null, {});
          }
        });
      });
    }
  });
}

/**
 * @param {{workspaceId:string, todoItem:todo}} inputData
 */
function socketUserNoteTodoMessage(inputData) {
  const {workspaceId, todoItem} = inputData;
  if (todoItem) {
    socketConnection.sendTodoListUpdateMessage({
      workspaceId,
      noteGlobalId: todoItem.noteGlobalId,
      globalId: todoItem.globalId
    });
  }
}

/**
 * @param {{workspaceId:string, itemInstance:item}} inputData
 */
function socketUserCounterMessage(inputData) {
  const {workspaceId, itemInstance} = inputData;
  if (itemInstance) {
    socketConnection.sendItemsCountMessage({
      workspaceId,
      globalId: itemInstance.globalId,
      parentId: itemInstance.parentId,
      type: itemInstance.type
    });
  }
}
