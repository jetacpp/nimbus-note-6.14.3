import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as todo} from "../../../../db/models/todo";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as date} from "../../../../utilities/dateHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiTodoRemove(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

  if (!globalId) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();
  let queryData = {globalId: globalId};
  todo.find(queryData, {workspaceId}, (err, todoItem) => {
    if (todoItem && Object.keys(todoItem).length) {
      todo.remove(queryData, {workspaceId}, () => {

        let itemQueryData = {globalId: todoItem.noteGlobalId};
        let itemSaveData = {
          "dateUpdated": curDate,
          "updatedAt": curDate,
          "dateUpdatedUser": curDate,
          "syncDate": curDate,
          "needSync": true
        };
        item.update(itemQueryData, setUpdateProps(itemSaveData, curDate), {workspaceId}, () => {
          item.find(itemQueryData, {workspaceId}, (err, itemInstance) => {
            callback(null, {});
            socketUserNoteTodoMessage({workspaceId, todoItem});
            socketUserCounterMessage({workspaceId, itemInstance});
          });
        });
      });
    } else {
      callback(error.itemNotFound(), {});
    }
  });
}

/**
 * @param {{workspaceId:string, todoItem:todo}} inputData
 */
function socketUserNoteTodoMessage(inputData) {
  const {workspaceId, todoItem} = inputData;
  if (todoItem) {
    socketConnection.sendTodoListRemoveMessage({
      workspaceId,
      noteGlobalId: todoItem.noteGlobalId,
      globalId: todoItem.globalId
    });
  }
}

/**
 * @param {{workspaceId:string, itemInstance:item}} inputData
 */
function socketUserCounterMessage(inputData) {
  const {workspaceId, itemInstance} = inputData;
  if (itemInstance) {
    socketConnection.sendItemsCountMessage({
      workspaceId,
      globalId: itemInstance.globalId,
      parentId: itemInstance.parentId,
      type: itemInstance.type
    });
  }
}
