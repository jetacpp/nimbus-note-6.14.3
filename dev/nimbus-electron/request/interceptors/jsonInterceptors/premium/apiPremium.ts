import chalk from "chalk";
import {default as user} from "../../../../db/models/user";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiPremium(request, callback) {
  user.getPublicPremium(request.authInfo, callback);
}
