import chalk from "chalk";
import {default as workspace} from "../../../../db/models/workspace";
import {default as urlParser} from "../../../../utilities/urlParser";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiWorkspacePremium(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  const data = await workspace.getPublicPremium(workspaceId, routeParams[3]);
  callback(null, data);
}
