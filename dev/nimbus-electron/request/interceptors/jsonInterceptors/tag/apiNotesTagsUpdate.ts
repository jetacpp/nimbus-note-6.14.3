import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as tag} from "../../../../db/models/tag";
import {default as noteTags} from "../../../../db/models/noteTags";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as date} from "../../../../utilities/dateHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiNotesTagsUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

  const {notesIds, tagsToAdd, tagsToRemove} = request.body;

  const hasTags = tagsToRemove.length || tagsToAdd.length;
  if (!hasTags || !notesIds.length) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();

  for (let noteGlobalId of notesIds) {
    for (let tagToAdd of tagsToAdd) {
      await addNoteTag({
        curDate,
        workspaceId,
        noteGlobalId,
        tagText: tagToAdd
      });
    }

    for (let tagToRemove of tagsToRemove) {
      await removeNoteTag({
        curDate,
        workspaceId,
        noteGlobalId,
        tagText: tagToRemove
      });
    }

    await updateItem({
      curDate,
      workspaceId,
      noteGlobalId
    });
  }

  return callback(null, {});
}

/**
 * @param {{curDate:int, workspaceId:string, noteGlobalId:string, tag:string}} inputData
 */
async function addNoteTag(inputData) {
  // @ts-ignore
  return new Promise((resolve) => {
    const {curDate, workspaceId, noteGlobalId, tagText} = inputData;

    let saveTagData = {
      "tag": tagText,
      "dateUpdated": curDate,
      "syncDate": curDate,
      "needSync": true
    };

    let queryData = {tag: saveTagData.tag};
    tag.find(queryData, {workspaceId}, (err, tagItem) => {
      if (tagItem && Object.keys(tagItem).length) {
        queryData = <any>{tagGlobalId: tagItem.id, noteGlobalId: noteGlobalId};
        noteTags.findAll(queryData, {workspaceId}, (err, noteTagsItem) => {
          if (noteTagsItem && Object.keys(noteTagsItem).length) {
            return resolve({
              err: error.wrongInput(),
              res: {}
            });
          } else {
            let saveNoteTagData = {
              'tagGlobalId': tagItem.globalId,
              'noteGlobalId': noteGlobalId,
              'dateUpdated': curDate,
              "syncDate": curDate,
              "needSync": true
            };

            noteTags.add(setUpdateProps(noteTags.prepareModelData(saveNoteTagData), curDate), {workspaceId}, () => {
              socketNoteTagMessage({workspaceId, noteGlobalId, tag: saveTagData.tag, action: 'add'});
              return resolve({
                err: null,
                res: {}
              });
            });
          }
        });
      } else {
        tag.add(setUpdateProps(tag.prepareModelData(saveTagData), curDate), {workspaceId}, (err, tagItem) => {
          if (tagItem && Object.keys(tagItem).length) {
            queryData = <any>{tagGlobalId: tagItem.globalId, noteGlobalId: noteGlobalId};

            noteTags.find(queryData, {workspaceId}, (err, noteTagsItem) => {
              if (noteTagsItem && Object.keys(noteTagsItem).length) {
                return resolve({
                  err: error.wrongInput(),
                  res: {}
                });
              } else {
                let saveNoteTagData = {
                  "tagGlobalId": tagItem.globalId,
                  "noteGlobalId": noteGlobalId,
                  "dateUpdated": curDate,
                  "syncDate": curDate,
                  "needSync": true
                };
                noteTags.add(setUpdateProps(noteTags.prepareModelData(saveNoteTagData), curDate), {workspaceId}, () => {
                  socketUserTagMessage({workspaceId, tagList: [saveTagData.tag], action: 'add'});
                  socketNoteTagMessage({workspaceId, noteGlobalId, tag: saveTagData.tag, action: 'add'});
                  return resolve({
                    err: null,
                    res: {}
                  });
                });
              }
            })
          } else {
            return resolve({
              err: error.itemNotFound(),
              res: {}
            });
          }
        });
      }
    });
  });
}

/**
 * @param {{curDate:int, workspaceId:string, noteGlobalId:string, tagText:string}} inputData
 */
async function removeNoteTag(inputData) {
  // @ts-ignore
  return new Promise((resolve) => {
    const {curDate, workspaceId, noteGlobalId, tagText} = inputData;

    let queryData = {tag: tagText};
    tag.find(queryData, {workspaceId}, (err, tagItem) => {
      if (tagItem && Object.keys(tagItem).length)
        noteTags.remove({tagGlobalId: tagItem.globalId, noteGlobalId: noteGlobalId}, {workspaceId}, (err, count) => {
          if (count) {
            socketNoteTagMessage({workspaceId, noteGlobalId, tag: tagItem.tag, action: 'remove'});
            return resolve({
              err: null,
              res: {}
            });
          } else {
            return resolve({
              err: error.itemRemoveError(),
              res: {}
            });
          }
        });
      else {
        return resolve({
          err: error.itemNotFound(),
          res: {}
        });
      }
    });
  });
}

/**
 * @param {{curDate:int, workspaceId, noteGlobalId:string}} inputData
 */
function updateItem(inputData) {
  // @ts-ignore
  return new Promise(async (resolve) => {
    const {curDate, workspaceId, noteGlobalId} = inputData;

    let itemQueryData = {globalId: noteGlobalId};
    let itemSaveData = {
      "dateUpdated": curDate,
      "updatedAt": curDate,
      "dateUpdatedUser": curDate,
      "syncDate": curDate,
      "needSync": true,
    };

    itemSaveData['tags'] = await noteTags.getAllNoteTagItemsTitle({noteGlobalId, workspaceId});
    const updateProps = setUpdateProps(itemSaveData, curDate);
    item.update(itemQueryData, updateProps, {workspaceId}, () => {
      return resolve({
        err: null,
        res: {}
      });
    });
  });
}

/**
 * @param {{workspaceId:string, noteGlobalId:string, tag:string, action:string}} inputData
 */
function socketNoteTagMessage(inputData) {
  const {workspaceId, noteGlobalId, tag, action} = inputData;
  if (noteGlobalId && tag) {
    socketConnection.sendNoteTagsMessage(inputData);
  }
}

/**
 * @param {{workspaceId:string, tagList:[string], action:string}} inputData
 */
function socketUserTagMessage(inputData) {
  const {workspaceId, tagList, action} = inputData;
  if (tagList && tagList.length) {
    socketConnection.sendTagsMessage(inputData);
  }
}
