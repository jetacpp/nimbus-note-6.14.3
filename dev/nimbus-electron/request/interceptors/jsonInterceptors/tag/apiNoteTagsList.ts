import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as tag} from "../../../../db/models/tag";
import {default as noteTags} from "../../../../db/models/noteTags";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiNoteTagsList(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let globalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

  if (!globalId) {
    return callback(error.wrongInput(), []);
  }

  let queryData = {noteGlobalId: globalId};
  noteTags.findAll(queryData, {workspaceId}, (err, noteTagsItems) => {
    let noteTagsItemsIdList = [];
    for (let h in noteTagsItems) {
      if (noteTagsItems.hasOwnProperty(h)) {
        noteTagsItemsIdList.push(noteTagsItems[h].tagGlobalId);
      }
    }

    if (noteTagsItemsIdList.length) {
      tag.findAll({globalId: {"$in": noteTagsItemsIdList}}, {workspaceId}, (err, tagList) => {
        if (tagList && tagList.length) {
          let tagTextObject = {};
          for (let c in tagList) {
            if (tagList.hasOwnProperty(c)) {
              if (tagList[c].tag) {
                tagTextObject[tagList[c].tag] = tagList[c].tag;
              }
            }
          }

          let tagTextList = [];
          for (let d in tagTextObject) {
            if (tagTextObject.hasOwnProperty(d)) {
              tagTextList.push(tagTextObject[d]);
            }
          }

          callback(null, tagTextList);
        } else {
          callback(null, []);
        }
      });
    } else {
      callback(null, []);
    }
  });
}
