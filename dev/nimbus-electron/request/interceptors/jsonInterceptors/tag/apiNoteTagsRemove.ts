import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as tag} from "../../../../db/models/tag";
import {default as noteTags} from "../../../../db/models/noteTags";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as date} from "../../../../utilities/dateHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiNoteTagsRemove(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let noteGlobalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;
  let tagText = typeof (routeParams[7] !== 'undefined') ? decodeURI(routeParams[7]) : null;

  if (!noteGlobalId || !tagText) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();

  let itemQueryData = {globalId: noteGlobalId};
  let itemSaveData = {
    "dateUpdated": curDate,
    "updatedAt": curDate,
    "dateUpdatedUser": curDate,
    "syncDate": curDate,
    "needSync": true,
  };

  let queryData = {tag: tagText};
  tag.find(queryData, {workspaceId}, (err, tagItem) => {
    if (tagItem && Object.keys(tagItem).length)
      noteTags.remove({tagGlobalId: tagItem.globalId, noteGlobalId: noteGlobalId}, {workspaceId}, async (err, count) => {
        if (count) {
          itemSaveData['tags'] = await noteTags.getAllNoteTagItemsTitle({noteGlobalId, workspaceId});
          const updateProps = setUpdateProps(itemSaveData, curDate);

          item.update(itemQueryData, updateProps, {workspaceId}, () => {
            callback(null, {});
            socketNoteTagMessage({workspaceId, noteGlobalId, tag: tagItem.tag, action: 'remove'});
          });
        } else {
          callback(error.itemRemoveError(), {});
        }
      });
    else {
      callback(error.itemNotFound(), {});
    }
  });
}

/**
 * @param {{workspaceId:string, noteGlobalId:string, tag:string, action:string}} inputData
 */
function socketNoteTagMessage(inputData) {
  const {workspaceId, noteGlobalId, tag, action} = inputData;
  if (noteGlobalId && tag) {
    socketConnection.sendNoteTagsMessage(inputData);
  }
}
