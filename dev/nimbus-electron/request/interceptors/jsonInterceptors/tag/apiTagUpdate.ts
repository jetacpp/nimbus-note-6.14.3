import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as item} from "../../../../db/models/item";
import {default as tag} from "../../../../db/models/tag";
import {default as noteTags} from "../../../../db/models/noteTags";
import {default as workspace} from "../../../../db/models/workspace";
import {default as date} from "../../../../utilities/dateHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiTagUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let queryParams = urlParser.getQueryParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let tagText = typeof (queryParams.tag !== 'undefined') ? decodeURI(queryParams.tag) : null;

  if (!tagText) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();
  let saveTagData = <any>{
    "tag": tagText,
    "dateUpdated": curDate,
    //"syncDate": curDate,
    "title": tagText,
    "needSync": true
  };

  if (request.body && request.body.tag) {
    let updatedTagText = request.body.tag.trim();
    if (updatedTagText) {
      saveTagData.tag = updatedTagText;
      saveTagData.title = updatedTagText;
    }
  }

  let saveItemData = {
    "syncDate": curDate,
    "needSync": true
  };

  let queryData = {tag: tagText};
  tag.find(queryData, {workspaceId}, (err, tagItem) => {
    if (tagItem && Object.keys(tagItem).length) {
      saveTagData.oldTitle = tagItem.tag;
      tag.update(queryData, setUpdateProps(saveTagData, curDate), {workspaceId}, (err, response) => {
        socketUserTagMessage({workspaceId, tagList: [tagItem.tag], action: 'remove'});
        socketUserTagMessage({workspaceId, tagList: [saveTagData.tag], action: 'add'});

        let noteTagsQuery = {tagGlobalId: tagItem.globalId};
        noteTags.findAll(noteTagsQuery, {workspaceId}, (err, noteTagsList) => {
          if (noteTagsList && noteTagsList.length) {
            for (let n in noteTagsList) {
              if (noteTagsList.hasOwnProperty(n)) {
                let noteGlobalId = noteTagsList[n].noteGlobalId;
                item.update({globalId: noteGlobalId}, setUpdateProps(saveItemData, curDate), {workspaceId}, (err, response) => {
                });
                socketNoteTagMessage({workspaceId, noteGlobalId, tag: saveTagData.tag, action: 'update'});
              }
            }
          }
        });

        callback(null, {});
      });
    } else {
      tag.add(setUpdateProps(saveTagData, curDate), {workspaceId}, () => {
        socketUserTagMessage({workspaceId, tagList: [saveTagData.tag], action: 'remove'});
        socketUserTagMessage({workspaceId, tagList: [saveTagData.tag], action: 'add'});

        callback(null, {});
      });
    }
  });
}

/**
 * @param {{workspaceId:string, noteGlobalId:string, tag:string, action:string}} inputData
 */
function socketNoteTagMessage(inputData) {
  const {workspaceId, noteGlobalId, tag, action} = inputData;
  if (noteGlobalId && tag) {
    socketConnection.sendNoteTagsMessage(inputData);
  }
}

/**
 * @param {{workspaceId:string, tagList:[string], action:string}} inputData
 */
function socketUserTagMessage(inputData) {
  const {workspaceId, tagList, action} = inputData;
  if (tagList && tagList.length) {
    socketConnection.sendTagsMessage(inputData);
  }
}
