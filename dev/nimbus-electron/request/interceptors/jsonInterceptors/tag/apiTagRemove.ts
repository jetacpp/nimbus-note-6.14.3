import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as tag} from "../../../../db/models/tag";
import {default as noteTags} from "../../../../db/models/noteTags";
import {default as workspace} from "../../../../db/models/workspace";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiTagRemove(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let queryParams = urlParser.getQueryParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let tagText = typeof (queryParams.tag !== 'undefined') ? decodeURI(decodeURI(queryParams.tag)) : null;

  if (!tagText) {
    return callback(error.wrongInput(), {});
  }

  let queryData = {tag: tagText};
  tag.find(queryData, {workspaceId}, (err, tagItem) => {
    if (tagItem && Object.keys(tagItem).length) {
      tag.remove({tag: tagItem.tag}, {workspaceId}, () => {
      });

      noteTags.findAll({tagGlobalId: tagItem.globalId}, {workspaceId}, (err, noteTagsItems) => {
        callback(null, {});

        if (noteTagsItems && noteTagsItems.length) {
          for (let g in noteTagsItems) {
            noteTags.remove({
              tagGlobalId: noteTagsItems[g].tagGlobalId,
              noteGlobalId: noteTagsItems[g].noteGlobalId
            }, {}, function () {
            });
            socketNoteTagMessage({
              workspaceId,
              noteGlobalId: noteTagsItems[g].noteGlobalId,
              tag: tagItem.tag,
              action: 'remove'
            });
          }
        }
        socketUserTagMessage({workspaceId, tagList: [tagItem.tag], action: 'remove'});
      });
    } else {
      callback(error.itemNotFound(), {});
    }
  });
}

/**
 * @param {{workspaceId:string, noteGlobalId:string, tag:string, action:string}} inputData
 */
function socketNoteTagMessage(inputData) {
  const {workspaceId, noteGlobalId, tag, action} = inputData;
  if (noteGlobalId && tag) {
    socketConnection.sendNoteTagsMessage(inputData);
  }
}

/**
 * @param {{workspaceId:string, tagList:[string], action:string}} inputData
 */
function socketUserTagMessage(inputData) {
  const {workspaceId, tagList, action} = inputData;
  if (tagList && tagList.length) {
    socketConnection.sendTagsMessage(inputData);
  }
}
