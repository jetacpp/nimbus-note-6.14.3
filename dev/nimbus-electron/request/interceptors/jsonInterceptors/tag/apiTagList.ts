import chalk from "chalk";
import {default as tag} from "../../../../db/models/tag";
import {default as workspace} from "../../../../db/models/workspace";
import {default as urlParser} from "../../../../utilities/urlParser";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiTagList(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  tag.findAll({}, {workspaceId}, (err, tagList) => {
    let tagTextObject = {};
    for (let c in tagList) {
      if (tagList.hasOwnProperty(c)) {
        if (tagList[c].tag) {
          tagTextObject[tagList[c].tag] = tagList[c].tag;
        }
      }
    }

    let tagTextList = [];
    for (let d in tagTextObject) {
      if (tagTextObject.hasOwnProperty(d)) {
        tagTextList.push(tagTextObject[d]);
      }
    }

    callback(null, tagTextList);
  });
}
