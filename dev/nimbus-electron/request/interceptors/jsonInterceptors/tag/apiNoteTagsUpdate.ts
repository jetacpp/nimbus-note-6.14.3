import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as tag} from "../../../../db/models/tag";
import {default as noteTags} from "../../../../db/models/noteTags";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as date} from "../../../../utilities/dateHandler";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiNoteTagsUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let noteGlobalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;
  let tagText = typeof (routeParams[7] !== 'undefined') ? decodeURI(routeParams[7]) : null;

  if (!noteGlobalId || !tagText) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();

  let saveTagData = {
    "tag": tagText,
    "dateUpdated": curDate,
    "syncDate": curDate,
    "needSync": true
  };

  let itemQueryData = {globalId: noteGlobalId};
  let itemSaveData = {
    "dateUpdated": curDate,
    "updatedAt": curDate,
    "dateUpdatedUser": curDate,
    "syncDate": curDate,
    "needSync": true
  };

  let queryData = {tag: saveTagData.tag};
  tag.find(queryData, {workspaceId}, (err, tagItem) => {
    if (tagItem && Object.keys(tagItem).length) {
      queryData = <any>{tagGlobalId: tagItem.id, noteGlobalId: noteGlobalId};
      noteTags.findAll(queryData, {workspaceId}, (err, noteTagsItem) => {
        if (noteTagsItem && Object.keys(noteTagsItem).length) {

          return callback(error.itemNotFound(), {});

        } else {
          let saveNoteTagData = {
            'tagGlobalId': tagItem.globalId,
            'noteGlobalId': noteGlobalId,
            'dateUpdated': curDate,
            "syncDate": curDate,
            "needSync": true
          };
          noteTags.add(setUpdateProps(noteTags.prepareModelData(saveNoteTagData), curDate), {workspaceId}, async () => {

            itemSaveData['tags'] = await noteTags.getAllNoteTagItemsTitle({noteGlobalId, workspaceId});
            const updateProps = setUpdateProps(itemSaveData, curDate);

            item.update(itemQueryData, updateProps, {workspaceId}, () => {
              callback(null, {});
              socketNoteTagMessage({workspaceId, noteGlobalId, tag: saveTagData.tag, action: 'add'});
            });
          });
        }
      });
    } else {
      tag.add(setUpdateProps(tag.prepareModelData(saveTagData), curDate), {workspaceId}, (err, tagItem) => {
        if (tagItem && Object.keys(tagItem).length) {
          queryData = <any>{tagGlobalId: tagItem.globalId, noteGlobalId: noteGlobalId};

          noteTags.find(queryData, {workspaceId}, (err, noteTagsItem) => {
            if (noteTagsItem && Object.keys(noteTagsItem).length) {

              callback(error.itemNotFound(), {});

            } else {
              let saveNoteTagData = {
                "tagGlobalId": tagItem.globalId,
                "noteGlobalId": noteGlobalId,
                "dateUpdated": curDate,
                "syncDate": curDate,
                "needSync": true
              };
              noteTags.add(setUpdateProps(noteTags.prepareModelData(saveNoteTagData), curDate), {workspaceId}, async () => {

                itemSaveData['tags'] = await noteTags.getAllNoteTagItemsTitle({noteGlobalId, workspaceId});
                const updateProps = setUpdateProps(itemSaveData, curDate);

                item.update(itemQueryData, updateProps, {workspaceId}, () => {
                  callback(null, {});
                  socketUserTagMessage({workspaceId, tagList: [saveTagData.tag], action: 'add'});
                  socketNoteTagMessage({workspaceId, noteGlobalId, tag: saveTagData.tag, action: 'add'});
                });
              });
            }
          })
        } else {
          callback(error.itemNotFound(), {});
        }
      });
    }
  });
}

/**
 * @param {{workspaceId:string, noteGlobalId:string, tag:string, action:string}} inputData
 */
function socketNoteTagMessage(inputData) {
  const {workspaceId, noteGlobalId, tag, action} = inputData;
  if (noteGlobalId && tag) {
    socketConnection.sendNoteTagsMessage(inputData);
  }
}

/**
 * @param {{workspaceId:string, tagList:[string], action:string}} inputData
 */
function socketUserTagMessage(inputData) {
  const {workspaceId, tagList, action} = inputData;
  if (tagList && tagList.length) {
    socketConnection.sendTagsMessage(inputData);
  }
}
