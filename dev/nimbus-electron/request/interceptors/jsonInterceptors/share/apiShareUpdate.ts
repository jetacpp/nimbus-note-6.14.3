import chalk from "chalk";
import {default as item} from "../../../../db/models/item";
import {default as date} from "../../../../utilities/dateHandler";
import {default as error} from "../../../../utilities/customError";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as workspace} from "../../../../db/models/workspace";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiShareUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  if (!request.body.globalId) {
    return callback(error.wrongInput(), {});
  }

  if (typeof (request.body.shared) === "undefined") {
    return callback(error.wrongInput(), {});
  }

  if (typeof (request.body.passwordRequired) === "undefined") {
    return callback(error.wrongInput(), {});
  }

  if (typeof (request.body.shareId) === "undefined") {
    return callback(error.wrongInput(), {});
  }

  if (typeof (request.body.securityKey) === "undefined") {
    return callback(error.wrongInput(), {});
  }

  let globalId = request.body.globalId;
  let shared = !!request.body.shared;
  let passwordRequired = !!request.body.passwordRequired;
  let shareId = request.body.shareId;
  let securityKey = request.body.securityKey;
  let shared_url = request.body.shared_url;

  let curDate = date.now();
  let itemQueryData = {globalId: globalId};
  let itemSaveData = {
    "syncDate": curDate,
    "needSync": true,
    "shared": shared,
    "passwordRequired": passwordRequired,
    "shareId": shareId,
    "securityKey": securityKey,
    "shared_url": shared_url,
  };

  item.find(itemQueryData, {workspaceId}, (err, itemInstance) => {
    if (err || !itemInstance) {
      return callback(err, null);
    }

    itemInstance.syncDate = itemSaveData.syncDate;
    itemInstance.needSync = itemSaveData.needSync;
    itemInstance.shared = itemSaveData.shared;
    itemInstance.passwordRequired = itemSaveData.passwordRequired;
    itemInstance.shareId = itemSaveData.shareId;
    itemInstance.securityKey = itemSaveData.securityKey;
    itemInstance.dateUpdated = curDate;
    itemInstance.shared_url = shared_url;

    item.update(itemQueryData, setUpdateProps(itemSaveData, curDate), {workspaceId}, (err, data) => {
      if (err) {
        return callback(err, null);
      }

      callback(null, itemInstance);
    });
  });
}
