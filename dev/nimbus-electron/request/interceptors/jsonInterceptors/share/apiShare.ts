import chalk from "chalk";
import {default as item} from "./../../../../db/models/item";
import {default as error} from "./../../../../utilities/customError";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiShare(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  if (!request.body.globalId) {
    return callback(error.wrongInput(), {});
  }

  let globalId = request.body.globalId;
  let itemQueryData = {globalId: globalId};

  item.find(itemQueryData, {workspaceId, secure: true}, (err, itemInstance) => {
    if (err || !itemInstance) {
      return callback(err, null);
    }

    callback(null, itemInstance);
  });
}
