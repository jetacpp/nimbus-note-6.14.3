import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as user} from "../../../../db/models/user";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as appOnlineState} from "../../../../online/state";
import {default as appWindow} from "../../../../window/instance";
import {default as trans} from "../../../../translations/Translations";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiMeNameUpdate(request, callback) {
  if (!appOnlineState.get()) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:workspace:message:response', {
        message: trans.get('toast__user_name__update__offline'),
        //type: 'danger'
      });
    }
    return callback(null, {httpStatus: 404});
  }

  let saveUserData = <any>{};
  if(typeof (request.body.firstname) !== 'undefined') {
    saveUserData.firstname = request.body.firstname.trim();
  }

  if(typeof (request.body.lastname) !== 'undefined') {
    saveUserData.lastname = request.body.lastname.trim();
  }

  if(typeof (request.body.username) !== 'undefined') {
    saveUserData.username = request.body.username.trim();
  }

  if(!Object.keys(saveUserData).length) {
    return callback(null, {});
  }

  let curDate = date.now();

  let queryData = {email: request.authInfo.email};
  NimbusSDK.getApi().updateUserInfo({body: saveUserData, skipSyncHandlers: true}, (err, res) => {
    if (err || !res) {
      return callback(error.itemUpdateError(), {httpStatus: 500});
    }

    user.update(queryData, setUpdateProps(saveUserData, curDate), {}, (err, count) => {
      if (err || !count) {
        return callback(error.itemSaveError(), {});
      }

      callback(null, {});
    });
  });
}
