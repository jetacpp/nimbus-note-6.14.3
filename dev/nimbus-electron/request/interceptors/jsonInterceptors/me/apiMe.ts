import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as user} from "../../../../db/models/user";
import {default as orgs} from "../../../../db/models/orgs";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as PouchDb} from "../../../../../pdb";
import fs from "fs-extra";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiMe(request, callback) {
  if (!request.authInfo) {
    return callback(error.wrongInput(), {});
  }

  const { email } = request.authInfo
  const profile = await user.getProfileAsync(email);

  if(!profile) {
    return callback(error.itemNotFound(), {});
  }

  callback(null, profile);
}
