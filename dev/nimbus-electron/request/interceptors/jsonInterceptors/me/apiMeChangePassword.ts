import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as auth} from "../../../../auth/auth";
import {default as user} from "../../../../db/models/user";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiMeChangePassword(request, callback) {
  let defaultErrorResponse = {
    'name': 'WrongPassword',
    'field': 'password'
  };

  if (!request.body.oldPassword || !request.body.newPassword) {
    return callback(error.wrongInput(), defaultErrorResponse);
  }

  if (!request.authInfo) {
    return callback(error.wrongInput(), defaultErrorResponse);
  }

  let curDate = date.now();

  let findQuery = {login: request.authInfo.email};
  user.find(findQuery, {}, (err, userItem) => {
    if (userItem && Object.keys(userItem).length) {
      let oldPasswordHash = auth.cryptPassword(request.body.oldPassword, userItem._id);
      let newPasswordHash = auth.cryptPassword(request.body.newPassword, userItem._id);
      if (oldPasswordHash === userItem.password) {
        user.update({"userId": userItem.userId}, setUpdateProps({
          'password': newPasswordHash
        }, curDate), {}, (err, count) => {
          if (count) {
            callback(null, {});
          } else {
            callback(error.itemSaveError(), defaultErrorResponse);
          }
        });
      } else {
        callback(null, defaultErrorResponse);
      }
    }
  });
}
