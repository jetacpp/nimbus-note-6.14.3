import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as userSettings} from "../../../../db/models/userSettings";
import {default as AutoSyncManager} from "../../../../sync/nimbussdk/manager/AutoSyncManager";
import SelectedWorkspace from "../../../../workspace/SelectedWorkspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiSyncUpdate(request, callback) {
  if (typeof (request.body.syncStatus) === "undefined") {
    return callback(error.wrongInput(), {});
  }

  if (typeof (request.body.syncTimer) === "undefined") {
    return callback(error.wrongInput(), {});
  }

  if (!request.authInfo) {
    return callback(error.wrongInput(), {});
  }

  let syncStatus = parseInt(request.body.syncStatus);
  if (!syncStatus) {
    syncStatus = 0;
  }

  let syncTimer = parseInt(request.body.syncTimer);
  if (!syncTimer) {
    syncTimer = userSettings.SYNC_TIMER_MIN_DEFAULT;
  }

  if (syncTimer < 1 || syncTimer > 30) {
    syncTimer = userSettings.SYNC_TIMER_MIN_DEFAULT;
  }

  let workspaceId = await SelectedWorkspace.getGlobalId();

  let syncInfo = {
    'syncStatus': syncStatus,
    'syncTimer': syncTimer
  };

  userSettings.set(`syncStatus:${workspaceId}`, syncInfo.syncStatus, () => {
    // @ts-ignore
    userSettings.set(`syncTimer:${workspaceId}`, syncInfo.syncTimer, async () => {
      if (syncInfo.syncStatus) {
        AutoSyncManager.updateTimer({workspaceId});
      } else {
        AutoSyncManager.clearTimer({workspaceId});
      }

      callback(null, syncInfo);
    });
  });
}
