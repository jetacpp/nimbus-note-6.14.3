import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as user} from "../../../../db/models/user";
import {default as orgs} from "../../../../db/models/orgs";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiWorkspaceInfoGet(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let dateNextQuotaReset;

  if (!request.authInfo) {
    return callback(error.wrongInput(), {});
  }

  const orgInstance = <any>await orgs.getByWorkspaceId(workspaceId);
  if (orgInstance && orgInstance.usage && orgInstance.usage.traffic) {
    dateNextQuotaReset = user.getDateNextQuotaReset(orgInstance.usage.traffic.daysToReset);
  }

  let findQuery = {login: request.authInfo.email};
  user.find(findQuery, {}, (err, userItem) => {
    if (userItem && Object.keys(userItem).length) {
      let response = <any>{};

      if (!orgInstance) {
        response.orgId = userItem.orgId || "";
      } else {
        response.orgId = orgInstance.id;
      }

      if (userItem.notesEmail) {
        response.notesEmail = user.getNotesEmailPart(userItem.notesEmail);
      }

      if (typeof (dateNextQuotaReset) === 'undefined') {
        if (userItem.dateNextQuotaReset) {
          response.dateNextQuotaReset = userItem.dateNextQuotaReset;
        }
        if (userItem.quotaResetDate) {
          response.quotaResetDate = userItem.quotaResetDate;
        }
      } else {
        response.dateNextQuotaReset = dateNextQuotaReset;
        response.quotaResetDate = dateNextQuotaReset;
      }

      callback(null, response);
    } else {
      callback(error.itemNotFound(), {});
    }
  });
}
