import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as userSettings} from "../../../../db/models/userSettings";
import SelectedWorkspace from "../../../../workspace/SelectedWorkspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiSync(request, callback) {
  if (!request.authInfo) {
    return callback(error.wrongInput(), {});
  }

  let workspaceId = await SelectedWorkspace.getGlobalId();
  callback(null, await userSettings.getSyncInfo({workspaceId}));
}
