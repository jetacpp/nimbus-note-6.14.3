import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as user} from "../../../../db/models/user";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiWorkspaceEmailsGet(request, callback) {
  if (!request.authInfo) {
    return callback(error.wrongInput(), {});
  }

  let findQuery = {login: request.authInfo.email};
  user.find(findQuery, {}, (err, userItem) => {
    if (userItem && Object.keys(userItem).length) {
      let response = <any>{};

      if (userItem.notesEmail) {
        response.notesEmail = user.getNotesEmailPart(userItem.notesEmail);
      }

      if (userItem.userId) {
        response.userId = userItem.userId;
      }

      response.workspaceId = "default";

      callback(null, response);
    } else {
      callback(error.itemNotFound(), {});
    }
  });
}
