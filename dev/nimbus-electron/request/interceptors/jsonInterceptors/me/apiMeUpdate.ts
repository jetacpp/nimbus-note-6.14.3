import chalk from "chalk";
import {default as error} from "../../../../utilities/customError";
import {default as appWindowInstance} from "../../../../window/instance";
import {default as menu} from "../../../../menu/menu";
import {default as trans} from "../../../../translations/Translations";
import {default as user} from "../../../../db/models/user";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiMeUpdate(request, callback) {
  if (!request.body.userId) {
    return callback(error.wrongInput(), {});
  }

  if (!request.authInfo) {
    return callback(error.wrongInput(), {});
  }

  let curDate = date.now();

  let saveUserData = {"language": request.body.language};
  let queryData = {email: request.authInfo.email};

  user.update(queryData, setUpdateProps(saveUserData, curDate), {}, (err, count) => {
    if (err || !count) {
      return callback(error.itemSaveError(), {});
    }

    if (appWindowInstance.get()) {
      trans.setLang(saveUserData.language);
      appWindowInstance.get().webContents.send('event:change:language:response', {
        language: saveUserData.language,
        contextMenu: menu.getContextMenu(),
      });
      menu.update();
    }

    user.find(queryData, {}, (err, userItem) => {
      if (userItem && Object.keys(userItem).length) {
        let apiMe = user.getPublicData(userItem);
        callback(null, apiMe);
      } else {
        callback(error.itemNotFound(), {});
      }
    });
  });
}
