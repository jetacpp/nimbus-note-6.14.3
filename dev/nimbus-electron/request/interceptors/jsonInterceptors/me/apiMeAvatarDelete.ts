import chalk from "chalk";
import {default as user} from "../../../../db/models/user";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as error} from "../../../../utilities/customError";
import {default as PouchDb} from "../../../../../pdb";
import {default as date} from "../../../../utilities/dateHandler";
import AvatarSingleDownloader from "../../../../sync/downlaoder/AvatarSingleDonwloader";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as config} from "../../../../../config.runtime";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiMeAvatarDelete(request, callback) {
  let findQuery = {userId: request.authInfo.userId};
  let curDate = date.now();

  user.find(findQuery, {}, (err, userItem) => {
    if (userItem && Object.keys(userItem).length) {

      const saveUserData = {avatar: null};
      NimbusSDK.getApi().updateUserInfo({body: saveUserData, skipSyncHandlers: true}, (err, res) => {
        if (err || !res) {
          return callback(error.itemUpdateError(), {httpStatus: 500});
        }

        user.update({
          email: userItem.email ? userItem.email.toLowerCase() : ''
        }, setUpdateProps({avatar: null}, curDate), {}, (err, count) => {
          if (err || !count) {
            return callback(error.itemSaveError(), {});
          }

          return callback(null, {
            isFulfilled: false,
            isRejected: false
          });
        });
      });
    } else {
      return callback(null, {
        isFulfilled: false,
        isRejected: true
      });
    }
  });
}
