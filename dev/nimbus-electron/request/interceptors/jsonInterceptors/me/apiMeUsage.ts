import chalk from "chalk";
import {default as user} from "../../../../db/models/user";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiMeUsage(request, callback) {
  let findQuery = {userId: request.authInfo.userId};
  user.find(findQuery, {}, (err, userItem) => {
    if (userItem && Object.keys(userItem).length) {
      let apiMeUsage = user.getPublicUsage(userItem);
      callback(null, apiMeUsage);
    }
  });
}
