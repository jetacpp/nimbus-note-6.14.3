import chalk from "chalk";
import fs = require('fs-extra');
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as PouchDb} from "../../../../../pdb";

/**
 * @param {Request} request
 * @param {Response} response
 */
// @ts-ignore
export default async function apiAvatarPreview(request, response) {
  let routeParams = urlParser.getPathParams(request.url);
  let storedFileUUID = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;
  if(!storedFileUUID) {
    return response.status(404).send();
  }

  if (!PouchDb.getClientAttachmentPath()) {
    return response.status(404).send();
  }

  let targetPath = `${PouchDb.getClientAttachmentPath()}/${storedFileUUID}`;
  // @ts-ignore
  let exists = await fs.exists(targetPath);
  // @ts-ignore
  if (!exists) {
    errorHandler.log("apiAttachPreview => file not exist by path: " + targetPath);
    return response.status(404).send();
  }

  response.sendFile(targetPath);
}
