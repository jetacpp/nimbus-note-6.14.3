import chalk from "chalk";
import fs = require('fs-extra');
import {default as error} from "../../../../utilities/customError";
import {default as user} from "../../../../db/models/user";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as appOnlineState} from "../../../../online/state";
import {default as appWindow} from "../../../../window/instance";
import {default as trans} from "../../../../translations/Translations";
import {default as config} from "../../../../../config.runtime";
import {default as PouchDb} from "../../../../../pdb";
import AvatarSingleDownloader from "../../../../sync/downlaoder/AvatarSingleDonwloader";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiMeAvatarUpdate(request, callback) {
  if (!request.file) {
    return callback(null, {httpStatus: 500});
  }

  if (!appOnlineState.get()) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:workspace:message:response', {
        message: trans.get('toast__user_avatar__update__offline'),
        //type: 'danger'
      });
    }
    return callback(null, {httpStatus: 404});
  }

  let file = request.file;
  let saveData = {
    email: request.authInfo.email,
    userId: request.authInfo.userId
  };
  // @ts-ignore
  let exists = await fs.exists(config.nimbusAttachmentPath);
  // @ts-ignore
  if (!exists) {
    await fs.mkdir(config.nimbusAttachmentPath);
  }
  await prepareAttachLoad({saveData, file}, callback);
}

/**
 * @param {{ saveData, file }} inputData
 * @param {Function} callback
 */
async function prepareAttachLoad(inputData, callback) {
  if (!PouchDb.getClientAttachmentPath()) {
    return callback(error.wrongAttachPath(), {});
  }
  // @ts-ignore
  let exists = await fs.exists(PouchDb.getClientAttachmentPath());
  // @ts-ignore
  if (exists) {
    prepareAttachSave(inputData, callback);
  } else {
    fs.mkdir(PouchDb.getClientAttachmentPath(), () => {
      prepareAttachSave(inputData, callback);
    });
  }
}

/**
 * @param {{saveData:{}}} inputData
 * @param callback Function
 */
function prepareAttachSave(inputData, callback) {
  const {saveData, file} = inputData;
  const {email} = saveData;
  const fileUploadData = <any>{
    workspaceId: null,
    tempname: file.path,
    mime: file.mimetype || '',
    extension: file.originalname ? file.originalname.split('.').pop() : ''
  };

  NimbusSDK.getApi().filesPreupload(fileUploadData, async (err, tempName) => {
    if (err || !tempName) {
      errorHandler.log({
        err: err, response: tempName,
        description: "Upload problem => apiMeAvatarUpdate => filesPreupload",
        data: inputData
      });
      return callback(error.itemSaveError(), {httpStatus: 500});
    }

    const saveUserData = {avatar: {tempFileName: tempName}};
    NimbusSDK.getApi().updateUserInfo({body: saveUserData, skipSyncHandlers: true}, (err, res) => {
      if (err || !res) {
        return callback(error.itemUpdateError(), {httpStatus: 500});
      }

      const userId = res.user_id;
      const {avatar} = res;
      if (!avatar) {
        return callback(error.itemUpdateError(), {httpStatus: 500});
      }

      const {url, createdAt, updatedAt} = avatar;
      if (!url) {
        return callback(error.itemUpdateError(), {httpStatus: 500});
      }

      const storedFileUUID = url.split("/").pop();
      if (!storedFileUUID) {
        return callback(error.itemUpdateError(), {httpStatus: 500});
      }

      const targetPath = `${PouchDb.getClientAttachmentPath()}/${storedFileUUID}`;
      let curDate = date.now();

      user.find({email}, {}, async (err, userInstance) => {
        if (err || !userInstance) {
          return callback(error.itemFindError(), {httpStatus: 500});
        }

        await AvatarSingleDownloader.removeAvatarFile(userInstance.avatar);

        user.update({email}, setUpdateProps({avatar}, curDate), {}, (err, count) => {
          if (err || !count) {
            return callback(error.itemSaveError(), {});
          }

          try {
            fs.rename(file.path, targetPath, (err) => {
              if (err) {
                if (config.SHOW_WEB_CONSOLE) {
                  console.log(err);
                }
              }

              callback(null, {
                createdAt,
                updatedAt,
                storedFileUUID: `avatar/${storedFileUUID}`,
                userId: userId
              });
            });
          } catch (e) {
            return callback(error.itemUpdateError(), {httpStatus: 500});
          }
        });
      });
    });
  });
}
