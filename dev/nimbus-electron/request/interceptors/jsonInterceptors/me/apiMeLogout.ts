import chalk from "chalk";
import {BrowserWindow} from "electron";
import {default as auth} from "../../../../auth/auth";
import ErrorHandler, {default as errorHandler} from "../../../../utilities/errorHandler";
import Login from "../../../auth/login";
import GoogleAuth from "../../../auth/oauth/google";
import FacebookAuth from "../../../auth/oauth/facebook";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiMeLogout(request, callback) {
  const authInfo = request ? request.authInfo : {};
  if(authInfo && authInfo.authProvider) {
    switch (authInfo.authProvider) {
      case Login.AUTH_PROVIDER_GOOGLE: {
        errorHandler.log('Google logout')
        GoogleAuth.logout();
        break;
      }
      case Login.AUTH_PROVIDER_FACEBOOK: {
        errorHandler.log('Facebook logout')
        FacebookAuth.logout();
        break;
      }
      default: ErrorHandler.log(`Can not logout unknown auth provider: ${authInfo.authProvider}`)
    }
  }

  await errorHandler.clearSessionLogsStream();

  setTimeout(() => {
    clientLogOut();
  }, 100);

  setTimeout(() => {
    callback(null, {});
  }, 1000);
}

function logoutRequest() {
  let appWindows = BrowserWindow.getAllWindows();
  for (let i in appWindows) {
    let appWindow = appWindows[i];
    let pageContent = appWindow.webContents;
    pageContent.send('event:logout:request');
  }
}

function clientLogOut() {
  auth.logout();
  logoutRequest();
}
