import chalk from "chalk";
import {default as user} from "../../../../db/models/user";
import {default as error} from "../../../../utilities/customError";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiProTrial(request, callback) {
  if (!request.authInfo) {
    return callback(error.wrongInput(), {});
  }

  const { userId } = request.authInfo;
  const apiTrial = await user.getTrialAsync(userId);

  if(!apiTrial) {
    return callback(error.itemNotFound(), {});
  }

  callback(null, apiTrial);
}
