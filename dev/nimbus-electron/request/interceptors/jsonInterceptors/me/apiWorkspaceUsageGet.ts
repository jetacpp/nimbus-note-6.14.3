import chalk from "chalk";
import {default as user} from "../../../../db/models/user";
import {default as workspace} from "../../../../db/models/workspace";
import {default as orgs} from "../../../../db/models/orgs";
import {default as urlParser} from "../../../../utilities/urlParser";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiWorkspaceUsageGet(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let findQuery = {userId: request.authInfo.userId};

  const orgInstance = <any>await orgs.getByWorkspaceId(workspaceId);
  if (orgInstance && orgInstance.usage && orgInstance.usage.traffic) {
    const {current, max} = orgInstance.usage.traffic;
    return callback(null, {current, max});
  }

  user.find(findQuery, {}, (err, userItem) => {
    if (userItem && Object.keys(userItem).length) {
      callback(null, user.getPublicUsage(userItem));
    }
  });
}
