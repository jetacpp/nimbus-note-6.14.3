import chalk from "chalk";
import {default as settings} from "../../../../db/models/settings";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiAppSettings(request, callback) {
  callback(null, await settings.getAppSettings());
}
