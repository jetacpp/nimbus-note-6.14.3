import chalk from "chalk";
import AutoLaunch from 'auto-launch';
import {default as settings} from "../../../../db/models/settings";
import {default as errorHandler} from "../../../../utilities/errorHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiAppSettingsUpdate(request, callback) {
  let { autoLoad, spellChecker } = request.body
  autoLoad = autoLoad ? 1 : 0
  spellChecker = spellChecker ? 1 : 0

  if (typeof (request.body.autoLoad) !== "undefined") {
      const autoLauncher = new AutoLaunch({name: 'Nimbus Note', isHidden: true});
      autoLauncher.isEnabled()
          .then(function(isEnabled) {
              if(isEnabled && !autoLoad){
                  return autoLauncher.disable();
              }

              if(!isEnabled && autoLoad) {
                  return autoLauncher.enable();
              }
          })
          .catch((error) => {
              errorHandler.log(`AutoLauncher err: ${error == null ? "unknown" : (error.stack || error).toString()}`);
          });
      await settings.setAutoLoad(autoLoad)
  }

  if (typeof (request.body.spellChecker) !== "undefined") {
      await settings.setSpellChecker(spellChecker)
  }

  return callback(null, {
      autoLoad: !!autoLoad,
      spellChecker: !!spellChecker,
  });
}
