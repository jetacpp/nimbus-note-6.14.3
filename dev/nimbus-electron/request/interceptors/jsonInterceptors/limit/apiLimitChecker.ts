import chalk from "chalk";
import {default as attach} from "../../../../db/models/attach";
import {default as workspace} from "../../../../db/models/workspace";
import {default as urlParser} from "../../../../utilities/urlParser";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiLimitChecker(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

  if (!request.body) {
    return callback({});
  }

  if (!request.body.size) {
    return callback({});
  }

  // let displayName = request.body.displayName;
  // let noteGlobalId = request.body.noteGlobalId;
  // let subscribe = request.body.subscribe;
  let objectGlobalId = request.body.objectGlobalId;
  let objectType = request.body.objectType;
  let objectSize = request.body.size;

  if (await attach.checkFileUploadLimit({workspaceId, size: objectSize})) {
    return callback(null, {});
  }

  let response = {
    httpStatus: 403,
    limitSize: await attach.getFileUploadLimit({workspaceId}),
    name: "LimitError",
    objectGlobalId: objectGlobalId,
    objectSize: objectSize,
    objectType: objectType,
    premiumSize: attach.MAX_UPLOAD_FILE_SIZE_PREMIUM
  };

  callback(null, response);
}
