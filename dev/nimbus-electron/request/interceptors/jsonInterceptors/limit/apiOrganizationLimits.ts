import chalk from "chalk";
import {default as orgs} from "../../../../db/models/orgs";
import {default as urlParser} from "../../../../utilities/urlParser";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiOrganizationLimits(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let orgId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;
  const data = await orgs.getLimitsByOrgId(orgId);
  callback(null, data);
}
