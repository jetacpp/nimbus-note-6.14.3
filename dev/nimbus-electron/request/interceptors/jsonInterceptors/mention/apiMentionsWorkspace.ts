import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default function apiMentionsWorkspace(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let globalId = typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null;

  if (!globalId) {
    return callback(error.wrongInput(), {});
  }

  let queryData = {globalId: globalId};
  workspace.find(queryData, {}, async (err, workspaceItem) => {
    if (workspaceItem && Object.keys(workspaceItem).length) {
      const response = await workspace.getMentionResponseJson(workspaceItem);
      callback(null, response);
    } else {
      callback(error.itemNotFound(), {});
    }
  });
}
