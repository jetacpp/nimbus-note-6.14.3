import chalk from "chalk";
import fs = require('fs-extra');
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as date} from "../../../../utilities/dateHandler";
import {default as error} from "../../../../utilities/customError";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as item} from "../../../../db/models/item";
import {default as attach} from "../../../../db/models/attach";
import {default as workspace} from "../../../../db/models/workspace";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {default as PouchDb} from "../../../../../pdb";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiPreviewUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let noteGlobalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

  if (!request.body)
    return callback(error.wrongInput(), {});

  if (!noteGlobalId || !request.body.attachmentGlobalId)
    return callback(error.wrongInput(), {});

  let attachmentGlobalId = request.body.attachmentGlobalId;

  let curDate = date.now();
  let saveItemData = {
    "preview": {
      "global_id": attachmentGlobalId
    },
    "syncDate": curDate,
    "needSync": true
  };

  let attachQueryData = {globalId: attachmentGlobalId};
  attach.find(attachQueryData, {workspaceId}, async (err, attachItem) => {
    if (attachItem && Object.keys(attachItem).length) {
      if (!PouchDb.getClientAttachmentPath()) {
        return callback(null, {});
      }

      let originalPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
      // @ts-ignore
      let fileExist = await fs.exists(originalPath);
      // @ts-ignore
      if (!fileExist) {
        errorHandler.log("returnApiPreviewUpdateJson => file not exist by path: " + originalPath);
        return callback(null, {});
      }

      // let previewPath = `${originalPath}-preview`;
      // try { fs.unlink(previewPath); } catch (error) {
      //   errorHandler.log(`Problem to replace preview`);
      //   errorHandler.log(error.message);
      // }

      let queryData = {globalId: noteGlobalId};
      item.find(queryData, {workspaceId}, (err, itemInstance) => {
        if (itemInstance && Object.keys(itemInstance).length) {
          item.update(queryData, setUpdateProps(saveItemData, curDate), {workspaceId}, (err, count) => {
            if (err || !count) {
              return callback(null, {});
            }

            socketMessage({
              workspaceId,
              noteGlobalId: itemInstance.globalId,
              attachGlobalId: attachmentGlobalId
            });

            itemInstance.preview = {'global_id': attachmentGlobalId};
            callback(null, item.getPreviewResponseJson(itemInstance));
          });
        } else {
          return callback(error.itemNotFound(), {});
        }
      });
    }
  });
}

/**
 * @param {{workspaceId:string, noteGlobalId:string, attachGlobalId:string}} inputData
 */
function socketMessage(inputData) {
  const {workspaceId, noteGlobalId, attachGlobalId} = inputData;
  socketConnection.sendItemPreviewUpdateMessage({
    workspaceId,
    globalId: noteGlobalId,
    attachmentGlobalId: attachGlobalId
  });
}
