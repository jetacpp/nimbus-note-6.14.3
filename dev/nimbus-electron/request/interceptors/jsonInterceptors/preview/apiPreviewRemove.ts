import chalk from "chalk";
import fs = require('fs-extra');
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as date} from "../../../../utilities/dateHandler";
import {default as error} from "../../../../utilities/customError";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as attach} from "../../../../db/models/attach";
import {default as PouchDb} from "../../../../../pdb";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import config from "../../../../../config";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiPreviewRemove(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let noteGlobalId = typeof (routeParams[5] !== 'undefined') ? decodeURI(decodeURI(routeParams[5])) : null;

  if (!noteGlobalId)
    return callback(error.wrongInput(), {});

  let curDate = date.now();
  let itemSaveData = {
    "preview": null,
    "syncDate": curDate,
    "needSync": true
  };

  let queryData = {globalId: noteGlobalId};
  item.find(queryData, {workspaceId}, (err, itemInstance) => {
    if (itemInstance && Object.keys(itemInstance).length) {
      let responseData = item.getPreviewResponseJson(itemInstance);
      item.update(queryData, setUpdateProps(itemSaveData, curDate), {workspaceId}, (err, count) => {
        if (err || !count) {
          return callback(null, {});
        }

        if(responseData) {
          const { attachmentGlobalId } = responseData;
          if(attachmentGlobalId) {
            let attachQueryData = {globalId: attachmentGlobalId};
            attach.find(attachQueryData, {workspaceId}, async (err, attachItem) => {
              if (attachItem && Object.keys(attachItem).length) {
                if (PouchDb.getClientAttachmentPath()) {
                  let originalPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
                  let previewPath = `${originalPath}-preview`;

                  // @ts-ignore
                  let previewFileExist = await fs.exists(previewPath);
                  // @ts-ignore
                  if(previewFileExist) {
                    try {
                      fs.unlink(previewPath, () => {
                      });
                    } catch (e) {
                      if (config.SHOW_WEB_CONSOLE) {
                        errorHandler.log(`Error => apiPreviewRemove => remove preview file: ${previewPath}`);
                      }
                    }
                  }
                }
              }
            });
          }
        }

        socketMessage({workspaceId, noteGlobalId: itemInstance.globalId});

        callback(null, responseData);
      });
    } else {
      callback(error.itemNotFound(), {});
    }
  });
}

/**
 * @param {{workspaceId:string, noteGlobalId:string}} inputData
 */
function socketMessage(inputData) {
  const {workspaceId, noteGlobalId} = inputData;
  socketConnection.sendItemPreviewRemoveMessage({
    workspaceId,
    globalId: noteGlobalId
  });
}
