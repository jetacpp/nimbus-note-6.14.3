import chalk from "chalk";
import fs = require('fs-extra');
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as item} from "../../../../db/models/item";
import {default as attach} from "../../../../db/models/attach";
import {default as workspace} from "../../../../db/models/workspace";
import {default as PouchDb} from "../../../../../pdb";
import errorHandler from "../../../../utilities/errorHandler";
import {makeThumbnail} from "../attach/apiAttachPreview";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiPreviewList(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let filterParams = urlParser.getFilterParams(request.url) || {};
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;

  let queryParam = {
    'globalId': filterParams['noteGlobalId'],
    'preview': {"$ne": null}
  };

  item.findAll(queryParam, {workspaceId}, (err, itemList) => {
    if (err || !itemList.length) {
      return callback(null, []);
    }

    let previewInfoList = {};
    for (let i in itemList) {
      if (itemList.hasOwnProperty(i)) {
        let itemInstance = itemList[i];
        if (itemInstance.preview && typeof (itemInstance.preview['global_id']) !== "undefined") {
          let previewInfo = item.getPreviewResponseJson(itemInstance);
          previewInfoList[previewInfo.attachmentGlobalId] = previewInfo;
        }
      }
    }

    let previewAttachIdList = Object.keys(previewInfoList);
    if (!previewAttachIdList.length) {
      return callback(null, []);
    }

    attach.findAll({"globalId": {"$in": previewAttachIdList}}, {workspaceId}, async (err, attachList) => {
      if (err || !attachList) {
        return callback(null, []);
      }

      if (!PouchDb.getClientAttachmentPath()) {
        return callback(null, []);
      }

      for (let attachItem of attachList) {
        if (typeof (previewInfoList[attachItem.globalId]) === "undefined") {
          delete previewInfoList[attachItem.globalId];
          continue;
        }

        let originalPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;

        let previewPath = `${originalPath}-preview`;
        // @ts-ignore
        let previewFileExist = await fs.exists(previewPath);
        // @ts-ignore
        if(!previewFileExist) {
          // @ts-ignore
          let fileExist = await fs.exists(originalPath);
          // @ts-ignore
          if(!fileExist) {
            delete previewInfoList[attachItem.globalId];
          } else {
            try {
              await makeThumbnail(originalPath, previewPath);
            } catch (error) {
              errorHandler.log(`Problem to generate preview`);
              errorHandler.log(error.message);
              delete previewInfoList[attachItem.globalId];
            }
          }
        }
      }

      callback(null, Object.values(previewInfoList));
    });
  });
}
