import chalk from "chalk";
import {default as text} from "../../../../db/models/text";
import {default as item} from "../../../../db/models/item";
import {default as workspace} from "../../../../db/models/workspace";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as date} from "../../../../utilities/dateHandler";
import {default as error} from "../../../../utilities/customError";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {default as ipcConnection} from "../../../../sync/ipc/ipcFunctions";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiTextUpdate(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);

  if (!request.body) {
    return callback(error.wrongInput(), {});
  }

  if (!request.body.noteGlobalId) {
    return callback(error.wrongInput(), {});
  }

  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let curDate = date.now();
  let saveTextData = {
    "noteGlobalId": request.body.noteGlobalId,
    "text": request.body.text,
  };

  let itemQueryData = {globalId: saveTextData.noteGlobalId};
  let itemSaveData = <any>{
    "dateUpdated": curDate,
    "updatedAt": curDate,
    "dateUpdatedUser": curDate,
    "syncDate": curDate,
    "needSync": true
  };

  let textInstance = {
    dateUpdated: itemSaveData.dateUpdated,
    length: saveTextData.text.length,
    noteGlobalId: saveTextData.noteGlobalId,
    textShort: text.makeShortText(saveTextData.text),
    text: saveTextData.text
  };

  callback(null, textInstance);

  socketTextMessage({workspaceId, textInstance});

  let queryData = {noteGlobalId: saveTextData.noteGlobalId};
  text.update(queryData, setUpdateProps(saveTextData, curDate), {workspaceId}, (err, count) => {
    if (count) {
      item.find(itemQueryData, {workspaceId}, (err, itemInstance) => {
        if (itemInstance) {
          let sendUpdateNoteEvent = false;
          if (itemInstance.title === item.getDefaultTitle()) {
            sendUpdateNoteEvent = true;
            itemSaveData.title = item.prepareNoteTitle(saveTextData.text);
          }

          item.update(itemQueryData, setUpdateProps(itemSaveData, curDate), {workspaceId}, () => {
            if (!itemInstance.offlineOnly) {
              ipcMessage({workspaceId});
            }

            if (sendUpdateNoteEvent) {
              socketNoteMessage({workspaceId, itemInstance});
            }
          });
        }
      });
    }
  });
}

/**
 * @param {{workspaceId:string}} inputData
 */
function ipcMessage(inputData) {
  //ipcConnection.requestAutoSync(inputData);
}

/**
 * @param {{workspaceId:string, itemInstance:item}} inputData
 */
function socketNoteMessage(inputData) {
  const {workspaceId, itemInstance} = inputData;
  socketConnection.sendItemUpdateMessage({
    workspaceId,
    globalId: itemInstance.globalId
  });
}

/**
 * @param {{workspaceId:string, textInstance:text}} inputData
 */
function socketTextMessage(inputData) {
  const {workspaceId, textInstance} = inputData;
  socketConnection.sendTextUpdateMessage({
    workspaceId,
    globalId: textInstance.noteGlobalId,
    typing: true
  });
}
