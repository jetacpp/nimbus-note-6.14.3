import chalk from "chalk";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as text} from "../../../../db/models/text";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiTextList(request, callback) {
  let routeParams = urlParser.getPathParams(request.url);
  let queryParams = urlParser.getQueryParams(request.url) || {};
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let filterParams = <any>urlParser.getFilterParams(request.url);

  let params = {workspaceId};
  let onlyShort = queryParams.onlyShort;

  if (onlyShort) {
    params["onlyShort"] = onlyShort;
  }

  if (filterParams && filterParams.noteGlobalId) {
    text.findAll(filterParams, params, (err, textList) => {
      if(err || !textList) {
        return callback(error.wrongInput(), []);
      }
      return callback(null, text.getResponseListJson(textList, onlyShort));
    });
  } else {
    return callback(error.wrongInput(), []);
  }
}
