import chalk from "chalk";
import {default as text} from "../../../../db/models/text";
import {default as workspace} from "../../../../db/models/workspace";
import {default as urlParser} from "../../../../utilities/urlParser";
import {default as error} from "../../../../utilities/customError";
import {default as NimbusSDK} from "../../../../sync/nimbussdk/net/NimbusSDK";
import {default as appOnlineState} from "../../../../online/state";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";

/**
 * @param {Request} request
 * @param {Function} callback
 */
export default async function apiTextTokenUpdate(request, callback) {
  if (!appOnlineState.get()) {
    return callback(null, {httpStatus: 404});
  }

  let routeParams = urlParser.getPathParams(request.url);
  let workspaceId = typeof (routeParams[3] !== 'undefined') ? await workspace.getLocalId(routeParams[3]) : null;
  let noteGlobalId = typeof (routeParams[5] !== 'undefined') ? routeParams[5] : null;

  if (!noteGlobalId) {
    return callback(error.wrongInput(), null);
  }

  let saveTextData = {
    noteGlobalId,
    tokens: []
  };

  let curDate = date.now();

  NimbusSDK.getApi().getNoteTextToken({
    workspaceId: typeof (routeParams[3] !== 'undefined') ? routeParams[3] : null,
    globalId: noteGlobalId,
    skipSyncHandlers: true
  }, (err, res) => {
    if (err || !res) {
      return callback(error.itemUpdateError(), {httpStatus: 500});
    }

    if (res.tokens && res.tokens.length) {
      saveTextData.tokens = res.tokens;
      const {expiresAt, nodeId, token, url, ttl} = saveTextData.tokens[saveTextData.tokens.length - 1];

      let queryData = {noteGlobalId: saveTextData.noteGlobalId};
      text.update(queryData, setUpdateProps(saveTextData, curDate), {workspaceId}, (err, count) => {
        if (err || !count) {
          return callback(err, null);
        }

        callback(null, {
          expiresAt,
          nodeId,
          token,
          url,
          ttl
        });
      });
    } else {
      return callback(error.itemUpdateError(), {httpStatus: 500});
    }
  });
}
