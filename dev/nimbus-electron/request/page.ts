import chalk from "chalk";
import {default as config} from "../../config";
import {default as appWindowInstance} from "../window/instance";
import {default as childWindow} from "../window/child";
import {default as AutoSyncManager} from "../sync/nimbussdk/manager/AutoSyncManager";
import {default as workspace} from "../db/models/workspace";

import {default as nimbusCommonAssets} from "./assets/nimbus-common/loader";
import {default as nimbusAngularAssets} from "./assets/webnotes/loader";
import {default as nimbusAuthAssets} from "./assets/auth/loader";
import {default as nimbusPaymentAssets} from "./assets/nimbus-payment/loader";
import {default as nimbusUpdateAssets} from "./assets/nimbus-update/loader";
import SelectedWorkspace from "../workspace/SelectedWorkspace";
import {default as App} from "../sync/process/application/App";
import AppRefresh from "../popup/AppRefresh";

let currentPageName;

export default class RequestPage {
  static loadNimbusCommonAssets = nimbusCommonAssets.load;
  static loadNimbusAngularAssets = nimbusAngularAssets.load;
  static loadNimbusAuthAssets = nimbusAuthAssets.load;
  static loadNimbusPaymentAssets = nimbusPaymentAssets.load;
  static loadNimbusUpdateAssets = nimbusUpdateAssets.load;

  /**
   * @return string
   */
  static getCurrentPageName() {
    return currentPageName;
  }

  /**
   * @param {string} pageName
   */
  static setCurrentPageName(pageName) {
    currentPageName = pageName;
  }

  /**
   * @param {string} pageName
   * @param {string|null} url
   */
  static load(pageName, url = null) {
    let appWindow = appWindowInstance.get();
    if (appWindow) {
      setTimeout(function () {
        switch (pageName) {
          case config.PAGE_NOTE_NAME :
            RequestPage.loadNote(appWindow, url);
            break;
          default :
            RequestPage.loadAuth(appWindow);
        }
      }, 100);
    }
  }

  static async navToNoteUrl(url) {
    await RequestPage.reload(false, url);
  }

  static async reload(checkSync = false, url = null) {
    if (RequestPage.isAuth()) {
      return RequestPage.load(config.PAGE_AUTH_NAME);
    } else {
      if(checkSync) {
        const canRefreshApp = await RequestPage.canRefreshApp();
        if(!canRefreshApp) {
          return;
        }
      }
      if (childWindow.get(childWindow.TYPE_PURCHASE)) {
        childWindow.get(childWindow.TYPE_PURCHASE).close();
        childWindow.set(childWindow.TYPE_PURCHASE, null);
      }
      return RequestPage.load(config.PAGE_NOTE_NAME, url);
    }
  }

  static async canRefreshApp() {
    const selectedWorkspaceId = await SelectedWorkspace.getGlobalId();
    const workspaceId = selectedWorkspaceId ? selectedWorkspaceId : null;
    const canStartSync = await App.canStartNewSync({workspaceId});
    if (canStartSync) {
      return true;
    }
    AppRefresh.show();
    return false;
  }

  /**
   * @param {{}} appWindow
   * @param {string|null} url
   */
  // @ts-ignore
  static loadNote(appWindow, url = null) {
    if (appWindow.webContents) {
      appWindow.webContents.removeListener('did-finish-load', RequestPage.didFinishLoadAuthPage);
    }
    appWindow.webContents.removeListener('did-finish-load', RequestPage.didFinishLoadNotePage);
    appWindow.webContents.on('did-finish-load', RequestPage.didFinishLoadNotePage);
    const navUrl = url ? url : `${config.LOCAL_SERVER_HTTP_HOST}${config.nimbusAngularIndexFile}`;
    appWindow.loadURL(navUrl);
  }

  /**
   * @param {{}} appWindow
   */
  static loadAuth(appWindow) {
    if (appWindow.webContents) {
      appWindow.webContents.removeListener('did-finish-load', RequestPage.didFinishLoadNotePage);
    }
    appWindow.webContents.removeListener('did-finish-load', RequestPage.didFinishLoadAuthPage);
    appWindow.webContents.on('did-finish-load', RequestPage.didFinishLoadAuthPage);
    appWindow.loadURL(config.LOCAL_SERVER_HTTP_HOST + config.nimbusAuthIndexFile);
  }

  static async didFinishLoadNotePage() {
    let appWindow = appWindowInstance.get();
    if (appWindow) {
      await nimbusAngularAssets.load(appWindow);
      if (RequestPage.getCurrentPageName() !== config.PAGE_NOTE_NAME) {
        let currentURL = appWindow.webContents.getURL();
        let selectedWorkspace = undefined;
        const workspaceIdFromUrl = RequestPage.getWorkspaceIdFromUrl(currentURL);
        if(workspaceIdFromUrl) {
          selectedWorkspace = await workspace.getById(workspaceIdFromUrl);
        } else {
          selectedWorkspace = <{globalId:string}>await SelectedWorkspace.get();
        }
        const selectedWorkspaceId = selectedWorkspace && !await workspace.isMainForUser(selectedWorkspace) ? selectedWorkspace.globalId : null;
        SelectedWorkspace.setGlobalId(selectedWorkspaceId);
        const availableWorkspaceList = <[string]>await workspace.getAvailableIdList();
        for (let workspaceId of availableWorkspaceList) {
          if (!selectedWorkspaceId && !workspaceId) {
            AutoSyncManager.initTimer({workspaceId, initState: true});
          }

          if (workspaceId && (selectedWorkspaceId === workspaceId)) {
            AutoSyncManager.initTimer({workspaceId, initState: true});
          }
        }
      }
    }
    RequestPage.setCurrentPageName(config.PAGE_NOTE_NAME);
  }

  static getWorkspaceIdFromUrl(url) {
    if(!url) { return undefined }
    const parts = url.split('/');
    if(typeof parts['3'] === 'undefined') { return undefined }
    if(parts['3'] !== 'ws') { return undefined }
    if(typeof parts['4'] === 'undefined') { return undefined }
    if(!parts['4']) { return undefined }
    return parts['4'];
  }

  static async didFinishLoadAuthPage() {
    let appWindow = appWindowInstance.get();
    if (appWindow) {
      await nimbusAuthAssets.load(appWindow);
      AutoSyncManager.clearTimers();
    }
    RequestPage.setCurrentPageName(config.PAGE_AUTH_NAME);
  }

  static isAuth() {
    let appWindow = appWindowInstance.get();
    if (appWindow) {
      let currentUrl = appWindow.webContents.getURL();
      if (currentUrl.indexOf("/auth/web/auth/") >= 0) {
        return true;
      }
    }
    return false;
  }
}
