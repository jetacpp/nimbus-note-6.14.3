import chalk from "chalk";
import fs = require('fs-extra');
import {default as config} from "../../../../config";
import {default as moduleLoader} from "./moduleLoader";
import {default as errorHandler} from "../../../utilities/errorHandler";

export default class CommonAssetsLoader {
  /**
   * @param {{}} appWindow
   * @param {string} assetsPath
   * @param {Function} callback
   */
  static async load(appWindow, assetsPath, callback = (err, res) => {
  }) {
    let pageContent = appWindow.webContents;

    try {
      let elCommonCssPath = `${config.nimbusCommonAssetsPath}/css/common.css`;
      const cssContent = await fs.readFile(elCommonCssPath, config.DEFAULT_CHARSET);
      await pageContent.insertCSS(cssContent.toString());

      let elCommonJsPath = `${config.nimbusCommonAssetsPath}/js/common.js`;
      const jsContent = await fs.readFile(elCommonJsPath, config.DEFAULT_CHARSET);
      await pageContent.executeJavaScript(jsContent.toString());
    } catch (err) {
      errorHandler.log(`Problem to load common assets: ${err.toString()}`);
      return null;
    }

    try {
      await moduleLoader.load(appWindow);
    } catch (err) {
      errorHandler.log(`Problem to load page module: ${err.toString()}`);
      return null;
    }

    try {
      let elCommonCssPath = `${assetsPath}/css/custom.css`;
      const cssContent = await fs.readFile(elCommonCssPath, config.DEFAULT_CHARSET);
      await pageContent.insertCSS(cssContent.toString());

      let elCommonJsPath = `${assetsPath}/js/custom.js`;
      const jsContent = await fs.readFile(elCommonJsPath, config.DEFAULT_CHARSET);
      await pageContent.executeJavaScript(jsContent.toString());
    } catch (err) {
      errorHandler.log(`Problem to load page assets ${err.toString()}`);
      return null;
    }

    return pageContent;
  }
}
