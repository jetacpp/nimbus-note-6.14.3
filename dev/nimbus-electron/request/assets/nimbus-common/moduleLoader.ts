import fs = require('fs-extra');
import {default as errorHandler} from "../../../utilities/errorHandler";
import {default as config} from "../../../../config";

export default class CommonModuleLoader {
  /**
   * @param {appWindowInstance} appWindow
   */
  static load(appWindow) {
    return new Promise(async (resolve) => {
      let elPluginJsPath;
      let elPluginCssPath;

      elPluginCssPath = `${config.nimbusModulesPath}/jquery-toast-plugin/dist/jquery.toast.min.css`;
      elPluginJsPath = `${config.nimbusModulesPath}/jquery-toast-plugin/dist/jquery.toast.min.js`;
      await CommonModuleLoader.loadModuleFiles(appWindow.webContents, elPluginCssPath, elPluginJsPath);

      resolve(appWindow.webContents);
    });
  }

  /**
   * @param {*} pageContent
   * @param {string} elPluginCssPath
   * @param {string} elPluginJsPath
   */
  static async loadModuleFiles(pageContent, elPluginCssPath, elPluginJsPath) {
    try {
      const cssContent = await fs.readFile(elPluginCssPath, config.DEFAULT_CHARSET);
      pageContent.insertCSS(cssContent.toString());

      const jsContent = await fs.readFile(elPluginJsPath, config.DEFAULT_CHARSET);
      pageContent.executeJavaScript(jsContent);
    } catch (err) {
      errorHandler.log(`Problem: moduleLoader => to load module: ${err.toString()}`);
    }
  }
}
