import {default as config} from "../../../../config";

import {default as nimbusCommonAssets} from "../nimbus-common/loader";
import {default as NimbusSDK} from "../../../sync/nimbussdk/net/NimbusSDK";
import {default as user} from "../../../db/models/user";

export default class PaymentAssetsLoader {
  static load(appWindow) {
    nimbusCommonAssets.load(appWindow, config.nimbusPaymentAssetsPath, () => {
      NimbusSDK.getApi().userLogin((err, login) => {
        user.find({login: login}, {}, (err, userInfo) => {
          let importAssetsData = {};
          if (userInfo) {
            // @ts-ignore
            importAssetsData.language = userInfo.language;
          }
          appWindow.webContents.send('event:initApp:request', importAssetsData);
        });
      });
    });
  }
}
