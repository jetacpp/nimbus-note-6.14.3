import {default as config} from "../../../../config";
import {default as nimbusCommonAssets} from "../nimbus-common/loader";

export default class WebAuthAssetsLoader {
  static async load(appWindow) {
    await nimbusCommonAssets.load(appWindow, config.nimbusAuthAssetsPath);
  }
}


