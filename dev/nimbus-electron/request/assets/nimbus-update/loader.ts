import chalk from "chalk";
import {default as config} from "../../../../config";
import {default as nimbusCommonAssets} from "../nimbus-common/loader";
import {default as NimbusSDK} from "../../../sync/nimbussdk/net/NimbusSDK";
import {default as user} from "../../../db/models/user";
import Translations from "../../../translations/Translations";

export default class UpdateAssetsLoader {
  static load(appWindow) {
    nimbusCommonAssets.load(appWindow, config.nimbusUpdateAssetsPath, () => {
      NimbusSDK.getApi().userLogin((err, login) => {
        user.find({login: login}, {}, (err, userInfo) => {
          let importAssetsData = {
            trans: [],
            language: Translations.getLang()
          };
          if (userInfo) {
            // @ts-ignore
            importAssetsData.language = userInfo.language;
          }
          appWindow.webContents.send('event:initApp:request', importAssetsData);
        });
      });
    });
  }
}
