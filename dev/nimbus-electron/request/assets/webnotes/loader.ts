import {default as config} from "../../../../config";
import {default as nimbusCommonAssets} from "../nimbus-common/loader";
import {default as appOnlineState} from "../../../online/state";
import {default as NimbusSDK} from "../../../sync/nimbussdk/net/NimbusSDK";
import {default as user} from "../../../db/models/user";
import {default as menu} from "../../../menu/menu";
import {Menu, MenuItem} from "electron";
import {default as trans} from "../../../translations/Translations";

export default class WebNotesAssetsLoader {
  static async load(appWindow) {
    const pageContent = await nimbusCommonAssets.load(appWindow, config.nimbusAngularAssetsPath);
    if(!pageContent) { return; }

    let importAssetsData = <any>{appOnlineStatus: appOnlineState.get()};
    NimbusSDK.getApi().userLogin((err, login) => {
      user.find({login: login}, {}, (err, userInfo) => {
        if (userInfo) {
          importAssetsData.language = userInfo.language;
          importAssetsData.originURL = config.LOCAL_SERVER_HTTP_HOST;
          importAssetsData.contextMenu = menu.getContextMenu();
        }
        pageContent.send('event:initApp:request', importAssetsData);
        pageContent.send('event:client:login:response', {login: login});
      });
    });

    pageContent.on('context-menu', (event, params) => {
      const menuItems = []

      if(params.dictionarySuggestions && params.dictionarySuggestions.length) {
        for (const suggestion of params.dictionarySuggestions) {
          menuItems.push({
            label: suggestion,
            click: () => pageContent.replaceMisspelling(suggestion)
          })
        }

        menuItems.push({
          type: 'separator'
        })
      }

      if (params.misspelledWord) {
        menuItems.push(
            new MenuItem({
              label: `${trans.get('menu_edit__add_to_dictionary')}`,
              click: () => pageContent.session.addWordToSpellCheckerDictionary(params.misspelledWord)
            })
        )

        menuItems.push({
          type: 'separator'
        })
      }

      for(const menuItem of menu.getContextMenu()) {
        menuItems.push(menuItem)
      }

      const contextMenu = Menu.buildFromTemplate(menuItems)
      contextMenu.popup()
    })
  }
}
