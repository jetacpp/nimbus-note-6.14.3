import chalk from "chalk";
import {default as config} from "../../../config";
import {default as auth} from "../../auth/auth";
import {default as appOnlineState} from "../../online/state";
import {default as NimbusSDK} from "../../sync/nimbussdk/net/NimbusSDK";
import {default as SyncManager} from "../../sync/process/SyncManager";
import {default as appPage} from "../page";
import {default as pdb} from "../../../pdb";
import {default as orgs} from "../../db/models/orgs";

export default class Registration {
  static ERROR_REGISTRATION_FAILED = -1;
  static ERROR_REGISTRATION_OFFLINE = -9;

  /**
   * @param {{}} frmData
   * @param {Function} callback
   */
  static tryRegisterClient(frmData = null, callback = (res) => {
  }) {
    if (frmData && callback) {
      if(frmData.login) {
        frmData.login = frmData.login.toLowerCase();
      }

      if (appOnlineState.get()) {
        NimbusSDK.getApi().signUp(frmData.login, frmData.password, (err, response) => {
          if (err) {
            let errorResponse = {errorCode: err};
            if(response) {
              if(response.challange) {
                errorResponse['challenge'] = response.challenge;
              }
              if(response.body) {
                errorResponse['body'] = response.body;
              }
            }
            return callback(errorResponse);
          }

          frmData = auth.prepareFormSyncData(frmData, response);
          frmData.authProvider = '';

          auth.register(frmData, (err, formResponse) => {
            if (formResponse.errorCode) {
              return callback(formResponse);
            }

            if (formResponse && formResponse.data && formResponse.data.email) {
              NimbusSDK.getAccountManager().setUserEmail(formResponse.data.email, () => {
                auth.getUser(async (err, authInfo) => {
                  if (authInfo && Object.keys(authInfo).length) {
                    if (pdb.getDbClientId() != authInfo.userId) {
                      pdb.setDbClientId(authInfo.userId);
                    }

                    SyncManager.clearAllSyncData();
                    if (appOnlineState.get()) {
                      await orgs.syncUserOrganizationsWorkspaces();
                    }

                    return appPage.load(config.PAGE_NOTE_NAME);
                  } else {
                    return appPage.load(config.PAGE_AUTH_NAME);
                  }
                });
              });
            } else {
              return appPage.load(config.PAGE_AUTH_NAME);
            }
          });
        });
      } else {
        return callback({errorCode: Registration.ERROR_REGISTRATION_OFFLINE});
      }
    }
  }
}
