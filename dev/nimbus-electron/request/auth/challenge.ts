import {default as appOnlineState} from "../../online/state";
import {default as NimbusSDK} from "../../sync/nimbussdk/net/NimbusSDK";

export default class Challenge {
    static ERROR_CHALLENGE_OFFLINE = -9;

    /**
     * @param {{}} frmData
     * @param {Function} callback
     */
    static tryChallengeClient(frmData = null, callback = (res) => {
    }) {
        if (appOnlineState.get()) {
            NimbusSDK.getApi().challenge(frmData, (err, response) => {
                if (err) {
                    let errorResponse = {errorCode: err};
                    if(response) {
                        if(response.challenge) {
                            errorResponse['challenge'] = response.challenge;
                        }
                        if(response.body) {
                            errorResponse['body'] = response.body;
                        }
                    }
                    return callback(errorResponse);
                }

                return callback(response);
            });
        } else {
            return callback({errorCode: Challenge.ERROR_CHALLENGE_OFFLINE});
        }
    }
}
