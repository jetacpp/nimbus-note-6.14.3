import {ipcMain} from "electron";
import {default as config} from "../../../config";

import {default as appOnlineState} from "../../online/state";
import {default as NimbusSDK} from "../../sync/nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../sync/process/events/SyncStatusChangedEvent";
import {default as SyncManager} from "../../sync/process/SyncManager";
import {default as ReminderNotice} from "../../popup/ReminderNotice";
import {default as appPage} from "../page";
import {default as workspace} from "../../db/models/workspace";

export default class Logout {
  // @ts-ignore
  static async tryLogoutClient() {
    setTimeout(async () => {
      const availableWorkspaceList = <[string]>await workspace.getAvailableIdList();
      for (let workspaceId of availableWorkspaceList) {
        await SyncStatusChangedEvent.setPauseStatus({workspaceId});
      }
    }, 1);

    setTimeout(async () => {
      Logout.logoutNimbusClient();
    }, 100);

    setTimeout(async () => {
      appPage.load(config.PAGE_AUTH_NAME);
    }, 250);

    setTimeout(() => {
      SyncManager.clearAllSyncData();
      ReminderNotice.flushAll();
    }, 1000);
  }

  /**
   * @param {{}} frmData
   */
  // @ts-ignore
  static async tryLogoutOnlineClient(frmData) {
    let logout = frmData.logout;
    if (logout) {
      await Logout.logoutNimbusClient();
    }
  }

  static logoutNimbusClient() {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (appOnlineState.get()) {
        // NimbusSDK.getApi().logout(() => {
          resolve(true);
        // });
      } else {
        NimbusSDK.getApi().clearAccountSession(() => {
          resolve(true);
        });
      }
    });
  }

  static listenRequest() {
    ipcMain.on('event:logout:response', () => {
      Logout.tryLogoutClient();
    });
    ipcMain.on('event:logout:online:response', (event, arg) => {
      Logout.tryLogoutOnlineClient(arg);
    });
  }
}
