import {default as config} from "../../../config.runtime";
import {default as NimbusSDK} from "../../sync/nimbussdk/net/NimbusSDK";

export default class Remind {
    /**
     * @param {{}} frmData
     * @param {Function} callback
     */
    static tryRemindPassword(frmData = null, callback = (res) => {
    }) {
        if (!frmData) {
            return callback({errorCode: -2});
        }

        if (!frmData.login) {
            return callback({errorCode: -2});
        }

        apiRestorePassword(frmData.login, (err, repsponse) => {
            if (err) { return callback({errorCode: err}); }
            return callback({});
        });
    }
}

/**
 * @param {string} email
 * @param {Function} callback
 */
function apiRestorePassword(email, callback = (err, res) => {
}) {
    NimbusSDK.getApi().remindPassword(email, (err, response) => {
        callback(err, response);
    });
}
