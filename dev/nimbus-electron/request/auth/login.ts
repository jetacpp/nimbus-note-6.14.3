import chalk from "chalk";
import {default as config} from "../../../config";
import {default as auth} from "../../auth/auth";
import {default as appOnlineState} from "../../online/state";
import {default as NimbusSDK} from '../../sync/nimbussdk/net/NimbusSDK';
import {default as SyncManager} from "../../sync/process/SyncManager";
import {default as appPage} from "../page";
import {default as menu} from "../../menu/menu";
import {default as trans} from "../../translations/Translations";
import {default as userModel} from "../../db/models/user";
import {default as pdb} from "../../../pdb";
import {default as orgs} from "../../db/models/orgs";
import {default as generator} from "../../utilities/generatorHandler";

export default class Login {
  static AUTH_PROVIDER_GOOGLE = 'google';
  static AUTH_PROVIDER_FACEBOOK = 'facebook';

  /**
   * @param {string} login
   * @param {string} sessionId
   * @param {string} provider
   * @param {Function} callback
   */
  static tryAuthClient(login, sessionId, provider = '', callback = (res) => {
  }) {
    if (appOnlineState.get()) {

      // login = '';
      // sessionId = '';

      NimbusSDK.getApi().auth(login, sessionId, (err, response) => {
        if (err || !response) {
          return callback({errorCode: err});
        }

        let frmData:any = {login: login.toLowerCase()};

        frmData = auth.prepareFormSyncData(frmData, response);
        frmData.authProvider = provider;

        // @ts-ignore
        userModel.find({email: frmData.login}, {}, async (err, itemInstance) => {
          if (itemInstance) {
            if (itemInstance.language) { frmData.language = itemInstance.language; }
            Login.offlineClientLogin(frmData, {oauth: true}, callback);
          } else {
            frmData.password = generator.randomString(8)
            Login.offlineClientRegister(frmData, callback);
          }
        });
      });
    }
  }

  /**
   * @param {{}} frmData
   * @param {Function} callback
   */
  static tryLoginClient(frmData = null, callback = (res) => {
  }) {
    if (frmData && callback) {
      if(frmData.login) {
        frmData.login = frmData.login.toLowerCase();
      }

      if (appOnlineState.get()) {

        NimbusSDK.getApi().signIn(frmData.login, frmData.password, (err, response) => {
          if (err) {
            let errorResponse = {errorCode: err}
            if(response) {
              if(response.challenge) {
                errorResponse['challenge'] = response.challenge;
              }
              if(response.body) {
                errorResponse['body'] = response.body;
              }
            }
            return callback(errorResponse);
          }

          frmData = auth.prepareFormSyncData(frmData, response);
          frmData.authProvider = '';

          // @ts-ignore
          userModel.find({email: frmData.login}, {}, async (err, itemInstance) => {
            if (itemInstance) {
              if (itemInstance.language) { frmData.language = itemInstance.language; }
              await auth.updateUserPassword(frmData);
              Login.offlineClientLogin(frmData, {}, callback);
            } else {
              Login.offlineClientRegister(frmData, callback);
            }
          });
        });
      } else {
        Login.offlineClientLogin(frmData, {}, callback);
      }
    } else {
      Login.offlineClientAutoLogin();
    }
  }

  /**
   * @param {{}} frmData
   * @param {{oauth:boolean}} options
   * @param {Function} callback
   */
  static offlineClientLogin(frmData, options, callback) {
    auth.login(frmData, options, (err, formResponse) => {
      if (formResponse.errorCode) {
        return callback(formResponse);
      }

      if (frmData.language) {
        trans.setLang(frmData.language);
        menu.update();
      }

      if (formResponse && formResponse.data && formResponse.data.email) {
        NimbusSDK.getAccountManager().setUserEmail(formResponse.data.email, () => {
          callback(formResponse);

          auth.getUser(async (err, authInfo) => {
            if (authInfo && Object.keys(authInfo).length) {
              if (pdb.getDbClientId() != authInfo.userId) {
                pdb.setDbClientId(authInfo.userId);
              }

              SyncManager.clearAllSyncData();
              if (appOnlineState.get()) {
                await orgs.syncUserOrganizationsWorkspaces();
              }

              return appPage.load(config.PAGE_NOTE_NAME);
            } else {
              return appPage.load(config.PAGE_AUTH_NAME);
            }
          });
        });
      } else {
        return appPage.load(config.PAGE_AUTH_NAME);
      }
    });
  }

  /**
   * @param {{}} frmData
   * @param {Function} callback
   */
  static offlineClientRegister(frmData, callback) {
    auth.register(frmData, (err, formResponse) => {
      if (formResponse.errorCode) {
        return callback(formResponse);
      }

      if(formResponse.data && formResponse.data.userId) {
        if (pdb.getDbClientId() != formResponse.data.userId) {
          pdb.setDbClientId(formResponse.data.userId)
        }
      }

      callback(null, formResponse);

      auth.getUser(async (err, authInfo) => {
        if (authInfo && Object.keys(authInfo).length) {
          if (pdb.getDbClientId() != authInfo.userId) {
            pdb.setDbClientId(authInfo.userId);
          }

          SyncManager.clearAllSyncData();
          if (appOnlineState.get()) {
            await orgs.syncUserOrganizationsWorkspaces();
          }

          return appPage.load(config.PAGE_NOTE_NAME);
        } else {
          return appPage.load(config.PAGE_AUTH_NAME);
        }
      });
    });
  }

  static offlineClientAutoLogin() {
    if (config.CLIENT_AUTO_LOGIN) {
      auth.authorized((err, client) => {
        if (client) {
          if (client && client.email) {
            NimbusSDK.getAccountManager().setUserEmail(client.email, () => {
              appPage.load(config.PAGE_NOTE_NAME);
            });
          }
        } else {
          appPage.load(config.PAGE_AUTH_NAME);
        }
      });
    } else {
      return appPage.load(config.PAGE_AUTH_NAME);
    }
  }
}
