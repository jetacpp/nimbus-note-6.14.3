import {default as appWindow} from "../../../window/instance";

export const oauthWindowParams = {
    useContentSize: true,
    center: true,
    show: false,
    resizable: true,
    alwaysOnTop: true,
    'standard-window': true,
    autoHideMenuBar: true,
    webPreferences: {
        devTools: false,
        nodeIntegration: false,
        webSecurity: false,
        plugins: true,
    },
};

export const oauthRejectHandler = () => {
    appWindow.get().webContents.send('event:oauth:close', {});
};
