import { BrowserWindow, session } from 'electron';
import facebookOauth from './provider/index';
import config from "../../../../../config";
import {oauthWindowParams, oauthRejectHandler} from "../window";

const FACEBOOK_OAUTH_SCOPES = ['profile', 'email'];

const info = {
    key: config.CLIENT_ID_FACEBOOK,
    secret: config.CLIENT_SECRET_FACEBOOK,
    scope: FACEBOOK_OAUTH_SCOPES,
    redirect_uri: config.CLIENT_URL_FACEBOOK,
};

export default class FacebookAuth {
    static async auth() {
        const window = new BrowserWindow({
            ...oauthWindowParams,
            title: 'Facebook Auth',
        });
        return await facebookOauth.auth(info, session, window, oauthRejectHandler);
    }

    static async getAccessToken(authorizationCode) {
        try {
            const token = await facebookOauth.getAccessToken(
                authorizationCode,
                info,
            );

            if(!token) { throw new Error('Empty token payload'); }
            const {access_token: accessToken} = token;
            if(!accessToken) { throw new Error('Empty id_token payload'); }

            return accessToken;
        } catch (error) {
            console.log(error);
            return null;
        }
    }

    static logout() {
        const window = new BrowserWindow({
            ...oauthWindowParams,
            title: 'Facebook Logout',
        });
        return facebookOauth.logout(info, session, window);
    }
}
