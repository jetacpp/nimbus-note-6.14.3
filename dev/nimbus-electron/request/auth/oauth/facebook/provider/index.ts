import url from 'url';
import {Oauth, LOGIN_FAILURE_URL, LOGIN_SUCCESS_URL, IToken} from './oauth';

const getAccessToken = async (code, info):Promise<IToken> => {
    try {
        const oauth = new Oauth(info);
        return await oauth.getTokens(code);
    } catch (error) {
        console.log(error);
        return null;
    }
};

const bindWindowsLoginEvents = (oauth, session, win, rejectHandler = () => {}) => {
    win.on('closed', () => rejectHandler());

    win.webContents.on('did-finish-load', async () =>  {
        win.show();
    });

    win.on('page-title-updated', async () => {
        setImmediate(() => {
            const title = win.getTitle();
            if (title.startsWith('Denied')) {
                rejectHandler();
                win.removeAllListeners('closed');
                win.removeAllListeners('page-title-updated');
                win.close();
            } else if (title.startsWith('Success')) {
                win.removeAllListeners('closed');
                win.removeAllListeners('page-title-updated');
                win.close();
            }
        });
    });

    sessionHandler(oauth, session, win);
};

const bindWindowsLogoutEvents = (oauth, session, win) => {
    win.on('closed', () => console.log('Facebook logout success'));

    win.on('page-title-updated', async () => {
        await win.webContents.session.clearStorageData({
            storages: ['cookies', 'localstorage', 'indexdb'],
        });
        win.close();
    });

    sessionHandler(oauth, session, win, true);
};

const sessionHandler = (oauth, session, win, logout = false) => {
    const filterFailure = {
        urls: [`${LOGIN_FAILURE_URL}`],
    };

    session.defaultSession.webRequest.onCompleted(filterFailure, async (details) => {
        window.close();
    });

    const filterSuccess = {
        urls: [`${LOGIN_SUCCESS_URL}*`],
    };

    session.defaultSession.webRequest.onCompleted(filterSuccess, null);
    if(logout) {
        session.defaultSession.webRequest.onCompleted(filterSuccess, async (details) => {
            try {
                await win.webContents.session.clearStorageData({
                    storages: ['cookies', 'localstorage', 'indexdb'],
                });
                win.close();
            } catch (error) {
                console.log(error);
            }
        });
    } else {
        session.defaultSession.webRequest.onCompleted(filterSuccess, async (details) => {
            try {
                const redirectPath = oauth.info.redirect_uri;
                const queryString = url.parse(details.url).query;
                win.loadURL(`${redirectPath}?${queryString}`);
            } catch (error) {
                console.log(error);
            }
        });
    }
};

const auth = async (info, session, window, rejectHandler = () => {}) => {
    const oauth = new Oauth(info);
    const events = bindWindowsLoginEvents(oauth, session, window, rejectHandler);
    loadAuthUrl(oauth, window);
    return events;
};

const logout = async (info, session, window) => {
    const oauth = new Oauth(info);
    const events = bindWindowsLogoutEvents(oauth, session, window);
    loadAuthUrl(oauth, window);
    return events;
};

const loadAuthUrl = (oauth, window) => {
    window.loadURL(oauth.getAuthUrl(), {userAgent: 'Chrome'});
};

export default {
    auth,
    getAccessToken,
    logout,
}
