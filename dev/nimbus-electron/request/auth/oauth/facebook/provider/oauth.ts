import {OAuth2} from 'oauth';

export interface IInfo {
    key:string
    secret:string,
    scope:[string],
    redirect_uri:string,
}

export interface IToken {
    access_token:string
    token_type:string
    expires_in:number
}

export const LOGIN_SUCCESS_URL = 'https://www.facebook.com/connect/login_success.html';
export const LOGIN_FAILURE_URL = 'https://www.facebook.com/dialog/close';

export const BASE_SITE_URL = '';
export const AUTHORIZE_URL = 'https://www.facebook.com/dialog/oauth';
export const ACCESS_TOKEN_URL = 'https://graph.facebook.com/oauth/access_token';

export class Oauth {
    info:IInfo
    oauth:OAuth2

    constructor(info:IInfo){
        this.info = info
        this.oauth = new OAuth2(
            info.key, 
            info.secret,
            BASE_SITE_URL,
            AUTHORIZE_URL,
            ACCESS_TOKEN_URL,
        )
    }

    getAuthUrl() {
        return this.oauth.getAuthorizeUrl({
                redirect_uri: LOGIN_SUCCESS_URL,
                scope: this.info.scope,
            });
    }

    getTokens(code):Promise<IToken> {
        return new Promise((resolve,reject) => {
            this.oauth.getOAuthAccessToken(code, {
                redirect_uri: LOGIN_SUCCESS_URL,
            }, (error, _, __, result:IToken) => {
                if(error) {
                    return reject(error)
                }
                resolve(result)
            })
        })
    }
}
