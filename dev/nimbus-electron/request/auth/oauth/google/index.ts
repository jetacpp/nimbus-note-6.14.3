import googleOauth from './provider/index';
import config from "../../../../../config";
import {oauthWindowParams, oauthRejectHandler} from "../window";

const googleAuth = googleOauth({
    ...oauthWindowParams,
    title: 'Google Auth',
}, oauthRejectHandler);

const GOOGLE_OAUTH_SCOPES = ['profile', 'email', 'openid'];

export default class GoogleAuth {
    static async auth() {
        return await googleAuth.getAuthorizationCode(
            GOOGLE_OAUTH_SCOPES,
            config.CLIENT_ID_GOOGLE,
            config.CLIENT_SECRET_GOOGLE,
            config.CLIENT_URL_GOOGLE,
        );
    }

    static async getIdToken(authorizationCode) {
        try {
            const token = await googleAuth.getAccessToken(
                authorizationCode,
                config.CLIENT_ID_GOOGLE,
                config.CLIENT_SECRET_GOOGLE,
                config.CLIENT_URL_GOOGLE,
            );

            if(!token) { throw new Error('Empty token payload'); }
            const {id_token: idToken} = token;
            if(!idToken) { throw new Error('Empty id_token payload'); }

            return idToken;
        } catch (error) {
            console.log(error);
            return null;
        }
    }

    static async logout() {
        return await googleAuth.logout(
            GOOGLE_OAUTH_SCOPES,
            config.CLIENT_ID_GOOGLE,
            config.CLIENT_SECRET_GOOGLE,
            config.CLIENT_URL_GOOGLE,
        );
    }
}
