import {BrowserWindow} from "electron";
import {stringify} from "querystring";
import google from "googleapis";
import fetch from "node-fetch";
import errorHandler from "../../../../../utilities/errorHandler";

const OAuth2 = google.auth.OAuth2;

const FETCH_TOKEN_URL = 'https://accounts.google.com/o/oauth2/token';
const REDIRECT_URL = 'urn:ietf:wg:oauth:2.0:oob';

const getAuthenticationUrl = (scopes, clientId, clientSecret, redirectUri = REDIRECT_URL) => {
    const oauth2Client = new OAuth2(clientId, clientSecret, redirectUri);
    return oauth2Client.generateAuthUrl({ access_type: 'offline', scope: scopes });
};

const authorizeApp = (url, browserWindowParams, rejectHandler = () => {}) => {
    return new Promise((resolve, reject) => {
        const win = new BrowserWindow(browserWindowParams);

        win.webContents.on('did-finish-load', () => win.show());

        win.on('closed', () => {
            rejectHandler();
            reject(new Error('User closed the window'));
        });

        win.on('page-title-updated', () => {
            setImmediate(() => {
                const title = win.getTitle();
                if (title.startsWith('Denied')) {
                    reject(new Error(title.split(/[ =]/)[2]));
                    win.removeAllListeners('closed');
                    win.removeAllListeners('page-title-updated');
                    rejectHandler();
                    win.close();
                } else if (title.startsWith('Success')) {
                    resolve(title.split(/[ =]/)[2]);
                    win.removeAllListeners('closed');
                    win.removeAllListeners('page-title-updated');
                    win.close();
                }
            });
        });

        loadAuthUrl(url, win);
    });
};

const logoutApp = (url, browserWindowParams) => {
    const win = new BrowserWindow(browserWindowParams);

    win.on('closed', () => console.log('Google logout success'));

    win.on('page-title-updated', async () => {
        errorHandler.log('fire page-title-updated?')
        await win.webContents.session.clearStorageData({
            storages: ['cookies', 'localstorage', 'indexdb'],
        });
        errorHandler.log('close logout')
        win.close();
    });

    errorHandler.log('loadAuthUrl url?')
    errorHandler.log(url)
    loadAuthUrl(url, win);
};

export default function googleOauth(browserWindowParams, rejectHandler = () => {}, httpAgent = null) {
    const getAuthorizationCode = (scopes, clientId, clientSecret, redirectUri = REDIRECT_URL) => {
        const url = getAuthenticationUrl(scopes, clientId, clientSecret, redirectUri);
        return authorizeApp(url, browserWindowParams, rejectHandler);
    };

    const getAccessToken = async (authorizationCode, clientId, clientSecret, redirectUri = REDIRECT_URL) => {
        const data = stringify({
            code: authorizationCode,
            client_id: clientId,
            client_secret: clientSecret,
            grant_type: 'authorization_code',
            redirect_uri: redirectUri,
        });

        const res = await fetch(FETCH_TOKEN_URL, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: data,
            agent: httpAgent
        });
        return res.json();
    };

    const logout = async (scopes, clientId, clientSecret, redirectUri = REDIRECT_URL) => {
        errorHandler.log('start logout')
        const url = getAuthenticationUrl(scopes, clientId, clientSecret, redirectUri);
        errorHandler.log('process logout url?')
        errorHandler.log(url)
        return logoutApp(url, browserWindowParams);
    };

    return {
        getAuthorizationCode,
        getAccessToken,
        logout,
    };
};

const loadAuthUrl = (url, window) => {
    window.loadURL(url, {userAgent: 'Chrome'});
};
