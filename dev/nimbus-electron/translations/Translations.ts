import {default as auth} from "../auth/auth";
import {default as trans} from "./translationsData";

let lang = "";

export default class Translations {
  static initLang() {
    // @ts-ignore
    return new Promise((resolve) => {
      auth.getUser((err, authInfo) => {
        if (authInfo && Object.keys(authInfo).length && authInfo.language) {
          Translations.setLang(authInfo.language);
          return resolve(Translations.getLang());
        }
        Translations.setLang("en");
        return resolve(Translations.getLang());
      });
    });
  }

  /**
   * @param {string} newLang
   * @return string
   */
  static setLang(newLang) {
    lang = newLang;
    return lang;
  }

  /**
   * @return {string}
   */
  static getLang() {
    if (lang) {
      return lang;
    }
    return "en";
  }

  /**
   * @param {string} transKey
   * @returns {string}
   */
  static get(transKey) {
    if (typeof (trans[transKey]) === "undefined") {
      return transKey;
    }

    if (typeof (trans[transKey][Translations.getLang()]) === "undefined") {
      return transKey;
    }

    return trans[transKey][Translations.getLang()];
  }
}
