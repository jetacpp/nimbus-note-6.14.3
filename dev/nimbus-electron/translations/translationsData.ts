let trans = {};

trans["popup_crash__title"] = {
  'en': `Oops, we found a problem :(`,
  'ru': `К сожалению, мы обнаружили проблему :(`,
  'de': `Ups, wir haben ein Problem gefunden :(`
};

trans["popup_crash__message"] = {
  'en': `We apologize for any inconvenience, please Reload Nimbus Note`,
  'ru': `Приносим извинения за возможные неудобства, пожалуйста, Перезагрузите Nimbus Note`,
  'de': `Wir entschuldigen uns für etwaige Unannehmlichkeiten. Bitte laden Sie Nimbus Note erneut`
};

trans["popup_crash__close_app"] = {
  'en': `Close`,
  'ru': `Закрыть`,
  'de': `Schließen`
};

trans["popup_crash__refresh_app"] = {
  'en': `Reload Nimbus Note`,
  'ru': `Перезагрузить Nimbus Note`,
  'de': `Nachladen Nimbus Note`
};

trans["cache_clear__confirm_message"] = {
  'en': `Are you sure you wish to perform a clean sync? All Nimbus files stored on your device, including notes and attachments, will be erased. Any changes that have not been synced will be lost.`,
  'ru': `Вы действительно хотите выполнить повторную синхронизацию? Все файлы Nimbus, хранящиеся на вашем устройстве, включая заметки и вложения, будут удалены. Любые изменения, которые не были синхронизированы, будут потеряны.`,
  'de': `Möchten Sie eine saubere Synchronisierung wirklich durchführen? Alle auf Ihrem Gerät gespeicherten Nimbus-Dateien, einschließlich Notizen und Anhänge, werden gelöscht. Alle Änderungen, die nicht synchronisiert wurden, gehen verloren.`
};

trans["menu_account__start_sync"] = {
  'en': `Start Sync`,
  'ru': `Запустить синхронизацию`,
  'de': `Starten Sie die Synchronisierung`
};

trans["menu_account__traffic_info"] = {
  'en': `Account information`,
  'ru': `Информация об аккаунте`,
  'de': `Kontoinformationen`
};

trans["menu_account__export"] = {
  'en': `Export all notes`,
  'ru': `Экспорт всех заметок`,
  'de': `Export all notes`
};

trans["menu_account"] = {
  'en': `Account`,
  'ru': `Учетная запись`,
  'de': `Konto`
};

trans["menu_edit"] = {
  'en': `Edit`,
  'ru': `Редактировать`,
  'de': `Bearbeiten`
};

trans["menu_edit__undo"] = {
  'en': `Undo`,
  'ru': `Аннулировать`,
  'de': `Rückgängig machen`
};

trans["menu_edit__redo"] = {
  'en': `Redo`,
  'ru': `Восстановить`,
  'de': `Wiederholen`
};

trans["menu_edit__cut"] = {
  'en': `Cut`,
  'ru': `Вырезать`,
  'de': `Schneiden`
};

trans["menu_edit__copy"] = {
  'en': `Copy`,
  'ru': `Копировать`,
  'de': `Kopieren`
};

trans["menu_edit__paste"] = {
  'en': `Paste`,
  'ru': `Вставить`,
  'de': `Einfügen`
};

trans["menu_edit__lookup"] = {
  'en': `Look Up `,
  'ru': `Найти `,
  'de': `Schau hoch `
};

trans["menu_edit__pasteandmatchstyle"] = {
  'en': `Paste without format`,
  'ru': `Вставить без стилей`,
  'de': `Einfügen ohne Format`
};

trans["menu_edit__delete"] = {
  'en': `Delete`,
  'ru': `Удалить`,
  'de': `Löschen`
};

trans["menu_edit__selectall"] = {
  'en': `Select All`,
  'ru': `Выбрать все`,
  'de': `Wählen Sie Alle`
};

trans["menu_edit__add_to_dictionary"] = {
  'en': `Add to dictionary`,
  'ru': `Добавить в словарь`,
  'de': `Zum Wörterbuch hinzufügen`
};

trans["menu_edit__find_notes"] = {
  'en': `Find…`,
  'ru': `Найти…`,
  'de': `Finden…`
};

trans["menu_edit__add_note"] = {
  'en': `Add note`,
  'ru': `Добавить заметку`,
  'de': `Notiz hinzufügen`
};

trans["menu_view"] = {
  'en': `View`,
  'ru': `Вид`,
  'de': `Ansicht`
};

trans["menu_view__reload"] = {
  'en': `Refresh`,
  'ru': `Обновить`,
  'de': `Aktualisierung`
};

trans["menu_view__resetzoom"] = {
  'en': `Actual Size`,
  'ru': `Фактический Размер`,
  'de': `Tatsächliche Größe`
};

trans["menu_view__zoomin"] = {
  'en': `Zoom In`,
  'ru': `Увеличить`,
  'de': `Zoom zurücksetzen`
};

trans["menu_view__zoomout"] = {
  'en': `Zoom Out`,
  'ru': `Уменьшить`,
  'de': `Rauszoomen`
};

trans["menu_view__togglefullscreen"] = {
  'en': `Toggle Full Screen`,
  'ru': `Включить полноэкранный режим`,
  'de': `Vollbild umschalten`
};

trans["menu_window"] = {
  'en': `Window`,
  'ru': `Окно`,
  'de': `Fenster`
};

trans["menu_window__minimize"] = {
  'en': `Minimize`,
  'ru': `Минимизировать`,
  'de': `Minimieren`
};

trans["menu_window__maximize"] = {
  'en': `Maximize`,
  'ru': `Максимизировать`,
  'de': `Maximieren`
};

trans["menu_window__front"] = {
  'en': `Bring All to Front`,
  'ru': `Все на передний план`,
  'de': `Bring All nach vorne`
};

trans["menu_window__show"] = {
  'en': `Show`,
  'ru': `Показать`,
  'de': `Zeigen`
};

trans["menu_help"] = {
  'en': `Help`,
  'ru': `Помощь`,
  'de': `Hilfe`
};

trans["menu_help__submit_bug"] = {
  'en': `Submit Bug`,
  'ru': `Отправить ошибку`,
  'de': `Fehler melden`
};

trans["menu_help__help"] = {
  'en': `Open Help Center`,
  'ru': `Показать инструкцию`,
  'de': `Anleitung anzeigen`
};

trans["menu_help__contact_us"] = {
  'en': `Contact Us`,
  'ru': `Свяжитесь с нами`,
  'de': `Kontaktiere uns`
};

trans["menu_help__about"] = {
  'en': `About Nimbus Web`,
  'ru': `О Nimbus Web`,
  'de': `Über Nimbus Web`
};

trans["menu_help__updates"] = {
  'en': `Check for updates...`,
  'ru': `Проверить обновления...`,
  'de': `Auf Updates prüfen...`
};

trans["menu_file"] = {
  'en': `File`,
  'ru': `Файл`,
  'de': `Datei`
};

trans["menu_file__close"] = {
  'en': `Close Window`,
  'ru': `Заркыть Окно`,
  'de': `Fenster schließen`
};

trans["menu_file__logout"] = {
  'en': `Logout`,
  'ru': `Выйти`,
  'de': `Abmelden`
};

trans["menu_app__about"] = {
  'en': `About Nimbus Note`,
  'ru': `Про Nimbus Note`,
  'de': `Über Nimbus Note`
};

trans["menu_app__about_short"] = {
  'en': `About`,
  'ru': `О программе`,
  'de': `Über`
};

trans["menu_app__preferences"] = {
  'en': `Preferences…`,
  'ru': `Настройки…`,
  'de': `Einstellungen…`
};

trans["menu_app__hide"] = {
  'en': `Hide Nimbus Note`,
  'ru': `Спрятать Nimbus Note`,
  'de': `Verbergen Nimbus Note`
};

trans["menu_app__hideothers"] = {
  'en': `Hide Others`,
  'ru': `Скрыть другие`,
  'de': `Andere verstecken`
};

trans["menu_app__unhide"] = {
  'en': `Show All`,
  'ru': `Показать все`,
  'de': `Zeige alles`
};

trans["menu_app__quit"] = {
  'en': `Quit Nimbus Note`,
  'ru': `Закрыть Nimbus Note`,
  'de': `Schließen Nimbus Note`
};

trans["menu_speech"] = {
  'en': `Speech`,
  'ru': `Голосовое управление`,
  'de': `Rede`
};

trans["menu_speech__startspeaking"] = {
  'en': `Start Speaking`,
  'ru': `Начать разговор`,
  'de': `Beginnen Sie zu sprechen`
};

trans["menu_speech__stopspeaking"] = {
  'en': `Stop Speaking`,
  'ru': `Остановить разговор`,
  'de': `Hör auf zu reden`
};

trans["send_logs__confirm_details"] = {
  'en': `Send logs to Nimbus`,
  'ru': `Отправить логи в Nimbus`,
  'de': `Protokolle an Nimbus senden`
};

trans["send_logs__confirm_message"] = {
  'en': `Send account errors logs to Nimbus team`,
  'ru': `Отправить логи ошибок аккаунта команде Nimbus`,
  'de': `Sende Konto Fehler-Logs zu Nimbus Team`
};

trans["send_logs__confirm_details"] = {
  'en': `Send logs to Nimbus`,
  'ru': `Отправить логи в Nimbus`,
  'de': `Protokolle an Nimbus senden`
};

trans["show_sync_logs__details"] = {
  'en': `Send sync logs in console`,
  'ru': `Отправить логи синхронизации в консоль`,
  'de': `Senden sync logs in der Konsole`
};

trans["send_logs__confirm_btn_submit"] = {
  'en': `Send logs`,
  'ru': `Отправить логи`,
  'de': `Protokolle senden`
};

trans["send_logs__confirm_btn_cancel"] = {
  'en': `Cancel`,
  'ru': `Отменить`,
  'de': `Stornieren`
};

trans["cache_clear__confirm_details"] = {
  'en': `Clean sync data`,
  'ru': `Очистить данные синхронизации`,
  'de': `Säubern Sie die Synchronisierungsdaten`
};

trans["cache_clear__confirm_btn_cancel"] = {
  'en': `Cancel`,
  'ru': `Отменить`,
  'de': `Stornieren`
};

trans["cache_clear__confirm_btn_submit"] = {
  'en': `Delete local database`,
  'ru': `Удалить локальную базу`,
  'de': `Lokale Datenbank löschen`
};

trans["app_refresh__warning_message"] = {
  'en': `Sorry, you can not refresh application while sync is in-progress.`,
  'ru': `К сожалению, вы не можете обновить приложение во время синхронизации.`,
  'de': `Sie können die Anwendung leider nicht aktualisieren, während die Synchronisierung ausgeführt wird.`
};

trans["app_refresh__warning_details"] = {
  'en': `Please, wait until sync process complete`,
  'ru': `Пожалуйста, дождитесь завершения процесса синхронизации`,
  'de': `Bitte warten Sie, bis der Synchronisierungsvorgang abgeschlossen ist`
};

trans["app_refresh__confirm_btn"] = {
  'en': `Close`,
  'ru': `Закрыть`,
  'de': `Schließen`
};

trans["notice_sync_is_not_finished"] = {
  'en': `Attachment has not finished downloading. Please wait until sync is complete.`,
  'ru': `Приложение не закончило синхронизацию. Подождите, пока синхронизация завершится.`,
  'de': `Der Anhang wurde nicht heruntergeladen. Bitte warten Sie, bis die Synchronisierung abgeschlossen ist.`
};

trans["dock_menu__window_show"] = {
  'en': `Show Nimbus Note`,
  'ru': `Показать Nimbus Note`,
  'de': `Zeigen Nimbus Note`
};

trans["toast__workspace__remove__offline"] = {
  'en': `The workspace can not be removed in offline mode. Please, connect to the Internet`,
  'ru': `Нет возможности удалить воркспейс в оффлайн режиме, пожалуйста подключитесь к интернету`,
  'de': `Der Arbeitsbereich kann im Offline-Modus nicht entfernt werden. Bitte verbinden Sie sich mit dem Internet`
};

trans["toast__workspace__create__offline"] = {
  'en': `The workspace can not be created in offline mode. Please, connect to the Internet`,
  'ru': `Нет возможности создать воркспейс в оффлайн режиме, пожалуйста подключитесь к интернету`,
  'de': `Der Arbeitsbereich kann nicht im Offline-Modus erstellt werden. Bitte verbinden Sie sich mit dem Internet`
};

trans["toast__workspace__update__offline"] = {
  'en': `The workspace can not be updated in offline mode. Please, connect to the Internet`,
  'ru': `Нет возможности обновить воркспейс в оффлайн режиме, пожалуйста подключитесь к интернету`,
  'de': `Der Arbeitsbereich kann im Offline-Modus nicht aktualisiert werden. Bitte verbinden Sie sich mit dem Internet`
};

trans["toast__workspace__member__update__offline"] = {
  'en': `The workspace members can not be updated in offline mode. Please, connect to the Internet`,
  'ru': `Нет возможности обновить пользователей воркспейса в оффлайн режиме, пожалуйста подключитесь к интернету`,
  'de': `Die Mitglieder des Arbeitsbereichs können im Offline-Modus nicht aktualisiert werden. Bitte verbinden Sie sich mit dem Internet`
};

trans["toast__need_sync_before__share"] = {
  'en': `Please, make Sync before Share`,
  'ru': `Сделайте Синхронизацию перед Шарингом`,
  'de': `Bitte machen Sie Synch vor Freigabe`
};

trans["toast__can_not_share__offline"] = {
  'en': `You can not share/unshare when you are offline`,
  'ru': `Вы не можете сделать/отменить Шаринг, когда вы не в сети`,
  'de': `Sie können nicht freigeben / teilen, wenn Sie offline sind`
};

trans["toast__can_not_share__offline_item"] = {
  'en': `You can not share/unshare offline note or folder`,
  'ru': `Вы не можете сделать Шаринг для оффлайн директории или заметки`,
  'de': `Sie können Offline-Notizen oder Ordner nicht freigeben / deaktivieren`
};

trans["toast__can_not_update_application__offline"] = {
  'en': `Nimbus Note cannot update while offline. Make sure you are connected to the Internet.`,
  'ru': `Nimbus Note не может обновляться в оффлайн режиме. Убедитесь, что вы подключены к Интернету.`,
  'de': `Nimbus Note kann offline nicht aktualisiert werden. Stellen Sie sicher, dass Sie mit dem Internet verbunden sind.`
};

trans["toast__can_not_sync_access_denied"] = {
  'en': `You can not make sync, you don't have permissions to sync changed data`,
  'ru': `Вы не можете выполнить синхронизацию, у вас нет прав на синхронизацию измененных данных`,
  'de': `Sie können nicht synchronisiert machen, Sie haben keine Berechtigungen geänderten Daten synchronisieren`
};

trans["reminder_bottom_text"] = {
  'en': `Reminder`,
  'ru': `Напоминание`,
  'de': `Erinnerung`
};

trans["toast__note_folder_source_workspace_not_exist"] = {
  'en': `Note or folder does not found`,
  'ru': `Заметка или папка не найдена`,
  'de': `Hinweis oder Ordner nicht gefunden`
};

trans["toast__can_not_move_to_workspace__item_offline"] = {
  'en': `You can not move offline note or folder`,
  'ru': `Вы не можете перенести оффлайн директорию или заметку`,
  'de': `Sie können Offline-Notizen oder Ordner nicht verschieben`
};

trans["toast__can_not_move_to_workspace__note_encrypted"] = {
  'en': `You can not move encrypted note`,
  'ru': `Вы не можете перенести зашифрованную заметку`,
  'de': `Sie können keine verschlüsselte Notiz verschieben`
};

trans["toast__can_not_move_to_workspace__item_not_sync"] = {
  'en': `Please, make Sync before move note or folder to workspace`,
  'ru': `Сделайте Синхронизацию перед перемещением заметки или диреткории в другой воркспейс`,
  'de': `Bitte machen Sie die Synchronisierung, bevor Sie die Notiz oder den Ordner in den Arbeitsbereich verschieben`
};

trans["toast__can_not_move_to_workspace__quota_exceed_note_attachment"] = {
  'en': `Note contain files larger than 10mb.`,
  'ru': `Заметка содержит файл(ы) размером больше 10мб.`,
  'de': `Hinweis enthält Dateien, die größer als 10 MB sind.`
};

trans["toast__can_not_move_to_workspace__quota_exceed_folder_attachments"] = {
  'en': `Folder has notes with files larger than 10mb.`,
  'ru': `В папке есть заметки, в которых есть файлы размером больше 10мб.`,
  'de': `Der Ordner enthält Notizen mit Dateien, die größer als 10 MB sind.`
};

trans["toast__can_not_move_to_workspace__quota_exceed_attachments_user"] = {
  'en': `Switch to the Nimbus Pro plan to raise the limit to 1GB.`,
  'ru': `Перейдите на план Nimbus Pro, чтобы поднять лимит до 1GB.`,
  'de': `Wechseln Sie zum Nimbus Pro-Plan, um den Grenzwert auf 1 GB zu erhöhen.`
};

trans["toast__can_not_move_to_workspace__quota_exceed_attachments_owner"] = {
  'en': `The project owner must switch to the Nimbus Pro plan to raise the limit on file size.`,
  'ru': `Владелец проекта должен перейти на план Nimbus Pro, чтобы поднять лимит на размер файлов.`,
  'de': `Der Projektbesitzer muss zum Nimbus Pro-Plan wechseln, um die Dateigröße zu erhöhen.`
};

trans["update_error__title"] = {
  'en': `Problem to check for updates`,
  'ru': `Не удалось проверить обновления`,
  'de': `Probleem om te controleren op updates`
};

trans["update_error__message"] = {
  'en': `Please check your Internet connection, or try again later.`,
  'ru': `Пожалуйста, проверьте подключение к Интернету или повторите попытку позже.`,
  'de': `Bitte überprüfen Sie Ihre Internetverbindung oder versuchen Sie es später erneut.`
};

trans["update_not_available__title"] = {
  'en': `No Updates`,
  'ru': `Обновлений нет`,
  'de': `Keine Aktualisierungen`
};

trans["update_not_available__message"] = {
  'en': `Your current version is up-to-date.`,
  'ru': `Ваша текущая версия актуальна.`,
  'de': `Ihre aktuelle Version ist aktuell.`
};

trans["update_downloaded__title"] = {
  'en': `Install Updates`,
  'ru': `Установить обновления`,
  'de': `Installiere Updates`
};

trans["update_downloaded__message"] = {
  'en': `Your updates have downloaded. The application will quit to install updates.`,
  'ru': `Ваши обновления загружены. Приложение выйдет для установки обновлений.`,
  'de': `Ihre Updates wurden heruntergeladen. Die Anwendung wird beendet, um Updates zu installieren.`
};

trans["toast__user_name__update__offline"] = {
  'en': `The user First Name and Last Name can not be updated in offline mode. Please, connect to the Internet`,
  'ru': `Нет возможности обновить Имя и Фамилию пользователя в оффлайн режиме, пожалуйста подключитесь к интернету`,
  'de': `Der Vor- und Nachname des Benutzers kann im Offline-Modus nicht aktualisiert werden. Bitte verbinden Sie sich mit dem Internet`
};

trans["toast__user_avatar__update__offline"] = {
  'en': `The user Avatar can not be updated in offline mode. Please, connect to the Internet`,
  'ru': `Нет возможности обновить Аватар пользователя в оффлайн режиме, пожалуйста подключитесь к интернету`,
  'de': `Der Benutzer Avatar kann im Offline-Modus nicht aktualisiert werden. Bitte verbinden Sie sich mit dem Internet`
};

trans["export_notes_dialog"] = {
  'en': `Export note`,
  'ru': `Экспорт заметки`,
  'de': `Export note`
};

trans["export_folder_dialog"] = {
  'en': `Export notes`,
  'ru': `Экспорт заметок`,
  'de': `Export notes`
};

export default trans;