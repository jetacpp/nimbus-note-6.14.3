import chalk from "chalk";
import {app, Menu} from "electron";
import {default as appPage} from "../request/page";
import {default as appWindow} from "../window/instance";
import {default as localCache} from "../sync/cache/LocalCache";
import {default as trans} from "../translations/Translations";
import {default as errorHandler} from "../utilities/errorHandler";
import SelectedWorkspace from "../workspace/SelectedWorkspace";
import {default as AboutPopup} from "../popup/About";
import apiMeLogout from "../request/interceptors/jsonInterceptors/me/apiMeLogout";
import {
  openContactUsLink,
  openHelpLink,
  openSubmitBugLink,
} from "../utilities/helpHandler";
import syncHandler from "../utilities/syncHandler";

export default class ApplicationMenu {
  static add() {
    let template = <any>[
      {
        label: `${trans.get('menu_edit')}`,
        submenu: [
          {
            label: `${trans.get('menu_edit__undo')}`,
            role: 'undo'
          },
          {
            label: `${trans.get('menu_edit__redo')}`,
            role: 'redo'
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_edit__cut')}`,
            role: 'cut'
          },
          {
            label: `${trans.get('menu_edit__copy')}`,
            role: 'copy'
          },
          {
            label: `${trans.get('menu_edit__paste')}`,
            role: 'paste'
          },
          {
            label: `${trans.get('menu_edit__pasteandmatchstyle')}`,
            role: 'pasteandmatchstyle'
          },
          {
            label: `${trans.get('menu_edit__delete')}`,
            role: 'delete'
          },
          {
            label: `${trans.get('menu_edit__selectall')}`,
            role: 'selectall'
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_edit__add_note')}`,
            click() {
              if (appWindow.get()) {
                appWindow.get().webContents.send('event:client:add:note:response');
              }
            },
            accelerator: "Alt+N"
          },
          {
            label: `${trans.get('menu_edit__find_notes')}`,
            click() {
              if (appWindow.get()) {
                appWindow.get().webContents.send('event:client:find:notes:response');
              }
            },
            accelerator: "CommandOrControl+Alt+F"
          }
        ]
      },
      {
        label: `${trans.get('menu_view')}`,
        submenu: [
          {
            role: 'toggledevtools'
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_view__reload')}`,
            click() {
              appPage.reload(true);
            },
            accelerator: "CommandOrControl+Alt+R"
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_view__togglefullscreen')}`,
            role: 'togglefullscreen'
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_view__resetzoom')}`,
            role: 'resetzoom'
          },
          {
            label: `${trans.get('menu_view__zoomin')}`,
            role: 'zoomin'
          },
          {
            label: `${trans.get('menu_view__zoomout')}`,
            role: 'zoomout'
          }
        ]
      },
      {
        label: `${trans.get('menu_account')}`,
        submenu: [
          {
            label: `${trans.get('menu_account__traffic_info')}`,
            accelerator: "CommandOrControl+Alt+P",
            click() {
              if (appWindow.get()) {
                appWindow.get().webContents.send('event:client:traffic:info:response');
              }
            }
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_account__start_sync')}`,
            accelerator: "CommandOrControl+S",
            async click() {
              const workspaceId = await SelectedWorkspace.getGlobalId();
              if (appWindow.get()) {
                appWindow.get().webContents.send('event:client:autosync:response', {workspaceId});
              }
            }
          },
          {
            label: `${trans.get('cache_clear__confirm_details')}`,
            accelerator: "CommandOrControl+Alt+T",
            click() {
              localCache.requestClear();
            }
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('show_sync_logs__details')}`,
            accelerator: "CommandOrControl+Alt+K",
            click() {
              syncHandler.showLogs();
            }
          },
          {
            label: `${trans.get('send_logs__confirm_details')}`,
            accelerator: "CommandOrControl+Alt+L",
            click() {
              errorHandler.requestSendSessionLogs();
            }
          }
        ]
      },
      {
        label: `${trans.get('menu_window')}`,
        role: 'window',
        submenu: [
          {
            label: `${trans.get('menu_window__minimize')}`,
            role: 'minimize'
          }
        ]
      },
      {
        label: `${trans.get('menu_help')}`,
        role: 'help',
        submenu: [
          {
            label: `${trans.get('menu_help__submit_bug')}`,
            click() {
              openSubmitBugLink();
            }
          },
          {
            label: `${trans.get('menu_help__help')}`,
            click() {
              openHelpLink();
            }
          },
          {
            label: `${trans.get('menu_help__contact_us')}`,
            click() {
              openContactUsLink();
            }
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_help__about')}`,
            click() {
              require('electron').shell.openExternal('http://nimbusweb.co')
            }
          }
        ]
      }
    ];

    if (process.platform === 'darwin') { // OSX App menu
      template.unshift({
        label: `${trans.get('menu_file')}`,
        submenu: [
          {
            label: `${trans.get('menu_account__export')}`,
            click() {
              if (appWindow.get()) {
                appWindow.get().webContents.send('event:client:export:workspace:notes:response');
              }
            }
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_file__logout')}`,
            click() {
              if (appWindow.get()) {
                apiMeLogout({}, () => {});
              }
            }
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_file__close')}`,
            role: 'close'
          }
        ]
      });
    } else {
      template.unshift({
        label: `${trans.get('menu_file')}`,
        submenu: [
          {
            label: `${trans.get('menu_account__export')}`,
            click() {
              if (appWindow.get()) {
                appWindow.get().webContents.send('event:client:export:workspace:notes:response');
              }
            }
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_app__preferences')}`,
            click() {
              if (appWindow.get()) {
                appWindow.get().webContents.send('event:client:display:settings:response');
              }
            }
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_file__logout')}`,
            click() {
              if (appWindow.get()) {
                appWindow.get().webContents.send('event:logout:request');
              }
            }
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_file__close')}`,
            role: 'close'
          },
          {
            label: `${trans.get('menu_app__quit')}`,
            role: 'quit',
            accelerator: "CommandOrControl+Q",
          }
        ]
      });
    }

    if (process.platform === 'darwin') { // OSX App menu
      template.unshift({
        label: 'Nimbus Note',
        //@ts-ignore
        submenu: [
          {
            label: `${trans.get('menu_app__about')}`,
            role: 'about'
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_app__preferences')}`,
            click() {
              if (appWindow.get()) {
                appWindow.get().webContents.send('event:client:display:settings:response');
              }
            }
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_app__hide')}`,
            role: 'hide'
          },
          {
            label: `${trans.get('menu_app__hideothers')}`,
            role: 'hideothers'
          },
          {
            label: `${trans.get('menu_app__unhide')}`,
            role: 'unhide'
          },
          {
            type: 'separator'
          },
          {
            label: `${trans.get('menu_app__quit')}`,
            role: 'quit'
          }
        ]
      });
    }

    if (process.platform === 'darwin') { // OSX App menu
      if (typeof (template[5]) !== "undefined") {
        template[5].submenu.push({label: `${trans.get('menu_window__maximize')}`, role: 'zoom'});
        // @ts-ignore
        template[5].submenu.push({type: 'separator'});
        // @ts-ignore
        template[5].submenu.push({label: `${trans.get('menu_window__front')}`, role: 'front'});
        // @ts-ignore
        template[5].submenu.push({type: 'separator'});
        // @ts-ignore
        template[5].submenu.push({
          label: `${trans.get('menu_window__show')}`,
          click() {
            if (appWindow.get()) {
              if (!appWindow.get().isVisible()) {
                appWindow.get().show();
              }
            }
          }
        });
      }
    } else {
      template[4].submenu.push({
        label: `${trans.get('menu_window__maximize')}`,
        click() {
          if (appWindow.get()) {
            appWindow.get().maximize();
          }
        }
      });
    }

    if (process.platform === 'darwin') { // OSX App menu
      if (typeof (template[5]) !== "undefined") {
        // @ts-ignore
        template[5].submenu.push({type: 'separator'});
        // @ts-ignore
        template[5].submenu.push(
          {
            label: `${trans.get('menu_speech')}`,
            submenu: [
              {
                label: `${trans.get('menu_speech__startspeaking')}`,
                role: 'startspeaking'
              },
              {
                label: `${trans.get('menu_speech__stopspeaking')}`,
                role: 'stopspeaking'
              }
            ]
          }
        );
      }

      const dockTemplate = [
        {
          label: `${trans.get('dock_menu__window_show')}`,
          click() {
            if (appWindow.get()) {
              if (!appWindow.get().isVisible()) {
                appWindow.get().show();
              }
            }
          }
        }
      ];

      const dockMenu = Menu.buildFromTemplate(dockTemplate);
      app.dock.setMenu(dockMenu)
    } else {
      template[5].submenu.push({
        label: `${trans.get('menu_help__updates')}`,
        click(item) {
          const {menuCheck} = require('./../utilities/autoUpdater');
          menuCheck(item);
        }
      });
      template[5].submenu.push({
        label: `${trans.get('menu_app__about_short')}`,
        click() {
          AboutPopup.show();
        }
      });
    }

    // @ts-ignore
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);
  }

  static update() {
    if (appWindow.get()) {
      ApplicationMenu.add();
    }
  }

  static toggleAppWindow() {
    if (appWindow.get()) {
      if(appWindow.get().isMinimized()) {
        appWindow.get().restore();
      }

      if (!appWindow.get().isVisible()) {
        appWindow.get().show();
      }
    }
  }

  static getTrayMenu() {
    return Menu.buildFromTemplate([
      {
        label: `${trans.get('dock_menu__window_show')}`,
        click: function() {
          ApplicationMenu.toggleAppWindow();
        }
      },
      {
        label: `${trans.get('menu_edit__add_note')}`,
        click() {
          if (appWindow.get()) {
            appWindow.get().webContents.send('event:client:add:note:response');
            ApplicationMenu.toggleAppWindow();
          }
        },
        accelerator: "Alt+N"
      },
      {
        label: `${trans.get('menu_file__logout')}`,
        click() {
          if (appWindow.get()) {
            appWindow.get().webContents.send('event:logout:request');
            ApplicationMenu.toggleAppWindow();
          }
        }
      },
      {
        label: `${trans.get('menu_app__quit')}`,
        role: 'quit',
      }
    ]);
  }

  static getContextMenu() {
    const menu = []

    menu.push({
      // @ts-ignore
      role: "cut",
      label: `${trans.get('menu_edit__cut')}`,
    })
    menu.push({
      // @ts-ignore
      role: "copy",
      label: `${trans.get('menu_edit__copy')}`,
    })
    menu.push({
      // @ts-ignore
      role: "paste",
      label: `${trans.get('menu_edit__paste')}`,
    })
    menu.push({
      // @ts-ignore
      type: 'separator'
    })
    menu.push({
      // @ts-ignore
      role: "pasteAndMatchStyle",
      label: `${trans.get('menu_edit__pasteandmatchstyle')}`,
    })
    menu.push({
      // @ts-ignore
      role: "delete",
      label: `${trans.get('menu_edit__delete')}`,
    })
    menu.push({
      // @ts-ignore
      role: "selectAll",
      label: `${trans.get('menu_edit__selectall')}`,
    })

    return menu
  }
}
