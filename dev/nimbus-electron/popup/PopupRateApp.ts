import chalk from "chalk";
import {default as config} from "../../config.runtime";
import {default as appWindow} from "../window/instance";
import {default as settings} from "../db/models/settings";
import {default as auth} from "../auth/auth";

export default class PopupRateApp {
  static POPUP_SHOW_TIME = 345600000;

  static APP_INSTALL_DATE = "appInstallDate";
  static APP_RATE_POPUP_SHOWN = "appRatePopupShown";

  static show() {
    if (appWindow.get()) {
      if (process.platform === 'darwin') {
        appWindow.get().webContents.send('event:client:popup:rate:app:show:response', {
          problemUrl: "https://nimbusweb.me/contact-us.php",
          storeUrl: `itms-apps://itunes.apple.com/app/id${config.APPSTORE_APP_ID}`,
          modal: true
        });
      }
    }
  }

  static check() {
    auth.getUser((err, authInfo) => {
      if (authInfo && Object.keys(authInfo).length) {
        settings.get(PopupRateApp.APP_RATE_POPUP_SHOWN, (err, appRatePopupShown) => {
          if (typeof (appRatePopupShown) !== "object" && appRatePopupShown) {
            return;
          }

          /**
           * @param {number} timestamp
           */
          let setInstallTimestamp = async (timestamp) => {
            // @ts-ignore
            return new Promise(async (resolve) => {
              settings.set(PopupRateApp.APP_INSTALL_DATE, timestamp, () => {
                return resolve(timestamp);
              });
            });
          };

          // @ts-ignore
          settings.get(PopupRateApp.APP_INSTALL_DATE, async (err, appInstallTimestamp) => {
            const currentTimestamp = new Date().getTime();

            if (typeof (appInstallTimestamp) === "object" && Object.keys(appInstallTimestamp).length === 0) {
              appInstallTimestamp = currentTimestamp;
              await setInstallTimestamp(appInstallTimestamp);
            }

            const timeDiff = currentTimestamp - appInstallTimestamp;
            if (timeDiff < PopupRateApp.POPUP_SHOW_TIME) {
              return;
            }

            settings.set(PopupRateApp.APP_RATE_POPUP_SHOWN, true, () => {
              PopupRateApp.show();
            });
          });
        });
      }
    });
  }
}
