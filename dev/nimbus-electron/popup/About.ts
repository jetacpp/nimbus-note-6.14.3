import {app, dialog} from 'electron';
import {default as trans} from "../translations/Translations";
import {default as config} from "../../config.runtime";

export default class About {
  static show() {
    const appVersion = app.getVersion();
    const copyRight = `© ${new Date().getFullYear()} NIMBUS WEB Inc.\nAll Rights Reserved.`;
    dialog.showMessageBox({
      type: 'info',
      title: trans.get('menu_app__about'),
      message: `Version ${appVersion} (${appVersion})\n\n${copyRight}`,
      icon: config.applicationIconPath,
    });
  }
}
