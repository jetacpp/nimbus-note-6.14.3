import chalk from "chalk";
import {default as item} from "../db/models/item";
import {default as workspace} from "../db/models/workspace";
import {default as date} from "../utilities/dateHandler";
import {default as appWindowInstance} from "../window/instance";
import {default as trans} from "../translations/Translations";
import {default as config} from "../../config";

let reminders;

export default class ReminderNotice {
  static TIMER_MAX_VALUE = 2147483647;

  /**
   * @param {[WorkspaceObj]} workspaces
   */
  static init(workspaces) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (reminders) {
        return resolve(false);
      }

      if (!reminders) {
        reminders = {};
      }

      for (let workspaceInstance of workspaces) {
        const workspaceId = await workspace.getLocalId(workspaceInstance.workspaceId);
        const workspaceTitle = workspaceInstance.title;
        const notesList = <[any]>await item.getNotesWithReminders(workspaceId);
        for (let noteInstance of notesList) {
          if (typeof (reminders[noteInstance.globalId]) !== 'undefined') {
            continue;
          }

          const reminderInstance = noteInstance.reminder;
          if (reminderInstance) {
            const reminderData = {
              workspaceId: workspaceInstance.globalId,
              workspaceTitle,
              globalId: noteInstance.globalId,
              parentId: noteInstance.parentId,
              title: noteInstance.title,
              reminder: reminderInstance
            };
            ReminderNotice.setTimeoutByGlobalId(reminderData);
          }
        }
      }

      return resolve(true);
    });
  }

  /**
   * @param {{globalId:string, parentId:string, reminder:ReminderObj}} reminderData
   */
  static setTimeoutByGlobalId(reminderData) {
    const {globalId} = reminderData;
    ReminderNotice.clearIntervalByGlobalId(globalId);
    ReminderNotice.clearTimeoutByGlobalId(globalId);
    ReminderNotice.clearByGlobalId(globalId);

    if (typeof reminders[globalId] === 'undefined') {
      reminders[globalId] = {
        'timeout': null,
        'interval': null
      }
    }

    if (!reminderData.reminder.date) {
      return;
    }

    const reminderTimeout = reminderData.reminder.date - date.now();
    // const reminderTimeout = 10;
    let reminderTimeoutDiff;
    if (reminderTimeout >= 0) {
      reminderTimeoutDiff = reminderTimeout * 1000;
    } else { // reminder set as past datetime
      if(!reminderData.reminder.interval) {
        return;
      }
      const pastTimeout = date.now() - reminderData.reminder.date;
      const missedIntervals = Math.floor(pastTimeout / reminderData.reminder.interval);
      if(missedIntervals >= 1) {
        reminderTimeoutDiff = (pastTimeout - (missedIntervals * reminderData.reminder.interval)) * 1000;
      } else {
        reminderTimeoutDiff = (pastTimeout - reminderData.reminder.interval) * 1000;
      }
      reminderTimeoutDiff = (reminderData.reminder.interval * 1000) - reminderTimeoutDiff;
    }


    if (reminderTimeoutDiff > ReminderNotice.TIMER_MAX_VALUE) {
      return;
    }

    const timeoutInstance = setTimeout(async function () {
      if (!this.reminderData) {
        return;
      }

      const noteExist = await ReminderNotice.checkIfNoteExist(this.reminderData);
      if (!noteExist) {
        return;
      }

      ReminderNotice.clearTimeoutByGlobalId(this.reminderData.globalId);
      if (this.reminderData.reminder.interval) {
        ReminderNotice.setIntervalByGlobalId(this.reminderData);
      }

      ReminderNotice.show(this.reminderData);
    }, reminderTimeoutDiff);
    timeoutInstance['reminderData'] = reminderData;
    reminders[globalId]['timeout'] = timeoutInstance;
    // } else {
    //   if (reminderData.reminder.interval) {
    //     ReminderNotice.setIntervalByGlobalId(reminderData);
    //   }
    // }
  }

  /**
   * @param {{globalId:string, parentId:string, reminder:ReminderObj}} reminderData
   */
  static setIntervalByGlobalId(reminderData) {
    const {globalId} = reminderData;
    ReminderNotice.clearIntervalByGlobalId(globalId);
    if (typeof reminders[globalId] === 'undefined') {
      reminders[globalId] = {
        'timeout': null,
        'interval': null
      }
    }
    const reminderInterval = reminderData.reminder.interval * 1000;
    if (reminderInterval > ReminderNotice.TIMER_MAX_VALUE) {
      return;
    }

    const intervalInstance = setInterval(async function () {
      const noteExist = await ReminderNotice.checkIfNoteExist(this.reminderData);
      if (!noteExist) {
        return;
      }

      ReminderNotice.show(this.reminderData);
    }, reminderInterval);
    intervalInstance['reminderData'] = reminderData;
    reminders[globalId]['interval'] = intervalInstance;
  }

  /**
   * @param {string} globalId
   */
  static clearTimeoutByGlobalId(globalId) {
    if (typeof reminders[globalId] !== 'undefined' && reminders[globalId]) {
      if (reminders[globalId].timeout) {
        clearTimeout(reminders[globalId].timeout);
      }
      reminders[globalId].timeout = null;
    }
  }

  /**
   * @param {string} globalId
   */
  static clearIntervalByGlobalId(globalId) {
    if (typeof reminders[globalId] !== 'undefined' && reminders[globalId]) {
      if (reminders[globalId].interval) {
        clearInterval(reminders[globalId].interval);
      }

      if (reminders[globalId].timeout) {
        clearTimeout(reminders[globalId].timeout);
      }

      reminders[globalId].timeout = null;
      reminders[globalId].reminder = null;
    }
  }

  /**
   * @param {string} globalId
   */
  static clearByGlobalId(globalId) {
    if (typeof reminders[globalId] !== 'undefined' && reminders[globalId]) {
      delete reminders[globalId];
    }
  }

  /**
   * @param {string} globalId
   */
  static flushByGlobalId(globalId) {
    ReminderNotice.clearTimeoutByGlobalId(globalId);
    ReminderNotice.clearIntervalByGlobalId(globalId);
    ReminderNotice.clearByGlobalId(globalId);
  }

  static flushAll() {
    if (reminders) {
      if (Object.keys(reminders).length) {
        for (let globalId in reminders) {
          if (reminders.hasOwnProperty(globalId)) {
            ReminderNotice.flushByGlobalId(globalId);
          }
        }
      }
      reminders = null;
    }
  }

  /**
   * @param {{globalId:string, parentId:string, reminder:ReminderObj}} reminderData
   */
  static show(reminderData) {
    if (!reminderData) {
      return;
    }

    reminderData.noteUrl = `${config.LOCAL_SERVER_HTTP_HOST}/ws/${reminderData.workspaceId}/folder/${reminderData.parentId}/note/${reminderData.globalId}`;
    if (appWindowInstance.get()) {
      let notificationData = <any>{
        title: reminderData.workspaceTitle.length > 100 ? `${reminderData.workspaceTitle.substring(0, 50)}...` : reminderData.workspaceTitle,
        body: {
          subtitle: reminderData.title.length > 100 ? `${reminderData.title.substring(0, 50)}...` : reminderData.title,
          body: trans.get('reminder_bottom_text'),
          reminderData: reminderData,
          bundleId: config.APPBUNDLE_ID,
          showCloseButton: true,
          canReply: false,
          soundName: 'default'
        }
      };
      if (process.platform !== 'darwin') {
        notificationData.body.icon = config.aboutIconPath;
      }
      appWindowInstance.get().webContents.send('event:client:send:notification:response', notificationData);
    }
  }

  /**
   * @param {{globalId:string, parentId:string, reminder:ReminderObj}} reminderData
   */
  static async checkIfNoteExist(reminderData) {
    const note = await ReminderNotice.getNoteForReminder(reminderData);
    if (note) {
      return note;
    }

    ReminderNotice.clearTimeoutByGlobalId(reminderData.globalId);
    ReminderNotice.clearIntervalByGlobalId(reminderData.globalId);
    return null;
  }

  /**
   * @param {{globalId:string, workspaceId:string}}
   */
  static getNoteForReminder({workspaceId, globalId}) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (typeof (workspaceId) === 'undefined') {
        return resolve(null);
      }

      if (!globalId) {
        return resolve(null);
      }

      item.find({
        type: 'note',
        globalId,
        rootId: {'$ne': 'trash'}
      }, {
        workspaceId: workspaceId ? await workspace.getLocalId(workspaceId) : null
      }, (err, noteInstance) => {
        if (err || !noteInstance) {
          return resolve(null);
        }
        return resolve(noteInstance);
      });
    });
  }
}