import {app, dialog} from 'electron';
import {default as trans} from "../translations/Translations";
import {default as appWindow} from "../window/instance";

export default class AppRefresh {
  static show() {
    if (appWindow.get()) {
      dialog.showMessageBox(appWindow.get(), {
        type: "warning",
        title: app.name,
        message: `${trans.get('app_refresh__warning_message')}`,
        detail: `${trans.get('app_refresh__warning_details')}`,
        buttons: [
          `${trans.get('app_refresh__confirm_btn')}`
        ],
        defaultId: 0,
        cancelId: 0
      });
    }
  }
}
