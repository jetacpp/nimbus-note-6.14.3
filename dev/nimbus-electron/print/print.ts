import chalk from "chalk";
import fs = require('fs-extra');
import {BrowserWindow, ipcMain} from "electron";
import {default as config} from "../../config.runtime";
import {default as appWindowInstance} from "../window/instance";

let printWin;

export default class Print {
  static listenPrint() {
    ipcMain.on('event:print', (event, arg) => {
      if (printWin) {
        printWin.close();
      }

      if (arg && arg.template) {
        let tempFilePath = `${config.clientDataPath}/print.html`;
        try {
          fs.writeFile(tempFilePath, arg.template, (err) => {
            if (!err) {
              let title = arg.title || "Nimbus Note";
              let width = arg.width || 600;
              let height = arg.height || 600;
              printWin = new BrowserWindow({
                show: false,
                width: width,
                height: height,
                title: title,
                parent: appWindowInstance.get() || null
              });

              printWin.webContents.on('did-finish-load', () => {
                //printWin.webContents.print({silent: false, printBackground: false, alwaysOnTop: true});
                printWin.webContents.executeJavaScript("window.print()");
                setTimeout(() => {
                  try {
                    fs.writeFile(tempFilePath, '', function (err) {
                      printWin = null;
                    });
                  } catch (e) {
                    if (config.SHOW_WEB_CONSOLE) {
                      console.log("Problem: print => clear print file => did-finish-load", e);
                    }
                  }
                }, 1000);
              });

              printWin.once('close', function () {
                try {
                  fs.writeFile(tempFilePath, '', function (err) {
                    printWin = null;
                  });
                } catch (e) {
                  if (config.SHOW_WEB_CONSOLE) {
                    console.log("Problem: print => clear print file => close", e);
                  }
                }
              });

              printWin.loadURL("file://" + tempFilePath);
            } else {
              if (config.SHOW_WEB_CONSOLE) {
                console.log(err);
              }
            }
          });
        } catch (e) {
          if (config.SHOW_WEB_CONSOLE) {
            console.log("Problem: print => to save print file", e);
          }
        }
      }
    });
  }
}
