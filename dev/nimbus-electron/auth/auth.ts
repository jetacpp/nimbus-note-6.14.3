import chalk from "chalk";
import md5 from "js-md5";
import {default as config} from "../../config";
import {default as userModel, default as user} from "../db/models/user";
import {default as settings} from "../db/models/settings";
import {default as date} from "../utilities/dateHandler";
import {default as error} from "../utilities/customError";

let loggedUserData;
let loggedUserInfo;

export default class Auth {
  static errorCode = {
    ERROR_NONE: 0,
    ERROR_WRONG_AUTH_INPUT: 1,
    ERROR_USER_NOT_FOUND: 2,
    ERROR_USER_ALREADY_EXIST: -4,
    ERROR_USER_REGISTER: 4,
    ERROR_USER_LOGIN: 5,
    ERROR_USER_OFFLINE: 6
  };

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static login(data, options = {oauth: false}, callback = (err, res) => {
  }) {
    if (!data) {
      return callback(error.wrongInput(), {errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT});
    }

    if (!data.login) {
      return callback(error.wrongInput(), {errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT});
    }

    if(!options.oauth && !data.password) {
      return callback(error.wrongInput(), {errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT});
    }

    let findQuery = {
      login: data.login
    };

    user.find(findQuery, {secure: true}, (err, doc) => {
      if (!doc) {
        return callback(error.itemNotFound(), {errorCode: Auth.errorCode.ERROR_USER_NOT_FOUND});
      }

      if(!options.oauth) {
        if (!Auth.confirmPassword(data, doc)) {
          return callback(error.noAuth(), {errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT});
        }
      }

      let updateFields = {
        'localSession': Auth.cryptSessionToken(doc.email, doc._id)
      };
      updateFields = Auth.prepareSyncData(updateFields, data);

      findQuery = {
        // @ts-ignore
        userId: doc.userId
      };

      user.update(findQuery, updateFields, {}, (err, count) => {
        if (!count) {
          return callback(error.itemSaveError(), {errorCode: Auth.errorCode.ERROR_USER_NOT_FOUND});
        }

        settings.set(config.SETTINGS_APP_AUTH_NAME, updateFields, (err, list) => {
          if (!list) {
            return callback(error.itemSaveError(), {errorCode: Auth.errorCode.ERROR_USER_LOGIN});
          }

          loggedUserData = user.getPublicData(doc);
          loggedUserInfo = user.getPublicInfo(doc);
          return callback(null, {errorCode: Auth.errorCode.ERROR_NONE, data: loggedUserData});
        });
      });
    });
  }

  /**
   * @param {{}} data
   * @param {Function} callback
   */
  static register(data, callback = (err, res) => {
  }) {
    if (!data) {
      return callback(error.wrongInput(), {errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT});
    }

    if (!data.login || !data.password) {
      return callback(error.wrongInput(), {errorCode: Auth.errorCode.ERROR_WRONG_AUTH_INPUT});
    }

    let findQuery = {
      login: data.login
    };
    user.find(findQuery, {}, (err, doc) => {
      if (doc && Object.keys(doc).length) {
        return callback(error.wrongInput(), {errorCode: Auth.errorCode.ERROR_USER_ALREADY_EXIST});
      }

      user.create(data, {secure: true}, (err, doc) => {
        if (!doc) {
          return callback(error.itemSaveError(), {errorCode: Auth.errorCode.ERROR_USER_REGISTER});
        }

        let updateFields = {
          'id': doc._id,
          'password': Auth.cryptPassword(doc.password, doc._id),
          'localSession': Auth.cryptSessionToken(doc.email, doc._id),
          'username': user.prepareName(doc.email, doc._id)
        };
        updateFields = Auth.prepareSyncData(updateFields, data);

        findQuery = {
          // @ts-ignore
          userId: doc.userId
        };
        user.update(findQuery, updateFields, {}, (err, count) => {
          if (!count) {
            return callback(error.itemSaveError(), {errorCode: Auth.errorCode.ERROR_USER_NOT_FOUND});
          }

          loggedUserData = user.getPublicData(doc);
          loggedUserData.username = updateFields.username;

          let saveFields = {'localSession': updateFields.localSession};
          settings.set(config.SETTINGS_APP_AUTH_NAME, saveFields, (err, list) => {
            if (!list) {
              return callback(error.itemSaveError(), {errorCode: Auth.errorCode.ERROR_USER_REGISTER});
            }

            return callback(null, {errorCode: Auth.errorCode.ERROR_NONE, data: loggedUserData});
          });
        });
      });
    });
  }

  /**
   * @param {{}} data
   */
  static async updateUserPassword(data) {
    // @ts-ignore
    return new Promise((resolve) => {
      if (!(data.login && data.password)) {
        return resolve(0);
      }

      let findQuery = {
        login: data.login
      };
      user.find(findQuery, {}, (err, userItem) => {
        if (err || !userItem) {
          return resolve(0);
        }

        if (!Object.keys(userItem).length) {
          return resolve(0);
        }

        let passwordHash = Auth.cryptPassword(data.password, userItem._id);
        user.update({'userId': userItem.userId}, {'password': passwordHash}, {}, (err, count) => {
          if (!count) {
            error.itemNotFound("user not found for updateUserPassword hash after successful login");
            return resolve(0);
          }

          resolve(1);
        });
      });
    });
  }

  /**
   * @param {Function} callback
   */
  static authorized(callback = (err, res) => {
  }) {
    settings.get(config.SETTINGS_APP_AUTH_NAME, (err, data) => {
      if (!data) {
        return callback(null, null);
      }

      if (!data.localSession) {
        return callback(null, null);
      }

      let findQuery = {localSession: data.localSession};
      user.find(findQuery, {useLocalSession: true}, (err, doc) => {
        if (!doc || (doc && !Object.keys(doc).length)) {
          return callback(null, null);
        }

        loggedUserData = user.getPublicData(doc);
        return callback(null, loggedUserData);
      })
    });
  }

  /**
   * @param {Function} callback
   */
  static logout(callback = (err, res) => {
  }) {
    settings.remove(config.SETTINGS_APP_AUTH_NAME, (err, status) => {
      return callback(err, status);
    });
  }

  /**
   * @param {{}} data
   * @param {{}} doc
   * @return {boolean}
   */
  static confirmPassword(data, doc) {
    let inputCorrectFormat = data && doc && data.password && doc.password;
    return inputCorrectFormat && Auth.cryptPassword(data.password, doc._id) === doc.password;
  }

  /**
   * @param {string} password
   * @param {string} salt
   */
  static cryptPassword(password, salt) {
    return md5(salt + password);
  }

  /**
   * @param {string} string
   * @param {string} salt
   */
  static cryptSessionToken(string, salt) {
    // @ts-ignore
    return md5(salt + string + date.now() + Math.random());
  }

  /**
   * @param {Function} callback
   */
  static getUser(callback = (err, res) => {
  }) {
    // if (loggedUserData && Object.keys(loggedUserData).length) {
    //   return callback(null, loggedUserData);
    // }
    //
    // if (loggedUserData) {
    //   return callback(null, loggedUserData);
    // } else {
      Auth.authorized((err, loggedUserData) => {
        return callback(err, loggedUserData);
      });
    // }
  }

  static async getUserAsync() {
    // @ts-ignore
    return new Promise(async (resolve) => {
      Auth.getUser((err, userInfo) => {
        return resolve(userInfo);
      });
    });
  }

  static fetchActualUserAsync() {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const authInfo = <{email}>await Auth.getUserAsync();
      if(!authInfo) {
        return resolve(null);
      }

      const {email} = authInfo;
      if(!email) {
        return resolve(null);
      }

      userModel.find({email}, {}, async (err, itemInstance) => {
        if (err || !itemInstance) {
          return resolve(null);
        }
        return resolve(itemInstance);
      });
    });
  }

  /**
   * @param {{}} frmData
   * @param {{}} response
   * @return {{}}
   */
  static prepareFormSyncData(frmData, response) {
    /*if (typeof (response.firstname) !== 'undefined') {
      frmData.firstname = response.firstname;
    }

    if (typeof (response.lastname) !== 'undefined') {
      frmData.lastname = response.lastname;
    }

    if (typeof (response.avatar) !== 'undefined') {
      frmData.avatar = response.avatar;
    }*/

    if (response.notes_email) {
      frmData.notesEmail = response.notes_email;
    }

    if (response.premium) {
      frmData.subscribe = response.premium.active ? 1 : 0;
      frmData.paymentStartDate = response.premium.start_date;
      frmData.paymentEndDate = response.premium.end_date;
      frmData.paymentSystemCode = response.premium.source;
    }

    if (response.usage && response.usage.notes) {
      frmData.usageCurrent = response.usage.notes.current;
      frmData.usageMax = response.usage.notes.max;
    }

    if (typeof (response.days_to_quota_reset) !== "undefined") {
      frmData.dateNextQuotaReset = user.getDateNextQuotaReset(response.days_to_quota_reset);
    }

    return frmData;
  }

  /**
   * @param {{}} updateFields
   * @param {{}} data
   * @return {{}}
   */
  static prepareSyncData(updateFields, data) {
    /*if (data.firstname) {
      updateFields.firstname = data.firstname;
    }

    if (data.lastname) {
      updateFields.lastname = data.lastname;
    }

    if (data.avatar) {
      updateFields.avatar = data.avatar;
    }*/

    if (data.notesEmail) {
      updateFields.notesEmail = data.notesEmail;
    }

    if (typeof (data.subscribe) !== "undefined") {
      updateFields.subscribe = data.subscribe;
    }

    if (data.paymentStartDate) {
      updateFields.paymentStartDate = data.paymentStartDate;
    }

    if (data.paymentEndDate) {
      updateFields.paymentEndDate = data.paymentEndDate;
    }

    if (data.paymentSystemCode) {
      updateFields.paymentSystemCode = data.paymentSystemCode;
    }

    if (data.usageCurrent) {
      updateFields.usageCurrent = data.usageCurrent;
    }

    if (data.usageMax) {
      updateFields.usageMax = data.usageMax;
    }

    if (typeof (data.dateNextQuotaReset) !== "undefined") {
      updateFields.dateNextQuotaReset = data.dateNextQuotaReset;
    }

    if (typeof (data.authProvider) !== "undefined") {
      updateFields.authProvider = data.authProvider;
    }

    return updateFields;
  }
}
