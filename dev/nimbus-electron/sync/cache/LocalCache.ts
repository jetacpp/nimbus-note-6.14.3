import chalk from "chalk";
import fs = require('fs-extra');
import {app, dialog} from 'electron';
import {default as pdb} from "../../../pdb";
import {default as appPage} from "../../request/page";
import {default as appWindow} from "../../window/instance";
import {default as SyncStatusChangedEvent} from "../process/events/SyncStatusChangedEvent";
import {default as trans} from "../../translations/Translations";
import {default as NimbusSDK} from "../nimbussdk/net/NimbusSDK";
import SelectedWorkspace from "../../workspace/SelectedWorkspace";
import {default as appOnlineState} from "../../online/state";
import {default as config} from "../../../config";
import {default as workspace} from "../../db/models/workspace";
import {default as errorHandler} from "../../utilities/errorHandler";
import {default as orgs} from "../../db/models/orgs";
import {clearDirectory} from "../../utilities/clearHandler";
import {clearExportDir} from "../../utilities/exportHandler";

export default class LocalCache {
  static requestClear() {
    const dialogOptions = {
      type: "warning",
      title: app.name,
      message: `${trans.get('cache_clear__confirm_message')}`,
      detail: `${trans.get('cache_clear__confirm_details')}`,
      buttons: [
        `${trans.get('cache_clear__confirm_btn_cancel')}`,
        `${trans.get('cache_clear__confirm_btn_submit')}`
      ],
      defaultId: 1,
      cancelId: 0
    };

    if (appWindow.get()) {
      dialog.showMessageBox(appWindow.get(), dialogOptions).then(async ({response}) => {
        if (response) {
          const availableWorkspaceList = <[string]>await workspace.getAvailableIdList();
          for (let workspaceId of availableWorkspaceList) {
            await SyncStatusChangedEvent.setPauseStatus({workspaceId});
          }

          let attachmentPath = pdb.getClientAttachmentPath();
          if (attachmentPath) {
            await clearDirectory(attachmentPath);
          }

          let tempAttachmentPath = pdb.getClientTempAttachmentPath();
          if (tempAttachmentPath) {
            clearDirectory(tempAttachmentPath);
          }

          let exportPath = pdb.getClientExportPath();
          if (exportPath) {
            clearExportDir(exportPath);
          }

          let dbPath = pdb.getClientDbPath();
          if (dbPath) {
            await errorHandler.clearSessionLogsStream();

            pdb.clearDbConnections();
            await LocalCache.clearWorkspaces(dbPath);

            // await SelectedWorkspace.set(null);
            // SelectedWorkspace.setGlobalId(null);

            if (NimbusSDK.getAccountManager()) {

              const completeClearCache = () => {
                const jsCode = `
                  if(window.localStorage) {
                    for (let i = 0, len = window.localStorage.length; i < len; ++i) { 
                      let key = window.localStorage.key(i);
                      if(key && (key.indexOf('nimbus-note-tree') === 0 || key.indexOf('nimbus-note-folders') === 0)) {
                        window.localStorage.removeItem(key);
                      }
                    }
                  }
                `;
                appWindow.get().webContents.executeJavaScript(jsCode);

                appPage.reload();
                setTimeout(async () => {
                  const workspaceId = await SelectedWorkspace.getGlobalId();
                  if (appWindow.get()) {
                    appWindow.get().webContents.send('event:client:autosync:response', {workspaceId});
                  }
                }, 5000);
              };

              if (appOnlineState.get()) {
                await orgs.syncUserOrganizationsWorkspaces();
              }

              completeClearCache();
            }
          }
        }
      });
    }
  }

  /**
   * @param {string} dirPath
   */
  static clearWorkspaces(dirPath) {
    return new Promise(async (resolve) => {
      let points = [];
      try {
        points = await fs.readdir(dirPath);
      } catch (e) {
        return resolve(false);
      }
      if (!points.length) {
        return resolve(false);
      }

      for (let i = 0; i < points.length; i++) {
        let pointPath = dirPath + '/' + points[i].split('/')[0].split('\\')[0];
        try {
          let pointStat = await fs.lstat(pointPath);
          if (pointStat.isFile()) {
            await fs.unlink(pointPath);
          } else if (pointStat.isDirectory()) {
            await clearDirectory(pointPath);
            await fs.rmdir(pointPath);
          }
        } catch (e) {
          if (config.SHOW_WEB_CONSOLE) {
            console.log("Error => LocalCache => clearWorkspaces => remove file or directory: ", pointPath, e);
          }
        }
      }

      return resolve(true);
    });
  }
}
