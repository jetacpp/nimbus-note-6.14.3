let collection = {};

export default class SocketStore {
  /**
   * @param {{workspaceId:string, storeType:string, data: {}}} inputData
   */
  static add(inputData) {
    const {workspaceId, storeType, data} = inputData;

    if (typeof (collection[workspaceId]) === 'undefined') {
      collection[workspaceId] = {};
    }

    if (typeof (collection[workspaceId][storeType]) === 'undefined') {
      collection[workspaceId][storeType] = [];
    }

    collection[workspaceId][storeType].push(data);
  }

  /**
   * @param {{workspaceId:string, storeType:string}} inputData
   * @return {[]}
   */
  static get(inputData) {
    const {workspaceId, storeType} = inputData;

    if (typeof (collection[workspaceId]) === 'undefined') {
      collection[workspaceId] = {};
    }

    if (typeof (collection[workspaceId][storeType]) === 'undefined') {
      return [];
    }

    return collection[workspaceId][storeType];
  }

  /**
   * @param {{workspaceId:string, storeType:string}} inputData
   */
  static clear(inputData) {
    const {workspaceId, storeType} = inputData;

    if (typeof (collection[workspaceId]) === 'undefined') {
      return;
    }

    if (typeof (collection[workspaceId][storeType]) !== 'undefined') {
      collection[workspaceId][storeType] = [];
    }
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static flush(inputData) {
    const {workspaceId} = inputData;

    if (typeof (collection[workspaceId]) === 'undefined') {
      return;
    }

    for (let storeType in collection[workspaceId]) {
      if (collection[workspaceId].hasOwnProperty(storeType)) {
        collection[workspaceId][storeType] = [];
      }
    }
  }

  static flushAll() {
    collection = {};
  }
}
