import chalk from 'chalk'
import {default as socketServer} from "./server";
import {default as socketStore} from "./socketStore";
import {default as socketStoreType} from "./socketStoreType";
import {default as item} from "../../db/models/item";
import {default as todo} from "../../db/models/todo";
import {default as attach} from "../../db/models/attach";
import SelectedWorkspace from "../../workspace/SelectedWorkspace";
import {default as appWindow} from "../../window/instance";
import {default as errorHandler} from "../../utilities/errorHandler";

/**
 * @type {boolean}
 */
let socketStoreSizeOverloadState = {};
/**
 * @type {[]}
 */
let socketAttachmentsCopy = {};

export default class SocketFunctions {
  static SOCKET_STORE_SEND_MESSAGES_COUNT = 200;

  /**
   * @param {{workspaceId:string, globalId:string, parentId:string, type:string}} inputData
   */
  static async sendItemsCountMessage(inputData) {
    let {workspaceId, globalId, parentId, type} = inputData;
    if (await SelectedWorkspace.getGlobalId() !== workspaceId) {
      return;
    }

    if (type === "note") {
      item.count({parentId: parentId, type: type}, {workspaceId}, function (err, notesCount) {
        if (!err) {
          let folderMessage = {
            globalId: parentId,
            childrenNotesCount: notesCount
          };

          socketServer.emit('note:count', {
            'message': folderMessage
          });
        }
      });

      todo.count({noteGlobalId: globalId, "checked": false}, {workspaceId}, function (err, todosCount) {
        let noteMessage = <any>{globalId: globalId};
        if (!err) {
          noteMessage.todosCount = todosCount;
        }
        attach.count({noteGlobalId: globalId, inList: true}, {workspaceId}, function (err, attachCount) {
          if (!err) {
            noteMessage.attachmentsCount = attachCount;
          }

          socketServer.emit('note:count', {
            'message': noteMessage
          });
        });
      });
    }
  }

  /**
   * @param {{workspaceId:string, globalId:string, parentId:string, type:string, reminderRemove:boolean}} inputData
   */
  static async sendItemUpdateMessage(inputData) {
    const selectedWorkspaceId = await SelectedWorkspace.getGlobalId();
    let {workspaceId, globalId, parentId, isCreate, type, reminderRemove} = inputData;
    if (selectedWorkspaceId !== workspaceId) {
      return;
    }

    if (!parentId) {
      parentId = null;
    }
    if (!isCreate) {
      isCreate = false;
    }
    if (!type) {
      type = 'note';
    }

    if (globalId) {
      let message = {
        globalId: globalId
      };

      if (parentId && type) {
        message['parentId'] = parentId;
        message['isCreate'] = isCreate;
        message['type'] = type;
      }

      socketServer.emit('note:update', {
        'message': message
      });

      if (type === 'note') {
        if (reminderRemove) {
          socketServer.emit('noteReminder:remove', {
            'message': {globalId}
          });
        } else {
          socketServer.emit('noteReminder:update', {
            'message': {globalId}
          });
        }
      }
    }
  }

  /**
   * @param {{workspaceId:string, globalIdList:[string]}} inputData
   */
  static async sendItemRemoveMessage(inputData) {
    const {workspaceId, globalIdList} = inputData;
    if (await SelectedWorkspace.getGlobalId() !== workspaceId) {
      return;
    }

    if (globalIdList && globalIdList.length) {
      socketServer.emit('note:remove', {
        'message': {
          globalId: globalIdList
        }
      });
    }
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async sendTextUpdateMessage(inputData) {
    let {workspaceId, globalId, typing, textVersion} = inputData;
    if (await SelectedWorkspace.getGlobalId() !== workspaceId) {
      return;
    }

    if (globalId) {
      if(textVersion >= 2) {
        typing = true;
      }
      socketServer.emit('noteText:update', {
        'message': {
          globalId: globalId,
          typing
        }
      });
    }
  }

  /**
   * @param {{workspaceId:string, globalId:string, attachmentGlobalId:string}} inputData
   */
  static async sendItemPreviewUpdateMessage(inputData) {
    const {workspaceId, globalId, attachmentGlobalId} = inputData;
    if (await SelectedWorkspace.getGlobalId() !== workspaceId) {
      return;
    }

    if (globalId && attachmentGlobalId) {
      socketServer.emit('notePreview:update', {
        'message': {
          globalId: globalId,
          attachmentGlobalId: attachmentGlobalId
        }
      });
    }
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async sendItemPreviewRemoveMessage(inputData) {
    const {workspaceId, globalId} = inputData;
    if (await SelectedWorkspace.getGlobalId() !== workspaceId) {
      return;
    }

    if (globalId) {
      socketServer.emit('notePreview:remove', {
        'message': {
          globalId: globalId,
          attachmentGlobalId: globalId
        }
      });
    }
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string, globalId:string}} inputData
   */
  static async sendTodoListUpdateMessage(inputData) {
    const {workspaceId, noteGlobalId, globalId} = inputData;
    if (await SelectedWorkspace.getGlobalId() !== workspaceId) {
      return;
    }

    if (globalId && noteGlobalId) {
      socketServer.emit('noteTodo:update', {
        'message': {
          globalId: globalId,
          noteGlobalId: noteGlobalId
        }
      });
    }
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string, globalId:string}} inputData
   */
  static async sendTodoListRemoveMessage(inputData) {
    const {workspaceId, noteGlobalId, globalId} = inputData;
    if (await SelectedWorkspace.getGlobalId() !== workspaceId) {
      return;
    }

    if (globalId && noteGlobalId) {
      socketServer.emit('noteTodo:remove', {
        'message': {
          noteGlobalId: noteGlobalId,
          globalId: globalId
        }
      });
    }
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string, globalId:string}} inputData
   */
  static async sendNoteAttachmentsUpdateMessage(inputData) {
    const {workspaceId, noteGlobalId, globalId} = inputData;
    if (await SelectedWorkspace.getGlobalId() !== workspaceId) {
      return;
    }

    if (globalId && noteGlobalId) {
      setTimeout(() => {
        socketServer.emit('noteAttachment:update', {
          'message': {
            noteGlobalId: noteGlobalId,
            globalId: globalId
          }
        });

        socketServer.emit('note:update', {'message': {globalId: noteGlobalId}});
      }, 1000);
    }
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string, globalId:string}} inputData
   */
  static async sendNoteAttachmentsRemoveMessage(inputData) {
    const {workspaceId, noteGlobalId, globalId} = inputData;
    if (await SelectedWorkspace.getGlobalId() !== workspaceId) {
      return;
    }

    if (globalId && noteGlobalId) {
      setTimeout(() => {
        socketServer.emit('noteAttachment:remove', {
          'message': {
            noteGlobalId: noteGlobalId,
            globalId: globalId
          }
        });
        socketServer.emit('note:update', {'message': {globalId: noteGlobalId}});
      }, 1000);
    }
  }

  /**
   * @param {{workspaceId:string, tagList:[string], action:string}} inputData
   */
  static async sendTagsMessage(inputData) {
    const {workspaceId, tagList, action} = inputData;
    if (await SelectedWorkspace.getGlobalId() !== workspaceId) {
      return;
    }

    if (tagList && tagList.length && action) {
      socketServer.emit('workspaceTag:' + action, {
        'message': {
          tags: tagList
        },
        workspaceId
      });
    }
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string, tag:string, action:string}} inputData
   */
  static async sendNoteTagsMessage(inputData) {
    const {workspaceId, noteGlobalId, tag, action} = inputData;
    if (await SelectedWorkspace.getGlobalId() !== workspaceId) {
      return;
    }

    if (noteGlobalId && tag && action) {
      socketServer.emit('noteTag:' + action, {
        'message': {
          noteGlobalId: noteGlobalId,
          tag: tag
        }
      });
    }
  }

  /**
   * @param {{workspaceId:string, storeType:string, data:*}} inputData
   */
  static addStoreData(inputData) {
    socketStore.add(inputData);
  }

  /**
   * @param {{workspaceId:string, storeType:string}} inputData
   */
  static clearStoreData(inputData) {
    socketStore.clear(inputData);
  }

  /**
   * @param {{workspaceId:string, storeType:string}} inputData
   */
  static getStoreData(inputData) {
    return socketStore.get(inputData);
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static flushStore(inputData) {
    const {workspaceId} = inputData;
    SocketFunctions.setSocketStoreSizeOverloadState({workspaceId, state: false});
    SocketFunctions.clearAttachmentsDataCopy(inputData);
    socketStore.flush(inputData);
  }

  static flushStores() {
    SocketFunctions.clearSocketStoresSizeOverloadState();
    SocketFunctions.clearAttachmentsDataCopies();
    socketStore.flushAll();
  }

  /**
   * @return {boolean}
   */
  static getSocketStoreSizeOverloadState(inputData) {
    const {workspaceId} = inputData;
    return typeof (socketStoreSizeOverloadState[workspaceId]) !== 'undefined' ? socketStoreSizeOverloadState[workspaceId] : false;
  }

  /**
   * @param {{workspaceId:string, state:boolean}} inputData
   */
  static setSocketStoreSizeOverloadState(inputData) {
    const {workspaceId, state} = inputData;
    socketStoreSizeOverloadState[workspaceId] = state;
  }

  static clearSocketStoresSizeOverloadState() {
    socketStoreSizeOverloadState = {};
  }

  /**
   * @param {{workspaceId:string, storeType:string}} inputData
   */
  static sendCollectedStoreData(inputData) {
    const {workspaceId, storeType} = inputData;
    let storeItems = socketStore.get(inputData);
    let noReloadOnEvents = [
      socketStoreType.NOTES_TAGS_FOR_UPDATE,
      socketStoreType.TAGS_FOR_UPDATE
    ];

    //console.log('storeType: ', storeType, storeItems.length);
    if (noReloadOnEvents.indexOf(storeType) < 0) {
      if (SocketFunctions.getSocketStoreSizeOverloadState({workspaceId})) {
        return;
      } else if (storeItems.length > SocketFunctions.SOCKET_STORE_SEND_MESSAGES_COUNT) {
        return SocketFunctions.setSocketStoreSizeOverloadState({workspaceId, state: true});
      }
    }

    for (let storeItem of storeItems) {
      if (storeType === socketStoreType.NOTES_FOR_UPDATE_COUNTERS) {
        SocketFunctions.sendItemsCountMessage({
          workspaceId,
          globalId: storeItem.globalId,
          parentId: storeItem.parentId,
          type: storeItem.type
        });
      } else if (storeType === socketStoreType.NOTES_FOR_UPDATE || storeType === socketStoreType.FOLDERS_FOR_UPDATE) {
        SocketFunctions.sendItemUpdateMessage({
          workspaceId,
          globalId: storeItem.globalId,
          parentId: storeItem.parentId,
          isCreate: storeItem.isCreate,
          type: storeItem.type,
          reminderRemove: storeItem.reminderRemove
        });
      } else if (storeType === socketStoreType.NOTES_FOR_REMOVE || storeType === socketStoreType.FOLDERS_FOR_REMOVE) {
        SocketFunctions.sendItemRemoveMessage({
          workspaceId,
          globalIdList: storeItem.globalId
        });
      } else if (storeType === socketStoreType.NOTES_FOR_UPDATE_TEXT) {
        SocketFunctions.sendTextUpdateMessage({
          workspaceId,
          globalId: storeItem.globalId,
          textVersion: storeItem.textVersion
        });
      } else if (storeType === socketStoreType.TODO_LIST_FOR_UPDATE) {
        SocketFunctions.sendTodoListUpdateMessage({
          workspaceId,
          noteGlobalId: storeItem.noteGlobalId,
          globalId: storeItem.globalId
        });
      } else if (storeType === socketStoreType.TODO_LIST_FOR_REMOVE) {
        SocketFunctions.sendTodoListRemoveMessage({
          workspaceId,
          noteGlobalId: storeItem.noteGlobalId,
          globalId: storeItem.globalId
        });
      } else if (storeType === socketStoreType.NOTES_ATTACHMENTS_FOR_UPDATE) {
        SocketFunctions.sendNoteAttachmentsUpdateMessage({
          workspaceId,
          noteGlobalId: storeItem.noteGlobalId,
          globalId: storeItem.globalId
        });
      } else if (storeType === socketStoreType.NOTES_ATTACHMENTS_FOR_REMOVE) {
        SocketFunctions.sendNoteAttachmentsRemoveMessage({
          workspaceId,
          noteGlobalId: storeItem.noteGlobalId,
          globalId: storeItem.globalId
        });
      } else if (storeType === socketStoreType.NOTES_TAGS_FOR_UPDATE) {
        SocketFunctions.sendNoteTagsMessage({
          workspaceId,
          noteGlobalId: storeItem.noteGlobalId,
          tag: storeItem.tag,
          action: storeItem.action
        });
      } else if (storeType === socketStoreType.TAGS_FOR_UPDATE) {
        SocketFunctions.sendTagsMessage({
          workspaceId,
          tagList: storeItem.tags,
          action: storeItem.action
        });
      }
    }
    socketStore.clear(inputData);
  }

  static setAttachmentsDataCopy(inputData) {
    const {workspaceId} = inputData;
    socketAttachmentsCopy[workspaceId] = SocketFunctions.getCollectedStoreData({
      workspaceId,
      storeType: socketStoreType.NOTES_ATTACHMENTS_FOR_UPDATE
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @return {[]}
   */
  static getAttachmentsDataCopy(inputData) {
    const {workspaceId} = inputData;
    return typeof (socketAttachmentsCopy[workspaceId]) !== 'undefined' ? socketAttachmentsCopy[workspaceId] : [];
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static clearAttachmentsDataCopy(inputData) {
    const {workspaceId} = inputData;
    if (typeof (socketAttachmentsCopy[workspaceId]) !== 'undefined') {
      socketAttachmentsCopy[workspaceId] = [];
    }
  }

  static clearAttachmentsDataCopies() {
    socketAttachmentsCopy = {};
  }

  /**
   * @param {{workspaceId:string, storeType:string}} inputData
   */
  static getCollectedStoreData(inputData) {
    return socketStore.get(inputData);
  }
}
