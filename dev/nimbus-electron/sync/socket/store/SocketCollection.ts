import {default as socketConnection} from "../socketFunctions";
import {default as appPage} from "../../../request/page";
import SelectedWorkspace from "../../../workspace/SelectedWorkspace";
import {default as errorHandler} from "../../../utilities/errorHandler";

export default class SocketCollection {

  /**
   * @param {{workspaceId:string, storeType:string}} inputData
   */
  static sendSocketCollectedData = (inputData) => {
    socketConnection.sendCollectedStoreData(inputData);
  };

  /**
   * @param {{workspaceId:string, storeType:string}} inputData
   */
  static getSocketCollectedData = (inputData) => {
    return socketConnection.getCollectedStoreData(inputData);
  };

  /**
   * @param {{workspaceId:string, storeType:string}} inputData
   */
  static clearSocketCollectedData = (inputData) => {
    socketConnection.clearStoreData(inputData);
  };

  /**
   * @param {{workspaceId:string}} inputData
   */
  static flushSocketCollectedData = (inputData) => {
    socketConnection.flushStore(inputData);
  };

  /**
   * @param {{workspaceId:string}} inputData
   * @return {boolean}
   */
  static needSendSocketCollection = async (inputData) => {
    const {workspaceId} = inputData;
    const selectedWorkspaceId = await SelectedWorkspace.getGlobalId();
    if (selectedWorkspaceId !== workspaceId) {
      return false;
    }

    if (socketConnection.getSocketStoreSizeOverloadState(inputData)) {
      appPage.reload();
      return false;
    }

    return true;
  };

  /**
   * @param {{workspaceId:string}} inputData
   */
  static saveAttachmentsCopy = (inputData) => {
    socketConnection.setAttachmentsDataCopy(inputData);
  };

  /**
   * @param {{workspaceId:string}} inputData
   */
  static getAttachmentsCopy = (inputData) => {
    return socketConnection.getAttachmentsDataCopy(inputData);
  };

  /**
   * @param {{workspaceId:string}} inputData
   */
  static clearAttachmentsCopy = (inputData) => {
    socketConnection.clearAttachmentsDataCopy(inputData);
  };
}
