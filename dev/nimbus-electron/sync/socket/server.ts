let io;
let localSocket;

export default class SocketServer {
  /**
   * @param {{}} server
   */
  static start(server) {
    if (!localSocket) {
      io = require('socket.io')(server);
      io.on('connection', (socket) => {
        localSocket = socket;
        io.on('disconnect', () => {
          localSocket = null;
        });
      });
    }
  }

  /**
   * @param {String} eventName
   * @param {{}} eventData
   */
  static emit(eventName, eventData) {
    eventData = eventData || {};
    if (localSocket && eventName) {
      setTimeout(() => {
        localSocket.emit(eventName, eventData);
      }, 250);
    }
  }
}
