import chalk from "chalk";
import fs from "fs-extra";
import {default as AttachmentHubDownloader} from "../process/hub/AttachmentHubDownloader";
import {default as socketConnection} from "../socket/socketFunctions";
import {default as appWindow} from "../../window/instance";
import {default as PouchDb} from "../../../pdb";
import {default as SyncAttachmentEntity} from "../nimbussdk/net/response/entities/SyncAttachmentEntity";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as urlParser} from "../../utilities/urlParser";

export default class AvatarSingleDownloader {
  static ERROR_BAD_AVATAR_INPUT_DATA = 1;
  static ERROR_AVATAR_DOWNLOAD_PROBLEM = 2;

  static AVATAR_TYPE_USER = 'user';
  static AVATAR_TYPE_WORKSPACE = 'workspace';
  static AVATAR_TYPE_ORGANIZATION = 'organization';

  /**
   * @param {{url:string, oldUrl:string, type:string}} inputData
   */
  static download(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {url, oldUrl, type} = inputData;
      if (!url) {
        return resolve({
          errorCode: AvatarSingleDownloader.ERROR_BAD_AVATAR_INPUT_DATA,
          complete: false,
          description: "Bad avatar url input data: " + url
        });
      }

      const storedFileUUID = url.split("/").pop();
      if(!storedFileUUID) {
        return resolve({
          errorCode: AvatarSingleDownloader.ERROR_BAD_AVATAR_INPUT_DATA,
          complete: false,
          description: "Bad avatar storedFileUUID input data: " + storedFileUUID
        });
      }

      let targetPath = `${PouchDb.getClientAttachmentPath()}/${storedFileUUID}`;
      // @ts-ignore
      let exists = await fs.exists(targetPath);
      // @ts-ignore
      if (exists) {
        return resolve({
          errorCode: 0,
          isDownloaded: true,
          description: "Avatar already exist with this storedFileUUID: " + storedFileUUID
        });
      }

      let entity = new SyncAttachmentEntity();
      entity.file_uuid = storedFileUUID;
      entity.location = url;
      let result = await AttachmentHubDownloader.getInstance().download({workspaceId: null, entity});
      if (!result) {
        return resolve({
          errorCode: AvatarSingleDownloader.ERROR_AVATAR_DOWNLOAD_PROBLEM,
          complete: false,
          description: "Avatar download problem: " + url
        });
      }

      if(url !== oldUrl) {
        await AvatarSingleDownloader.removeAvatarFile({url: oldUrl});
      }

      if(type === AvatarSingleDownloader.AVATAR_TYPE_USER) {
        AvatarSingleDownloader.socketUserAttachMessage({avatar: `avatar/${storedFileUUID}`});
      }

      resolve({
        errorCode: 0,
        complete: !!result.downloaded,
        description: "Avatar download success: " + storedFileUUID,
        response: result
      });
    });
  }

  /**
   * @param {string} url
   */
  static async checkAvatarExist(url) {
    let routeParams = urlParser.getPathParams(url);
    if(!routeParams.length) {
      return false;
    }
    let storedFileUUID = typeof (routeParams[2] !== 'undefined') ? routeParams[2] : null;
    if(storedFileUUID) {
      return false;
    }
    // @ts-ignore
    return await fs.exists(`${PouchDb.getClientAttachmentPath()}/${storedFileUUID}`);
  }

  /**
   * @param {{url:string}} avatar
   */
  static async removeAvatarFile(avatar) {
    if(!avatar) {
      return;
    }

    const {url} = avatar;
    if(!url) {
      return;
    }

    const storedPath = PouchDb.getClientAttachmentPath();
    if(!storedPath) {
      return;
    }

    const storedFileUUID = url.split("/").pop();
    if(!storedFileUUID) {
      return;
    }

    const targetPath = `${storedPath}/${storedFileUUID}`;
    try {
      // @ts-ignore
      const fileExist = await fs.exists(targetPath);
      // @ts-ignore
      if(fileExist) {
        fs.unlink(targetPath, (err) => {
          if (err) {
            if (config.SHOW_WEB_CONSOLE) {
              console.log(err);
            }
          }
        });
      }
    } catch (err) {
      console.log(err);
    }
  }

  /**
   * @param {{workspaceId:string, attachItem:{}}} inputData
   */
  static socketUserAttachMessage(inputData) {
    const {avatar} = inputData;
    if(avatar) {
      appWindow.get().webContents.send('event:client:me:name:update:response', {avatar});
    }
  }
}