import chalk from "chalk";
import fs from "fs-extra";
import {default as AttachmentHubDownloader} from "../process/hub/AttachmentHubDownloader";
import {default as socketConnection} from "../socket/socketFunctions";
import {default as item} from "../../db/models/item";
import {default as attach} from "../../db/models/attach";
import {default as appWindow} from "../../window/instance";
import {default as PouchDb} from "../../../pdb";
import {default as config} from "../../../config";

export default class AttachmentSingleDownloader {
  static ERROR_BAD_ATTACHMENT_INPUT_DATA = 1;
  static ERROR_ATTACHMENT_NOT_FOUND_BY_ID = 2;
  static ERROR_ATTACHMENT_DOWNLOAD_PROBLEM = 3;
  static ERROR_ATTACHMENT_FOR_OFFILNE_NOTE = 4;

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async download(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId} = inputData;
      if (!globalId) {
        return resolve({
          errorCode: AttachmentSingleDownloader.ERROR_BAD_ATTACHMENT_INPUT_DATA,
          complete: false,
          description: "Bad attachment id input data: " + globalId
        });
      }

      let queryData = {globalId: globalId};
      attach.find(queryData, {workspaceId}, async (err, attachItem) => {
        if (!attachItem) {
          return resolve({
            errorCode: AttachmentSingleDownloader.ERROR_ATTACHMENT_NOT_FOUND_BY_ID,
            complete: false,
            description: "Attachment not found by id: " + globalId
          });
        }

        /**
         * @param {{workspaceId:string, globalId:string}} inputData
         */
        const findUserItem = (inputData) => {
          // @ts-ignore
          return new Promise((resolve) => {
            const {workspaceId, globalId} = inputData;
            item.find({globalId}, {workspaceId}, (err, itemInstance) => {
              if (err || !itemInstance) {
                return resolve(null);
              }
              resolve(itemInstance);
            });
          });
        };

        const itemInstance = <{offlineOnly:boolean}>await findUserItem({workspaceId, globalId: attachItem.noteGlobalId});
        if (itemInstance.offlineOnly) {
          AttachmentSingleDownloader.socketUserAttachMessage({workspaceId, attachItem});
          return resolve({
            errorCode: AttachmentSingleDownloader.ERROR_ATTACHMENT_FOR_OFFILNE_NOTE,
            complete: true,
            description: "Attachment exist for offline note: " + globalId
          });
        }

        attach.update(queryData, {
          displayName: attachItem.displayName,
          isDownloaded: false
        }, {workspaceId}, async () => {

          let targetPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
          // @ts-ignore
          let exists = await fs.exists(targetPath);
          let entity = <{location:any}>await AttachmentHubDownloader.getUserAttachmentEntity({workspaceId, globalId});
          // @ts-ignore
          if (exists && entity.location) {
            // @ts-ignore
            if (await fs.exists(targetPath)) {
              try {
                await fs.unlink(targetPath);
              } catch (e) {
                if (config.SHOW_WEB_CONSOLE) {
                  console.log("Error => AttachmentSingleDownloader => remove: ", targetPath, e);
                }
              }
            }
          }

          if (!entity) {
            AttachmentSingleDownloader.socketUserAttachMessage({workspaceId, attachItem});
            return resolve({
              errorCode: AttachmentSingleDownloader.ERROR_ATTACHMENT_NOT_FOUND_BY_ID,
              complete: false,
              description: "Attachment not found by id: " + globalId
            });
          }

          let result = await AttachmentHubDownloader.getInstance().download({workspaceId, entity});

          if (!result) {
            AttachmentSingleDownloader.socketUserAttachMessage({workspaceId, attachItem});
            return resolve({
              errorCode: AttachmentSingleDownloader.ERROR_ATTACHMENT_DOWNLOAD_PROBLEM,
              complete: false,
              description: "Attachment download problem: " + globalId
            });
          }

          attach.update(queryData, {
            displayName: attachItem.displayName,
            isDownloaded: !!result.downloaded
          }, {workspaceId}, () => {
            AttachmentSingleDownloader.socketUserAttachMessage({workspaceId, attachItem});
          });

          resolve({
            errorCode: 0,
            complete: !!result.downloaded,
            description: "Attachment download success: " + globalId,
            response: result
          });

        });
      });
    });
  }

  /**
   * @param {{workspaceId:string, attachItem:{}}} inputData
   */
  static socketUserAttachMessage(inputData) {
    const {workspaceId, attachItem} = inputData;
    if (attachItem) {
      if (attachItem.inList) {
        socketConnection.sendNoteAttachmentsUpdateMessage({
          workspaceId,
          noteGlobalId: attachItem.noteGlobalId,
          globalId: attachItem.globalId
        });
      } else {
        if (appWindow.get()) {
          appWindow.get().webContents.send('event:client:update:inline:attach:response', {
            noteGlobalId: attachItem.noteGlobalId,
            globalId: attachItem.globalId
          });
        }
      }
    }
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async cancel(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId} = inputData;
      if (!globalId) {
        return resolve({
          errorCode: AttachmentSingleDownloader.ERROR_BAD_ATTACHMENT_INPUT_DATA,
          complete: false,
          description: "Bad attachment id input data: " + globalId
        });
      }

      //TODO: add abort download connection
    });
  }
}