import chalk from "chalk";
import {default as NimbusErrorHandler} from "../nimbussdk/net/exception/common/NimbusErrorHandler";
import {default as config} from "../../../config";
import {default as NimbusSDK} from "../nimbussdk/net/NimbusSDK";
import {default as App} from "./application/App";
import {default as NimbusSyncService} from "./services/NimbusSyncService";
import {default as AutoSyncManager} from "../nimbussdk/manager/AutoSyncManager";
import {default as appOnlineState} from "../../online/state";
import {default as SyncStatusChangedEvent} from "./events/SyncStatusChangedEvent";
import {default as SyncStatusDisplayEvent} from "./events/SyncStatusDisplayEvent";
import {default as ConnectionCollector} from "./connection/ConnectionCollector";
import {default as errorHandler} from "../../utilities/errorHandler";
import {default as workspace} from "../../db/models/workspace";
import {default as socketConnection} from "../socket/socketFunctions";
import API from "../nimbussdk/net/API";
import syncHandler from "../../utilities/syncHandler";

const DIFF_SYNC_TIME_TO_START_MSEC = 2500;

/**
 * @type {NimbusSyncService}
 */
let syncService = null;

/**
 * @type {number}
 */
let lastSyncStartTime = 0;

export default class SyncManager {

  /**
   * @param {{workspaceId:string, timeout:int, manualSync:boolean}} inputData
   */
  static startSyncOnline(inputData) {
    if(!SyncManager.readyToSync(inputData)) { return }
    const {workspaceId, timeout} = inputData;

    if (timeout > 0) {
      setTimeout(() => {
        if (appOnlineState.get()) {
          if (config.SHOW_WEB_CONSOLE) {
            console.log(`SyncManager start online (ws: ${workspaceId})`);
          }
          SyncManager.sync(inputData);
        } else {
          errorHandler.log({
            err: null, response: null,
            description: "Sync problem => SyncManager => startSyncOnline",
            data: inputData
          });
        }
      }, timeout);
    } else {
      if (appOnlineState.get()) {
        if (config.SHOW_WEB_CONSOLE) {
          console.log(`SyncManager start online (ws: ${workspaceId})`);
        }
        SyncManager.sync(inputData);
      } else {
        errorHandler.log({
          err: null, response: null,
          description: "Sync problem => SyncManager => startSyncOnline",
          data: inputData
        });
      }
    }
  }

  /**
   * @param {{workspaceId: string, timeout:number}} inputData
   */
  static readyToSync = (inputData) => {
    const { workspaceId, timeout } = inputData;
    const passTimeFromLastRun = new Date().getTime() - lastSyncStartTime
    if(passTimeFromLastRun < DIFF_SYNC_TIME_TO_START_MSEC) {
      syncHandler.sendLog(`Problem: try sync workspace too often for workspaceId: ${workspaceId} and timeout: ${timeout}`)
      return false
    }
    lastSyncStartTime = new Date().getTime()
    return true
  }

  /**
   * @param {{ workspaceId:string, manualSync:boolean }} inputData
   * @param {Function} callback
   */
  // @ts-ignore
  static async sync(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;

    if (config.SHOW_WEB_CONSOLE) {
      console.log(`Sync start (ws: ${workspaceId})`);
    }

    // @ts-ignore
    let userNotAuthorizedHandler = async (err, inputData) => {
      await SyncStatusDisplayEvent.set({
        workspaceId: workspaceId,
        newStatus: SyncStatusDisplayEvent.STATUS.CANCELED
      });

      if (config.SHOW_WEB_CONSOLE) {
        console.log("Error: sync user not authorized");
      }

      errorHandler.log({
        err: null, response: null,
        description: "Sync problem => SyncManager => userNotAuthorizedHandler"
      });

      SyncManager.clearSyncQueue(inputData);

      API.checkSignInStatus(err, null);
    };

    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.AUTHORIZED
    });

    NimbusSDK.getApi().userInfo({workspaceId}, (err, updatedInfo) => {
      if (err || !updatedInfo) {
        // @ts-ignore
        return SyncStatusChangedEvent.setErrorStatus({workspaceId, err}, async () => {
          await userNotAuthorizedHandler(err, inputData);
        });
      }

      if (config.SHOW_WEB_CONSOLE) {
        console.log("Success: get actual user info sync");
      }

      NimbusSDK.getAccountManager().isAuthorized(async (err, isAuthorized) => {
        if (err || !isAuthorized) {
          // @ts-ignore
          return SyncStatusChangedEvent.setErrorStatus({workspaceId, err}, async () => {
            await userNotAuthorizedHandler(err, inputData);
          });
        }

        if (config.SHOW_WEB_CONSOLE) {
          console.log(`Success: user authorized sync (ws: ${workspaceId})`);
        }

        SyncManager.clearSyncQueue(inputData);

        if (await App.canStartNewSync(inputData)) {
          syncService = new NimbusSyncService();
          await syncService.onStartCommand(inputData, callback);
        } else {
          if (config.SHOW_WEB_CONSOLE) {
            console.log(`Warning: can't start sync because sync already started (ws: ${workspaceId})`);
          }
          errorHandler.log({
            err: null, response: null,
            description: "Sync problem => SyncManager => canStartNewSync",
            data: {
              workspaceId
            }
          });

          callback(null, false);
        }
      });
    });
  }

  static stopAllSync() {
    // @ts-ignore
    NimbusSDK.getAccountManager().isAuthorized(async (err, isAuthorized) => {
      if (isAuthorized && syncService) {
        const availableWorkspaceList = <string[]>await workspace.getAvailableIdList();
        for (let workspaceId of availableWorkspaceList) {
          const syncNeedStop = await SyncManager.stopSync(workspaceId);
          if (!syncNeedStop) {
            continue;
          }

          errorHandler.log({
            err: null, response: null,
            description: "Sync => SyncManager => stopAllSync",
            data: {
              workspaceId
            }
          });
        }
      } else {
        errorHandler.log({
          err: null, response: null,
          description: "Sync problem => SyncManager => stopAllSync => userNotAuthorizedHandler"
        });
      }
    });
  }

  /**
   * @param {string} workspaceId
   */
  // @ts-ignore
  static async stopSync(workspaceId) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const syncType = await App.getRunningSyncType({workspaceId});
      if (syncType === App.SYNC_TYPES.NONE) {
        return resolve(false);
      }

      const response = {
        errorCode: -500,
        errDesc: 'Stop all requests to sync server',
        workspaceId
      };

      NimbusErrorHandler.throwNimbusApiErrorIfExist({
        response,
        functionName: "stopAllSync"
      });

      await ConnectionCollector.abort({workspaceId});

      await App.setRunningSyncType({
        workspaceId: workspaceId,
        type: App.SYNC_TYPES.NONE
      });

      await SyncStatusChangedEvent.setStatus({
        workspaceId,
        newStatus: SyncStatusChangedEvent.STATUS.PAUSED,
      });

      await SyncStatusDisplayEvent.set({
        workspaceId,
        newStatus: SyncStatusDisplayEvent.STATUS.CANCELED,
      });

      NimbusSyncService.onSyncEnd({
        workspaceId,
        startQueueSync: false
      });

      return resolve(true);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static addSyncQueue(inputData) {
    const {workspaceId} = inputData;
    const state = true;
    AutoSyncManager.setQueueSync({workspaceId, state});
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static clearSyncQueue(inputData) {
    AutoSyncManager.clearQueueSync(inputData);
  }

  /**
   * Clear sync data for all workspaces
   */
  static clearAllSyncData() {
    ConnectionCollector.clearAll();
    socketConnection.flushStores();
    AutoSyncManager.clearTimers();
    workspace.clearDefaultWorkspace();
  }
}
