export default class AttachmentObj {
  static TYPE_IMAGE = "image";
  static TYPE_VIDEO = "video";
  static TYPE_AUDIO = "audio";
  static TYPE_DOC = "doc";
  static TYPE_ARCHIVE = "archive";
  static TYPE_FILE = "file";

  /**
   * @type {string}
   */
  globalId = "";
  /**
   * @type {string}
   */
  parentId = "";
  /**
   * @type {number}
   */
  dateAdded = 0;
  /**
   * @type {number}
   */
  syncDate = 0;
  /**
   * @type {boolean}
   */
  needSync = false;
  /**
   * @type {string|null}
   */
  location = "";
  /**
   * @type {string|null}
   */
  tempName = "";
  /**
   * @type {string|null}
   */
  type = "";
  /**
   * @type {string}
   */
  typeExtra = "";
  /**
   * @type {number}
   */
  size = 0;
  /**
   * @type {string|null}
   */
  extension = "";
  /**
   * @type {string|null}
   */
  displayName = "";
  /**
   * @type {string}
   */
  oldDisplayName = "";
  /**
   * @type {number}
   */
  inList = 0;
  /**
   * @type {string}
   */
  uniqueUserName = "";
  /**
   * @type {boolean}
   */
  isAttachedToNote = false;
  /**
   * @type {string|null}
   */
  fileUUID = "";

  /**
   * @type {boolean}
   */
  isDownloaded = false;
  /**
   * @type {string}
   */
  localPath = "";

  /**
   * @type {boolean}
   */
  isDownloadingRunning = false;
  /**
   * @type {number}
   */
  currentProgress = 0;

  /**
   * @param {{}} obj
   * @returns {boolean}
   */
  static isDownloaded(obj) {
    return obj.isDownloaded;
  }

  /**
   * @param {{}} obj
   * @param {boolean} downloaded
   */
  static setDownloaded(obj, downloaded) {
    obj.isDownloaded = downloaded;
    return obj;
  }

  /**
   * @param {{}} obj
   * @returns {string}
   */
  static getLocalPath(obj) {
    return obj.localPath;
  }

  /**
   * @param {{}} obj
   * @param {string|null} localPath
   */
  static setLocalPath(obj, localPath) {
    obj.localPath = localPath;
    obj = AttachmentObj.setDownloaded(obj, !!localPath);
    return obj;
  }

  /**
   * @param {{}} obj
   * @returns {string}
   */
  static getLocalPathWithFile(obj) {
    return "file://" + obj.localPath;
  }

  /**
   * @param {{}} obj
   * @return {boolean}
   */
  static isValid(obj) {
    return true;
  }

  /**
   * @param {boolean} downloaded
   */
  setDownloaded(downloaded) {
    return AttachmentObj.setDownloaded(this, downloaded);
  }

  /**
   * @returns {string}
   */
  getLocalPath() {
    return AttachmentObj.getLocalPath(this);
  }

  /**
   * @param {string|null} localPath
   */
  setLocalPath(localPath) {
    return AttachmentObj.setLocalPath(this, localPath);
  }

  /**
   * @returns {string}
   */
  getLocalPathWithFile() {
    return AttachmentObj.getLocalPathWithFile(this);
  }

  /**
   * @return {boolean}
   */
  isValid() {
    return AttachmentObj.isValid(this);
  }
}
