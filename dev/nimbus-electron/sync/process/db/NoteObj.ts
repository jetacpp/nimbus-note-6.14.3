import {default as config} from "../../../../config";

export default class NoteObj {
  static ROLE_NOTE = "note";
  static ROLE_CLIP = "clip";
  static ROLE_WATCHES_TEXT = "watches_text";
  static ROLE_WATCHES_AUDIO = "watches_audio";
  static ROLE_PICTURE = "picture";
  static ROLE_AUDIO = "audio";
  static ROLE_VIDEO = "video";
  static ROLE_TODO = "todo";
  static ROLE_SCREENSHOT = "screenshot";
  static ROLE_SCREENCAST = "screencast";
  static ROLE_PDF = "pdf";
  static ROLE_EDIT = "edit";

  /**
   * @type {string}
   */
  globalId = "";

  /**
   * @type {boolean}
   */
  isTemp = false;
  /**
   * @type {number}
   */
  dateAdded = 0;
  /**
   * @type {number}
   */
  dateUpdated = 0;
  /**
   * @type {number}
   */
  syncDate = 0;
  /**
   * @type {boolean}
   */
  isDownloaded = false;
  /**
   * @type {boolean}
   */
  needSync = false;

  /**
   * @type {string}
   */
  parentId = "";
  /**
   * @type {string|null}
   */
  rootParentId = "";
  /**
   * @type {number}
   */
  index = 0;
  /**
   * @type {string}
   */
  type = "";
  /**
   * @type {string}
   */
  title = "";
  /**
   * @type {string}
   */
  shortText = "";

  /**
   * @type {string|null}
   */
  url = "";
  /**
   * @type {string}
   */
  role = "";
  /**
   * @type {string}
   */
  uniqueUserName = "";
  /**
   * @type {string|null}
   */
  firstImage = "";
  /**
   * @type {boolean}
   */
  isMoreThanLimit = false;
  /**
   * @type {boolean}
   */
  existOnServer = false;
  /**
   * @type {string}
   */
  tags = "";
  /**
   * @type {number}
   */
  editnote = 0;
  /**
   * @type {number}
   */
  isEncrypted = 0;
  /**
   * @type {string|null}
   */
  color = "";
  /**
   * @type {string|null}
   */
  textAttachmentGlobalId = "";
  /**
   * @type {boolean}
   */
  isMaybeInTrash = false;
  /**
   * @type {number}
   */
  attachmentsInListCount = 0;
  /**
   * @type {boolean}
   */
  attachmentsInListExist = false;
  /**
   * @type {boolean}
   */
  reminderExist = false;
  /**
   * @type {boolean}
   */
  todoExist = false;
  /**
   * @type {string}
   */
  reminderLabel = "";
  /**
   * @type {number}
   */
  todoCount = 0;
  /**
   * @type {number}
   */
  locationLat = 0;
  /**
   * @type {number}
   */
  locationLng = 0;
  /**
   * @type {string}
   */
  locationAddress = "";
  /**
   * @type {boolean}
   */
  isLocationExist = false;

  /**
   * @type {{global_id: string}}
   */
  preview = {
    'global_id': ""
  };

  /**
   * @param {{}} obj
   * @return {string}
   */
  static getGlobalId(obj) {
    return obj.globalId;
  }

  /**
   * @param {{}} obj
   * @param {string} globalId
   */
  static setGlobalId(obj, globalId) {
    obj.globalId = globalId;
    return obj;
  }

  /**
   * @param {{}} obj
   * @param {number} latitude
   * @param {number} longitude
   */
  static setLocation(obj, latitude, longitude) {
    obj = NoteObj.setLocationLat(obj, latitude);
    obj = NoteObj.setLocationLng(obj, longitude);
    let isExist = latitude && longitude;
    //obj = obj.setLocationExist(obj, isExist);
    let address = null;
    obj = NoteObj.setLocationAddress(obj, address);
    return obj;
  }

  /**
   * @param {{}} obj
   * @return {number}
   */
  static getLocationLat(obj) {
    return obj.locationLat;
  }

  /**
   * @param {{}} obj
   * @param {number} locationLat
   */
  static setLocationLat(obj, locationLat) {
    obj.locationLat = locationLat;
    return obj;
  }

  /**
   * @param {{}} obj
   * @return {number}
   */
  static getLocationLng(obj) {
    return obj.locationLng;
  }

  /**
   * @param {{}} obj
   * @param {number} locationLng
   */
  static setLocationLng(obj, locationLng) {
    obj.locationLng = locationLng;
    return obj;
  }

  /**
   * @param {{}} obj
   * @return {string}
   */
  static getLocationAddress(obj) {
    return obj.locationAddress;
  }

  /**
   * @param {{}} obj
   * @param {number} locationAddress
   */
  static setLocationAddress(obj, locationAddress) {
    obj.locationAddress = locationAddress;
    return obj;
  }

  /**
   * @param {{}} obj
   * @return {boolean}
   */
  static isLocationExist(obj) {
    return obj.isLocationExist;
  }

  /**
   * @param {{}} obj
   * @param {number} locationExist
   */
  static setLocationExist(obj, locationExist) {
    obj.isLocationExist = locationExist;
    return obj;
  }

  /**
   * @param {Object} obj
   * @return {boolean}
   */
  static isReminderExist(obj) {
    return obj.reminderExist;
  }

  /**
   * @param {{}} obj
   * @returns {boolean}
   */
  static isTodoExist(obj) {
    return obj.todoExist;
  }

  /**
   * @param {{}} obj
   * @returns {string}
   */
  static getReminderLabel(obj) {
    return obj.reminderLabel;
  }

  /**
   * @param {{}} obj
   * @returns {number}
   */
  static getTodoCount(obj) {
    return obj.todoCount;
  }

  /**
   * @param {{}} obj
   * @param {boolean} reminderExist
   */
  static setReminderExist(obj, reminderExist) {
    obj.reminderExist = reminderExist;
    return obj;
  }

  /**
   * @param {{}} obj
   * @param {boolean} todoExist
   */
  static setTodoExist(obj, todoExist) {
    obj.todoExist = todoExist;
    return obj;
  }

  /**
   * @param {{}} obj
   * @param {string|null} reminderLabel
   */
  static setReminderLabel(obj, reminderLabel) {
    obj.reminderLabel = reminderLabel;
    obj = NoteObj.setReminderExist(obj, !!reminderLabel);
    return obj;
  }

  /**
   * @param {{}} obj
   * @param {number} todoCount
   */
  static setTodoCount(obj, todoCount) {
    obj.todoCount = todoCount;
    obj = NoteObj.setTodoExist(obj, todoCount > 0);
    return obj;
  }

  /**
   * @param {{}} obj
   * @param {boolean} isDateAddedSelected
   * @returns {number}
   */
  static getNotesListDate(obj, isDateAddedSelected) {
    return isDateAddedSelected ? obj.dateAdded : obj.dateUpdated;
  }

  /**
   * @param {{}} obj
   * @returns {string}
   */
  static getTagsAsString(obj) {
    let builder = "";
    let tagsList = NoteObj.getTagsList(obj);
    for (let i = 0; i < tagsList.length; i++) {
      builder += tagsList[i];
      if (i !== tagsList.length - 1) {
        builder += ", ";
      }
    }
    return builder;
  }

  /**
   * @param {{}} obj
   * @return {number}
   */
  static getAttachmentsInListCount(obj) {
    return obj.attachmentsInListCount;
  }

  /**
   * @param {{}} obj
   * @param {number} attachmentsInListCount
   */
  static setAttachmentsInListCount(obj, attachmentsInListCount) {
    obj.attachmentsInListCount = attachmentsInListCount;
    obj = NoteObj.setAttachmentsInListExist(obj, attachmentsInListCount > 0);
    return obj;
  }

  /**
   * @param {{}} obj
   * @returns {boolean}
   */
  static isAttachmentsInListExist(obj) {
    return obj.attachmentsInListExist;
  }

  /**
   * @param {Object} obj
   * @param {boolean} attachmentsInListExist
   */
  static setAttachmentsInListExist(obj, attachmentsInListExist) {
    obj.attachmentsInListExist = attachmentsInListExist;
    return obj;
  }

  /**
   * @param {{}} obj
   * @return {string}
   */
  static getText(obj) {
    let text = "";
    try {
      text = obj.shortText;
    } catch (e) {

      if (config.SHOW_WEB_CONSOLE) {
        console.log("NoteObj.prototype.getText problem: ", e);
      }

    }
    return text;
  }

  /**
   * @param {{}} obj
   * @returns {string}
   */
  static getTags(obj) {
    return obj.tags;
  }

  /**
   * @param {{}} obj
   * @returns {[]}
   */
  static getTagsList(obj) {
    if (obj.tags) {
      return JSON.parse(obj.tags);
    } else {
      return [];
    }
  }

  /**
   * @param {Object} obj
   * @return {[]}
   */
  static getTagsArray(obj) {
    if (!obj.tags) {
      obj.tags = "";
    }
    return JSON.parse(obj.tags);
  }

  /**
   * @param {{}} obj
   * @param {string[]} tags
   */
  static setTags(obj, tags) {
    if (!tags) {
      tags = [];
    }
    obj.tags = JSON.stringify(tags);
    return obj;
  }

  /**
   * @param {{}} obj
   * @param {string} text
   */
  static setText(obj, text) {
    obj.text = text;
    return obj;
  }

  /**
   * @return {string}
   */
  getGlobalId() {
    return NoteObj.getGlobalId(this);
  }

  /**
   * @param {string} globalId
   */
  setGlobalId(globalId) {
    return NoteObj.setGlobalId(this, globalId);
  }

  /**
   * @param {number} latitude
   * @param {number} longitude
   */
  setLocation(latitude, longitude) {
    return NoteObj.setLocation(this, latitude, longitude);
  }

  /**
   * @return {number}
   */
  getLocationLat() {
    return NoteObj.getLocationLat(this);
  }

  /**
   * @param {number} locationLat
   */
  setLocationLat(locationLat) {
    return NoteObj.setLocationLat(this, locationLat);
  }

  /**
   * @return {number}
   */
  getLocationLng() {
    return NoteObj.getLocationLng(this);
  }

  /**
   * @param {number} locationLng
   */
  setLocationLng(locationLng) {
    return NoteObj.setLocationLng(this, locationLng);
  }

  /**
   * @return {string}
   */
  getLocationAddress() {
    return NoteObj.getLocationAddress(this);
  }

  /**
   * @param {number} locationAddress
   */
  setLocationAddress(locationAddress) {
    return NoteObj.setLocationAddress(this, locationAddress);
  }

  /**
   * @param {number} locationExist
   */
  setLocationExist(locationExist) {
    return NoteObj.setLocationExist(this, locationExist);
  }

  /**
   * @return {boolean}
   */
  isReminderExist() {
    return NoteObj.isReminderExist(this);
  }

  /**
   * @return {boolean}
   */
  isTodoExist() {
    return NoteObj.isTodoExist(this);
  }

  /**
   * @return {string}
   */
  getReminderLabel() {
    return NoteObj.getReminderLabel(this);
  }

  /**
   * @return {number}
   */
  getTodoCount() {
    return NoteObj.getTodoCount(this);
  }

  /**
   * @param {boolean} reminderExist
   */
  setReminderExist(reminderExist) {
    return NoteObj.setReminderExist(this, reminderExist);
  }

  /**
   * @param {boolean} todoExist
   */
  setTodoExist(todoExist) {
    return NoteObj.setTodoExist(this, todoExist);
  }

  /**
   * @param {string|null} reminderLabel
   */
  setReminderLabel(reminderLabel) {
    return NoteObj.setReminderLabel(this, reminderLabel);
  }

  /**
   * @param {number} todoCount
   */
  setTodoCount(todoCount) {
    return NoteObj.setTodoCount(this, todoCount);
  }

  /**
   * @param {boolean} isDateAddedSelected
   * @return {number}
   */
  getNotesListDate(isDateAddedSelected) {
    return NoteObj.getNotesListDate(this, isDateAddedSelected);
  }

  /**
   * @return {string}
   */
  getTagsAsString() {
    return NoteObj.getTagsAsString(this);
  }

  /**
   * @return {number}
   */
  getAttachmentsInListCount() {
    return NoteObj.getAttachmentsInListCount(this);
  }

  /**
   * @param {number} attachmentsInListCount
   */
  setAttachmentsInListCount(attachmentsInListCount) {
    return NoteObj.setAttachmentsInListCount(this, attachmentsInListCount);
  }

  /**
   * @returns {boolean}
   */
  isAttachmentsInListExist() {
    return NoteObj.isAttachmentsInListExist(this);
  }

  /**
   * @param {boolean} attachmentsInListExist
   */
  setAttachmentsInListExist(attachmentsInListExist) {
    return NoteObj.setAttachmentsInListExist(this, attachmentsInListExist);
  }

  /**
   * @return {string}
   */
  getText() {
    return NoteObj.getText(this);
  }

  /**
   * @return {string}
   */
  getTags() {
    return NoteObj.getTags(this);
  }

  /**
   * @return {[]}
   */
  getTagsList() {
    return NoteObj.getTagsList(this);
  }

  /**
   * @return {[]}
   */
  getTagsArray() {
    return NoteObj.getTagsArray(this);
  }

  /**
   * @param {string[]} tags
   */
  setTags(tags) {
    return NoteObj.setTags(this, tags);
  }

  /**
   * @param {string} text
   */
  setText(text) {
    return NoteObj.setText(this, text);
  }
}
