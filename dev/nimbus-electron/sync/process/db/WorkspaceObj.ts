export default class WorkspaceObj {
  static DEFAULT_NAME = "default";

  /**
   * @type {string}
   */
  globalId = "";
  /**
   * @type {string}
   */
  title = "";
  /**
   * @type {*}
   */
  org = null;
  /**
   * @type {int}
   */
  userId = 0;
  /**
   * @type {number}
   */
  createdAt = 0;
  /**
   * @type {number}
   */
  updatedAt = 0;
  /**
   * @type {boolean}
   */
  isDefault = false;
  /**
   * @type {int}
   */
  countMembers = 0;
  /**
   * @type {int}
   */
  countInvites = 0;
  /**
   * @type {*}
   */
  access = null;
  /**
   * @type {*}
   */
  defaultEncryptionKeyId = null;
  /**
   * @type {number}
   */
  syncDate = 0;
  /**
   * @type {boolean}
   */
  needSync = false;
  /**
   * @type {boolean}
   */
  isNotesLimited = false;
  /**
   * @type {{id: number, email:string, username:string, firstname:string, lastname:string}}
   */
  user = null;
  /**
   * @type {string}
   */
  notesEmail = '';
  /**
   * @type {WorkspaceAvatar}
   */
  avatar = '';
  /**
   * @type {string|null}
   */
  color = null;

  /**
   * @param {{}} obj
   * @param {Function} callback
   * @returns {string}
   */
  static getTitle(obj, callback = (err, res) => {
  }) {
    if (obj.isDefault && !obj.title) {
      return callback(null, WorkspaceObj.DEFAULT_NAME);
    }
    callback(null, obj.title);
  }

  /**
   * @param {{}} obj
   * @param {string} title
   */
  static setTitle(obj, title) {
    obj.title = title;
    return obj;
  }

  /**
   * @param {Function} callback
   */
  getTitle(callback = (err, res) => {
  }) {
    WorkspaceObj.getTitle(this, callback);
  }

  /**
   * @param {string} title
   */
  setTitle(title) {
    return WorkspaceObj.setTitle(this, title);
  }
}

export class WorkspaceAvatar {
  /**
   * @type {string}
   */
  url = "";
  /**
   * @type {number}
   */
  createdAt = "";
  /**
   * @type {number}
   */
  updatedAt = "";
}
