export default class TagObj {
  /**
   * @type {string}
   */
  title = "";
  /**
   * @type {string|null}
   */
  oldTitle = "";
  /**
   * @type {number}
   */
  dateAdded = 0;
  /**
   * @type {number}
   */
  dateUpdated = 0;
  /**
   * @type {number}
   */
  syncDate = 0;
  /**
   * @type {string}
   */
  uniqueUserName = "";
  /**
   * @type {string|null}
   */
  parentId = "";
  /**
   * @type {boolean}
   */
  needSync = false;

  /**
   * @type {boolean}
   */
  isChecked = false;
  /**
   * @type {number}
   */
  notesCount = -1;
}
