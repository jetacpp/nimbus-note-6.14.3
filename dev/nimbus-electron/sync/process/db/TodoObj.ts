export default class TodoObj {
  /**
   * @type {string}
   */
  globalId = "";
  /**
   * @type {string}
   */
  parentId = "";
  /**
   * @type {number}
   */
  dateAdded = 0;
  /**
   * @type {number}
   */
  syncDate = 0;
  /**
   * @type {boolean}
   */
  needSync = false;
  /**
   * @type {string}
   */
  label = "";
  /**
   * @type {boolean}
   */
  checked = false;
  /**
   * @type {string}
   */
  uniqueUserName = "";
}
