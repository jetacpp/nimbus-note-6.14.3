export default class ReminderObj {
  /**
   * @type {string}
   */
  date = "";
  /**
   * @type {number}
   */
  lat = 0;
  /**
   * @type {number}
   */
  lng = 0;
  /**
   * @type {string}
   */
  phone = "";
  /**
   * @type {number}
   */
  interval = 0;
  /**
   * @type {number}
   */
  priority = 2;
  /**
   * @type {number}
   */
  remind_until = 0;
}
