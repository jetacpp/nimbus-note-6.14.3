import {default as OrgUsageEntity} from "../../nimbussdk/net/response/entities/OrgUsageEntity";
import {default as OrgLimitsEntity} from "../../nimbussdk/net/response/entities/OrgLimitsEntity";
import {default as OrgUserEntity} from "../../nimbussdk/net/response/entities/OrgUserEntity";

export default class OrgObj {
    /**
     * @type {string}
     */
    id = '';
    /**
     * @type {string} enum("private","business")
     */
    type = '';
    /**
     * @type {string} enum("notes","capture")
     */
    serviceType = '';
    /**
     * @type {string}
     */
    title = '';
    /**
     * @type {string}
     */
    description = '';
    /**
     * @type {OrgUsageEntity}
     */
    usage = new OrgUsageEntity();
    /**
     * @type {OrgLimitsEntity}
     */
    limits = new OrgLimitsEntity();
    /**
     * @type {[FeatureEntity]}
     */
    features = [];
    /**
     * @type {OrgUserEntity}
     */
    user = new OrgUserEntity();
    /**
     * @type {string|null} only for type=business
     */
    sub = null;
    /**
     * @type {string|null} only for type=business
     */
    domain = null;
    /**
     * @type {boolean}
     */
    suspended = false;
    /**
     * @type {int|null}
     */
    suspendedAt = false;
    /**
     * @type {string|null}
     */
    suspendedReason = null;
    /**
     * @type {WorkspaceAccessEntity} only for type=business
     */
    access = null;
    /**
     * @type {string}
     */
    smallLogoUrl = null;
    /**
     * @type {string}
     */
    bigLogoUrl = null;
    /**
     * @type {number}
     */
    syncDate = 0;
    /**
     * @type {boolean}
     */
    needSync = false;
}
