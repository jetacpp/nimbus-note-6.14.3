import {default as AppConf} from "../utils/AppConf";

export default class FolderObj {
  static ROOT = "root";
  static DEFAULT = "default";
  static TRASH = "trash";
  static GOD = "JESUS_CHRIST";
  static ERASED_FROM_TRASH = "ERASED_FROM_TRASH";
  static ALL_NOTES = "ALL_NOTES";
  static DEFAULT_NAME = "My Notes";

  /**
   * @type {string}
   */
  globalId = "";
  /**
   * @type {string}
   */
  parentId = "";
  /**
   * @type {string|null}
   */
  rootParentId = "";
  /**
   * @type {number}
   */
  index = 0;
  /**
   * @type {string}
   */
  type = "";
  /**
   * @type {boolean}
   */
  existOnServer = false;
  /**
   * @type {string|null}
   */
  color = "";
  /**
   * @type {string}
   */
  title = "";
  /**
   * @type {number}
   */
  dateAdded = 0;
  /**
   * @type {number}
   */
  dateUpdated = 0;
  /**
   * @type {number}
   */
  syncDate = 0;
  /**
   * @type {string}
   */
  uniqueUserName = "";
  /**
   * @type {boolean}
   */
  onlyOffline = false;
  /**
   * @type {boolean}
   */
  needSync = false;
  /**
   * @type {boolean}
   */
  isMaybeInTrash = false;

  /**
   * @type {boolean}
   */
  isClicked = false;
  /**
   * @type {number}
   */
  subfoldersCount = -1;
  /**
   * @type {number}
   */
  notesCount = -1;
  /**
   * @type {number}
   */
  level = -1;
  /**
   * @type {number}
   */
  shared = 0;

  /**
   * @param {{}} obj
   * @param {Function} callback
   * @returns {string}
   */
  static getTitle(obj, callback = (err, res) => {
  }) {
    if (FolderObj.DEFAULT === obj.globalId && obj.title === FolderObj.DEFAULT_NAME) {
      return callback(null, FolderObj.DEFAULT_NAME);
    }
    callback(null, obj.title);
  }

  /**
   * @param {{}} obj
   * @param {string} title
   */
  static setTitle(obj, title) {
    if (FolderObj.DEFAULT === obj.globalId) {
      AppConf.setDefaultFolderTitle(title);
    }
    obj.title = title;
    return obj;
  }

  /**
   * @param {{}} obj
   * @return {boolean}
   */
  static isValid(obj) {
    return true;
  }

  /**
   * @param {Function} callback
   */
  getTitle(callback = (err, res) => {
  }) {
    FolderObj.getTitle(this, callback);
  }

  /**
   * @param {string} title
   */
  setTitle(title) {
    return FolderObj.setTitle(this, title);
  }

  /**
   * @return {boolean}
   */
  isValid() {
    return FolderObj.isValid(this);
  }
}
