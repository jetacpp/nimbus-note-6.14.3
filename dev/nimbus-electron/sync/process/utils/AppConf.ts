import chalk from "chalk";
import {default as NimbusSDK} from "../../nimbussdk/net/NimbusSDK";
import {default as settings} from "../../../db/models/settings";

let SyncConfigInstance = null;

export default class SyncConfig {
  static SYNC_TYPE_IS_HEADER = "sync_type_is_header";
  static SHOWED_WELCOME_SCREEN = "showed_welcome_screen";
  static WEBVIEW_ONE_COLUMN_VIEW = "webview_one_column_view";
  static QUICK_NOTE_WIDGET_ITEM_TEXT = "quick_note_widget_item_text";
  static QUICK_NOTE_WIDGET_ITEM_TODO = "quick_note_widget_item_todo";
  static QUICK_NOTE_WIDGET_ITEM_CAMERA = "quick_note_widget_item_camera";
  static QUICK_NOTE_WIDGET_ITEM_AUDIO = "quick_note_widget_item_audio";
  static QUICK_NOTE_WIDGET_ITEM_VIDEO = "quick_note_widget_item_video";
  static QUICK_NOTE_WIDGET_ITEM_PAINTER = "quick_note_widget_item_painter";
  static QUICK_NOTE_WIDGET_ITEM_TIME_REMINDER = "quick_note_widget_item_time_reminder";
  static QUICK_NOTE_WIDGET_ITEM_PLACE_REMINDER = "quick_note_widget_item_place_reminder";
  static IS_AFTER_AUTH_SCREEN = "is_after_auth_screen";
  static APP_THEME = "theme";
  static SHOW_FORMAT_PANEL = "show_format_panel";
  static NOTE_ONE_WIDGET = "note_one_widget";
  static NOTE_ONE_WIDGET_FOLDER = "note_one_widget_folder";
  static NOTE_ONE_WIDGET_FOLDER_TEMP = "note_one_widget_folder_temp";
  static NOTE_ONE_WIDGET_NOTE = "note_one_widget_note";
  static TODO_WIDGET_NOTE = "todoWidget";
  static NOTE_ONE_WIDGET_VISIBILITY = "note_one_widget_visibility";
  static NOTE_ONE_WIDGET_ACCESS_PASSWORD_WIDGET_ID = "note_one_widget_access_password_widget_id";
  static EDITOR_SCALE_IMAGES = "editor_scale_images";
  static EDITOR_DEFAULT_TAGS = "editor_default_tags";
  static EDITOR_AUTOSAVE = "editor_autosave";
  static TODO_SHOW_COMPLETED_TODO = "todo_show_completed_todo";
  static SEARCH_IN_NOTE_TEXT = "search_in_note_text";

  static VISIBILITY_CONTROL_RECT_LEFT = "RECT_LEFT";
  static VISIBILITY_CONTROL_RECT_RIGHT = "RECT_RIGHT";
  static VISIBILITY_CONTROL_RECT_TOP = "RECT_TOP";
  static VISIBILITY_CONTROL_RECT_BOTTOM = "RECT_BOTTOM";

  static APP_LOCK_PASSWORD = "APP_LOCK_PASSWORD";
  static APP_LOCK_FINGERPRINT = "APP_LOCK_FINGERPRINT";
  static KEY_AUTOSYNC_DISABLED_AFTER_BACKUP = "autosync_disabled_after_backup";
  static KEY_SYNC_IMPORTED_DATA_ENABLED = "sync_imported_data_enabled";
  static KEY_NEED_4X_DB_MIGRATION = "need_4x_db_migration";

  static TOP_SCROLL = "topScroll_";
  static CURRENT_VISIBLE_FRAGMENT_ONE_NOTE_WIDGET = "CURRENT_VISIBLE_FRAGMENT_ONE_NOTE_WIDGET";
  static SORT_NOTE_TYPE = "sort_note_type";
  static SORT_TAG_TYPE = "sort_tag_type";
  static SORT_FOLDER_TYPE = "sort_folder_type";
  static SEARCH_QUERY_CAHCE = "search_query_cache";
  static NIMBUS_NOTE_3_0_TUTORIAL = "NimbusNote_3.0_tutorial";
  static IMAGE_QUALITY_LEVEL = "IMAGE_QUALITY_LEVEL";
  static NOTES_LIST_VIEW_SHOW_TAGS = "notes_list_view_show_tags";
  static NOTES_LIST_VIEW_MODE = "notes_list_view_mode";
  static NOTES_LIST_CIRCLE_PREVIEW_VIEW = "notes_list_view_circle_preview_view";
  static ATTACHMENT_GLOBAL_ID = "attachment_global_id";
  static DEFAULT = "default";
  static SECRET_MODE = "SECRET_MODE";
  static WHATS_NEW_SHOWED = "WHATS_NEW_SHOWED";
  static LAST_OPEN_FOLDER = "last_open_folder";
  static DEFAULT_FOLDER = "default_folder";
  static DEFAULT_WIDGET_FOLDER = "default_widget_folder";
  static FULL_LIMIT_QUOTA_SHOWED = "full_limit_quota";
  static FULL_LIMIT_NOTE_SHOWED = "full_limit_note";
  static FULL_LIMIT_ATTACH_SHOWED = "full_limit_attach";
  static LOCATION_IS_UNAVAILABLE_NEVER_ASK_AGAIN = "location_is_unavailable_never_ask_again";

  static init() {
    if (!SyncConfigInstance) {
      SyncConfigInstance = new SyncConfig();
    }
  }

  /**
   * @param {string} key
   * @param {*} defValue
   * @param {Function} callback
   */
  static get(key, defValue, callback = (err, res) => {
  }) {
    defValue = defValue || null;
    settings.get(key, function (err, value) {
      callback(err, value ? value : defValue);
    });
  }

  /**
   * @param {string} key
   * @param {*} value
   * @param {Function} callback
   */
  static put(key, value, callback = (err, res) => {
  }) {
    if (!value) {
      return callback(null, value);
    }

    settings.set(key, value, function (err, response) {
      callback(err, response);
    });
  }

  /**
   * @param {string} key
   * @param {Function} callback
   */
  static remove(key, callback = (err, res) => {
  }) {
    settings.remove(key, callback);
  }

  /**
   * @param {string} title
   */
  static setDefaultFolderTitle(title) {
    NimbusSDK.getAccountManager().getUserEmail(function (err, userEmail) {
      SyncConfig.put(SyncConfig.DEFAULT + "_" + userEmail, title);
    });
  }

  /**
   * @param {Function} callback
   */
  static getDefaultFolderTitle(callback = (err, res) => {
  }) {
    NimbusSDK.getAccountManager().getUserEmail(function (err, userEmail) {
      SyncConfig.get((SyncConfig.DEFAULT + "_" + userEmail), "My Notes", function (err, value) {
        callback(err, value);
      });
    });
  }
}
