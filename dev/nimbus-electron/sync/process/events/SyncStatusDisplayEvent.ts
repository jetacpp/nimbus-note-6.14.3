import chalk from "chalk";
import {default as appWindowInstance} from "../../../window/instance";
import {default as App} from "../application/App";

let status = {};

/**
 * @constructor
 */
export default class SyncStatusDisplayEvent {
  static STATUS = {
    "NOT_AVAILABLE": "NOT_AVAILABLE",
    "CANCELED": "CANCELED",
    "READY": "READY",
    "DONE": "DONE",

    "AUTHORIZED": "AUTHORIZED",
    "DOWNLOADING_META_INFO": "DOWNLOADING_META_INFO",

    "REMOVE_FOLDERS": "REMOVE_FOLDERS",
    "REMOVE_NOTES": "REMOVE_NOTES",
    "REMOVE_ATTACHMENTS": "REMOVE_ATTACHMENTS",

    "UPDATE_CONTENT": "UPDATE_CONTENT",
    "DOWNLOAD_NOTES": "DOWNLOAD_NOTES",
    "DOWNLOAD_ATTACHMENT": "DOWNLOAD_ATTACHMENT",
    "UPLOAD_FOLDERS": "UPLOAD_FOLDERS",
    "UPLOAD_TAGS": "UPLOAD_TAGS",
    "UPLOAD_NOTES": "UPLOAD_NOTES",
    "UPLOAD_ATTACHMENTS": "UPLOAD_ATTACHMENTS",

    "TRAFFIC_LIMIT": "TRAFFIC_LIMIT",
    "NEED_SYNC_BEFORE_SHARE": "NEED_SYNC_BEFORE_SHARE"
  };

  static RESET_SYNC_TYPE_STATUS_LIST = [
    SyncStatusDisplayEvent.STATUS.CANCELED,
    SyncStatusDisplayEvent.STATUS.NOT_AVAILABLE,
    SyncStatusDisplayEvent.STATUS.TRAFFIC_LIMIT
  ];

  /**
   * @param {{workspaceId:string, newStatus:string, props:{}}} inputData
   * @return {string}
   */
  static async set(inputData) {
    const {workspaceId, newStatus, props} = inputData;
    let data = {
      workspaceId: workspaceId,
      status: newStatus
    };

    if (SyncStatusDisplayEvent.RESET_SYNC_TYPE_STATUS_LIST.indexOf(newStatus) >= 0) {
      await App.setRunningSyncType({
        workspaceId: workspaceId,
        type: App.SYNC_TYPES.NONE
      });
    }

    if (appWindowInstance.get()) {
      if (props) {
        for (let i in props) {
          if (props.hasOwnProperty(i)) {
            data[i] = props[i];
          }
        }
      }

      appWindowInstance.get().webContents.send('event:client:sync:response', data);
    }

    return status[workspaceId] = newStatus;
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @return {string}
   */
  static get(inputData) {
    const {workspaceId} = inputData;
    return typeof (status[workspaceId]) === 'undefined' ? null : status[workspaceId];
  }
}

