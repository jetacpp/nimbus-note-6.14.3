import {default as config} from "../../../../config";
import {default as SyncStatusDisplayEvent} from "./SyncStatusDisplayEvent";
import {default as socketConnection} from "../../socket/socketFunctions";
import {default as appOnlineState} from "../../../online/state";
import {default as ConnectionCollector} from "../connection/ConnectionCollector";
import {default as App} from "../application/App";
import chalk from "chalk";
import {default as AutoSyncManager} from "../../nimbussdk/manager/AutoSyncManager";

let status = {};
let progress = {};
let err = {};

export default class SyncStatusChangedEvent {

  static STATUS = {
    "HEADER_START": "HEADER_START",
    "HEADER_FINISH": "HEADER_FINISH",
    "FULL_START": "FULL_START",
    "FULL_FINISH": "FULL_FINISH",
    "FULL_IN_PROGRESS": "FULL_IN_PROGRESS",
    "FAILED": "FAILED",
    "PAUSED": "PAUSED",
    "NONE": "NONE"
  };

  /**
   * @param {{workspaceId:string, newStatus:string, newProgress:int}} inputData
   * @constructor
   */
  constructor(inputData) {
    let {workspaceId, newStatus, newProgress} = inputData;
    newProgress = newProgress || null;
    status[workspaceId] = newStatus;
    if (newProgress) {
      progress[workspaceId] = newProgress;
    }
  }

  /**
   * @param {{workspaceId:string, newStatus:string, newProgress:int}} inputData
   */
  static setStatus(inputData) {
    let {workspaceId, newStatus, newProgress} = inputData;
    newProgress = newProgress || null;
    status[workspaceId] = newStatus;
    if (newProgress) {
      progress[workspaceId] = newProgress;
    }
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @returns {string}
   */
  static getStatus(inputData) {
    const {workspaceId} = inputData;
    return typeof (status[workspaceId]) !== 'undefined' ? status[workspaceId] : SyncStatusChangedEvent.STATUS.NONE;
  }

  /**
   * @param {workspaceId:string, err:Error} inputData
   * @param {Function} callback
   */
  // @ts-ignore
  static async setErrorStatus(inputData, callback = () => {
  }) {
    const {workspaceId, newErr} = inputData;

    if (config.SHOW_WEB_CONSOLE) {
      console.log(`Start cancelSync sync (ws: ${workspaceId})`);
    }

    err[workspaceId] = newErr || new Error("throw error status");
    status[workspaceId] = SyncStatusChangedEvent.STATUS.FAILED;

    await App.setRunningSyncType({
      workspaceId: workspaceId,
      type: App.SYNC_TYPES.NONE
    });
    socketConnection.flushStore(inputData);

    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.CANCELED
    });

    AutoSyncManager.clearQueueSync(inputData);

    callback();
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static setPauseStatus(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId} = inputData;

      if (config.SHOW_WEB_CONSOLE) {
        console.log(`Start pauseSync sync (ws: ${workspaceId})`);
      }

      const canStartNewSync = await App.canStartNewSync({workspaceId});
      if (!canStartNewSync) {
        err[workspaceId] = null;
        status[workspaceId] = SyncStatusChangedEvent.STATUS.PAUSED;

        await App.setRunningSyncType({
          workspaceId: workspaceId,
          type: App.SYNC_TYPES.NONE
        });
        socketConnection.flushStore(inputData);
        await ConnectionCollector.abort({workspaceId});

        await SyncStatusDisplayEvent.set({
          workspaceId: workspaceId,
          newStatus: SyncStatusDisplayEvent.STATUS.CANCELED
        });
      }

      AutoSyncManager.clearQueueSync(inputData);

      return resolve(true);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static setCleanStatus(inputData) {
    const {workspaceId} = inputData;
    status[workspaceId] = SyncStatusChangedEvent.STATUS.NONE;
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static needPreventSync(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId} = inputData;
      if (!appOnlineState.get()) {
        const err = new Error('Error: syncPrevent - client offline');
        await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
        if (config.SHOW_WEB_CONSOLE) {
          console.log(`Start syncPrevent: client offline (ws: ${workspaceId})`);
        }
        return resolve(true);
      }

      if ([SyncStatusChangedEvent.STATUS.FAILED, SyncStatusChangedEvent.STATUS.PAUSED].indexOf(SyncStatusChangedEvent.getStatus(inputData)) >= 0) {
        switch (SyncStatusChangedEvent.getStatus(inputData)) {
          case SyncStatusChangedEvent.STATUS.FAILED:
            const err = new Error('Error: syncPrevent - client error');
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            if (config.SHOW_WEB_CONSOLE) {
              console.log(`Start syncPrevent: client failed (ws: ${workspaceId})`);
            }
            break;
          case SyncStatusChangedEvent.STATUS.PAUSED:
            await SyncStatusChangedEvent.setPauseStatus(inputData);
            if (config.SHOW_WEB_CONSOLE) {
              console.log(`Start syncPrevent: client paused (ws: ${workspaceId})`);
            }
            break;
        }

        return resolve(true);
      }
      return resolve(false);
    });
  }
}
