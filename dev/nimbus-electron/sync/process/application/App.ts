import chalk from "chalk";
import {default as NimbusSDK} from "../../nimbussdk/net/NimbusSDK";
import {default as SyncType} from "../SyncType";
import {default as userSettings} from "../../../db/models/userSettings";
import {default as workspace} from "../../../db/models/workspace";
import {default as auth} from "../../../auth/auth";

export default class App {
  static SYNC_TYPES = SyncType;
  static CURRENT_RUNNING_SYNC_SETTING = 'sync-type-running';

  /**
   * @return {Promise<string>}
   */
  static async initSyncState() {
    // @ts-ignore
    return new Promise((resolve) => {
      auth.getUser((err, authInfo) => {
        if (authInfo && Object.keys(authInfo).length) {
          workspace.findAll({}, {}, async (err, workspaceInstances) => {
            if (err) {
              return resolve(true);
            }

            /**
             * @param {{workspaceId:string}} inputData
             */
            const updateUserSettings = async (inputData) => {
              // @ts-ignore
              return new Promise((resolve) => {
                const {workspaceId} = inputData;
                userSettings.set(`${App.CURRENT_RUNNING_SYNC_SETTING}:${workspaceId}`, App.SYNC_TYPES.NONE, () => {
                  return resolve(true);
                });
              });
            };

            let workspacesIdList = ['', null, workspace.DEFAULT_NAME];
            for (let workspaceInstance of workspaceInstances) {
              const workspaceId = workspaceInstance.globalId;
              workspacesIdList.push(workspaceId);
            }

            for (let workspaceId of workspacesIdList) {
              await updateUserSettings({workspaceId});
            }

            NimbusSDK.setup();
            resolve(true);
          });
        } else {
          resolve(false);
        }
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<string>}
   */
  static async getRunningSyncType(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId} = inputData;
      userSettings.get(`${App.CURRENT_RUNNING_SYNC_SETTING}:${workspaceId}`, (err, data) => {
        if (err || !data) {
          return resolve(App.SYNC_TYPES.NONE);
        }

        if (data && Object.keys(data).length === 0) {
          return resolve(App.SYNC_TYPES.NONE);
        }

        resolve(data);
      });
    });
  }

  /**
   * @param {{workspaceId:string, type:string}} inputData
   * @return {Promise<string>}
   */
  static async setRunningSyncType(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, type} = inputData;
      userSettings.set(`${App.CURRENT_RUNNING_SYNC_SETTING}:${workspaceId}`, type, (err, list) => {
        if (!list) {
          return resolve(App.SYNC_TYPES.NONE);
        }
        resolve(type);
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<boolean>}
   */
  static async canStartNewSync(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const currentRunningSync = await App.getRunningSyncType(inputData);
      resolve(currentRunningSync === App.SYNC_TYPES.NONE);
    });
  }
}
