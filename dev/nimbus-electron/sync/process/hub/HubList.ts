export default class HubList {
  /**
   * @param []
   */
  list;

  constructor() {
    this.list = [];
  }

  /**
   * @param {{}} item
   */
  add(item) {
    this.list.push(item);
  }

  /**
   * @param {int} pos
   */
  get(pos) {
    return typeof (this.list[pos]) === "undefined" ? this.list[pos] : null;
  }

  /**
   * @param {{}} item
   */
  remove(item) {
    for (let i = 0; i < this.list.length; i++) {
      if (item === this.list[i]) {
        this.list.splice(i, 1);
      }
    }
  }

  /**
   * @return {int}
   */
  size() {
    return this.list.length;
  }
}
