import chalk from "chalk";
import fs = require('fs-extra');
import request = require("request");
import {default as NimbusSDK} from "../../nimbussdk/net/NimbusSDK";
import {default as ConnectionCollector} from "../connection/ConnectionCollector";
import {default as config} from "../../../../config.runtime";
import {default as urlParser} from "../../../utilities/urlParser";
import {default as errorHandler} from "../../../utilities/errorHandler";
import {default as AttachmentObjRepository} from "../repositories/AttachmentObjRepository";
import {default as AttachmentObj} from "../db/AttachmentObj";
import {default as PouchDb} from "../../../../pdb";
import {default as ApiConst} from "../../nimbussdk/net/ApiConst";
import * as path from "path";

/**
 * @type AttachmentHubDownloader
 */
let hubDownloader;

export default class AttachmentHubDownloader {
  /**
   * @return {AttachmentHubDownloader}
   */
  static getInstance() {
    if (!hubDownloader) {
      hubDownloader = new AttachmentHubDownloader();
    }
    return hubDownloader;
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async getUserAttachmentEntity(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      AttachmentObjRepository.get(inputData, (err, attachmentObj) => {
        if (err) {
          return resolve(null);
        }

        if (!attachmentObj) {
          return resolve(null);
        }

        resolve(AttachmentObjRepository.convertToSyncAttachmentEntityForManualDownload(attachmentObj));
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @return {Promise<AttachmentObj>}
   */
  static async getUserAttachment(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      AttachmentObjRepository.get(inputData, (err, attachmentObj) => {
        if (err) {
          return resolve(null);
        }
        resolve(attachmentObj);
      });
    });
  }

  /**
   * @param {{workspaceId:string, entity:AttachmentObj}} inputData
   * @return {Promise<{}>}
   */
  static async saveFile(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, entity} = inputData;

      /**
       * @param {{workspaceId:string, globalId:string, remotePath:string, targetPath:string}} inputData
       * @return {Promise<Object|null>}
       */
      let saveRemoteFile = async (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          /**
           * @param {{workspaceId:string, globalId:string, remotePath:string, targetPath:string}} inputData
           */
          let getRemoteFile = async (inputData) => {
            // @ts-ignore
            return new Promise(async (resolve) => {
              let {workspaceId, globalId, remotePath, targetPath} = inputData;
              if(targetPath) { targetPath = path.resolve(targetPath); }
              let attachment = await AttachmentHubDownloader.getUserAttachment({workspaceId, globalId});
              // @ts-ignore
              let fileExist = await fs.exists(targetPath);
              // @ts-ignore
              if (fileExist) {
                return resolve({
                  attachment,
                  downloaded: true,
                  wasDownloadedBefore: true
                });
              } else {
                try {
                  NimbusSDK.getAccountManager().getAccountSession((err, session) => {
                    if (err || !session) {
                      return resolve({
                        attachment
                      });
                    }

                    if (!session.getSessionId()) {
                      return resolve({
                        attachment
                      });
                    }

                    let headers = {
                      "EverHelper-Session-ID": session.getSessionId(),
                      'x-client-software': ApiConst._CLIENT_SOFTWARE,
                      'x-client-version': config.APP_VERSION
                    };

                    let options = {
                      uri: remotePath,
                      method: "GET",
                      followRedirect: true,
                      maxRedirects: 10,
                      forever: false,
                      headers: headers
                    };

                    let requestUniqueId = new Date().getTime() + ":attach:" + globalId;

                    let writeStream = <any>fs.createWriteStream(targetPath);
                    writeStream.uniqueId = requestUniqueId;
                    writeStream.on('error', async (e) => {
                      writeStream.close();
                      ConnectionCollector.clearById({
                        workspaceId,
                        id: req.uniqueId
                      });

                      if (config.SHOW_WEB_CONSOLE) {
                        console.log("Problem: save to file: ", targetPath, " with error: ", e.message);
                      }

                      try {
                        // @ts-ignore
                        if (await fs.exists(targetPath)) {
                          try {
                            fs.unlink(targetPath, () => {
                            });
                          } catch (e) {
                            if (config.SHOW_WEB_CONSOLE) {
                              console.log("Error => AttachmentHubDownloader => getRemoteFile => remove file: ", targetPath, e);
                            }
                          }
                        }
                      } catch (e) {
                        if (config.SHOW_WEB_CONSOLE) {
                          console.log("Problem: remove file info that got error during the load: ", targetPath);
                        }
                      }

                      return resolve({
                        attachment
                      });
                    });
                    writeStream.on('finish', async () => {
                      ConnectionCollector.clearById({
                        workspaceId,
                        id: req.uniqueId
                      });
                      return resolve({
                        attachment,
                        downloaded: true
                      });
                    });

                    let req = <any>request
                      .get(options)
                      .on('error', (err) => {
                        errorHandler.log("Can not download attach: " + options.uri);
                        let errMessage = "Bad response from server";
                        if (err.code === 'ESOCKETTIMEDOUT' || err.code === 'ETIMEDOUT') {
                          errMessage = "Timeout request from server";
                        }
                        writeStream.emit('error', new Error(errMessage));

                        if (config.SHOW_WEB_CONSOLE) {
                          console.log('options: ', options.uri);
                          console.log('err: ', err.code);
                        }

                        return resolve({
                          attachment
                        });
                      })
                      .on('abort', () => {
                        errorHandler.log("Abort download attach: " + options.uri);
                        writeStream.emit('error', new Error("Abort download attach from server"));
                        return resolve({
                          attachment
                        });
                      })
                      .on('response', (response) => {
                        let statusCode = response.statusCode;
                        if (statusCode != 200) {
                          errorHandler.log("Can not download attach: " + options.uri);
                          writeStream.emit('error', new Error("No response from server"));
                          return resolve({
                            attachment
                          });
                        }
                      });

                    req.uniqueId = requestUniqueId;
                    ConnectionCollector.add({
                      workspaceId,
                      uniqueId: requestUniqueId,
                      xhrConnection: req
                    });
                    req.pipe(writeStream);
                  });
                } catch (err) {
                  let errMsg = "Problem to download and save file with id: ";
                  errMsg += globalId + " and path: " + targetPath;
                  errMsg += " details: " + err.toString();
                  if (config.SHOW_WEB_CONSOLE) {
                    console.log(errMsg);
                  }
                }
              }
            });
          };

          resolve(await getRemoteFile(inputData));
        });
      };

      /**
       * @param {{workspaceId:string, globalId:string, remotePath:string, targetPath:string, clientAttachmentBasePath:string}} inputData
       */
      let createFile = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {clientAttachmentBasePath} = inputData;
          // @ts-ignore
          fs.mkdir(clientAttachmentBasePath, async () => {
            resolve(await saveRemoteFile(inputData));
          });
        });
      };

      let globalId = entity.global_id;
      let attachment = await AttachmentHubDownloader.getUserAttachment({workspaceId, globalId});
      if (!PouchDb.getClientAttachmentPath()) {
        return resolve({
          attachment
        });
      }

      let remotePath = entity.location ? urlParser.getAttachmentRemoteUri(entity.location) : "";
      let targetPath = `${PouchDb.getClientAttachmentPath()}/${entity.file_uuid}`;
      if (!remotePath || !targetPath) {
        return resolve({
          attachment
        });
      }

      // @ts-ignore
      let baseDirectoryExists = await fs.exists(PouchDb.getClientAttachmentPath());
      // @ts-ignore
      if (!baseDirectoryExists) {
        let baseDirectory = await createFile({
          workspaceId,
          globalId,
          remotePath,
          targetPath,
          clientAttachmentBasePath: PouchDb.getClientAttachmentPath()
        });
        if (!baseDirectory) {
          return resolve({
            attachment
          });
        }
      }

      return resolve(await saveRemoteFile({
        workspaceId,
        globalId,
        remotePath,
        targetPath,
        clientAttachmentBasePath: PouchDb.getClientAttachmentPath()
      }));
    });
  }

  /**
   * @param {{workspaceId:string, entity:{}}} inputData
   * @return {Promise<AttachmentObj>}
   */
  async download(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      let attachment = await AttachmentHubDownloader.saveFile(inputData);
      if (!attachment) {
        return resolve(null);
      }
      resolve(attachment);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @return {Promise<boolean>}
   */
  async checkNeedDownload(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (!PouchDb.getClientAttachmentPath()) {
        return resolve(false);
      }

      let entity = <{file_uuid:string}>await AttachmentHubDownloader.getUserAttachmentEntity(inputData);

      if (!entity) {
        return resolve(false);
      }

      if (!entity.file_uuid) {
        return resolve(false);
      }

      let targetPath = `${PouchDb.getClientAttachmentPath()}/${entity.file_uuid}`;
      if (!targetPath) {
        return resolve(false);
      }

      // @ts-ignore
      let fileAlreadyDownloaded = await fs.exists(targetPath);
      // @ts-ignore
      if (fileAlreadyDownloaded) {
        return resolve(false);
      }

      resolve(entity);
    });
  }
}
