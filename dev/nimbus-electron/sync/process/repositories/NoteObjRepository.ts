import chalk from "chalk";
import {default as config} from "../../../../config";
import {default as date} from "../../../utilities/dateHandler";
import {default as NimbusSDK} from "../../nimbussdk/net/NimbusSDK";
import {default as AbstractNote} from "../../nimbussdk/net/response/entities/AbstractNote";
import {default as NoteObj} from "../db/NoteObj";
import {default as FolderObj} from "../db/FolderObj";
import {default as AttachmentObj} from "../db/AttachmentObj";
import {default as FolderObjRepository} from "./FolderObjRepository";
import {default as AttachmentObjRepository} from "./AttachmentObjRepository";
import {default as TodoObjRepository} from "./TodoObjRepository";
import {default as TagObjRepository} from "./TagObjRepository";
import {default as SyncFullNoteUpdateEntity} from "../../nimbussdk/net/response/entities/SyncFullNoteUpdateEntity";
import {default as SyncHeaderNoteUpdateEntity} from "../../nimbussdk/net/response/entities/SyncHeaderNoteUpdateEntity";
import {default as generator} from "../../../utilities/generatorHandler";
import {default as itemModel} from "../../../db/models/item";
import {default as textModel} from "../../../db/models/text";
import {default as tagModel} from "../../../db/models/tag";
import {default as todoModel} from "../../../db/models/todo";
import {default as noteTagsModel} from "../../../db/models/noteTags";
import {default as attachModel} from "../../../db/models/attach";
import {default as socketConnection} from "../../socket/socketFunctions";
import {default as socketStoreType} from "../../socket/socketStoreType";
import {default as orgs} from "../../../db/models/orgs";
import {default as ReminderNotice} from "../../../popup/ReminderNotice";
import {default as workspace} from "../../../db/models/workspace";
import TextEditor from "../../../db/TextEditor";

export default class NoteObjRepository {
  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static get(inputData, callback = (err, res) => {
  }) {
    let {workspaceId, globalId} = inputData;

    if (globalId === null) {
      globalId = "";
    }

    itemModel.find({"globalId": globalId, "type": "note"}, {workspaceId}, (err, noteObj) => {
      if (NoteObjRepository.isValid(noteObj)) {
        // @ts-ignore
        callback(err, Object.assign({}, noteObj));
      } else {
        callback(err, null);
      }
    });
  };

  /**
   * @param {string} title
   * @return {string}
   */
  static getTitle(title) {
    return title;
  }

  /**
   * @param {{workspaceId:string, parentId:string, role:string, tag:string}} inputData
   * @param {Function} callback
   */
  static create(inputData, callback = (err, res) => {
  }) {
    let {workspaceId, parentId, role, tag} = inputData;
    if (tag) {
      let noteObj = new NoteObj();
      noteObj.parentId = parentId;
      noteObj.role = role;
      if (tag.trim()) {
        noteObj = NoteObj.setTags(noteObj, JSON.parse(tag));
      }
      return callback(null, noteObj);
    } else {
      if (parentId === null) {
        parentId = FolderObj.DEFAULT;
      }
      if (role === null) {
        role = NoteObj.ROLE_NOTE;
      }

      let globalId = NoteObjRepository.generateGlobalId();
      let currentTimeInSeconds = new Date().getTime();

      NimbusSDK.getAccountManager().getUniqueUserName((uniqueUserName) => {
        let note = new NoteObj();
        note.parentId = parentId;
        note.globalId = globalId;
        note.index = 0;
        note.dateAdded = currentTimeInSeconds;
        note.dateUpdated = currentTimeInSeconds;
        note.syncDate = 0;
        note.rootParentId = null;
        note.type = "note";
        note.title = NoteObjRepository.getTitle(role);
        note = NoteObj.setText(note, "");
        note.shortText = "";
        note = NoteObj.setLocation(note, 0, 0);
        note = NoteObj.setTodoCount(note, 0);
        note.url = null;
        note.role = role;
        note.isEncrypted = 0;
        note.uniqueUserName = uniqueUserName;
        note.firstImage = null;
        note.isDownloaded = true;
        note.needSync = true;
        note.isMoreThanLimit = false;
        note.existOnServer = false;
        note.isTemp = false;
        note = NoteObj.setReminderLabel(note, null);
        note.editnote = 1;
        note.color = null;
        note.textAttachmentGlobalId = null;
        note = NoteObj.setAttachmentsInListCount(note, 0);

        return callback(null, note);
      });
    }
  }

  /**
   * @return {string}
   */
  static generateGlobalId() {
    return generator.randomString(16);
  }

  /**
   * @param {NoteObj|NoteObj[]} noteObjs
   * @return {Promise<any>}
   */
  static async update(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, noteObjs} = inputData;
      if (!noteObjs) {
        return resolve(false);
      }

      /**
       * @param {{workspaceId:string, noteObj:{}}} inputData
       */
      let processUserNote = async (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {workspaceId, noteObj} = inputData;
          if (noteObj.type !== "note") {
            return resolve(false);
          }

          const globalId = noteObj.globalId;
          let existItemInstance = <any>await NoteObjRepository.getUserNote({workspaceId, globalId});

          let saveInstance = <any>await NoteObjRepository.convertSyncInstanceToLocal({
            workspaceId,
            noteObj,
            existItemInstance
          });
          if (!saveInstance) {
            return resolve(false);
          }

          let reminderRemove = false;
          if (saveInstance.reminder) {
            const workspaceInstance = <{globalId:string, title:string}>(workspaceId ? await workspace.getById(workspaceId) : await workspace.getDefault());
            const reminderData = {
              workspaceId: workspaceInstance.globalId,
              workspaceTitle: workspaceInstance.title,
              globalId: saveInstance.globalId,
              parentId: saveInstance.parentId,
              title: saveInstance.title,
              reminder: saveInstance.reminder
            };
            if (existItemInstance && existItemInstance.reminder) {
              const existReminder = existItemInstance.reminder;
              const saveReminder = saveInstance.reminder;
              if ((existReminder.date !== saveReminder.date) ||
                (existReminder.interval !== saveReminder.interval)) {
                ReminderNotice.flushByGlobalId(existItemInstance.globalId);
                ReminderNotice.setTimeoutByGlobalId(reminderData);
              }
            } else {
              ReminderNotice.flushByGlobalId(saveInstance.globalId);
              ReminderNotice.setTimeoutByGlobalId(reminderData);
            }
          } else {
            if (existItemInstance && existItemInstance.reminder) {
              ReminderNotice.flushByGlobalId(existItemInstance.globalId);
              reminderRemove = true;
            }
          }

          saveInstance = NoteObjRepository.updateTrashProperties({
            itemObj: noteObj,
            saveInstance,
            existItemInstance
          });

          let userNoteSaved = null;
          let isCreate = false;
          if (existItemInstance) {
            userNoteSaved = await NoteObjRepository.updateUserNote({workspaceId, item: saveInstance});
            if (userNoteSaved) {
              let rootIdChanged = existItemInstance.rootId != saveInstance.rootId;
              let parentIdChanged = existItemInstance.parentId != saveInstance.parentId;
              if (rootIdChanged || parentIdChanged) {
                NoteObjRepository.sendSocketEventOnUpdate({
                  workspaceId,
                  globalId: existItemInstance.globalId,
                  parentId: existItemInstance.parentId,
                  isCreate,
                  reminderRemove,
                  textVersion: existItemInstance.text_version
                });
              }
            }
          } else {
            userNoteSaved = await NoteObjRepository.createUserNote({
              workspaceId,
              item: saveInstance
            });
            isCreate = true;
          }

          let userNoteTextSaved = null;
          if (userNoteSaved) {
            if(typeof (noteObj.text) !== "undefined") {
              userNoteTextSaved = await NoteObjRepository.saveUserNoteText({
                workspaceId,
                saveInstance,
                saveText: noteObj.text
              });
            }

            if(noteObj.text_version >= 2) {
              userNoteTextSaved = true;
            }
          }

          if (userNoteSaved && typeof (noteObj.todo) !== "undefined") {
            await NoteObjRepository.saveUserNoteTodoList({
              workspaceId,
              saveInstance,
              saveTodoList: noteObj.todo
            });
          }

          if (userNoteSaved && typeof (noteObj.attachements) !== "undefined") {
            await NoteObjRepository.saveUserNoteAttachmentList({
              workspaceId,
              saveInstance,
              saveAttachmentList: noteObj.attachements
            });
          }

          if (userNoteTextSaved) {
            NoteObjRepository.sendSocketEventOnUpdate({
              workspaceId,
              globalId: userNoteSaved.globalId,
              parentId: userNoteSaved.parentId,
              isCreate,
              reminderRemove,
              textVersion: userNoteSaved.text_version
            });
          }

          resolve(userNoteSaved && userNoteTextSaved);
        });
      };

      let result = null;
      if (noteObjs instanceof Array) {
        for (let noteObj of noteObjs) {
          result = await processUserNote({workspaceId, noteObj});
        }
      } else {
        result = await processUserNote({workspaceId, noteObj: noteObjs});
      }

      resolve(result);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @return {Promise<FolderObj|null>}
   */
  static async getUserNote(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId} = inputData;
      itemModel.find({"globalId": globalId, "type": "note"}, {workspaceId}, (err, noteObj) => {
        if (err || !noteObj) {
          return resolve(null);
        }

        resolve(noteObj);
      });
    });
  }

  /**
   * @param {{workspaceId:string, item:{}}} inputData
   */
  static async updateUserNote(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, item} = inputData;

      if (config.SHOW_WEB_CONSOLE) {
        //console.log("update:", item);
      }

      itemModel.update({"globalId": item.globalId}, item, {workspaceId}, async (err, count) => {
        if (err || !count) {
          return resolve(null);
        }

        await NoteObjRepository.saveUserNoteTags({workspaceId, item});
        resolve(!!count ? item : null);
      });
    });
  }

  /**
   * @param {{workspaceId:string, item:{}}} inputData
   */
  static async createUserNote(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, item} = inputData;

      if (config.SHOW_WEB_CONSOLE) {
        //console.log("save:", item);
      }

      /**
       * @param {{}} queryData
       * @return {Promise}
       */
      let getFolderByQuery = async (queryData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          itemModel.find(queryData, {workspaceId}, (err, item) => {
            if (err) {
              return resolve(null);
            }

            resolve(item);
          });
        });
      };

      let queryData = {
        globalId: item.parentId,
        type: 'folder'
      };
      let parentFolder = <{rootId:string}>await getFolderByQuery(queryData);
      if (parentFolder) {
        item.rootId = parentFolder.rootId;
      }

      itemModel.add(item, {workspaceId}, async (err, itemInstance) => {
        if (err || !itemInstance) {
          return resolve(null);
        }

        await NoteObjRepository.saveUserNoteTags({workspaceId, item});
        resolve(!!itemInstance ? item : null);
      });
    });
  }

  /**
   * @param {{itemObj:NoteObj, saveInstance: {{}}, existItemInstance:NoteObj}} inputData
   * @return {{}}
   */
  static updateTrashProperties(inputData) {
    const {itemObj, saveInstance, existItemInstance} = inputData;

    if (itemObj.parentId) {
      saveInstance["parentId"] = itemObj.parentId === 'root' ? 'default' : itemObj.parentId;
      saveInstance["rootId"] = itemObj.parentId === 'trash' ? 'trash' : 'root';
      if (itemObj.parentId !== "trash") {
        saveInstance["rootParentId"] = itemObj.parentId;
      }
    }

    saveInstance.needSync = false;
    if(existItemInstance && existItemInstance.needSync) {
      if(existItemInstance.text_version >= 2) {
        saveInstance.needSync = existItemInstance.needSync;
      }
    }

    return saveInstance;
  }

  /**
   * @param {{workspaceId:string, saveInstance:{}, saveTodoList:[]}} inputData
   */
  static async saveUserNoteTodoList(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, saveInstance, saveTodoList} = inputData;

      for (let i in saveTodoList) {
        if (saveTodoList.hasOwnProperty(i)) {
          saveTodoList[i].noteGlobalId = saveInstance.globalId;
          saveTodoList[i].globalId = saveTodoList[i].global_id;
        }
      }

      let saveTodoGlobalIdList = saveTodoList.map((todoInstance) => {
        return todoInstance.globalId;
      });

      let noteTodoList = <{}>await TodoObjRepository.findNoteTodoList({
        workspaceId,
        noteGlobalId: saveInstance.globalId
      });

      let removeTodoList = [];
      for (let j in noteTodoList) {
        if (noteTodoList.hasOwnProperty(j)) {
          if (saveTodoGlobalIdList.indexOf(noteTodoList[j].globalId) < 0 && !noteTodoList[j].needSync) {
            removeTodoList.push(noteTodoList[j]);
          }
        }
      }

      if (removeTodoList.length) {
        await TodoObjRepository.removeNoteTodoList({workspaceId, todoList: removeTodoList});
      }

      for (let k in noteTodoList) {
        if(!noteTodoList.hasOwnProperty(k)) continue;
        const noteTodoItem = noteTodoList[k];

        for (let n in saveTodoList) {

          if(!saveTodoList.hasOwnProperty(n)) continue;
          const saveTodoItem = saveTodoList[n];

          if(noteTodoItem.globalId !== saveTodoItem.global_id) {
            continue;
          }

          if(noteTodoItem.needSync) {
            saveTodoList.splice(n, 1);
          }
        }
      }

      let result = await TodoObjRepository.update({
        workspaceId,
        todoObjs: saveTodoList
      });

      let todoCount = await TodoObjRepository.getUncheckedCount({
        workspaceId,
        noteGlobalId: saveInstance.globalId
      });

      let todoTotalCount = await TodoObjRepository.getCount({
        workspaceId,
        noteGlobalId: saveInstance.globalId
      });
      await NoteObjRepository.updateUserNoteProps({
        workspaceId,
        globalId: saveInstance.globalId,
        props: {
          todoCount,
          todoExist: !!todoTotalCount
        }
      });

      resolve(result);
    });
  }

  /**
   * @param {{workspaceId:string, saveInstance:{}, saveAttachmentList:[]}} inputData
   */
  static async saveUserNoteAttachmentList(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, saveInstance, saveAttachmentList} = inputData;
      for (let i in saveAttachmentList) {
        saveAttachmentList[i].noteGlobalId = saveInstance.globalId;
        saveAttachmentList[i].globalId = saveAttachmentList[i].global_id;
      }

      let saveAttachmentGlobalIdList = saveAttachmentList.map((attachmentInstance) => {
        return attachmentInstance.globalId;
      });

      let noteAttachmentList = <{}>await AttachmentObjRepository.findNoteAttachmentList({
        workspaceId,
        noteGlobalId: saveInstance.globalId
      });

      let removeAttachmentList = [];
      for (let j in noteAttachmentList) {
        if (noteAttachmentList.hasOwnProperty(j)) {
          if (saveAttachmentGlobalIdList.indexOf(noteAttachmentList[j].globalId) < 0 && !noteAttachmentList[j].needSync) {
            removeAttachmentList.push(noteAttachmentList[j]);
          }
        }
      }

      if (removeAttachmentList.length) {
        await AttachmentObjRepository.removeNoteAttachmentList({
          workspaceId,
          attachmentList: removeAttachmentList
        });
      }

      for (let k in noteAttachmentList) {
        if(!noteAttachmentList.hasOwnProperty(k)) continue;
        const noteAttachmentItem = noteAttachmentList[k];

        for (let n in saveAttachmentList) {

          if(!saveAttachmentList.hasOwnProperty(n)) continue;
          const saveAttachmentItem = saveAttachmentList[n];

          if(noteAttachmentItem.globalId !== saveAttachmentItem.global_id) {
            continue;
          }

          if(noteAttachmentItem.needSync) {
            saveAttachmentList.splice(n, 1);
          }
        }
      }

      let result = await AttachmentObjRepository.update({
        workspaceId,
        attachmentObjs: saveAttachmentList,
        sendSocketEvents: true
      });

      let attachmentsInListCount = await AttachmentObjRepository.getInListCount({
        workspaceId,
        noteGlobalId: saveInstance.globalId
      });

      await NoteObjRepository.updateUserNoteProps({
        workspaceId,
        globalId: saveInstance.globalId,
        props: {
          attachmentsInListCount,
          attachmentsInListExist: !!attachmentsInListCount
        }
      });

      resolve(result);
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string, props:{}}} inputData
   */
  static async updateUserNoteProps(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, globalId, props} = inputData;
      if(!globalId || !props) {
        return resolve(false);
      }

      itemModel.update({globalId}, props, {workspaceId}, async (err, count) => {
        if (err || !count) {
          return resolve(false);
        }
        return resolve(true);
      });
    });
  }

  /**
   * @param {{workspaceId:string, saveInstance:{}, saveText:string}} inputData
   * @return {Promise<boolean>}
   */
  static async saveUserNoteText(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, saveInstance, saveText} = inputData;

      let getUserNoteText = async (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {workspaceId, noteGlobalId} = inputData;
          textModel.find({noteGlobalId: noteGlobalId}, {workspaceId}, (err, userNoteText) => {
            if (err || !userNoteText) {
              return resolve(null);
            }

            resolve(userNoteText);
          });
        });
      };

      /**
       * @param {{workspaceId:string, item:NoteObj, userNoteText:{}, saveText:string}} inputData
       * @return {Promise<boolean>}
       */
      let updateUserNoteText = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, item, userNoteText, saveText} = inputData;
          let userNoteTextData = {
            noteGlobalId: userNoteText.noteGlobalId,
            shortText: item.shortText,
            needSync: false
          };
          let noteTextInstance = textModel.prepareSyncOnlyProperties(userNoteText, userNoteTextData);
          noteTextInstance.textShort = item.shortText;
          noteTextInstance.text = saveText ? saveText : item.shortText;
          if (item.text_version) {
            noteTextInstance.text_version = item.text_version;
          }

          textModel.update({noteGlobalId: item.globalId}, noteTextInstance, {workspaceId}, (err, count) => {
            if (err || !count) {
              return resolve(false);
            }

            NoteObjRepository.sendSocketTextEventOnUpdate({
              workspaceId,
              globalId: userNoteText.noteGlobalId,
              textVersion: item.text_version
            });

            resolve(!!count);
          });
        });
      };

      /**
       * @param {{workspaceId:string, item:NoteObj, saveText:string}} inputData
       * @return {Promise<any>}
       */
      let createUserNoteText = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, item, saveText} = inputData;

          let userNoteText = textModel.getDefaultModel(item.globalId);

          let userNoteTextData = {
            noteGlobalId: item.globalId,
            shortText: item.shortText,
            needSync: false,
          };

          let noteTextInstance = textModel.prepareSyncOnlyProperties(userNoteText, userNoteTextData);
          noteTextInstance.textShort = item.shortText;
          noteTextInstance.text = saveText ? saveText : item.shortText;
          if (item.text_version) {
            noteTextInstance.text_version = item.text_version;
          }
          textModel.add(noteTextInstance, {workspaceId}, (err, itemInstance) => {
            if (err || !itemInstance) {
              return resolve(false);
            }

            NoteObjRepository.sendSocketTextEventOnUpdate({
              workspaceId,
              globalId: userNoteTextData.noteGlobalId,
              textVersion: item.text_version
            });

            resolve(!!itemInstance);
          });
        });
      };

      let userNoteTextSaved = null;
      let existUserNoteText = await getUserNoteText({workspaceId, noteGlobalId: saveInstance.globalId});
      if (existUserNoteText) {
        userNoteTextSaved = await updateUserNoteText({
          workspaceId,
          item: saveInstance,
          userNoteText: existUserNoteText,
          saveText
        });
      } else {
        userNoteTextSaved = await createUserNoteText({
          workspaceId,
          item: saveInstance,
          saveText
        });
      }

      resolve(userNoteTextSaved);
    });
  }

  /**
   * @param {{workspaceId:string, item:{}}} inputData
   */
  // @ts-ignore
  static async saveUserNoteTags(inputData) {
    const {workspaceId, item} = inputData;
    let tagList = typeof (item.tags) == "string" && item.tags ? JSON.parse(item.tags) : item.tags;
    if (!tagList) {
      tagList = [];
    }

    /**
     * @param {string} noteGlobalId
     * @param {[]} tagList
     * @return {Promise}
     */
    let refreshUserItemNoteTags = async (noteGlobalId, tagList) => {
      // @ts-ignore
      return new Promise((resolve) => {
        noteTagsModel.findAll({
          noteGlobalId: noteGlobalId,
          //needSync: false
        }, {workspaceId}, (err, userNoteTags) => {
          if (err || !userNoteTags) {
            return resolve([]);
          }

          for (let userNoteTag of userNoteTags) {
            tagModel.find({globalId: userNoteTag.tagGlobalId}, {workspaceId}, (err, userTag) => {
              if (userTag) {
                if (tagList.indexOf(userTag.tag) < 0) {
                  noteTagsModel.remove({
                    tagGlobalId: userTag.globalId,
                    noteGlobalId: noteGlobalId
                  }, {workspaceId}, (err, count) => {
                    TagObjRepository.emitSocketEventForNote({
                      workspaceId,
                      tag: userTag.tag,
                      noteGlobalId,
                      action: "remove"
                    });
                  });
                }
              }
            });
          }

          resolve(tagList);

        });
      });
    };

    await refreshUserItemNoteTags(item.globalId, tagList);

    if (tagList && tagList.length) {

      /**
       * @param {string} tag
       * @return {Promise<tagModel|null>}
       */
      let getUserTag = async (tag) => {
        // @ts-ignore
        return new Promise((resolve) => {
          tagModel.find({"tag": tag}, {workspaceId}, (err, tagItem) => {
            if (err || !tagItem) {
              return resolve(null);
            }
            resolve(tagItem);
          });
        });
      };

      /**
       * @param {{workspaceId:string, tag:string}} inputData
       */
      let checkUserTagRemoved = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          TagObjRepository.getErasedTagForUploadOnServer(inputData, (err, tagObj) => {
            if (err) {
              return resolve(false);
            }

            resolve(!!tagObj);
          });
        });
      };

      /**
       * @param {tag} tagObj
       * @return {Promise<tag|null>}
       */
      let addUserTag = async (tagObj) => {
        // @ts-ignore
        return new Promise((resolve) => {
          let fillModelProperties = (item, data) => {
            return tagModel.prepareCommonProperties(tagModel.prepareItemDbProperties(item, data), data);
          };

          let tagInstance = fillModelProperties(tagModel.prepareModelData(tagObj), tagObj);
          tagModel.add(tagInstance, {workspaceId}, (err, result) => {
            if (err || !result) {
              return resolve(null);
            }

            resolve(tagInstance);
          });
        });
      };

      /**
       * @param {tag} tagObj
       * @return {Promise<tag|null>}
       */
      let updateUserTag = async (tagObj) => {
        // @ts-ignore
        return new Promise((resolve) => {
          let fillSyncObjProperties = (item, data) => {
            return tagModel.prepareSyncOnlyProperties(tagModel.prepareItemDbProperties(item, data), data);
          };

          let tagInstance = fillSyncObjProperties(tagModel.prepareModelData(tagObj), tagObj);
          tagModel.update({tag: tagInstance.tag}, tagInstance, {workspaceId}, (err, count) => {
            if (err || !count) {
              return resolve(null);
            }

            TagObjRepository.emitSocketEventForNote({
              workspaceId,
              tag: tagObj.tag,
              noteGlobalId: tagObj.noteGlobalId,
              action: "update"
            });

            resolve(tagInstance);
          });
        });
      };

      /**
       * @param {string} noteGlobalId
       * @param {string} tagGlobalId
       * @param {string} tag
       * @return {Promise<noteTagsModel|null>}
       */
      let processUserNoteTag = async (noteGlobalId, tagGlobalId, tag) => {
        // @ts-ignore
        return new Promise((resolve) => {
          noteTagsModel.find({
            noteGlobalId: noteGlobalId,
            tagGlobalId: tagGlobalId
          }, {workspaceId}, (err, userNoteTag) => {
            if (err) {
              return resolve(false);
            }

            if (userNoteTag) {
              return resolve(true);
            }

            let userNoteTagData = noteTagsModel.prepareModelData({
              tagGlobalId: tagGlobalId,
              noteGlobalId: noteGlobalId,
              needSync: false
            });

            noteTagsModel.add(userNoteTagData, {workspaceId}, (err, result) => {
              if (err || !result) {
                return resolve(false);
              }

              TagObjRepository.emitSocketEventForNote({
                workspaceId,
                tag,
                noteGlobalId,
                action: "add"
              });

              resolve(true);
            });
          });
        });
      };

      let noteTagsList = <{}>await TagObjRepository.findNoteTagsList({
        workspaceId,
        noteGlobalId: item.globalId
      });

      let removeTagsList = [];
      for (let j in noteTagsList) {
        if (noteTagsList.hasOwnProperty(j)) {
          if (tagList.indexOf(noteTagsList[j].globalId) < 0 && !noteTagsList[j].needSync) {
            removeTagsList.push(noteTagsList[j]);
          }
        }
      }

      if (removeTagsList.length) {
        await TagObjRepository.removeNoteTagList({
          workspaceId,
          tagList: removeTagsList
        });
      }

      for (let k in noteTagsList) {
        if(!noteTagsList.hasOwnProperty(k)) continue;
        const noteTagItem = noteTagsList[k];

        for (let n in tagList) {

          if(!tagList.hasOwnProperty(n)) continue;
          const saveTag = tagList[n];

          if(noteTagItem.tag !== saveTag) {
            continue;
          }

          if(noteTagItem.needSync) {
            tagList.splice(n, 1);
          }
        }
      }

      for (let tag of tagList) {
        let userTag = null;

        let tagItem = await getUserTag(tag);
        if (tagItem) {
          userTag = await updateUserTag(tagItem);
        } else {
          userTag = await addUserTag(tagModel.prepareModelData({tag}));
        }

        if (userTag) {
          await processUserNoteTag(item.globalId, userTag.globalId, tag);
        }
      }
    }
  }

  /**
   * @param {{workspaceId:string, globalId:string, parentId:string, isCreate:boolean, reminderRemove:boolean, textVersion:number}} inputData
   */
  static sendSocketEventOnUpdate(inputData) {
    const {workspaceId, globalId, parentId, isCreate, reminderRemove, textVersion} = inputData;
    socketConnection.addStoreData({
      workspaceId,
      storeType: socketStoreType.NOTES_FOR_UPDATE,
      data: {
        globalId: globalId,
        parentId: parentId,
        isCreate: isCreate,
        type: 'note',
        reminderRemove
      }
    });

    NoteObjRepository.sendSocketTextEventOnUpdate({
      workspaceId,
      globalId,
      textVersion
    });

    socketConnection.addStoreData({
      workspaceId,
      storeType: socketStoreType.NOTES_FOR_UPDATE_COUNTERS,
      data: {
        globalId: globalId,
        parentId: parentId,
        type: 'note'
      }
    });
  }

  /**
   * @param {{workspaceId:string, globalIdList:[string]}} inputData
   */
  static sendSocketEventOnRemove(inputData) {
    const {workspaceId, globalIdList} = inputData;
    if (globalIdList && globalIdList.length) {
      socketConnection.addStoreData({
        workspaceId,
        storeType: socketStoreType.NOTES_FOR_REMOVE,
        data: {
          globalId: globalIdList
        }
      });
    }
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static sendSocketTextEventOnUpdate(inputData) {
    const {workspaceId, globalId, textVersion} = inputData;
    socketConnection.addStoreData({
      workspaceId,
      storeType: socketStoreType.NOTES_FOR_UPDATE_TEXT,
      data: {
        globalId,
        textVersion
      }
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string, parentId:string, type:string}} inputData
   */
  static sendSocketEventOnCounter(inputData) {
    const {workspaceId, globalId, parentId, type} = inputData;
    socketConnection.addStoreData({
      workspaceId,
      storeType: socketStoreType.NOTES_FOR_UPDATE_COUNTERS,
      data: {
        globalId: globalId,
        parentId: parentId,
        type: type
      }
    });
  }

  /**
   * @param {{workspaceId:string, noteObj:NoteObj, existItemInstance:boolean}}
   * @return {Promise<item|null>}
   */
  static async convertSyncInstanceToLocal(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, noteObj, existItemInstance} = inputData;

      if (!noteObj) {
        return resolve(null);
      }

      let fillSyncObjProperties = (item, data) => {
        return itemModel.prepareNoteOnlyProperties(itemModel.prepareItemDbProperties(item, data), data);
      };

      let fillModelProperties = (item, data) => {
        item.role = data.role || "note";
        return itemModel.prepareCommonProperties(itemModel.prepareItemDbProperties(item, data), data);
      };

      let itemInstance = null;

      if (existItemInstance) {
        itemInstance = fillSyncObjProperties(itemModel.prepareModelData(existItemInstance), noteObj);
        itemInstance = itemModel.changeDateUpdate(itemInstance, noteObj);
        if (itemInstance.title !== noteObj.title) {
          itemInstance.title = noteObj.title;
        }
      } else {
        itemInstance = fillModelProperties(itemModel.prepareModelData(noteObj), noteObj);
      }

      resolve(itemInstance);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static clearAllMoreThanLimitInNotesI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({
      "type": "note",
      "isMoreThanLimit": true
    }, {workspaceId}, async (err, noteObjs) => {
      if (!noteObjs.length) {
        return callback(err, false);
      }

      for (let noteObj of noteObjs) {
        noteObj.isMoreThanLimit = false;
      }

      let result = await NoteObjRepository.update({workspaceId, noteObjs});
      callback(null, result);

    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static isAvailableNotesForSync(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.count({
      "type": "note",
      "isTemp": false,
      "needSync": true,
      "isMoreThanLimit": false
    }, {workspaceId}, (err, count) => {
      callback(err, count !== 0);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getErasedFromTrashNotesForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({
      "type": "note",
      "erised": true,
      "offlineOnly": {"$ne": true},
      "needSync": true
    }, {workspaceId}, (err, noteObjs) => {
      let notes = [];
      for (let noteObj of noteObjs) {
        notes.push(noteObj.globalId);
      }
      callback(err, notes);
    });
  };

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getUpdatedHeaderNotesForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({
      "type": "note",
      "parentId": {"$ne": FolderObj.ERASED_FROM_TRASH},
      "offlineOnly": {"$ne": true},
      "needSync": true,
      //"isDownloaded": false
    }, {
      workspaceId,
      "order": {"dateUpdated": "desc"}
      // @ts-ignore
    }, async (err, noteObjs) => {
      let notes = [];
      for (let noteObj of noteObjs) {
        let syncNote = new SyncHeaderNoteUpdateEntity();
        syncNote.global_id = noteObj.globalId;
        syncNote.parent_id = noteObj.parentId;
        syncNote.root_parent_id = noteObj.rootParentId;
        syncNote.tags = <[string]>await NoteObjRepository.getNoteTagsList({workspaceId, globalId: noteObj.globalId});
        // @ts-ignore
        syncNote.shared = parseInt(noteObj.shared);
        syncNote.date_added_user = noteObj.dateAdded;
        syncNote.date_updated_user = noteObj.dateUpdated;
        notes.push(syncNote);
      }
      callback(err, notes);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @return {Promise<[]>}
   */
  static async getNoteTagsList(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId} = inputData;
      noteTagsModel.findAll({"noteGlobalId": globalId}, {workspaceId}, (err, noteTagObjs) => {
        if (err) {
          resolve([]);
        }

        let tagGlobalIdList = [];
        for (let noteTagObj of noteTagObjs) {
          tagGlobalIdList.push(noteTagObj.tagGlobalId);
        }

        if (!tagGlobalIdList.length) {
          return resolve([]);
        }

        tagModel.findAll({"globalId": {"$in": tagGlobalIdList}}, {workspaceId}, (err, tagObjs) => {
          if (err) {
            resolve([]);
          }

          let tagObjTitleList = [];
          for (let tagObj of tagObjs) {
            tagObjTitleList.push(tagObj.tag);
          }

          resolve(tagObjTitleList);
        });
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getAvailableNoteForDownloadFromServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    let globalId = null;
    itemModel.findAll({
      "type": "note",
      "needSync": false,
      "isTemp": false,
      "isEncrypted": 0,
      "parentId": {"$nin": [FolderObj.TRASH, FolderObj.ERASED_FROM_TRASH]},
      "isDownloaded": false
    }, {
      workspaceId,
      "order": {"dateUpdated": "desc"}
    }, (err, noteObjs) => {
      if (noteObjs.length > 0) {
        globalId = noteObjs[0].globalId;
      }
      callback(err, globalId);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getUpdatedNotesForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({
      "type": "note",
      "offlineOnly": {"$ne": true},
      "needSync": true,
      "globalId": {"$ne": "default"}
    }, {
      workspaceId,
      "order": {"dateUpdated": "desc"}
    }, (err, noteObjs) => {
      if (err) {
        return callback(err, []);
      }

      callback(err, noteObjs);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getUpdatedFullNotesCountForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({
      "type": "note",
      "offlineOnly": {"$ne": true},
      "needSync": true
    }, {
      workspaceId,
      "order": {"dateUpdated": "desc"}
    }, (err, noteObjs) => {
      callback(err, noteObjs.length);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getAvailableNotesCountForDownloadFromServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({
      "type": "note"
    }, {workspaceId}, (err, noteObjs) => {
      callback(err, noteObjs.length);
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @param {Function} callback
   */
  static getSyncNoteEntity(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, noteGlobalId} = inputData;
    // @ts-ignore
    itemModel.find({"globalId": noteGlobalId, "type": "note"}, {workspaceId}, async (err, noteObj) => {
      let note = <any>new SyncFullNoteUpdateEntity();
      if (NoteObjRepository.isValid(noteObj)) {
        note.global_id = noteObj.globalId;
        note.parent_id = noteObj.parentId;
        note.root_parent_id = noteObj.rootParentId;
        note.index = noteObj.index;
        note.date_added_user = noteObj.dateAdded;
        note.date_updated_user = noteObj.dateUpdated;
        note.type = noteObj.type;
        note.title = noteObj.title;
        note.location_lat = NoteObj.getLocationLat(noteObj);
        note.location_lng = NoteObj.getLocationLng(noteObj);
        note.shared = noteObj.shared ? 1 : 0;
        note.role = noteObj.role;
        note.editnote = noteObj.editnote ? 1 : 0;
        note.color = noteObj.color ? noteObj.color : '';
        note.url = noteObj.url;
        note.favorite = noteObj.favorite ? 1 : 0;
        note.is_encrypted = noteObj.isEncrypted ? 1 : 0;
        note.is_fullwidth = noteObj.isFullwidth ? 1 : 0;
        note.text_version = 1;
        if(noteObj.text_version) {
          note.text_version = noteObj.text_version;
        }

        if(noteObj.text_version < 2) {
          let textItem = await NoteObjRepository.getTextItemForNote({workspaceId, noteGlobalId: noteObj.globalId});
          if (textItem && textItem['text']) {
            note.text = textItem['text'];
            note.text_short = textItem['textShort'];
          } else {
            note.text = textModel.getDefaultNoteText();
            note.text_short = "";
          }
        } else if(noteObj.text_version >= 2) {
          delete note.text;
          delete note.text_short;
          note.noteObj = noteObj;
        }
        note.tags = await NoteObjRepository.getTagsForNote({workspaceId, noteGlobalId: noteObj.globalId});
        note.preview = noteObj.preview ? noteObj.preview : null;
        note.reminder = noteObj.reminder ? noteObj.reminder : null;
        if(note.reminder && !note.reminder.phone) {
          note.reminder.phone = '';
        }
      }
      callback(err, note);
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @return {Promise<[]>}
   */
  static async getTagsForNote(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, noteGlobalId} = inputData;

      if (!noteGlobalId) {
        return resolve([]);
      }

      noteTagsModel.findAll({noteGlobalId: noteGlobalId}, {workspaceId}, (err, noteTagItems) => {
        if (err) {
          return resolve([]);
        }

        let noteTagItemsIdList = [];
        for (let noteTagItem of noteTagItems) {
          noteTagItemsIdList.push(noteTagItem.tagGlobalId);
        }

        tagModel.findAll({
          "globalId": {"$in": noteTagItemsIdList}
        }, {workspaceId}, (err, tagObjs) => {
          if (err) {
            return resolve([]);
          }

          let tags = [];
          for (let tagObj of tagObjs) {
            tags.push(tagObj.title);
          }

          resolve(tags);
        });
      });
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @return {Promise<string>}
   */
  static async getTextItemForNote(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, noteGlobalId} = inputData;

      if (!noteGlobalId) {
        return resolve(null);
      }

      textModel.find({noteGlobalId: noteGlobalId}, {workspaceId}, (err, textItem) => {
        if (err) {
          return resolve(null);
        }

        resolve(textItem);
      });
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @param {Function} callback
   */
  static checkIfNoteCanBePassedInAvailableTrafficQuotaForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    NoteObjRepository.getNeedForUploadNoteSizeInBytes(inputData, (err, noteSizeInBytes) => {
      let accountManager = NimbusSDK.getAccountManager();
      accountManager.getTrafficMax(function (err, trafficMax) {
        accountManager.getTrafficCurrent(function (err, trafficCurrent) {
          let availableTraffic = trafficMax - trafficCurrent;
          callback(err, availableTraffic >= noteSizeInBytes);
        });
      });
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @param {Function} callback
   */
  static getNeedForUploadNoteSizeInBytes(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, noteGlobalId} = inputData;
    let size = 0;
    // @ts-ignore
    NoteObjRepository.get({workspaceId, globalId: noteGlobalId}, async (err, noteObj) => {
      if (NoteObjRepository.isValid(noteObj)) {
        let noteTextItem = await NoteObjRepository.getTextItemForNote({workspaceId, noteGlobalId: noteObj.globalId});
        if (noteTextItem && noteTextItem['text']) {
          size += Buffer.byteLength(noteTextItem['text'], 'utf8');
        }
        AttachmentObjRepository.getNoteAttachmentsForUploadSizeInBytes(inputData, (err, attachmentSize) => {
          size += attachmentSize;
          callback(err, size);
        });
      } else {
        callback(err, 0);
      }
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   */
  static async getNeedForUploadNoteSizeInBytesAsync(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      NoteObjRepository.getNeedForUploadNoteSizeInBytes(inputData, (err, res) => {
        resolve(res);
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<number>}
   */
  static async getUploadNotesTraffic(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId} = inputData;

      NoteObjRepository.getUpdatedNotesForUploadOnServer(inputData, async (err, noteObjs) => {
        if (err || !noteObjs) {
          return resolve(0);
        }

        if (!noteObjs.length) {
          return resolve(0);
        }

        let size = 0;
        for (let noteObj of noteObjs) {
          size += <number>await NoteObjRepository.getNeedForUploadNoteSizeInBytesAsync({
            workspaceId,
            noteGlobalId: noteObj.globalId
          });
        }
        resolve(size);
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<number>}
   */
  static async getTotalTrafficAfterSync(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId} = inputData;
      let accountManager = NimbusSDK.getAccountManager();
      // @ts-ignore
      accountManager.getTrafficCurrent(async (err, trafficCurrent) => {
        let orgTraffic;
        let totalTraffic = 0;
        let uploadTraffic = <number>await NoteObjRepository.getUploadNotesTraffic(inputData);

        const orgInstance = <any>await orgs.getByWorkspaceId(workspaceId);
        if (orgInstance && orgInstance.usage && orgInstance.usage.traffic) {
          orgTraffic = orgInstance.usage.traffic;
          trafficCurrent = orgTraffic.current;
        }

        if (trafficCurrent) {
          totalTraffic = trafficCurrent;
        }

        if (uploadTraffic) {
          totalTraffic += uploadTraffic;
        }

        resolve({
          orgTraffic: orgTraffic,
          totalTrafficAfterSync: uploadTraffic + trafficCurrent
        });
      });
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @param {Function} callback
   */
  static checkIfNoteNotMoreThanLimitNoteSize(inputData, callback = (err, res) => {
  }) {
    NoteObjRepository.getNeedForUploadNoteSizeInBytes(inputData, (err, noteSizeinBytes) => {
      NimbusSDK.getAccountManager().getLimitNotesMaxSize((err, maxSize) => {
        callback(err, maxSize >= noteSizeinBytes);
      });
    });
  }

  /**
   * @param {{workspaceId:string, noteObj:NoteObj}} inputData
   * @param {Function} callback
   */
  static async updateFullNoteDownloadedFromServerI(inputData, callback = (err, res) => {
  }) {
    const {noteObj} = inputData;
    if (!noteObj) {
      return callback(null, false);
    }
    let result = await NoteObjRepository.update(inputData);
    callback(null, result);
  }

  /**
   * @param {{workspaceId:string, noteObjs:[AbstractNote]}} inputData
   * @param {Function} callback
   */
  static async updateStructureNotesDownloadedFromServerI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, noteObjs} = inputData;
    if (!noteObjs.length) {
      return callback(null, false);
    }

    let result = await NoteObjRepository.update({workspaceId, noteObjs});
    callback(null, result);
  }

  /**
   * @param {{workspaceId:string, notes:[string]}} inputData
   * @param {Function} callback
   */
  static deleteRemovedItemsDownloadedFromServerI(inputData, callback = (err, res) => {
  }) {
    NoteObjRepository.deleteNotesAndAllDataFromDevice(inputData, (err, result) => {
      callback(err, true);
    });
  }

  /**
   * @param {{workspaceId:string, globalIds:[string]}} inputData
   * @param {Function} callback
   */
  // @ts-ignore
  static async deleteNotesAndAllDataFromDevice(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalIds} = inputData;
    if (globalIds && globalIds.length) {
      let deleteUserNote = async (globalId) => {
        // @ts-ignore
        return new Promise((resolve) => {
          let findQuery = {"globalId": globalId, "type": "note"};
          itemModel.find(findQuery, {workspaceId}, (err, itemInstance) => {
            itemModel.erase(findQuery, {workspaceId}, (err, numRemoved) => {
              if (err || !numRemoved) {
                return resolve(null);
              }

              resolve(itemInstance);
            });
          });
        });
      };

      let removeIdList = [];
      for (let globalId of globalIds) {
        let itemInstance = <any>await deleteUserNote(globalId);
        if (itemInstance) {
          removeIdList.push(itemInstance.globalId);
          NoteObjRepository.sendSocketEventOnCounter({
            workspaceId,
            globalId: itemInstance.globalId,
            parentId: itemInstance.parentId,
            type: itemInstance.type
          });
        }
      }

      NoteObjRepository.sendSocketEventOnRemove({
        workspaceId,
        globalIdList: removeIdList
      });
    }

    callback(null, true);
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static callAfterUploadErasedFromTrashNotesOnServerI(inputData, callback = (err, res) => {
  }) {
    NoteObjRepository.deleteFolderNotesAndAllDataFromDevice(inputData, (err, result) => {
      callback(err, true);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static deleteFolderNotesAndAllDataFromDevice(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId} = inputData
    let queryData = {"type": "note", "erised": true};
    if (globalId) {
      // @ts-ignore
      queryData = {"globalId": globalId};
    }

    // @ts-ignore
    itemModel.findAll(queryData, {workspaceId}, async (err, itemObjs) => {
      let deleteItemAndAllRelatedData = async (itemObj) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          let eraseItem = async (itemObj) => {
            // @ts-ignore
            return new Promise((resolve) => {
              // @ts-ignore
              itemModel.erase({"globalId": itemObj.globalId}, {workspaceId}, async (err, result) => {
                resolve(true);
              });
            });
          };

          let eraseNoteTodos = async (noteObj) => {
            // @ts-ignore
            return new Promise((resolve) => {
              todoModel.erase({"noteGlobalId": noteObj.globalId}, {workspaceId}, (err, result) => {
                resolve(true);
              });
            });
          };

          let eraseNoteTags = async (noteObj) => {
            // @ts-ignore
            return new Promise((resolve) => {
              noteTagsModel.erase({"noteGlobalId": noteObj.globalId}, {workspaceId}, (err, result) => {
                resolve(true);
              });
            });
          };

          let eraseNoteText = async (noteObj) => {
            // @ts-ignore
            return new Promise((resolve) => {
              textModel.erase({"noteGlobalId": noteObj.globalId}, {workspaceId}, (err, result) => {
                resolve(true);
              });
            });
          };

          /**
           * @param {{workspaceId:string, noteObj:NoteObj}} inputData
           */
          let eraseNoteAttachments = async (inputData) => {
            // @ts-ignore
            return new Promise((resolve) => {
              const {workspaceId, noteObj} = inputData;
              attachModel.findAll({"noteGlobalId": noteObj.globalId}, {workspaceId}, async (err, attachObjs) => {
                if (err) {
                  return resolve(false);
                }

                /**
                 * @param {{workspaceId:string, attachmentObj:AttachmentObj}} inputData
                 */
                let deleteUserAttachment = async (inputData) => {
                  // @ts-ignore
                  return new Promise((resolve) => {
                    const {workspaceId, attachmentObj} = inputData;
                    attachModel.erase({
                      "globalId": attachmentObj.globalId
                    }, {workspaceId}, (err, response) => {
                      AttachmentObjRepository.deleteAttachmentFile(AttachmentObj.getLocalPath(attachmentObj), (err, response) => {
                        resolve(response);
                      });
                    });
                  });
                };

                for (let attachmentObj of attachObjs) {
                  await deleteUserAttachment({workspaceId, attachmentObj});
                }

                resolve(true);
              });
            });
          };

          if (itemObj.type === "note") {
            await eraseNoteTodos(itemObj);
            await eraseNoteTags(itemObj);
            await eraseNoteText(itemObj);
            await eraseNoteAttachments({workspaceId, noteObj: itemObj});
          }
          await eraseItem(itemObj);

          resolve(true);
        });
      };

      let removeIdList = [];
      for (let itemInstance of itemObjs) {
        await deleteItemAndAllRelatedData(itemInstance);
        removeIdList.push(itemInstance.globalId);
        NoteObjRepository.sendSocketEventOnCounter({
          workspaceId,
          globalId: itemInstance.globalId,
          parentId: itemInstance.parentId,
          type: itemInstance.type
        });
      }

      NoteObjRepository.sendSocketEventOnRemove({
        workspaceId,
        globalIdList: removeIdList
      });

      callback(null, true);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getUserNotesR(inputData, callback = (err, res) => {
  }) {
    NoteObjRepository.getAllNotesR(inputData, (err, list) => {
      callback(err, list);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getAllNotesR(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({"type": "note"}, {workspaceId}, (err, list) => {
      return callback(err, list);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static getR(inputData, callback = (err, res) => {
  }) {
    let {workspaceId, globalId} = inputData;
    if (globalId === null) {
      globalId = "";
    }

    itemModel.find({"globalId": globalId, "type": "note"}, {workspaceId}, (err, item) => {
      callback(err, item);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static callAfterUploadUpdatedHeaderNotesOnServerI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({
      "type": "note",
      "parentId": {"$ne": FolderObj.ERASED_FROM_TRASH},
      "needSync": true
    }, {workspaceId}, async (err, noteObjs) => {
      if (err) {
        return callback(err, false);
      }

      for (let noteObj of noteObjs) {
        noteObj.needSync = false;
      }

      callback(null, await NoteObjRepository.update({workspaceId, noteObjs}));
    });
  }

  /**
   * @param {{workspaceId:string, note:NoteObj, lastUpdateTime:int, syncStartDate:int}} inputData
   * @param {Function} callback
   */
  static callAfterUploadFullSyncI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, note, lastUpdateTime, syncStartDate} = inputData;
    // @ts-ignore
    NoteObjRepository.getR({workspaceId, globalId: note.global_id}, async (err, noteObj) => {
      if (err) {
        callback(null, null);
      }

      if (!NoteObjRepository.isValid(noteObj)) {
        return callback(null, null);
      }

      const updateSyncProp = noteObj.syncDate <= syncStartDate;

      noteObj.tags = note.tags;
      let existItemInstance = await NoteObjRepository.getUserNote({workspaceId, globalId: noteObj.globalId});
      let saveInstance = await NoteObjRepository.convertSyncInstanceToLocal({
        workspaceId,
        noteObj,
        existItemInstance
      });
      if (!saveInstance) {
        return callback(null, false);
      }

      saveInstance['isDownloaded'] = true;
      saveInstance['existOnServer'] = true;

      if(updateSyncProp) {
        saveInstance['needSync'] = false;
        saveInstance['syncDate'] = lastUpdateTime;
      }

      let result = await NoteObjRepository.updateUserNote({workspaceId, item: saveInstance});
      if(result) {
        let noteTodoList = <any>await TodoObjRepository.findNoteTodoList({
          workspaceId,
          noteGlobalId: noteObj.globalId
        });
        for(let noteTodo of noteTodoList) {
          if (noteTodo.syncDate > syncStartDate) {
            continue;
          }
          await TodoObjRepository.updateNeedSync({
            workspaceId,
            globalId: noteTodo.globalId,
            noteGlobalId: noteTodo.noteGlobalId
          });
        }

        let noteAttachmentList = <any>await AttachmentObjRepository.findNoteAttachmentList({
          workspaceId,
          noteGlobalId: noteObj.globalId
        });

        for(let noteAttachment of noteAttachmentList) {
          if (noteAttachment.syncDate > syncStartDate) {
            continue;
          }
          await AttachmentObjRepository.updateNeedSync({
            workspaceId,
            globalId: noteAttachment.globalId,
            noteGlobalId: noteAttachment.noteGlobalId,
            displayName: noteAttachment.displayName
          });
        }

        let noteTagList = <any>await TagObjRepository.findNoteTagList({
          workspaceId,
          noteGlobalId: noteObj.globalId
        });

        for(let noteTag of noteTagList) {
          if (noteTag.syncDate > syncStartDate) {
            continue;
          }
          await TagObjRepository.updateNeedSync({
            workspaceId,
            globalId: noteTag.globalId,
            noteGlobalId: noteTag.noteGlobalId,
            tag: noteTag.tag
          });
        }

      }
      if(noteObj.text_version >= 2) {
        TextEditor.updateSyncedStatus({noteId: noteObj.globalId});
      }
      callback(null, result);
    });
  }

  /**
   * @param {NoteObj} noteObj
   * @return {boolean}
   */
  static isValid(noteObj) {
    return !!noteObj;
  }

  /**
   * @param {{workspaceId:string, parentId:string}} inputData
   * @param {Function} callback
   */
  static checkIfNoteInTrash(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, parentId} = inputData;
    let isInTrash = false;

    if (FolderObj.TRASH === parentId) {
      return callback(null, true);
    }

    //TODO: check folder or note in trash recursive
    FolderObjRepository.get({workspaceId, globalId: parentId}, (err, folderObj) => {
      // if (folderObj) {
      //     while (true) {
      //         if (folderObj === null) {
      //             break;
      //         }
      //         if (FolderObj.TRASH === folderObj.parentId) {
      //             isInTrash = true;
      //             break;
      //         }
      //         if (FolderObj.GOD === folderObj.parentId || FolderObj.ROOT === folderObj.parentId) {
      //             break;
      //         }
      //         //TODO: improve nested tree check note in trash
      //         //FolderObjRepository.get({ workspaceId, globalId: folderObj.parentId }, callback);
      //     }
      // }

      isInTrash = false;
      return callback(err, isInTrash);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async deleteTodoData(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId} = inputData;

      textModel.erase({
        "noteGlobalId": globalId,
        "type": "note"
      }, {workspaceId}, (err, response) => {
        if (err) {
          return resolve(null);
        }

        itemModel.erase({
          "globalId": globalId,
          "type": "note"
        }, {workspaceId}, (err, response) => {
          if (err) {
            return resolve(null);
          }

          resolve(response);
        });
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param callback
   */
  static clearRemovedData(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({
      "erised": true,
      "type": "note"
      // @ts-ignore
    }, {workspaceId}, async (err, noteObjs) => {
      for (let noteObj of noteObjs) {
        await NoteObjRepository.deleteTodoData({workspaceId, globalId: noteObj.globalId});
      }
      callback(null, true);
    });
  }

  static removeItemPreview(inputData) {
    return new Promise(resolve => {
      const { workspaceId, globalId } = inputData;

      itemModel.find({globalId}, {workspaceId}, (err, itemObj) => {
        if(err || !itemObj) {
          return resolve(false);
        }

        itemModel.update({
          globalId,
          type: 'note',
        }, {preview: null},{workspaceId}, () => {
          return resolve(true);
        });
      });
    });
  }

  static cleanUploadItem(note) {
    if(!note) { return note }

    const cleanProps = [
      'erised',
      'rootId',
      'globalId',
      '_id',
      'parentId',
      'rootParentId',
      'createdAt',
      'dateAdded',
      'dateUpdated',
      'updatedAt',
      'dateUpdatedUser',
      'locationLat',
      'locationLng',
      'lastChangeBy',
      'size',
      'isEncrypted',
      'offlineOnly',
      'shared',
      'passwordRequired',
      'shareId',
      'securityKey',
      'isFullwidth',
      'existOnServer',
      'uniqueUserName',
      'syncDate',
      'needSync',
      'isMaybeInTrash',
      'isClicked',
      'subfoldersCount',
      'notesCount',
      'level',
      'isTemp',
      'shortText',
      'firstImage',
      'isMoreThanLimit',
      'textAttachmentGlobalId',
      'attachmentsInListCount',
      'attachmentsInListExist',
      'reminderExist',
      'todoExist',
      'reminderLabel',
      'todoCount',
      'locationAddress',
      'isLocationExist',
      'isDownloaded',
      'noteObj',
    ]

    for(let prop in note) {
      if(!note.hasOwnProperty(prop)) { continue }

      if(prop && prop.indexOf('UpdateTime') > 0) {
        delete note[prop]
        continue
      }

      if(cleanProps.indexOf(prop) >= 0) {
        delete note[prop]
      }

      if(prop === 'attachements' && note[prop]) {
        note[prop] = note[prop].map(attachment => AttachmentObjRepository.cleanUploadItem(attachment))
      }
    }

    return { ...note }
  }
}
