import chalk from "chalk";
import {default as config} from "../../../../config";
import {default as NimbusSDK} from "../../nimbussdk/net/NimbusSDK";
import {default as TagObj} from "../db/TagObj";
import {default as NoteObjRepository} from "./NoteObjRepository";
import {default as NotesUpdateRequest} from "../../nimbussdk/net/request/NotesUpdateRequest";
import {default as tagModel} from "../../../db/models/tag";
import {default as noteTagsModel} from "../../../db/models/noteTags";
import {default as socketConnection} from "../../socket/socketFunctions";
import {default as socketStoreType} from "../../socket/socketStoreType";
import {default as selectedItem} from "../../../window/selectedItem";
import {default as attachModel} from "../../../db/models/attach";

export default class TagObjRepository {
  /**
   * @type {string}
   */
  static LAST_OPEN_TAG = "last_open_tag";

  /**
   * @param {{workspaceId:string, tag:string}} inputData
   * @param {Function} callback
   * @returns {TagObj}
   */
  static create(inputData, callback = (err, res) => {
  }) {
    const {tag} = inputData;
    NimbusSDK.getAccountManager().getUniqueUserName((err, uniqueUserName) => {
      let tagObj = new TagObj();
      tagObj.title = tag;
      tagObj.oldTitle = null;
      tagObj.dateAdded = new Date().getTime();
      tagObj.dateUpdated = tagObj.dateAdded;
      tagObj.needSync = true;
      tagObj.syncDate = 0;
      tagObj.parentId = null;
      tagObj.uniqueUserName = uniqueUserName;
      callback(err, tagObj);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   * @return {TagObj[]}
   */
  static getUserTagsR(inputData, callback = (err, res) => {
  }) {
    TagObjRepository.getAllTagsR(inputData, (err, list) => {
      callback(err, list);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getAllTagsR(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    tagModel.findAll({}, {workspaceId}, (err, list) => {
      return callback(err, list);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static isAvailableNotesForSync(inputData, callback = (err, res) => {
  }) {
    NoteObjRepository.isAvailableNotesForSync(inputData, callback);
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getErasedTagsForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    tagModel.findAll({
      "erised": true,
      "needSync": true
    }, {workspaceId}, (err, userTagsObjs) => {
      let userTagsTitle = [];
      for (let userTagsObj of userTagsObjs) {
        userTagsTitle.push(userTagsObj.title);
      }
      callback(null, userTagsTitle);
    });
  }

  /**
   * @param {{workspaceId:string, tag:string}} inputData
   * @param {Function} callback
   */
  static getErasedTagForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, tag} = inputData;
    tagModel.find({
      "tag": tag,
      "erised": true,
      "needSync": true
    }, {workspaceId}, (err, userTagObj) => {
      if (err || !userTagObj) {
        return callback(null, null);
      }
      callback(null, userTagObj);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getRenamedTagsForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    tagModel.findAll({
      "oldTitle": {"$nin": [null, ""]}
    }, {workspaceId}, (err, userTags) => {
      let renamedTags = [];
      for (let tagObj of userTags) {
        renamedTags.push(new NotesUpdateRequest.Body.RenameTag(tagObj.oldTitle, tagObj.title));
      }
      callback(err, renamedTags);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getUpdatedTagsForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    tagModel.findAll({}, {
      workspaceId,
      "order": {"title": "asc"}
    }, (err, tagsObjs) => {
      let tags = [];
      for (let tagObj of tagsObjs) {
        tags.push(tagObj.title);
      }
      callback(err, tags);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static callAfterUploadErasedTagsOnServerI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    tagModel.findAll({
      "erised": true,
      "needSync": true
    }, {workspaceId}, async (err, userTags) => {
      if (err) {
        return callback(err, false);
      }

      /**
       * @param {{workspaceId:string, userTag:TagObj}} inputData
       */
      let deleteTags = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, userTag} = inputData;
          tagModel.erase({tag: userTag.tag}, {workspaceId}, (err, response) => {
            resolve(true);
          });
        });
      };

      for (let userTag of userTags) {
        await deleteTags({workspaceId, userTag});
      }

      callback(null, true);
    });
  }

  /**
   * @param {{workspaceId:string, tags:[TagObj]}} inputData
   */
  static async callAfterUploadStoreTagsI(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, tags} = inputData;
      if (!tags.length) {
        return resolve(true);
      }

      tagModel.findAll({
        "tag": {"$in": tags}
        // @ts-ignore
      }, {workspaceId}, async (err, tagObjs) => {
        for (let i in tagObjs) {
          if (tagObjs.hasOwnProperty(i)) {
            tagObjs[i].needSync = false;
          }
        }
        await TagObjRepository.update({workspaceId, tagObjs});
        resolve(true);
      });
    });
  }

  /**
   * @param {{workspaceId:string, tags:[TagObj]}} inputData
   */
  static callAfterUploadRenamedTagsI(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, tags} = inputData;
      if (!tags.length) {
        return resolve(true);
      }

      let tagList = tags.map((tagSynObj) => {
        return tagSynObj['newtag'];
      });

      tagModel.findAll({
        "oldTitle": {"$ne": [null, ""]},
        "tag": {"$in": tagList}
        // @ts-ignore
      }, {workspaceId}, async (err, tagObjs) => {
        for (let i in tagObjs) {
          if (tagObjs.hasOwnProperty(i)) {
            tagObjs[i].oldTitle = null;
            tagObjs[i].needSync = false;
          }
        }
        await TagObjRepository.update({workspaceId, tagObjs});
        resolve(true);
      });
    });
  }

  /**
   * @param {{workspaceId:string, tag:string}} inputData
   * @param {Function} callback
   */
  static checkIfTagRenamed(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, tag} = inputData;
    tagModel.findAll({
      "oldTitle": tag,
      "needSync": true
    }, {workspaceId}, (err, tagObjs) => {
      callback(err, tagObjs.length ? tagObjs[0] : null);
    });
  }

  /**
   * @param {{workspaceId:string, tagObjs:[TagObj]|TagObj}} inputData
   */
  static async update(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, tagObjs} = inputData;
      if (!tagObjs) {
        return resolve(false);
      }

      /**
       * @param {{workspaceId:string, title:string}} inputData
       * @return {Promise<TagObj|null>}
       */
      let getUserTag = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, title} = inputData;
          tagModel.find({"title": title}, {workspaceId}, (err, tagObj) => {
            if (err || !tagObj) {
              return resolve(null);
            }
            resolve(tagObj);
          });
        });
      };

      /**
       * @param {{workspaceId:string, item:TagObj}} inputData
       * @return {Promise<boolean>}
       */
      let updateUserTag = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, item} = inputData;
          if (config.SHOW_WEB_CONSOLE) {
            //console.log("update:", item);
          }

          tagModel.update({"tag": item.title}, item, {workspaceId}, (err, count) => {
            if (err || !count) {
              return resolve(false);
            }

            TagObjRepository.emitSocketEvent({
              workspaceId,
              tagList: [(item.title || item.tag)],
              action: "update"
            });
            resolve(!!count);
          });
        });
      };

      /**
       * @param {{workspaceId:string, item:TagObj}} inputData
       * @return {Promise<*>}
       */
      let createUserTag = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, item} = inputData;
          if (config.SHOW_WEB_CONSOLE) {
            //console.log("save:", item);
          }

          tagModel.add(item, {workspaceId}, (err, itemInstance) => {
            if (err || !itemInstance) {
              return resolve(false);
            }

            TagObjRepository.emitSocketEvent({
              workspaceId,
              tagList: [(item.title || item.tag)],
              action: "add"
            });
            resolve(!!itemInstance);
          });
        });
      };

      /**
       * @param {{workspaceId:string, tagObj:TagObj}} inputData
       * @return {Promise<boolean>}
       */
      let processUserTag = async (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {workspaceId, tagObj} = inputData;
          let existItemInstance = await getUserTag({workspaceId, title: tagObj.title});
          let saveInstance = await TagObjRepository.convertSyncInstanceToLocal(tagObj, existItemInstance);

          //console.log("tag saveInstance: ", saveInstance);

          if (!saveInstance) {
            return resolve(false);
          }

          saveInstance['needSync'] = false;
          if (existItemInstance) {
            resolve(await updateUserTag({workspaceId, item: saveInstance}));
          } else {
            resolve(await createUserTag({workspaceId, item: saveInstance}));
          }
        });
      };

      let result = null;
      let updatedList = [];
      if (tagObjs instanceof Array) {
        for (let tagObj of tagObjs) {
          result = await processUserTag({workspaceId, tagObj});
          if (result) {
            updatedList.push(tagObj);
          }
        }
      } else {
        result = await processUserTag({workspaceId, tagObj: tagObjs});
        if (result) {
          updatedList.push(tagObjs);
        }
      }

      resolve(result);
    });
  }

  /**
   * @param {{workspaceId:string, tagObjs:[TagObj]}} inputData
   */
  static sendSocketEventOnUpdate(inputData) {
    const {workspaceId, tagObjs} = inputData;
    let tagList = [];
    for (let tagObj of tagObjs) {
      let updatedTag = tagObj.title ? tagObj.title : tagObj.tag;
      if (updatedTag) {
        tagList.push(updatedTag);
      }
    }

    TagObjRepository.emitSocketEvent({
      workspaceId,
      tagList,
      action: "add"
    });
  }

  /**
   * @param {{workspaceId:string, tagList:[tagObj], action:string}} inputData
   */
  // @ts-ignore
  static async emitSocketEvent(inputData) {
    const {workspaceId, tagList, action} = inputData;
    if (tagList.length) {
      socketConnection.addStoreData({
        workspaceId,
        storeType: socketStoreType.TAGS_FOR_UPDATE,
        data: {
          action: action,
          tags: tagList
        }
      });

      /**
       * @param {{workspaceId:string, tag:string}} inputData
       * @return {Promise<tagModel>}
       */
      let findUserTag = async (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {workspaceId, tag} = inputData;
          tagModel.find({tag: tag}, {workspaceId}, (err, tagInstance) => {
            if (err || !tagInstance) {
              return resolve(null);
            }
            resolve(tagInstance);
          });
        });
      };

      /**
       * @param {{workspaceId:string, globalId:string}} inputData
       * @return {Promise<noteTagsModel[]>}
       */
      let findUserNoteTag = async function (inputData) {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {workspaceId, globalId} = inputData;
          const selectedNoteGlobalId = selectedItem.get({
            workspaceId,
            itemType: selectedItem.TYPE_NOTE
          });
          noteTagsModel.findAll({
            tagGlobalId: globalId,
            noteGlobalId: selectedNoteGlobalId
          }, {workspaceId}, (err, noteTagsList) => {
            if (err || !noteTagsList) {
              return resolve([]);
            }
            resolve(noteTagsList);
          });
        });
      };

      for (let tag of tagList) {
        let tagInstance = <{globalId:string}>await findUserTag({workspaceId, tag});
        if (tagInstance) {
          let noteTagsList = <[any]>await findUserNoteTag({workspaceId, globalId: tagInstance.globalId});
          for (let noteTagInstance of noteTagsList) {
            TagObjRepository.emitSocketEventForNote({
              workspaceId,
              tag,
              noteGlobalId: noteTagInstance.noteGlobalId,
              action
            });
          }
        }
      }
    }
  }

  /**
   * @param {{workspaceId:string, tag:string, noteGlobalId:string, action:string}} inputData
   */
  static emitSocketEventForNote(inputData) {
    const {workspaceId, tag, noteGlobalId, action} = inputData;
    if (tag && noteGlobalId) {
      socketConnection.addStoreData({
        workspaceId,
        storeType: socketStoreType.NOTES_TAGS_FOR_UPDATE,
        data: {
          action: action,
          noteGlobalId: noteGlobalId,
          tag: tag
        }
      });
    }
  }

  /**
   * @param {TagObj} tagObj
   * @param {item} existItemInstance
   * @return {Promise<item|null>}
   */
  static async convertSyncInstanceToLocal(tagObj, existItemInstance) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (!tagObj) {
        return resolve(null);
      }

      /**
       * @param {item} item
       * @param {} data
       * @return {*}
       */
      let fillSyncObjProperties = (item, data) => {
        return tagModel.prepareSyncOnlyProperties(tagModel.prepareItemDbProperties(item, data), data);
      };

      let fillModelProperties = (item, data) => {
        return tagModel.prepareCommonProperties(tagModel.prepareItemDbProperties(item, data), data);
      };

      let itemInstance = null;

      if (existItemInstance) {
        itemInstance = fillSyncObjProperties(tagModel.prepareModelData(existItemInstance), tagObj);
      } else {
        itemInstance = fillModelProperties(tagModel.prepareModelData(tagObj), tagObj);
      }

      resolve(itemInstance);
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   */
  static async findNoteTagsList(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, noteGlobalId} = inputData;
      tagModel.findAll({
        "noteGlobalId": noteGlobalId
      }, {workspaceId}, (err, tagObjs) => {
        if (err) {
          return resolve([]);
        }
        resolve(tagObjs);
      });
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   */
  static async findNoteTagList(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, noteGlobalId} = inputData;
      tagModel.findAll({
        "noteGlobalId": noteGlobalId
      }, {workspaceId}, (err, tagObjs) => {
        if (err) {
          return resolve([]);
        }
        resolve(tagObjs);
      });
    });
  }


  /**
   * @param {{workspaceId:string, tagObjs:[TagObj]}} inputData
   * @param {Function} callback
   */
  static async updateTagsDownloadedFromServerI(inputData, callback = (err, res) => {
  }) {
    const {tagObjs} = inputData;
    if (!tagObjs.length) {
      return callback(null, false);
    }

    let result = await TagObjRepository.update(inputData);
    callback(null, result);
  }

  /**
   * @param {{workspaceId:string, tag:string}} inputData
   */
  static async deleteData(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, tag} = inputData;
      tagModel.erase({
        "tag": tag
      }, {workspaceId}, (err, response) => {
        if (err) {
          return resolve(null);
        }
        resolve(response);
      });
    });
  }

  /**
   * @param {{workspaceId:string, attachmentList:[AttachmentObj]}} inputData
   */
  static async removeNoteTagList(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, tagList} = inputData;
      if (!tagList.length) {
        return resolve(true);
      }

      let tagGlobalIdList = [];
      for (let tagInstance of tagList) {
        TagObjRepository.emitSocketEventForNote({
          workspaceId,
          tag: tagInstance.tag,
          noteGlobalId: tagInstance.noteGlobalId,
          action: "remove"
        });

        tagGlobalIdList.push(tagInstance.globalId);
      }

      if (!tagGlobalIdList.length) {
        return resolve(true);
      }

      tagModel.erase({
        "globalId": {"$in": tagGlobalIdList}
      }, {workspaceId}, (err, numRemoved) => {
        if (err) {
          return resolve(false);
        }
        resolve(true);
      });
    });
  }

  /**
   * @param {{workspaceId:string, tag:string}} inputData
   */
  static async deleteNoteData(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, tag} = inputData;
      if (!tag) {
        return resolve(false);
      }

      /**
       * @param {{workspaceId:string, tagGlobalId:string, noteGlobalId:string}} inputData
       * @return {Promise}
       */
      let eraseUserNoteTags = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, tagGlobalId, noteGlobalId} = inputData;
          noteTagsModel.erase({
            "tagGlobalId": tagGlobalId,
            "noteGlobalId": noteGlobalId
          }, {workspaceId}, (err, response) => {
            if (err) {
              return resolve(null);
            }

            resolve(response);
          });
        });
      };

      /**
       * @param {{workspaceId:string, tagGlobalId:string}} inputData
       */
      let removeUserNoteTags = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, tagGlobalId} = inputData;
          noteTagsModel.findAll({tagGlobalId: tagGlobalId}, {workspaceId}, async (err, noteTagsList) => {
            if (err) {
              return resolve(false);
            }

            for (let noteTagInstance of noteTagsList) {
              await eraseUserNoteTags({
                workspaceId,
                tagGlobalId: noteTagInstance.tagGlobalId,
                noteGlobalId: noteTagInstance.noteGlobalId
              });

              TagObjRepository.emitSocketEventForNote({
                workspaceId,
                tag,
                noteGlobalId: noteTagInstance.noteGlobalId,
                action: "remove"
              });
            }

            resolve(true);
          });
        });
      };

      tagModel.findAll({title: tag}, {workspaceId}, async (err, tagsList) => {
        if (err || !tagsList) {
          return resolve(false);
        }

        for (let tagInstance of tagsList) {
          await removeUserNoteTags({workspaceId, tagGlobalId: tagInstance.globalId});
        }

        resolve(true);
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param callback
   */
  static clearRemovedData(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    /**
     * @param {{workspaceId:string, tag:string}} inputData
     */
    let deleteEraisedData = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        const {workspaceId, tag} = inputData;
        tagModel.erase({
          "tag": tag,
          "erised": true
        }, {workspaceId}, (err, response) => {
          if (err) {
            return resolve(null);
          }
          resolve(response);
        });
      });
    };

    tagModel.findAll({
      "erised": true
      // @ts-ignore
    }, {workspaceId}, async (err, tagObjs) => {
      for (let tagObj of tagObjs) {
        await TagObjRepository.deleteNoteData({workspaceId, tag: tagObj.tag});
        await TagObjRepository.deleteData({workspaceId, tag: tagObj.tag});
        await deleteEraisedData({workspaceId, tag: tagObj.tag});
      }
      callback(null, true);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string, noteGlobalId:string}} inputData
   */
  static async updateNeedSync(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId, noteGlobalId, tag} = inputData;
      if(!globalId || !noteGlobalId) {
        return resolve(0);
      }

      tagModel.update({
        globalId,
        noteGlobalId
      }, {needSync: false, tag}, {workspaceId}, (err, count) => {
        if(err || !count) {
          return resolve(0);
        }
        return resolve(count);
      });
    });
  }
}
