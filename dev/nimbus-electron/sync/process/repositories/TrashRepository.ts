import chalk from "chalk";
import {default as NoteObjRepository} from "./NoteObjRepository";

export default class TrashObjRepository {
  /**
   * @param {{workspaceId:string, parentId:string}} inputData
   * @param callback Function
   */
  static checkIfNoteInTrash(inputData, callback = (err, res) => {
  }) {
    NoteObjRepository.checkIfNoteInTrash(inputData, (err, noteInTrash) => {
      if (err) {
        return callback(err, false);
      }

      callback(err, noteInTrash);
    });
  }
}
