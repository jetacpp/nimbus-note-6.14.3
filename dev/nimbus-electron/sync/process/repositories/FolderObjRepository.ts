import chalk from "chalk";
import {default as config} from "../../../../config";
import {default as NimbusSDK} from "../../nimbussdk/net/NimbusSDK";
import {default as NoteObj} from "../db/NoteObj";
import {default as FolderObj} from "../db/FolderObj";
import {default as SyncFolderEntity} from "../../nimbussdk/net/response/entities/SyncFolderEntity";
import {default as NoteObjRepository} from "./NoteObjRepository";
import {default as itemModel} from "../../../db/models/item";
import {default as socketConnection} from "../../socket/socketFunctions";
import {default as socketStoreType} from "../../socket/socketStoreType";

export default class FolderObjRepository {
  /**
   * @type {string}
   */
  static DEFAULT_FOLDER = "default_folder";

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static get(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId} = inputData;
    itemModel.find({"globalId": globalId, "type": "folder"}, {workspaceId}, (err, folderObj) => {
      if (FolderObjRepository.isValid(folderObj)) {
        callback(err, {...folderObj});
      } else {
        callback(err, null);
      }
    });
  }

  /**
   * @param {FolderObj} folderObj
   * @return {boolean}
   */
  static isValid(folderObj) {
    return folderObj && FolderObj.isValid(folderObj);
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getUserFoldersR(inputData, callback = (err, res) => {
  }) {
    FolderObjRepository.getAllFoldersR(inputData, (err, list) => {
      callback(err, list);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getAllFoldersR(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({"type": "folder"}, {workspaceId}, (err, list) => {
      return callback(err, list);
    });
  }

  /**
   * @param {{workspaceId:string, folderName:string, parentId:string, globalId:string|null}} inputData
   * @param {Function} callback
   */
  static create(inputData, callback = (err, res) => {
  }) {
    let {workspaceId, folderName, parentId, globalId} = inputData;
    if (globalId == null) {
      globalId = FolderObjRepository.generateGlobalId();
    }

    NimbusSDK.getAccountManager().getUniqueUserName((err, uniqueUserName) => {
      FolderObjRepository.checkIfFolderInTrash({workspaceId, globalId: parentId}, (err, folderInTrash) => {
        let folder = new FolderObj();
        folder.needSync = true;
        folder.globalId = globalId;
        folder.parentId = parentId;
        folder.rootParentId = null;
        folder.index = 0;
        folder.type = "folder";
        folder.existOnServer = false;
        folder.uniqueUserName = uniqueUserName;
        folder = FolderObj.setTitle(folder, folderName);
        folder.dateAdded = new Date().getTime();
        folder.dateUpdated = folder.dateAdded;
        folder.syncDate = 0;
        folder.onlyOffline = false;
        folder.isMaybeInTrash = folderInTrash;
        folder.color = "";
        callback(err, folder);
      });
    });
  }

  /**
   * @return {string}
   */
  static generateGlobalId() {
    return NoteObjRepository.generateGlobalId();
  }

  /**
   * @param {{workspaceId:string, globalId:string }} inputData
   * @param {Function} callback
   * @return {boolean}
   */
  static checkIfFolderInTrash(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId} = inputData;
    let isInTrash = false;
    if (FolderObj.TRASH === globalId) {
      isInTrash = true;
      return callback(null, isInTrash);
    }

    /**
     * @param {{workspaceId:string, globalId: string}} inputData
     * @return {Promise<Object|null>}
     */
    let getUserFolderById = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        FolderObjRepository.get(inputData, (err, folderObj) => {
          if (err) {
            return resolve(null);
          }
          resolve(folderObj);
        });
      });
    };

    FolderObjRepository.get(inputData, async (err, folderObj) => {
      if (folderObj) {
        if (FolderObj.TRASH === folderObj.parentId) {
          isInTrash = true;
          return callback(err, isInTrash);
        }

        while (true) {
          if (folderObj === null) {
            break;
          }
          if (FolderObj.TRASH === folderObj.parentId) {
            isInTrash = true;
            break;
          }
          if (FolderObj.GOD === folderObj.parentId || FolderObj.ROOT === folderObj.parentId) {
            break;
          }
          folderObj = await getUserFolderById({workspaceId, globalId: folderObj.parentId});
        }

        callback(err, isInTrash);
      } else {
        callback(err, false);
      }
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static checkIfFolderExist(inputData, callback = (err, res) => {
  }) {
    FolderObjRepository.get(inputData, (err, folderObj) => {
      callback(null, !!folderObj);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static isAvailableNotesForSync(inputData, callback = (err, res) => {
  }) {
    NoteObjRepository.isAvailableNotesForSync(inputData, callback);
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getErasedFromTrashFoldersForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({
      "type": "folder",
      "erised": true,
      "offlineOnly": {"$ne": true},
      "needSync": true,
    }, {workspaceId}, (err, folderObjs) => {
      let foldersIdList = [];
      for (let folder of folderObjs) {
        foldersIdList.push(folder.globalId);
      }
      callback(err, foldersIdList);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getUpdatedFoldersForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    let convertToSyncUserFolderEntity = async (folderObj) => {
      // @ts-ignore
      return new Promise((resolve) => {
        FolderObjRepository.convertToSyncFolderEntity(folderObj, (err, folder) => {
          if (err) {
            return resolve(null);
          }
          resolve(folder);
        });
      });
    };

    itemModel.findAll({
      "type": "folder",
      "parentId": {"$nin": [FolderObj.ERASED_FROM_TRASH, FolderObj.GOD]},
      "offlineOnly": {"$ne": true},
      "needSync": true,
    }, {
      workspaceId,
      "order": {"dateAdded": "asc"}
      // @ts-ignore
    }, async (err, folderObjs) => {
      let foldersConvertedList = [];
      for (let folderObj of folderObjs) {
        let folder = await convertToSyncUserFolderEntity(folderObj);
        if (folder) {
          foldersConvertedList.push(folder);
        }
      }
      callback(err, foldersConvertedList);
    });
  }

  /**
   * @param {FolderObj} folderObj
   * @param {Function} callback
   * @return {SyncFolderEntity}
   */
  static convertToSyncFolderEntity(folderObj, callback = (err, res) => {
  }) {
    FolderObj.getTitle(folderObj, (err, title) => {
      let folder = new SyncFolderEntity();
      folder.global_id = folderObj.globalId;
      folder.parent_id = folderObj.parentId;
      folder.root_parent_id = folderObj.rootParentId;
      folder.index = folderObj.index;
      folder.date_added_user = folderObj.dateAdded;
      folder.date_updated_user = folderObj.dateUpdated;
      folder.type = folderObj.type;
      folder.title = title;
      folder.shared = folderObj.shared ? 1 : 0;
      folder.color = folderObj.color;
      callback(err, folder);
    });
  }

  /**
   * @param {{workspaceId:string, folderObjs:[FolderObj]}} inputData
   * @param {Function} callback
   */
  static async updateFoldersDownloadedFromServerI(inputData, callback = (err, res) => {
  }) {
    const {folderObjs} = inputData;
    if (!folderObjs.length) {
      return callback(null, false);
    }

    let result = await FolderObjRepository.update(inputData);
    callback(null, result);
  }

  /**
   * @param {{workspaceId:string, folderObjs:FolderObj|FolderObj[]}} inputData
   */
  static async update(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, folderObjs} = inputData;
      if (!folderObjs) {
        return resolve(false);
      }

      let processUserFolder = async (folderObj) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          if (folderObj.type !== "folder") {
            return resolve(false);
          }

          let existItemInstance = <{rootId:string}>await FolderObjRepository.getUserFolder({workspaceId, globalId: folderObj.globalId});
          let saveInstance = <{shared:boolean, rootId:string}>await FolderObjRepository.convertSyncInstanceToLocal(folderObj, existItemInstance);

          if (!saveInstance) {
            return resolve(false);
          }

          saveInstance = FolderObjRepository.updateTrashProperties(folderObj, saveInstance);

          if (existItemInstance) {
            let updateNestedItems = (existItemInstance.rootId !== saveInstance.rootId);
            let updateUserFolder = await FolderObjRepository.updateUserFolder({
              workspaceId,
              item: saveInstance,
              updateNestedItems
            });
            resolve(updateUserFolder);
          } else {
            let createdUserFolder = await FolderObjRepository.createUserFolder({workspaceId, item: saveInstance});
            resolve(createdUserFolder);
          }
        });
      };

      let result = null;
      if (folderObjs instanceof Array) {
        for (let folderObj of folderObjs) {
          result = await processUserFolder(folderObj);
        }
      } else {
        result = await processUserFolder(folderObjs);
      }
      resolve(result);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @return {Promise<FolderObj|null>}
   */
  static async getUserFolder(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId} = inputData;
      itemModel.find({"globalId": globalId, "type": "folder"}, {workspaceId}, (err, folderObj) => {
        if (err || !folderObj) {
          return resolve(null);
        }
        resolve(folderObj);
      });
    });
  }

  /**
   * @param {{workspaceId:string, item:FolderObj, updateNestedItems:boolean}} inputData
   * @return {Promise}
   */
  static async updateUserFolder(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      let {workspaceId, item, updateNestedItems} = inputData;
      updateNestedItems = updateNestedItems || false;

      if (config.SHOW_WEB_CONSOLE) {
        //console.log("update:", item);
      }

      itemModel.update({"globalId": item.globalId}, item, {workspaceId}, async (err, count) => {
        if (err || !count) {
          return resolve(false);
        }

        if (updateNestedItems) {
          /**
           * @param {{workspaceId:string, item:FolderObj}} inputData
           * @return {Promise}
           */
          let updateUserNestedItems = async (inputData) => {
            // @ts-ignore
            return new Promise(async (resolve) => {
              const {workspaceId, item} = inputData;

              /**
               * @param {{workspaceId:string, parentId:string}} inputData
               */
              let getItemsByParentId = async (inputData) => {
                // @ts-ignore
                return new Promise(async (resolve) => {
                  const {workspaceId, parentId} = inputData;
                  itemModel.findAll({'parentId': parentId}, {workspaceId}, (err, itemList) => {
                    if (err) {
                      return resolve([]);
                    }
                    resolve(itemList);
                  });
                });
              };

              /**
               * @param {{workspaceId:string, queryData: {}, updateData: {}}} inputData
               * @return {Promise}
               */
              let updateItemByQuery = async (inputData) => {
                // @ts-ignore
                return new Promise(async (resolve) => {
                  const {workspaceId, queryData, updateData} = inputData;
                  itemModel.update(queryData, updateData, {workspaceId}, () => {
                    resolve(true);
                  });
                });
              };

              let nestedItems = [];
              /**
               * @param {{workspaceId:string, itemInstance:FolderObj}} inputData
               */
                // @ts-ignore
              let getAllNestedItems = async (inputData) => {
                  const {workspaceId, itemInstance} = inputData;
                  if (itemInstance) {
                    nestedItems.push(itemInstance);
                    let nestedItemList = <{}>await getItemsByParentId({workspaceId, parentId: itemInstance.globalId});
                    for (let k in nestedItemList) {
                      await getAllNestedItems({workspaceId, itemInstance: nestedItemList[k]});
                    }
                  }
                };

              await getAllNestedItems({workspaceId, itemInstance: item});
              for (let t in nestedItems) {
                let nestedItem = nestedItems[t];

                if (nestedItem.globalId === "default") {
                  continue;
                }

                let queryData = {globalId: nestedItem.globalId};
                let updateData = {rootId: item.rootId};

                await updateItemByQuery({workspaceId, queryData, updateData});
              }

              resolve(true);
            });
          };

          await updateUserNestedItems({workspaceId, item});
        }

        FolderObjRepository.sendSocketEventOnUpdate({
          workspaceId,
          globalId: item.globalId,
          parentId: item.parentId,
          isCreate: false
        });
        resolve(!!count);
      });
    });
  }

  /**
   * @param {{workspaceId:string, item:FolderObj}} inputData
   */
  static async createUserFolder(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, item} = inputData;
      if (config.SHOW_WEB_CONSOLE) {
        //console.log("save:", item);
      }

      /**
       * @param {{workspaceId:string, queryData:{}}} inputData
       * @return {Promise}
       */
      let getFolderByQuery = async (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {workspaceId, queryData} = inputData;
          itemModel.find(queryData, {workspaceId}, (err, item) => {
            if (err) {
              return resolve(null);
            }
            resolve(item);
          });
        });
      };

      let queryData = {
        globalId: item.parentId,
        type: 'folder'
      };
      let parentFolder = <{rootId:string}>await getFolderByQuery({workspaceId, queryData});
      if (parentFolder) {
        item.rootId = parentFolder.rootId;
      }

      itemModel.add(item, {workspaceId}, (err, itemInstance) => {
        if (err || !itemInstance) {
          return resolve(false);
        }

        FolderObjRepository.sendSocketEventOnUpdate({
          workspaceId,
          globalId: item.globalId,
          parentId: item.parentId,
          isCreate: true
        });
        resolve(!!itemInstance);
      });
    });
  }

  /**
   * @param {NoteObj|FolderObj} itemObj
   * @param {{}} saveInstance
   * @return {{}}
   */
  static updateTrashProperties(itemObj, saveInstance) {
    if (itemObj.parentId) {
      saveInstance.parentId = itemObj.parentId === 'root' ? 'root' : itemObj.parentId;
      saveInstance.rootId = itemObj.parentId === 'trash' ? 'trash' : itemObj.rootId;
      if (itemObj.parentId !== "trash") {
        saveInstance["rootParentId"] = itemObj.parentId;
      }
    }
    saveInstance.needSync = false;
    return saveInstance;
  }

  /**
   * @param {{workspaceId:string, globalId:string, parentId:string, isCreate:boolean}} inputData
   */
  static sendSocketEventOnUpdate(inputData) {
    const {workspaceId, globalId, parentId, isCreate} = inputData;
    socketConnection.addStoreData({
      workspaceId,
      storeType: socketStoreType.FOLDERS_FOR_UPDATE,
      data: {
        globalId: globalId,
        parentId: parentId,
        isCreate: isCreate,
        type: 'folder'
      }
    });
  }


  /**
   * @param {{workspaceId:string,globalIdList:[string]}} inputData
   */
  static sendSocketEventOnRemove(inputData) {
    const {workspaceId, globalIdList} = inputData;
    if (globalIdList && globalIdList.length) {
      socketConnection.addStoreData({
        workspaceId,
        storeType: socketStoreType.FOLDERS_FOR_REMOVE,
        data: {
          globalId: globalIdList
        }
      });
    }
  }

  /**
   * @param {FolderObj} folderObj
   * @param {item} existItemInstance
   * @return {Promise<item|null>}
   */
  static async convertSyncInstanceToLocal(folderObj, existItemInstance) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (!folderObj) {
        return resolve(null);
      }

      let fillSyncObjProperties = (item, data) => {
        return itemModel.prepareFolderOnlyProperties(itemModel.prepareItemDbProperties(item, data), data);
      };

      let fillModelProperties = (item, data) => {
        item.role = data.role || "folder";
        return itemModel.prepareCommonProperties(itemModel.prepareItemDbProperties(item, data), data);
      };

      let itemInstance = null;

      if (existItemInstance) {
        itemInstance = fillSyncObjProperties(itemModel.prepareModelData(existItemInstance), folderObj);
        itemInstance = itemModel.changeDateUpdate(itemInstance, folderObj);

        if (itemInstance.rootId !== folderObj.rootId) {
          itemInstance.rootId = folderObj.rootId || 'root';
        }
        if (itemInstance.title !== folderObj.title) {
          itemInstance.title = folderObj.title;
        }
      } else {
        itemInstance = fillModelProperties(itemModel.prepareModelData(folderObj), folderObj);
      }
      resolve(itemInstance);
    });
  }

  /**
   * @param {{workspaceId:string, folder:FolderObj}} inputData
   */
  static async createUserFolderObj(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, folder} = inputData;
      FolderObjRepository.create({
        workspaceId,
        folderName: folder.title,
        parentId: folder.parent_id,
        globalId: folder.global_id
      }, (err, folderObj) => {
        if (err) {
          return resolve(null);
        }
        resolve(folderObj);
      });
    });
  }

  /**
   * @param {{workspaceId:string, folderIds:[string]}} inputData
   * @param {Function} callback
   */
  static deleteRemovedItemsDownloadedFromServerI(inputData, callback = (err, res) => {
  }) {
    FolderObjRepository.deleteFoldersAndAllDataOnDevice(inputData, (err, result) => {
      callback(err, true);
    });
  }

  /**
   * @param {{workspaceId:string, folderIds:[string]}} inputData
   * @param {Function} callback
   */
  // @ts-ignore
  static async deleteFoldersAndAllDataOnDevice(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, folderIds} = inputData;
    if (folderIds && folderIds.length) {
      /**
       * @param {{ workspaceId:string, globalId:string }} inputData
       */
      let deleteUserFolder = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          FolderObjRepository.deleteFolderAndAllDataFromDevice(inputData, (err, response) => {
            if (err) {
              return resolve(false);
            }
            resolve(true);
          });
        });
      };

      for (let globalId of folderIds) {
        await deleteUserFolder({workspaceId, globalId});
      }
    }

    FolderObjRepository.sendSocketEventOnRemove({workspaceId, globalIdList: folderIds});
    callback(null, true);
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static deleteFolderAndAllDataFromDevice(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId} = inputData;
    itemModel.find({"globalId": globalId, "type": "folder"}, {workspaceId}, async (err, folderObj) => {
      if (err || !folderObj) {
        return callback(err, false);
      }

      /**
       * @param {{workspaceId:string, globalId:string}} inputData
       */
      let removeUserItemAndAllDataFromDevice = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          NoteObjRepository.deleteFolderNotesAndAllDataFromDevice(inputData, (err, result) => {
            if (err) {
              return resolve(false);
            }
            resolve(result);
          });
        });
      };

      /**
       * @param {{workspaceId:string, parentId:string}} inputData
       */
      let getItemsByParentId = async (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {workspaceId, parentId} = inputData;
          itemModel.findAll({'parentId': parentId}, {workspaceId}, (err, itemList) => {
            if (err) {
              return resolve([]);
            }
            resolve(itemList);
          });
        });
      };

      let nestedItems = [];

      /**
       * @param {{workspaceId:string, itemInstance:FolderObj}} inputData
       */
        // @ts-ignore
      let getAllNestedItems = async function (inputData) {
          const {workspaceId, itemInstance} = inputData;
          if (itemInstance) {
            nestedItems.push(itemInstance);
            let nestedItemList = <{}>await getItemsByParentId({workspaceId, parentId: itemInstance.globalId});
            for (let k in nestedItemList) {
              await getAllNestedItems({workspaceId, itemInstance: nestedItemList[k]});
            }
          }
        };

      await getAllNestedItems({workspaceId, itemInstance: folderObj});

      for (let t in nestedItems) {
        let nestedItem = nestedItems[t];

        if (nestedItem.globalId === "default") {
          continue;
        }

        await removeUserItemAndAllDataFromDevice({workspaceId, globalId: nestedItem.globalId});
      }

      callback(err, true);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  // @ts-ignore
  static async callAfterUploadErasedFromTrashFoldersOnServerI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({
      "type": "folder",
      "erised": true
    }, {workspaceId}, async (err, folderObjs) => {
      if (err) {
        return callback(err, false);
      }

      /**
       * @param {{workspaceId:string, folderObj:FolderObj}} inputData
       */
      let deleteFolderAndAllDataFromDevice = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, folderObj} = inputData;
          FolderObjRepository.deleteFolderAndAllDataFromDevice({
            workspaceId,
            globalId: folderObj.globalId
          }, (err, result) => {
            if (err) {
              return resolve(null);
            }

            resolve(result);
          });
        });
      };

      for (let folderObj of folderObjs) {
        await deleteFolderAndAllDataFromDevice({workspaceId, folderObj});
      }

      callback(null, true);
    });
  }

  /**
   * @param {{workspaceId:string, folders:[FolderObj]}} inputData
   */
  static async callAfterUploadExistFoldersOnServerI(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, folders} = inputData;
      if (!folders.length) {
        return resolve(true);
      }

      let globalIdList = folders.map((folderSynObj) => {
        return folderSynObj['global_id'];
      });

      itemModel.findAll({
        "type": "folder",
        "globalId": {"$in": globalIdList},
        "parentId": {"$nin": [FolderObj.ERASED_FROM_TRASH, FolderObj.GOD]}
        // @ts-ignore
      }, {workspaceId}, async (err, folderObjs) => {
        for (let i in folderObjs) {
          if (folderObjs.hasOwnProperty(i)) {
            folderObjs[i].existOnServer = true;
            folderObjs[i].needSync = false;
          }
        }
        await FolderObjRepository.update({workspaceId, folderObjs});
        resolve(true);
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   * @return {string}
   */
  static getDefaultFolder(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    let globalId = FolderObj.DEFAULT;
    FolderObjRepository.get({workspaceId, globalId}, (err, folderObj) => {
      if (folderObj == null) {
        globalId = FolderObj.DEFAULT;
      }

      callback(err, globalId);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async deleteData(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId} = inputData;
      itemModel.erase({
        "globalId": globalId,
        "type": "folder"
      }, {workspaceId}, (err, response) => {
        if (err) {
          return resolve(null);
        }

        resolve(response);
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param callback
   */
  static clearRemovedData(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    itemModel.findAll({
      "erised": true,
      "type": "folder"
      // @ts-ignore
    }, {workspaceId}, async (err, folderObjs) => {
      for (let folderObj of folderObjs) {
        await FolderObjRepository.deleteData({workspaceId, globalId: folderObj.globalId});
      }
      callback(null, true);
    });
  }
}
