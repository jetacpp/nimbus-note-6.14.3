import chalk from "chalk";
import fs from "fs-extra";
import {default as config} from "../../../../config";
import {default as NimbusSDK} from "../../nimbussdk/net/NimbusSDK";
import {default as AttachmentObj} from "../db/AttachmentObj";
import {default as NoteObjRepository} from "./NoteObjRepository";
import {default as SyncAttachmentRenamedEntity} from "../../nimbussdk/net/response/entities/SyncAttachmentRenamedEntity";
import {default as SyncAttachmentEntity} from "../../nimbussdk/net/response/entities/SyncAttachmentEntity";
import {default as attach, default as attachModel} from "../../../db/models/attach";
import {default as itemModel} from "../../../db/models/item";
import {default as socketConnection} from "../../socket/socketFunctions";
import {default as socketStoreType} from "../../socket/socketStoreType";
import {default as PouchDb} from "../../../../pdb";
import {default as todo, default as todoModel} from "../../../db/models/todo";

export default class AttachmentObjRepository {
  static DOWNLOAD_MAX_TRY = 5;

  /**
   * @param {string} globalId
   * @param {string} parentId
   * @param {Function} callback
   */
  static create(globalId, parentId, callback = (err, res) => {
  }) {
    NimbusSDK.getAccountManager().getUniqueUserName((err, uniqueUserName) => {
      let attachment = new AttachmentObj();
      attachment.globalId = globalId;
      attachment.parentId = parentId;
      attachment.dateAdded = new Date().getTime();
      attachment.syncDate = 0;
      attachment.needSync = true;
      attachment.location = null;
      attachment = attachment.setLocalPath(null);
      attachment.tempName = null;
      attachment.type = null;
      attachment.typeExtra = null;
      attachment.size = 0;
      attachment.extension = null;
      attachment.displayName = null;
      attachment.oldDisplayName = null;
      attachment.inList = 0;
      attachment.uniqueUserName = uniqueUserName;
      attachment.fileUUID = null;

      callback(err, attachment);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param callback Function
   */
  static isAvailableNotesForSync(inputData, callback = (err, res) => {
  }) {
    NoteObjRepository.isAvailableNotesForSync(inputData, callback);
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param callback Function
   */
  static getErasedAttachmentsForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    attachModel.findAll({
      "erised": true,
      "needSync": true,
      //"isMoreThanLimit": true
      // @ts-ignore
    }, {workspaceId}, async (err, attatchObjs) => {
      let attachmentsList = {};
      for (let attachmentObj of attatchObjs) {
        //if (attachmentObj.tempName || attachmentObj.location) {
        if (typeof (attachmentsList[attachmentObj.noteGlobalId]) === "undefined") {
          attachmentsList[attachmentObj.noteGlobalId] = [];
        }
        attachmentsList[attachmentObj.noteGlobalId].push(attachmentObj.globalId);
        //}
      }

      let attachmentsIdList = await AttachmentObjRepository.filterOfflineAttachmentList({workspaceId, attachmentsList});

      callback(err, attachmentsIdList);
    });
  }

  /**
   * @param {{workspaceId:string, attachmentsList:[AttachmentObj]}} inputData
   * @return {Promise}
   */
  static async filterOfflineAttachmentList(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, attachmentsList} = inputData;
      let noteIdList = Object.keys(attachmentsList);
      itemModel.findAll({
        "globalId": {"$in": noteIdList},
        "type": "note",
      }, {workspaceId}, (err, noteObjs) => {
        let getAttachmentsList = (attachmentsList) => {
          let result = [];
          for (let noteGlobalId in attachmentsList) {
            if (attachmentsList.hasOwnProperty(noteGlobalId)) {
              let attachmentsChunk = attachmentsList[noteGlobalId];
              for (let attachmentId of attachmentsChunk) {
                result.push(attachmentId);
              }
            }
          }
          return result;
        };

        for (let noteObj of noteObjs) {
          if (noteObj.offlineOnly) {
            if (typeof (attachmentsList[noteObj.globalId]) !== 'undefined') {
              delete attachmentsList[noteObj.globalId];
            }
          }
        }

        resolve(getAttachmentsList(attachmentsList));
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getUserAttachmentsR(inputData, callback = (err, res) => {
  }) {
    AttachmentObjRepository.getAllAttachmentsR(inputData, (err, list) => {
      callback(err, list);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getAllAttachmentsR(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    attachModel.findAll({}, {workspaceId}, (err, list) => {
      return callback(err, list);
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @param {Function} callback
   */
  static getRenamedAttachmentsForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, noteGlobalId} = inputData;
    attachModel.findAll({
      "parentId": noteGlobalId,
      "oldDisplayName": {"$ne": null},
      "needSync": true
    }, {workspaceId}, (err, attachObjs) => {
      let attachmentEntities = [];
      for (let attachmentObj of attachObjs) {
        if (attachmentObj.tempName || attachmentObj.location) {
          let entity = new SyncAttachmentRenamedEntity();
          entity.global_id = attachmentObj.globalId;
          entity.display_name = attachmentObj.displayName;
          attachmentEntities.push(entity);
        }
      }
      callback(err, attachmentEntities);
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @param {Function} callback
   */
  static getNoteAttachmentForUploadOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, noteGlobalId} = inputData;
    attachModel.findAll({
      "noteGlobalId": noteGlobalId,
      "needSync": true,
      "erised": false
    }, {workspaceId}, (err, attachObjs) => {
      if (config.SHOW_WEB_CONSOLE) {
        //console.log("attachObjs: ", attachObjs);
      }

      if (err) {
        return callback(err, []);
      }

      callback(err, attachObjs);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param callback Function
   */
  static getNotDownloadedAttachmentFromDownloadedNotesForDownloadFromServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    attachModel.findAll({
      "location": {"$ne": null},
      "isDownloaded": false,
      "tryDownloadCounter": {"$lt": AttachmentObjRepository.DOWNLOAD_MAX_TRY},
    }, {
      workspaceId,
      "order": {
        "dateAdded": "desc"
      }
    }, (err, attachObjs) => {
      callback(err, attachObjs);
    });
  }

  /**
   * @param {{workspaceId:string, parentId:string}} inputData
   * @param {Function} callback
   */
  static getNoteAttachmentsInListCount(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, parentId} = inputData;
    attachModel.count({
      "parentId": parentId,
      "inList": 1
    }, {workspaceId}, (err, attachmentsCount) => {
      callback(err, attachmentsCount);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string, tempName:string}} inputData
   * @param callback Function
   */
  static updateTempNameAfterUploadOnServerI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, tempName} = inputData;
    // @ts-ignore
    AttachmentObjRepository.getR(inputData, async (err, attachmentObj) => {
      let result = null;

      if (AttachmentObjRepository.isValid(attachmentObj)) {
        attachmentObj.tempName = tempName;
        result = await AttachmentObjRepository.update({workspaceId, attachmentObjs: [attachmentObj]});
      }

      callback(err, result);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async updateDownloadStatus(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId} = inputData;
      // @ts-ignore
      AttachmentObjRepository.getR(inputData, async (err, attachmentObj) => {
        let result = null;
        if (AttachmentObjRepository.isValid(attachmentObj)) {
          attachmentObj.isDownloaded = true;
          result = await AttachmentObjRepository.update({workspaceId, attachmentObjs: [attachmentObj]});
        }
        resolve(result);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static getR(inputData, callback = (err, res) => {
  }) {
    let {workspaceId, globalId} = inputData;
    if (globalId === null) {
      globalId = "";
    }

    attachModel.find({
      "globalId": globalId
    }, {workspaceId}, (err, attachObj) => {
      callback(err, attachObj);
    });
  }


  /**
   * @param {{workspaceId:string, attachmentObj:AttachmentObj}} inputData
   * @param callback Function
   */
  // @ts-ignore
  static async updateAttachmentFromManualDownloadI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, attachmentObj} = inputData;
    let result = null;
    if (attachmentObj && attachmentObj.globalId) {
      attachmentObj.isDownloaded = true;
      result = await AttachmentObjRepository.update({workspaceId, attachmentObjs: [attachmentObj]});
    }
    callback(null, result);
  }

  /**
   * @param {{workspaceId:string, attachmentObjs:AttachmentObj|[AttachmentObj], sendSocketEvents}} inputData
   * @param {boolean} sendSocketEvents
   */
  static async update(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      let {workspaceId, attachmentObjs, sendSocketEvents} = inputData;
      sendSocketEvents = sendSocketEvents || false;
      if (!attachmentObjs) {
        return resolve(false);
      }

      /**
       * @param {{workspaceId:string, globalId:string}} inputData
       * @return {Promise<AttachmentObj|null>}
       */
      let getUserAttachment = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, globalId} = inputData;
          attachModel.find({globalId: globalId}, {workspaceId}, (err, attachmentObj) => {
            if (err || !attachmentObj) {
              return resolve(null);
            }
            resolve(attachmentObj);
          });
        });
      };

      /**
       * @param {{workspaceId:string, item:AttachmentObj}} inputData
       * @return {Promise<boolean>}
       */
      let updateUserAttachment = async (inputData) => {
        if (config.SHOW_WEB_CONSOLE) {
          //console.log("updateUserAttachment::", item);
        }

        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, item} = inputData;
          attachModel.update({
            globalId: item.globalId,
            noteGlobalId: item.noteGlobalId
          }, item, {workspaceId}, (err, count) => {
            if (err || !count) {
              return resolve(false);
            }
            resolve(!!count);
          });
        });
      };

      /**
       * @param {{workspaceId:string, item:AttachmentObj}} inputData
       * @return {Promise<boolean>}
       */
      let createUserAttachment = async (inputData) => {
        if (config.SHOW_WEB_CONSOLE) {
          //console.log("save::", item);
        }

        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, item} = inputData;
          attachModel.add(item, {workspaceId}, (err, attachmentInstance) => {
            if (err || !attachmentInstance) {
              return resolve(false);
            }
            resolve(!!attachmentInstance);
          });
        });
      };

      /**
       * @param {{workspaceId:string, attachmentObj:AttachmentObj}} inputData
       * @return {Promise<boolean>}
       */
      let processUserAttachment = async (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {workspaceId, attachmentObj} = inputData;
          let existItemInstance = await getUserAttachment({workspaceId, globalId: attachmentObj.globalId});
          let saveInstance = await AttachmentObjRepository.convertSyncInstanceToLocal(attachmentObj, existItemInstance);
          if (!saveInstance) {
            return resolve(false);
          }

          saveInstance['needSync'] = false;
          let result = null;
          if (existItemInstance) {
            result = await updateUserAttachment({workspaceId, item: saveInstance});
          } else {
            result = await createUserAttachment({workspaceId, item: saveInstance});
          }

          if (result && sendSocketEvents) {
            AttachmentObjRepository.sendSocketEventOnUpdate({workspaceId, attachmentObj: saveInstance});
          }

          resolve(result);
        });
      };

      for (let attachmentObj of attachmentObjs) {
        await processUserAttachment({workspaceId, attachmentObj});
      }

      resolve(true);
    });
  }

  /**
   * @param {{workspaceId:string, attachmentObj: {}}} inputData
   */
  static sendSocketEventOnUpdate(inputData) {
    const {workspaceId, attachmentObj} = inputData;
    if (attachmentObj) {
      socketConnection.addStoreData({
        workspaceId,
        storeType: socketStoreType.NOTES_ATTACHMENTS_FOR_UPDATE,
        data: {
          noteGlobalId: attachmentObj.noteGlobalId,
          globalId: attachmentObj.globalId
        }
      });
    }
  }

  /**
   * @param {{workspaceId:string, globalId:string, noteGlobalId:string}} inputData
   */
  static sendSocketEventOnRemove(inputData) {
    const {workspaceId, globalId, noteGlobalId} = inputData;
    if (globalId && noteGlobalId) {
      socketConnection.addStoreData({
        workspaceId,
        storeType: socketStoreType.NOTES_ATTACHMENTS_FOR_REMOVE,
        data: {
          noteGlobalId: noteGlobalId,
          globalId: globalId
        }
      });
    }
  }

  /**
   * @param {AttachmentObj} attachmentObj
   * @param {item} existItemInstance
   * @return {Promise<Object|null>}
   */
  static async convertSyncInstanceToLocal(attachmentObj, existItemInstance) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (!attachmentObj) {
        return resolve(null);
      }

      /**
       * @param {item} item
       * @param {} data
       * @return {*}
       */
      let fillSyncObjProperties = (item, data) => {
        return attachModel.prepareSyncOnlyProperties(attachModel.prepareItemDbProperties(item, data), data);
      };

      let fillModelProperties = (item, data) => {
        return attachModel.prepareCommonProperties(attachModel.prepareItemDbProperties(item, data), data);
      };

      let attachmentInstance = null;
      if (existItemInstance) {
        attachmentInstance = fillSyncObjProperties(attachModel.prepareModelData(existItemInstance), attachmentObj);
        if (attachmentInstance.displayName !== attachmentObj.display_name) {
          attachmentInstance.oldDisplayName = attachmentInstance.displayName;
        }
        if (attachmentObj.display_name) {
          attachmentInstance.displayName = attachmentObj.display_name;
        }
      } else {
        attachmentInstance = fillModelProperties(attachModel.prepareModelData(attachmentObj), attachmentObj);
        attachmentInstance.isDownloaded = false;
      }

      resolve(attachmentInstance);
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   */
  static async findNoteAttachmentList(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, noteGlobalId} = inputData;
      attachModel.findAll({
        "noteGlobalId": noteGlobalId
      }, {workspaceId}, (err, attachObjs) => {
        if (err) {
          return resolve([]);
        }
        resolve(attachObjs);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async findAttachment(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId} = inputData;
      attachModel.find({globalId}, {workspaceId}, (err, attachObjs) => {
        if (err) {
          return resolve([]);
        }
        resolve(attachObjs);
      });
    });
  }

  /**
   * @param {{workspaceId:string, attachmentList:[AttachmentObj]}} inputData
   */
  static async removeNoteAttachmentList(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, attachmentList} = inputData;
      if (!attachmentList.length) {
        return resolve(true);
      }

      let attachmentGlobalIdList = [];
      for (let attachmentInstance of attachmentList) {
        AttachmentObjRepository.sendSocketEventOnRemove({
          workspaceId,
          globalId: attachmentInstance.globalId,
          noteGlobalId: attachmentInstance.noteGlobalId
        });
        attachmentGlobalIdList.push(attachmentInstance.globalId);
      }

      if (!attachmentGlobalIdList.length) {
        return resolve(true);
      }

      attachModel.erase({
        "globalId": {"$in": attachmentGlobalIdList}
      }, {workspaceId}, (err, numRemoved) => {
        if (err) {
          return resolve(false);
        }
        resolve(true);
      });
    });
  }

  /**
   * @param {{workspaceId:string, attachmentObjs:[AttachmentObj]}} inputData
   * @param callback Function
   */
  static async updateAttachmentsDownloadedFromServerI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, attachmentObjs} = inputData;
    let result = null;
    if (attachmentObjs && attachmentObjs.length > 0) {
      result = await AttachmentObjRepository.update({workspaceId, attachmentObjs});
    }
    callback(null, result);
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param callback Function
   */
  static checkIfAttachmentNotMoreThanLimitAttachmentSize(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    AttachmentObjRepository.get(inputData, async (err, attachmentObj) => {
      if (!attachmentObj) {
        return callback(null, false);
      }
      const attachSize = attachmentObj.size || 0;
      const result = await attachModel.checkFileUploadLimit({workspaceId, size: attachSize});
      return callback(err, result);
    });
  }

  /**
   * @param attachmentObj AttachmentObj
   * @param callback Function
   */
  static convertToSyncAttachmentEntity(attachmentObj, callback = (err, res) => {
  }) {
    let attachment = new SyncAttachmentEntity();
    attachment.global_id = attachmentObj.globalId;
    attachment.date_added = attachmentObj.dateAdded;
    attachment.display_name = attachmentObj.displayName;
    attachment.in_list = attachmentObj.inList ? 1 : 0;
    attachment.type = attachmentObj.type;
    attachment.tempname = attachmentObj.tempName;
    attachment.mime = AttachmentObjRepository.getMimeTypeFromExtension(attachmentObj.extension);

    callback(null, attachment);
  }

  /**
   * @param {Object} attachmentObj
   * @return {Object}
   */
  static convertToSyncAttachmentEntityForManualDownload(attachmentObj) {
    let attachment = new SyncAttachmentEntity();
    attachment.global_id = attachmentObj.globalId;
    attachment.date_added = attachmentObj.dateAdded;
    attachment.date_updated = attachmentObj.syncDate;
    attachment.location = attachmentObj.location;
    attachment.type = attachmentObj.type;
    attachment.role = null;
    attachment.type_extra = attachmentObj.typeExtra;
    attachment.size = attachmentObj.size;
    attachment.in_list = attachmentObj.inList;
    attachment.is_encrypted = 0;
    attachment.display_name = attachmentObj.displayName;
    attachment.mime = AttachmentObjRepository.getMimeTypeFromExtension(attachmentObj.extension);
    attachment.tempname = attachmentObj.tempName;
    attachment.file_uuid = attachmentObj.storedFileUUID ? attachmentObj.storedFileUUID : attachmentObj.fileUUID;
    return attachment;
  }

  /**
   * @param {string} extension
   * @return {string}
   */
  static getMimeTypeFromExtension(extension) {
    //TODO: get mime type from extension
    return "";
  }

  /**
   * @param {string} mimeType
   * @return {string}
   */
  static getExtensionFromMimeType(mimeType) {
    //TODO: get mime type from mime type
    return "";
  }

  /**
   * @param {{workspaceId:string, globalIds:[string]}} inputData
   * @param callback Function
   */
  static deleteRemovedItemsDownloadedFromServerI(inputData, callback = (err, res) => {
  }) {
    const {globalIds} = inputData;
    if (!globalIds.length) {
      return callback(null, false);
    }

    AttachmentObjRepository.deleteAttachmentsAndAllDataFromDevice(inputData, (err, response) => {
      callback(err, true);
    });
  }

  /**
   * @param {{workspaceId:string, globalIds:[string]}} inputData
   * @param {Function} callback
   */
  // @ts-ignore
  static async deleteAttachmentsAndAllDataFromDevice(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalIds} = inputData;
    /**
     * @param {{workspaceId:string, globalId:string}} inputData
     */
    let deleteUserAttachment = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        AttachmentObjRepository.deleteAttachmentAndAllDataFromDevice(inputData, (err, response) => {
          if (err) {
            return resolve(false);
          }
          resolve(response);
        });
      });
    };

    for (let globalId of globalIds) {
      await deleteUserAttachment({workspaceId, globalId});
    }

    callback(null, true);
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static deleteAttachmentAndAllDataFromDevice(inputData, callback = (err, res) => {
  }) {
    AttachmentObjRepository.checkIfAttachmentExist(inputData, (err, attachExist) => {
      if (attachExist) {
        const {workspaceId, globalId} = inputData;
        AttachmentObjRepository.get(inputData, (err, attachment) => {
          attachModel.erase({
            "globalId": globalId
          }, {workspaceId}, (err, response) => {
            let targetPath = AttachmentObjRepository.getAttachLocalFilePath(attachment);
            AttachmentObjRepository.deleteAttachmentFile(targetPath, (err, response) => {
              AttachmentObjRepository.sendSocketEventOnRemove({
                workspaceId,
                globalId: attachment.globalId,
                noteGlobalId: attachment.noteGlobalId
              });
              callback(err, response);
            });
          });
        });
      } else {
        callback(err, false);
      }
    });
  }

  static eraseAttachmentFile(inputData) {
    return new Promise(resolve => {
      const { workspaceId, globalId } = inputData
      attachModel.find({globalId}, {workspaceId}, (err, attachObj) => {
        if(err || !attachObj) {
          return resolve(false);
        }

        itemModel.find({globalId: attachObj.noteGlobalId}, {workspaceId}, (err, itemObj) => {
          if(err || !itemObj) {
            return resolve(false);
          }

          if(itemObj && itemObj.preview && itemObj.preview.global_id && itemObj.preview.global_id === globalId) {
            itemModel.update({
              globalId: attachObj.noteGlobalId,
              type: 'note',
            }, {preview: null},{workspaceId}, () => {
              attachModel.erase({
                "globalId": globalId
              }, {workspaceId}, () => {
                return resolve(true);
              });
            });
          } else {
            attachModel.erase({
              "globalId": globalId
            }, {workspaceId}, () => {
              return resolve(true);
            });
          }
        });

      });
    });
  }

  /**
   * @param {attach} attachItem
   * @return {string}
   */
  static getAttachLocalFilePath(attachItem) {
    let targetPath = "";
    if (attachItem && attachItem.storedFileUUID) {
      targetPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
    }

    return targetPath;
  }

  static async deleteAttachmentFile(localPath, callback = (err, res) => {
  }) {
    if (!localPath) {
      return callback(null, true);
    }

    if (localPath) {
      // @ts-ignore
      let exists = await fs.exists(localPath);
      // @ts-ignore
      if (exists) {
        try {
          fs.unlink(localPath, () => {
          });
        } catch (e) {
          if (config.SHOW_WEB_CONSOLE) {
            console.log("Error => AttachmentObjRepository => deleteAttachmentFile => remove file: ", localPath, e);
          }
        }
      }
      callback(null, true);
    }
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static checkIfAttachmentExist(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId} = inputData;
    attachModel.find({
      "globalId": globalId
    }, {workspaceId}, (err, attachObj) => {
      callback(err, !!attachObj);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static get(inputData, callback = (err, res) => {
  }) {
    let {workspaceId, globalId} = inputData;
    if (globalId === null) {
      globalId = "";
    }

    attachModel.find({
      "globalId": globalId
    }, {workspaceId}, (err, attachObj) => {
      if (AttachmentObjRepository.isValid(attachObj)) {
        callback(err, {...attachObj});
      } else {
        callback(err, null);
      }
    });
  }

  static isValid(attachmentObj) {
    return attachmentObj !== null && AttachmentObj.isValid(attachmentObj);
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param callback Function
   */
  static callAfterUploadErasedAttachmentsOnServerI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    attachModel.findAll({
      "erised": true,
      "needSync": true
    }, {workspaceId}, async (err, attachObjs) => {
      if (err) {
        return callback(err, false);
      }

      /**
       * @param {{workspaceId:string, attachmentObj:AttachmentObj}} inputData
       */
      let deleteUserAttachment = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, attachmentObj} = inputData;
          attachModel.erase({
            "globalId": attachmentObj.globalId
          }, {workspaceId}, (err, response) => {
            let targetPath = AttachmentObjRepository.getAttachLocalFilePath(attachmentObj);
            AttachmentObjRepository.deleteAttachmentFile(targetPath, (err, response) => {
              resolve(response);
            });
          });
        });
      };

      for (let attachmentObj of attachObjs) {
        await deleteUserAttachment({workspaceId, attachmentObj});
      }

      callback(null, true);
    });
  }

  /**
   * @param {{workspaceId:string, parentId:string}} inputData
   * @param {Function} callback
   */
  static callAfterUploadFullSyncI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, parentId} = inputData;
    attachModel.findAll({
      "parentId": parentId,
      "isAttachedToNote": false
      // @ts-ignore
    }, {workspaceId}, async (err, attachmentObjs) => {
      for (let i in attachmentObjs) {
        if (attachmentObjs.hasOwnProperty(i)) {
          if (attachmentObjs[i].tempName || attachmentObjs[i].location) {
            attachmentObjs[i].isAttachedToNote = true;
          }
        }
      }
      let result = await AttachmentObjRepository.update({workspaceId, attachmentObjs});
      callback(err, result);
    });
  }

  /**
   * @param {{workspaceId:string, parentId:string}} inputData
   * @param {Function} callback
   */
  static callAfterUploadRenamedAttachmentsI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, parentId} = inputData;
    attachModel.findAll({
      "parentId": parentId,
      "oldDisplayName": {"$ne": null}
      // @ts-ignore
    }, {workspaceId}, async (err, attachmentObjs) => {
      for (let i in attachmentObjs) {
        if (attachmentObjs.hasOwnProperty(i)) {
          attachmentObjs[i].oldDisplayName = null;
        }
      }
      let result = await AttachmentObjRepository.update({workspaceId, attachmentObjs});
      callback(err, result);
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @param {Function} callback
   */
  static getNoteAttachmentsForUploadSizeInBytes(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, noteGlobalId} = inputData;
    attachModel.findAll({
      "noteGlobalId": noteGlobalId,
      "location": "",
      "erised": false
    }, {workspaceId, secure: true}, (err, attachmentObjs) => {
      let size = 0;
      for (let attachmentObj of attachmentObjs) {
        if (attachmentObj.size) {
          size += attachmentObj.size;
        }
      }
      callback(err, size);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static clearRemovedData(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    attachModel.findAll({
      "erised": true
    }, {workspaceId}, (err, attachObjs) => {
      for (let attachmentObj of attachObjs) {
        AttachmentObjRepository.deleteAttachmentAndAllDataFromDevice({
          workspaceId,
          globalId: attachmentObj.globalId
        }, (err, response) => {
        });
      }
      callback(null, true);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  static updateAttachmentDownloadCounter(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    AttachmentObjRepository.get(inputData, (err, attachItem) => {
      if (err || !attachItem) {
        return callback(err, false);
      }

      if (!attachItem.tryDownloadCounter) {
        attachItem.tryDownloadCounter = 0;
      }
      attachItem.tryDownloadCounter += 1;
      attachItem.isDownloaded = false;

      attachModel.update({
        globalId: attachItem.globalId,
        noteGlobalId: attachItem.noteGlobalId
      }, attachItem, {workspaceId}, (err, count) => {
        return callback(err, !!count);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string, noteGlobalId:string}} inputData
   */
  static async updateNeedSync(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId, noteGlobalId, displayName} = inputData;
      if(!globalId || !noteGlobalId) {
        return resolve(0);
      }

      attachModel.update({
        globalId,
        noteGlobalId
      }, {needSync: false, displayName}, {workspaceId}, (err, count) => {
        if(err || !count) {
          return resolve(0);
        }
        return resolve(count);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string, location:string}} inputData
   */
  static async updateLocation(inputData) {
    return new Promise((resolve) => {
      const {workspaceId, globalId, location} = inputData;
      if(!globalId || !location) return resolve(false);
      attachModel.update({globalId}, {location}, {workspaceId}, (err, count) => {
        if(err || !count) {
          return resolve(0);
        }
        return resolve(count);
      });
    });
  }

  /**
   * @param {{workspaceId:string, savedAttachments: {location:string}}} inputData
   */
  static async updateLocations(inputData) {
    const { workspaceId, savedAttachments } = inputData;
    if(!savedAttachments) return;
    if(!Object.keys(savedAttachments).length) return;

    for(let globalId in savedAttachments) {
      if(!savedAttachments.hasOwnProperty(globalId)) continue;
      const attachment = savedAttachments[globalId];
      if(!globalId || !attachment) continue;
      if(!attachment.location) continue;
      await AttachmentObjRepository.updateLocation({
        workspaceId,
        globalId,
        location: attachment.location,
      });
    }
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   */
  static async getInListCount(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, noteGlobalId} = inputData;
      if(!noteGlobalId) {
        return resolve(0);
      }

      attach.count({
        noteGlobalId,
        inList: true
      }, {workspaceId}, (err, itemAttachCount) => {
        if(err || !itemAttachCount) {
          return resolve(0);
        }
        return resolve(itemAttachCount);
      });
    });
  }

  static cleanUploadItem(attachment) {
    if(!attachment) { return attachment }

    const cleanProps = [
        'location',
        'size',
        'mime',
        'file_uuid',
        'extension',
    ]

    for(let prop in attachment) {
      if(!attachment.hasOwnProperty(prop)) { continue }

      if(prop && prop.indexOf('UpdateTime') > 0) {
        delete attachment[prop]
        continue
      }

      if(cleanProps.indexOf(prop) >= 0) {
        delete attachment[prop]
      }
    }

    return { ...attachment }
  }
}
