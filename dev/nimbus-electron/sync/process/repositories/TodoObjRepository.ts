import chalk from "chalk";
import {default as config} from "../../../../config";
import {default as NimbusSDK} from "../../nimbussdk/net/NimbusSDK";
import {default as TodoObj} from "../db/TodoObj";
import {default as NoteObjRepository} from "./NoteObjRepository";
import {default as SyncTodoEntity} from "../../nimbussdk/net/response/entities/SyncTodoEntity";
import {default as todo, default as todoModel} from "../../../db/models/todo";
import {default as socketConnection} from "../../socket/socketFunctions";
import {default as socketStoreType} from "../../socket/socketStoreType";

export default class TodoObjRepository {
  /**
   * @param {{workspaceId:string, parentId:string}} inputData
   * @param {Function} callback
   */
  static create(inputData, callback = (err, res) => {
  }) {
    const {parentId} = inputData;
    NimbusSDK.getAccountManager().getUniqueUserName((err, uniqueUserName) => {
      let todo = new TodoObj();
      todo.globalId = TodoObjRepository.generateGlobalId();
      todo.parentId = parentId;
      todo.dateAdded = new Date().getTime();
      todo.label = null;
      todo.checked = false;
      todo.uniqueUserName = uniqueUserName;
      todo.needSync = true;
      todo.syncDate = 0;

      callback(err, todo);
    });
  }

  /**
   * @return {string}
   */
  static generateGlobalId() {
    return NoteObjRepository.generateGlobalId();
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @param {Function} callback
   */
  static getSyncTodoEntityList(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, noteGlobalId} = inputData;
    let findQuery = {noteGlobalId: noteGlobalId};
    let options = <any>{workspaceId};
    options.findParams = {
      selector: findQuery,
      sort: [{"orderNumber": "asc"}]
    };

    todoModel.findAll(findQuery, options, (err, todoObjs) => {
      let todoList = [];
      for (let todoObj of todoObjs) {
        let todo = new SyncTodoEntity();
        todo.global_id = todoObj.globalId;
        todo.checked = todoObj.checked;
        todo.label = todoObj.label;
        todo.date = todoObj.dateAdded;
        todoList.push(todo);
      }
      callback(err, todoList);
    });
  }

  /**
   * @param {{workspaceId:string, todoObjs:[TodoObj]}} inputData
   * @param {Function} callback
   */
  static updateNoteTodosDownloadedFromServerI(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, todoObjs} = inputData;
    if (todoObjs && todoObjs.length > 0) {
      todoModel.findAll({
        "parentId": todoObjs[0].parentId
      }, {workspaceId}, (err, list) => {
        // @ts-ignore
        todoModel.remove({"parentId": todoObjs[0].parentId}, {}, async (err, response) => {
          let result = await TodoObjRepository.update({workspaceId, todoObjs: list});
          callback(err, result);
        });
      });
    } else {
      callback(null, true);
    }
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   * @return {TodoObj[]}
   */
  static getUserTodoR(inputData, callback = (err, res) => {
  }) {
    TodoObjRepository.getAllTodoR(inputData, (err, list) => {
      callback(err, list);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getAllTodoR(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    todoModel.findAll({}, {workspaceId}, (err, list) => {
      callback(err, list);
    });
  }

  /**
   * @param {{workspaceId:string, todoObjs:[TodoObj]}} inputData
   */
  static async update(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, todoObjs} = inputData;
      if (!todoObjs) {
        return resolve(false);
      }

      /**
       * @param {{workspaceId:string, globalId:string}} inputData
       * @return {Promise<TodoObj|null>}
       */
      let getUserTodo = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, globalId} = inputData;
          todoModel.find({globalId: globalId}, {workspaceId}, (err, todoObj) => {
            if (err || !todoObj) {
              return resolve(null);
            }
            resolve(todoObj);
          });
        });
      };

      /**
       * @param {{workspaceId:string, item:TodoObj}} inputData
       * @return {Promise<boolean>}
       */
      let updateUserTodo = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, item} = inputData;
          if (config.SHOW_WEB_CONSOLE) {
            //console.log("update:", item);
          }

          todoModel.update({
            globalId: item.globalId,
            noteGlobalId: item.noteGlobalId
          }, item, {workspaceId}, (err, count) => {
            if (err || !count) {
              return resolve(false);
            }
            resolve(!!count);
          });
        });
      };

      /**
       * @param {{workspaceId:string, item:TodoObj}} inputData
       * @return {Promise<boolean>}
       */
      let createUserTodo = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, item} = inputData;
          if (config.SHOW_WEB_CONSOLE) {
            //console.log("save:", item);
          }

          todoModel.add(item, {workspaceId}, (err, todoInstance) => {
            if (err || !todoInstance) {
              return resolve(false);
            }
            resolve(!!todoInstance);
          });
        });
      };

      /**
       * @param {{workspaceId:string, todoObj:TodoObj}} inputData
       * @return {Promise<boolean>}
       */
      let processUserTodo = async (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {workspaceId, todoObj} = inputData;
          let existItemInstance = await getUserTodo({workspaceId, globalId: todoObj.globalId});

          let saveInstance = <{orderNumber:number, needSync:boolean}>await TodoObjRepository.convertSyncInstanceToLocal(todoObj, existItemInstance);
          if (!saveInstance) {
            return resolve(false);
          }

          if (typeof (todoObj.orderNum) !== "undefined") {
            saveInstance.orderNumber = todoObj.orderNum;
          }

          saveInstance['needSync'] = false;

          let savedInstance = null;
          if (existItemInstance) {
            savedInstance = await updateUserTodo({workspaceId, item: saveInstance});
            resolve(savedInstance);
          } else {
            savedInstance = await createUserTodo({workspaceId, item: saveInstance});
            resolve(savedInstance);
          }
        });
      };

      let orderNumber = 1;
      for (let todoObj of todoObjs) {
        todoObj.orderNumber = orderNumber;
        orderNumber++;
        let result = await processUserTodo({workspaceId, todoObj});
        if (result) {
          TodoObjRepository.sendSocketEventOnUpdate({
            workspaceId,
            noteGlobalId: todoObj.noteGlobalId,
            globalId: todoObj.globalId
          });
        }
      }

      resolve(true);
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string, globalId:string}} inputData
   */
  static sendSocketEventOnUpdate(inputData) {
    const {workspaceId, noteGlobalId, globalId} = inputData;
    if (globalId && noteGlobalId) {
      socketConnection.addStoreData({
        workspaceId,
        storeType: socketStoreType.TODO_LIST_FOR_UPDATE,
        data: {
          noteGlobalId: noteGlobalId,
          globalId: globalId
        }
      });
    }
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string, globalId:string}} inputData
   */
  static sendSocketEventOnRemove(inputData) {
    const {workspaceId, noteGlobalId, globalId} = inputData;
    if (globalId && noteGlobalId) {
      socketConnection.addStoreData({
        workspaceId,
        storeType: socketStoreType.TODO_LIST_FOR_REMOVE,
        data: {
          noteGlobalId: noteGlobalId,
          globalId: globalId
        }
      });
    }
  }

  /**
   * @param {TodoObj} todoObj
   * @param {item} existItemInstance
   * @return {Promise<Object|null>}
   */
  static async convertSyncInstanceToLocal(todoObj, existItemInstance) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (!todoObj) {
        return resolve(null);
      }

      /**
       * @param {item} item
       * @param {} data
       * @return {any}
       */
      let fillSyncObjProperties = (item, data) => {
        return todoModel.prepareSyncOnlyProperties(todoModel.prepareItemDbProperties(item, data), data);
      };

      let fillModelProperties = (item, data) => {
        return todoModel.prepareCommonProperties(todoModel.prepareItemDbProperties(item, data), data);
      };

      let todoInstance = null;
      if (existItemInstance) {
        todoInstance = fillSyncObjProperties(todoModel.prepareModelData(existItemInstance), todoObj);
        if (todoInstance.label !== todoObj.label) {
          todoInstance.label = todoObj.label;
        }
        if (todoInstance.checked !== todoObj.checked) {
          todoInstance.checked = todoObj.checked;
        }
      } else {
        todoInstance = fillModelProperties(todoModel.prepareModelData(todoObj), todoObj);
      }

      resolve(todoInstance);
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   */
  static async findNoteTodoList(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, noteGlobalId} = inputData;
      todoModel.findAll({
        "noteGlobalId": noteGlobalId
      }, {workspaceId}, (err, todoObjs) => {
        if (err) {
          return resolve([]);
        }
        resolve(todoObjs);
      });
    });
  }

  /**
   * @param {{workspaceId:string, todoList:[TodoObj]}} inputData
   */
  static async removeNoteTodoList(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, todoList} = inputData;
      if (!todoList.length) {
        return resolve(true);
      }

      let todoGlobalIdList = [];
      for (let todoInstance of todoList) {
        TodoObjRepository.sendSocketEventOnRemove({
          workspaceId,
          noteGlobalId: todoInstance.noteGlobalId,
          globalId: todoInstance.globalId
        });
        todoGlobalIdList.push(todoInstance.globalId);
      }

      if (!todoGlobalIdList.length) {
        return resolve(true);
      }

      todoModel.erase({
        "globalId": {"$in": todoGlobalIdList}
      }, {workspaceId}, (err, numRemoved) => {
        if (err) {
          return resolve(false);
        }
        resolve(true);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async deleteData(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId} = inputData;
      todoModel.erase({
        "globalId": globalId
      }, {workspaceId}, (err, response) => {
        if (err) {
          return resolve(null);
        }
        resolve(response);
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param callback
   */
  static clearRemovedData(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    todoModel.findAll({
      "erised": true
      // @ts-ignore
    }, {workspaceId}, async (err, todoObjs) => {
      for (let todoObj of todoObjs) {
        await TodoObjRepository.deleteData({workspaceId, globalId: todoObj.globalId});
      }
      callback(null, true);
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string, noteGlobalId:string}} inputData
   */
  static async updateNeedSync(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId, noteGlobalId} = inputData;
      if(!globalId || !noteGlobalId) {
        return resolve(0);
      }

      todoModel.update({
        globalId,
        noteGlobalId
      }, {needSync: false}, {workspaceId}, (err, count) => {
        if(err || !count) {
          return resolve(0);
        }
        return resolve(count);
      });
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   */
  static async getUncheckedCount(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, noteGlobalId} = inputData;
      if(!noteGlobalId) {
        return resolve(0);
      }

      todo.count({
        noteGlobalId,
        checked: false
      }, {workspaceId}, (err, itemTodoCount) => {
        if(!err || itemTodoCount) {
          return resolve(0);
        }
        return resolve(itemTodoCount);
      });
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   */
  static async getCount(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, noteGlobalId} = inputData;
      if(!noteGlobalId) {
        return resolve(0);
      }

      todo.count({
        noteGlobalId
      }, {workspaceId}, (err, itemTodoCount) => {
        if(!err || itemTodoCount) {
          return resolve(0);
        }
        return resolve(itemTodoCount);
      });
    });
  }
}
