import chalk from "chalk";
import {default as userModel} from "../../../db/models/user";

export default class UserObjRepository {
  /**
   * @param {{queryData:{}, updateData:{}}} inputData
   * @param {Function} callback
   */
  static update(inputData, callback = (err, res) => {
  }) {
    const {queryData, updateData} = inputData;
    userModel.update(queryData, updateData, {}, callback);
  }

  /**
   * @param {int} days
   * @return {int}
   */
  static getDateNextQuotaReset(days) {
    return userModel.getDateNextQuotaReset(days);
  }
}
