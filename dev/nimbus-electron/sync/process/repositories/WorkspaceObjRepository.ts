import chalk from "chalk";
import fs from "fs-extra";
import {default as config} from "../../../../config";
import {default as WorkspaceObj} from "../db/WorkspaceObj";
import {default as workspaceModel} from "../../../db/models/workspace";
import {default as SyncWorkspaceEntity} from "../../nimbussdk/net/response/entities/SyncWorkspaceEntity";
import {default as OrgObjRepository} from "./OrgObjRepository";
import {default as appWindow} from "../../../window/instance";
import {default as itemModel} from "../../../db/models/item";
import {default as FolderObj} from "../db/FolderObj";
import {default as attach} from "../../../db/models/attach";
import {default as PouchDb} from "../../../../pdb";

import {default as SyncManager} from "../../../sync/process/SyncManager";
import {default as AccountManager} from "../../../sync/nimbussdk/manager/AccountManager";
import AvatarSingleDownloader from "../../downlaoder/AvatarSingleDonwloader";
import {clearDirectory} from "../../../utilities/clearHandler";

export default class WorkspaceObjRepository {
  /**
   * @param {{globalId:string}} inputData
   * @param {Function} callback
   */
  static get(inputData, callback = (err, res) => {
  }) {
    let {globalId} = inputData;
    if (globalId === null) {
      globalId = "";
    }

    workspaceModel.find({globalId}, {}, (err, workspaceObj) => {
      if (err || !workspaceObj) {
        return callback(err, null);
      }
      callback(err, {...workspaceObj});
    });
  };

  /**
   * @param {{workspace:WorkspaceObj}} inputData
   * @param {Function} callback
   */
  static create(inputData, callback = (err, res) => {
  }) {
    const {workspace} = inputData;
    if (!workspace) {
      return callback(null, null);
    }
    let workspaceObj = new WorkspaceObj();
    workspaceObj.userId = workspace.userId;
    workspaceObj.globalId = workspace.globalId;
    workspaceObj.title = workspace.title;
    workspaceObj.isDefault = workspace.isDefault;
    workspaceObj.createdAt = workspace.createdAt;
    workspaceObj.updatedAt = workspace.updatedAt;
    workspaceObj.countMembers = workspace.countMembers;
    workspaceObj.countInvites = 0;
    workspaceObj.access = null;
    workspaceObj.defaultEncryptionKeyId = null;
    workspaceObj.syncDate = 0;
    workspaceObj.needSync = true;
    workspaceObj.isNotesLimited = workspace.isNotesLimited;
    workspaceObj.user = workspace.user;
    workspaceObj.notesEmail = workspace.notesEmail;
    workspaceObj.avatar = workspace.avatar || null;
    workspaceObj.color = workspace.color || null;
    return callback(null, workspaceObj);
  }

  /**
   * @param {{workspaceObjs: WorkspaceObj|[WorkspaceObj]}} inputData
   */
  static async update(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceObjs} = inputData;
      if (!workspaceObjs) {
        return resolve(false);
      }

      /**
       * @param {WorkspaceObj} workspaceObj
       */
      let processUserWorkspace = async (workspaceObj) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {org} = workspaceObj;
          workspaceObj.orgId = org ? org.id : null;

          let existItemInstance = <{avatar:{url:string}}>await WorkspaceObjRepository.getUserWorkspace({globalId: workspaceObj.globalId});
          let saveInstance = <{needSync:boolean, globalId:string, avatar:{url:string}}>await WorkspaceObjRepository.convertSyncInstanceToLocal(workspaceObj, existItemInstance);
          if (!saveInstance) {
            return resolve(false);
          }

          const saveAvatarData = <any>{};
          if (typeof (saveInstance.avatar) !== 'undefined') {
            if(saveInstance.avatar) {
              if (existItemInstance && existItemInstance.avatar) {
                if(existItemInstance.avatar.url !== saveInstance.avatar.url) {
                  saveAvatarData.avatar = saveInstance.avatar;
                } else {
                  const avatarFileExist = await AvatarSingleDownloader.checkAvatarExist(saveInstance.avatar.url);
                  if(!avatarFileExist) {
                    saveAvatarData.avatar = saveInstance.avatar;
                  }
                }
              } else {
                saveAvatarData.avatar = saveInstance.avatar;
              }
            } else {
              saveAvatarData.avatar = null;
            }
          }

          if(Object.keys(saveAvatarData).length) {
            if (typeof (saveAvatarData.avatar) !== 'undefined') {
              if(saveAvatarData.avatar) {
               AvatarSingleDownloader.download({
                  url: saveAvatarData.avatar.url,
                  oldUrl: existItemInstance && existItemInstance.avatar ? existItemInstance.avatar.url : '',
                  type: AvatarSingleDownloader.AVATAR_TYPE_WORKSPACE,
                });
              }
            }
          }

          saveInstance.needSync = false;
          if (existItemInstance) {
            let updateUserWorkspace = await WorkspaceObjRepository.updateUserWorkspace({item: saveInstance});
            resolve(updateUserWorkspace);
          } else {
            let createdUserWorkspace = await WorkspaceObjRepository.createUserWorkspace({item: saveInstance});
            resolve(createdUserWorkspace);
          }
        });
      };

      let result = null;
      if (workspaceObjs instanceof Array) {
        for (let workspaceObj of workspaceObjs) {
          result = await processUserWorkspace(workspaceObj);
        }
      } else {
        result = await processUserWorkspace(workspaceObjs);
      }
      resolve(result);
    });
  }

  /**
   * @param {{workspaceObjs: WorkspaceObj|[WorkspaceObj]}} inputData
   */
  static async remove(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceObjs} = inputData;
      if (!workspaceObjs) {
        return resolve(false);
      }

      let result = false;
      if (workspaceObjs instanceof Array) {
        for (let workspaceObj of workspaceObjs) {
          result = <boolean>await WorkspaceObjRepository.removeUserWorkspace({item: workspaceObj});
        }
      } else {
        result = <boolean>await WorkspaceObjRepository.removeUserWorkspace({item: workspaceObjs});
      }
      resolve(result);
    });
  }

  /**
   * @param {{globalId:string}} inputData
   * @return {Promise<FolderObj|null>}
   */
  static async getUserWorkspace(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {globalId} = inputData;
      workspaceModel.find({"globalId": globalId}, {}, (err, workspaceObj) => {
        if (err || !workspaceObj) {
          return resolve(null);
        }
        resolve(workspaceObj);
      });
    });
  }

  /**
   * @param {{item:WorkspaceObj}} inputData
   * @return {Promise<boolean>}
   */
  static async removeUserWorkspace(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      let {item} = inputData;
      if (!item) {
        return resolve(false);
      }

      await WorkspaceObjRepository.removeWorkspaceData(inputData);

      workspaceModel.erase({
        "globalId": item.globalId,
        "erised": {"$in": [true, false]}
      }, {}, async (err, count) => {
        if (err || !count) {
          return resolve(false);
        }

        WorkspaceObjRepository.sendSocketEventOnRemove([item.globalId]);
        resolve(!!count);
      });
    });
  };

  /**
   * @param {{item:WorkspaceObj}} inputData
   * @return {Promise}
   */
  static async removeWorkspaceData(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      let {item} = inputData;
      if (!item) {
        return resolve(false);
      }

      const workspaceId = item.globalId;
      await SyncManager.stopSync(workspaceId);

      const query = {
        "type": "note",
        "erised": {"$in": [true, false]}
      };
      itemModel.findAll(query, {workspaceId}, async (err, noteObjs) => {
        if (err || !noteObjs) {
          return resolve(true);
        }

        /**
         * @param {{workspaceId:string, attachItem:AttachmentObj}} inputData
         */
        const removeUserWorkspaceAttachment = (inputData) => {
          // @ts-ignore
          return new Promise(async (resolve) => {
            const {workspaceId, attachItem} = inputData;
            let removeAttachQuery = {globalId: attachItem.globalId};
            attach.remove(removeAttachQuery, {workspaceId}, async () => {
              if (PouchDb.getClientAttachmentPath()) {
                let targetPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
                // @ts-ignore
                let exists = await fs.exists(targetPath);
                // @ts-ignore
                if (exists) {
                  try {
                    fs.unlink(targetPath, () => {
                    });
                  } catch (e) {
                    if (config.SHOW_WEB_CONSOLE) {
                      console.log("Error => removeUserWorkspaceAttachment: ", targetPath);
                    }
                  }
                }
              }
              return resolve(true);
            });
          });
        };

        /**
         * @param {{workspaceId:string, noteObj:NoteObj}} inputData
         */
        const removeUserWorkspaceAttachments = (inputData) => {
          // @ts-ignore
          return new Promise(async (resolve) => {
            const {workspaceId, noteObj} = inputData;
            // @ts-ignore
            attach.findAll({noteGlobalId: noteObj.globalId}, {workspaceId}, async (err, attachList) => {
              if (attachList && attachList.length) {
                for (let attachItem of attachList) {
                  await removeUserWorkspaceAttachment({workspaceId, attachItem});
                }
              }
              return resolve(true);
            });
          });
        };

        for (let noteObj of noteObjs) {
          await removeUserWorkspaceAttachments({workspaceId, noteObj});
        }

        const dbDirPath = PouchDb.getClientDbPath();
        // @ts-ignore
        if (dbDirPath && await fs.exists(dbDirPath)) {
          const workspaceDirPath = `${dbDirPath}/${workspaceId}`;
          await clearDirectory(workspaceDirPath);
          // @ts-ignore
          let exists = await fs.exists(workspaceDirPath);
          // @ts-ignore
          if (exists) {
            try {
              fs.rmdir(workspaceDirPath, () => {
              });
            } catch (e) {
              if (config.SHOW_WEB_CONSOLE) {
                console.log("Error => removeUserWorkspaceAttachments: ", workspaceDirPath, e);
              }
            }
          } else {
            if (config.SHOW_WEB_CONSOLE) {
              console.log("Error => removeWorkspaceData => empty workspaceDirPath: ", workspaceDirPath);
            }
          }
        } else {
          if (config.SHOW_WEB_CONSOLE) {
            console.log("Error => removeWorkspaceData => empty dbDirPath: ", dbDirPath);
          }
        }

        AccountManager.setLastUpdateTime({
          workspaceId,
          lastUpdateTime: 1
        });

        return resolve(true);
      });
    });
  }

  /**
   * @param {{item:WorkspaceObj}} inputData
   * @return {Promise}
   */
  static async updateUserWorkspace(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      let {item} = inputData;

      if (config.SHOW_WEB_CONSOLE) {
        //console.log("update:", item);
      }

      workspaceModel.update({"globalId": item.globalId}, item, {}, async (err, count) => {
        if (err || !count) {
          return resolve(false);
        }

        WorkspaceObjRepository.sendSocketEventOnUpdate(item.globalId, false);
        resolve(!!count);
      });
    });
  }

  /**
   * @param {{item:WorkspaceObj}} inputData
   */
  static async createUserWorkspace(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {item} = inputData;
      if (config.SHOW_WEB_CONSOLE) {
        //console.log("save:", item);
      }

      workspaceModel.add(item, {}, (err, itemInstance) => {
        if (err || !itemInstance) {
          return resolve(false);
        }

        WorkspaceObjRepository.sendSocketEventOnUpdate(item.globalId, true);
        resolve(!!itemInstance);
      });
    });
  }

  /**
   * @param {string} globalId
   * @param {boolean} isCreate
   */
  // @ts-ignore
  static async sendSocketEventOnUpdate(globalId, isCreate) {
    const workspace = await WorkspaceObjRepository.getUserWorkspace({globalId});
    const workspaceData = await workspaceModel.getResponseJson(workspace);
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:update:workspace:response', {
        workspace: workspaceData
      });
    }
  }

  /**
   * @param {[]} globalIdList
   */
  static sendSocketEventOnRemove(globalIdList) {
    if (globalIdList && globalIdList.length) {
      for (let globalId of globalIdList) {
        if (appWindow.get()) {
          appWindow.get().webContents.send('event:client:remove:workspace:response', {
            workspaceId: globalId
          });
        }
      }
    }
  }

  /**
   * @param {string} workspaceId
   */
  // @ts-ignore
  static sendSocketEventOnUpdateMembers(workspaceId) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:update:workspace:members:response', {workspaceId});
    }
  }

  /**
   * @param {string} workspaceId
   */
  // @ts-ignore
  static sendSocketEventOnUpdateInvites(workspaceId) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:update:workspace:invites:response', {workspaceId});
    }
  }

  static sendSocketEventOnWorkspacesAccessChange() {
    // @ts-ignore
    workspaceModel.findAll({}, {}, async (err, workspacesList) => {
      if (err || !workspacesList.length) {
        const defaultWorkspace = await workspaceModel.getDefault();
        workspacesList = [defaultWorkspace];
      }
      const workspaces = <[any]>await workspaceModel.getResponseListJson(workspacesList);
      let workspacesAccess = workspaces
        .filter((workspaceInstance) => (workspaceInstance.access))
        .map((workspaceInstance) => (workspaceInstance.access));

      if (appWindow.get()) {
        appWindow.get().webContents.send('event:client:update:workspaces:access:response', {workspacesAccess});
        appWindow.get().webContents.send('event:client:update:workspaces:premium:response', {});
      }
    });
  }

  /**
   * @param {WorkspaceObj} workspaceObj
   * @param {item} existItemInstance
   * @return {Promise<item|null>}
   */
  static async convertSyncInstanceToLocal(workspaceObj, existItemInstance) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (!workspaceObj) {
        return resolve(null);
      }

      let fillSyncObjProperties = (item, data) => {
        return workspaceModel.prepareSyncOnlyProperties(workspaceModel.prepareItemDbProperties(item, data), data);
      };

      let fillModelProperties = (item, data) => {
        return workspaceModel.prepareCommonProperties(workspaceModel.prepareItemDbProperties(item, data), data);
      };

      let workspaceInstance = null;
      if (existItemInstance) {
        workspaceInstance = fillSyncObjProperties(workspaceModel.prepareModelData(existItemInstance), workspaceObj);
      } else {
        workspaceInstance = fillModelProperties(workspaceModel.prepareModelData(workspaceObj), workspaceObj);
      }

      resolve(workspaceInstance);
    });
  }

  /**
   * @param {Function} callback
   */
  static getErasedWorkspacesForUploadOnServer(callback = (err, res) => {
  }) {
    workspaceModel.findAll({
      "erised": true,
      "needSync": true
    }, {}, (err, workspacesObjs) => {
      let workspaces = [];
      for (let workspaceObjs of workspacesObjs) {
        workspaces.push(workspaceObjs.globalId);
      }
      callback(err, workspaces);
    });
  };

  /**
   * @param {Function} callback
   */
  static getUpdatedWorkspacesForUploadOnServer(callback = (err, res) => {
  }) {
    workspaceModel.findAll({
      "needSync": true
    }, {
      "order": {"updatedAt": "desc"}
    }, (err, noteObjs) => {
      if (err) {
        return callback(err, []);
      }

      callback(err, noteObjs);
    });
  }

  /**
   * @param {Function} callback
   */
  static getUpdatedWorkspacesCountForUploadOnServer(callback = (err, res) => {
  }) {
    workspaceModel.findAll({
      "needSync": true
    }, {
      "order": {"updatedAt": "desc"}
    }, (err, workspaceObjs) => {
      callback(err, workspaceObjs.length);
    });
  }

  /**
   * @param {string} globalId
   * @param {Function} callback
   */
  static getSyncWorkspaceEntity(globalId, callback = (err, res) => {
  }) {
    // @ts-ignore
    workspaceModel.find({"globalId": globalId}, {}, async (err, workspaceObj) => {
      let workspace = new SyncWorkspaceEntity();
      if (workspaceObj) {
        workspace.globalId = workspaceObj.globalId;
        workspace.title = workspaceObj.title;
        workspace.org = await OrgObjRepository.getUserOrg({globalId: workspaceObj.orgId});
        workspace.userId = workspaceObj.userId;
        workspace.createdAt = workspaceObj.createdAt;
        workspace.updatedAt = workspaceObj.updatedAt;
        workspace.isDefault = workspaceObj.isDefault;
        workspace.countMembers = workspaceObj.countMembers;
        workspace.countInvites = workspaceObj.countInvites;
        workspace.access = workspaceObj.access;
        workspace.isNotesLimited = workspaceObj.isNotesLimited;
      }
      callback(err, workspace);
    });
  }

  /**
   * @param {WorkspaceObj} workspaceObj
   * @param {Function} callback
   */
  static async updateWorkspaceDownloadedFromServerI(workspaceObj, callback = (err, res) => {
  }) {
    if (!workspaceObj) {
      return callback(null, false);
    }

    let result = await WorkspaceObjRepository.update({workspaceObjs: workspaceObj});
    callback(null, result);
  }

  /**
   * @param {WorkspaceObj[]} workspaceObjs
   * @param {Function} callback
   */
  static async updateWorkspacesDownloadedFromServerI(workspaceObjs, callback = (err, res) => {
  }) {
    if (!workspaceObjs.length) {
      return callback(null, false);
    }

    let result = await WorkspaceObjRepository.update({workspaceObjs: workspaceObjs});
    callback(null, result);
  }

  /**
   * @param {Function} callback
   */
  static getUserWorkspacesR(callback = (err, res) => {
  }) {
    WorkspaceObjRepository.getAllWorkspacesR((err, list) => {
      callback(err, list);
    });
  }

  /**
   * Always return just user notes
   * @param {Function} callback
   */
  static getAllWorkspacesR(callback = (err, res) => {
  }) {
    workspaceModel.findAll({}, {}, (err, list) => {
      return callback(err, list);
    });
  }

  /**
   * @param {string} globalId
   * @param {Function} callback
   */
  static getR(globalId, callback = (err, res) => {
  }) {
    if (globalId === null) {
      globalId = "";
    }

    workspaceModel.find({globalId}, {}, (err, item) => {
      callback(err, item);
    });
  }


  /**
   * @param callback
   */
  static clearRemovedData(callback = (err, res) => {
  }) {
    workspaceModel.findAll({
      "erised": true,
      // @ts-ignore
    }, {}, async (err, workspaceObjs) => {
      for (let workspaceObj of workspaceObjs) {
        // delete notes/folders/attachments/todo/tags
      }
      callback(null, true);
    });
  }
}
