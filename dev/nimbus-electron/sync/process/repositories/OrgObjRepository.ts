import chalk from "chalk";
import {default as config} from "../../../../config";
import orgs, {default as orgModel} from "../../../db/models/orgs";
import {default as OrgEntity} from "../../nimbussdk/net/response/entities/OrgEntity";
import {default as appWindow} from "../../../window/instance";
import OrgObj from "../db/OrgObj";
import ModelOrgs from "../../../db/models/orgs";
import SelectedOrganization from "../../../organization/SelectedOrganization";
import {default as auth} from "../../../auth/auth";
import {default as appPage} from "../../../request/page";
import {default as SyncManager} from "../SyncManager";
import AvatarSingleDownloader from "../../downlaoder/AvatarSingleDonwloader";

export default class OrgObjRepository {
  static get(inputData, callback = (err, res) => {
  }) {
    const {id} = inputData;

    orgModel.find({id}, {}, (err, orgObj) => {
      if (err || !orgObj) {
        return callback(err, null);
      }
      callback(err, {...orgObj});
    });
  }

  /**
   * @param {{organization:OrgObj}} inputData
   * @param {Function} callback
   */
  static create(inputData, callback = (err, res) => {
  }) {
    const {organization} = inputData;
    if (!organization) {
      return callback(null, null);
    }
    let organizationObj = new OrgObj();
    organizationObj.id = organization.id;
    organizationObj.type = organization.type;
    organizationObj.serviceType = organization.serviceType;
    organizationObj.title = organization.title;
    organizationObj.description = organization.description;
    organizationObj.usage = organization.usage;
    organizationObj.limits = organization.limits;
    organizationObj.features = organization.features;
    organizationObj.user = organization.user;
    organizationObj.sub = organization.sub;
    organizationObj.suspended = organization.suspended;
    organizationObj.suspendedAt = organization.suspendedAt;
    organizationObj.suspendedReason = organization.suspendedReason;
    organizationObj.access = organization.access;
    organizationObj.smallLogoUrl = organization.smallLogoUrl;
    organizationObj.bigLogoUrl = organization.bigLogoUrl;
    organizationObj.syncDate = 0;
    organizationObj.needSync = true;
    return callback(null, organizationObj);
  }

  /**
   * @param {{orgObjs: OrgEntity|[OrgEntity]}} inputData
   */
  static async update(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {orgObjs} = inputData;
      if (!orgObjs) {
        return resolve(false);
      }

      /**
       * @param {OrgEntity} orgObj
       */
      let processUserOrg = async (orgObj) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          let existItemInstance = <{id:string, suspended:boolean, type:string, smallLogoUrl: string}>await OrgObjRepository.getUserOrg({id: orgObj.id});
          let saveInstance = <{needSync:boolean, suspended:boolean, smallLogoUrl: string}>await OrgObjRepository.convertSyncInstanceToLocal(orgObj, existItemInstance);

          if (!saveInstance) {
            return resolve(false);
          }


          if(existItemInstance && existItemInstance.type === ModelOrgs.TYPE_BUSINESS) {
            if(!existItemInstance.suspended && saveInstance.suspended) {
              const authInfo = <{userId, variables}>await auth.fetchActualUserAsync();
              const activeOrganization = await SelectedOrganization.getCurrentOrganization(authInfo);
              if(activeOrganization && existItemInstance.id === activeOrganization.globalId) {
                  setTimeout(() => {
                    SyncManager.stopAllSync();
                    appPage.reload(false);
                  }, 3000);
              }
            }
          }

          let smallLogoUrl;
          if (typeof (saveInstance.smallLogoUrl) !== 'undefined') {
            if(saveInstance.smallLogoUrl) {
              if (existItemInstance && existItemInstance.smallLogoUrl) {
                if(existItemInstance.smallLogoUrl !== saveInstance.smallLogoUrl) {
                  smallLogoUrl = saveInstance.smallLogoUrl;
                } else {
                  const avatarFileExist = await AvatarSingleDownloader.checkAvatarExist(saveInstance.smallLogoUrl);
                  if(!avatarFileExist) {
                    smallLogoUrl = saveInstance.smallLogoUrl;
                  }
                }
              } else {
                smallLogoUrl = saveInstance.smallLogoUrl;
              }
            } else {
              smallLogoUrl = null;
            }
          }

          if(smallLogoUrl) {
            AvatarSingleDownloader.download({
              url: smallLogoUrl,
              oldUrl: existItemInstance && existItemInstance.smallLogoUrl ? existItemInstance.smallLogoUrl : '',
              type: AvatarSingleDownloader.AVATAR_TYPE_ORGANIZATION,
            });
          }

          saveInstance.needSync = false;
          if (existItemInstance) {
            let updateOrg = await OrgObjRepository.updateUserOrg({item: saveInstance});
            resolve(updateOrg);
          } else {
            let createdOrg = await OrgObjRepository.createUserOrg({item: saveInstance});
            resolve(createdOrg);
          }
        });
      };

      let result = null;
      if (orgObjs instanceof Array) {
        for (let orgObj of orgObjs) {
          result = await processUserOrg(orgObj);
        }
      } else {
        result = await processUserOrg(orgObjs);
      }
      resolve(result);
    });
  }

  /**
   * @param {{id:string}} inputData
   * @return {Promise<FolderObj|null>}
   */
  static async getUserOrg(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {id} = inputData;
      orgModel.find({id}, {}, (err, orgObj) => {
        if (err || !orgObj) {
          return resolve(null);
        }
        resolve(orgObj);
      });
    });
  }

  /**
   * @param {{item:OrgEntity}} inputData
   * @return {Promise}
   */
  static async updateUserOrg(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      let {item} = inputData;

      if (config.SHOW_WEB_CONSOLE) {
        //console.log("update:", item);
      }

      orgModel.update({id: item.id}, item, {}, async (err, count) => {
        if (err || !count) {
          return resolve(false);
        }

        OrgObjRepository.sendSocketEventOnUpdate(item.id, false);
        resolve(!!count);
      });
    });
  }

  /**
   * @param {{item:OrgEntity}} inputData
   */
  static async createUserOrg(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {item} = inputData;
      if (config.SHOW_WEB_CONSOLE) {
        //console.log("save:", item);
      }

      orgModel.add(item, {}, (err, itemInstance) => {
        if (err || !itemInstance) {
          return resolve(false);
        }

        OrgObjRepository.sendSocketEventOnUpdate(item.id, true);
        resolve(!!itemInstance);
      });
    });
  }

  /**
   * @param {{item:WorkspaceObj}} inputData
   * @return {Promise<boolean>}
   */
  static async removeUserOrg(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      let {item} = inputData;
      if (!item) {
        return resolve(false);
      }

      orgModel.erase({
        "id": item.id,
        "erised": {"$in": [true, false]}
      }, {}, async (err, count) => {
        if (err || !count) {
          return resolve(false);
        }

        OrgObjRepository.sendSocketEventOnRemove([item.id]);
        resolve(!!count);
      });
    });
  };

  /**
   * @param {string} id
   * @param {boolean} isCreate
   */
  // @ts-ignore
  static async sendSocketEventOnUpdate(id, isCreate) {
    const organization = await OrgObjRepository.getUserOrg({id});
    if(!organization) {
      return;
    }

    if(organization['type'] !== orgs.TYPE_BUSINESS) {
      return;
    }

    const organizations = <[orgs]>await orgs.getResponseListJson([organization]);
    const organizationData = organizations && organizations.length ? organizations[0] : null;
    if(!organizationData) {
      return;
    }

    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:update:organization:response', {
        org: organizationData
      });
    }
  }

  /**
   * @param {[]} idList
   */
  static async sendSocketEventOnRemove(idList) {
    if (idList && idList.length) {
      for (let id of idList) {
        const organization = await OrgObjRepository.getUserOrg({id});
        const organizationData = await orgModel.getResponseJson(organization);
        if(organizationData && organizationData.type !== orgs.TYPE_BUSINESS) {
          return;
        }

        if (appWindow.get()) {
          appWindow.get().webContents.send('event:client:remove:organization:response', {id});
        }
      }
    }
  }

  /**
   * @param {OrgEntity} orgObj
   * @param {item} existItemInstance
   * @return {Promise<item|null>}
   */
  static async convertSyncInstanceToLocal(orgObj, existItemInstance) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (!orgObj) {
        return resolve(null);
      }

      let fillSyncObjProperties = (item, data) => {
        return orgModel.prepareSyncOnlyProperties(orgModel.prepareItemDbProperties(item, data), data);
      };

      let fillModelProperties = (item, data) => {
        return orgModel.prepareCommonProperties(orgModel.prepareItemDbProperties(item, data), data);
      };

      let orgInstance = null;
      if (existItemInstance) {
        orgInstance = fillSyncObjProperties(orgModel.prepareModelData(existItemInstance), orgObj);
      } else {
        orgInstance = fillModelProperties(orgModel.prepareModelData(orgObj), orgObj);
      }

      resolve(orgInstance);
    });
  }

  /**
   * @param {{organizationObjs: OrgObj|[OrgObj]}} inputData
   */
  static async remove(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {organizationObjs} = inputData;
      if (!organizationObjs) {
        return resolve(false);
      }

      let result = false;
      if (organizationObjs instanceof Array) {
        for (let organizationObj of organizationObjs) {
          result = <boolean>await OrgObjRepository.removeUserOrg({item: organizationObj});
        }
      } else {
        result = <boolean>await OrgObjRepository.removeUserOrg({item: organizationObjs});
      }
      resolve(result);
    });
  }
}
