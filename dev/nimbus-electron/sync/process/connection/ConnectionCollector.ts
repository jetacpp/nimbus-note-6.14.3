import chalk from 'chalk';

let xhrConnections = {};

export default class ConnectionCollector {
  /**
   * @type number
   */
  static MAX_STORED_CONNECTIONS_PER_WORKSPACE = 50;

  /**
   * @param {{workspaceId:string, uniqueId:string, xhrConnection:{}}} inputData
   */
  static add(inputData) {
    const {workspaceId, uniqueId, xhrConnection} = inputData;

    if (typeof (xhrConnections[workspaceId]) === 'undefined') {
      xhrConnections[workspaceId] = {};
    }

    if (uniqueId && xhrConnection) {
      const connectionsId = Object.keys(xhrConnections[workspaceId]);
      if (connectionsId.length >= ConnectionCollector.MAX_STORED_CONNECTIONS_PER_WORKSPACE) {
        if (typeof (xhrConnections[workspaceId][connectionsId[0]]) !== "undefined") {
          delete xhrConnections[workspaceId][connectionsId[0]];
        }
      }
      xhrConnections[workspaceId][uniqueId] = xhrConnection;
    }
  }

  /**
   * @param {{workspaceId:string, id:string}} inputData
   */
  static getById(inputData) {
    const {workspaceId, id} = inputData;

    if (typeof (xhrConnections[workspaceId]) === 'undefined') {
      return null;
    }

    if (typeof(xhrConnections[workspaceId][id]) === "undefined") {
      return null;
    }

    return xhrConnections[workspaceId][id];
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @return []
   */
  static get(inputData) {
    const {workspaceId} = inputData;
    return typeof (xhrConnections[workspaceId]) !== 'undefined' ? xhrConnections[workspaceId] : {};
  }

  /**
   * @param {{workspaceId:string, id:string}} inputData
   */
  static clearById(inputData) {
    const {workspaceId, id} = inputData;

    if (typeof (xhrConnections[workspaceId]) === 'undefined') {
      return;
    }

    if (typeof(xhrConnections[workspaceId][id]) === "undefined") {
      return;
    }

    delete xhrConnections[workspaceId][id];
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static clear(inputData) {
    const {workspaceId} = inputData;
    xhrConnections[workspaceId] = {};
  }

  static clearAll() {
    xhrConnections = {}
  }

  /**
   * @param {{workspaceId:string, id:string}} inputData
   */
  static abortById(inputData) {
    const {workspaceId, id} = inputData;

    if (typeof (xhrConnections[workspaceId]) === 'undefined') {
      return;
    }

    if (typeof(xhrConnections[workspaceId][id]) === "undefined") {
      return;
    }

    xhrConnections[workspaceId][id].abort();
    ConnectionCollector.clearById(inputData);
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static abort(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId} = inputData;

      if (typeof (xhrConnections[workspaceId]) !== 'undefined') {
        if (xhrConnections[workspaceId] && Object.keys(xhrConnections[workspaceId]).length) {
          for (let id in xhrConnections[workspaceId]) {
            if (xhrConnections[workspaceId].hasOwnProperty(id)) {
              xhrConnections[workspaceId][id].abort();
              ConnectionCollector.clearById({workspaceId, id});
            }
          }
        }
      }

      ConnectionCollector.clear(inputData);
      resolve(true);
    });
  }
}
