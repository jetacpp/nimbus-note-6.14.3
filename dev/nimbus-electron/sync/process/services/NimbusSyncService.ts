import chalk from "chalk";
import {default as config} from "../../../../config";
import {default as App} from "../application/App";
import {default as NimbusSDK} from "../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../events/SyncStatusChangedEvent";
import {default as SyncStatusDisplayEvent} from "../events/SyncStatusDisplayEvent";
import {default as socketConnection} from "../../socket/socketFunctions";
import {default as sessionLastUpdateTime} from "../rx/handlers/sessionLastUpdateTime";
import {default as ConnectionCollector} from "../connection/ConnectionCollector";
import {default as headerSync} from "../rx/handlers/headerSync";
import {default as saveSyncUpdateTime} from "../rx/handlers/saveSyncUpdateTime";
import {default as fullSync} from "../rx/handlers/fullSync";
import {default as updateUserUsage} from "../rx/handlers/updateUserUsage";
import {default as Exception} from "../../nimbussdk/net/exception/Exception";
import {default as AutoSyncManager} from "../../nimbussdk/manager/AutoSyncManager";
import {default as errorHandler} from "../../../utilities/errorHandler";
import {default as date} from "../../../utilities/dateHandler";
import syncHandler from "../../../utilities/syncHandler";

export default class NimbusSyncService {
  /**
   * @param {{workspaceId:string, manualSync:boolean}} inputData
   * @param {Function} callback
   * @return {Function}
   */
  async onStartCommand(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, manualSync} = inputData;
    const syncStartDate = date.now();

    let self = this;
    sessionLastUpdateTime.setSyncSessionUpdateTime({workspaceId, lastUpdateTime: 0});
    if (await App.canStartNewSync({workspaceId})) {
      SyncStatusChangedEvent.setCleanStatus({workspaceId});

      ConnectionCollector.clear({workspaceId});
      socketConnection.flushStore({workspaceId});
      syncHandler.init();

      syncHandler.log(`sync => start ${new Date().toISOString()} (ws: ${workspaceId})`);

      // @ts-ignore
      NimbusSDK.getAccountManager().getSyncTypeIsHeader(async (err, isHeaderSync) => {
        if (err) {
          errorHandler.log({
            err: err, response: isHeaderSync,
            description: "Sync problem => NimbusSyncService => getSyncTypeIsHeader",
            data: inputData
          });
        }

        //TODO: user can select sync type (quick/fast) in popup
        //isHeaderSync = typeof (isHeaderSync) === "boolean" ? isHeaderSync : false;
        isHeaderSync = false;

        if (config.SHOW_WEB_CONSOLE) {
          syncHandler.log(`sync => start => isHeader: ${isHeaderSync} (ws: ${workspaceId})`);
        }

        await App.setRunningSyncType({
          workspaceId: workspaceId,
          type: isHeaderSync ? App.SYNC_TYPES.HEADER : App.SYNC_TYPES.FULL
        });

        if (isHeaderSync) {
          await headerSync({
            workspaceId,
            service: self,
            manualSync,
            syncStartDate
            // @ts-ignore
          }, async (err, syncComplete) => {
            if (err) {
              let errorData = {
                err: err, response: syncComplete,
                description: "Sync problem => NimbusSyncService => headerSync",
                data: inputData
              };
              errorHandler.log(errorData);
            }

            syncHandler.log(`sync => complete header sync: ${syncComplete} (ws: ${workspaceId})`);

            await saveSyncUpdateTime(inputData);
            await self.onSyncComplete({workspaceId, syncComplete, syncType: App.SYNC_TYPES.HEADER});
            callback(err, true);
          });
        } else {
          fullSync({
            workspaceId,
            service: self,
            manualSync,
            syncStartDate
            // @ts-ignore
          }, async (err, syncComplete) => {
            if (err) {
              let errorData = {
                err: err, response: syncComplete,
                description: "Sync problem => NimbusSyncService => fullSync",
                data: inputData
              };
              errorHandler.log(errorData);
            }

            syncHandler.log(`sync => complete full sync: ${syncComplete} (ws: ${workspaceId})`);

            await saveSyncUpdateTime(inputData);
            await self.onSyncComplete({workspaceId, syncComplete, syncType: App.SYNC_TYPES.FULL});
            callback(err, true);
          });
        }
      });
    } else {
      errorHandler.log({
        err: null, response: null,
        description: "Sync problem => NimbusSyncService => canStartNewSync",
        data: {
          workspaceId
        }
      });
      return callback(new Exception(`NimbusSyncService.onStartCommand already run sync`), null);
    }
  }

  /**
   * @param {{workspaceId:string, syncComplete:boolean, syncType:string}} inputData
   */
  // @ts-ignore
  async onSyncComplete(inputData) {
    const {workspaceId, syncComplete, syncType} = inputData;
    let syncStatus = await SyncStatusChangedEvent.getStatus({workspaceId});
    await App.setRunningSyncType({
      workspaceId: workspaceId,
      type: App.SYNC_TYPES.NONE
    });
    if (syncComplete) {
      await updateUserUsage({
        workspaceId,
        useForSync: true
      });
      if (syncStatus !== SyncStatusChangedEvent.STATUS.FAILED) {
        const status = SyncStatusDisplayEvent.get({workspaceId});
        if (status !== SyncStatusDisplayEvent.STATUS.TRAFFIC_LIMIT) {
          await SyncStatusDisplayEvent.set({
            workspaceId: workspaceId,
            newStatus: SyncStatusDisplayEvent.STATUS.DONE
          });
        }
      }
      ConnectionCollector.clear({workspaceId});

      if (config.SHOW_WEB_CONSOLE) {
        syncHandler.log(`sync => complete for syncType: ${syncType} (ws: ${workspaceId})`);
      }
    } else {
      errorHandler.log({
        err: null, response: null,
        description: "Sync problem => NimbusSyncService => onSyncComplete",
        data: {
          ...inputData,
          syncComplete: syncComplete,
          syncType: syncType
        }
      });

      const status = SyncStatusDisplayEvent.get({workspaceId});
      if (status !== SyncStatusDisplayEvent.STATUS.TRAFFIC_LIMIT) {
        await SyncStatusDisplayEvent.set({
          workspaceId: workspaceId,
          newStatus: SyncStatusDisplayEvent.STATUS.CANCELED
        });
      }

      if (SyncStatusChangedEvent.getStatus({workspaceId}) === SyncStatusChangedEvent.STATUS.PAUSED) {
        await ConnectionCollector.abort({workspaceId});
      }

      if (config.SHOW_WEB_CONSOLE) {
        console.log(`Sync failed: ${syncType} (ws: ${workspaceId})`);
      }

      syncHandler.showLogs();
    }
    NimbusSyncService.onSyncEnd({
      workspaceId,
      startQueueSync: true
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  cancelSync(inputData, callback = (err, res) => {
  }) {
    NimbusSyncService.cancelSync(inputData, callback);
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  // @ts-ignore
  static async cancelSync(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    errorHandler.log({
      err: null, response: null,
      description: "Sync => NimbusSyncService => cancelSync",
      data: inputData
    });
    await updateUserUsage({
      workspaceId,
      useForSync: true
    });
    let syncType = await App.getRunningSyncType({workspaceId});
    await App.setRunningSyncType({
      workspaceId: workspaceId,
      type: App.SYNC_TYPES.NONE
    });
    ConnectionCollector.clear({workspaceId});
    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.CANCELED
    });
    if (config.SHOW_WEB_CONSOLE) {
      console.log(`Sync failed: ${syncType} (ws: ${workspaceId})`);
    }
    NimbusSyncService.onSyncEnd({
      workspaceId,
      startQueueSync: true
    });
  }

  /**
   * @param {{workspaceId:string, startQueueSync:boolean}} inputData
   */
  static onSyncEnd(inputData) {
    const {workspaceId, startQueueSync} = inputData;
    socketConnection.flushStore(inputData);
    AutoSyncManager.updateTimer(inputData);
    if (startQueueSync) {
      AutoSyncManager.makeQueueSync(inputData);
    }
  };
}
