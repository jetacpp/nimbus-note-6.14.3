import {default as RuntimeException} from "./RuntimeException";

export default class NoteIsMoreThanLimitException extends RuntimeException {
  static TYPE = {
    "QUOTA": "QUOTA",
    "NOTE": "NOTE",
    "ATTACHMENT": "ATTACHMENT"
  };

  /**
   * @type {NoteIsMoreThanLimitException.TYPE}
   */
  type;

  /**
   * @type {string}
   */
  globalId;

  constructor(type, globalId) {
    super("NoteIsMoreThanLimitException");
    this.globalId = globalId;
    this.type = type;
  }

  /**
   * @returns {string}
   */
  getGlobalId() {
    return this.globalId;
  }

  /**
   * @returns {NoteIsMoreThanLimitException.TYPE}
   */
  getType() {
    return this.type;
  }
}
