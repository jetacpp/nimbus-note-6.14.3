export default class RuntimeException {
  /**
   * @type string
   */
  message;

  /**
   * @param {string} message
   */
  constructor(message) {
    this.message = "RuntimeException: " + message;
    console.log(this.message);
  }

  /**
   * @returns {string}
   */
  getMessage() {
    return this.message;
  }
}