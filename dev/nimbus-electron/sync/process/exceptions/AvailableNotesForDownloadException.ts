import {default as RuntimeException} from "./RuntimeException";

export default class AvailableNotesForDownloadException extends RuntimeException {
  constructor() {
    super("AvailableNotesForDownloadException");
  }
}
