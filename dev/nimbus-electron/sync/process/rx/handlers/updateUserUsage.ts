import chalk from "chalk";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as UserObjRepository} from "../../repositories/UserObjRepository";
import {default as errorHandler} from "../../../../utilities/errorHandler";

/**
 * @param {{useForSync:boolean}} inputData
 * @return {Promise}
 */
export default async function updateUserUsage(inputData) {
  // @ts-ignore
  return new Promise(async (resolve) => {
    const {workspaceId, useForSync} = inputData;
    if (useForSync) {
      if (await SyncStatusChangedEvent.needPreventSync({})) return resolve(false);
    }

    NimbusSDK.getApi().userInfo({
      workspaceId,
      skipSyncHandlers: !useForSync
    }, async (err, userInstance) => {
      if (err || !userInstance) {
        if (useForSync) {
          await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
          errorHandler.log({
            err: err, response: userInstance,
            description: "Sync/Purchase problem => updateUserUsage => userInfo",
            data: inputData
          });
        }
        return resolve(null);
      }

      if (userInstance && userInstance.user_id) {
        let queryData = {
          email: userInstance.login
        };

        let updateData = {
          usageCurrent: null,
          usageMax: null,
          dateNextQuotaReset: null,
          subscribe: null,
          paymentStartDate: null,
          paymentEndDate: null,
          paymentSystemCode: null
        };

        if (userInstance.usage && userInstance.usage.notes) {
          updateData.usageCurrent = userInstance.usage.notes.current;
          updateData.usageMax = userInstance.usage.notes.max;
        }

        updateData.dateNextQuotaReset = UserObjRepository.getDateNextQuotaReset(userInstance.days_to_quota_reset);

        if (userInstance.premium) {
          updateData.subscribe = userInstance.premium.active ? 1 : 0;
          updateData.paymentStartDate = userInstance.premium.start_date;
          updateData.paymentEndDate = userInstance.premium.end_date;
          updateData.paymentSystemCode = userInstance.premium.source;
        }

        UserObjRepository.update({queryData, updateData}, (err, res) => {
          if (err) {
            errorHandler.log({
              err: err, response: res,
              description: "Sync/Purchase problem => updateUserUsage => UserObjRepository.update",
              data: {
                queryData,
                updateData
              }
            });
          }
          resolve(userInstance.usage.notes);
        });
      } else {
        errorHandler.log({
          err: err, response: userInstance,
          description: "Sync/Purchase problem => updateUserUsage",
          data: inputData
        });
        return resolve(null);
      }
    });
  });
}
