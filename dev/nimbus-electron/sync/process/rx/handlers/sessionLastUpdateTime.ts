import chalk from "chalk";

let syncSessionLastUpdateTime = {};

export default class SessionLastUpdateTime {
  /**
   * @param {{workspaceId:string}} inputData
   * @return int
   */
  static getSyncSessionUpdateTime(inputData) {
    const {workspaceId} = inputData;
    return typeof (syncSessionLastUpdateTime[workspaceId]) !== 'undefined' ? syncSessionLastUpdateTime[workspaceId] : 0;
  }

  /**
   * @param {{workspaceId:string, lastUpdateTime:int}} inputData
   */
  static setSyncSessionUpdateTime(inputData) {
    const {workspaceId, lastUpdateTime} = inputData;
    syncSessionLastUpdateTime[workspaceId] = lastUpdateTime;
  }
}
