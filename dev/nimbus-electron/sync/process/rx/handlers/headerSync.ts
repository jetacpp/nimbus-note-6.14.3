import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as fastDownload} from "./fastDownload";
import {default as userInfo} from "./userInfo";
import {default as fastUpload} from "./fastUpload";
import {default as fullUpload} from "./fullUpload";
import {default as clearRemovedData} from "./clearRemovedData";
import {default as clearSocketStore} from "./clearSocketStore";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param { workspaceId:string, service:NimbusSyncService } inputData
 * @param {Function} callback
 */
export default function headerSync(inputData, callback = (err, res) => {
}) {
  // @ts-ignore
  return new Promise(async () => {
    const {workspaceId} = inputData;

    /**
     * @param { workspaceId:string, service:NimbusSyncService } inputData
     */
    let fastUserDownload = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        fastDownload(inputData, (err) => {
          if (err) {
            errorHandler.log({
              err: err, response: null,
              description: "Sync problem => headerSync => fastUserDownload",
              data: inputData
            });
          }
          resolve(err);
        });
      });
    };

    /**
     * @param {{workspaceId:string, service:NimbusSyncService}} inputData
     */
    let getUserInfo = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        userInfo({...inputData, saveUserInfo: true}, (err) => {
          if (err) {
            errorHandler.log({
              err: err, response: null,
              description: "Sync problem => headerSync => getUserInfo",
              data: inputData
            });
          }
          resolve(err);
        });
      });
    };

    /**
     * @param {{workspaceId:string, service:NimbusSyncService}} inputData
     */
    let fastUserUpload = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        fastUpload(inputData, (err) => {
          if (err) {
            let errorData = {
              err: err, response: null,
              description: "Sync problem => headerSync => fastUserUpload",
              data: inputData
            };
            errorHandler.displayErrorPopup(errorData);
            errorHandler.log(errorData);
          }
          resolve(err);
        });
      });
    };

    /**
     * @param {{workspaceId:string, service:NimbusSyncService}} inputData
     */
    let fullUserUpload = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        fullUpload(inputData, (err) => {
          if (err) {
            errorHandler.log({
              err: err, response: null,
              description: "Sync problem => headerSync => fullUserUpload",
              data: inputData
            });
          }
          resolve(err);
        });
      });
    };

    /**
     * @param {{workspaceId:string, service:NimbusSyncService}} inputData
     */
    let clearUserRemovedData = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        clearRemovedData(inputData, (err) => {
          if (err) {
            errorHandler.log({
              err: err, response: null,
              description: "Sync problem => headerSync => clearUserRemovedData",
              data: inputData
            });
          }
          resolve(err);
        });
      });
    };

    syncHandler.log(`sync => headerSync => start (ws: ${workspaceId})`);

    clearSocketStore({workspaceId});

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    SyncStatusChangedEvent.setStatus({workspaceId, newStatus: SyncStatusChangedEvent.STATUS.HEADER_START});

    let err;

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    syncHandler.log(`sync => headerSync => fastUserDownload (ws: ${workspaceId})`);
    if (err = await fastUserDownload(inputData)) {
      return callback(err, false);
    }

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    syncHandler.log(`sync => headerSync => getUserInfo (ws: ${workspaceId})`);
    if (err = await getUserInfo(inputData)) {
      return callback(err, false);
    }

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    syncHandler.log(`sync => headerSync => fastUserUpload (ws: ${workspaceId})`);
    if (err = await fastUserUpload(inputData)) {
      return callback(err, false);
    }

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    syncHandler.log(`sync => headerSync => fullUserUpload (ws: ${workspaceId})`);
    if (err = await fullUserUpload(inputData)) {
      return callback(err, false);
    }

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    syncHandler.log(`sync => headerSync => clearUserRemovedData (ws: ${workspaceId})`);
    if (err = await clearUserRemovedData(inputData)) {
      return callback(err, false);
    }

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    let syncStatus = await SyncStatusChangedEvent.getStatus({workspaceId});
    if (syncStatus !== SyncStatusChangedEvent.STATUS.FAILED) {
      SyncStatusChangedEvent.setStatus({workspaceId, newStatus: SyncStatusChangedEvent.STATUS.HEADER_FINISH});
    }

    callback(err, true);
  });
}
