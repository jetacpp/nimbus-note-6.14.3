import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as SyncStatusDisplayEvent} from "../../events/SyncStatusDisplayEvent";
import {default as sessionLastUpdateTime} from "./sessionLastUpdateTime";
import {default as FolderObjRepository} from "../../repositories/FolderObjRepository";
import {default as TagObjRepository} from "../../repositories/TagObjRepository";
import {default as NotesUpdateRequest} from "../../../nimbussdk/net/request/NotesUpdateRequest";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default async function uploadUpdatedFoldersAndTags(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;
  syncHandler.log(`=> uploadUpdatedFoldersAndTags => start for workspaceId: ${workspaceId}`);

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<[]>}
   */
  let getUpdatedFoldersForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      FolderObjRepository.getUpdatedFoldersForUploadOnServer(inputData, (err, folders) => {
        if (err) {
          errorHandler.log({
            err: err, response: folders,
            description: "Sync problem => uploadUpdatedFoldersAndTags => getUpdatedFoldersForUploadOnServer",
            data: inputData
          });
        }
        resolve(folders);
      });
    });
  };

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<[]>}
   */
  let getRenamedTagsForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      TagObjRepository.getRenamedTagsForUploadOnServer(inputData, (err, renameTags) => {
        if (err) {
          errorHandler.log({
            err: err, response: renameTags,
            description: "Sync problem => uploadUpdatedFoldersAndTags => getRenamedTagsForUploadOnServer",
            data: inputData
          });
        }
        resolve(renameTags);
      });
    });
  };

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<[]>}
   */
  let getUpdatedTagsForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      TagObjRepository.getUpdatedTagsForUploadOnServer(inputData, (err, storeTags) => {
        if (err) {
          errorHandler.log({
            err: err, response: storeTags,
            description: "Sync problem => uploadUpdatedFoldersAndTags => getUpdatedTagsForUploadOnServer",
            data: inputData
          });
        }
        resolve(storeTags);
      });
    });
  };

  syncHandler.log(`=> uploadUpdatedFoldersAndTags => getUpdatedFoldersForUploadOnServer`);
  let folders: any = await getUpdatedFoldersForUploadOnServer(inputData);
  syncHandler.log(`=> uploadUpdatedFoldersAndTags => getRenamedTagsForUploadOnServer`);
  let renameTags: any = await getRenamedTagsForUploadOnServer(inputData);
  syncHandler.log(`=> uploadUpdatedFoldersAndTags => getUpdatedTagsForUploadOnServer`);
  let storeTags: any = await getUpdatedTagsForUploadOnServer(inputData);

  let makeRequest = folders.length || renameTags.length || storeTags.length;
  if (!makeRequest) {
    return callback(null, true);
  }

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  if (folders.length) {
    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.UPLOAD_FOLDERS
    });
  }

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  if (renameTags.length) {
    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.UPLOAD_TAGS
    });
  }

  syncHandler.log(`=> uploadUpdatedFoldersAndTags => NotesUpdateRequest.Body => workspaceId: ${workspaceId}`);

  let body = new NotesUpdateRequest.Body({});
  body.storeFolders(folders);
  body.setRenameTags(renameTags);
  body.storeTags(storeTags);

  syncHandler.log(body);

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);

  syncHandler.log(`=> uploadUpdatedFoldersAndTags => updateNotes`);
  NimbusSDK.getApi().updateNotes({workspaceId, body}, async function (err, response) {
    syncHandler.log(`=> uploadUpdatedFoldersAndTags => updateNotes => err: ${err}`);

    if (err) {
      await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
      errorHandler.log({
        err: err, response: response,
        description: "Sync problem => uploadUpdatedFoldersAndTags => updateNotes"
      });
      return callback(err, false);
    }

    /**
     * @param {{workspaceId:string, folders:[FolderObj], renameTags:[TagObj], storeTags:[TagObj]}} inputData
     * @return {Promise<boolean>}
     */
    let processUploadResponse = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, folders, renameTags, storeTags} = inputData;
        await FolderObjRepository.callAfterUploadExistFoldersOnServerI({workspaceId, folders});
        await TagObjRepository.callAfterUploadStoreTagsI({workspaceId, tags: storeTags});
        await TagObjRepository.callAfterUploadRenamedTagsI({workspaceId, tags: renameTags});
        resolve(true);
      });
    };

    syncHandler.log(`=> uploadUpdatedFoldersAndTags => updateNotes => processUploadResponse`);
    await processUploadResponse({workspaceId, folders, renameTags, storeTags});

    syncHandler.log(`=> uploadUpdatedFoldersAndTags => updateNotes => response`);
    if (response && response.last_update_time) {
      sessionLastUpdateTime.setSyncSessionUpdateTime({workspaceId, lastUpdateTime: response.last_update_time});
    } else {
      if (config.SHOW_WEB_CONSOLE) {
        console.log(`Set Sync Session Update Time fail: (ws: ${workspaceId})`, response);
      }
      errorHandler.log({
        err: err, response: response,
        description: "Sync problem => uploadUpdatedFoldersAndTags"
      });
    }

    syncHandler.log(`=> uploadUpdatedFoldersAndTags => done`);
    callback(err, true);
  });
}
