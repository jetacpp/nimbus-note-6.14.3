import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as TagObjRepository} from "../../repositories/TagObjRepository";
import {default as errorHandler} from "../../../../utilities/errorHandler";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default async function downloadTags(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  NimbusSDK.getApi().getTags({workspaceId}, async (err, tags) => {
    if (err) {
      await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
      errorHandler.log({
        err: err, response: tags,
        description: "Sync problem => downloadTags => getTags"
      });
      return callback(err, false);
    }

    /**
     * @param {{workspaceId:string, tag: string}} inputData
     * @return {Promise<Object|null>}
     */
    let checkUserTagItem = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        TagObjRepository.checkIfTagRenamed(inputData, async (err, tagObj) => {
          if (err) {
            errorHandler.log({
              err: err, response: tagObj,
              description: "Sync problem => downloadTags => checkUserTagItem",
              data: inputData
            });
            return resolve(null);
          }

          /**
           * @param {{workspaceId:string, tag:string}} inputData
           */
          let createUserTag = async (inputData) => {
            // @ts-ignore
            return new Promise((resolve) => {
              TagObjRepository.create(inputData, (err, tagObj) => {
                if (err) {
                  errorHandler.log({
                    err: err, response: tagObj,
                    description: "Sync problem => downloadTags => createUserTag",
                    data: inputData
                  });
                  return resolve(null);
                }

                resolve(tagObj);
              });
            });
          };

          /**
           * @param {{workspaceId:string, tag:TagObj}} inputData
           */
          let checkUserTagRemoved = async (inputData) => {
            // @ts-ignore
            return new Promise((resolve) => {
              TagObjRepository.getErasedTagForUploadOnServer(inputData, (err, tagObj) => {
                if (err) {
                  errorHandler.log({
                    err: err, response: tagObj,
                    description: "Sync problem => downloadTags => checkUserTagRemoved",
                    data: inputData
                  });
                  return resolve(false);
                }

                resolve(tagObj);
              });
            });
          };

          if (!tagObj) {
            let isUserTagRemoved = await checkUserTagRemoved(inputData);
            if (!isUserTagRemoved) {
              tagObj = await createUserTag(inputData);
            }
          }

          if (!tagObj) {
            errorHandler.log({
              err: err, response: tagObj,
              description: "Sync problem => downloadTags => !tagObj",
              data: inputData
            });
            return resolve(null);
          }

          tagObj.needSync = false;
          resolve(tagObj);
        });
      });
    };

    /**
     * @param {{workspaceId:string, tags:[TagObj]}} inputData
     * @return {Promise<[TagObj]>}
     */
    let checkUserTagItems = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, tags} = inputData;
        let tagObjs = [];
        for (let tag of tags) {
          tag = await checkUserTagItem({workspaceId, tag});
          if (tag) {
            tagObjs.push(tag);
          }
        }
        resolve(tagObjs);
      });
    };

    TagObjRepository.getAllTagsR(inputData, async (err, tagList) => {
      let removeTagsList = [];

      for (let tagItem of tagList) {
        if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
        if (tags.indexOf(tagItem.tag) < 0 && tagItem.needSync == false) {
          removeTagsList.push(tagItem.tag);
        }
      }

      for (let removeTag of removeTagsList) {
        if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
        await TagObjRepository.deleteNoteData({workspaceId, tag: removeTag});
        await TagObjRepository.deleteData({workspaceId, tag: removeTag});
      }

      if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);

      TagObjRepository.emitSocketEvent({
        workspaceId,
        tagList: removeTagsList,
        action: "remove"
      });

      if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
      if (tags && tags.length) {
        let tagObjs = await checkUserTagItems({workspaceId, tags});
        TagObjRepository.updateTagsDownloadedFromServerI({workspaceId, tagObjs}, (err, response) => {
          if (err) {
            errorHandler.log({
              err: err, response: response,
              description: "Sync problem => downloadTags => updateTagsDownloadedFromServerI",
              data: {
                workspaceId,
                tagObjs
              }
            });
          }
          callback(err, response);
        });
      } else {
        callback(err, true);
      }
    });
  });
}
