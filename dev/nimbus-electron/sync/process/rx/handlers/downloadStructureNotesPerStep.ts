import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as PreviewSyncManager} from "../../../nimbussdk/manager/PreviewSyncManager";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as SyncStatusDisplayEvent} from "../../events/SyncStatusDisplayEvent";
import {default as TrashRepository} from "../../repositories/TrashRepository";
import {default as NoteObjRepository} from "../../repositories/NoteObjRepository";
import {default as NoteObj} from "../../db/NoteObj";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import TextEditor from "../../../../db/TextEditor";
import item from "../../../../db/models/item";
import workspace from "../../../../db/models/workspace";
import {propOutDate} from "../../../../utilities/syncPropsHandler";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param {{workspaceId:string, start:int, amount:int, totalNotes:int}} inputData
 * @param {Function} callback
 */
export default async function downloadStructureNotesPerStep(inputData, callback = (err, res) => {
}) {
  const {workspaceId, start, amount, totalNotes} = inputData;

  syncHandler.log(`sync => downloadStructureNotesPerStep (ws: ${workspaceId})`);
  if ((start + amount) > totalNotes) {
    syncHandler.log(`sync => downloadStructureNotesPerStep => start: ${start} amount: ${totalNotes - start}`);
  } else {
    syncHandler.log(`sync => downloadStructureNotesPerStep => start: ${start} amount: ${amount}`);
  }

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  NimbusSDK.getApi().getStructureNotes({workspaceId, start, amount}, async (err, body) => {
    if (err) {
      await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
      errorHandler.log({
        err: err, response: body,
        description: "Sync problem => downloadStructureNotesPerStep => getStructureNotes",
        data: {
          start: start,
          amount: amount
        }
      });
      return callback(err, false);
    }

    NimbusSDK.getAccountManager().getUniqueUserName(async (err, uniqueUserName) => {
      if (err) {
        await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
        errorHandler.log({
          err: err, response: uniqueUserName,
          description: "Sync problem => downloadStructureNotesPerStep => getUniqueUserName"
        });
        return callback(err, false);
      }

      /**
       * @param {{workspaceId:string, note:NoteObj}} inputData
       * @return {Promise<boolean>}
       */
      let checkUserNoteInTrash = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, note} = inputData;
          TrashRepository.checkIfNoteInTrash({workspaceId, parentId: note.parent_id}, (err, isMaybeInTrash) => {
            if (err) {
              errorHandler.log({
                err: err, response: isMaybeInTrash,
                description: "Sync problem => downloadStructureNotesPerStep => checkUserNoteInTrash",
                data: {
                  note: note
                }
              });
              return resolve(false);
            }
            resolve(isMaybeInTrash);
          });
        });
      };

      /**
       * @param {{workspaceId:string, note:NoteObj, noteFieldsUpdateDates:{}}} inputData
       * @return {Promise<Array>}
       */
      let getNoteInfo = (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, note, noteFieldsUpdateDates} = inputData;
          NoteObjRepository.get({workspaceId, globalId: note.global_id}, async (err, noteObj) => {
            if (err) {
              errorHandler.log({
                err: err, response: noteObj,
                description: "Sync problem => downloadStructureNotesPerStep => getNoteInfo",
                data: {
                  note: note
                }
              });
              return resolve(null);
            }

            /**
             * @param {{workspaceId:string, note:NoteObj}} inputData
             * @return {Promise<NoteObj|null>}
             */
            let createUserNoteObj = async (inputData) => {
              // @ts-ignore
              return new Promise((resolve) => {
                const {workspaceId, note} = inputData;
                NoteObjRepository.create({
                  workspaceId,
                  parentId: note.parent_id,
                  role: note.role,
                  tag: null
                }, (err, noteObj) => {
                  if (err) {
                    errorHandler.log({
                      err: err, response: noteObj,
                      description: "Sync problem => downloadStructureNotesPerStep => createUserNoteObj",
                      data: {
                        note: note
                      }
                    });
                    return resolve(null);
                  }

                  resolve(noteObj);
                });
              });
            };

            /**
             * note - download note & noteObj - exist note
             * @param {{workspaceId:string, note:{}, noteObj: {}}} inputData
             */
            let addPreviewEvents = (inputData) => {
              let {workspaceId, note, existNoteObj, noteObj} = inputData;

              if (!existNoteObj) {
                if (noteObj && noteObj.preview && noteObj.preview.global_id) {
                  PreviewSyncManager.pushUpdateEvent({
                    workspaceId,
                    globalId: note.global_id,
                    attachmentGlobalId: noteObj.preview.global_id
                  });
                }
                return;
              }

              if (existNoteObj.preview && existNoteObj.preview.global_id) {
                if (noteObj.preview) {
                  if (noteObj.preview.global_id) {
                    if (existNoteObj.preview.global_id !== noteObj.preview.global_id) {
                      PreviewSyncManager.pushUpdateEvent({
                        workspaceId,
                        globalId: note.global_id,
                        attachmentGlobalId: noteObj.preview.global_id
                      });
                    }
                  } else {
                    PreviewSyncManager.pushRemoveEvent({
                      workspaceId,
                      globalId: note.global_id
                    });
                  }
                } else {
                  PreviewSyncManager.pushRemoveEvent({
                    workspaceId,
                    globalId: note.global_id
                  });
                }
              } else {
                if (noteObj.preview && noteObj.preview.global_id) {
                  PreviewSyncManager.pushUpdateEvent({
                    workspaceId,
                    globalId: note.global_id,
                    attachmentGlobalId: noteObj.preview.global_id
                  });
                } else {
                  if(existNoteObj.preview) {
                    PreviewSyncManager.pushRemoveEvent({
                      workspaceId,
                      globalId: note.global_id
                    });
                  }
                }
              }
            };

            const existNoteObj = noteObj ? {...noteObj} : null;

            let needPropUpdate = true;
            if (existNoteObj) {
              if(existNoteObj.syncDate > note.date_updated) {
                needPropUpdate = false;
              }
            }

            if(!existNoteObj) {
              noteObj = await createUserNoteObj({workspaceId, note});
            }

            if (!noteObj) {
              return resolve(null);
            }

            if (note.todo) {
              let todoCount = {total: 0, unchecked: 0};
              for (let todoItem of note.todo) {
                todoCount.total += 1;
                if (!todoItem.checked) {
                  todoCount.unchecked += 1;
                }
              }
              note.todo_count = todoCount;
            }

            if (note.attachements) {
              let attachementsCount = {total: 0, in_list: 0};
              for (let attachementItem of note.attachements) {
                attachementsCount.total += 1;
                if (!attachementItem.in_list) {
                  attachementsCount.in_list += 1;
                }
              }
              note.attachements_count = attachementsCount;
            }

            if(needPropUpdate) {
              noteObj.globalId = note.global_id;
              noteObj.parentId = note.parent_id;
              noteObj.rootParentId = note.root_parent_id;
              noteObj.dateAdded = note.date_added_user;
              noteObj.dateUpdated = note.date_updated_user;
              noteObj.syncDate = note.date_updated;
              noteObj.type = note.type;
              noteObj.isMaybeInTrash = await checkUserNoteInTrash({workspaceId, note});
              noteObj.is_imported = note.is_imported;
            }

            if(needPropUpdate) {
              noteObj = NoteObj.setLocation(noteObj, note.location_lat, note.location_lng);
              // noteObj = NoteObj.setTags(noteObj, note.tags);

              noteObj.attachements = note.attachements ? note.attachements.slice() : [];
              noteObj.todo = note.todo ? note.todo.slice() : [];

              // noteObj = NoteObj.setTodoCount(noteObj, note.todo_count ? note.todo_count.unchecked : 0);
              // noteObj = NoteObj.setAttachmentsInListCount(noteObj, note.attachments_count ? note.attachments_count.in_list : 0);
            }


            const previewInput = { existNoteObj, workspaceId, note, noteObj: null };
            if(propOutDate({existNoteObjProp: 'preview', serverNoteObjProp: 'preview'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.preview = note.preview ? note.preview : null;
            } else {
              noteObj.preview = existNoteObj.preview;
            }
            addPreviewEvents({...previewInput, noteObj});

            if(propOutDate({existNoteObjProp: 'tags', serverNoteObjProp: 'tags'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.tags = note.tags;
            } else {
              noteObj.tags = existNoteObj.tags;
            }

            if(propOutDate({existNoteObjProp: 'title', serverNoteObjProp: 'title'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.title = note.title;
            } else {
              noteObj.title = existNoteObj.title;
            }

            if(propOutDate({existNoteObjProp: 'shortText', serverNoteObjProp: 'text_short'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.shortText = note.text_short;
            } else {
              noteObj.shortText = existNoteObj.shortText;
            }

            if(propOutDate({existNoteObjProp: 'text', serverNoteObjProp: 'text'}, existNoteObj, noteFieldsUpdateDates)) {
              if (note.text) {
                noteObj = NoteObj.setText(noteObj, note.text);
              }
            } else {
              noteObj.text = existNoteObj.text;
            }

            if(needPropUpdate) {
              noteObj.text_version = note.text_version;
              noteObj.isDownloaded = false;
              noteObj.needSync = false;
              if(existNoteObj && existNoteObj.needSync) {
                if(existNoteObj.text_version >= 2) {
                  noteObj.needSync = existNoteObj.needSync;
                }
              }
            }

            if(propOutDate({existNoteObjProp: 'isEncrypted', serverNoteObjProp: 'is_encrypted'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.isEncrypted = note.is_encrypted;
            } else {
              noteObj.isEncrypted = existNoteObj.isEncrypted;
            }

            if(propOutDate({existNoteObjProp: 'isFullwidth', serverNoteObjProp: 'is_fullwidth'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.isFullwidth = note.is_fullwidth;
            } else {
              noteObj.isFullwidth = existNoteObj.isFullwidth;
            }

            if(propOutDate({existNoteObjProp: 'url', serverNoteObjProp: 'url'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.url = note.url;
            } else {
              noteObj.url = existNoteObj.url;
            }

            noteObj.uniqueUserName = uniqueUserName;
            noteObj.existOnServer = true;
            noteObj.isTemp = false;
            noteObj.isMoreThanLimit = false;

            if(propOutDate({existNoteObjProp: 'editnote', serverNoteObjProp: 'editnote'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.editnote = note.editnote;
            } else {
              noteObj.editnote = existNoteObj.editnote;
            }

            if(propOutDate({existNoteObjProp: 'color', serverNoteObjProp: 'color'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.color = note.color;
            } else {
              noteObj.color = existNoteObj.color;
            }

            if(propOutDate({existNoteObjProp: 'shared', serverNoteObjProp: 'shared'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.shared = !!note.shared;
              noteObj.shared_url = note.shared_url ? note.shared_url : '';
            } else {
              noteObj.shared = existNoteObj.shared;
              noteObj.shared_url = existNoteObj.shared_url ? existNoteObj.shared_url : '';
            }

            if(propOutDate({existNoteObjProp: 'favorite', serverNoteObjProp: 'favorite'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.favorite = !!note.favorite;
            } else {
              noteObj.favorite = existNoteObj.favorite;
            }

            if(propOutDate({existNoteObjProp: 'reminder', serverNoteObjProp: 'reminder'}, existNoteObj, noteFieldsUpdateDates)) {
              noteObj.reminder = note.reminder ? note.reminder : null;
            } else {
              noteObj.reminder = existNoteObj.reminder;
            }

            resolve(noteObj);
          });
        });
      };

      /**
       * @param {{workspaceId:string, noteObjs:[NoteObj]}} inputData
       */
      let updateUserStructureNotes = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          NoteObjRepository.updateStructureNotesDownloadedFromServerI(inputData, (err, response) => {
            if (err) {
              errorHandler.log({
                err: err, response: response,
                description: "Sync problem => downloadStructureNotesPerStep => updateUserStructureNotes",
                data: inputData
              });
              return resolve(false);
            }
            resolve(response);
          });
        });
      };

      if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
      if (body) {
        if (body.notes && body.notes.length) {
          if (body.notes.length) {
            await SyncStatusDisplayEvent.set({
              workspaceId: workspaceId,
              newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOAD_NOTES
            });
          }

          const { fieldsUpdateDates } = body;
          for (let i = 0; i < body.notes.length; i++) {
            if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
            const noteItem = body.notes[i];
            let noteFieldsUpdateDates = {};
            if(noteItem && noteItem.global_id) {
              if(fieldsUpdateDates && typeof (fieldsUpdateDates[noteItem.global_id]) !== 'undefined') {
                noteFieldsUpdateDates = fieldsUpdateDates[noteItem.global_id]
              }
            }
            let noteObj = <{globalId:string, text_version:number}>await getNoteInfo({
              workspaceId,
              note: noteItem,
              noteFieldsUpdateDates
            });
            if (noteObj) {
              if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
              await updateUserStructureNotes({workspaceId, noteObjs: [noteObj]});
              if(noteObj.text_version > 1) {
                const workspaceInstance = <{globalId:string}>(workspaceId ? await workspace.getById(workspaceId) : await workspace.getDefault());
                if(workspaceInstance) {
                  const saveTextDate = {
                    note: item.getResponseJson(noteObj),
                    workspaceId: workspaceInstance.globalId,
                    noteId: noteObj.globalId
                  };

                  const saveTextRes = <{result:boolean}>await TextEditor.makeBgNoteSync(saveTextDate);
                  if(saveTextRes && !saveTextRes.result) {
                    syncHandler.sendLog({
                      err: new Error(`Can not download note text v2: ${workspaceInstance.globalId}_${noteObj.globalId}`),
                      // response: saveTextRes,
                      // description: "Sync problem => downloadStructureNotesPerStep => makeBgNoteSync",
                      // data: saveTextDate
                    });
                  }
                }
              }

              if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
              if (body.notes.length) {
                await SyncStatusDisplayEvent.set({
                  workspaceId: workspaceId,
                  newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOAD_NOTES,
                  props: {
                    "current": (start + i + 1),
                    "total": totalNotes
                  }
                });
              }
            }
          }
        }
      }

      callback(err, body ? body.last_update_time : null);
    });
  });
}
