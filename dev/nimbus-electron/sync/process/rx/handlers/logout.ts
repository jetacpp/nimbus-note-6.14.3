import chalk from "chalk";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as workspace} from "../../../../db/models/workspace";

/**
 * @param {Function} callback
 */
export default async function logout(callback = (err, res) => {
}) {
  if (await SyncStatusChangedEvent.needPreventSync({})) return callback(null, false);
  const availableWorkspaceList = <string[]>await workspace.getAvailableIdList();
  NimbusSDK.getApi().logout(async (err) => {
    if (err) {
      for (let workspaceId of availableWorkspaceList) {
        await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
        errorHandler.log({
          err: err, response: null,
          description: "Sync problem => logout",
          data: {
            workspaceId
          }
        });
      }

      return callback(null, false);
    }

    callback(err, true);
  });
}
