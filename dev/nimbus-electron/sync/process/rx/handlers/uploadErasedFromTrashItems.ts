import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as sessionLastUpdateTime} from "./sessionLastUpdateTime";
import {default as NotesUpdateRequest} from "../../../nimbussdk/net/request/NotesUpdateRequest";
import {default as FolderObjRepository} from "../../repositories/FolderObjRepository";
import {default as NoteObjRepository} from "../../repositories/NoteObjRepository";
import {default as AttachmentObjRepository} from "../../repositories/AttachmentObjRepository";
import {default as TagObjRepository} from "../../repositories/TagObjRepository";
import {default as errorHandler} from "../../../../utilities/errorHandler";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default async function uploadErasedFromTrashItems(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;

  /**
   * @param {{workspaceId:string}} inputData
   */
  let getErasedFromTrashFoldersForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      FolderObjRepository.getErasedFromTrashFoldersForUploadOnServer(inputData, (err, removedFolders) => {
        if (err) {
          errorHandler.log({
            err: err, response: removedFolders,
            description: "Sync problem => uploadErasedFromTrashItems => getErasedFromTrashFoldersForUploadOnServer",
            data: inputData
          });
          return resolve([]);
        }
        resolve(removedFolders);
      });
    });
  };

  /**
   * @param {{workspaceId:string}} inputData
   */
  let getErasedFromTrashNotesForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      NoteObjRepository.getErasedFromTrashNotesForUploadOnServer(inputData, (err, removedNotes) => {
        if (err) {
          errorHandler.log({
            err: err, response: removedNotes,
            description: "Sync problem => uploadErasedFromTrashItems => getErasedFromTrashNotesForUploadOnServer",
            data: inputData
          });
          return resolve([]);
        }
        resolve(removedNotes);
      });
    });
  };

  /**
   * @param {{workspaceId:string}} inputData
   */
  let getErasedAttachmentsForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      AttachmentObjRepository.getErasedAttachmentsForUploadOnServer(inputData, (err, removedAttachments) => {
        if (err) {
          errorHandler.log({
            err: err, response: removedAttachments,
            description: "Sync problem => uploadErasedFromTrashItems => getErasedAttachmentsForUploadOnServer",
            data: inputData
          });
          return resolve([]);
        }
        resolve(removedAttachments);
      });
    });
  };

  /**
   * @param {{workspaceId:string}} inputData
   */
  let getErasedTagsForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      TagObjRepository.getErasedTagsForUploadOnServer(inputData, (err, removeTags) => {
        if (err) {
          errorHandler.log({
            err: err, response: removeTags,
            description: "Sync problem => uploadErasedFromTrashItems => getErasedTagsForUploadOnServer",
            data: inputData
          });
          return resolve([]);
        }
        resolve(removeTags);
      });
    });
  };

  /**
   * @param {{workspaceId:string}} inputData
   */
  let getUpdatedHeaderNotesForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      NoteObjRepository.getUpdatedHeaderNotesForUploadOnServer(inputData, (err, storeNotes) => {
        if (err) {
          errorHandler.log({
            err: err, response: storeNotes,
            description: "Sync problem => uploadErasedFromTrashItems => getUpdatedHeaderNotesForUploadOnServer",
            data: inputData
          });
          return resolve([]);
        }
        resolve(storeNotes);
      });
    });
  };

  let removedFolders: any = await getErasedFromTrashFoldersForUploadOnServer(inputData);
  let removedNotes: any = await getErasedFromTrashNotesForUploadOnServer(inputData);
  let removedAttachments: any = await getErasedAttachmentsForUploadOnServer(inputData);
  let removeTags: any = await getErasedTagsForUploadOnServer(inputData);
  let storeNotes: any = await getUpdatedHeaderNotesForUploadOnServer(inputData);

  let makeRequest = removedFolders.length || removedNotes.length || removedAttachments.length || removeTags.length || storeNotes.length;
  if (!makeRequest) {
    return callback(null, true);
  }

  let body = new NotesUpdateRequest.Body({});
  body.removeFolders(removedFolders);
  body.removeNotes(removedNotes);
  body.removeAttachements(removedAttachments);
  body.removeTags(removeTags);
  //body.storeNotes(storeNotes);

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  NimbusSDK.getApi().updateNotes({workspaceId, body}, async (err, response) => {
    if (err) {
      await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
      errorHandler.log({
        err: err, response: response,
        description: "Sync problem => uploadErasedFromTrashItems => updateNotes",
        data: {
          body: body
        }
      });
      return callback(err, false);
    }

    /**
     * @param {{workspaceId:string, response:{}}} inputData
     * @return {Promise<[]>}
     */
    let processUploadResponse = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        /**
         * @param {{workspaceId:string}} inputData
         */
        let callAfterUploadErasedFromTrashFoldersOnServerI = async (inputData) => {
          // @ts-ignore
          return new Promise((resolve) => {
            FolderObjRepository.callAfterUploadErasedFromTrashFoldersOnServerI(inputData, (err, response) => {
              if (err) {
                errorHandler.log({
                  err: err, response: response,
                  description: "Sync problem => uploadErasedFromTrashItems => processUploadResponse",
                  data: inputData
                });
                return resolve(null);
              }

              resolve(response);
            });
          });
        };

        let callAfterUploadErasedFromTrashNotesOnServerI = async (inputData) => {
          // @ts-ignore
          return new Promise((resolve) => {
            NoteObjRepository.callAfterUploadErasedFromTrashNotesOnServerI(inputData, (err, response) => {
              if (err) {
                errorHandler.log({
                  err: err, response: response,
                  description: "Sync problem => uploadErasedFromTrashItems => callAfterUploadErasedFromTrashNotesOnServerI",
                  data: inputData
                });
                return resolve(null);
              }

              resolve(response);
            });
          });
        };

        /**
         * @param {{workspaceId:string}} inputData
         */
        let callAfterUploadUpdatedHeaderNotesOnServerI = async (inputData) => {
          // @ts-ignore
          return new Promise((resolve) => {
            NoteObjRepository.callAfterUploadUpdatedHeaderNotesOnServerI(inputData, (err, response) => {
              if (err) {
                errorHandler.log({
                  err: err, response: response,
                  description: "Sync problem => uploadErasedFromTrashItems => callAfterUploadUpdatedHeaderNotesOnServerI",
                  data: inputData
                });
                return resolve(null);
              }
              resolve(response);
            });
          });
        };

        /**
         * @param {{workspaceId:string}} inputData
         */
        let callAfterUploadErasedAttachmentsOnServerI = async (inputData) => {
          // @ts-ignore
          return new Promise((resolve) => {
            AttachmentObjRepository.callAfterUploadErasedAttachmentsOnServerI(inputData, (err, response) => {
              if (err) {
                errorHandler.log({
                  err: err, response: response,
                  description: "Sync problem => uploadErasedFromTrashItems => callAfterUploadErasedAttachmentsOnServerI",
                  data: inputData
                });
                return resolve(null);
              }
              resolve(response);
            });
          });
        };

        /**
         * @param {{workspaceId:string}} inputData
         */
        let callAfterUploadErasedTagsOnServerI = async (inputData) => {
          // @ts-ignore
          return new Promise((resolve) => {
            TagObjRepository.callAfterUploadErasedTagsOnServerI(inputData, (err, response) => {
              if (err) {
                errorHandler.log({
                  err: err, response: response,
                  description: "Sync problem => uploadErasedFromTrashItems => callAfterUploadErasedTagsOnServerI",
                  data: inputData
                });
                return resolve(null);
              }
              resolve(response);
            });
          });
        };

        await callAfterUploadErasedFromTrashFoldersOnServerI(inputData);
        await callAfterUploadErasedFromTrashNotesOnServerI(inputData);
        //await callAfterUploadUpdatedHeaderNotesOnServerI(inputData);
        await callAfterUploadErasedAttachmentsOnServerI(inputData);
        await callAfterUploadErasedTagsOnServerI(inputData);

        resolve(true);
      });
    };

    /*for (let responseBody of response) {
        await processUploadResponse({workspaceId, response: responseBody});
    }*/
    await processUploadResponse({workspaceId, response});

    if (response && response.last_update_time) {
      sessionLastUpdateTime.setSyncSessionUpdateTime({workspaceId, lastUpdateTime: response.last_update_time});
    } else {
      errorHandler.log({
        err: err, response: response,
        description: "Sync problem => uploadErasedFromTrashItems"
      });
    }

    callback(err, true);
  });
}
