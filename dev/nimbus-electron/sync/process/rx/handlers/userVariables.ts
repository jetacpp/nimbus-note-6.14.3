import chalk from "chalk";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as user} from "../../../../db/models/user";
import {default as auth} from "../../../../auth/auth";
import {setUpdateProps} from "../../../../utilities/syncPropsHandler";
import {default as date} from "../../../../utilities/dateHandler";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default async function userVariables(inputData, callback = (err, res) => {
}) {
    const {workspaceId, saveUserVariables, skipSyncHandlers} = inputData;
    syncHandler.log(`=> userVariables => start for workspaceId: ${workspaceId}`);
    if (!skipSyncHandlers && await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);

    syncHandler.log(`=> userVariables => userVariables`);
    NimbusSDK.getApi().userVariables(inputData, async (err, userVariables) => {
        syncHandler.log(`=> userVariables => userVariables => err: ${err}`);
        if (err || !userVariables) {
            if (!skipSyncHandlers) {
                await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            }

            errorHandler.log({
                err: err, response: userVariables,
                description: "Sync problem => userVariables",
                data: inputData
            });
            return callback(err, false);
        }

        syncHandler.log(`=> userVariables => userVariables => userVariables`);
        if (!saveUserVariables) {
            callback(err, null);
        }

        const curDate = date.now();
        syncHandler.log(`=> userVariables => userVariables => fetchActualUserAsync`);
        const authInfo = <{email, variables}>await auth.fetchActualUserAsync();
        syncHandler.log(`=> userVariables => userVariables => variables`);
        const variables = authInfo && authInfo.variables ? authInfo.variables : user.getDefaultVariables();

        let saveUserData = {variables: {...variables}};
        for(let userVariable of userVariables) {
            if(!userVariable.key || typeof (userVariable.value) === 'undefined') { continue; }
            saveUserData['variables'][userVariable.key] = userVariable.value;
        }

        let queryData = {email: authInfo.email};
        syncHandler.log(`=> userVariables => userVariables => user.update => saveUserData`);
        user.update(queryData, setUpdateProps(saveUserData, curDate), {}, (err, count) => {
            if (!err && count) {
                // TODO: display updated settings
            }
        });

        syncHandler.log(`=> userVariables => end with variables`);
        return callback(err, saveUserData['variables']);
    });
}
