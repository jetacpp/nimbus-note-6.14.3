import chalk from "chalk";
import {default as config} from "../../../../../config";

/**
 * @param {{workspaceId: string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default function updateFolderCounters(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;

  /*FolderObjRepository.updateFolderCounters(inputData, (err, response) => {
      callback(err, response);
  });*/

  callback(null, null);
}
