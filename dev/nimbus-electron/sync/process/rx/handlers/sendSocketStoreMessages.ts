import chalk from "chalk";
import {default as SocketCollection} from "../../../socket/store/SocketCollection";
import {default as socketStoreType} from "../../../socket/socketStoreType";
import {default as errorHandler} from "../../../../utilities/errorHandler";

/**
 * @param {{workspaceId:string}} inputData
 */
export default function sendSocketStoreMessages(inputData) {
  const {workspaceId} = inputData;

  if (!SocketCollection.needSendSocketCollection({workspaceId})) {
    return;
  }

  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.NOTES_FOR_UPDATE_COUNTERS});
  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.NOTES_FOR_UPDATE});
  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.NOTES_FOR_REMOVE});
  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.NOTES_FOR_UPDATE_TEXT});

  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.FOLDERS_FOR_UPDATE});
  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.FOLDERS_FOR_REMOVE});

  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.TODO_LIST_FOR_UPDATE});
  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.TODO_LIST_FOR_REMOVE});

  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.NOTES_TAGS_FOR_UPDATE});
  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.TAGS_FOR_UPDATE});

  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.NOTES_ATTACHMENTS_FOR_UPDATE});
  SocketCollection.sendSocketCollectedData({workspaceId, storeType: socketStoreType.NOTES_ATTACHMENTS_FOR_REMOVE});
}

