import chalk from "chalk";
import {default as SocketCollection} from "../../../socket/store/SocketCollection";

/**
 * @param {{workspaceId:string}} inputData
 */
export default function clearSocketStore(inputData) {
  SocketCollection.flushSocketCollectedData(inputData);
}

