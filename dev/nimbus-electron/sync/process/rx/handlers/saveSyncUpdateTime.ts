import chalk from "chalk";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as sessionLastUpdateTime} from "./sessionLastUpdateTime";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as AccountManager} from "../../../nimbussdk/manager/AccountManager";

/**
 * @param {{workspaceId: string, service:NimbusSyncService}} inputData
 */
export default async function saveSyncUpdateTime(inputData) {
  // @ts-ignore
  return new Promise(function (resolve) {
    const {workspaceId} = inputData;

    AccountManager.getLastUpdateTime(inputData, (err, lastUpdateTime) => {
      if (err) {
        errorHandler.log({
          err: err, response: lastUpdateTime,
          description: "Sync problem => saveSyncUpdateTime => getLastUpdateTime",
          data: inputData
        });
        return resolve(null);
      }

      let syncSessionUpdateTime = sessionLastUpdateTime.getSyncSessionUpdateTime(inputData);
      if (syncSessionUpdateTime && (syncSessionUpdateTime > lastUpdateTime)) {
        AccountManager.setLastUpdateTime({
          workspaceId,
          lastUpdateTime: syncSessionUpdateTime
        }, (err, responseTime) => {
          if (err) {
            errorHandler.log({
              err: err, response: responseTime,
              description: "Sync problem => saveSyncUpdateTime => setLastUpdateTime",
              data: {
                workspaceId: workspaceId,
                syncSessionUpdateTime: syncSessionUpdateTime,
                lastUpdateTime: lastUpdateTime
              }
            });
            return resolve(null);
          }
          resolve(responseTime);
        });
      } else {
        resolve(null);
      }
    });
  });
}
