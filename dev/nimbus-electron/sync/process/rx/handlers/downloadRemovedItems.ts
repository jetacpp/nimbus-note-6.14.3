import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as SyncStatusDisplayEvent} from "../../events/SyncStatusDisplayEvent";
import {default as FolderObjRepository} from "../../repositories/FolderObjRepository";
import {default as NoteObjRepository} from "../../repositories/NoteObjRepository";
import {default as AttachmentObjRepository} from "../../repositories/AttachmentObjRepository";
import {default as errorHandler} from "../../../../utilities/errorHandler";

const TYPE_NOTE = "note";
const TYPE_ATTACHEMENT = "attachement";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default async function downloadRemovedItems(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  NimbusSDK.getApi().getRemovedItems(inputData, async (err, response) => {
    if (err) {
      await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
      errorHandler.log({
        err: err, response: response,
        description: "Sync problem => downloadRemovedItems => getRemovedItems"
      });
      return callback(err, false);
    }

    /**
     * @param {{workspaceId:string, globalId:string, removedTime:int}} inputData
     * @return {Promise<Object|null>}
     */
    let getAvailableFolder = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        const {workspaceId, globalId, removedTime} = inputData;
        FolderObjRepository.get({workspaceId, globalId}, (err, folder) => {
          if (err) {
            errorHandler.log({
              err: err, response: folder,
              description: "Sync problem => downloadRemovedItems => getAvailableFolder",
              data: inputData
            });
            return resolve(null);
          }

          if (folder && (folder.syncDate < removedTime)) {
            resolve(globalId);
          } else {
            resolve(null);
          }
        });
      });
    };

    /**
     * @param {{workspaceId:string, globalId:string, removedTime:int}} inputData
     * @return {Promise<Object|null>}
     */
    let getAvailableNote = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        const {workspaceId, globalId, removedTime} = inputData;
        NoteObjRepository.get({workspaceId, globalId}, (err, note) => {
          if (err) {
            errorHandler.log({
              err: err, response: note,
              description: "Sync problem => downloadRemovedItems => getAvailableNote",
              data: inputData
            });
            return resolve(null);
          }

          if (note && note.syncDate < removedTime) {
            resolve(globalId);
          } else {
            resolve(null);
          }
        });
      });
    };

    /**
     * @param {{workspaceId:string, globalId:string, removedTime:int}} inputData
     * @return {Promise<Object|null>}
     */
    let getAvailableAttachment = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        const {workspaceId, globalId, removedTime} = inputData;
        AttachmentObjRepository.get({workspaceId, globalId}, (err, attachment) => {
          if (err) {
            errorHandler.log({
              err: err, response: attachment,
              description: "Sync problem => downloadRemovedItems => getAvailableAttachment",
              data: inputData
            });
          }

          if (attachment && attachment.syncDate < removedTime) {
            resolve(globalId);
          } else {
            resolve(null);
          }
        });
      });
    };

    /**
     * @param {{workspaceId:string, globalId:string}} inputData
     * @return {Promise<any>}
     */
    let checkIfFolderExist = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        FolderObjRepository.checkIfFolderExist(inputData, (err, isFolderExist) => {
          if (err) {
            errorHandler.log({
              err: err, response: isFolderExist,
              description: "Sync problem => downloadRemovedItems => checkIfFolderExist",
              data: inputData
            });
          }
          resolve(isFolderExist);
        });
      });
    };

    /**
     * @param {{workspaceId:string, removedItems:[NoteObj|FolderObj]}} inputData
     * @return {Promise<Array>}
     */
    let checkUserRemovedItems = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, removedItems} = inputData;
        let removedItemsObj = {
          'folders': [],
          'notes': [],
          'attachments': []
        };

        for (let removedItem of removedItems) {
          let globalId = removedItem.global_id;
          let removedTime = removedItem.time;
          let type = removedItem.type;
          let findGlobalId = null;

          if (type.toLowerCase() === TYPE_NOTE) {
            let isFolderExist = await checkIfFolderExist({workspaceId, globalId});
            if (isFolderExist) {
              findGlobalId = await getAvailableFolder({workspaceId, globalId, removedTime});
              if (findGlobalId) {
                removedItemsObj['folders'].push(findGlobalId);
              }
            } else {
              findGlobalId = await getAvailableNote({workspaceId, globalId, removedTime});
              if (findGlobalId) {
                removedItemsObj['notes'].push(findGlobalId);
              }
            }
          } else if (type.toLowerCase() === TYPE_ATTACHEMENT) {
            findGlobalId = await getAvailableAttachment({workspaceId, globalId, removedTime});
            if (findGlobalId) {
              removedItemsObj['attachments'].push(findGlobalId);
            }
          }
        }

        resolve(removedItemsObj);
      });
    };

    let removedItems = response && response.removedItems ? response.removedItems : [];

    let folders = [], notes = [], attachments = [];

    if (removedItems && removedItems.length) {
      let removedItemsObj = await checkUserRemovedItems({workspaceId, removedItems});

      folders = removedItemsObj['folders'];
      notes = removedItemsObj['notes'];
      attachments = removedItemsObj['attachments'];

      /**
       * @param {{workspaceId:string, folderIds:[string]}} inputData
       */
      let removeUserFolders = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          FolderObjRepository.deleteRemovedItemsDownloadedFromServerI(inputData, (err, response) => {
            if (err) {
              errorHandler.log({
                err: err, response: response,
                description: "Sync problem => downloadRemovedItems => checkUserRemovedItems => removeUserFolders",
                data: inputData
              });
              return resolve(false);
            }

            resolve(response);
          });
        });
      };

      let deleteUserNotes = async (notes) => {
        // @ts-ignore
        return new Promise((resolve) => {
          NoteObjRepository.deleteRemovedItemsDownloadedFromServerI({
            workspaceId,
            globalIds: notes
          }, (err, response) => {
            if (err) {
              errorHandler.log({
                err: err, response: response,
                description: "Sync problem => downloadRemovedItems => checkUserRemovedItems => deleteUserNotes",
                data: {
                  notes: notes
                }
              });
              return resolve(false);
            }
            resolve(response);
          });
        });
      };

      /**
       * @param {{workspaceId:string, globalIds:[string]}} inputData
       */
      let deleteUserAttachments = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, globalIds} = inputData;
          AttachmentObjRepository.deleteRemovedItemsDownloadedFromServerI({workspaceId, globalIds}, (err, response) => {
            if (err) {
              errorHandler.log({
                err: err, response: response,
                description: "Sync problem => downloadRemovedItems => checkUserRemovedItems => deleteUserAttachments",
                data: inputData
              });
              return resolve(false);
            }
            resolve(response);
          });
        });
      };

      if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
      if (folders.length || notes.length || attachments.length) {
        if (folders.length) {
          if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
          await SyncStatusDisplayEvent.set({
            workspaceId: workspaceId,
            newStatus: SyncStatusDisplayEvent.STATUS.REMOVE_FOLDERS
          });
          await removeUserFolders({workspaceId, folderIds: folders});
        }
        if (notes.length) {
          if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
          await SyncStatusDisplayEvent.set({
            workspaceId: workspaceId,
            newStatus: SyncStatusDisplayEvent.STATUS.REMOVE_NOTES
          });
          await deleteUserNotes(notes);
        }
        if (attachments.length) {
          if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
          await SyncStatusDisplayEvent.set({
            workspaceId: workspaceId,
            newStatus: SyncStatusDisplayEvent.STATUS.REMOVE_ATTACHMENTS
          });
          await deleteUserAttachments({workspaceId, globalIds: attachments});
        }
      }
    }

    callback(err, true);
  });
}
