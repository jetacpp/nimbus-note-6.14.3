import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as selectedItem} from "../../../../window/selectedItem";
import {default as PreviewSyncManager} from "../../../nimbussdk/manager/PreviewSyncManager";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as SyncStatusDisplayEvent} from "../../events/SyncStatusDisplayEvent";
import {default as AttachmentObjRepository} from "../../repositories/AttachmentObjRepository";
import {default as AttachmentHubDownloader} from "../../hub/AttachmentHubDownloader";
import {default as socketConnection} from "../../../socket/socketFunctions";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import syncHandler from "../../../../utilities/syncHandler";

const MAX_QUEUE_SIZE = 20;

/**
 * @param { workspaceId:string, service:NimbusSyncService } inputData
 * @param {Function} callback
 */
export default function fullAttachmentsInListDownloadForDownloadedNotes(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;
  syncHandler.log(`sync => fullAttachmentsInListDownloadForDownloadedNotes => start (ws: ${workspaceId})`);

  AttachmentObjRepository.getNotDownloadedAttachmentFromDownloadedNotesForDownloadFromServer(inputData, async (err, attachments) => {
    if (err) {
      errorHandler.log({
        err: err, response: attachments,
        description: "Sync problem => fullAttachmentsInListDownloadForDownloadedNotes => getNotDownloadedAttachmentFromDownloadedNotesForDownloadFromServer",
        data: inputData
      });
      return callback(err, false);
    }

    if (!attachments) {
      return callback(err, false);
    }

    if (!attachments.length) {
      return callback(err, true);
    }

    if (attachments.length) {
      if (await SyncStatusChangedEvent.needPreventSync(inputData)) return false;
      await SyncStatusDisplayEvent.set({
        workspaceId: workspaceId,
        newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOAD_ATTACHMENT
      });
    }

    /**
     * @param {{workspaceId:string, globalId:string}} inputData
     */
    let updateAttachmentDownloadCounter = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        AttachmentObjRepository.updateAttachmentDownloadCounter(inputData, (err, response) => {
          if (err) {
            errorHandler.log({
              err: err, response: response,
              description: "Sync problem => fullAttachmentsInListDownloadForDownloadedNotes => updateAttachmentDownloadCounter",
              data: inputData
            });
            return resolve(false);
          }
          resolve(response);
        });
      });
    };

    /**
     * @param {{workspaceId:string, entity:AttachmentObj}} inputData
     */
    let downloadAttachmentItem = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, entity} = inputData;
        let attachmentData = await AttachmentHubDownloader.getInstance().download(inputData);
        if (attachmentData && attachmentData.attachment) {
          let attachment = attachmentData.attachment;
          if (!attachmentData.downloaded) {
            await AttachmentObjRepository.updateAttachmentDownloadCounter({workspaceId, globalId: attachment.globalId});
            return resolve(false);
          }

          let noteGlobalId = selectedItem.get({
            workspaceId,
            itemType: selectedItem.TYPE_NOTE
          });
          if (attachment.noteGlobalId === noteGlobalId) {
            socketConnection.sendNoteAttachmentsUpdateMessage({
              workspaceId,
              noteGlobalId: attachment.noteGlobalId,
              globalId: attachment.globalId
            });
          }

          PreviewSyncManager.updateNotePreview({
            workspaceId,
            globalId: attachment.noteGlobalId,
            attachmentGlobalId: attachment.globalId
          });
          AttachmentObjRepository.updateAttachmentFromManualDownloadI({
            workspaceId,
            attachmentObj: attachment
          }, async (err, response) => {
            if (err) {
              errorHandler.log({
                err: err, response: response,
                description: "Sync problem => fullAttachmentsInListDownloadForDownloadedNotes => updateAttachmentFromManualDownloadI",
                data: {
                  attachment: attachment
                }
              });
              return resolve(false);
            }
            resolve(response);
          });
        } else {
          await AttachmentObjRepository.updateAttachmentDownloadCounter({workspaceId, globalId: entity.global_id});
          return resolve(false);
        }
      });
    };

    /**
     * @param {{workspaceId:string, attachment:AttachmentObj}} inputData
     */
    let checkAttachmentNeedDownload = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, attachment} = inputData;
        if (!attachment) {
          errorHandler.log({
            err: err, response: null,
            description: "Sync problem => fullAttachmentsInListDownloadForDownloadedNotes => checkAttachmentNeedDownload",
            data: inputData
          });
          return resolve(false);
        }

        let entity = await AttachmentHubDownloader.getInstance().checkNeedDownload({
          workspaceId,
          globalId: attachment.globalId
        });
        resolve(entity);
      });
    };

    let attachmentsNeedDownload = [];
    let attachmentsNotNeedDownload = [];

    let attachmentsLength = attachments.length;
    for (let i = 0; i < attachmentsLength; i++) {
      let entity = await checkAttachmentNeedDownload({workspaceId, attachment: attachments[i]});
      if (entity) {
        attachmentsNeedDownload.push(entity);
      } else {
        attachmentsNotNeedDownload.push(attachments[i]);
      }
    }
    attachments = null;

    /**
     * @param {{workspaceId:string, attachmentsList:[attachmentObj]}} inputData
     */
    let downloadAttachmentsQueue = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, attachmentsList} = inputData;
        if (!attachmentsList) {
          return resolve(true);
        }

        if (!attachmentsList.length) {
          return resolve(true);
        }

        let attachments = attachmentsList.slice();
        let totalCount = attachments.length;
        let downloadedCount = 0;
        let downloadingCount = 0;

        /**
         * @param {{workspaceId:string, attachment:AttachmentObj}} inputData
         */
        let downloadAttachmentQueueItem = async (inputData) => {
          const {workspaceId, attachment} = inputData;

          if (!attachment) {
            ++downloadedCount;
            return false;
          }

          if (await SyncStatusChangedEvent.needPreventSync(inputData)) return false;
          await downloadAttachmentItem({workspaceId, entity: attachment});
          ++downloadedCount;
          --downloadingCount;

          if (await SyncStatusChangedEvent.needPreventSync(inputData)) return false;
          await SyncStatusDisplayEvent.set({
            workspaceId: workspaceId,
            newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOAD_ATTACHMENT,
            props: {
              "current": downloadedCount,
              "total": totalCount
            }
          });

          return true;
        };

        let checkAttachQueueInterval = setInterval(async () => {

          if (await SyncStatusChangedEvent.needPreventSync(inputData)) {
            if (checkAttachQueueInterval) {
              clearInterval(checkAttachQueueInterval);
            }
            return resolve(false);
          }

          if (downloadedCount >= totalCount) {
            if (checkAttachQueueInterval) {
              clearInterval(checkAttachQueueInterval);
            }
            return resolve(true);
          }

          if (attachments.length && downloadingCount < MAX_QUEUE_SIZE) {
            ++downloadingCount;
            downloadAttachmentQueueItem({workspaceId, attachment: attachments.shift()});
          }

        }, 10);
      });
    };

    let attachmentsNeedDownloadSort = [];
    let totalNeedDownloadAttachments = attachmentsNeedDownload.length;
    let attachmentsNotNeedDownloadLength = attachmentsNotNeedDownload.length;

    if (totalNeedDownloadAttachments || attachmentsNotNeedDownloadLength) {
      if (await SyncStatusChangedEvent.needPreventSync(inputData)) return false;
      await SyncStatusDisplayEvent.set({
        workspaceId: workspaceId,
        newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOAD_ATTACHMENT
      });
    }

    for (let i = totalNeedDownloadAttachments - 1; i >= 0; i--) {
      if (attachmentsNeedDownload[i].in_list) {
        attachmentsNeedDownloadSort.push(attachmentsNeedDownload[i]);
      } else {
        attachmentsNeedDownloadSort.unshift(attachmentsNeedDownload[i]);
      }
    }
    attachmentsNeedDownload = null;
    await downloadAttachmentsQueue({workspaceId, attachmentsList: attachmentsNeedDownloadSort});

    for (let j = 0; j < attachmentsNotNeedDownloadLength; j++) {
      if (await SyncStatusChangedEvent.needPreventSync(inputData)) {
        return callback(err, false);
      }
      await AttachmentObjRepository.updateDownloadStatus({
        workspaceId,
        globalId: attachmentsNotNeedDownload[j].globalId
      });
    }

    callback(err, true);
  });
}
