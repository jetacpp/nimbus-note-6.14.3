import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as SyncStatusDisplayEvent} from "../../events/SyncStatusDisplayEvent";
import {default as sessionLastUpdateTime} from "./sessionLastUpdateTime";
import {default as NoteObjRepository} from "../../repositories/NoteObjRepository";
import {default as TagObjRepository} from "../../repositories/TagObjRepository";
import {default as TodoObjRepository} from "../../repositories/TodoObjRepository";
import {default as AttachmentObjRepository} from "../../repositories/AttachmentObjRepository";
import {default as NotesUpdateRequest} from "../../../nimbussdk/net/request/NotesUpdateRequest";
import {default as uploadUpdatedAttachmentsOnServer} from "./uploadUpdatedAttachmentsOnServer";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import workspace from "../../../../db/models/workspace";
import item from "../../../../db/models/item";
import TextEditor from "../../../../db/TextEditor";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import API from "../../../nimbussdk/net/API";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param { workspaceId:string, service:NimbusSyncService, syncStartDate:int } inputData
 * @param {Function} callback
 */
export default async function uploadUpdatedFullNotesOnServer(inputData, callback = (err, res) => {
}) {
  const {workspaceId, syncStartDate} = inputData;
  syncHandler.log(`sync => fullUserUpload => uploadUpdatedFullNotesOnServer => start (ws: ${workspaceId})`);

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<int>}
   */
  let getUpdatedFullNotesCountForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      NoteObjRepository.getUpdatedFullNotesCountForUploadOnServer(inputData, (err, availableNoteCount) => {
        if (err) {
          errorHandler.log({
            err: err, response: availableNoteCount,
            description: "Sync problem => uploadUpdatedFullNotesOnServer => getUpdatedFullNotesCountForUploadOnServer",
            data: inputData
          });
          return resolve(0);
        }
        resolve(availableNoteCount);
      });
    });
  };

  /**
   * @return {Promise<{}|null>}
   */
  let getUpdatedNotesForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      NoteObjRepository.getUpdatedNotesForUploadOnServer(inputData, (err, noteObjs) => {
        if (err) {
          errorHandler.log({
            err: err, response: noteObjs,
            description: "Sync problem => uploadUpdatedFullNotesOnServer => getUpdatedNotesForUploadOnServer",
            data: inputData
          });
          return resolve([]);
        }
        resolve(noteObjs);
      });
    });
  };

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @return {Promise<boolean>}
   */
  let checkIfNoteCanBePassedInAvailableTrafficQuotaForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      NoteObjRepository.checkIfNoteCanBePassedInAvailableTrafficQuotaForUploadOnServer(inputData, (err, noteCanBePassed) => {
        if (err || !noteCanBePassed) {
          errorHandler.log({
            err: err, response: noteCanBePassed,
            description: "Sync problem => uploadUpdatedFullNotesOnServer => checkIfNoteCanBePassedInAvailableTrafficQuotaForUploadOnServer",
            data: inputData
          });
          return resolve(false);
        }
        resolve(noteCanBePassed);
      });
    });
  };

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @return {Promise<boolean>}
   */
  let checkIfNoteNotMoreThanLimitNoteSize = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      NoteObjRepository.checkIfNoteNotMoreThanLimitNoteSize(inputData, (err, noteNotMoreThanLimitSize) => {
        if (err || !noteNotMoreThanLimitSize) {
          errorHandler.log({
            err: err, response: noteNotMoreThanLimitSize,
            description: "Sync problem => uploadUpdatedFullNotesOnServer => checkIfNoteNotMoreThanLimitNoteSize",
            data: inputData
          });
          return resolve(false);
        }
        resolve(noteNotMoreThanLimitSize);
      });
    });
  };

  /**
   * @param {{workspaceId:string, noteGlobalId:string}}  inputData
   * @return {Promise<[]>}
   */
  let uploadUserUpdatedAttachmentsOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      uploadUpdatedAttachmentsOnServer(inputData, (err, attachments) => {
        if (err) {
          errorHandler.log({
            err: err, response: attachments,
            description: "Sync problem => uploadUpdatedFullNotesOnServer => uploadUserUpdatedAttachmentsOnServer",
            data: inputData
          });
          return resolve([]);
        }
        resolve(attachments);
      });
    });
  };

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @return {Promise<{}>}
   */
  let getSyncNoteEntity = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      NoteObjRepository.getSyncNoteEntity(inputData, (err, note) => {
        if (err || !note) {
          errorHandler.log({
            err: err, response: note,
            description: "Sync problem => uploadUpdatedFullNotesOnServer => getSyncNoteEntity",
            data: inputData
          });
          return resolve(null);
        }
        note.date_updated_user = note.date_updated_user ? note.date_updated_user : note.date_added_user;
        resolve(note);
      });
    });
  };

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @return {Promise<[]>}
   */
  let getSyncTodoEntityList = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      TodoObjRepository.getSyncTodoEntityList(inputData, (err, noteTodos) => {
        if (err) {
          errorHandler.log({
            err: err, response: noteTodos,
            description: "Sync problem => uploadUpdatedFullNotesOnServer => getSyncTodoEntityList",
            data: inputData
          });
          return resolve([]);
        }

        if (!noteTodos.length) {
          return resolve([]);
        }

        resolve(noteTodos.map(noteTodo => ({...noteTodo})));
      });
    });
  };

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @return {Promise<[]>}
   */
  let getRenamedAttachmentsForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      AttachmentObjRepository.getRenamedAttachmentsForUploadOnServer(inputData, (err, renamedAttachments) => {
        if (err) {
          errorHandler.log({
            err: err, response: renamedAttachments,
            description: "Sync problem => uploadUpdatedFullNotesOnServer => getRenamedAttachmentsForUploadOnServer",
            data: inputData
          });
          return resolve([]);
        }
        resolve(renamedAttachments);
      });
    });
  };

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<[]>}
   */
  let getRenamedTagsForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      TagObjRepository.getRenamedTagsForUploadOnServer(inputData, (err, renameTags) => {
        if (err) {
          errorHandler.log({
            err: err, response: renameTags,
            description: "Sync problem => uploadUpdatedFullNotesOnServer => getRenamedAttachmentsForUploadOnServer",
            data: inputData
          });
        }

        resolve(renameTags);
      });
    });
  };

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<[]>}
   */
  let getUpdatedTagsForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      TagObjRepository.getUpdatedTagsForUploadOnServer(inputData, (err, storeTags) => {
        if (err) {
          errorHandler.log({
            err: err, response: storeTags,
            description: "Sync problem => uploadUpdatedFullNotesOnServer => getUpdatedTagsForUploadOnServer",
            data: inputData
          });
        }
        resolve(storeTags);
      });
    });
  };

  /**
   * @param {{workspaceId:string, syncStartDate:int, notesForUpload:[NoteObj]}} inputData
   */
  let uploadNotesOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise(async (resolve) => {
      let {workspaceId, notesForUpload} = inputData;

      syncHandler.log(`=> uploadNotesOnServer => start for workspaceId: ${workspaceId}`);

      let body = new NotesUpdateRequest.Body({});
      let notes = notesForUpload.map(note => NoteObjRepository.cleanUploadItem(note));

      let renameTags = <[string]>await getRenamedTagsForUploadOnServer(inputData);
      let storeTags = <[string]>await getUpdatedTagsForUploadOnServer(inputData);

      body.setRenameTags(renameTags);
      body.storeTags(storeTags);
      body.storeNotes(notes.filter(note => note.global_id));
      body.removeAttachements();

      if (await SyncStatusChangedEvent.needPreventSync(inputData)) return resolve(false);

      syncHandler.log(`=> uploadNotesOnServer => updateNotes => body`);
      NimbusSDK.getApi().updateNotes({workspaceId, body}, async (err, responseBody) => {
        syncHandler.log(`=> uploadNotesOnServer => updateNotes => err: ${err}`);

        if (err || !responseBody) {
          await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
          let errorData = {
            err: err, response: responseBody,
            description: "Sync problem => uploadUpdatedFullNotesOnServer => updateNotes",
            data: inputData
          };
          errorHandler.log(errorData);
          errorHandler.displayErrorPopup(errorData);
          if(err === API.ERROR_TEMPFILE_NOT_FOUND) {
            await checkFailedItem({
              workspaceId,
              responseBody
            });
          } else if(err === API.ERROR_ATTACHMENT_NOT_FOUND) {
            if(responseBody && responseBody.body && responseBody.body._errorDesc) {
              const errData = responseBody.body._errorDesc.split(' ');
              if(typeof errData[1] !== 'undefined' && errData[1] && errData[1].length > 10) {
                await AttachmentObjRepository.eraseAttachmentFile({
                  workspaceId: workspaceId,
                  globalId: errData[1],
                });
                if(body && body.store && body.store.notes) {
                  const noteItem = body.store.notes.find(note => {
                    return note.preview && (note.preview.global_id === errData[1]);
                  });
                  if(noteItem && noteItem.global_id) {
                    await NoteObjRepository.removeItemPreview({
                      workspaceId: workspaceId,
                      globalId: noteItem.global_id,
                    });
                  }
                }
              }
            }
          }
          return resolve(false);
        }

        /**
         * @param {{workspaceId:string, note:NoteObj, lastUpdateTime:int, syncStartDate:int}} inputData
         * @return {Promise<[]>}
         */
        let noteCallAfterUploadFullSyncI = async (inputData) => {
          // @ts-ignore
          return new Promise((resolve) => {
            const {note} = inputData;
            // @ts-ignore
            NoteObjRepository.callAfterUploadFullSyncI(inputData, async (err, response) => {
              if (err) {
                errorHandler.log({
                  err: err, response: response,
                  description: "Sync problem => uploadUpdatedFullNotesOnServer => noteCallAfterUploadFullSyncI",
                  data: {
                    note: note
                  }
                });
              }

              if(note && note.global_id && note.noteObj) {
                if(note.noteObj.text_version > 1) {
                  const {noteObj} = note;
                  let noteObjJson = item.getResponseJson(noteObj);

                  if(noteObjJson) {
                    noteObjJson['synced'] = true;
                    const workspaceInstance = <{globalId:string}>(workspaceId ? await workspace.getById(workspaceId) : await workspace.getDefault());
                    if(workspaceInstance) {

                      const saveTextDate = {
                        note: noteObjJson,
                        workspaceId: workspaceInstance.globalId,
                        noteId: noteObj.globalId
                      };

                      const saveTextRes = <{result: boolean}>await TextEditor.makeBgNoteSync(saveTextDate);
                      if(saveTextRes && !saveTextRes.result) {
                        syncHandler.sendLog({
                          err: new Error(`Can not upload note text v2: ${workspaceInstance.globalId}_${noteObj.globalId}`),
                          response: saveTextRes,
                          description: "Sync problem => uploadNotesOnServer => makeBgNoteSync",
                          data: saveTextDate
                        });
                      }
                    }
                  }
                }
                delete note.noteObj;
              }

              resolve(true);
            });
          });
        };

        /**
         * @param {{workspaceId:string, note:NoteObj}} inputData
         * @return {Promise<[]>}
         */
        let attachmentCallAfterUploadFullSyncI = async (inputData) => {
          // @ts-ignore
          return new Promise((resolve) => {
            const {workspaceId, note} = inputData;
            AttachmentObjRepository.callAfterUploadFullSyncI({
              workspaceId,
              parentId: note.globalId
            }, (err, response) => {
              if (err) {
                errorHandler.log({
                  err: err, response: response,
                  description: "Sync problem => uploadUpdatedFullNotesOnServer => attachmentCallAfterUploadFullSyncI",
                  data: inputData
                });
              }
              resolve(true);
            });
          });
        };

        /**
         * @param {{workspaceId:string, note:NoteObj}} inputData
         * @return {Promise<[]>}
         */
        let attachmentCallAfterUploadRenamedAttachmentsI = async (inputData) => {
          // @ts-ignore
          return new Promise((resolve) => {
            const {workspaceId, note} = inputData;
            AttachmentObjRepository.callAfterUploadRenamedAttachmentsI({
              workspaceId,
              parentId: note.globalId
            }, (err, response) => {
              if (err) {
                errorHandler.log({
                  err: err, response: response,
                  description: "Sync problem => uploadUpdatedFullNotesOnServer => attachmentCallAfterUploadRenamedAttachmentsI",
                  data: inputData
                });
              }
              resolve(note);
            });
          });
        };

        let updateNotesIdList = [];
        for (let note of notes) {
          if (await SyncStatusChangedEvent.needPreventSync(inputData)) { break }
          if (responseBody && responseBody.date_updated && typeof (responseBody.date_updated[note.global_id]) !== "undefined") {
            await noteCallAfterUploadFullSyncI({
              workspaceId,
              note,
              lastUpdateTime: responseBody.date_updated[note.global_id],
              syncStartDate
            });
            updateNotesIdList.push(note.global_id);
          }

          if (await SyncStatusChangedEvent.needPreventSync(inputData)) { break }
          await attachmentCallAfterUploadFullSyncI({workspaceId, note});

          if (await SyncStatusChangedEvent.needPreventSync(inputData)) { break }
          await attachmentCallAfterUploadRenamedAttachmentsI({workspaceId, note});

          if (await SyncStatusChangedEvent.needPreventSync(inputData)) { break }
          socketConnection.sendItemUpdateMessage({workspaceId, globalId: note.global_id});
        }

        if (responseBody && responseBody.last_update_time) {
          if (await SyncStatusChangedEvent.needPreventSync(inputData)) return resolve(false);
          sessionLastUpdateTime.setSyncSessionUpdateTime({workspaceId, lastUpdateTime: responseBody.last_update_time});

          if (await SyncStatusChangedEvent.needPreventSync(inputData)) return resolve(false);
          await AttachmentObjRepository.updateLocations({workspaceId, savedAttachments: responseBody.savedAttachments});
        } else {
          if (config.SHOW_WEB_CONSOLE) {
            console.log(`Set Sync Session Update Time fail (ws: ${workspaceId}):`, responseBody);
          }
          errorHandler.log({
            err: err, response: responseBody,
            description: "Sync problem => uploadUpdatedFullNotesOnServer"
          });
        }
        resolve(true);
      });
    });
  };

  let availableNoteCount = await getUpdatedFullNotesCountForUploadOnServer(inputData);
  if (availableNoteCount) {
    syncHandler.log(`sync => uploadUpdatedFullNotesOnServer => availableNoteCount: ${availableNoteCount} (ws: ${workspaceId})`);
  } else {
    syncHandler.log(`sync => uploadUpdatedFullNotesOnServer => All notes uploaded. No notes to upload: ${availableNoteCount} (ws: ${workspaceId})`);
    return callback(null, true);
  }

  let noteObjs: any = await getUpdatedNotesForUploadOnServer(inputData);

  if (!noteObjs.length) {
    if (config.SHOW_WEB_CONSOLE) {
      console.log(`No notes for upload (ws: ${workspaceId})`);
    }
    return callback(null, true);
  }

  let uploadNotesList = [];
  for (let noteObj of noteObjs) {
    let noteGlobalId = noteObj.globalId;

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    let noteCanBePassed = await checkIfNoteCanBePassedInAvailableTrafficQuotaForUploadOnServer({
      workspaceId,
      noteGlobalId
    });
    if (config.SHOW_WEB_CONSOLE) {
      //console.log("noteCanBePassed: ", noteCanBePassed, noteGlobalId);
    }

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    if (!noteCanBePassed) {
      await SyncStatusDisplayEvent.set({
        workspaceId: workspaceId,
        newStatus: SyncStatusDisplayEvent.STATUS.TRAFFIC_LIMIT
      });
      errorHandler.log({
        err: null, response: null,
        description: "Sync problem => uploadUpdatedFullNotesOnServer",
        data: {
          noteCanBePassed,
          noteGlobalId
        }
      });

      return callback(null, false);
    }

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    let noteNotMoreThanLimitSize = await checkIfNoteNotMoreThanLimitNoteSize({workspaceId, noteGlobalId});
    if (!noteNotMoreThanLimitSize) {
      if (config.SHOW_WEB_CONSOLE) {
        console.log(`noteNotMoreThanLimitSize: ${noteNotMoreThanLimitSize} ${noteGlobalId} (ws: ${workspaceId})`);
      }
      errorHandler.log({
        err: null, response: null,
        description: "Sync problem => uploadUpdatedFullNotesOnServer",
        data: {
          noteNotMoreThanLimitSize: noteNotMoreThanLimitSize,
          noteGlobalId: noteGlobalId
        }
      });
      continue;
    }

    let note: any = await getSyncNoteEntity({workspaceId, noteGlobalId});

    if (config.SHOW_WEB_CONSOLE) {
      // console.log(`Sync note not found for upload: ${noteGlobalId} (ws: ${workspaceId})`);
    }

    if (!note) {
      continue;
    }

    let noteTodos: any = await getSyncTodoEntityList({workspaceId, noteGlobalId});

    if (config.SHOW_WEB_CONSOLE) {
      //console.log(`noteTodos (ws: ${workspaceId}):`, noteTodos);
    }

    if (noteTodos.length) {
      note.todo = noteTodos;
    }

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    let attachments: any = await uploadUserUpdatedAttachmentsOnServer({workspaceId, noteGlobalId});

    if (config.SHOW_WEB_CONSOLE) {
      //console.log(`attachments for note ${noteGlobalId} (ws: ${workspaceId}):`, attachments);
    }

    if (attachments.length) {
      note.setAttachments(attachments);
    }

    let renamedAttachments: any = await getRenamedAttachmentsForUploadOnServer({workspaceId, noteGlobalId});

    if (config.SHOW_WEB_CONSOLE) {
      //console.log(`renamedAttachments for note ${noteGlobalId} (ws: ${workspaceId}):`, renamedAttachments);
    }

    if (renamedAttachments.length) {
      note.setAttachments(renamedAttachments);
    }

    if (note.is_encrypted) {
      if (typeof (note.text) !== "undefined") {
        delete note.text;
      }
      if (typeof (note.text_short) !== "undefined") {
        delete note.text_short;
      }
    }

    uploadNotesList.push(note);
  }

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  if (uploadNotesList.length) {
    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.UPLOAD_NOTES
    });
  }

  syncHandler.log(`sync => fullUpload => uploadUpdatedFullNotesOnServer => uploadNotesOnServer (ws: ${workspaceId})`);
  let notesUploadResult = await uploadNotesOnServer({workspaceId, syncStartDate, notesForUpload: uploadNotesList});
  if (config.SHOW_WEB_CONSOLE) {
    //console.log(`Sync note upload result: ${noteUploadResult} (ws: ${workspaceId})`);
  }

  callback(null, true);
}

async function checkFailedItem({workspaceId, responseBody}) {
  if(!responseBody) {
    return;
  }

  const { failedItem } = responseBody;
  if(!failedItem) {
    return;
  }

  const { type, item } = failedItem;
  if(!item) {
    return;
  }

  if(type === 'attachment') {
    const globalId = item.global_id;
    if(!globalId) {
      return;
    }

    const removeAttachment = await AttachmentObjRepository.findAttachment({workspaceId, globalId});
    if (!removeAttachment) {
      return;
    }

    await AttachmentObjRepository.removeNoteAttachmentList({
      workspaceId,
      attachmentList: [removeAttachment]
    });
  }
}
