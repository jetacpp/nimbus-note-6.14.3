import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as userModel} from "../../../../db/models/user";
import AvatarSingleDownloader from "../../../downlaoder/AvatarSingleDonwloader";
import {default as appWindow} from "../../../../window/instance";

/**
 * @param {{workspaceId:string, service:NimbusSyncService, skipSyncHandlers:boolean, saveUserTrial:boolean, userInstance:{}}} inputData
 * @param {Function} callback
 */
export default function getTrial(inputData, callback = (err, res) => {
}) {
  const {workspaceId, skipSyncHandlers, saveUserTrial, userInstance} = inputData;
  if (!skipSyncHandlers) {
    if (config.SHOW_WEB_CONSOLE) {
      console.log(`Start getTrial sync (ws: ${workspaceId})`);
    }
  }

  NimbusSDK.getApi().getTrial({workspaceId, skipSyncHandlers}, async (err, userTrial) => {
    if (err) {
      if (!skipSyncHandlers) {
        await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
      }

      errorHandler.log({
        err: err, response: userTrial,
        description: "Sync problem => getTrial => getTrial"
      });

      return callback(err, false);
    }

    if(saveUserTrial && userInstance && userTrial) {
      if(Object.keys(userTrial) && Object.keys(userInstance)) {
        userModel.find({email: userInstance.login}, {}, (err, findUserInstance) => {
          if (!err && findUserInstance) {
            userModel.update({email: userInstance.login}, {trial: userTrial}, {workspaceId}, (err, res) => {
              if (!err && res) {
                // TODO: update webnotes trial
                // appWindow.get().webContents.send('event:client:me:name:update:response', {});
              }
            });
          }
        });
      }
    }

    return callback(null, userTrial);
  });
}