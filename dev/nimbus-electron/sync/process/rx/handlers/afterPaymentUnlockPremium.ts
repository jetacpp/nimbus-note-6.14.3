import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as errorHandler} from "../../../../utilities/errorHandler";

/**
 * @param {{productId:string, token:string}} inputData
 * @param {Function} callback
 */
export default async function afterPaymentUnlockPremium(inputData, callback = (err, res) => {
}) {
  if (config.SHOW_WEB_CONSOLE) {
    console.log("Start After Payment Unlock Premium");
  }

  NimbusSDK.getApi().unlockPremium(inputData, async (err) => {
    if (err) {
      errorHandler.log({
        err: err, response: null,
        description: "Sync problem => afterPaymentUnlockPremium",
        data: inputData
      });
      return callback(err, false);
    }

    callback(err, true);
  });
}
