import chalk from "chalk";
import {default as fullDownload} from "./fullDownload";

/**
 * @param {{workspaceId:string, service: NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default function fullSync(inputData, callback = () => {
}) {
  fullDownload(inputData, callback);
}
