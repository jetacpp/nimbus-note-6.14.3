import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as uploadErasedFromTrashItems} from "./uploadErasedFromTrashItems";
import {default as uploadUpdatedFoldersAndTags} from "./uploadUpdatedFoldersAndTags";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default function fastUpload(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;
  syncHandler.log(`sync => fastUserUpload => start (ws: ${workspaceId})`);

  syncHandler.log(`sync => fastUserUpload => uploadErasedFromTrashItems (ws: ${workspaceId})`);
  uploadErasedFromTrashItems(inputData, (err) => {
    if (err) {
      errorHandler.log({
        err: err, response: null,
        description: "Sync problem => fastUpload => uploadErasedFromTrashItems",
        data: inputData
      });
      return callback(err, null);
    }

    syncHandler.log(`sync => fastUserUpload => uploadUpdatedFoldersAndTags (ws: ${workspaceId})`);
    uploadUpdatedFoldersAndTags(inputData, (err) => {
      if (err) {
        errorHandler.log({
          err: err, response: null,
          description: "Sync problem => fastUpload => uploadUpdatedFoldersAndTags",
          data: inputData
        });
        return callback(err, null);
      }

      callback(err, true);
    });
  });
}

