import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
// @ts-ignore
export default async function getDownloadNotesTotalAmount(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) callback(null, false);
  NimbusSDK.getApi().getNotesTotalAmount(inputData, async (err, body) => {
    if (err || !body) {
      await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
      errorHandler.log({
        err: err, response: body,
        description: "Sync problem => getDownloadNotesTotalAmount => getNotesTotalAmount"
      });
      return callback(err, null);
    }

    if (!Object.keys(body).length) {
      await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
      errorHandler.log({
        err: err, response: body,
        description: "Sync problem => getDownloadNotesTotalAmount => getNotesTotalAmount"
      });
      return callback(err, null);
    }

    let totalIterations = 0;
    let totalNotes = body.totalAmount;

    if (totalNotes > 0) {
      totalIterations = Math.ceil(totalNotes / config.DOWNLOAD_NOTE_COUNT_PER_STEP);
    }

    if (config.SHOW_WEB_CONSOLE) {
      syncHandler.log(`sync => getDownloadNotesTotalAmount => Total notes ready for download from server: ${totalNotes} (ws: ${workspaceId})`);
    }

    callback(err, {
      totalIterations,
      totalNotes
    });
  });
}
