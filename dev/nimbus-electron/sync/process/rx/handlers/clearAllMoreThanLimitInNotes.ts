import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as NoteObjRepository} from "../../repositories/NoteObjRepository";
import {default as errorHandler} from "../../../../utilities/errorHandler";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default function clearAllMoreThanLimitInNotes(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;

  NoteObjRepository.clearAllMoreThanLimitInNotesI(inputData, (err, response) => {
    if (err) {
      errorHandler.log({
        err: err, response: response,
        description: "Sync problem => clearAllMoreThanLimitInNotes => clearAllMoreThanLimitInNotesI",
        data: inputData
      });
    }
    callback(err, response);
  });
}
