import chalk from "chalk";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as userModel} from "../../../../db/models/user";
import {default as appWindow} from "../../../../window/instance";
import AvatarSingleDownloader from "../../../downlaoder/AvatarSingleDonwloader";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default async function userInfo(inputData, callback = (err, res) => {
}) {
  const {workspaceId, saveUserInfo} = inputData;
  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  NimbusSDK.getApi().userInfo({workspaceId}, async (err, userInstance) => {
    if (err) {
      await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
      errorHandler.log({
        err: err, response: userInstance,
        description: "Sync problem => userInfo",
        data: inputData
      });
      return callback(err, false);
    }

    if (saveUserInfo && userInstance) {
      userModel.find({email: userInstance.login}, {}, async (err, findUserInstance) => {
        if (!err && findUserInstance) {
          let saveUserData = <any>{};

          if (typeof (userInstance.firstname) !== 'undefined') {
            if (findUserInstance.firstname !== userInstance.firstname) {
              saveUserData.firstname = userInstance.firstname;
            }
          }
          if (typeof (userInstance.lastname) !== 'undefined') {
            if (findUserInstance.lastname !== userInstance.lastname) {
              saveUserData.lastname = userInstance.lastname;
            }
          }
          if (typeof (userInstance.username) !== 'undefined') {
            if (findUserInstance.username !== userInstance.username) {
              saveUserData.username = userInstance.username;
            }
          }

          if (typeof (userInstance.avatar) !== 'undefined') {
            if(userInstance.avatar) {
              if (findUserInstance.avatar) {
                if(findUserInstance.avatar.url !== userInstance.avatar.url) {
                  saveUserData.avatar = userInstance.avatar;
                } else {
                  const avatarFileExist = await AvatarSingleDownloader.checkAvatarExist(userInstance.avatar.url);
                  if(!avatarFileExist) {
                    saveUserData.avatar = userInstance.avatar;
                  }
                }
              } else {
                saveUserData.avatar = userInstance.avatar;
              }
            } else {
              saveUserData.avatar = null;
            }
          }

          if(Object.keys(saveUserData).length) {
            userModel.update({email: userInstance.login}, saveUserData, {workspaceId}, (err, res) => {
              if (!err && res) {
                if (typeof (saveUserData.firstname) !== 'undefined' || typeof (saveUserData.lastname) !== 'undefined' || typeof (saveUserData.username) !== 'undefined') {
                  appWindow.get().webContents.send('event:client:me:name:update:response', {
                    firstname: typeof (saveUserData.firstname) === "undefined" ? findUserInstance.firstname : saveUserData.firstname,
                    lastname: typeof (saveUserData.lastname) === "undefined" ? findUserInstance.lastname : saveUserData.lastname,
                    username: typeof (saveUserData.username) === "undefined" ? findUserInstance.username : saveUserData.username
                  });
                }
                if (typeof (saveUserData.avatar) !== 'undefined') {
                  if(saveUserData.avatar) {
                    AvatarSingleDownloader.download({
                      url: saveUserData.avatar.url,
                      oldUrl: findUserInstance.avatar ? findUserInstance.avatar.url : '',
                      type: AvatarSingleDownloader.AVATAR_TYPE_USER,
                    });
                  } else {
                    if(findUserInstance.avatar) {
                      appWindow.get().webContents.send('event:client:me:avatar:remove:response', {});
                    }
                  }
                }
              }
            });
          }
        }
      });
    }

    callback(err, userInstance);
  });
}
