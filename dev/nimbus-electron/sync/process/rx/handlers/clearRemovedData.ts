import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as AttachmentObjRepository} from "../../repositories/AttachmentObjRepository";
import {default as TodoObjRepository} from "../../repositories/TodoObjRepository";
import {default as TagObjRepository} from "../../repositories/TagObjRepository";
import {default as NoteObjRepository} from "../../repositories/NoteObjRepository";
import {default as FolderObjRepository} from "../../repositories/FolderObjRepository";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
// @ts-ignore
export default async function clearRemovedData(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;
  syncHandler.log(`sync => clearRemovedData => start (ws: ${workspaceId})`);

  /**
   * @return {Promise<boolean>}
   */
  let clearRemovedAttaches = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      AttachmentObjRepository.clearRemovedData(inputData, (err, response) => {
        if (err) {
          errorHandler.log({
            err: err, response: response,
            description: "Sync problem => clearRemovedData => clearRemovedAttaches",
            data: inputData
          });
          return resolve(false);
        }
        resolve(response);
      });
    });
  };

  /**
   * @return {Promise<boolean>}
   */
  let clearRemovedTodos = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      TodoObjRepository.clearRemovedData(inputData, (err, response) => {
        if (err) {
          return resolve(false);
        }
        resolve(response);
      });
    });
  };

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<boolean>}
   */
  let clearRemovedTags = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      TagObjRepository.clearRemovedData(inputData, (err, response) => {
        if (err) {
          return resolve(false);
        }
        resolve(response);
      });
    });
  };

  /**
   * @return {Promise<boolean>}
   */
  let clearRemovedNotes = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      NoteObjRepository.clearRemovedData(inputData, (err, response) => {
        if (err) {
          return resolve(false);
        }

        resolve(response);
      });
    });
  };

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<boolean>}
   */
  let clearRemovedFolders = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      FolderObjRepository.clearRemovedData(inputData, (err, response) => {
        if (err) {
          return resolve(false);
        }

        resolve(response);
      });
    });
  };

  syncHandler.log(`sync => clearRemovedData => clearRemovedAttaches (ws: ${workspaceId})`);
  await clearRemovedAttaches(inputData);

  syncHandler.log(`sync => clearRemovedData => clearRemovedTodos (ws: ${workspaceId})`);
  await clearRemovedTodos(inputData);

  syncHandler.log(`sync => clearRemovedData => clearRemovedTags (ws: ${workspaceId})`);
  await clearRemovedTags(inputData);

  syncHandler.log(`sync => clearRemovedData => clearRemovedNotes (ws: ${workspaceId})`);
  await clearRemovedNotes(inputData);

  syncHandler.log(`sync => clearRemovedData => clearRemovedFolders (ws: ${workspaceId})`);
  await clearRemovedFolders(inputData);

  callback(null, true);
}
