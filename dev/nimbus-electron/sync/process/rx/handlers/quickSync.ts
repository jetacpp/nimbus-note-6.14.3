import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as fastUpload} from "./fastUpload";
import {default as fullUpload} from "./fullUpload";
import {default as errorHandler} from "../../../../utilities/errorHandler";

/**
 * @param { workspaceId:string, service:NimbusSyncService } inputData
 * @param {Function} callback
 */
export default function quickSync(inputData, callback = (err, res) => {
}) {
  // @ts-ignore
  return new Promise(async () => {
    const {workspaceId} = inputData;

    /**
     * @param { workspaceId:string, service:NimbusSyncService } inputData
     */
    let fastUserUpload = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        fastUpload(inputData, (err) => {
          if (err) {
            errorHandler.log({
              err: err, response: null,
              description: "Sync problem => quickSync => fastUserUpload"
            });
          }
          resolve(err);
        });
      });
    };

    /**
     * @param { workspaceId:string, service:NimbusSyncService } inputData
     */
    let fullUserUpload = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        fullUpload(inputData, (err) => {
          if (err) {
            errorHandler.log({
              err: err, response: null,
              description: "Sync problem => quickSync => fullUserUpload",
              data: inputData
            });
          }
          resolve(err);
        });
      });
    };

    if (config.SHOW_WEB_CONSOLE) {
      console.log(`Start quickSync sync (ws: ${workspaceId})`);
    }

    SyncStatusChangedEvent.setStatus({workspaceId, newStatus: SyncStatusChangedEvent.STATUS.HEADER_START});

    let err;
    if (err = await fastUserUpload(inputData)) {
      return callback(err, false);
    }

    if (err = await fullUserUpload(inputData)) {
      return callback(err, false);
    }

    callback(err, true);
  });
}
