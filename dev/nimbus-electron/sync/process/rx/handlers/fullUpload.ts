import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as uploadUpdatedFullNotesOnServer} from "./uploadUpdatedFullNotesOnServer";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param { workspaceId:string, service:NimbusSyncService } inputData
 * @param {Function} callback
 */
export default function fullUpload(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;
  syncHandler.log(`sync => fullUserUpload => start (ws: ${workspaceId})`);

  uploadUpdatedFullNotesOnServer(inputData, callback);
}
