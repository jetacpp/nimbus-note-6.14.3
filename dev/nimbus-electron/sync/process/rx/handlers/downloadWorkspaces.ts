import chalk from "chalk";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as WorkspaceObjRepository} from "../../repositories/WorkspaceObjRepository";
import {default as WorkspaceObj} from "../../db/WorkspaceObj";
import {default as workspace, default as workspaceModel} from "../../../../db/models/workspace";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as auth} from "../../../../auth/auth";
import {default as OrgObjRepository} from "../../repositories/OrgObjRepository";
import {default as orgs} from "../../../../db/models/orgs";
import SelectedOrganization from "../../../../organization/SelectedOrganization";
import {default as appPage} from "../../../../request/page";
import SelectedWorkspace from "../../../../workspace/SelectedWorkspace";

/**
 * @param {{workspaceId:string, service:NimbusSyncService, skipSyncHandlers:boolean}} inputData
 * @param {Function} callback
 */
// @ts-ignore
export default async function downloadWorkspaces(inputData, callback = (err, res) => {
}) {
  const {workspaceId, skipSyncHandlers} = inputData;

  NimbusSDK.getApi().workspacesGet({workspaceId, skipSyncHandlers}, async (err, workspaces) => {
    if (err || !workspaces) {
      if (!skipSyncHandlers) {
        await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
      }

      errorHandler.log({
        err: err, response: workspaces,
        description: "Sync problem => downloadWorkspaces => getWorkspaces"
      });

      return callback(err, false);
    }

    /**
     * @param {{workspace:WorkspaceObj}} inputData
     * @return {Promise<Object|null>}
     */
    let checkUserWorkspaceItem = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        const {workspace, skipSyncHandlers} = inputData;
        const {globalId} = workspace;
        WorkspaceObjRepository.get({globalId}, async (err, workspaceObj) => {
          if (err) {
            errorHandler.log({
              err: err, response: workspaceObj,
              description: "Sync problem => downloadWorkspaces => checkUserWorkspaceItem",
              data: inputData
            });
            return resolve(null);
          }

          /**
           * @param {{workspace:WorkspaceObj}} inputData
           */
          let createUserWorkspaceObj = async (inputData) => {
            // @ts-ignore
            return new Promise((resolve) => {
              const {workspace} = inputData;
              WorkspaceObjRepository.create({workspace}, (err, workspaceObj) => {
                if (err) {
                  errorHandler.log({
                    err: err, response: workspaceObj,
                    description: "Sync problem => downloadWorkspaces => createUserWorkspaceObj",
                    data: inputData
                  });
                  return resolve(null);
                }

                resolve(workspaceObj);
              });
            });
          };

          if (!workspaceObj) {
            // TODO: check if modify date > then last update date and return null if no update
            workspaceObj = await createUserWorkspaceObj({workspace});
          }

          if (!workspaceObj) {
            return resolve(null);
          }

          workspaceObj.title = workspace.title;
          workspaceObj.userId = workspace.userId;
          workspaceObj.org = workspace.org;
          workspaceObj.isDefault = workspace.isDefault;
          workspaceObj.countMembers = workspace.countMembers;
          workspaceObj.countInvites = workspace.countInvites;
          workspaceObj.defaultEncryptionKeyId = workspace.defaultEncryptionKeyId;
          workspaceObj.access = workspace.access;
          workspaceObj.updatedAt = workspace.updatedAt;
          workspaceObj.lastUpdateTime = workspace.updatedAt;
          workspaceObj.syncDate = workspace.updatedAt;
          workspaceObj.needSync = false;
          workspaceObj.existOnServer = true;
          workspaceObj.isNotesLimited = workspace.isNotesLimited;
          workspaceObj.user = workspace.user;
          workspaceObj.notesEmail = workspace.notesEmail;
          workspaceObj.avatar = workspace.avatar || null;
          workspaceObj.color = workspace.color || null;

          if (!skipSyncHandlers) {
            //if (workspace.countMembers) {
              const members = await updateWorkspaceMembers({
                workspaceId,
                globalId: workspace.globalId,
                skipSyncHandlers
              });

              if (members) {
                workspaceObj.members = members;
              }
            //}

            if (!workspaceObj.members || !workspace.countMembers) {
              workspaceObj.members = [];
            }

            if (workspace.countInvites) {
              const invites = await updateWorkspaceInvites({
                workspaceId,
                globalId: workspace.globalId,
                skipSyncHandlers
              });

              if (invites) {
                workspaceObj.invites = invites;
              }
            }

            if (!workspaceObj.invites || !workspace.countInvites) {
              workspaceObj.invites = [];
            }
          }

          resolve(workspaceObj);
        });
      });
    };

    /**
     * @param {{workspaces: [WorkspaceObj], skipSyncHandlers:boolean}} inputData
     * @return {Promise<Array>}
     */
    let checkUserRemovedWorkspaces = (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaces, skipSyncHandlers} = inputData;
        // @ts-ignore
        workspaceModel.findAll({}, {}, async (err, localWorkspaces) => {
          if (err || !localWorkspaces) {
            localWorkspaces = [];
          }

          const workspacesIdList = workspaces.map((workspace) => {
            return workspace.globalId;
          });

          /**
           * @param {WorkspaceObj} workspace
           */
          const removeUserWorkspace = (workspace) => {
            // @ts-ignore
            return new Promise(async (resolve) => {
              const count = await WorkspaceObjRepository.remove({workspaceObjs: workspace});
              if(count) { await checkRemovedWorkspace(workspace); }
              return resolve(count);
            });
          };

          const checkRemovedWorkspace = async (workspaceItem) => {
            const org = <{type}>await orgs.getByWorkspaceId(workspaceItem.globalId);
            let isBusinessOrg = org && org.type === orgs.TYPE_BUSINESS;
            if(!org) { isBusinessOrg = workspaceItem.orgId && workspaceItem.orgId.charAt(0) === 'b'; }

            const selectedWorkspaceId = await SelectedWorkspace.getGlobalId();
            const countWorkspaces = await workspace.countUserWorkspacesByOrgId(workspaceItem.orgId);
            if(isBusinessOrg) {
              if(countWorkspaces) {
                await SelectedOrganization.set(workspaceItem.orgId);
              } else {
                await SelectedOrganization.set(null);
              }

              if(!countWorkspaces || (selectedWorkspaceId === workspaceItem.globalId)) {
                await appPage.reload(false);
              }
            } else {
              if(selectedWorkspaceId === workspaceItem.globalId) {
                await SelectedWorkspace.set(null);
                await appPage.reload(false);
              }
            }
          };

          for (let localWorkspace of localWorkspaces) {
            if (workspacesIdList.indexOf(localWorkspace.globalId) < 0) {
              await removeUserWorkspace(localWorkspace);
            }
          }

          return resolve();
        });
      });
    };

    /**
     * @param {{workspace:WorkspaceObj}} inputData
     * @return {Promise<Object|null>}
     */
    let updateWorkspaceInvites = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, globalId, skipSyncHandlers} = inputData;
        NimbusSDK.getApi().workspaceInvitesGet({
          workspaceId,
          globalId,
          skipSyncHandlers
        }, async (err, workspaceInvites) => {
          if (err) {
            return resolve(null);
          }
          return resolve(workspaceInvites.invites);
        });
      });
    };

    /**
     * @param {{workspaceId:string, skipSyncHandlers:boolean}} inputData
     * @return {Promise<Object|null>}
     */
    let updateWorkspaceMembers = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaceId, globalId, skipSyncHandlers} = inputData;
        NimbusSDK.getApi().workspaceMembersGet({
          workspaceId,
          globalId,
          skipSyncHandlers
        }, async (err, workspaceMembers) => {
          if (err || !workspaceMembers) {
            return resolve(null);
          }
          return resolve(workspaceMembers.members);
        });
      });
    };

    /**
     * @param {{workspaces: [WorkspaceObj], skipSyncHandlers:boolean}} inputData
     * @return {Promise<Array>}
     */
    let checkUserWorkspaceItems = async (inputData) => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const {workspaces, skipSyncHandlers} = inputData;
        for (let workspace of workspaces) {
          let workspaceObj = <{globalId:string}>await checkUserWorkspaceItem({workspace, skipSyncHandlers});
          if (workspaceObj) {
            await WorkspaceObjRepository.update({workspaceObjs: workspaceObj});
            if (!skipSyncHandlers) {
              WorkspaceObjRepository.sendSocketEventOnUpdateInvites(workspaceObj.globalId);
              WorkspaceObjRepository.sendSocketEventOnUpdateMembers(workspaceObj.globalId);
            }
          }
        }

        if (!skipSyncHandlers) {
          WorkspaceObjRepository.sendSocketEventOnWorkspacesAccessChange();
        }

        resolve(true);
      });
    };

    let saveUserDefaultOrg = async (inputData) => {
      return new Promise(async (resolve) => {
        const {workspaces} = inputData;
        const userInfo = <{email}>await auth.getUserAsync();
        if(!userInfo) {
          return resolve(false);
        }

        let findOrg = null;
        for (let workspace of workspaces) {
          const {org} = workspace;
          if(!org) { continue; }
          const {user} = org;
          if(!user) { continue; }

          if(user.email === userInfo.email) {
            findOrg = org;
            break;
          }
        }

        if(!findOrg) {
          return resolve(false);
        }

        const orgSaved = await OrgObjRepository.update({orgObjs: findOrg});
        return resolve(orgSaved);
      });
    };

    if (workspaces && workspaces.length) {
      await saveUserDefaultOrg({workspaces});
      await checkUserRemovedWorkspaces({workspaces, skipSyncHandlers});
      await checkUserWorkspaceItems({workspaces, skipSyncHandlers});
    }

    callback(err, true);
  });
}
