import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as FolderObjRepository} from "../../repositories/FolderObjRepository";
import {default as FolderObj} from "../../db/FolderObj";
import {default as errorHandler} from "../../../../utilities/errorHandler";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default async function downloadFolders(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  NimbusSDK.getAccountManager().getUniqueUserName(async (err, uniqueUserName) => {
    if (err) {
      await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
      errorHandler.log({
        err: err, response: uniqueUserName,
        description: "Sync problem => downloadFolders => getUniqueUserName"
      });
      return callback(err, false);
    }

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    NimbusSDK.getApi().getFolders({workspaceId}, async (err, folders) => {
      if (err) {
        await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
        errorHandler.log({
          err: err, response: folders,
          description: "Sync problem => downloadFolders => getFolders"
        });
        return callback(err, false);
      }

      /**
       * @param {{workspaceId:string, uniqueUserName:string, folder:FolderObj}} inputData
       * @return {Promise<Object|null>}
       */
      let checkUserFolderItem = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          const {workspaceId, uniqueUserName, folder} = inputData;
          FolderObjRepository.get({workspaceId, globalId: folder.global_id}, async (err, folderObj) => {
            if (err) {
              errorHandler.log({
                err: err, response: folderObj,
                description: "Sync problem => downloadFolders => checkUserFolderItem",
                data: {
                  uniqueUserName: uniqueUserName,
                  folder: folder
                }
              });
              return resolve(null);
            }

            /**
             * @param {{workspaceId:string, folder:FolderObj}} inputData
             */
            let createUserFolderObj = async (inputData) => {
              // @ts-ignore
              return new Promise((resolve) => {
                const {workspaceId, folder} = inputData;
                FolderObjRepository.create({
                  workspaceId,
                  folderName: folder.title,
                  parentId: folder.parent_id,
                  globalId: folder.global_id
                }, (err, folderObj) => {
                  if (err) {
                    errorHandler.log({
                      err: err, response: folderObj,
                      description: "Sync problem => downloadFolders => createUserFolderObj",
                      data: inputData
                    });
                    return resolve(null);
                  }

                  resolve(folderObj);
                });
              });
            };

            if (folderObj) {
              if (folderObj.syncDate > folder.date_updated) {
                return resolve(null);
              } else {
                folderObj = await createUserFolderObj({workspaceId, folder});
              }
            } else {
              folderObj = await createUserFolderObj({workspaceId, folder});
            }

            if (!folderObj) {
              return resolve(null);
            }

            /**
             * @param {{workspaceId:string, globalId:string}} inputData
             * @return {Promise}
             */
            let getUserParentFolderObj = async (inputData) => {
              // @ts-ignore
              return new Promise((resolve) => {
                FolderObjRepository.get(inputData, async (err, folderObj) => {
                  if (err) {
                    errorHandler.log({
                      err: err, response: folderObj,
                      description: "Sync problem => downloadFolders => getUserParentFolderObj",
                      data: inputData
                    });
                    return resolve(null);
                  }

                  resolve(folderObj);
                });
              });
            };

            let parentFolder = <{rootId:string}>await getUserParentFolderObj({workspaceId, globalId: folder.parent_id});
            folderObj = FolderObj.setTitle(folderObj, folder.title);
            folderObj.parentId = folder.parent_id;
            folderObj.rootParentId = folder.root_parent_id;

            if (folderObj.rootParentId && folderObj.parentId !== folderObj.rootParentId) {
              folderObj.rootId = "trash";
              folderObj.rootParentId = "trash";
            }

            if (parentFolder && parentFolder.rootId === "trash") {
              folderObj.rootId = "trash";
              folderObj.rootParentId = "trash";
            }

            folderObj.index = folder.index;
            folderObj.type = folder.type;
            folderObj.existOnServer = true;
            folderObj.dateAdded = folder.date_added_user;
            folderObj.dateUpdated = folder.date_updated_user;
            folderObj.syncDate = folder.date_updated;
            folderObj.uniqueUserName = uniqueUserName;
            folderObj.onlyOffline = false;
            folderObj.needSync = false;
            folderObj.shared = !!folder.shared;
            folderObj.color = folder.color;

            resolve(folderObj);
          });
        });
      };

      /**
       * @param {{workspaceId:string, uniqueUserName:string, folders:[FolderObj]}} inputData
       * @return {Promise<Array>}
       */
      let checkUserFolderItems = async (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {workspaceId, uniqueUserName, folders} = inputData;
          for (let folder of folders) {
            let folderObj = await checkUserFolderItem({workspaceId, uniqueUserName, folder});
            if (folderObj) {
              await FolderObjRepository.update({workspaceId, folderObjs: folderObj});
            }
          }
          resolve(true);
        });
      };

      if (folders && folders.length) {
        await checkUserFolderItems({workspaceId, uniqueUserName, folders});
      }

      callback(err, true);
    });

  });
}
