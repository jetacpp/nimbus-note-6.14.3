import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as OrgObj} from "../../db/OrgObj";
import {default as orgsModel} from "../../../../db/models/orgs";
import {default as OrgObjRepository} from "../../repositories/OrgObjRepository";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param {{workspaceId:string, service:NimbusSyncService, skipSyncHandlers:boolean}} inputData
 * @param {Function} callback
 */
// @ts-ignore
export default async function downloadOrganizations(inputData, callback = (err, res) => {
}) {
    const {workspaceId, skipSyncHandlers} = inputData;

    syncHandler.log(`=> downloadOrganizations start for workspaceId: ${workspaceId}`)

    syncHandler.log(`=> downloadOrganizations => organizationsGet`);
    NimbusSDK.getApi().organizationsGet({workspaceId, skipSyncHandlers}, async (err, organizations) => {
        syncHandler.log(`=> downloadOrganizations => organizationsGet => err: ${err}`);
        if (err) {
            if (!skipSyncHandlers) {
                await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            }

            errorHandler.log({
                err: err, response: organizations,
                description: "Sync problem => downloadOrganizations => getOrganizations"
            });

            return callback(err, false);
        }

        /**
         * @param {{organization:OrgObj}} inputData
         * @return {Promise<Object|null>}
         */
        let checkUserOrganizationItem = async (inputData) => {
            // @ts-ignore
            return new Promise((resolve) => {
                const {organization, skipSyncHandlers} = inputData;
                const {id} = organization;
                OrgObjRepository.get({id}, async (err, organizationObj) => {
                    if (err) {
                        errorHandler.log({
                            err: err, response: organizationObj,
                            description: "Sync problem => downloadOrganizations => checkUserOrganizationItem",
                            data: inputData
                        });
                        return resolve(null);
                    }

                    /**
                     * @param {{organization:OrgObj}} inputData
                     */
                    let createUserOrganizationObj = async (inputData) => {
                        // @ts-ignore
                        return new Promise((resolve) => {
                            const {organization} = inputData;
                            OrgObjRepository.create({organization}, (err, organizationObj) => {
                                if (err) {
                                    errorHandler.log({
                                        err: err, response: organizationObj,
                                        description: "Sync problem => downloadOrganizations => createUserOrganizationObj",
                                        data: inputData
                                    });
                                    return resolve(null);
                                }

                                resolve(organizationObj);
                            });
                        });
                    };

                    if (!organizationObj) {
                        // TODO: check if modify date > then last update date and return null if no update
                        organizationObj = await createUserOrganizationObj({organization});
                    }

                    if (!organizationObj) {
                        return resolve(null);
                    }

                    organizationObj.id = organization.id;
                    organizationObj.type = organization.type;
                    organizationObj.serviceType = organization.serviceType;
                    organizationObj.title = organization.title;
                    organizationObj.description = organization.description;
                    organizationObj.usage = organization.usage;
                    organizationObj.limits = organization.limits;
                    organizationObj.features = organization.features;
                    organizationObj.user = organization.user;
                    organizationObj.sub = organization.sub;
                    organizationObj.suspended = organization.suspended;
                    organizationObj.suspendedAt = organization.suspendedAt;
                    organizationObj.suspendedReason = organization.suspendedReason;
                    organizationObj.access = organization.access;
                    organizationObj.smallLogoUrl = organization.smallLogoUrl || null;
                    organizationObj.bigLogoUrl = organization.bigLogoUrl || null;

                    resolve(organizationObj);
                });
            });
        };

        /**
         * @param {{organizations: [OrgObj], skipSyncHandlers:boolean}} inputData
         * @return {Promise<Array>}
         */
        let checkUserRemovedOrganizations = (inputData) => {
            // @ts-ignore
            return new Promise(async (resolve) => {
                const {organizations, skipSyncHandlers} = inputData;
                // @ts-ignore
                orgsModel.findAll({}, {}, async (err, localOrganizations) => {
                    if (err || !localOrganizations) {
                        localOrganizations = [];
                    }

                    const organizationsIdList = organizations.map((organization) => {
                        return organization.id;
                    });

                    /**
                     * @param {OrgObj} organization
                     */
                    const removeUserOrganization = (organization) => {
                        // @ts-ignore
                        return new Promise(async (resolve) => {
                            const count = await OrgObjRepository.remove({organizationObjs: organization});
                            return resolve(count);
                        });
                    };

                    for (let localOrganization of localOrganizations) {
                        if(localOrganization.type !== orgsModel.TYPE_BUSINESS) { continue; }
                        if (organizationsIdList.indexOf(localOrganization.id) < 0) {
                            await removeUserOrganization(localOrganization);
                        }
                    }

                    return resolve();
                });
            });
        };

        /**
         * @param {{organizations: [OrgObj], skipSyncHandlers:boolean}} inputData
         * @return {Promise<Array>}
         */
        let checkUserOrganizationsItems = async (inputData) => {
            // @ts-ignore
            return new Promise(async (resolve) => {
                const {organizations, skipSyncHandlers} = inputData;
                for (let organization of organizations) {
                    let organizationObj = <{id:string}>await checkUserOrganizationItem({organization, skipSyncHandlers});
                    if (organizationObj) {
                        await OrgObjRepository.update({orgObjs: organizationObj});
                    }
                }
                resolve(true);
            });
        };

        if (organizations) {
            await checkUserRemovedOrganizations({organizations, skipSyncHandlers});
            await checkUserOrganizationsItems({organizations, skipSyncHandlers});
        }

        callback(err, true);
    });
}
