import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as SyncStatusDisplayEvent} from "../../events/SyncStatusDisplayEvent";
import {default as NoteIsMoreThanLimitException} from "../../exceptions/NoteIsMoreThanLimitException";
import {default as AttachmentObjRepository} from "../../repositories/AttachmentObjRepository";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as PouchDb} from "../../../../../pdb";
import syncHandler from "../../../../utilities/syncHandler";
import fs from "fs-extra";

/**
 * @param { workspaceId:string, service:NimbusSyncService, parentId:string } inputData
 * @param {Function} callback
 */
export default async function uploadUpdatedAttachmentsOnServer(inputData, callback = (err, res) => {
}) {
  const {workspaceId, noteGlobalId} = inputData;

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @return {Promise<[]>}
   */
  let getNoteAttachmentForUploadOnServer = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      AttachmentObjRepository.getNoteAttachmentForUploadOnServer(inputData, (err, attachments) => {
        if (err) {
          errorHandler.log({
            err: err, response: attachments,
            description: "Sync problem => uploadUpdatedAttachmentsOnServer => getNoteAttachmentForUploadOnServer",
            data: inputData
          });
          return resolve([]);
        }
        resolve(attachments);
      });
    });
  };

  /**
   * @param {{workspaceId:string, attachmentObj:AttachmentObj}} inputData
   * @return {Promise<Object>}
   */
  let processUploadResponse = async (inputData) => {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {workspaceId, attachmentObj} = inputData;

      /**
       * @param {{workspaceId:string, globalId:string}} inputData
       * @return {Promise<boolean>}
       */
      let checkIfAttachmentNotMoreThanLimitAttachmentSize = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          AttachmentObjRepository.checkIfAttachmentNotMoreThanLimitAttachmentSize(inputData, (err, attachmentNotMoreThanLimit) => {
            if (err) {
              errorHandler.log({
                err: err, response: attachmentNotMoreThanLimit,
                description: "Sync problem => uploadUpdatedAttachmentsOnServer => checkIfAttachmentNotMoreThanLimitAttachmentSize",
                data: inputData
              });
              return resolve(false);
            }
            resolve(attachmentNotMoreThanLimit);
          });
        });
      };

      /**
       * @param {{workspaceId:string, tempname:string, extension:string, mime:string}} inputData
       * @return {Promise<boolean>}
       */
      let filesPreupload = async (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {workspaceId} = inputData;
          if (await SyncStatusChangedEvent.needPreventSync(inputData)) return resolve(false);
          NimbusSDK.getApi().filesPreupload(inputData, async (err, tempName) => {
            if (err || !tempName) {
              // await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
              syncHandler.log({
                err: err, response: tempName,
                description: "Sync problem => uploadUpdatedAttachmentsOnServer => filesPreupload",
                data: inputData
              });
              return resolve(null);
            }
            resolve(tempName);
          });
        });
      };

      /**
       * @param {{workspaceId:string, globalId:string, tempName:string}} inputData
       * @return {Promise<boolean>}
       */
      let updateTempNameAfterUploadOnServerI = async (inputData) => {
        // @ts-ignore
        return new Promise((resolve) => {
          AttachmentObjRepository.updateTempNameAfterUploadOnServerI(inputData, (err) => {
            if (err) {
              errorHandler.log({
                err: err, response: null,
                description: "Sync problem => uploadUpdatedAttachmentsOnServer => updateTempNameAfterUploadOnServerI",
                data: inputData
              });
              return resolve(null);
            }
            resolve(true);
          });
        });
      };

      let tempname = `${PouchDb.getClientAttachmentPath()}/${attachmentObj.storedFileUUID}`;
      let extension = attachmentObj.extension;
      let mime = attachmentObj.mime || '';
      // @ts-ignore
      let fileExist = await fs.exists(tempname);
      // @ts-ignore
      if (!fileExist) {
        await AttachmentObjRepository.eraseAttachmentFile({
          workspaceId: inputData.workspaceId,
          globalId: attachmentObj.globalId,
        });
        return resolve(null);
      }

      let attachmentNotMoreThanLimit = await checkIfAttachmentNotMoreThanLimitAttachmentSize({
        workspaceId,
        globalId: attachmentObj.globalId
      });
      if (!attachmentNotMoreThanLimit) {
        if (config.SHOW_WEB_CONSOLE) {
          console.log(`NoteIsMoreThanLimitException: ${NoteIsMoreThanLimitException.TYPE.ATTACHMENT} ${attachmentObj.globalId} ${attachmentObj.parentId} (ws: ${workspaceId})`);
        }
        return resolve(null);
      }

      if (attachmentObj.tempName) {
        if (config.SHOW_WEB_CONSOLE) {
          console.log(`Uploading attach already presented on sync server: ${attachmentObj.globalId} ${attachmentObj.tempName} (ws: ${workspaceId})`);
        }
      } else {
        const uploadedName = await filesPreupload({workspaceId, tempname, extension, mime});
        if (!uploadedName) {
          return resolve(null);
        }

        attachmentObj.tempName = uploadedName;

        let updateResult = await updateTempNameAfterUploadOnServerI({
          workspaceId,
          globalId: attachmentObj.globalId,
          tempName: attachmentObj.tempName
        });

        if (!updateResult) {
          return resolve(null);
        }
      }

      AttachmentObjRepository.convertToSyncAttachmentEntity(attachmentObj, (err, attachmentEntity) => {
        if (err || !attachmentEntity) {
          return resolve(null);
        }
        resolve(attachmentEntity);
      });
    });
  };

  let attachments: any = await getNoteAttachmentForUploadOnServer({workspaceId, noteGlobalId});
  if (attachments.length) {
    if (config.SHOW_WEB_CONSOLE) {
      console.log(`Start upload updated attaches on server for note: ${noteGlobalId} (ws: ${workspaceId})`);
      console.log(`Upload attaches count: ${attachments.length} (ws: ${workspaceId})`);
    }
  }

  let uploadedAttachmentsList = [];
  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  if (attachments.length) {
    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.UPLOAD_ATTACHMENTS
    });
  }

  for (let attachmentObj of attachments) {
    if (attachmentObj.location) {
      if (config.SHOW_WEB_CONSOLE) {
        console.log(`No need to upload attach, just renamed: ${attachmentObj.globalId} for note ${noteGlobalId} (ws: ${workspaceId})`);
      }
      continue;
    }

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) { break }
    let attachmentEntity = await processUploadResponse({workspaceId, attachmentObj});
    if (attachmentEntity) {
      uploadedAttachmentsList.push(attachmentEntity);
    } else {
      if (config.SHOW_WEB_CONSOLE) {
        syncHandler.log(`Attach upload fail: ${attachmentObj.globalId} for note ${noteGlobalId} (ws: ${workspaceId})`);
      }
      /*errorHandler.log({
        err: null, response: null,
        description: "Sync problem => uploadUpdatedAttachmentsOnServer",
        data: {
          attachmentObj,
          attachmentEntity,
          noteGlobalId
        }
      });*/
    }
  }

  callback(null, uploadedAttachmentsList);
}
