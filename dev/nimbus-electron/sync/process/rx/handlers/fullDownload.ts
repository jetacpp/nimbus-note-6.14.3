import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as headerSync} from "./headerSync";
import {default as socketStoreType} from "../../../socket/socketStoreType";
import {default as PreviewSyncManager} from "../../../nimbussdk/manager/PreviewSyncManager";
import {default as LimitsPopup} from "../../../ipc/LimitsPopup";
import {default as user} from "../../../../db/models/user";
import {default as NimbusSDK} from "../../../nimbussdk/net/NimbusSDK";
import {default as NoteObjRepository} from "../../repositories/NoteObjRepository";
import {default as fullAttachmentsInListDownloadForDownloadedNotes} from "./fullAttachmentsInListDownloadForDownloadedNotes";
import {default as sendSocketStoreMessages} from "./sendSocketStoreMessages";
import {default as reloadCurrentNoteContent} from "./reloadCurrentNoteContent";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as downloadOrganizations} from "./downloadOrganizations";
import {default as downloadWorkspaces} from "./downloadWorkspaces";
import {default as getTrial} from "./getTrial";
import {default as userInfo} from "./userInfo";
import {default as userVariables} from "./userVariables";
import {default as SyncStatusDisplayEvent} from "../../events/SyncStatusDisplayEvent";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param {{workspaceId: string, service:NimbusSyncService, manualSync:boolean}} inputData
 * @param {Function} callback
 */
export default async function fullDownload(inputData, callback = (err, res) => {
}) {
  const {workspaceId, manualSync} = inputData;

  syncHandler.log(`sync => fullDownload => start (ws: ${workspaceId})`);

  PreviewSyncManager.clear(inputData);

  /**
   * @param {{workspaceId:string, service:NimbusSyncService}} inputData
   */
  let downloadUserInfo = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      userInfo({...inputData, saveUserInfo: true}, (err, responseUserInfo) => {
        if (err) {
          errorHandler.log({
            err: err, response: responseUserInfo,
            description: "Sync problem => fullDownload => downloadUserInfo",
            data: inputData
          });
          return resolve(null);
        }
        resolve(responseUserInfo);
      });
    });
  };

  /**
   * @param {{workspaceId:string, service:NimbusSyncService}} inputData
   */
  let downloadUserVariables = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      userVariables({...inputData, saveUserVariables: true}, (err, responseUserVariables) => {
        if (err) {
          errorHandler.log({
            err: err, response: responseUserVariables,
            description: "Sync problem => fullDownload => downloadUserVariables",
            data: inputData
          });
          return resolve(null);
        }
        resolve(responseUserVariables);
      });
    });
  };

  /**
   * @param {{workspaceId:string, service:NimbusSyncService, userInstance:{}}} inputData
   */
  let downloadUserTrial = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      getTrial({...inputData, saveUserTrial: true, skipSyncHandlers: true}, (err, responseUserTrial) => {
        if (err) {
          errorHandler.log({
            err: err, response: responseUserTrial,
            description: "Sync problem => fullDownload => getUserTrial",
            data: inputData
          });
          return resolve(null);
        }
        resolve(responseUserTrial);
      });
    });
  };

  /**
   * @param {{workspaceId:string, service:NimbusSyncService}} inputData
   */
  let downloadUserOrganizations = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      downloadOrganizations(inputData, (err, responseOrganizations) => {
        if (err) {
          errorHandler.log({
            err: err, response: responseOrganizations,
            description: "Sync problem => fullDownload => downloadUserOrganizations",
            data: inputData
          });
          return resolve(null);
        }
        resolve(responseOrganizations);
      });
    });
  };

  /**
   * @param {{workspaceId:string, service:NimbusSyncService}} inputData
   */
  let downloadUserWorkspaces = async (inputData) => {
    // @ts-ignore
    return new Promise((resolve) => {
      downloadWorkspaces(inputData, (err, responseWorkspaces) => {
        if (err) {
          errorHandler.log({
            err: err, response: responseWorkspaces,
            description: "Sync problem => fullDownload => downloadUserWorkspaces",
            data: inputData
          });
          return resolve(null);
        }
        resolve(responseWorkspaces);
      });
    });
  };


  syncHandler.log(`sync => fastDownload => start (ws: ${workspaceId})`);

  await SyncStatusDisplayEvent.set({
    workspaceId: workspaceId,
    newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOADING_META_INFO
  });

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  syncHandler.log(`sync => fastDownload => downloadUserVariables (ws: ${workspaceId})`);
  const userVariablesList = await downloadUserVariables(inputData);

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  syncHandler.log(`sync => fastDownload => downloadUserInfo (ws: ${workspaceId})`);
  const userInstance = await downloadUserInfo(inputData);

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  syncHandler.log(`sync => fastDownload => downloadUserTrial (ws: ${workspaceId})`);
  await downloadUserTrial({...inputData, userInstance});

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  syncHandler.log(`sync => fastDownload => downloadUserOrganizations (ws: ${workspaceId})`);
  await downloadUserOrganizations(inputData);

  if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
  syncHandler.log(`sync => fastDownload => downloadUserWorkspaces (ws: ${workspaceId})`);
  await downloadUserWorkspaces(inputData);

  syncHandler.log(`sync => fastDownload => getTotalTrafficAfterSync (ws: ${workspaceId})`);
  let {orgTraffic, totalTrafficAfterSync} = <{orgTraffic:any, totalTrafficAfterSync:number}>await NoteObjRepository.getTotalTrafficAfterSync(inputData);
  if (totalTrafficAfterSync && totalTrafficAfterSync >= user.WARNING_UPLOAD_SIZE) {
    let accountManager = NimbusSDK.getAccountManager();

    let isPremiumActive = await accountManager.isPremiumActiveAsync();
    let trafficMax = orgTraffic ? orgTraffic.max : await accountManager.getTrafficMaxAsync();

    LimitsPopup.check({
      workspaceId,
      manualSync,
      data: {
        objectSize: totalTrafficAfterSync,
        limitSize: trafficMax,
        isPremiumActive: isPremiumActive
      }
    });

    if (totalTrafficAfterSync >= trafficMax) {
      await SyncStatusDisplayEvent.set({
        workspaceId: workspaceId,
        newStatus: SyncStatusDisplayEvent.STATUS.TRAFFIC_LIMIT
      });

      errorHandler.log({
        err: null, response: null,
        description: "Sync problem => fullDownload => (totalTrafficAfterSync >= trafficMax)",
        data: {
          totalTrafficAfterSync,
          trafficMax
        }
      });

      return callback(null, false);
    }
  }

  await headerSync(inputData, async () => {

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    fullAttachmentsInListDownloadForDownloadedNotes(inputData, async (err) => {
      if (err) {
        errorHandler.log({
          err: err, response: null,
          description: "Sync problem => fullDownload => headerSync"
        });
      }

      if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
      syncHandler.log(`sync => reloadCurrentNoteContent (ws: ${workspaceId})`);
      await reloadCurrentNoteContent({workspaceId, storeType: socketStoreType.NOTES_ATTACHMENTS_FOR_UPDATE});

      if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
      syncHandler.log(`sync => headerSync => sendSocketStoreMessages (ws: ${workspaceId})`);
      sendSocketStoreMessages(inputData);
      PreviewSyncManager.clear(inputData);
      callback(err, true);
    });
  });
}
