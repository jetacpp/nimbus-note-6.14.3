import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as SyncStatusDisplayEvent} from "../../events/SyncStatusDisplayEvent";
import {default as getDownloadNotesTotalAmount} from "./getDownloadNotesTotalAmount";
import {default as downloadStructureNotesPerStep} from "./downloadStructureNotesPerStep";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import {default as AccountManager} from "../../../nimbussdk/manager/AccountManager";

/**
 * @param {{workspaceId:string, service:NimbusSyncService}} inputData
 * @param {Function} callback
 */
export default function downloadAllStructureNotes(inputData, callback = (err, res) => {
}) {
  const {workspaceId} = inputData;

  getDownloadNotesTotalAmount(inputData, async (err, data) => {
    if (err || !data) {
      errorHandler.log({
        err: err, response: data,
        description: "Sync problem => downloadAllStructureNotes => getDownloadNotesTotalAmount"
      });
      return callback(err, null);
    }

    const totalIterations = data.totalIterations;
    const totalNotes = data.totalNotes;

    /**
     * @param {{workspaceId:string, iterationCount:int, downloadNoteCountPerStep:int, totalNotes:int}} inputData
     */
    let downloadUserNotesIteration = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        const {workspaceId, iterationCount, downloadNoteCountPerStep, totalNotes} = inputData;
        const start = iterationCount * downloadNoteCountPerStep;
        const amount = downloadNoteCountPerStep;
        downloadStructureNotesPerStep({workspaceId, start, amount, totalNotes}, (err, lastUpdateTime) => {
          if (err || !lastUpdateTime) {
            return resolve(false);
          }

          setTimeout(() => {
            resolve(lastUpdateTime);
          }, 250);
        });
      });
    };

    if (totalIterations) {
      await SyncStatusDisplayEvent.set({
        workspaceId: workspaceId,
        newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOAD_NOTES
      });
    }

    let lastUpdateTime = null;
    for (let iterationCount = 0; iterationCount < totalIterations; iterationCount++) {
      if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(err, false);
      let lastUpdateTimePerIteration = await downloadUserNotesIteration({
        workspaceId,
        iterationCount,
        downloadNoteCountPerStep: config.DOWNLOAD_NOTE_COUNT_PER_STEP,
        totalNotes
      });
      if (lastUpdateTimePerIteration) {
        lastUpdateTime = lastUpdateTimePerIteration;
      }
    }

    if (!lastUpdateTime) {
      return callback(err, null);
    }

    AccountManager.setLastUpdateTime({workspaceId, lastUpdateTime}, (err, response) => {
      if (err) {
        errorHandler.log({
          err: err, response: response,
          description: "Sync problem => downloadAllStructureNotes => setLastUpdateTime",
          data: {
            lastUpdateTime: lastUpdateTime
          }
        });
      }
      callback(err, response);
    });
  });
}
