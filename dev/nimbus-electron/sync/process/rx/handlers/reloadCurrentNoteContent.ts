import chalk from "chalk";
import {default as SocketCollection} from "../../../socket/store/SocketCollection";
import {default as socketConnection} from "../../../../sync/socket/socketFunctions";
import {default as selectedItem} from "../../../../window/selectedItem";
import {default as appWindow} from "../../../../window/instance";
import {default as item} from "../../../../db/models/item";

/**
 * @param {{workspaceId:string, storeType:string}} inputData
 */
export default function reloadCurrentNoteContent(inputData) {
  // @ts-ignore
  return new Promise(async (resolve) => {
    const {workspaceId, storeType} = inputData;
    if (!SocketCollection.needSendSocketCollection(inputData)) {
      SocketCollection.clearAttachmentsCopy(inputData);
      return resolve(false);
    }

    const selectedNoteGlobalId = selectedItem.get({
      workspaceId,
      itemType: selectedItem.TYPE_NOTE
    });

    const selectedNote = <any>await item.getByGlobalId({
      'globalId': selectedNoteGlobalId,
      workspaceId
    });

    const downloadAttachments = SocketCollection.getAttachmentsCopy(inputData);
    if (selectedNoteGlobalId && downloadAttachments && downloadAttachments.length) {
      for (let attachData of downloadAttachments) {
        if (attachData.noteGlobalId === selectedNoteGlobalId) {
          if (appWindow.get()) {
            if(selectedNote && typeof (selectedNote['text_version']) !== 'undefined' && selectedNote['text_version'] >= 2) {
              const updateData = {
                workspaceId,
                noteGlobalId: selectedNote.globalId,
                globalId: attachData.globalId
              };
              socketConnection.sendNoteAttachmentsUpdateMessage(updateData);
            } else {
              appWindow.get().webContents.send('event:client:note:text:update:response', {
                globalId: attachData.globalId
              });
            }
          }
        }
      }
    }
    SocketCollection.clearAttachmentsCopy(inputData);
    return resolve(true);
  });
}

