import chalk from "chalk";
import {default as config} from "../../../../../config";
import {default as SyncStatusChangedEvent} from "../../events/SyncStatusChangedEvent";
import {default as SyncStatusDisplayEvent} from "../../events/SyncStatusDisplayEvent";
import {default as downloadFolders} from "./downloadFolders";
import {default as downloadTags} from "./downloadTags";
import {default as downloadRemovedItems} from "./downloadRemovedItems";
import {default as downloadAllStructureNotes} from "./downloadAllStructureNotes";
import {default as clearAllMoreThanLimitInNotes} from "./clearAllMoreThanLimitInNotes";
import {default as updateFolderCounters} from "./updateFolderCounters";
import {default as sendSocketStoreMessages} from "./sendSocketStoreMessages";
import {default as SocketCollection} from "../../../socket/store/SocketCollection";
import {default as errorHandler} from "../../../../utilities/errorHandler";
import syncHandler from "../../../../utilities/syncHandler";

/**
 * @param { workspaceId:string, service:NimbusSyncService, manualSync:boolean } inputData
 * @param {Function} callback
 */
export default function fastDownload(inputData, callback = (err, res) => {
}) {
  // @ts-ignore
  return new Promise(async () => {
    const {workspaceId, service} = inputData;

    syncHandler.log(`sync => fastDownload => start (ws: ${workspaceId})`);

    /**
     * @param {{workspaceId:string, service:NimbusSyncService}} inputData
     */
    let downloadUserFolders = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        downloadFolders(inputData, (err, responseFolders) => {
          if (err) {
            errorHandler.log({
              err: err, response: responseFolders,
              description: "Sync problem => fastDownload => downloadUserFolders",
              data: inputData
            });
            resolve(null);
          }
          resolve(responseFolders);
        });
      });
    };

    /**
     * @param {{workspaceId:string, service:NimbusSyncService}} inputData
     */
    let downloadUserTags = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        downloadTags(inputData, (err, responseTags) => {
          if (err) {
            errorHandler.log({
              err: err, response: responseTags,
              description: "Sync problem => fastDownload => downloadUserTags",
              data: inputData
            });
            resolve(null);
          }
          resolve(responseTags);
        });
      });
    };

    /**
     * @param {{workspaceId:string, service:NimbusSyncService}} inputData
     */
    let downloadUserRemovedItems = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        downloadRemovedItems(inputData, (err, responseRemovedItems) => {
          if (err) {
            errorHandler.log({
              err: err, response: responseRemovedItems,
              description: "Sync problem => fastDownload => responseRemovedItems",
              data: inputData
            });
            resolve(null);
          }
          resolve(responseRemovedItems);
        });
      });
    };

    /**
     * @param {{workspaceId:string, service:NimbusSyncService}} inputData
     */
    let downloadAllUserStructureNotes = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        downloadAllStructureNotes(inputData, (err, responseAllNotes) => {
          if (err) {
            errorHandler.log({
              err: err, response: responseAllNotes,
              description: "Sync problem => fastDownload => downloadAllUserStructureNotes",
              data: inputData
            });
            resolve(null);
          }
          resolve(responseAllNotes);
        });
      });
    };

    /**
     * @param {{workspaceId:string, service:NimbusSyncService}} inputData
     */
    let clearAllUserMoreThanLimitInNotes = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        clearAllMoreThanLimitInNotes(inputData, (err, responseClearNotes) => {
          if (err) {
            errorHandler.log({
              err: err, response: responseClearNotes,
              description: "Sync problem => fastDownload => clearAllUserMoreThanLimitInNotes",
              data: inputData
            });
            resolve(null);
          }
          resolve(responseClearNotes);
        });
      });
    };

    /**
     * @param {{workspaceId:string, service:NimbusSyncService}} inputData
     */
    let updateUserFolderCounters = async (inputData) => {
      // @ts-ignore
      return new Promise((resolve) => {
        updateFolderCounters(inputData, (err, responseFolderCounters) => {
          if (err) {
            errorHandler.log({
              err: err, response: responseFolderCounters,
              description: "Sync problem => fastDownload => updateUserFolderCounters",
              data: inputData
            });
            resolve(null);
          }

          resolve(responseFolderCounters);
        });
      });
    };

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    syncHandler.log(`sync => fastUserDownload => downloadUserFolders (ws: ${workspaceId})`);
    await downloadUserFolders(inputData);

    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOADING_META_INFO
    });
    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    sendSocketStoreMessages(inputData);

    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOADING_META_INFO
    });
    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    syncHandler.log(`sync => fastUserDownload => downloadUserTags (ws: ${workspaceId})`);
    await downloadUserTags(inputData);

    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOADING_META_INFO
    });
    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    sendSocketStoreMessages(inputData);

    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOADING_META_INFO
    });
    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    syncHandler.log(`sync => fastUserDownload => downloadUserRemovedItems (ws: ${workspaceId})`);
    await downloadUserRemovedItems(inputData);

    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOADING_META_INFO
    });
    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    sendSocketStoreMessages(inputData);

    await SyncStatusDisplayEvent.set({
      workspaceId: workspaceId,
      newStatus: SyncStatusDisplayEvent.STATUS.DOWNLOADING_META_INFO
    });
    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    syncHandler.log(`sync => fastUserDownload => downloadAllUserStructureNotes (ws: ${workspaceId})`);
    await downloadAllUserStructureNotes(inputData);

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    SocketCollection.saveAttachmentsCopy(inputData);
    sendSocketStoreMessages(inputData);

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    syncHandler.log(`sync => fastUserDownload => clearAllUserMoreThanLimitInNotes (ws: ${workspaceId})`);
    await clearAllUserMoreThanLimitInNotes(inputData);

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    sendSocketStoreMessages(inputData);

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    syncHandler.log(`sync => fastUserDownload => updateUserFolderCounters (ws: ${workspaceId})`);
    await updateUserFolderCounters(inputData);

    if (await SyncStatusChangedEvent.needPreventSync(inputData)) return callback(null, false);
    sendSocketStoreMessages(inputData);

    callback(null, true);
  });
}
