import {default as appWindowInstance} from "../../window/instance";

export default class AutoSyncRequest {
  /**
   * @param {{workspaceId:string}} inputData
   */
  static requestAutoSync(inputData) {
    if (appWindowInstance.get()) {
      appWindowInstance.get().webContents.send('event:client:autosync:request', inputData);
    }
  }
}
