import {isNull} from "util";
import chalk from "chalk";
import {default as appWindow} from "../../window/instance";
import {default as auth} from "../../auth/auth";
import {default as user} from "../../db/models/user";
import {default as workspace} from "../../db/models/workspace";
import SelectedWorkspace from "../../workspace/SelectedWorkspace";
import {default as NimbusSDK} from "../nimbussdk/net/NimbusSDK";

export default class LimitsPopup {
  static NOTICE_TYPE_LIMIT_ERROR = "LimitError";
  static NOTICE_OBJECT_ACCOUNT = "Account";

  /**
   * @param {{workspaceId:string, data:{}}} inputData
   */
  static show(inputData) {
    const {data} = inputData;
    const isNotice = data.objectSize < data.limitSize;
    const isPremium = data.isPremium;
    const isUserWorkspace = data.isUserWorkspace;
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:popup:limits:show:response', {
        limitSize: data.limitSize,
        name: LimitsPopup.NOTICE_TYPE_LIMIT_ERROR,
        objectSize: data.objectSize,
        objectType: LimitsPopup.NOTICE_OBJECT_ACCOUNT,
        isPremium,
        premiumSize: data.premiumSize,
        isNotice,
        isUserWorkspace,
        errorType: isNotice ? 'NoticeText' : 'ErrorText',
        errorMessage: isPremium ? 'ProText' : 'FreeText'
      });
    }
  }

  /**
   * @param {{workspaceId:string, manualSync:boolean, data: {}}} inputData
   */
  static check(inputData) {
    const {
      workspaceId,
      manualSync,
      data
    } = inputData;
    auth.getUser((err, authInfo) => {
      if (authInfo && Object.keys(authInfo).length) {
        let findQuery = {userId: authInfo.userId};

        user.find(findQuery, {}, async (err, userItem) => {
          if (err || !userItem) {
            return;
          }

          let isUserWorkspace;
          let isPremium;
          if (!workspaceId) {
            isUserWorkspace = true;
            isPremium = data.isPremiumActive;
          } else {
            const workspaceInstance = await workspace.getById(workspaceId);
            if (!workspaceInstance) {
              isUserWorkspace = true;
              isPremium = data.isPremiumActive;
            } else {
              isUserWorkspace = await workspace.isUserWorkspace(workspaceInstance);
              if (isUserWorkspace) {
                isPremium = data.isPremiumActive;
              } else {
                isPremium = await NimbusSDK.getAccountManager().isPremiumActiveAsync();
              }
            }
          }

          const errorData = {
            objectSize: data.objectSize,
            limitSize: data.limitSize,
            premiumSize: await NimbusSDK.getAccountManager().getTrafficMaxAsync(),
            isPremium,
            isUserWorkspace
          };

          if (errorData.objectSize >= errorData.limitSize) {
            if (await SelectedWorkspace.getGlobalId() === workspaceId) {
              if (manualSync) {
                LimitsPopup.show({workspaceId, data: errorData});
              } else {
                if (!userItem.popupLimitError) {
                  LimitsPopup.show({workspaceId, data: errorData});
                }
              }
              return user.update(findQuery, {popupLimitError: true, popupLimitNotice: true}, {}, () => {
              });
            }
          }

          if (errorData.isPremium) {
            return;
          }

          if (!userItem.popupLimitNotice) {
            if (errorData.objectSize >= user.WARNING_UPLOAD_SIZE) {
              if (await SelectedWorkspace.getGlobalId() === workspaceId) {
                LimitsPopup.show({workspaceId, data: errorData});
                return user.update(findQuery, {popupLimitNotice: true}, {}, () => {
                });
              }
            }
          }
        });
      }
    });
  }
}
