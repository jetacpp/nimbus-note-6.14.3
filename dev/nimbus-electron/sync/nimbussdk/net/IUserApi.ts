import {default as Base_Request} from "./common/Base_Request";

import {default as SignInResponse} from "./response/SignInResponse";
import {default as SignUpResponse} from "./response/SignUpResponse";
import {default as UserInfoResponse} from "./response/UserInfoResponse";
import {default as UpdateUserInfoResponse} from "./response/UpdateUserInfoResponse";
import {default as UserVariablesResponse} from "./response/UserVariablesResponse";
import {default as UpdateUserVariableResponse} from "./response/UpdateUserVariableResponse";
import {default as ChallengeResponse} from "./response/ChallengeResponse";
import {default as UserChangePasswordResponse} from "./response/UserChangePasswordResponse";
import {default as RemindPasswordResponse} from "./response/RemindPasswordResponse";
import {default as LogoutResponse} from "./response/LogoutResponse";
import {default as UserUnlockPremiumResponse} from "./response/UserUnlockPremiumResponse";
import TrialResponse from "./response/TrialResponse";

export default class IUserApi {
  signIn(req, callback) {
    Base_Request.make(req, SignInResponse, callback)
  };

  signUp(req, callback) {
    Base_Request.make(req, SignUpResponse, callback)
  };

  userInfo(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), UserInfoResponse, callback)
  };

  updateUserInfo(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), UpdateUserInfoResponse, callback)
  };

  userVariables(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), UserVariablesResponse, callback)
  };

  updateUserVariable(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), UpdateUserVariableResponse, callback)
  };

  challenge(req, callback) {
    Base_Request.make(req, ChallengeResponse, callback)
  };

  userChangePassword(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), UserChangePasswordResponse, callback)
  };

  remindPassword(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), RemindPasswordResponse, callback)
  };

  logout(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), LogoutResponse, callback)
  };

  unlockPremium(sessionId, token, url, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, {url: url}), UserUnlockPremiumResponse, callback)
  };

  getTrial(sessionId, token, url, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, {trialUrl: url}), TrialResponse, callback)
  };
}

