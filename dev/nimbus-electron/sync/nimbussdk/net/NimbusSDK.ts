import {isNull} from "util";
import {default as API} from "./API";
import {default as AccountManagerProvider} from "../manager/AccountManagerProvider";
import {default as NimbusApiProvider} from "./NimbusApiProvider";

/**
 * @type AccountManager
 */
let accountManager = null;
/**
 * @type API
 */
let api = null;

export default class NimbusSDK {
  public static setup() {
    NimbusApiProvider.setup(NimbusSDK);
  }

  /**
   * @return API
   */
  public static getApi() {
    if (isNull(api)) {
      api = NimbusApiProvider.getApi(NimbusSDK);
    }
    return api;
  }

  /**
   * @return AccountManager
   */
  public static getAccountManager() {
    if (isNull(accountManager)) {
      accountManager = AccountManagerProvider.get();
    }
    return accountManager;
  }
}
