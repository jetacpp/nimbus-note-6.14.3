import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesGetStructureRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @type {{}}
   */
  features;

  /**
   * @param {{workspaceId:string, lastUpdateTime:int, start:int, amount:int}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_GET);
    this.body = new Body(inputData);
    this.features = {
      noteEditor: true
    }
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  returnData;
  /**
   * @type {boolean}
   */
  getFieldsUpdateDates;
  /**
   * @type {boolean}
   */
  getSharedUrls;
  /**
   * @type {number}
   */
  last_update_time;
  /**
   * @type {Limit}
   */
  limit;

  /**
   * @param {{workspaceId:string, lastUpdateTime:int, start:int, amount:int}} inputData
   */
  constructor(inputData) {
    const {workspaceId, lastUpdateTime} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.getFieldsUpdateDates = true;
    this.getSharedUrls = true;
    this.returnData = "all";
    this.last_update_time = lastUpdateTime;
    this.limit = new Limit(inputData);
  }
}

class Limit {
  /**
   * @type {number}
   */
  start;
  /**
   * @type {number}
   */
  amount;
  /**
   * @type {string}
   */
  order;
  /**
   * @type {string}
   */
  type;
  /**
   * @type {string}
   */
  global_id;

  /**
   * @param {{start:int, amount:int}} inputData
   * @constructor
   */
  constructor(inputData) {
    const {start, amount, globalId} = inputData;
    this.start = start;
    this.amount = amount;
    this.order = "id desc";
    this.type = "note";
    if(globalId) {
      this.global_id = [globalId];
    }
  }
}

/*
 {
 "action": "notes:get",
 "body": {
 "last_update_time": $client.last_update_time,
 "returnData": "structure",
 "limit": {
 "type": "note",
 "start": 0,
 "amount": 500
 }
 }
 }
 */
