import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class WorkspaceMemberAddRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{}} body
   */
  constructor(body) {
    super(ApiConst.ACTION_WORKSPACE_MEMBER_ADD);
    this.body = new Body(body);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string} (reader|editor|admin)
   */
  role;
  /**
   * @type {string} (encryptionAccessor|encryptor|encryptionManager|encryptionDeny)
   */
  encryptRole;
  /**
   * @type {string}
   */
  email;
  /**
   * @type {string}
   */
  username;

  /**
   * @param {{}} body
   */
  constructor(body) {
    this.workspaceId = '';
    this.role = '';
    this.encryptRole = '';
    this.email = '';
    this.username = '';

    if (body.workspaceId) {
      this.workspaceId = body.workspaceId;
    }
    if (body.role) {
      this.role = body.role;
    }
    if (body.encryptRole) {
      this.encryptRole = body.encryptRole;
    }
    if (body.email) {
      this.email = body.email ? body.email.toLowerCase() : '';
    }
    if (body.username) {
      this.username = body.username;
    }
  }
}

/*
 {
 "action": "notes:addWorkspaceMember",
 "body": {
    workspaceId: '',
    role: '',
    encryptRole: '',
    email: '',
    username: ''
 }
 }
 */
