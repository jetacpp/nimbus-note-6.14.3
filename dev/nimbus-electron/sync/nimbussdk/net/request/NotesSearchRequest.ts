import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";
import chalk from "chalk";

export default class NotesSearchRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, query:string, start:int, amount:int}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_SEARCH);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  location;
  /**
   * @type {boolean}
   */
  buildExcerpts;
  /**
   * @type {boolean}
   */
  getMatches;
  /**
   * @type {boolean}
   */
  getShortAnnotations;
  /**
   * @type {string}
   */
  query;
  /**
   * @type {Limit}
   */
  limit;

  /**
   * @param {{workspaceId:string, query:string, buildExcerpts:boolean, getMatches:boolean, start:int, amount:int}} inputData
   */
  constructor(inputData) {
    const {workspaceId, query, buildExcerpts, getMatches} = inputData;
    if (workspaceId) {
      this.workspaceId = workspaceId;
    }
    if(buildExcerpts) {
      this.buildExcerpts = buildExcerpts;
    }
    if(getMatches) {
      this.getMatches = true;
    }
    // this.location = 'everywhere';
    // this.getShortAnnotations = true;
    this.query = query;
    this.limit = new Limit(inputData);
  }
}

class Limit {
  /**
   * @type {number}
   */
  start;
  /**
   * @type {number}
   */
  amount;

  /**
   * @param {{start:int, amount:int}} inputData
   */
  constructor(inputData) {
    const {start, amount} = inputData;
    this.start = start || 0;
    this.amount = amount || 20;
  }
}
