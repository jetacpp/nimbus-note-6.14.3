import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesInviteRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, email:string, items:[], isTestRequest:boolean}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_INVITE);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  email;
  /**
   * @type {string[]}
   */
  items;
  /**
   * @type {boolean}
   * @private
   */
  __sendMail;

  /**
   * @param {{workspaceId:string, email:string, items:[], isTestRequest:boolean}} inputData
   */
  constructor(inputData) {
    const {workspaceId, email, items, isTestRequest} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.email = email;
    this.items = items;
    this.__sendMail = isTestRequest;
  }
}

/*
 * {
 "action": "notes:invite",
 "body": {
 "email": "collaborator@mail.com",
 "items": ["4ik8oh3gXivUsmVT"]
 }
 }*/
