import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class WorkspaceInviteUpdateRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{}} body
   */
  constructor(body) {
    super(ApiConst.ACTION_WORKSPACE_INVITE_UPDATE);
    this.body = new Body(body);
  }
}

class Body {
  /**
   * @type {int}
   */
  id;
  /**
   * @type {string} (reader|editor|admin)
   */
  role;
  /**
   * @type {string} (encryptionAccessor|encryptor|encryptionManager|encryptionDeny)
   */
  encryptRole;

  /**
   * @param {{}} body
   */
  constructor(body) {
    this.id = 0;
    this.role = '';
    this.encryptRole = '';

    if (body.id) {
      this.id = body.id;
    }
    if (body.role) {
      this.role = body.role;
    }
    if (body.encryptRole) {
      this.encryptRole = body.encryptRole;
    }
  }
}

/*
 {
 "action": "notes:updateWorkspaceInvite",
 "body": {
    id: 0,
    role: '',
    encryptRole: ''
 }
 }
 */
