import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

/**
 * @param {{}} body
 * @constructor
 */
export default class WorkspaceUpdateRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  constructor(body) {
    super(ApiConst.ACTION_WORKSPACE_UPDATE);
    this.body = new Body(body);
  }
}

class Body {
  /**
   * @type {string}
   */
  globalId;
  /**
   * @type {string}
   */
  title;

  /**
   * @param {{}} body
   */
  constructor(body) {
    this.globalId = '';
    this.title = '';

    if (body.globalId) {
      this.globalId = body.globalId;
    }
    if (body.title) {
      this.title = body.title;
    }
  }
}

/*
 {
 "action": "notes:updateWorkspace",
 "body": {
    globalId: '',
    title: ''
 }
 }
 */
