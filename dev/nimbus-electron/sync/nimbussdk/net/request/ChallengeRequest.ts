import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

/**w
 * @constructor
 */
export default class ChallengeRequest extends Base_Request {
  /**
   * @type {{}}
   */
  urlParams;
  /**
   * @type {string}
   */
  state = "";
  /**
   * @type {string}
   */
  answer = "";

  constructor(state, answer) {
    super("challenge");
    this.urlParams = {
      host: ApiConst.BASE_AUTH_DATA.host,
      port: ApiConst.BASE_AUTH_DATA.port,
      path: ApiConst.ACTION_USER_GET_CHALLENGE
    };
    this.state = state;
    this.answer = answer;
  }
}
