import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesRestoreBackupRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, backupId:string, overwrite:boolean, restoreItems: []}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_RESTORE_BACKUP);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  global_id;
  /**
   * @type {boolean}
   */
  overwrite;
  /**
   * @type {string[]}
   */
  items;

  /**
   * @param {{workspaceId:string, backupId:string, overwrite:boolean, restoreItems: []}} inputData
   */
  constructor(inputData) {
    const {workspaceId, backupId, overwrite, restoreItems} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.global_id = backupId;
    this.overwrite = overwrite;
    this.items = restoreItems;
  }
}
