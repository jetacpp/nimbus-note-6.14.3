import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class WorkspaceMembersGetRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @type {{*}}
   */
  features;

  /**
   * @param {string} globalId
   */
  constructor(globalId) {
    super(ApiConst.ACTION_WORKSPACE_MEMBERS_GET);
    this.features = {
      "encryptRole": true
    };
    this.body = new Body(globalId);
  }
}

class Body {
  /**
   * @type {string}
   */
  globalId;

  /**
   * @param {string} globalId
   */
  constructor(globalId) {
    this.globalId = globalId;
  }
}

/*
 {
 "action": "notes:getWorkspaceMembers",
 "body": {
    globalId: ""
 }
 }
 */
