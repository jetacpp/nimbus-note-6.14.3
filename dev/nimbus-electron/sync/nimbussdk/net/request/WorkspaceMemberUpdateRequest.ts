import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class WorkspaceMemberUpdateRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{}} body
   */
  constructor(body) {
    super(ApiConst.ACTION_WORKSPACE_MEMBER_UPDATE);
    this.body = new Body(body);
  }
}

class Body {
  /**
   * @type {string}
   */
  memberId;
  /**
   * @type {string} (reader|editor|admin)
   */
  role;
  /**
   * @type {string} (encryptionAccessor|encryptor|encryptionManager|encryptionDeny)
   */
  encryptRole;

  /**
   * @param {{}} body
   */
  constructor(body) {
    this.memberId = '';
    this.role = '';
    this.encryptRole = '';

    if (body.memberId) {
      this.memberId = body.memberId;
    }
    if (body.role) {
      this.role = body.role;
    }
    if (body.encryptRole) {
      this.encryptRole = body.encryptRole;
    }
  }
}

/*
 {
 "action": "notes:updateWorkspaceMember",
 "body": {
    memberId: '',
    role: '',
    encryptRole: ''
 }
 }
 */
