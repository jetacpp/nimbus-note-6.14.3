import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

/**
 * @param {string} oldPassword
 * @param {string} newPassword
 * @constructor
 */
export default class UserChangePasswordRequest extends Base_Request {
  /**
   * @type {string}
   */
  oldpassword;
  /**
   * @type {string}
   */
  newpassword;

  constructor(oldPassword, newPassword) {
    super(ApiConst.ACTION_USER_CHANGE_PASSWORD);
    this.oldpassword = "";
    this.newpassword = "";
    if (oldPassword)
      this.oldpassword = oldPassword;
    if (newPassword)
      this.newpassword = newPassword;
  }
}
