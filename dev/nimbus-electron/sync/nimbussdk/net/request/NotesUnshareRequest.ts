import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesUnshareRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, globalIds:[]}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_UNSHARE);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string[]}
   */
  global_id;

  /**
   * @param {{workspaceId:string, globalIds:[]}} inputData
   */
  constructor(inputData) {
    const {workspaceId, globalIds} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.global_id = globalIds;
  }
}

/*
 * {
 "action": "notes:unshare",
 "body": {
 "global_id": [
 "id1",
 "id2"
 ]
 }
 }*/
