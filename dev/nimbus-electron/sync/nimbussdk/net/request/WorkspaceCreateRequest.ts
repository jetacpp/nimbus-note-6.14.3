import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class WorkspaceCreateRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{}} body
   */
  constructor(body) {
    super(ApiConst.ACTION_WORKSPACE_CREATE);
    this.body = new Body(body);
  }
}

class Body {
  /**
   * @type {string}
   */
  title;
  /**
   * @type {string}
   */
  orgId;

  /**
   * @param {{}} body
   */
  constructor(body) {
    this.title = '';

    if (body.orgId) {
      this.orgId = body.orgId;
    }
    if (body.title) {
      this.title = body.title;
    }
  }
}

/*
 {
 "action": "notes:createWorkspace",
 "body": {
    orgId: '', // optional
    title: ''
 }
 }
 */
