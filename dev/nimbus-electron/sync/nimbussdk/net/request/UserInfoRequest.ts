import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class UserInfoRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {{}}
   */
  features;

  /**
   * @param {{workspaceId:string}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_USER_INFO);
    this.body = new Body(inputData);
    this.features = {
      extendedUserInfo: true
    }
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;

  /**
   * @param {{workspaceId:string}} inputData
   */
  constructor(inputData) {
    const {workspaceId} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
  }
}
