import chalk from "chalk";
import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class UpdateUserVariableRequest extends Base_Request {
    /**
     * @type {Body}
     */
    body;
    /**
     * @type {{}}
     */
    features;

    /**
     * @param {{key:string, value:string|number|boolean|object}} inputData
     */
    constructor(inputData) {
        super(ApiConst.ACTION_UPDATE_USER_VARIABLES);
        this.body = {...inputData};
        this.features = {
            extendedUserInfo: true
        }
    }
}

class Body {
    /**
     * @type {string}
     */
    key;
    /**
     * @type {string|number|boolean|object}
     */
    value;

    /**
     * @param {{key:string, value:string|number|boolean|object}} inputData
     */
    constructor(inputData) {
        const {key, value} = inputData;
        if (typeof (key) !== 'undefined') {
            this.key = key;
        }
        if (typeof (value) !== 'undefined') {
            this.value = value;
        }
    }
}
