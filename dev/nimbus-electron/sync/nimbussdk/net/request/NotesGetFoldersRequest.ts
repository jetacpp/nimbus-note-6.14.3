import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesGetFoldersRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, lastUpdateTime:int}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_GET);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {number}
   */
  last_update_time;
  /**
   * @type {Limit}
   */
  limit;

  /**
   * @param {{workspaceId:string, lastUpdateTime:int}} inputData
   */
  constructor(inputData) {
    const {workspaceId, lastUpdateTime} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.last_update_time = lastUpdateTime;
    this.limit = new Limit();
  }
}

class Limit {
  /**
   * @type {string}
   */
  type;

  constructor() {
    this.type = "folder";
  }
}

/*
 {
 "action": "notes:get",
 "body": {
 "last_update_time": $client.last_update_time,
 "limit": {
 "type": "folder"
 }
 }
 }
 */
