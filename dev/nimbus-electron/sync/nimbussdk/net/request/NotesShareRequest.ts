import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesShareRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, globalIds:[string], password:string, recursive:boolean}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_SHARE);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string[]}
   */
  global_id;
  /**
   * @type {string}
   */
  password;
  /**
   * @type {boolean}
   */
  recursive;

  /**
   * @param {{workspaceId:string, globalIds:[string], password:string, recursive:boolean}} inputData
   */
  constructor(inputData) {
    const {workspaceId, globalIds, password, recursive} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.global_id = globalIds;
    if (typeof (password) !== 'undefined' && password !== null) {
      this.password = password;
    }
    this.recursive = recursive || false;
  }
}
