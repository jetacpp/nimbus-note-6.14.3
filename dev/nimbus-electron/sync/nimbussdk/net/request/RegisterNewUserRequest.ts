import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class RegisterNewUserRequest extends Base_Request {
  /**
   * @type {{}}
   */
  urlParams;

  /**
   * @type {string}
   */
  login;
  /**
   * @type {string}
   */
  password;
  /**
   * @type {string}
   */
  service;
  /**
   * @type {string[]}
   */
  languages;

  /**
   * @param {string} login
   * @param {string} password
   * @param {string} service
   * @param {string} language
   */
  constructor(login, password, service, language) {
    super("register");
    this.urlParams = {
      host: ApiConst.BASE_AUTH_DATA.host,
      port: ApiConst.BASE_AUTH_DATA.port,
      path: ApiConst.ACTION_USER_REGISTER
    };
    this.login = login;
    this.password = password;
    this.service = service;
    this.languages = [language];
  }
}
