import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class UserVariablesRequest extends Base_Request {
    /**
     * @type {{}}
     */
    body;
    /**
     * @type {{}}
     */
    features;

    constructor() {
        super(ApiConst.ACTION_USER_VARIABLES);
        this.body = {};
        this.features = {
            extendedUserInfo: true
        }
    }
}
