import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesAccountRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  constructor() {
    super(ApiConst.ACTION_NOTES_ACCOUNT);
    this.body = new Body();
  }
}

class Body {

}

