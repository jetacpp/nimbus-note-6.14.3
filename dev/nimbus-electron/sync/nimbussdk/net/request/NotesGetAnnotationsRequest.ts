import chalk from "chalk";
import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesGetAnnotationsRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {{}}
   */
  features;

  /**
   * @param {{workspaceId:string}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_GET_ANNOTATIONS);
    this.body = new Body(inputData);
    this.features = {
      //extendedUserInfo: true
    }
  }
}

class Body {
  /**
   * @type {string}
   */
  syncToken;
  /**
   * @type {string}
   */
  pageToken;
  /**
   * @type {number}
   */
  onPage;

  /**
   * @param {{syncToken:string, pageToken:string, onPage:number}} inputData
   */
  constructor(inputData) {
    const {syncToken, pageToken, onPage} = inputData;
    if (typeof (syncToken) !== 'undefined') {
      this.syncToken = syncToken;
    }
    if (typeof (pageToken) !== 'undefined') {
      this.pageToken = pageToken;
    }
    if (typeof (onPage) !== 'undefined') {
      this.onPage = onPage;
    }
  }
}
