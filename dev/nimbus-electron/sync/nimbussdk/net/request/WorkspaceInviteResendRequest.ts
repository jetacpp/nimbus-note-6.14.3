import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class WorkspaceInviteResendRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {int} id
   */
  constructor(id) {
    super(ApiConst.ACTION_WORKSPACE_INVITE_RESEND);
    this.body = new Body(id);
  }
}

class Body {
  /**
   * @type {int}
   */
  id;

  /**
   * @param {int} id
   */
  constructor(id) {
    this.id = 0;

    if (id) {
      this.id = id;
    }
  }
}

/*
 {
 "action": "notes:resendWorkspaceInvite",
 "body": {
    id: 0
 }
 }
 */
