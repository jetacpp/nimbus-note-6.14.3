import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class WorkspaceGetRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {string} globalId
   */
  constructor(globalId) {
    super(ApiConst.ACTION_WORKSPACE_GET);
    this.body = new Body(globalId);
  }
}

/**
 * @param {string} globalId
 * @constructor
 */
class Body {
  /**
   * @type {string}
   */
  globalId;

  /**
   * @param {string} globalId
   */
  constructor(globalId) {
    this.globalId = globalId;
  }
}

/*
 {
 "action": "notes:getWorkspace",
 "body": {
    globalId: ""
 }
 }
 */
