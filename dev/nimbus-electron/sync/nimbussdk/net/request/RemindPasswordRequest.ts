import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class RemindPasswordRequest extends Base_Request {
  /**
   * @type {{}}
   */
  urlParams;

  /**
   * @type {string}
   */
  login = "";

  /**
   * @param {string} email
   */
  constructor(email) {
    super("remind");
    this.urlParams = {
      host: ApiConst.BASE_AUTH_DATA.host,
      port: ApiConst.BASE_AUTH_DATA.port,
      path: ApiConst.ACTION_USER_REMIND_PASSWORD
    };
    this.login = "";
    if (email) {
      this.login = email.toLowerCase();
    }
  }
}
