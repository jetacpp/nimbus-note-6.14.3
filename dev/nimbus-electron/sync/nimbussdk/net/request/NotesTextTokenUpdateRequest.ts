import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesTextTokenUpdateRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_GET_NOTES_TEXT_TOKEN_UPDATE_REQUEST);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  globalId;

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  constructor(inputData) {
    const {workspaceId, globalId} = inputData;
    this.workspaceId = workspaceId;
    this.globalId = globalId;
  }
}

/*
 {
 "action": "notes:issueTextToken",
 "body": {
    workspaceId: "",
    globalId: ""
 }
 }
 */
