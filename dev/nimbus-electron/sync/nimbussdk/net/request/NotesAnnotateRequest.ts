import chalk from "chalk";
import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesAnnotateRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {{}}
   */
  features;

  /**
   * @param {{workspaceId:string}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_ANNOTATE);
    this.body = new Body(inputData);
    this.features = {
      //extendedUserInfo: true
    }
  }
}

class Body {
  /**
   * @type {string}
   */
  attachmentGlobalId;
  /**
   * @type {string}
   */
  tempFileName;
  /**
   * @type {string}
   */
  role;
  /**
   * @type {number}
   */
  trialCredits;

  /**
   * @param {{attachmentGlobalId:string, tempFileName:string, role:string, trialCredits:number}} inputData
   */
  constructor(inputData) {
    const {attachmentGlobalId, tempFileName, role, trialCredits} = inputData;
    if (typeof (attachmentGlobalId) !== 'undefined') {
      this.attachmentGlobalId = attachmentGlobalId;
    }
    if (typeof (tempFileName) !== 'undefined') {
      this.tempFileName = tempFileName;
    }
    if (typeof (role) !== 'undefined') {
      this.role = role;
    }
    if (typeof (trialCredits) !== 'undefined') {
      this.trialCredits = trialCredits;
    }
  }
}
