import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesGetAllRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, globalIds:[string]}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_GET);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  returnData;
  /**
   * @type {Limit}
   */
  limit;
  /**
   * @type {number}
   */
  last_update_time;

  /**
   * @param {{workspaceId:string, globalIds:[string]}} inputData
   */
  constructor(inputData) {
    const {workspaceId, globalIds} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.returnData = "all";
    this.limit = new Limit(globalIds);
    this.last_update_time = 0;
  }
}


class Limit {
  /**
   * @type {string[]}
   */
  global_id;

  /**
   * @param {string[]} globalIds
   */
  constructor(globalIds) {
    this.global_id = globalIds;
  }
}
