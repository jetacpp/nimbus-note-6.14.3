import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

/**
 * @constructor
 */
export default class NotesGetBackupsRequest extends Base_Request {
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_GET_BACKUPS);
  }
}
