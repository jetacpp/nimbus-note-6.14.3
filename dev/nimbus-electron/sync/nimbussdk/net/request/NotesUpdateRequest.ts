import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";
import chalk from "chalk";

class Store {
  /**
   * @type {[]}
   */
  notes;
  /**
   * @type {[]}
   */
  tags;

  constructor() {
    this.notes = [];
    this.tags = [];
  }
}

class Remove {
  /**
   * @type {[]}
   */
  notes;
  /**
   * @type {[]}
   */
  attachements;
  /**
   * @type {[]}
   */
  tags;

  constructor() {
    this.notes = [];
    this.attachements = [];
    this.tags = [];
  }
}

class RenameTag {
  /**
   * @type {string}
   */
  oldtag;
  /**
   * @type {string}
   */
  newtag;

  /**
   * @param {string} oldtag
   * @param {string} newtag
   */
  constructor(oldtag, newtag) {
    this.oldtag = oldtag;
    this.newtag = newtag;
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {Remove|null}
   */
  remove;
  /**
   * @type {Store|null}
   */
  store;
  /**
   * @type {RenameTag|null}
   */
  renameTags;

  /**
   * @param {{workspaceId:string, body:{}}} inputData
   */
  constructor(inputData) {
    const {workspaceId, body} = inputData;
    this.remove = null;
    this.store = null;
    this.renameTags = null;

    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }

    if (body) {
      if (body.remove)
        this.remove = body.remove;
      if (body.store)
        this.store = body.store;
      if (body.renameTags)
        this.renameTags = body.renameTags;
    }
  }

  static Store = Store;
  static Remove = Remove;
  static RenameTag = RenameTag;

  /**
   * @param {AbstractNote[]} storeFolders
   */
  storeFolders(storeFolders = []) {
    if (storeFolders && storeFolders.length) {
      if (!this.store) {
        this.store = new Store();
      }
      if (!this.store.notes) {
        this.store.notes = [];
      }
      for (let i in storeFolders) {
        this.store.notes.push(storeFolders[i]);
      }
    }
  }

  /**
   * @param {AbstractNote[]} storeNotes
   */
  storeNotes(storeNotes = []) {
    if (storeNotes && storeNotes.length) {
      if (!this.store) {
        this.store = new Store();
      }
      if (!this.store.notes) {
        this.store.notes = [];
      }
      for (let i in storeNotes) {
        this.store.notes.push(storeNotes[i]);
      }
    }
  }

  /**
   * @param {string[]} storeTags
   */
  storeTags(storeTags = []) {
    if (storeTags && storeTags.length) {
      if (!this.store) {
        this.store = new Store();
      }
      if (!this.store.tags) {
        this.store.tags = [];
      }
      for (let i in storeTags) {
        this.store.tags.push(storeTags[i]);
      }
    }
  }

  /**
   * @param {string[]} removeTags
   */
  removeTags(removeTags = []) {
    if (removeTags && removeTags.length) {
      if (!this.remove) {
        this.remove = new Remove();
      }
      if (!this.remove.tags) {
        this.remove.tags = [];
      }
      for (let i in removeTags) {
        this.remove.tags.push(removeTags[i]);
      }
    }
  }

  /**
   * @param {string[]} removeFolders
   */
  removeFolders(removeFolders = []) {
    if (removeFolders && removeFolders.length) {
      if (!this.remove) {
        this.remove = new Remove();
      }
      if (!this.remove.notes) {
        this.remove.notes = [];
      }
      for (let i in removeFolders) {
        this.remove.notes.push(removeFolders[i]);
      }
    }
  }

  /**
   * @param {string[]} removeNotes
   */
  removeNotes(removeNotes = []) {
    if (removeNotes && removeNotes.length) {
      if (!this.remove) {
        this.remove = new Remove();
      }
      if (!this.remove.notes) {
        this.remove.notes = [];
      }
      for (let i in removeNotes) {
        this.remove.notes.push(removeNotes[i]);
      }
    }
  }

  /**
   * @param {string[]} removeAttachments
   */
  removeAttachements(removeAttachments = []) {
    if (removeAttachments && removeAttachments.length) {
      if (!this.remove) {
        this.remove = new Remove();
      }
      if (!this.remove.attachements) {
        this.remove.attachements = [];
      }
      for (let i in removeAttachments) {
        this.remove.attachements.push(removeAttachments[i]);
      }
    }
  }

  /**
   * @param {RenameTag[]} renameTags
   */
  setRenameTags(renameTags = []) {
    if (renameTags && renameTags.length) {
      if (!this.renameTags) {
        this.renameTags = [];
      }
      for (let i in renameTags) {
        this.renameTags.push(renameTags[i]);
      }
    }
  }
}

export default class NotesUpdateRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @type {{}}
   */
  features;

  /**
   * @param {{workspaceId:string, body:{}}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_UPDATE);
    this.body = new Body(inputData);
    this.features = {
      noteEditor: true
    }
  }

  /**
   * @type Body
   */
  static Body = Body;

  /**
   * @param {{workspaceId:string, body:{}}} inputData
   * @return {NotesUpdateRequest}
   */
  static get(inputData) {
    return new NotesUpdateRequest(inputData);
  }
}
