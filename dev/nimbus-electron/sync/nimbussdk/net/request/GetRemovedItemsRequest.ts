import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class GetRemovedItemsRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, lastUpdateTime:int}} inputData
   * @constructor
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_GET);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {workspaceId}
   */
  workspaceId;
  /**
   * @type {boolean}
   */
  ignoreNotes;
  /**
   * @type {boolean}
   */
  getRemovedInfo;
  /**
   * @type {number}
   */
  last_update_time;

  /**
   * @param {{workspaceId:string, lastUpdateTime:int}} inputData
   */
  constructor(inputData) {
    const {workspaceId, lastUpdateTime} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.ignoreNotes = true;
    this.getRemovedInfo = true;
    this.last_update_time = lastUpdateTime;
  }
}
