import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class LogoutRequest extends Base_Request {
  constructor() {
    super(ApiConst.ACTION_USER_LOGOUT);
  }
}
