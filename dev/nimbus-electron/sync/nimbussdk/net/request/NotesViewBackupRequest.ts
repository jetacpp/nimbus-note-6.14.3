import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesViewBackupRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, backupId:string}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_VIEW_BACKUP);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  global_id;

  /**
   * @param {{workspaceId:string, backupId:string}} inputData
   */
  constructor(inputData) {
    const {workspaceId, backupId} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.global_id = backupId;
  }
}
