import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class FilesTempExistsRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, tempname:string}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_FILES_TEMP_EXISTS);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  tempname;

  /**
   * @param {{workspaceId:string, tempname:string}} inputData
   */
  constructor(inputData) {
    const {workspaceId, tempname} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.tempname = tempname;
  }
}
