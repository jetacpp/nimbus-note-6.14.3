import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class WorkspaceMemberDeleteRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {string} memberId
   */
  constructor(memberId) {
    super(ApiConst.ACTION_WORKSPACE_MEMBER_DELETE);
    this.body = new Body(memberId);
  }
}

class Body {
  /**
   * @type {string}
   */
  memberId;

  /**
   * @param {string} memberId
   */
  constructor(memberId) {
    this.memberId = '';

    if (memberId) {
      this.memberId = memberId;
    }
  }
}

/*
 {
 "action": "notes:deleteWorkspaceMember",
 "body": {
    memberId: ''
 }
 }
 */
