import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class WorkspaceDeleteRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {string} globalId
   */
  constructor(globalId) {
    super(ApiConst.ACTION_WORKSPACE_DELETE);
    this.body = new Body(globalId);
  }
}

class Body {
  /**
   * @type {string}
   */
  globalId;

  /**
   * @param {string} globalId
   */
  constructor(globalId) {
    this.globalId = '';

    if (globalId) {
      this.globalId = globalId;
    }
  }
}

/*
 {
 "action": "notes:deleteWorkspace",
 "body": {}
 }
 */
