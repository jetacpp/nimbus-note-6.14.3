import chalk from "chalk";
import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class UpdateUserInfoRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {{}}
   */
  features;

  /**
   * @param {{workspaceId:string}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_UPDATE_USER_INFO);
    this.body = new Body(inputData);
    this.features = {
      extendedUserInfo: true
    }
  }
}

class Body {
  /**
   * @type {{tempFileName:string}}
   */
  avatar;
  /**
   * @type {string}
   */
  firstname;
  /**
   * @type {string}
   */
  lastname;
  /**
   * @type string
   */
  username;

  /**
   * @param {{avatar:{tempFileName:string}, firstname:string, lastname:string, username:string}} inputData
   */
  constructor(inputData) {
    const {avatar, firstname, lastname, username} = inputData;
    if (typeof (avatar) !== 'undefined') {
      this.avatar = avatar;
    }
    if (typeof (firstname) !== 'undefined') {
      this.firstname = firstname;
    }
    if (typeof (lastname) !== 'undefined') {
      this.lastname = lastname;
    }
    if (typeof (username) !== 'undefined') {
      this.username = username;
    }
  }
}
