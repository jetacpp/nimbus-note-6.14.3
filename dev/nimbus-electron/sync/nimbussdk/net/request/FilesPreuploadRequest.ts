import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class FilesPreuploadRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, tempname:string, extension:string, mime:string}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_FILES_PREUPLOAD);
    this.body = new Body(inputData);
  }
}


class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  tempname;
  /**
   * @type {string}
   */
  extension;
  /**
   * @type {string}
   */
  mime;
  /**
   * @type {boolean}
   */
  isFormData;

  /**
   * @param {{workspaceId:string, tempname:string, extension:string, mime:string}} inputData
   */
  constructor(inputData) {
    const {workspaceId, tempname, extension, mime} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.tempname = tempname;
    this.extension = extension;
    this.mime = mime;
    this.isFormData = true;
  }
}
