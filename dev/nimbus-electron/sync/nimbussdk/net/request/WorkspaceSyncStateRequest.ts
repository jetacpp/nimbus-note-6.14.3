import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";
import {default as SyncWorkspaceEntity} from "../response/entities/SyncWorkspaceEntity";

export default class WorkspaceSyncStateRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {[SyncWorkspaceEntity]} workspaces
   */
  constructor(workspaces) {
    super(ApiConst.ACTION_WORKSPACES_SYNC_STATE);
    this.body = new Body(workspaces);
  }
}

class Body {
  /**
   * @type {[SyncWorkspaceEntity]}
   */
  workspaces;

  /**
   * @param {[SyncWorkspaceEntity]} workspaces
   */
  constructor(workspaces) {
    this.workspaces = workspaces;
  }
}

/*
 {
 "action": "notes:syncState",
 "body": {
    workspaces: [
      {

      }
    ]
 }
 }
 */
