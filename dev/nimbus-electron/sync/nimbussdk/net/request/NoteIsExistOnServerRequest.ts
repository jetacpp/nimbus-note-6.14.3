import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NoteIsExistOnServerRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_GET);
    this.body = new Body(inputData);
  }
}


class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  returnData;
  /**
   * @type {number}
   */
  last_update_time;
  /**
   * @type {Limit}
   */
  limit;

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   */
  constructor(inputData) {
    const {workspaceId} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.returnData = "structure";
    this.last_update_time = 0;
    this.limit = new Limit(inputData);
  }
}

class Limit {
  /**
   * @type {number}
   */
  global_id;

  /**
   * @param {{noteGlobalId:string}} inputData
   */
  constructor(inputData) {
    const {noteGlobalId} = inputData;
    this.global_id = noteGlobalId;
  }
}
