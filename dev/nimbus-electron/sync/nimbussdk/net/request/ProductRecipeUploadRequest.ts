import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class ProductRecipeUploadRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {string} recipeUrl
   */
  constructor(recipeUrl) {
    super(ApiConst.ACTION_PRODUCT_RECIPE_UPLOAD);
    this.body = new Body(recipeUrl);
  }
}

class Body {
  /**
   * @type {string}
   */
  recipeUrl;

  /**
   * @param {string} recipeUrl
   */
  constructor(recipeUrl) {
    this.recipeUrl = recipeUrl;
  }
}
