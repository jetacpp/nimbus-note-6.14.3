import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

/**
 * @param {{}} body
 * @constructor
 */
export default class MoveToWorkspaceRequest extends Base_Request {
  /**
   * @type int
   */
  timeout;
  /**
   * @type {Body}
   */
  body;

  constructor(body) {
    super(ApiConst.ACTION_MOVE_TO_WORKSPACE);
    this.timeout = 300000;
    this.body = new Body(body);
  }
}

class Body {
  /**
   * @type {string}
   */
  globalId;
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  toWorkspaceId;
  /**
   * @type {boolean}
   */
  remove;

  /**
   * @param {{}} body
   */
  constructor(body) {
    this.globalId = '';
    this.toWorkspaceId = '';
    this.workspaceId = '';
    this.remove = true;

    if (body.globalId) {
      this.globalId = body.globalId;
    }
    if(body.workspaceId) {
      this.workspaceId = body.workspaceId;
    }
    if (body.toWorkspaceId) {
      this.toWorkspaceId = body.toWorkspaceId;
    }
    if (typeof (body.remove) !== 'undefined') {
      this.remove = body.remove;
    }
  }
}

/*
 {
 "action": "notes:moveToWorkspace",
 "body": {
    globalId: '',
    toWorkspaceId: '',
    remove: true
 }
 }
 */
