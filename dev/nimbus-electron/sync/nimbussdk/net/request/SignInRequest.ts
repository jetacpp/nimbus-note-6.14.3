import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class SignInRequest extends Base_Request {
  /**
   * @type {{}}
   */
  urlParams;

  /**
   * @type {string}
   */
  login;
  /**
   * @type {string}
   */
  password;

  /**
   * @param {string} login
   * @param {string} password
   */
  constructor(login, password) {
    super("auth");
    this.urlParams = {
      host: ApiConst.BASE_AUTH_DATA.host,
      port: ApiConst.BASE_AUTH_DATA.port,
      path: ApiConst.ACTION_USER_AUTH
    };
    this.login = login;
    this.password = password;
  }
}
