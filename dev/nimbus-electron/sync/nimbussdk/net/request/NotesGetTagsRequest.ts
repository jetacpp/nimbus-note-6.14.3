import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesGetTagsRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, lastUpdateTime:int}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_GET);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {number}
   */
  last_update_time;
  /**
   * @type {boolean}
   */
  ignoreNotes;
  /**
   * @type {boolean}
   */
  getUserTags;

  /**
   * @param {{workspaceId:string, lastUpdateTime:int}} inputData
   */
  constructor(inputData) {
    const {workspaceId, lastUpdateTime} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.last_update_time = lastUpdateTime;
    this.ignoreNotes = true;
    this.getUserTags = true;
  }
}
