import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";


export default class NotesAcceptInviteRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, inviteId:string, items:[]}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_ACCEPT_INVITE);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  invite_id;
  /**
   * @type {string[]}
   */
  items;

  /**
   * @param {{workspaceId:string, inviteId:string, items:[]}} inputData
   */
  constructor(inputData) {
    const {workspaceId, inviteId, items} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.invite_id = inviteId;
    this.items = items;
  }
}
