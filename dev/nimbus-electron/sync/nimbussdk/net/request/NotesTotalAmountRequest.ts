import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class NotesTotalAmountRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @param {{workspaceId:string, lastUpdateTime:int}} inputData
   */
  constructor(inputData) {
    super(ApiConst.ACTION_NOTES_GET);
    this.body = new Body(inputData);
  }
}

class Body {
  /**
   * @type {string}
   */
  workspaceId;
  /**
   * @type {string}
   */
  returnData;
  /**
   * @type {number}
   */
  last_update_time;
  /**
   * @type {Limit}
   */
  limit;

  /**
   * @param {{workspaceId:string, lastUpdateTime:int}} inputData
   */
  constructor(inputData) {
    const {workspaceId, lastUpdateTime} = inputData;
    if (typeof (workspaceId) !== 'undefined') {
      this.workspaceId = workspaceId;
    }
    this.returnData = "structure";
    this.last_update_time = lastUpdateTime;
    this.limit = new Limit();
  }
}

class Limit {
  /**
   * @type {number}
   */
  start;
  /**
   * @type {number}
   */
  amount;
  /**
   * @type {string}
   */
  order;
  /**
   * @type {string}
   */
  type;

  constructor() {
    this.start = 0;
    this.amount = 0;
    this.order = "id desc";
    this.type = "note";
  }
}
