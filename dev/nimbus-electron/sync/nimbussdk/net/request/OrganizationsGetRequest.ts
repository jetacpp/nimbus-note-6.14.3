import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class OrganizationsGetRequest extends Base_Request {
    /**
     * @type {Body}
     */
    body;

    /**
     * @type {{*}}
     */
    features;

    constructor() {
        super(ApiConst.ACTION_ORGANIZATIONS_GET);
        this.features = {
            "encryptRole": true,
            "businessOrgs": true,
        };
        this.body = {};
    }
}

/*
 {
 "action": "orgs:getAll",
 }
 */
