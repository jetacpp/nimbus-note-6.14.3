import {default as ApiConst} from "../ApiConst";
import {default as Base_Request} from "../common/Base_Request";

export default class WorkspacesGetRequest extends Base_Request {
  /**
   * @type {Body}
   */
  body;

  /**
   * @type {{*}}
   */
  features;

  /**
   * @param {string} orgId
   */
  constructor(orgId = null) {
    super(ApiConst.ACTION_WORKSPACES_GET);
    this.features = {
      "encryptRole": true,
      "businessOrgs": true,
    };
    this.body = new Body(orgId);
  }
}

class Body {
  /**
   * @type {string}
   */
  orgId;

  /**
   * @param {string} orgId
   */
  constructor(orgId) {
    if (orgId) {
      this.orgId = orgId;
    }
  }
}

/*
 {
 "action": "notes:getWorkspaces",
 "body": {
    orgId: "" // optional
 }
 }
 */
