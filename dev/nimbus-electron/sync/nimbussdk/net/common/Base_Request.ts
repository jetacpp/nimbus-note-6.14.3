import chalk from "chalk";
import fs from "fs-extra";
import httpClient from "http";
import httpsClient from "https";
import FormData from "form-data";
import {default as config} from "../../../../../config";
import {AbortController, abortableFetch} from "abortcontroller-polyfill/dist/cjs-ponyfill";
import {default as ConnectionCollector} from "../../../process/connection/ConnectionCollector";
import {default as errorHandler} from "./../../../../utilities/errorHandler";
import {default as ApiConst} from "../ApiConst";
import {default as Base_Response} from "./Base_Response";
import {default as SelectedWorkspace} from "../../../../workspace/SelectedWorkspace";
import {default as workspace} from "../../../../db/models/workspace";

const {fetch} = abortableFetch(require('node-fetch'));

let method = 'POST';
let jsonMimeType = 'application/json';

export default class BaseRequest {
  static ERROR_INTERNAL_SERVER_ERROR = -10;
  static ERROR_REQUEST_ABORT_ERROR = -500;

  /**
   * @type {string}
   */
  action;
  /**
   * @type {string}
   */
  _client_software;


  /**
   * @param {string} action
   */
  constructor(action) {
    this.action = action;
    this._client_software = ApiConst._CLIENT_SOFTWARE;
  }

  /**
   * @param {{}} requestParams
   * @param {{}} responseClass
   * @param {Function} callback
   */
  static async make(requestParams = {}, responseClass, callback) {
    let params = <any>BaseRequest.getBodyJson(requestParams);
    let paramsData = <any>BaseRequest.getBody(requestParams);
    let headers = <any>BaseRequest.getHeaders(requestParams);

    // @ts-ignore
    let paramsDataBody = <any>{};
    if (paramsData) {
      paramsDataBody = paramsData;
      if (paramsData.body) {
        paramsDataBody = paramsData.body;
      }
    }

    if (paramsDataBody) {
      if (paramsDataBody.workspaceId) {
        paramsDataBody.workspaceId = await workspace.getLocalId(paramsDataBody.workspaceId);
      } else {
        if (typeof (paramsDataBody.workspaceId) === 'undefined') {
          const selectedWorkspaceId = await SelectedWorkspace.getGlobalId();
          if (selectedWorkspaceId) {
            paramsDataBody.workspaceId = selectedWorkspaceId;
          }
        } else {
          delete paramsDataBody.workspaceId;
        }
      }
    }

    const workspaceId = paramsDataBody.workspaceId ? paramsDataBody.workspaceId : null;
    const requestUniqueId = new Date().getTime() + ":api:" + paramsData.action;

    if (paramsDataBody && paramsDataBody.isFormData) {
      let form = new FormData();
      for (let filedName in headers) {
        if (headers.hasOwnProperty(filedName)) {
          form.append(filedName, headers[filedName]);
        }
      }

      if (paramsDataBody.tempname) {
        let formData = new FormData();
        let formFileHeaders = {};
        if (typeof (paramsDataBody.mime) !== "undefined" && paramsDataBody.mime) {
          formFileHeaders['contentType'] = paramsDataBody.mime;
        } else if (typeof (paramsDataBody.extension) !== "undefined" && paramsDataBody.extension) {
          formFileHeaders['filename'] = paramsDataBody.tempname + '.' + paramsDataBody.extension;
        }
        try {
          formData.append("file1", fs.createReadStream(paramsDataBody.tempname), formFileHeaders);
        } catch (e) {
          if (config.SHOW_WEB_CONSOLE) {
            console.log("Problem to read file for upload", e);
          }
        }

        let options = BaseRequest.getMultipartFormOptions(headers);
        let formAction = options.protocol + options.host + options.path;
        let formHeaders = formData.getHeaders();
        for (let i in options.headers) {
          if (options.headers.hasOwnProperty(i)) {
            formHeaders[i] = options.headers[i];
          }
        }

        let controller = <any>new AbortController();
        let signal = controller.signal;
        controller.uniqueId = requestUniqueId;
        ConnectionCollector.add({
          workspaceId,
          uniqueId: requestUniqueId,
          xhrConnection: controller
        });

        let formParams = {
          body: formData,
          headers: formHeaders,
          method: options.method,
          signal: signal
        };

        fetch(formAction, formParams)
          .then(function (res) {
            return res.json();
          }).then(function (json) {
          ConnectionCollector.clearById({
            workspaceId,
            id: controller.uniqueId
          });
          let response = new responseClass(json);
          callback(response.getErrorCode(), response);
        }).catch(err => {
          if (err.name === 'AbortError') {
            callback(BaseRequest.ERROR_REQUEST_ABORT_ERROR, {
              errorCode: BaseRequest.ERROR_REQUEST_ABORT_ERROR,
              errDesc: 'Abort request to sync server'
            });
          } else {
            callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
              errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
              errDesc: 'Fail to make request to sync server',
              err: err
            });
          }
        });
      }
    } else if(paramsDataBody && paramsDataBody.trialUrl) {

      if (!paramsDataBody.trialUrl) {
        return callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
          errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
          errDesc: 'Fail to make request to trial server',
          err: new Error("trialUrl is not defined")
        });
      }

      let options = BaseRequest.getRecipeFormOptions(headers);
      let formAction = paramsDataBody.trialUrl;
      let formHeaders = headers;
      for (let i in options.headers) {
        if (options.headers.hasOwnProperty(i)) {
          formHeaders[i] = options.headers[i];
        }
      }

      let formParams = {
        headers: formHeaders,
        method: 'GET'
      };

      fetch(formAction, formParams)
        .then(function (res) {
          return res.json();
        }).then(function (json) {
        let response = new responseClass(json);
        callback(response.getErrorCode(), response);
      }).catch(err => {
        if (err.name === 'AbortError') {
          callback(BaseRequest.ERROR_REQUEST_ABORT_ERROR, {
            errorCode: BaseRequest.ERROR_REQUEST_ABORT_ERROR,
            errDesc: 'Abort request to trialUrl server'
          });
        } else {
          callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
            errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
            errDesc: 'Fail to make request to trialUrl server',
            err: err
          });
        }
      });

    } else if (paramsDataBody && paramsDataBody.recipeUrl) {

      if (!paramsDataBody.recipeUrl) {
        return callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
          errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
          errDesc: 'Fail to make request to sync server',
          err: new Error("recipeUrl is not defined")
        });
      }

      let formData = new FormData();
      let recipePath = paramsDataBody.recipeUrl.replace("file://", "");
      let recipeContent = fs.readFileSync(recipePath);
      if (!recipeContent) {
        return callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR});
      }
      formData.append("receipt", recipeContent, "receipt.iap");

      let options = BaseRequest.getRecipeFormOptions(headers);
      let formAction = options.url;
      let formHeaders = formData.getHeaders();
      for (let i in options.headers) {
        if (options.headers.hasOwnProperty(i)) {
          formHeaders[i] = options.headers[i];
        }
      }

      let formParams = {
        body: formData,
        headers: formHeaders,
        method: options.method
      };

      fetch(formAction, formParams)
        .then(function (res) {
          return res.json();
        }).then(function (json) {
        let response = new responseClass(json);
        callback(response.getErrorCode(), response);
      }).catch(err => {
        if (err.name === 'AbortError') {
          callback(BaseRequest.ERROR_REQUEST_ABORT_ERROR, {
            errorCode: BaseRequest.ERROR_REQUEST_ABORT_ERROR,
            errDesc: 'Abort request to purchase validation server'
          });
        } else {
          callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
            errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
            errDesc: 'Fail to make request to purchase validation server',
            err: err
          });
        }
      });
    } else {

      headers['Content-Length'] = Buffer.byteLength(params, 'utf-8');
      let options = <any>BaseRequest.getOptions(headers);
      if (paramsData.urlParams) {
        for (let prop in paramsData.urlParams) {
          if (paramsData.urlParams.hasOwnProperty(prop)) {
            options[prop] = paramsData.urlParams[prop];
          }
        }
      } else {
        options.host = ApiConst.BASE_DOMAIN;
      }

      const {timeout} = paramsData;
      if(timeout) {
        options.timeout = timeout;
      }
      const httpProvider = options.port && options.port === 443 ? httpsClient : httpClient;
      // @ts-ignore
      let req = <any>httpProvider.request(options, (res) => {
        let chunks = [];
        res.on('data', (chunk) => {
          chunks.push(chunk);
        });
        res.on('end', () => {
          ConnectionCollector.clearById({
            workspaceId,
            id: req.uniqueId
          });
          Base_Response.get(chunks, res.headers['content-encoding'], responseClass, callback);
        });
      });

      if(timeout) {
        req.on('timeout', () => {
          req.abort();
        });
      }

      req.on('error', (err) => {
        callback(BaseRequest.ERROR_INTERNAL_SERVER_ERROR, {
          errorCode: BaseRequest.ERROR_INTERNAL_SERVER_ERROR,
          errDesc: 'Fail to make request to sync server',
          err: err
        });
      });

      req.on('abort', () => {
        callback(BaseRequest.ERROR_REQUEST_ABORT_ERROR, {
          errorCode: BaseRequest.ERROR_REQUEST_ABORT_ERROR,
          errDesc: 'Abort request to sync server'
        });
      });

      req.write(params);
      req.uniqueId = requestUniqueId;
      ConnectionCollector.add({
        workspaceId,
        uniqueId: requestUniqueId,
        xhrConnection: req
      });
      req.end();
    }
  }

  /**
   * @param {string} sessionId
   * @param {string} token
   * @param {{}} req
   * @returns {{}}
   */
  static prepareRequest(sessionId, token, req) {
    if (!req.headers) {
      req.headers = {};
    }
    req.headers["EverHelper-Session-ID"] = sessionId;

    return req;
  }

  /**
   * @param {{}} clientHeaders
   * @returns {{}}
   */
  static getOptions(clientHeaders = {}) {
    let options = {
      method: method,
      headers: {
        'Content-Type': jsonMimeType + '; charset=utf-8',
        'Accept-Encoding': 'gzip',
        'x-api-version': 3,
        'x-client-version': config.APP_VERSION,
        'x-client-software': ApiConst._CLIENT_SOFTWARE,
      }
    };

    for (let i in clientHeaders) {
      if (clientHeaders.hasOwnProperty(i)) {
        options.headers[i] = clientHeaders[i];
      }
    }

    return options;
  }

  /**
   * @param {{}} clientHeaders
   * @returns {{}}
   */
  static getMultipartFormOptions(clientHeaders = {}) {
    let options = {
      host: ApiConst.BASE_DOMAIN,
      path: '/' + ApiConst.ACTION_FILES_PREUPLOAD,
      protocol: 'http://',
      method: method,
      headers: {
        'x-allow-upload-errors': 1,
        'x-client-software': ApiConst._CLIENT_SOFTWARE,
        'x-client-version': config.APP_VERSION
      }
    };

    for (let i in clientHeaders) {
      if (clientHeaders.hasOwnProperty(i)) {
        options.headers[i] = clientHeaders[i];
      }
    }

    return options;
  }

  /**
   * @param {{}} clientHeaders
   * @returns {{}}
   */
  static getRecipeFormOptions(clientHeaders = {}) {
    let options = {
      url: ApiConst.ACTION_PRODUCT_RECIPE_UPLOAD,
      method: method,
      headers: {
        'x-allow-upload-errors': 1,
        'x-client-software': ApiConst._CLIENT_SOFTWARE,
        'x-client-version': config.APP_VERSION
      }
    };

    for (let i in clientHeaders) {
      if (clientHeaders.hasOwnProperty(i)) {
        options.headers[i] = clientHeaders[i];
      }
    }

    return options;
  }

  /**
   * @param {{}} requestParams
   * @returns {{}}
   */
  static getBody(requestParams = {}) {
    let params = {};
    for (let paramName in requestParams) {
      if (requestParams.hasOwnProperty(paramName) && paramName !== "headers") {
        params[paramName] = requestParams[paramName];
      }
    }

    return params;
  }

  /**
   * @param {{}} requestParams
   * @returns {string}
   */
  static getBodyJson(requestParams) {
    requestParams = requestParams || {};
    return JSON.stringify(BaseRequest.getBody(requestParams));
  }

  /**
   * @param {{}} requestParams
   * @returns {{}}
   */
  static getHeaders(requestParams) {
    requestParams = requestParams || {};
    return typeof (requestParams.headers) === "undefined" ? {} : requestParams.headers;
  }
}
