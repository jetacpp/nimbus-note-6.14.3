import chalk from "chalk";
import zlib from "zlib";

/**
 * @constructor
 */
export default class BaseResponse {
  /**
   * @type number
   */
  errorCode: number;

  constructor() {
    this.errorCode = null;
  }

  /**
   * @returns {number}
   */
  getErrorCode() {
    return this.errorCode;
  }

  /**
   * @returns {string}
   */
  toJson() {
    return JSON.stringify(this);
  }

  /**
   * @param {*} decoded
   * @returns {{}|string}
   */
  static getDecodedJson(decoded) {
    try {
      let responseString = decoded.toString();
      let parsedString = JSON.parse(responseString);
      return parsedString ? parsedString : '';
    } catch (err) {
      return '';
    }
  }

  /**
   * @param {Buffer|[]} chunks
   * @param {string} encoding
   * @param {Object} responseClass
   * @param {Function} callback
   */
  static get(chunks, encoding, responseClass, callback) {
    let buffer = Buffer.concat(chunks);
    if (encoding === 'gzip') {
      zlib.gunzip(buffer, function (err, decoded) {
        let response = new responseClass(BaseResponse.getDecodedJson(decoded));
        callback(response.getErrorCode(), response);
      });
    } else if (encoding === 'deflate') {
      zlib.inflate(buffer, function (err, decoded) {
        let response = new responseClass(BaseResponse.getDecodedJson(decoded));
        callback(response.getErrorCode(), response);
      });
    } else {
      let response = new responseClass(BaseResponse.getDecodedJson(buffer));
      callback(response.getErrorCode(), response);
    }
  }
}
