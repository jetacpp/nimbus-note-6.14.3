import util from "util";

import {default as config} from "../../../../config";

const BASE_DOMAIN = getBaseDomainFromJNI();
const BASE_URL = getBaseUrlFromJNI();
const BASE_AUTH_DATA = getBaseAuthDataFromJNI();
const BASE_BILLING_URL = getBillingUrlFromJNI();
const BASE_TRIAL_URL = getTrialUrlFromJNI();

const _CLIENT_SOFTWARE = getClientSoftwareFromJNI();
const SERVICE = getServiceNameFromJNI();

//user api
const ACTION_USER_AUTH = getActionUserAuthFromJNI();
const ACTION_USER_REGISTER = getActionUserRegisterFromJNI();
const ACTION_USER_INFO = getActionUserInfoFromJNI();
const ACTION_UPDATE_USER_INFO = getActionUpdateUserInfoFromJNI();
const ACTION_USER_VARIABLES = getActionUserVariablesFromJNI();
const ACTION_UPDATE_USER_VARIABLES = getActionUpdateUserVariablesFromJNI();
const ACTION_USER_GET_CHALLENGE = getActionUserGetChallengeFromJNI();
const ACTION_USER_CHANGE_PASSWORD = getActionUserChangePasswordFromJNI();
const ACTION_USER_REMIND_PASSWORD = getActionUserRemindPasswordFromJNI();
const ACTION_USER_LOGOUT = getActionUserLogoutFromJNI();

//notes api
const ACTION_NOTES_GET = getActionNotesGetFromJNI();
const ACTION_NOTES_UPDATE = getActionNotesUpdateFromJNI();
const ACTION_NOTES_SEARCH = getActionNotesSearchFromJNI();
const ACTION_NOTES_GET_FOLDERS = getActionNotesGetFoldersFromJNI();
const ACTION_NOTES_GET_TAGS = getActionNotesGetTagsFromJNI();
const ACTION_NOTES_SHARE = getActionNotesShareFromJNI();
const ACTION_NOTES_UNSHARE = getActionNotesUnshareFromJNI();
const ACTION_NOTES_ACCOUNT = getActionNotesAccountFromJNI();
const ACTION_NOTES_GET_BACKUPS = getActionNotesGetBackupsFromJNI();
const ACTION_NOTES_VIEW_BACKUP = getActionNotesViewBackupFromJNI();
const ACTION_NOTES_RESTORE_BACKUP = getActionNotesRestoreBackupFromJNI();
const ACTION_NOTES_INVITE = getActionNotesInviteFromJNI();
const ACTION_NOTES_ACCEPT_INVITE = getActionNotesAcceptInviteFromJNI();

//annotations api
const ACTION_NOTES_ANNOTATE = getActionNotesAnnotateFromJNI();
const ACTION_NOTES_GET_ANNOTATIONS = getActionNotesGetAnnotationsFromJNI();

// organizations api
const ACTION_ORGANIZATIONS_GET = getActionOrganizationsGetFromJNI();

//workspaces api
const ACTION_WORKSPACES_GET = getActionWorkspacesGetFromJNI();
const ACTION_WORKSPACE_GET = getActionWorkspaceGetFromJNI();
const ACTION_WORKSPACE_CREATE = getActionWorkspaceCreateFromJNI();
const ACTION_WORKSPACE_UPDATE = getActionWorkspaceUpdateFromJNI();
const ACTION_WORKSPACE_DELETE = getActionWorkspaceDeleteFromJNI();
const ACTION_WORKSPACE_MEMBERS_GET = getActionWorkspaceMembersGetFromJNI();
const ACTION_WORKSPACE_MEMBER_ADD = getActionWorkspaceMemberAddFromJNI();
const ACTION_WORKSPACE_MEMBER_UPDATE = getActionWorkspaceMemberUpdateFromJNI();
const ACTION_WORKSPACE_MEMBER_DELETE = getActionWorkspaceMemberDeleteFromJNI();
const ACTION_WORKSPACE_INVITES_GET = getActionWorkspaceInvitesGetFromJNI();
const ACTION_WORKSPACE_INVITE_RESEND = getActionWorkspaceInviteResendFromJNI();
const ACTION_WORKSPACE_INVITE_UPDATE = getActionWorkspaceInviteUpdateFromJNI();
const ACTION_WORKSPACE_INVITE_DELETE = getActionWorkspaceInviteDeleteFromJNI();
const ACTION_WORKSPACES_SYNC_STATE = getActionWorkspacesSyncStateFromJNI();
const ACTION_MOVE_TO_WORKSPACE = getActionMoveToWorkspaceFromJNI();
const ACTION_GET_NOTES_TEXT_TOKEN_UPDATE_REQUEST = getActionNotesTextTokenUpdateFromJNI();

//files api
const ACTION_FILES_PREUPLOAD = "files:preupload";
const ACTION_FILES_TEMP_EXISTS = getActionFilesTempExistsFromJNI();

//products api
const ACTION_PRODUCT_RECIPE_UPLOAD = getActionProductRecipeFromJNI();

/**
 * @param {{productId:string, token:string}} inputData
 * @returns {string}
 */
function getBillingUrl(inputData) {
  const {productId, token} = inputData;
  return util.format(BASE_BILLING_URL, productId, token);
}

/**
 * @returns {string}
 */
function getTrialUrl() {
  return util.format(BASE_TRIAL_URL);
}

function getBaseDomainFromJNI() {
  return config.syncApiServiceDomain;
}

function getBaseUrlFromJNI() {
  return "https://" + config.syncApiServiceDomain;
}

function getBaseAuthDataFromJNI() {
  return {
    host: config.authApiServiceDomain,
    port: config.authApiServicePort
  };
}

function getBillingUrlFromJNI() {
  return "https://everhelper.me/billing/wh/gplay.php?subscriptionId=%s&token=%s";
}

function getTrialUrlFromJNI() {
  return `${config.trialApiService}/trial`;
}

function getClientSoftwareFromJNI() {
  return process.platform !== 'darwin' ? "win_notes" : "mac_notes";
}

function getServiceNameFromJNI() {
  return "nimbus";
}

function getActionUserAuthFromJNI() {
  return config.API_AUTH_LOGIN_PATH;
}

function getActionUserRegisterFromJNI() {
  return config.API_AUTH_REGISTER_PATH;
}

function getActionUserInfoFromJNI() {
  return "user:info";
}

function getActionUpdateUserInfoFromJNI() {
  return "user:update";
}

function getActionUserVariablesFromJNI() {
  return "user:getVars";
}

function getActionUpdateUserVariablesFromJNI() {
  return "user:setVar";
}

function getActionUserGetChallengeFromJNI() {
  return config.API_AUTH_CHALLENGE_PATH;
}

function getActionUserRemindPasswordFromJNI() {
  return config.API_AUTH_REMIND_PATH;
}

function getActionUserChangePasswordFromJNI() {
  return "user_change_password";
}

function getActionUserLogoutFromJNI() {
  return "user:logout";
}

function getActionNotesGetFromJNI() {
  return "notes:get";
}

function getActionNotesUpdateFromJNI() {
  return "notes:update";
}

function getActionNotesSearchFromJNI() {
  return "notes:search";
}

function getActionNotesGetFoldersFromJNI() {
  return "notes:getFolders";
}

function getActionNotesGetTagsFromJNI() {
  return "notes:getTags";
}

function getActionNotesShareFromJNI() {
  return "notes:share";
}

function getActionNotesUnshareFromJNI() {
  return "notes:unshare";
}

function getActionNotesAccountFromJNI() {
  return "notes:account";
}

function getActionNotesGetBackupsFromJNI() {
  return "notes:getBackups";
}

function getActionNotesViewBackupFromJNI() {
  return "notes:viewBackup";
}

function getActionNotesRestoreBackupFromJNI() {
  return "notes:restoreBackup";
}

function getActionNotesInviteFromJNI() {
  return "notes:invite";
}

function getActionNotesAcceptInviteFromJNI() {
  return "notes:acceptInvite";
}

function getActionNotesAnnotateFromJNI() {
  return "notes:annotate";
}

function getActionNotesGetAnnotationsFromJNI() {
  return "notes:getAnnotations";
}

function getActionFilesTempExistsFromJNI() {
  return "files:tempExists";
}

function getActionProductRecipeFromJNI() {
  return "https://everhelper.me/billing/wh/appstore.php";
}

function getActionOrganizationsGetFromJNI() {
  return "orgs:getAll";
}

function getActionWorkspacesGetFromJNI() {
  return "notes:getWorkspaces";
}

function getActionWorkspaceGetFromJNI() {
  return "notes:getWorkspace";
}

function getActionWorkspaceCreateFromJNI() {
  return "notes:createWorkspace";
}

function getActionWorkspaceUpdateFromJNI() {
  return "notes:updateWorkspace";
}

function getActionWorkspaceDeleteFromJNI() {
  return "notes:deleteWorkspace";
}

function getActionWorkspaceMembersGetFromJNI() {
  return "notes:getWorkspaceMembers";
}

function getActionWorkspaceMemberAddFromJNI() {
  return "notes:addWorkspaceMember";
}

function getActionWorkspaceMemberUpdateFromJNI() {
  return "notes:updateWorkspaceMember";
}

function getActionWorkspaceMemberDeleteFromJNI() {
  return "notes:deleteWorkspaceMember";
}

function getActionWorkspaceInvitesGetFromJNI() {
  return "notes:getWorkspaceInvites";
}

function getActionWorkspaceInviteResendFromJNI() {
  return "notes:resendWorkspaceInvite";
}

function getActionWorkspaceInviteUpdateFromJNI() {
  return "notes:updateWorkspaceInvite";
}

function getActionWorkspaceInviteDeleteFromJNI() {
  return "notes:deleteWorkspaceInvite";
}

function getActionWorkspacesSyncStateFromJNI() {
  return "notes:syncState";
}

function getActionMoveToWorkspaceFromJNI() {
  return "notes:moveToWorkspace";
}

function getActionNotesTextTokenUpdateFromJNI() {
  return "notes:issueTextToken";
}

export default {
  BASE_DOMAIN,
  BASE_URL,
  BASE_AUTH_DATA,
  BASE_BILLING_URL,
  _CLIENT_SOFTWARE,
  SERVICE,
  ACTION_USER_AUTH,
  ACTION_USER_REGISTER,
  ACTION_USER_INFO,
  ACTION_UPDATE_USER_INFO,
  ACTION_USER_VARIABLES,
  ACTION_UPDATE_USER_VARIABLES,
  ACTION_USER_GET_CHALLENGE,
  ACTION_USER_CHANGE_PASSWORD,
  ACTION_USER_REMIND_PASSWORD,
  ACTION_USER_LOGOUT,
  ACTION_NOTES_GET,
  ACTION_NOTES_UPDATE,
  ACTION_NOTES_SEARCH,
  ACTION_NOTES_GET_FOLDERS,
  ACTION_NOTES_GET_TAGS,
  ACTION_NOTES_SHARE,
  ACTION_NOTES_UNSHARE,
  ACTION_NOTES_ACCOUNT,
  ACTION_NOTES_GET_BACKUPS,
  ACTION_NOTES_VIEW_BACKUP,
  ACTION_NOTES_RESTORE_BACKUP,
  ACTION_NOTES_INVITE,
  ACTION_NOTES_ACCEPT_INVITE,
  ACTION_NOTES_ANNOTATE,
  ACTION_NOTES_GET_ANNOTATIONS,
  ACTION_FILES_PREUPLOAD,
  ACTION_FILES_TEMP_EXISTS,
  ACTION_PRODUCT_RECIPE_UPLOAD,
  ACTION_ORGANIZATIONS_GET,
  ACTION_WORKSPACES_GET,
  ACTION_WORKSPACE_GET,
  ACTION_WORKSPACE_CREATE,
  ACTION_WORKSPACE_UPDATE,
  ACTION_WORKSPACE_DELETE,
  ACTION_WORKSPACE_MEMBERS_GET,
  ACTION_WORKSPACE_MEMBER_ADD,
  ACTION_WORKSPACE_MEMBER_UPDATE,
  ACTION_WORKSPACE_MEMBER_DELETE,
  ACTION_WORKSPACE_INVITES_GET,
  ACTION_WORKSPACE_INVITE_RESEND,
  ACTION_WORKSPACE_INVITE_UPDATE,
  ACTION_WORKSPACE_INVITE_DELETE,
  ACTION_WORKSPACES_SYNC_STATE,
  ACTION_MOVE_TO_WORKSPACE,
  ACTION_GET_NOTES_TEXT_TOKEN_UPDATE_REQUEST,

  getBillingUrl,
  getTrialUrl,
};