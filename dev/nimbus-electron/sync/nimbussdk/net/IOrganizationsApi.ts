import {default as Base_Request} from "./common/Base_Request";

import {default as OrganizationsGetResponse} from "./response/OrganizationsGetResponse";

/**
 * @constructor
 */
export default class IOrganizationsApi {
    organizationsGet(sessionId, token, req, callback) {
        Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), OrganizationsGetResponse, callback)
    };
}
