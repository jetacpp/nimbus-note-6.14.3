import {default as RuntimeException} from "./RuntimeException";

export default class NoteIsInTrashException extends RuntimeException {
  /**
   * @type string
   */
  globalId;

  /**
   * @param {string} globalId
   */
  constructor(globalId) {
    super("NoteIsInTrashException");
    this.globalId = globalId;
  }

  /**
   * @returns {string}
   */
  getGlobalId() {
    return this.globalId;
  }
}

