export default class RuntimeException {
  /**
   * @type string
   */
  message;

  /**
   * @param {string} message
   */
  constructor(message) {
    this.message = "RuntimeException: " + message;
  }

  /**
   * @returns {string}
   */
  getMessage() {
    return this.message;
  }
}