import {default as Exception} from './Exception';

export default class NoNeedDownloadSyncException extends Exception {
  constructor() {
    super("NoNeedDownloadSyncException");
    console.log("NoNeedDownloadSyncException");
  }
}