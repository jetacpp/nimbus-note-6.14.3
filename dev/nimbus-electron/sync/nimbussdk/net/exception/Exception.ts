export default class Exception {
  /**
   * @type string
   */
  message;

  /**
   * @param {string} message
   */
  constructor(message) {
    this.message = "Exception: " + message;
  }

  /**
   * @returns {string}
   */
  getMessage() {
    return this.message;
  }
}