import {default as config} from "../../../../../../config";
import {default as errorHandler} from "../../../../../utilities/errorHandler";

import {default as ErrorCodes} from "./ErrorCodes";
import {default as NimbusError} from "./NimbusError";
import {default as NimbusException} from "./NimbusException";

export default class NimbusErrorHandler {
  /**
   * @param {{response:Response, functionName:string}} inputData
   * @return {{}}
   */
  static throwNimbusApiErrorIfExist(inputData) {
    let {response, functionName} = inputData;
    functionName = functionName || '';

    let errorCode = response ? response.errorCode : ErrorCodes.UNKNOWN_ERROR;
    if (errorCode !== ErrorCodes.OK) {

      if (config.SHOW_WEB_CONSOLE) {
        console.log("throwNimbusApiErrorIfExist", response);
      }

      //NimbusErrorHandler.throwExceptionByErrorCode(errorCode);

      let errMessage = '';
      if (!response) {
        errMessage = `Api ${functionName} has empty response`;
        errorHandler.log(errMessage);
        return true;
      } else if (response.errorCode === null) {
        try {
          errMessage = `Api ${functionName} has error code null and response: ${JSON.stringify(response)}`;
        } catch (err) {
          errMessage = `Api ${functionName} response error code is null`;
        }
        errorHandler.log(errMessage);
        return true;
      } else {
        try {
          errMessage = `Api ${functionName} has bad error code and response: ${JSON.stringify(response)}`;
        } catch (err) {
          errMessage = `Api ${functionName} response error body is not an object`;
        }
        errorHandler.log(errMessage);
      }
    }

    return response.errorCode;
  }

  /**
   * @param {number} errorCode
   */
  static throwExceptionByErrorCode(errorCode) {
    throw new NimbusException(NimbusErrorHandler.getExceptionEnumByErrorCode(errorCode));
  }

  /**
   * @param {int} errorCode
   * @returns {int}
   */
  static getExceptionEnumByErrorCode(errorCode) {
    let nimbusError = NimbusError.UNKNOWN_ERROR;
    switch (errorCode) {
      case ErrorCodes.NOT_WELL_FORMED: {
        nimbusError = NimbusError.NOT_WELL_FORMED;
        break;

      }
      case ErrorCodes.ACTION_PARAM_IS_MISSED: {
        nimbusError = NimbusError.ACTION_PARAM_IS_MISSED;
        break;

      }
      case ErrorCodes.UNRECOGNIZED_ACTION: {
        nimbusError = NimbusError.UNRECOGNIZED_ACTION;
        break;

      }
      case ErrorCodes.USER_ALREADY_EXISTS: {
        nimbusError = NimbusError.USER_ALREADY_EXISTS;
        break;

      }
      case ErrorCodes.STORAGE_ENGINE_RETURNS_ERROR: {
        nimbusError = NimbusError.STORAGE_ENGINE_RETURNS_ERROR;
        break;

      }
      case ErrorCodes.AUTH_FAILED: {
        nimbusError = NimbusError.AUTH_FAILED;
        break;

      }
      case ErrorCodes.USER_NOT_EXISTS: {
        nimbusError = NimbusError.USER_NOT_EXISTS;
        break;

      }
      case ErrorCodes.INTERNAL_DATA_JSON_MAILFORMED: {
        nimbusError = NimbusError.INTERNAL_DATA_JSON_MAILFORMED;
        break;

      }
      case ErrorCodes.WRONG_ARGUMENTS_COUNT: {
        nimbusError = NimbusError.WRONG_ARGUMENTS_COUNT;
        break;

      }
      case ErrorCodes.INTERNAL_SERVER_ERROR: {
        nimbusError = NimbusError.INTERNAL_SERVER_ERROR;
        break;

      }
      case ErrorCodes.INTERNAL_FILE_SYSTEM_ERROR: {
        nimbusError = NimbusError.INTERNAL_FILE_SYSTEM_ERROR;
        break;

      }
      case ErrorCodes.INTERNAL_SENDMAIL_ERROR: {
        nimbusError = NimbusError.INTERNAL_SENDMAIL_ERROR;
        break;

      }
      case ErrorCodes.TOO_MUCH_REQUESTS: {
        nimbusError = NimbusError.TOO_MUCH_REQUESTS;
        break;

      }
      case ErrorCodes.ACCESS_DENIED: {
        nimbusError = NimbusError.ACCESS_DENIED;
        break;

      }
      case ErrorCodes.MAX_EMAILS_LIMIT_REACHED: {
        nimbusError = NimbusError.MAX_EMAILS_LIMIT_REACHED;
        break;

      }
      case ErrorCodes.DATA_TOO_LARGE: {
        nimbusError = NimbusError.DATA_TOO_LARGE;
        break;

      }
      case ErrorCodes.ALREADY_LOCKED: {
        nimbusError = NimbusError.ALREADY_LOCKED;
        break;

      }
      case ErrorCodes.EXTERNAL_DATA_MALFORMED: {
        nimbusError = NimbusError.EXTERNAL_DATA_MALFORMED;
        break;

      }
      case ErrorCodes.NOT_FOUND: {
        nimbusError = NimbusError.NOT_FOUND;
        break;

      }
      case ErrorCodes.COUNT_ITEMS_QUOTA_EXCEED: {
        nimbusError = NimbusError.COUNT_ITEMS_QUOTA_EXCEED;
        break;

      }
      case ErrorCodes.ITEM_ALREADY_EXISTS: {
        nimbusError = NimbusError.ITEM_ALREADY_EXISTS;
        break;

      }
      case ErrorCodes.TEMPFILE_NOT_FOUND: {
        nimbusError = NimbusError.TEMPFILE_NOT_FOUND;
        break;

      }
      case ErrorCodes.WRONG_CAPTCHA: {
        nimbusError = NimbusError.WRONG_CAPTCHA;
        break;

      }
      case ErrorCodes.USERNAME_ALREADY_EXISTS: {
        nimbusError = NimbusError.USERNAME_ALREADY_EXISTS;
        break;
      }
      case ErrorCodes.ERROR_INVALID_USERNAME: {
        nimbusError = NimbusError.ERROR_INVALID_USERNAME;
        break;
      }
    }
    return nimbusError;
  }
}

