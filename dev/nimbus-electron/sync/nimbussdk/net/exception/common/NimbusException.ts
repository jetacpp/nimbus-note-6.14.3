import {default as NimbusError} from "./NimbusError";

export default class NimbusException {
  /**
   * @type Error
   */
  error;

  /**
   * @param {Error} nimbusError
   */
  constructor(nimbusError) {
    this.error = nimbusError;
    if (this.error)
      new NimbusError("name::" + this.error.toString() + "; errorCode::(" + this.error.getErrorCode() + ")");
  }

  /**
   * @returns {Error}
   */
  getError() {
    return this.error;
  }
}

