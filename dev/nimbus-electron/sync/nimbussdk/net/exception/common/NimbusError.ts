export default class NimbusError {
  static OK = 0;
  static NOT_WELL_FORMED = -1;
  static ACTION_PARAM_IS_MISSED = -2;
  static UNRECOGNIZED_ACTION = -3;
  static USER_ALREADY_EXISTS = -4;
  static STORAGE_ENGINE_RETURNS_ERROR = -5;
  static AUTH_FAILED = -6;
  static USER_NOT_EXISTS = -7;
  static INTERNAL_DATA_JSON_MAILFORMED = -8;
  static WRONG_ARGUMENTS_COUNT = -9;
  static INTERNAL_SERVER_ERROR = -10;
  static INTERNAL_FILE_SYSTEM_ERROR = -11;
  static INTERNAL_SENDMAIL_ERROR = -12;
  static TOO_MUCH_REQUESTS = -13;
  static ACCESS_DENIED = -14;
  static MAX_EMAILS_LIMIT_REACHED = -15;
  static DATA_TOO_LARGE = -16;
  static ALREADY_LOCKED = -17;
  static EXTERNAL_DATA_MALFORMED = -18;
  static NOT_FOUND = -19;
  static COUNT_ITEMS_QUOTA_EXCEED = -20;
  static ITEM_ALREADY_EXISTS = -21;
  static TEMPFILE_NOT_FOUND = -23;
  static WRONG_CAPTCHA = -1001;
  static USERNAME_ALREADY_EXISTS = -24;
  static ERROR_INVALID_USERNAME = -25;
  static UNKNOWN_ERROR = -1000;

  /**
   * @type int
   */
  errorCode;

  /**
   * @param {int} errorCode
   */
  constructor(errorCode) {
    this.errorCode = errorCode;
  }

  /**
   * @return int
   */
  getErrorCode() {
    return this.errorCode;
  }
}