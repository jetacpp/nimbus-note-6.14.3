import {isNull} from "util";
import {default as NimbusSDK} from "./NimbusSDK";
import {default as API} from './API';

import {default as IUserApi} from "./IUserApi";
import {default as IFilesApi} from "./IFilesApi";
import {default as INotesApi} from "./INotesApi";
import {default as IWorkspacesApi} from "./IWorkspacesApi";
import IOrganizationsApi from "./IOrganizationsApi";

/**
 * @type {API}
 */
let api = null;

export default class NimbusApiProvider {
  /**
   * @param {NimbusSDK} NimbusSdkModule
   */
  public static setup(NimbusSdkModule) {
    api = new API(new IUserApi(), new IFilesApi(), new INotesApi(), new IWorkspacesApi(), new IOrganizationsApi(), NimbusSdkModule);
  }

  /**
   * @param {NimbusSDK} NimbusSdkModule
   * @return API
   */
  public static getApi(NimbusSdkModule) {
    if (isNull(api)) {
      NimbusApiProvider.setup(NimbusSdkModule);
    }
    return api;
  }
}
