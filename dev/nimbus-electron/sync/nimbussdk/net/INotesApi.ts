import {default as Base_Request} from "./common/Base_Request";

import {default as NotesAccountResponse} from "./response/NotesAccountResponse";
import {default as NotesGetStructureResponse} from "./response/NotesGetStructureResponse";
import {default as NotesTotalAmountResponse} from "./response/NotesTotalAmountResponse";
import {default as GetRemovedItemsResponse} from "./response/GetRemovedItemsResponse";
import {default as NotesGetAllResponse} from "./response/NotesGetAllResponse";
import {default as NotesSearchResponse} from "./response/NotesSearchResponse";
import {default as NotesGetFoldersResponse} from "./response/NotesGetFoldersResponse";
import {default as NotesGetTagsResponse} from "./response/NotesGetTagsResponse";
import {default as NotesShareResponse} from "./response/NotesShareResponse";
import {default as NotesUnshareResponse} from "./response/NotesUnshareResponse";
import {default as NotesGetBackupsResponse} from "./response/NotesGetBackupsResponse";
import {default as NotesViewBackupResponse} from "./response/NotesViewBackupResponse";
import {default as NotesRestoreBackupResponse} from "./response/NotesRestoreBackupResponse";
import {default as NotesInviteResponse} from "./response/NotesInviteResponse";
import {default as NotesAcceptInviteResponse} from "./response/NotesAcceptInviteResponse";
import {default as NoteIsExistOnServerResponse} from "./response/NoteIsExistOnServerResponse";
import {default as NotesUpdateResponse} from "./response/NotesUpdateResponse";
import {default as NotesTextTokenUpdateResponse} from "./response/NotesTextTokenUpdateResponse";
import {default as NotesAnnotateResponse} from "./response/NotesAnnotateResponse";

export default class INotesApi {
  getStructureNotes(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesGetStructureResponse, callback)
  };

  getNotesTotalAmount(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesTotalAmountResponse, callback)
  };

  getRemovedItems(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), GetRemovedItemsResponse, callback)
  };

  getFullNotes(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesGetAllResponse, callback)
  };

  notesSearch(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesSearchResponse, callback)
  };

  getFolders(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesGetFoldersResponse, callback)
  };

  getTags(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesGetTagsResponse, callback)
  };

  shareNotes(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesShareResponse, callback)
  };

  unshareNotes(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesUnshareResponse, callback)
  };

  account(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesAccountResponse, callback)
  };

  notesBackup(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesGetBackupsResponse, callback)
  };

  viewBackup(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesViewBackupResponse, callback)
  };

  restoreBackup(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesRestoreBackupResponse, callback)
  };

  invite(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesInviteResponse, callback)
  };

  acceptInvite(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesAcceptInviteResponse, callback)
  };

  checkIfNoteExistOnServer(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NoteIsExistOnServerResponse, callback)
  };

  updateNotes(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesUpdateResponse, callback)
  };

  getNoteTextToken(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesTextTokenUpdateResponse, callback)
  }

  notesAnnotate(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), NotesAnnotateResponse, callback)
  }
}

