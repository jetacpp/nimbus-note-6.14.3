import {default as Base_Request} from "./common/Base_Request";

import {default as WorkspacesGetResponse} from "./response/WorkspacesGetResponse";
import {default as WorkspaceGetResponse} from "./response/WorkspaceGetResponse";
import {default as WorkspaceCreateResponse} from "./response/WorkspaceCreateResponse";
import {default as WorkspaceUpdateResponse} from "./response/WorkspaceUpdateResponse";
import {default as WorkspaceDeleteResponse} from "./response/WorkspaceDeleteResponse";
import {default as WorkspaceMembersGetResponse} from "./response/WorkspaceMembersGetResponse";
import {default as WorkspaceMemberAddResponse} from "./response/WorkspaceMemberAddResponse";
import {default as WorkspaceMemberUpdateResponse} from "./response/WorkspaceMemberUpdateResponse";
import {default as WorkspaceMemberDeleteResponse} from "./response/WorkspaceMemberDeleteResponse";
import {default as WorkspaceInvitesGetResponse} from "./response/WorkspaceInvitesGetResponse";
import {default as WorkspaceInviteResendResponse} from "./response/WorkspaceInviteResendResponse";
import {default as WorkspaceInviteUpdateResponse} from "./response/WorkspaceInviteUpdateResponse";
import {default as WorkspaceInviteDeleteResponse} from "./response/WorkspaceInviteDeleteResponse";
import {default as WorkspaceSyncStateResponse} from "./response/WorkspaceSyncStateResponse";
import {default as MoveToWorkspaceResponse} from "./response/MoveToWorkspaceResponse";

/**
 * @constructor
 */
export default class IWorkspacesApi {
  workspacesGet(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspacesGetResponse, callback)
  };

  workspaceGet(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceGetResponse, callback)
  };

  workspaceCreate(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceCreateResponse, callback)
  };

  workspaceUpdate(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceUpdateResponse, callback)
  };

  workspaceDelete(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceDeleteResponse, callback)
  };

  workspaceMembersGet(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceMembersGetResponse, callback)
  };

  workspaceMemberAdd(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceMemberAddResponse, callback)
  };

  workspaceMemberUpdate(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceMemberUpdateResponse, callback)
  };

  workspaceMemberDelete(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceMemberDeleteResponse, callback)
  };

  workspaceInvitesGet(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceInvitesGetResponse, callback)
  };

  workspaceInviteResend(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceInviteResendResponse, callback)
  };

  workspaceInviteUpdate(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceInviteUpdateResponse, callback)
  };

  workspaceInviteDelete(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceInviteDeleteResponse, callback)
  };

  workspaceSyncState(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), WorkspaceSyncStateResponse, callback)
  };

  moveToWorkspace(sessionId, token, req, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, req), MoveToWorkspaceResponse, callback)
  }
}
