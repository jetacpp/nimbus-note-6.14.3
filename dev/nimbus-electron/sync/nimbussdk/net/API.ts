import chalk from "chalk";
import fetch from "node-fetch";
import fs from "fs-extra";
import {default as ApiConst} from "./ApiConst";
import {default as Session} from "../manager/Session";
import {default as SyncStatusChangedEvent} from "../../process/events/SyncStatusChangedEvent";
import {default as NimbusErrorHandler} from "./exception/common/NimbusErrorHandler";
import {default as NoteIsInTrashException} from "./exception/NoteIsInTrashException";
import {default as errorHandler} from "../../../utilities/errorHandler";
import {default as AccountManager} from "../manager/AccountManager";

import {default as SignInRequest} from "./request/SignInRequest";
import {default as RegisterNewUserRequest} from "./request/RegisterNewUserRequest";
import {default as NotesAccountRequest} from "./request/NotesAccountRequest";
import {default as UserInfoRequest} from "./request/UserInfoRequest";
import {default as UpdateUserInfoRequest} from "./request/UpdateUserInfoRequest";
import {default as UserVariablesRequest} from "./request/UserVariablesRequest";
import {default as UpdateUserVariableRequest} from "./request/UpdateUserVariableRequest";
import {default as UserChangePasswordRequest} from "./request/UserChangePasswordRequest";
import {default as RemindPasswordRequest} from "./request/RemindPasswordRequest";
//import {default as LogoutRequest} from "./request/LogoutRequest";
import {default as FilesTempExistsRequest} from "./request/FilesTempExistsRequest";
import {default as FilesPreuploadRequest} from "./request/FilesPreuploadRequest";
import {default as ProductRecipeUploadRequest} from "./request/ProductRecipeUploadRequest";
import {default as NotesGetStructureRequest} from "./request/NotesGetStructureRequest";
import {default as NotesTotalAmountRequest} from "./request/NotesTotalAmountRequest";
import {default as GetRemovedItemsRequest} from "./request/GetRemovedItemsRequest";
import {default as NotesGetAllRequest} from "./request/NotesGetAllRequest";
import {default as NotesSearchRequest} from "./request/NotesSearchRequest";
import {default as NotesGetFoldersRequest} from "./request/NotesGetFoldersRequest";
import {default as NotesGetTagsRequest} from "./request/NotesGetTagsRequest";
import {default as NotesShareRequest} from "./request/NotesShareRequest";
import {default as NotesUnshareRequest} from "./request/NotesUnshareRequest";
import {default as NotesGetBackupsRequest} from "./request/NotesGetBackupsRequest";
import {default as NotesViewBackupRequest} from "./request/NotesViewBackupRequest";
import {default as NotesRestoreBackupRequest} from "./request/NotesRestoreBackupRequest";
import {default as NotesInviteRequest} from "./request/NotesInviteRequest";
import {default as NotesAcceptInviteRequest} from "./request/NotesAcceptInviteRequest";
import {default as NoteIsExistOnServerRequest} from "./request/NoteIsExistOnServerRequest";
import {default as NotesUpdateRequest} from "./request/NotesUpdateRequest";
import {default as OrganizationsGetRequest} from "./request/OrganizationsGetRequest";
import {default as WorkspacesGetRequest} from "./request/WorkspacesGetRequest";
import {default as WorkspaceGetRequest} from "./request/WorkspaceGetRequest";
import {default as WorkspaceCreateRequest} from "./request/WorkspaceCreateRequest";
import {default as WorkspaceUpdateRequest} from "./request/WorkspaceUpdateRequest";
import {default as WorkspaceDeleteRequest} from "./request/WorkspaceDeleteRequest";
import {default as WorkspaceMembersGetRequest} from "./request/WorkspaceMembersGetRequest";
import {default as WorkspaceMemberAddRequest} from "./request/WorkspaceMemberAddRequest";
import {default as WorkspaceMemberUpdateRequest} from "./request/WorkspaceMemberUpdateRequest";
import {default as WorkspaceMemberDeleteRequest} from "./request/WorkspaceMemberDeleteRequest";
import {default as WorkspaceInvitesGetRequest} from "./request/WorkspaceInvitesGetRequest";
import {default as WorkspaceInviteResendRequest} from "./request/WorkspaceInviteResendRequest";
import {default as WorkspaceInviteUpdateRequest} from "./request/WorkspaceInviteUpdateRequest";
import {default as WorkspaceInviteDeleteRequest} from "./request/WorkspaceInviteDeleteRequest";
import {default as WorkspaceSyncStateRequest} from "./request/WorkspaceSyncStateRequest";
import {default as MoveToWorkspaceRequest} from "./request/MoveToWorkspaceRequest";
import {default as NotesTextTokenUpdateRequest} from "./request/NotesTextTokenUpdateRequest";
import {default as NotesAnnotateRequest} from "./request/NotesAnnotateRequest";
import {default as NotesGetAnnotationsRequest} from "./request/NotesGetAnnotationsRequest";
import {default as ChallengeRequest} from "./request/ChallengeRequest";

import {default as IUserApi} from "./IUserApi";
import {default as IFilesApi} from "./IFilesApi";
import {default as INotesApi} from "./INotesApi";
import {default as IWorkspacesApi} from "./IWorkspacesApi";
import {default as IOrganizationsApi} from "./IOrganizationsApi";
import {default as appWindow} from "../../../window/instance";
import apiMeLogout from "../../../request/interceptors/jsonInterceptors/me/apiMeLogout";
import {default as appOnlineState} from "../../../online/state";


/**
 * @type {NimbusSDK}
 */
let NimbusLibSDK = null;

/**
 * @param {IUserApi} userApi
 * @param {IFilesApi} filesApi
 * @param {INotesApi} notesApi
 * @param {NimbusSDK} NimbusSdkModule
 * @constructor
 */
export default class API {
  static ERROR_ACCESS_DENIED = -14;
  static ERROR_QUOTA_EXCEED = -20;
  static ERROR_USER_NOT_AUTHORIZED = -6;
  static ERROR_TEMPFILE_NOT_FOUND = -23;
  static ERROR_ATTACHMENT_NOT_FOUND = -19;

  static DESC_DEFAULT_QUOTA_EXCEED = 'default_quota_exceed';
  static DESC_USAGE_QUOTA_EXCEED = 'usage_quota_exceed';
  static DESC_TOO_MANY_WORKSPACES = 'too_many_workspaces';
  static DESC_TOO_MANY_WORKSPACE_MEMBERS = 'too_many_workspace_members';
  static DESC_TOO_MANY_WORKSPACE_INVITES = 'too_many_workspace_invites';
  static DESC_TOO_MANY_NOTES = 'too_many_notes';

  userApi: IUserApi;
  filesApi: IFilesApi;
  notesApi: INotesApi;
  workspacesApi: IWorkspacesApi;
  organizationsApi: IOrganizationsApi;

  constructor(userApi, filesApi, notesApi, workspacesApi, organizationsApi, NimbusSdkModule) {
    this.userApi = userApi;
    this.filesApi = filesApi;
    this.notesApi = notesApi;
    this.workspacesApi = workspacesApi;
    this.organizationsApi = organizationsApi;
    NimbusLibSDK = NimbusSdkModule;
  }

  static checkSignInStatus(err, errSession) {
    if ((err === API.ERROR_USER_NOT_AUTHORIZED) && appWindow.get() && appOnlineState.get()) {
      apiMeLogout({}, () => {});
    }
  }

  /**
   * @param {string} login
   * @param {string} password
   * @param {Function} callback
   */
  signIn(login, password, callback = (err, res) => {
  }) {
    let self = this;
    self.userApi.signIn(new SignInRequest(login, password), (err, response) => {
      if (err || !response) {
        errorHandler.log({
          err: err, response: response,
          description: "API problem => signIn",
          data: {
            login,
            password
          }
        });
        return callback(err, response);
      }

      if (!response.body || !Object.keys(response.body).length) {
        errorHandler.log({
          err: err, response: response,
          description: "API problem => signIn",
          data: {
            login,
            password
          }
        });
        return callback(err, response);
      }

      self.auth(login, response.body.sessionId, callback);
    });
  }

  auth(login, sessionId, callback = (err, res) => {
  }) {
    let self = this;
    let accountManager = NimbusLibSDK.getAccountManager();
    accountManager.setUniqueUserName(login, () => {
      let session = new Session(login, sessionId);
      NimbusLibSDK.getAccountManager().setAccountSession(session, () => {
        self.userInfo({skipSyncHandlers: true}, (err, userInfo) => {
          if (err) {
            errorHandler.log({
              err: err, response: userInfo,
              description: "API problem => signIn => userInfo"
            });
            return callback(err, {});
          }

          accountManager.setUserLogin(userInfo.login);
          accountManager.setUserId(userInfo.user_id);
          accountManager.setUserFirstName(userInfo.firstname);
          accountManager.setUserLastName(userInfo.lastname);
          accountManager.setUserAvatar(userInfo.avatar);
          accountManager.setPremiumActive(userInfo.premium.active);
          accountManager.setPremiumStartDate(userInfo.premium.start_date);
          accountManager.setPremiumEndDate(userInfo.premium.end_date);
          accountManager.setPremiumSource(userInfo.premium.source);
          accountManager.setRegisterDate(userInfo.register_date);
          accountManager.setDaysToQuotaReset(userInfo.days_to_quota_reset);
          accountManager.setTrafficMax(userInfo.usage.notes.max);
          accountManager.setTrafficCurrent(userInfo.usage.notes.current);
          accountManager.setLimitNotesMaxSize(userInfo.limits.NOTES_MAX_SIZE);
          accountManager.setLimitNotesMonthUsageQuota(userInfo.limits.NOTES_MONTH_USAGE_QUOTA);
          accountManager.setLimitNotesMaxAttachmentSize(userInfo.limits.NOTES_MAX_ATTACHMENT_SIZE);

          let signInData = userInfo;
          self.account((err, accountInfo) => {
            if (err) {
              errorHandler.log({
                err: err, response: accountInfo,
                description: "API problem => signIn => account"
              });
              return callback(err, signInData);
            }

            if (accountInfo && accountInfo.notes_email) {
              signInData.notes_email = accountInfo.notes_email;
            }

            callback(err, signInData);
          });
        });
      });
    });
  }

  /**
   * @param {string} login
   * @param {string} password
   * @param {Function} callback
   */
  signUp(login, password, callback = (err, res) => {
  }) {
    let self = this;
    let language = "en";
    if (language !== null && language.length > 2) {
      language = language.substring(0, 2);
    }
    self.userApi.signUp(new RegisterNewUserRequest(login, password, ApiConst.SERVICE, language), (err, response) => {
      if (err) {
        errorHandler.log({
          err: err, response: response,
          description: "API problem => signUp",
          data: {
            login,
            password
          }
        });
        return callback(err, response);
      }

      self.signIn(login, password, callback);
    });
  }

  /**
   * @param {Function} callback
   */
  userLogin(callback = (err, res) => {
  }) {
    NimbusLibSDK.getAccountManager().getUserLogin((err, login) => {
      if (err || !login) {
        errorHandler.log({
          err: err, response: login,
          description: "API problem => userLogin"
        });
        return callback(err, null);
      }

      callback(err, login.toLowerCase());
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  userInfo(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      const errSession = err;
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => userInfo => getAccountSession"
        });
        return callback(err, null);
      }

      self.userApi.userInfo(session.getSessionId(), session.getToken(), new UserInfoRequest(inputData), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => userInfo",
              data: inputData
            });

            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "userInfo"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => userInfo",
              data: inputData
            });
            API.checkSignInStatus(err, errSession);
            return callback(err, null);
          }
        }

        let responseExist = response.body && Object.keys(response.body).length;
        if (responseExist) {
          let accountManager = NimbusLibSDK.getAccountManager();
          let trafficCurrent = response.body.usage.notes.current;
          let trafficMax = response.body.usage.notes.max;
          let daysToReset = response.body.days_to_quota_reset;
          let maxTextSize = response.body.limits.NOTES_MAX_SIZE;
          let maxAttachmentSize = response.body.limits.NOTES_MAX_ATTACHMENT_SIZE;

          /*const orgInstance = await orgs.getByWorkspaceId(null);
          if (orgInstance && orgInstance.usage) {
            if(orgInstance.usage.traffic) {
              const orgTraffic = orgInstance.usage.traffic;
              trafficCurrent = orgTraffic.current;
              trafficMax = orgTraffic.max;
              daysToReset = orgTraffic.daysToReset;
            }

            if(orgInstance.limits) {
              const orgLimits = orgInstance.limits;
              maxTextSize = orgLimits.textSize;
              maxAttachmentSize = orgLimits.attachmentSize;
            }
          }*/

          accountManager.setUserId(response.body.user_id);
          accountManager.setUserLogin(response.body.login);
          accountManager.setPremiumActive(response.body.premium.active);
          accountManager.setPremiumStartDate(response.body.premium.start_date);
          accountManager.setPremiumEndDate(response.body.premium.end_date);
          accountManager.setPremiumSource(response.body.premium.source);
          accountManager.setRegisterDate(response.body.register_date);
          accountManager.setDaysToQuotaReset(daysToReset);
          accountManager.setTrafficMax(trafficMax);
          accountManager.setTrafficCurrent(trafficCurrent);
          accountManager.setLimitNotesMaxSize(maxTextSize);
          accountManager.setLimitNotesMaxAttachmentSize(maxAttachmentSize);
          accountManager.setLimitNotesMonthUsageQuota(response.body.limits.NOTES_MONTH_USAGE_QUOTA);
        }
        let responseData = responseExist ? response.body : null;
        callback(err, responseData);
      });
    });
  }

  /**
   * @param {{workspaceId:string, body: {avatar:{tempFileName:string}, firstname:string, lastname:string, username:string}, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  updateUserInfo(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, body, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => updateUserInfo => getAccountSession"
        });
        return callback(err, null);
      }
      
      self.userApi.updateUserInfo(session.getSessionId(), session.getToken(), new UpdateUserInfoRequest(body), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => updateUserInfo",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "updateUserInfo"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => updateUserInfo",
              data: inputData
            });
            return callback(err, null);
          }
        }
        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  userVariables(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => userVariables => getAccountSession"
        });
        return callback(err, null);
      }

      self.userApi.userVariables(session.getSessionId(), session.getToken(), new UserVariablesRequest(), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => userVariables",
              data: inputData
            });

            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "userVariables"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => userVariables",
              data: inputData
            });

            return callback(err, null);
          }
        }

        if(typeof (response.body.vars) === 'undefined') {
          return callback(err, null);
        }

        callback(err, response.body.vars);
      });
    });
  }

  /**
   * @param {{workspaceId:string, body: {key: string, value: string|number|boolean|object}, skipSyncHandlers: boolean} inputData
   * @param {Function} callback
   */
  updateUserVariable(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, body, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => updateUserVariable => getAccountSession"
        });
        return callback(err, null);
      }

      self.userApi.updateUserVariable(session.getSessionId(), session.getToken(), new UpdateUserVariableRequest(body), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => updateUserVariable",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "updateUserVariable"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => updateUserVariable",
              data: inputData
            });
            return callback(err, null);
          }
        }
        callback(err, response.body);
      });
    });
  }

  /**
   * @param {Function} callback
   */
  account(callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => account => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.account(session.getSessionId(), session.getToken(), new NotesAccountRequest(), async (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "account"})) {
          await SyncStatusChangedEvent.setErrorStatus({err});
          errorHandler.log({
            err: err, response: response,
            description: "API problem => account"
          });
          return callback(err, null);
        }
        let accountManager = NimbusLibSDK.getAccountManager();
        let email1 = response.body.notes_email;
        accountManager.setEmailForShareNotes(email1, function () {
          callback(err, response.body);
        });
      });
    });
  }

  /**
   * @param {{state: string, answer: string}} inputData
   * @param {Function} callback
   */
  challenge(inputData, callback = (err, res) => {
  }) {
    let self = this;
    const {state, answer} = inputData;
    self.userApi.challenge(new ChallengeRequest(state, answer), (err, response) => {
      if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "challenge"})) {
        errorHandler.log({
          err: err, response: response,
          description: "API problem => challenge"
        });

        return callback(err, response);
      }

      callback(null, response);
    });
  }

  /**
   * @param {string} oldPassword
   * @param {string} newPassword
   * @param {Function} callback
   */
  userChangePassword(oldPassword, newPassword, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => userChangePassword => getAccountSession"
        });
        return callback(err, null);
      }

      self.userApi.userChangePassword(session.getSessionId(), session.getToken(), new UserChangePasswordRequest(oldPassword, newPassword), (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "userChangePassword"})) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => userChangePassword",
            data: {
              oldPassword: oldPassword,
              newPassword: newPassword
            }
          });
        }
        callback(err, response);
      });
    });
  }

  /**
   * @param {string} email
   * @param {Function} callback
   */
  remindPassword(email, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => remindPassword => getAccountSession"
        });
        return callback(err, null);
      }

      self.userApi.remindPassword(session.getSessionId(), session.getToken(), new RemindPasswordRequest(email), (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "remindPassword"})) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => remindPassword",
            data: {
              email: email
            }
          });
        }
        callback(err, response);
      });
    });
  }

  /**
   * @param {Function} callback
   */
  logout(callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => logout => getAccountSession"
        });
      }

      self.clearAccountSession((err, response) => {
        if (err) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => clearAccountSession"
          });
        }
        callback(err, response);
      });
    });
  }

  /**
   * @param {Function} callback
   */
  clearAccountSession(callback = (err, res) => {
  }) {
    NimbusLibSDK.getAccountManager().clearAccountSession(callback);
  }

  /**
   * @param {string} fileUrl
   * @param {Function} callback
   */
  productRecipeUpload(fileUrl, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => productRecipeUpload => getAccountSession"
        });
        return callback(err, null);
      }

      self.filesApi.productRecipeUpload(session.getSessionId(), session.getToken(), new ProductRecipeUploadRequest(fileUrl), (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "productRecipeUpload"})) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => productRecipeUpload",
            data: {
              fileUrl: fileUrl
            }
          });
        }
        callback(err, response);
      });
    });
  }

  /**
   * @param {{productId: string, token: string}} inputData
   * @param {Function} callback
   */
  unlockPremium(inputData, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => unlockPremium => getAccountSession"
        });
        return callback(err, null);
      }

      self.userApi.unlockPremium(session.getSessionId(), session.getToken(), ApiConst.getBillingUrl(inputData), (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "unlockPremium"})) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => unlockPremium",
            data: inputData
          });
        }
        callback(err, response);
      });
    });
  }

  /**
   * @param {{provider:string}} inputData
   */
  async oauth(inputData) {
    try {
      let {provider, token} = inputData;
      const url = `https://everhelper.me/auth/openidconnect.php?env=app&provider=${provider}&oauth_email_key=${token}&format=json`;
      const res = await fetch(url, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
      });
      return res.json();
    } catch (error) {
      errorHandler.log(error.message);
      return null;
    }
  }

  /**
   * @param {{workspaceId: string, tempname: string, extension:string, mime: string}} inputData
   * @param {Function} callback
   */
  async filesPreupload(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, tempname} = inputData;
    let self = this;

    // @ts-ignore
    let fileExist = await fs.exists(tempname);
    // @ts-ignore
    if (!fileExist) {
      let err = new Error("filesPreupload => file not exist by path: " + tempname);
      errorHandler.log({
        err: err, response: null,
        description: "API problem => filesPreupload",
        data: inputData
      });
      return callback(err, null);
    }

    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => filesPreupload => getAccountSession"
        });
        return callback(err, null);
      }

      self.filesApi.filesPreupload(session.getSessionId(), session.getToken(), new FilesPreuploadRequest(inputData), async (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "filesPreupload"})) {
          await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
          errorHandler.log({
            err: err, response: response,
            description: "API problem => filesPreupload",
            data: inputData
          });
          return callback(err, null);
        }

        let filePath = null;
        if (response && response.body && response.body.files) {
          if (response.body.files.length && response.body.files[0].file1) {
            filePath = response.body.files[0].file1;
          }
        }

        callback(err, filePath);
      });
    });
  }

  /**
   * @param {{workspaceId:string, tempname:string}} inputData
   * @param {Function} callback
   */
  filesTempExists(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => filesTempExists => getAccountSession"
        });
        return callback(err, null);
      }

      self.filesApi.filesTempExists(session.getSessionId(), session.getToken(), new FilesTempExistsRequest(inputData), async (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "filesTempExists"})) {
          await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
          errorHandler.log({
            err: err, response: response,
            description: "API problem => filesTempExists",
            data: inputData
          });
          return callback(err, null);
        }

        callback(err, response);
      });
    });
  }

  /**
   * @param {{workspaceId:string, start:int, amount:int, globalId:int}} inputData
   * @param {Function} callback
   */
  getStructureNotes(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => getStructureNotes => getAccountSession"
        });
        return callback(err, null);
      }

      AccountManager.getLastUpdateTime(inputData, (err, lastUpdateTime) => {
        if (err) {
          errorHandler.log({
            err: err, response: lastUpdateTime,
            description: "API problem => getStructureNotes => getLastUpdateTime"
          });
          return callback(err, null);
        }

        inputData.lastUpdateTime = lastUpdateTime;
        self.notesApi.getStructureNotes(session.getSessionId(), session.getToken(), new NotesGetStructureRequest(inputData), async (err, response) => {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "getStructureNotes"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => getStructureNotes",
              data: inputData
            });
            return callback(err, null);
          }

          callback(err, response.body);
        });
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  getNotesTotalAmount(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => getNotesTotalAmount => getAccountSession"
        });
        return callback(err, null);
      }

      AccountManager.getLastUpdateTime(inputData, (err, lastUpdateTime) => {
        //err = -9;
        if (err) {
          errorHandler.log({
            err: err, response: lastUpdateTime,
            description: "API problem => getNotesTotalAmount => getLastUpdateTime"
          });
          return callback(err, null);
        }

        inputData.lastUpdateTime = lastUpdateTime;
        self.notesApi.getNotesTotalAmount(session.getSessionId(), session.getToken(), new NotesTotalAmountRequest(inputData), async (err, response) => {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "getNotesTotalAmount"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => getNotesTotalAmount",
              data: inputData
            });
            return callback(err, null);
          }

          callback(err, response.body);
        });
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  getRemovedItems(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => getRemovedItems => getAccountSession"
        });
        return callback(err, null);
      }

      AccountManager.getLastUpdateTime(inputData, (err, lastUpdateTime) => {
        if (err) {
          errorHandler.log({
            err: err, response: lastUpdateTime,
            description: "API problem => getRemovedItems => getLastUpdateTime"
          });
          return callback(err, null);
        }

        inputData.lastUpdateTime = lastUpdateTime;
        self.notesApi.getRemovedItems(session.getSessionId(), session.getToken(), new GetRemovedItemsRequest(inputData), async (err, response) => {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "getRemovedItems"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => getRemovedItems",
              data: inputData
            });
            return callback(err, null);
          }
          callback(err, response.body);
        });
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalIds:[string]}} inputData
   * @param {Function} callback
   */
  getFullNotes(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => getFullNotes => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.getFullNotes(session.getSessionId(), session.getToken(), new NotesGetAllRequest(inputData), async (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "getFullNotes"})) {
          await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
          errorHandler.log({
            err: err, response: response,
            description: "API problem => getFullNotes",
            data: inputData
          });
          return callback(err, null);
        }

        let noteGlobalId = null;
        if (response.body.totalAmount > 0) {
          let note = response.body.notes.get(0);
          noteGlobalId = note.global_id;
          if (!(note.parent_id.toLowerCase() === "JESUS_CHRIST")) {
            return callback(err, response.body);
          }
        }
        throw new NoteIsInTrashException(noteGlobalId);
      });
    });
  }

  /**
   * @param {{workspaceId:string, query:string}} inputData
   * @param {Function} callback
   */
  notesSearch(inputData, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => notesSearch => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.notesSearch(session.getSessionId(), session.getToken(), new NotesSearchRequest(inputData), async (err, response) => {
        if (err) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => notesSearch",
            data: inputData
          });
          return callback(err, null);
        }

        if (response.body.totalAmount === 0) {
          response.body.global_ids = "";
        }
        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  getFolders(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => getFolders => getAccountSession"
        });
        return callback(err, null);
      }

      AccountManager.getLastUpdateTime(inputData, (err, lastUpdateTime) => {
        if (err) {
          errorHandler.log({
            err: err, response: lastUpdateTime,
            description: "API problem => getFolders => getLastUpdateTime"
          });
          return callback(err, null);
        }

        inputData.lastUpdateTime = lastUpdateTime;
        self.notesApi.getFolders(session.getSessionId(), session.getToken(), new NotesGetFoldersRequest(inputData), async (err, response) => {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "getFolders"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => getFolders",
              data: inputData
            });
            return callback(err, null);
          }

          callback(err, response.body.notes);
        });
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  getTags(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => getTags => getAccountSession"
        });
        return callback(err, null);
      }

      AccountManager.getLastUpdateTime(inputData, (err, lastUpdateTime) => {
        if (err) {
          errorHandler.log({
            err: err, response: lastUpdateTime,
            description: "API problem => getTags => getLastUpdateTime"
          });
          return callback(err, null);
        }

        inputData.lastUpdateTime = 0;
        self.notesApi.getTags(session.getSessionId(), session.getToken(), new NotesGetTagsRequest(inputData), async (err, response) => {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "getTags"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => getTags",
              data: inputData
            });
            return callback(err, null);
          }

          callback(err, response.body.tags);
        });
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalIds:[string], password:string, recursive:boolean}} inputData
   * @param {Function} callback
   */
  shareNotes(inputData, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => shareNotes => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.shareNotes(session.getSessionId(), session.getToken(), new NotesShareRequest(inputData), (err, response) => {
        if (err) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => shareNotes",
            data: inputData
          });
          return callback(err, null);
        }
        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalIds:[]}} inputData
   * @param {Function} callback
   */
  unshareNotes(inputData, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => unshareNotes => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.unshareNotes(session.getSessionId(), session.getToken(), new NotesUnshareRequest(inputData), (err, response) => {
        if (err) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => unshareNotes",
            data: inputData
          });
        }
        callback(err, response);
      });
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  notesBackup(inputData, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => notesBackup => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.notesBackup(session.getSessionId(), session.getToken(), new NotesGetBackupsRequest(inputData), (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "notesBackup"})) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => notesBackup",
            data: inputData
          });
        }
        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, backupId:string}} inputData
   * @param {Function} callback
   */
  viewBackup(inputData, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => viewBackup => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.viewBackup(session.getSessionId(), session.getToken(), new NotesViewBackupRequest(inputData), (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "viewBackup"})) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => viewBackup",
            data: inputData
          });
        }
        callback(err, response);
      });
    });
  }

  /**
   * @param {{workspaceId:string, backupId:string, overwrite:boolean, restoreItems: []}} inputData
   * @param {Function} callback
   */
  restoreBackup(inputData, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => restoreBackup => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.restoreBackup(session.getSessionId(), session.getToken(), new NotesRestoreBackupRequest(inputData), (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "restoreBackup"})) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => restoreBackup",
            data: inputData
          });
        }
        callback(err, response);
      });
    });
  }

  /**
   * @param {{workspaceId:string, email:string, items:[], isTestRequest:boolean}} inputData
   * @param {Function} callback
   */
  invite(inputData, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => invite => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.invite(session.getSessionId(), session.getToken(), new NotesInviteRequest(inputData), (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "invite"})) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => invite",
            data: inputData
          });
        }
        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, inviteId:string, items:[]}} inputData
   * @param {Function} callback
   */
  acceptInvite(inputData, callback = (err, res) => {
  }) {
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => acceptInvite => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.acceptInvite(session.getSessionId(), session.getToken(), new NotesAcceptInviteRequest(inputData), (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "acceptInvite"})) {
          errorHandler.log({
            err: err, response: response,
            description: "API problem => acceptInvite",
            data: inputData
          });
        }
        callback(err, response);
      });
    });
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   * @param {Function} callback
   */
  checkIfNoteExistOnServer(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, noteGlobalId} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => checkIfNoteExistOnServer => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.checkIfNoteExistOnServer(session.getSessionId(), session.getToken(), new NoteIsExistOnServerRequest(inputData), async (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "checkIfNoteExistOnServer"})) {
          await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
          errorHandler.log({
            err: err, response: response,
            description: "API problem => checkIfNoteExistOnServer",
            data: inputData
          });
          return callback(err, null);
        }

        if (response.body.totalAmount > 0) {
          let note = response.body.notes.get(0);
          if (!(note.parent_id.toLowerCase() === "trash")) {
            return callback(err, response.body);
          }
        }
        throw new NoteIsInTrashException(noteGlobalId);
      });
    });
  }

  /**
   * @param {{workspaceId:string, body:{}}} inputData
   * @param {Function} callback
   */
  updateNotes(inputData, callback = (err, res) => {
  }) {
    const {workspaceId} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => updateNotes => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.updateNotes(session.getSessionId(), session.getToken(), NotesUpdateRequest.get(inputData), async (err, response) => {
        if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "updateNotes"})) {
          await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
          errorHandler.log({
            err: err, response: response,
            description: "API problem => updateNotes",
            data: inputData
          });
          return callback(err, response);
        }

        let accountManager = NimbusLibSDK.getAccountManager();
        accountManager.setTrafficMax(response.body.usage.max);
        accountManager.setTrafficCurrent(response.body.usage.current);
        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  organizationsGet(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => organizationsGet => getAccountSession"
        });
        return callback(err, null);
      }

      self.organizationsApi.organizationsGet(session.getSessionId(), session.getToken(), new OrganizationsGetRequest(), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => organizationsGet"
            });
            return callback(err, null);
          }

          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "organizationsGet"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => organizationsGet"
            });
            return callback(err, null);
          }
        }

        const {body} = response;
        const organizations = body && body.orgs ? body.orgs : [];
        callback(err, organizations);
      });
    });
  }

  /**
   * @param {{workspaceId:string, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  workspacesGet(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err || !session) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspacesGet => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspacesGet(session.getSessionId(), session.getToken(), new WorkspacesGetRequest(), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspacesGet"
            });
            return callback(err, null);
          }

          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspacesGet"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspacesGet"
            });
            return callback(err, null);
          }
        }

        const {body} = response;
        const workspaces = body && body.workspaces ? body.workspaces : [];
        callback(err, workspaces);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  workspaceGet(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceGet => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceGet(session.getSessionId(), session.getToken(), new WorkspaceGetRequest(globalId), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceGet",
              data: inputData
            });
            return callback(err, null);
          }

          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspaceGet"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceGet",
              data: inputData
            });
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, body:{}, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  workspaceCreate(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, body, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceCreate => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceCreate(session.getSessionId(), session.getToken(), new WorkspaceCreateRequest(body), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceCreate",
              data: inputData
            });
            return callback(err, null);
          }

          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspaceCreate"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceCreate",
              data: inputData
            });
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, body:{}, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  workspaceUpdate(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, body, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceUpdate => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceUpdate(session.getSessionId(), session.getToken(), new WorkspaceUpdateRequest(body), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceUpdate",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspaceUpdate"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceUpdate",
              data: inputData
            });

            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  workspaceDelete(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceDelete => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceDelete(session.getSessionId(), session.getToken(), new WorkspaceDeleteRequest(globalId), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceDelete",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspaceDelete"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceDelete",
              data: inputData
            });
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @param {Function} callback
   */
  workspaceMembersGet(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err || !session) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceMembersGet => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceMembersGet(session.getSessionId(), session.getToken(), new WorkspaceMembersGetRequest(globalId), async (err, response) => {
        if (err || !response) {
          return callback(err, null);
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, body: {}}} inputData
   * @param {Function} callback
   */
  workspaceMemberAdd(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, body, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceMemberAdd => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceMemberAdd(session.getSessionId(), session.getToken(), new WorkspaceMemberAddRequest(body), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceMemberAdd",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspaceMemberAdd"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceMemberAdd",
              data: inputData
            });
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, body:{}}} inputData
   * @param {Function} callback
   */
  workspaceMemberUpdate(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, body, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceMemberUpdate => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceMemberUpdate(session.getSessionId(), session.getToken(), new WorkspaceMemberUpdateRequest(body), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceMemberUpdate",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspaceMemberUpdate"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceMemberUpdate",
              data: inputData
            });
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, memberId:string}} inputData
   * @param {Function} callback
   */
  workspaceMemberDelete(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, memberId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceMemberDelete => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceMemberDelete(session.getSessionId(), session.getToken(), new WorkspaceMemberDeleteRequest(memberId), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceMemberDelete",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspaceMemberDelete"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceMemberDelete",
              data: inputData
            });
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:int}} inputData
   * @param {Function} callback
   */
  workspaceInvitesGet(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceInvitesGet => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceInvitesGet(session.getSessionId(), session.getToken(), new WorkspaceInvitesGetRequest(globalId), async (err, response) => {
        if (err || !response) {
          return callback(err, []);
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:int}} inputData
   * @param {Function} callback
   */
  workspaceInviteResend(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceInviteResend => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceInviteResend(session.getSessionId(), session.getToken(), new WorkspaceInviteResendRequest(globalId), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceInviteResend",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspaceInviteResend"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceInviteResend",
              data: inputData
            });
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, body:{}}} inputData
   * @param {Function} callback
   */
  workspaceInviteUpdate(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, body, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceInviteUpdate => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceInviteUpdate(session.getSessionId(), session.getToken(), new WorkspaceInviteUpdateRequest(body), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceInviteUpdate",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspaceInviteUpdate"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceInviteUpdate",
              data: inputData
            });
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:int}} inputData
   * @param {Function} callback
   */
  workspaceInviteDelete(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceInviteDelete => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceInviteDelete(session.getSessionId(), session.getToken(), new WorkspaceInviteDeleteRequest(globalId), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceInviteDelete",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspaceInviteDelete"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceInviteDelete",
              data: inputData
            });
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, workspaces:[], skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  workspaceSyncState(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, workspaces, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => workspaceSyncState => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.workspaceSyncState(session.getSessionId(), session.getToken(), new WorkspaceSyncStateRequest(workspaces), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceSyncState",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "workspaceSyncState"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => workspaceSyncState",
              data: inputData
            });
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, body:{globalId:string, toWorkspaceId:string, remove:boolean}, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  moveToWorkspace(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, body, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => moveToWorkspace => getAccountSession"
        });
        return callback(err, null);
      }

      self.workspacesApi.moveToWorkspace(session.getSessionId(), session.getToken(), new MoveToWorkspaceRequest(body), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => moveToWorkspace",
              data: inputData
            });
            return callback(err, response.body);
          }
          if (!response.body) {
            return callback(err, response.body);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "moveToWorkspace"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => moveToWorkspace",
              data: inputData
            });
            return callback(err, response.body);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  getNoteTextToken(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, globalId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      const errSession = err;
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => getNoteTextToken => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.getNoteTextToken(session.getSessionId(), session.getToken(), new NotesTextTokenUpdateRequest({
        workspaceId,
        globalId
      }), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => getNoteTextToken",
              data: inputData
            });
            API.checkSignInStatus(err, errSession);
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "getNoteTextToken"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => getNoteTextToken",
              data: inputData
            });
            API.checkSignInStatus(err, errSession);
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  getTrial(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => getTrial => getAccountSession"
        });
        return callback(err, null);
      }

      self.userApi.getTrial(session.getSessionId(), session.getToken(), ApiConst.getTrialUrl(), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => getTrial",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "getTrial"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => getTrial",
              data: inputData
            });
            return callback(err, null);
          }
        }

        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, body: {attachmentGlobalId:string, tempFileName:string, role:string, trialCredits:number}, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  notesAnnotate(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, body, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => updateUserInfo => getAccountSession"
        });
        return callback(err, null);
      }

      self.notesApi.notesAnnotate(session.getSessionId(), session.getToken(), new NotesAnnotateRequest(body), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => notesAnnotate",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "notesAnnotate"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => notesAnnotate",
              data: inputData
            });
            return callback(err, null);
          }
        }
        callback(err, response.body);
      });
    });
  }

  /**
   * @param {{workspaceId:string, body: {syncToken:string, pageToken:string, onPage:number}, skipSyncHandlers:boolean}} inputData
   * @param {Function} callback
   */
  notesGetAnnotations(inputData, callback = (err, res) => {
  }) {
    const {workspaceId, body, skipSyncHandlers} = inputData;
    let self = this;
    NimbusLibSDK.getAccountManager().getAccountSession((err, session) => {
      if (err) {
        errorHandler.log({
          err: err, response: session,
          description: "API problem => updateUserInfo => notesGetAnnotations"
        });
        return callback(err, null);
      }

      self.notesApi.notesAnnotate(session.getSessionId(), session.getToken(), new NotesGetAnnotationsRequest(body), async (err, response) => {
        if (skipSyncHandlers) {
          if (err || !response) {
            errorHandler.log({
              err: err, response: response,
              description: "API problem => notesGetAnnotations",
              data: inputData
            });
            return callback(err, null);
          }
          if (!response.body) {
            return callback(err, null);
          }
        } else {
          if (NimbusErrorHandler.throwNimbusApiErrorIfExist({response, functionName: "notesGetAnnotations"})) {
            await SyncStatusChangedEvent.setErrorStatus({workspaceId, err});
            errorHandler.log({
              err: err, response: response,
              description: "API problem => notesGetAnnotations",
              data: inputData
            });
            return callback(err, null);
          }
        }
        callback(err, response.body);
      });
    });
  }
}
