import {default as Base_Request} from "./common/Base_Request";

import {default as FilesPreuploadResponse} from "./response/FilesPreuploadResponse";
import {default as FilesTempExistsResponse} from "./response/FilesTempExistsResponse";
import {default as ProductRecipeUploadResponse} from "./response/ProductRecipeUploadResponse";

export default class IFilesApi {
  filesPreupload(sessionId, token, request, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, request), FilesPreuploadResponse, callback);
  };

  filesTempExists(sessionId, token, request, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, request), FilesTempExistsResponse, callback);
  };

  productRecipeUpload(sessionId, token, request, callback) {
    Base_Request.make(Base_Request.prepareRequest(sessionId, token, request), ProductRecipeUploadResponse, callback);
  };
}
