import {default as Base_Response} from "../common/Base_Response";

export default class LogoutResponse extends Base_Response {
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.errorCode = null;
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}
