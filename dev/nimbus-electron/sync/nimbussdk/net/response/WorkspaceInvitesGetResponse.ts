import {default as Base_Response} from "../common/Base_Response";
import {default as WorkspaceInviteEntity} from "./entities/WorkspaceInviteEntity";

export default class WorkspaceInvitesGetResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {WorkspaceInviteEntity[]}
   */
  invites;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.invites = [];
    if (data.invites)
      this.invites = data.invites;
  }
}

/*
{
  "errorCode": 0,
  "body": {
    "invites": [
      {

      }
    ]
  }
}
* */
