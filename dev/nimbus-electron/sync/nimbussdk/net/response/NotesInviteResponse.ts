import {default as Base_Response} from "../common/Base_Response";

export default class NotesInviteResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {number}
   */
  invite_id;
  /**
   * @type {string}
   */
  secret_key;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.invite_id = 0;
    this.secret_key = "";
    if (data.invite_id)
      this.invite_id = data.invite_id;
    if (data.secret_key)
      this.secret_key = data.secret_key;
  }
}
