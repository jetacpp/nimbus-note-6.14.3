import {default as Base_Response} from "../common/Base_Response";
import {default as NimbusError} from "../exception/common/NimbusError";

export default class UserUnlockPremiumResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;
  /**
   * @type {number}
   */
  err;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    this.err = NimbusError.OK;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {string}
   */
  errDesc;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.errDesc = "";
    if (data.errDesc)
      this.errDesc = data.errDesc;
  }
}
