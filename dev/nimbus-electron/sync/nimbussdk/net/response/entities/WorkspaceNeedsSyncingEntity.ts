import {default as WorkspaceStateEntity} from "./WorkspaceStateEntity";

export default class WorkspaceNeedsSyncingEntity {
  /**
   * @type {WorkspaceStateEntity}
   */
  state = null;
  /**
   * @type {string} (created|deleted|updated)
   */
  reason = '';
}

