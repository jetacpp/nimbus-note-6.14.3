import {default as AbstractAttachment} from "./AbstractAttachment";

export default class SyncAttachmentEntity extends AbstractAttachment {
  /**
   * @type {string}
   */
  global_id = "";
  /**
   * @type {number}
   */
  date_added = 0;
  /**
   * @type {number}
   */
  date_updated = 0;
  /**
   * @type {string}
   */
  location = "";
  /**
   * @type {string}
   */
  type = "";
  /**
   * @type {string}
   */
  role = "attachment";
  /**
   * @type {string}
   */
  type_extra = "";
  /**
   * @type {number}
   */
  size = 0;
  /**
   * @type {number}
   */
  in_list = 0;
  /**
   * @type {number}
   */
  is_encrypted = 0;
  /**
   * @type {string}
   */
  display_name = "";
  /**
   * @type {string}
   */
  mime = "";
  /**
   * @type {string}
   */
  tempname = "";
  /**
   * @type {string}
   */
  file_uuid = "";
  /**
   * @type {string}
   */
  extension = "";

  /**
   * @return {string}
   */
  static getLocalPath() {
    return "";
  };

  /**
   * @return {string}
   */
  static getLocalPathWithFile() {
    return "";
  };
}

/*
 {
 "type_extra": "{}",
 "global_id": "o1491374147x0001",
 "display_name": "MFC71u.dll",
 "mime": "application/octet-stream",
 "date_added": 1491374193,
 "date_updated": 1491386070,
 "type": "file",
 "role": "attachment",
 "size": 1047552,
 "in_list": 1,
 "is_encrypted": 0,
 "location": "https://s3.amazonaws.com/fvd-data/notes/106576/1491374194-nI6IxT/MFC71u.dll",
 "file_uuid": "218ca8d2-cd85-41ec-8731-251e855c15ac"
 }
 */
