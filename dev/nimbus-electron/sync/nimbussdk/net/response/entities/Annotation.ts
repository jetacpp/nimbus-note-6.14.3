import {default as Extractions} from "./Extractions";
import {default as TextDetailedElement} from "./TextDetailedElement";

export default class Annotation {
  /**
   * @type {string}
   */
  noteGlobalId;
  /**
   * @type {string}
   */
  attachmentGlobalId;
  /**
   * @type {enum("ocr", "text")}
   */
  type;
  /**
   * @type {string}
   */
  locale;
  /**
   * @type {string}
   */
  text;
  /**
   * @type {TextDetailedElement[]}
   */
  textDetailed;
  /**
   * @type {string}
   */
  label;
  /**
   * @type {Extractions}
   */
  extractions;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.noteGlobalId = '';
    this.attachmentGlobalId = '';
    this.type = '';
    this.locale = '';
    this.text = '';
    this.textDetailed = [];
    this.label = '';
    this.extractions = [];

    if (data.noteGlobalId)
      this.noteGlobalId = data.noteGlobalId;
    if (data.attachmentGlobalId)
      this.attachmentGlobalId = data.attachmentGlobalId;
    if (data.type)
      this.type = data.type;
    if (data.textDetailed)
      this.textDetailed = data.textDetailed;
    if (data.label)
      this.label = data.label;
    if(data.extractions) {
      this.extractions = data.extractions;
    }
  }
}
