import {default as Vertex} from "./Vertex";

export default class TextDetailedElement {
  /**
   * @type {string}
   */
  description;
  /**
   * @type {Vertex[]}
   */
  poly;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.description = '';
    this.poly = [];

    if (data.description)
      this.description = data.description;
    if (data.poly)
      this.poly = data.poly;
  }
}
