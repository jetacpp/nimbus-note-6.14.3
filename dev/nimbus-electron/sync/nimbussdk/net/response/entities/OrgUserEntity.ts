export default class OrgUserEntity {
  /**
   * @type {int}
   */
  id = 0;
  /**
   * @type {string|null}
   */
  email = null;
  /**
   * @type {string}
   */
  username = '';
  /**
   * @type {string|null}
   */
  firstname = null;
  /**
   * @type {string|null}
   */
  lastname = null;

  /**
   * @param {{}|null} data
   */
  constructor(data = null) {
    if (data) {
      this.id = data.id || 0;
      this.email = data.email ? data.email.toLowerCase() : '';
      this.username = data.username || '';
      this.firstname = data.firstname || null;
      this.lastname = data.lastname || null;
    }
  }
}

