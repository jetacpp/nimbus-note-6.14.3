export default class UserVar {
    /**
     * @type {string}
     */
    key = '';
    /**
     * @type {string|number|boolean|object}
     */
    value = '';

    /**
     * @param {{}} data
     */
    constructor(data = null) {
        this.key = data.key || '';
        this.value = data.value || '';
    }
}

