import {default as Premium} from "./Premium";
import {default as Usage} from "./Usage";
import {default as Limits} from "./Limits";

export default class Info {
  /**
   * @type {number}
   */
  user_id = 0;
  /**
   * @type {Premium}
   */
  premium = new Premium();
  /**
   * @type {string}
   */
  register_date = "";
  /**
   * @type {number}
   */
  days_to_quota_reset = 0;
  /**
   * @type {Usage}
   */
  usage = new Usage();
  /**
   * @type {Limits}
   */
  limits = new Limits();

  /**
   * @param {{}|null} data
   */
  constructor(data = null) {
    if (data) {
      this.user_id = data.user_id || 0;
      this.premium = data.premium || new Premium();
      this.register_date = data.register_date || "";
      this.days_to_quota_reset = data.days_to_quota_reset || 0;
      this.usage = data.usage || new Usage();
      this.limits = data.limits || new Limits();
    }
  }
}
