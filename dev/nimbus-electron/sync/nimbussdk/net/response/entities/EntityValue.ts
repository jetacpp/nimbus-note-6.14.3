export default class EntityValue {
  /**
   * @type {string}
   */
  text;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.text = '';

    if (data.text)
      this.text = data.text;
  }
}
