export default class Backup {
  /**
   * @type {string}
   */
  global_id = "";
  /**
   * @type {number}
   */
  date = 0;
  /**
   * @type {number}
   */
  count_notes = 0;
  /**
   * @type {number}
   */
  count_folders = 0;

  /**
   * @param {{}|null} data
   */
  constructor(data = null) {
    if (data) {
      this.global_id = data.global_id || "";
      this.date = data.date || 0;
      this.count_notes = data.count_notes || 0;
      this.count_folders = data.count_folders || 0;
    }
  }
}
