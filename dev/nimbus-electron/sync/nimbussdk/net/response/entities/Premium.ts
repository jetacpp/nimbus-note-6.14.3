export default class Premium {
  /**
   * @type {string}
   */
  type = '';
  /**
   * @type {boolean}
   */
  active = false;
  /**
   * @type {string}
   */
  start_date = "";
  /**
   * @type {string}
   */
  end_date = "";
  /**
   * @type {string}
   */
  source = "";
  /**
   * @type {boolean}
   */
  is_business = false;

  /**
   * @param {{}|null} data
   */
  constructor(data = null) {
    if (data) {
      this.type = data.type || '';
      this.active = data.active || false;
      this.start_date = data.start_date || "";
      this.end_date = data.end_date || "";
      this.source = data.source || "";
      this.is_business = data.is_business || false;
    }
  }
}
