export default class OrgUsageEntity {
  /**
   * @type {number}
   */
  current = 0;
  /**
   * @type {number}
   */
  max = 0;
  /**
   * @type {int}
   */
  daysToReset = 0;

  /**
   * @param {{}|null} data
   */
  constructor(data = null) {
    if (data) {
      this.current = data.current || 0;
      this.max = data.max || 0;
      this.daysToReset = data.daysToReset || 0;
    }
  }
}

