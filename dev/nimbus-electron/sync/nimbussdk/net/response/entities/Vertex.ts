export default class Vertex {
  /**
   * @type {number}
   */
  x;
  /**
   * @type {number}
   */
  y;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.x = 0;
    this.y = 0;

    if (data.x)
      this.x = data.x;
    if (data.y)
      this.y = data.y;
  }
}
