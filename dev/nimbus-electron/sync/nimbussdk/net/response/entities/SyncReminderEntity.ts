import {default as AbstractReminder} from "./AbstractReminder";

export default class SyncReminderEntity extends AbstractReminder {
  /**
   * @type {string}
   */
  label = "";
  /**
   * @type {number}
   */
  date = 0;
  /**
   * @type {number}
   */
  lat = 0;
  /**
   * @type {number}
   */
  lng = 0;
  /**
   * @type {string}
   */
  phone = "";
  /**
   * @type {number}
   */
  priority = 0;
  /**
   * @type {number}
   */
  interval = 0;
  /**
   * @type {number}
   */
  remind_until = 0;
}
