export default class SyncTodoEntity {
  /**
   * @type {string}
   */
  global_id = "";
  /**
   * @type {boolean}
   */
  checked = false;
  /**
   * @type {string}
   */
  label = "";
  /**
   * @type {number}
   */
  date = 0;

  /*not use @Deprecated*/
  /**
   * @type {number}
   */
  event_date = 0;
  /**
   * @type {number}
   */
  location_lat = 0;
  /**
   * @type {number}
   */
  location_lng = 0;
}

