export default class FeatureEntity {
    /**
     * {string} enum("ocr","bcr")
     */
    name = '';

    /**
     * @param {{}|null} data
     */
    constructor(data = null) {
        if (data) {
            this.name = data.name || '';
        }
    }
}
