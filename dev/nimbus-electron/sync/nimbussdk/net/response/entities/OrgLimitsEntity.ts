export default class OrgLimitsEntity {
  /**
   * @type {int}
   */
  traffic = 0;
  /**
   * @type {int}
   */
  workspaces = 0;
  /**
   * @type {int}
   */
  attachmentSize = 0;
  /**
   * @type {int}
   */
  encryptionKeysPerWorkspace = 0;
  /**
   * @type {int}
   */
  membersPerWorkspace = 0;

  /**
   * @param {{}|null} data
   */
  constructor(data = null) {
    if (data) {
      this.traffic = data.traffic || 0;
      this.workspaces = data.workspaces || 0;
      this.attachmentSize = data.attachmentSize || 0;
      this.encryptionKeysPerWorkspace = data.encryptionKeysPerWorkspace || 0;
      this.membersPerWorkspace = data.membersPerWorkspace || 0;
    }
  }
}

