import {default as OrgUsageEntity} from "./OrgUsageEntity";
import {default as OrgLimitsEntity} from "./OrgLimitsEntity";
import {default as OrgUserEntity} from "./OrgUserEntity";
import {default as FeatureEntity} from "./FeatureEntity";

export default class OrgEntity {
  /**
   * @type {string}
   */
  id = '';
  /**
   * @type {string} enum("private","business")
   */
  type = '';
  /**
   * @type {string}
   */
  title = '';
  /**
   * @type {string}
   */
  description = '';
  /**
   * @type {OrgUsageEntity}
   */
  usage = new OrgUsageEntity();
  /**
   * @type {OrgLimitsEntity}
   */
  limits = new OrgLimitsEntity();
  /**
   * @type {[FeatureEntity]}
   */
  features = [];
  /**
   * @type {OrgUserEntity}
   */
  user = new OrgUserEntity();
  /**
   * @type {string|null} only for type=business
   */
  sub = null;
  /**
   * @type {string|null} only for type=business
   */
  domain = null;
  /**
   * @type {boolean}
   */
  suspended = false;
  /**
   * @type {int|null}
   */
  suspendedAt = false;
  /**
   * @type {string|null}
   */
  suspendedReason = null;
  /**
   * @type {WorkspaceAccessEntity} only for type=business
   */
  access = null;

  /**
   * @param {{}|null} data
   */
  constructor(data = null) {
    if (data) {
      this.id = data.id || '';
      this.type = data.type || '';
      this.title = data.title || '';
      this.description = data.description || '';
      this.usage = data.usage || new OrgUsageEntity();
      this.limits = data.limits || new OrgLimitsEntity();
      this.features = data.features || [];
      this.user = data.user || new OrgUserEntity();
      this.sub = data.sub || null;
      this.suspended = data.suspended || false;
      this.suspendedAt = data.suspendedAt || 0;
      this.suspendedReason = data.suspendedReason || null;
      this.access = data.access || null;
    }
  }
}

