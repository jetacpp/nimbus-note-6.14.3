export default class Challenge {
    static TYPE_CAPTCHA = 'captcha';
    static TYPE_OTP = 'otp';

    /**
     * @type {string}
     */
    type = "";
    /**
     * @type {string}
     */
    state = "";
    /**
     * @type {string}
     */
    image = "";

    /**
     * @param {{}|null} data
     */
    constructor(data = null) {
        if (data) {
            this.type = data.type || "";
            this.state = data.state || "";
            this.image = data.image || "";
        }
    }
}
