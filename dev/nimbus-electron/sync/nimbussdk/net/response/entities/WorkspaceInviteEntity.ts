export default class WorkspaceInviteEntity {
  /**
   * @type {int}
   */
  id = 0;
  /**
   * @type {string}
   */
  workspaceId = '';
  /**
   * @type {string} (reader|editor|admin)
   */
  role = '';
  /**
   * @type {string} (encryptionAccessor|encryptor|encryptionManager|encryptionDeny)
   */
  encryptRole = '';
  /**
   * @type {int}
   */
  addedByUserId = '';
  /**
   * @type {string}
   */
  email = '';
  /**
   * @type {number}
   */
  createdAt = '';
  /**
   * @type {number}
   */
  updatedAt = '';
  /**
   * @type {boolean}
   */
  used = false;
}

