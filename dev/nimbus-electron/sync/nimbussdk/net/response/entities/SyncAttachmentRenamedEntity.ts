import {default as AbstractAttachment} from "./AbstractAttachment";

export default class SyncAttachmentRenamedEntity extends AbstractAttachment {
  /**
   * @type {string}
   */
  global_id = "";
  /**
   * @type {string}
   */
  display_name = "";
}
