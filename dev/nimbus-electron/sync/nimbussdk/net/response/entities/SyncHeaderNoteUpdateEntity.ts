import {default as AbstractNote} from "./AbstractNote";

export default class SyncHeaderNoteUpdateEntity extends AbstractNote {
  /**
   * @type {string}
   */
  global_id = "";
  /**
   * @type {string}
   */
  parent_id = "";
  /**
   * @type {string}
   */
  root_parent_id = "";
  /**
   * @type {string[]}
   */
  tags = [];
  /**
   * @type {number}
   */
  date_added_user = 0;
  /**
   * @type {number}
   */
  date_updated_user = 0;
}
