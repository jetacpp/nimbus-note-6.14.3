import {default as EntityValue} from "./EntityValue";

export default class EntityGroup {
  /**
   * @type {string}
   */
  class;
  /**
   * @type {EntityValue[]}
   */
  values;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.class = '';
    this.values = '';

    if (data.class)
      this.class = data.class;
    if (data.values)
      this.values = data.values;
  }
}
