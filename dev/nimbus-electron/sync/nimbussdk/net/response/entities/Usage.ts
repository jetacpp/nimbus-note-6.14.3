export default class Usage {
  /**
   * @type {Notes}
   */
  notes = null;

  /**
   * @param {{}} data
   */
  constructor(data = null) {
    this.notes = new Notes((data ? data.notes : null));
  }
}

/**
 * @param {{}} data
 * @constructor
 */
class Notes {
  /**
   * @type {int}
   */
  max = 0;
  /**
   * @type {int}
   */
  current = 0;

  /**
   * @param {{}|null} data
   */
  constructor(data = null) {
    if (data) {
      this.max = data.max || 0;
      this.current = data.current || 0;
    }
  }
}
