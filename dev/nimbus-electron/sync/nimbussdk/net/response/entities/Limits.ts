export default class Limits {
  /**
   * @type {number}
   */
  NOTES_MAX_SIZE = 0;
  /**
   * @type {number}
   */
  NOTES_MONTH_USAGE_QUOTA = 0;
  /**
   * @type {number}
   */
  NOTES_MAX_ATTACHMENT_SIZE = 0;

  /**
   * @param {{}|null} data
   */
  constructor(data = null) {
    if (data) {
      this.NOTES_MAX_SIZE = data.NOTES_MAX_SIZE || 0;
      this.NOTES_MONTH_USAGE_QUOTA = data.NOTES_MONTH_USAGE_QUOTA || 0;
      this.NOTES_MAX_ATTACHMENT_SIZE = data.NOTES_MAX_ATTACHMENT_SIZE || 0;
    }
  }
}
