import {default as AbstractAttachment} from "./AbstractAttachment";
import {default as AbstractNote} from "./AbstractNote";
import {default as SyncReminderEntity} from "./SyncReminderEntity";
import {default as SyncTodoEntity} from "./SyncTodoEntity";

class AttachmentsCount {
  /**
   * @type {number}
   */
  in_list = 0;
}

export default class SyncNoteDownloadEntity extends AbstractNote {
  /**
   * @type {string}
   */
  global_id = "";
  /**
   * @type {string}
   */
  parent_id = "";
  /**
   * @type {string}
   */
  root_parent_id = "";
  /**
   * @type {number}
   */
  index = 0;
  /**
   * @type {number}
   */
  date_added_user = 0;
  /**
   * @type {number}
   */
  date_updated_user = 0;
  /**
   * @type {number}
   */
  date_added = 0;
  /**
   * @type {number}
   */
  date_updated = 0;
  /**
   * @type {string}
   */
  type = "";
  /**
   * @type {string}
   */
  title = "";
  /**
   * @type {string}
   */
  text = "";
  /**
   * @type {int}
   */
  text_version = 1;
  /**
   * @type {string}
   */
  last_change_by = "";
  /**
   * @type {number}
   */
  location_lat = 0;
  /**
   * @type {number}
   */
  location_lng = 0;
  /**
   * @type {string}
   */
  //shared = 0;
  /**
   * @type {string}
   */
  //favorite = 0;
  /**
   * @type {string}
   */
  role = "";
  /**
   * @type {number}
   */
  editnote = 0;
  /**
   * @type {SyncReminderEntity}
   */
  reminder = new SyncReminderEntity();
  /**
   * @type {string}
   */
  text_short = "";
  /**
   * @type {string}
   */
  color = "";
  /**
   * @type {string}
   */
  url = "";
  /**
   * @type {number}
   */
  is_encrypted = 0;
  /**
   * @type {number}
   */
  //is_completed = 0;
  /**
   * @type {AttachmentsCount}
   */
  attachments_count = new AttachmentsCount();
  /**
   * @type {SyncTodoEntity[]}
   */
  todo = [];
  /**
   * @type {string[]}
   */
  tags = [];
  /**
   * @type {AbstractAttachment[]}
   */
  attachements = [];
  /**
   * @type {{global_id: string}|null}
   */
  preview = null;
  /**
   * @type string
   */
  shared_url = "";
}
