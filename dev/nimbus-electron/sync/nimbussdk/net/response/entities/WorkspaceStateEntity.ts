export default class WorkspaceStateEntity {
  /**
   * @type {string}
   */
  globalId = '';
  /**
   * @type {int}
   */
  lastUpdateTime = 0;
}

