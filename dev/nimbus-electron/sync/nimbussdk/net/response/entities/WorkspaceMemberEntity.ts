export default class WorkspaceMemberEntity {
  /**
   * @type {string}
   */
  globalId = '';
  /**
   * @type {int}
   */
  addedByUserId = 0;
  /**
   * @type {*}
   */
  user = 0;
  /**
   * @type {number}
   */
  createdAt = 0;
  /**
   * @type {number}
   */
  updatedAt = 0;
  /**
   * @type {string}
   */
  workspaceId = '';
  /**
   * @type {string}
   */
  type = 'full';
  /**
   * @type {string} (reader|editor|admin)
   */
  role = '';
  /**
   * @type {string} (encryptionAccessor|encryptor|encryptionManager|encryptionDeny)
   */
  encryptRole = '';
  /**
   * @type {string}
   */
  orgId = '';
}

