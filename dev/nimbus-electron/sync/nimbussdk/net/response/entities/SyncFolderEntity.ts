import {default as AbstractNote} from "./AbstractNote";

export default class SyncFolderEntity extends AbstractNote {
  /**
   * @type {string}
   */
  global_id = "";
  /**
   * @type {string}
   */
  parent_id = "";
  /**
   * @type {string}
   */
  root_parent_id = "";
  /**
   * @type {number}
   */
  index = 0;
  /**
   * @type {number}
   */
  date_added = 0;
  /**
   * @type {number}
   */
  date_added_user = 0;
  /**
   * @type {number}
   */
  date_updated_user = 0;
  /**
   * @type {string}
   */
  type = "";
  /**
   * @type {string}
   */
  title = "";
  /**
   * @type {number}
   */
  shared = 0;
  /**
   * @type {string}
   */
  color = "";

  /**
   * @param {string} title
   */
  setTitle(title) {
    if (title != null) {
      this.title = title;
    }
  }

  toString() {
    return 'SyncFolderEntity{' + 'date_added=' + this.date_added + '}';
  }
}
