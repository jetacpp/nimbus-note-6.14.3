import {default as AbstractAttachment} from "./AbstractAttachment";
import {default as AbstractNote} from "./AbstractNote";
import {default as SyncTodoEntity} from "./SyncTodoEntity";

export default class SyncFullNoteUpdateEntity extends AbstractNote {
  /**
   * @type {string}
   */
  global_id = "";
  /**
   * @type {string}
   */
  parent_id = "";
  /**
   * @type {string}
   */
  root_parent_id = "";
  /**
   * @type {number}
   */
  index = 0;
  /**
   * @type {number}
   */
  date_added_user = 0;
  /**
   * @type {number}
   */
  date_updated_user = 0;
  /**
   * @type {string}
   */
  type = "";
  /**
   * @type {string}
   */
  title = "";
  /**
   * @type {string}
   */
  text = "";
  /**
   * @type {string}
   */
  last_change_by = "";
  /**
   * @type {number}
   */
  location_lat = 0;
  /**
   * @type {number}
   */
  location_lng = 0;
  /**
   * @type {number}
   */
  shared = 0;
  /**
   * @type {number}
   */
  //favorite = 0;
  /**
   * @type {string}
   */
  role = "";
  /**
   * @type {number}
   */
  editnote = 0;
  /**
   * @type {{}}
   */
  reminder = {};
  /**
   * @type {string}
   */
  text_short = "";
  /**
   * @type {string}
   */
  color = "";
  /**
   * @type {string}
   */
  url = "";
  /**
   * @type {number}
   */
  is_encrypted = 0;
  /**
   * @type {number}
   */
  //is_completed = 0;
  /**
   * @type {SyncTodoEntity[]}
   */
  todo = [];
  /**
   * @type {string[]}
   */
  tags = [];
  /**
   * @type {AbstractAttachment[]}
   */
  attachements = [];

  /**
   * @param {{}} reminder
   */
  setReminder(reminder) {
    this.reminder = reminder !== null ? reminder : false;
  }

  /**
   * @param {SyncTodoEntity[]} todo
   */
  setTodo(todo) {
    this.todo = todo;
  }

  /**
   * @param {string[]} tags
   */
  setTags(tags) {
    this.tags = tags;
  }

  /**
   * @param {AbstractAttachment[]} attachments
   */
  setAttachments(attachments) {
    if (attachments !== null && attachments.length) {
      if (!this.attachements) {
        this.attachements = [];
      }

      for (let i in attachments) {
        if (attachments.hasOwnProperty(i)) {
          this.attachements.push(attachments[i]);
        }
      }
      //this.attachements.addAll(attachments);
    }
  }
}
