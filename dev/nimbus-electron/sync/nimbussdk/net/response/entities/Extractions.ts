import {default as EntityGroup} from "./EntityGroup";

export default class Extractions {
  /**
   * @type {string}
   */
  type;
  /**
   * @type {EntityGroup[]}
   */
  entities;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.type = '';
    this.entities = [];

    if (data.type)
      this.type = data.type;
    if (data.entities)
      this.entities = data.entities;
  }
}
