import {default as SyncReminderEntity} from "./SyncReminderEntity";

export default class StructureNote {
  /**
   * @type {string}
   */
  global_id = "";
  /**
   * @type {string}
   */
  parent_id = "";
  /**
   * @type {string}
   */
  root_parent_id = "";
  /**
   * @type {number}
   */
  date_added = 0;
  /**
   * @type {number}
   */
  date_updated = 0;
  /**
   * @type {number}
   */
  date_added_user = 0;
  /**
   * @type {number}
   */
  date_updated_user = 0;
  /**
   * @type {string}
   */
  type = "";
  /**
   * @type {string}
   */
  role = "";
  /**
   * @type {string}
   */
  title = "";
  /**
   * @type {string}
   */
  url = "";
  /**
   * @type {number}
   */
  location_lat = 0;
  /**
   * @type {number}
   */
  location_lng = 0;
  /**
   * @type {number}
   */
  shared = 0;
  /**
   * @type {number}
   */
  favorite = 0;
  /**
   * @type {number}
   */
  editnote = 0;
  /**
   * @type {number}
   */
  is_encrypted = 0;
  /**
   * @type {number}
   */
  is_completed = 0;
  /**
   * @type {string}
   */
  color = "";
  /**
   * @type {TodoCount}
   */
  todo_count = new TodoCount();
  /**
   * @type {number}
   */
  index = 0;
  /**
   * @type {string}
   */
  text_short = "";
  /**
   * @type {string[]}
   */
  tags = [];
  /**
   * @type {SyncReminderEntity}
   */
  reminder = new SyncReminderEntity();
  /**
   * @type {AttachmentsCount}
   */
  attachments_count = new AttachmentsCount();

  /**
   * @param noteAttachmentsInListCount number
   */
  setAttachmentsInListCount(noteAttachmentsInListCount) {
    this.attachments_count = new AttachmentsCount(noteAttachmentsInListCount);
  };

  /**
   * @param text string
   */
  setText(text) {
    if (text != null) {
      this.text_short = text;
    }
  };

  /**
   * @param label string
   */
  setReminderLabel(label) {
    if (this.reminder && label != null) {
      this.reminder.label = label;
    }
  };

  /**
   * @param activeTodoCount number
   */
  setTodoCount(activeTodoCount) {
    if (this.todo_count) {
      this.todo_count.unchecked = activeTodoCount;
    }
  };

  /**
   * @param locationLat string
   * @param locationLng string
   */
  setLocation(locationLat, locationLng) {
    if (locationLat && locationLng) {
      this.location_lat = locationLat;
      this.location_lng = locationLng;
    }
  };

  /**
   * @returns {string}
   */
  toString() {
    return "StructureNote{" +
      "title='" + this.title + '\'' +
      ", parent_id='" + this.parent_id + '\'' +
      ", type='" + this.type + '\'' +
      '}';
  };
}

class AttachmentsCount {
  /**
   * @type {number}
   */
  in_list = 0;

  /**
   * @param {boolean|null} noteAttachmentsInListCount
   */
  constructor(noteAttachmentsInListCount = null) {
    this.in_list = noteAttachmentsInListCount;
  }
}

class TodoCount {
  /**
   * @type {number}
   */
  checked = 0;
  /**
   * @type {number}
   */
  unchecked = 0;

  /**
   * @param {{}|null} data
   */
  constructor(data = null) {
    if (data) {
      this.checked = data.checked || 0;
      this.unchecked = data.unchecked || 0;
    }
  }
}
