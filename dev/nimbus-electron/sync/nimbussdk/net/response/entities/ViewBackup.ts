export default class ViewBackup {
  /**
   * @type {string}
   */
  global_id = "";
  /**
   * @type {string}
   */
  title = "";
  /**
   * @type {string}
   */
  parent_id = "";
  /**
   * @type {number}
   */
  date_added_user = 0;
  /**
   * @type {number}
   */
  date_updated_user = 0;
  /**
   * @type {string}
   */
  type = "";
}

/*
 {
 "global_id": "6x3mji7l1zml2zn0",
 "title": "112",
 "parent_id": "2y0nln5y6iq676ja",
 "date_added_user": 1438252076,
 "date_updated_user": 1438252076,
 "type": "folder"
 },*/
