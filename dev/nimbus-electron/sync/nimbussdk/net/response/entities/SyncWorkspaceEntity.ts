import {default as OrgEntity} from "./OrgEntity";
import {default as WorkspaceAccessEntity} from "./WorkspaceAccessEntity";

export default class SyncWorkspaceEntity {
  /**
   * @type {string}
   */
  globalId = "";
  /**
   * @type {string}
   */
  title = '';
  /**
   * @type {OrgEntity}
   */
  org = null;
  /**
   * @type {number}
   */
  userId = 0;
  /**
   * @type {number}
   */
  createdAt = 0;
  /**
   * @type {number}
   */
  updatedAt = 0;
  /**
   * @type {boolean}
   */
  isDefault = false;
  /**
   * @type {int}
   */
  countMembers = 0;
  /**
   * @type {int}
   */
  countInvites = 0;
  /**
   * @type {WorkspaceAccessEntity} only for type=business
   */
  access = null;
  /**
   * @type {string}
   */
  defaultEncryptionKeyId = null;
  /**
   * @type {boolean}
   */
  isNotesLimited = false;
}
