import {default as WorkspaceMemberEntity} from "./WorkspaceMemberEntity";

export default class WorkspaceAccessEntity {
  /**
   * @type {string} (reader|editor|admin)
   */
  role = '';
  /**
   * @type {[string]} (canRead|canEdit|canComment|canManageMembers|canAccessEncryptedNotes|canEncryptNotes|canManageEncryptionKeys)
   */
  privileges = [];
  /**
   * @type {[WorkspaceMemberEntity]}
   */
  member = [];
}

