import chalk from "chalk";
import {default as Base_Response} from "../common/Base_Response";

export default class TrialResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = 0;
    if (data) {
      this.body = new Body(data);
      if (data.errorCode != null) {
        this.errorCode = data.errorCode;
      }
    }
  }
}

class Body {
  /**
   * @type {string|null}
   */
  state;
  /**
   * @type {number}
   */
  dateStart;
  /**
   * @type {number}
   */
  dateUntil;
  /**
   * @type {number}
   */
  daysRemain;
  /**
   * @type {string|null}
   */
  requestorService;
  /**
   * @type {string|null}
   */
  requestorApp;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.state = null;
    this.dateStart = 0;
    this.dateUntil = 0;
    this.daysRemain = 0;
    this.requestorService = null;
    this.requestorApp = null;

    if (data.state)
      this.state = data.state;
    if (data.dateStart)
      this.dateStart = data.dateStart;
    if (data.dateUntil)
      this.dateUntil = data.dateUntil;
    if (data.daysRemain)
      this.daysRemain = data.daysRemain;
    if (data.requestorService)
      this.requestorService = data.requestorService;
    if (data.requestorApp)
      this.requestorApp = data.requestorApp;
  }
}
