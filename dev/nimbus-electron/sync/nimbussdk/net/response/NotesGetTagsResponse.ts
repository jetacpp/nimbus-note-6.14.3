import {default as Base_Response} from "../common/Base_Response";
import {default as Usage} from "./entities/Usage";

export default class NotesGetTagsResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;

    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {string[]}
   */
  tags;
  /**
   * @type {number}
   */
  last_update_time;
  /**
   * @type {Usage}
   */
  usage;

  constructor(data) {
    this.tags = [];
    this.last_update_time = 0;
    this.usage = {};
    if (data.tags)
      this.tags = data.tags;
    if (data.last_update_time)
      this.last_update_time = data.last_update_time;
    if (data.usage)
      this.usage = new Usage(data.usage);
  }
}

/*

{
  "errorCode": 0,
  "body": {
    "tags": [
      "tag1",
      "tag2",
      "tag3"
    ],
    "last_update_time": 123456789,
    "usage": {
      "max": 104857600,
      "current": 420740
    }
  }
}
* */

