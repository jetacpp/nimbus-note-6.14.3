import {default as Base_Response} from "../common/Base_Response";
import {default as StructureNote} from "./entities/StructureNote";

export default class NoteIsExistOnServerResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {number}
   */
  totalAmount;
  /**
   * @type {StructureNote[]}
   */
  notes;
  /**
   * @type {number}
   */
  last_update_time;
  /**
   * @type {Usage}
   */
  usage;

  constructor(data) {
    this.totalAmount = 0;
    this.notes = [];
    this.last_update_time = 0;
    this.usage = {};
    if (data.totalAmount)
      this.totalAmount = data.totalAmount;
    if (data.notes)
      this.notes = data.notes;
    if (data.last_update_time)
      this.last_update_time = data.last_update_time;
    if (data.usage)
      this.usage = new Usage(data.usage);
  }
}

class Usage {
  /**
   * @type {number}
   */
  max;
  /**
   * @type {number}
   */
  current;

  constructor(data) {
    this.max = 0;
    this.current = 0;

    if (data.max)
      this.max = data.max;
    if (data.current)
      this.current = data.current;
  }
}
