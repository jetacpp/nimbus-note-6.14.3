import {default as Base_Response} from "../common/Base_Response";

export default class FilesPreuploadResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode !== null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {File[]}
   */
  files;

  constructor(data) {
    this.files = [];

    if (data.files) {
      for (let i in data.files) {
        if (data.files.hasOwnProperty(i)) {
          this.files.push(new File({file1: data.files[i]}));
        }
      }
    }
  }
}

class File {
  /**
   * @type {string}
   */
  file1;

  constructor(data) {
    this.file1 = "";

    if (data.file1) {
      this.file1 = data.file1;
    }
  }
}

/*
 * {
 "errorCode": 0,
 "body": {
 "files": {
 "file1": "1370480258-x06oMN9LoC1Kl6TxVKvdGJnRsfg1CE6w"
 }
 }
 }*/

