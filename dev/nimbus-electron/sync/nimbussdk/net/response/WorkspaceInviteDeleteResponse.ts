import {default as Base_Response} from "../common/Base_Response";

export default class WorkspaceInviteDeleteResponse extends Base_Response {
  /**
   * @type {{}}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

/*
{
  "errorCode": 0,
  "body": {

  }
}
* */
