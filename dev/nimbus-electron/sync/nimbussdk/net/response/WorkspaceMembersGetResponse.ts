import {default as Base_Response} from "../common/Base_Response";
import {default as WorkspaceMemberEntity} from "./entities/WorkspaceMemberEntity";

export default class WorkspaceMembersGetResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body) {
      this.body = new Body(data.body);
    }
    if (data.errorCode != null) {
      this.errorCode = data.errorCode;
    }
  }
}

class Body {
  /**
   * @type {WorkspaceMemberEntity[]}
   */
  members;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.members = [];
    if (data.members) {
      this.members = data.members;
    }
  }
}

/*
{
  "errorCode": 0,
  "body": {
    "members": [
      {

      }
    ]
  }
}
* */
