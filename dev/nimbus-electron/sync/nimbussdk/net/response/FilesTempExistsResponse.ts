import {default as Base_Response} from "../common/Base_Response";

/**
 * @param {{}} data
 * @constructor
 */
export default class FilesTempExistsResponse extends Base_Response {
  /**
   * @type {{}}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = data.body;
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

/*
 * {
 "errorCode" : 0,
 "body" : {
 "1392998508-HCxymzmkSz1eQ45RI5fooVGm3FyOZxI1" : true
 }
 }*/

