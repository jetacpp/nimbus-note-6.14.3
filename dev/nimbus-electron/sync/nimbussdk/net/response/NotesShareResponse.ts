import {default as Base_Response} from "../common/Base_Response";

export default class NotesShareResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = data.body;
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

/*
* {
  "errorCode": 0,
  "body": {
    "B397CRcdOzKsVyZS": "https://nimbus.everhelper.me/client/notes/share/376113/vJR2TI2yZ6D9Hsf0un67mTCqPlJiy7XN/",
    "aIbs4iR3TNRTOx44": "https://nimbus.everhelper.me/client/notes/share/376114/ESigdLM9W15D7a8svw0PO9xHsdQNKweV/"
  }
}*/

