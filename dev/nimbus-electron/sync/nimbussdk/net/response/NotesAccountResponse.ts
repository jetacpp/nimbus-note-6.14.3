import {default as Base_Response} from "../common/Base_Response";

export default class NotesAccountResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;

    if (data.body) {
      this.body = new Body(data.body);
    }
    if (data.errorCode !== null) {
      this.errorCode = data.errorCode;
    }
  }
}

class Body {
  /**
   * @type {string}
   */
  notes_email;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.notes_email = "";
    if (data.notes_email) {
      this.notes_email = data.notes_email;
    }
  }
}
