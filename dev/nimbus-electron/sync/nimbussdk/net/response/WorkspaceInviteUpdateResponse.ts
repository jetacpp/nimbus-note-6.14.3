import {default as Base_Response} from "../common/Base_Response";
import {default as WorkspaceInviteEntity} from "./entities/WorkspaceInviteEntity";

export default class WorkspaceInviteUpdateResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {WorkspaceInviteEntity}
   */
  invite;

  /**
   * @param {{}} body
   */
  constructor(body) {
    this.invite = null;
    if (body.invite) {
      this.invite = body.invite;
    }
  }
}

/*
{
  "errorCode": 0,
  "body": {

  }
}
* */
