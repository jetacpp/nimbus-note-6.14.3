import chalk from "chalk";
import {default as Base_Response} from "../common/Base_Response";
import {default as Premium} from "./entities/Premium";
import {default as Usage} from "./entities/Usage";
import {default as Limits} from "./entities/Limits";

export default class UserInfoResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {number}
   */
  user_id;
  /**
   * @type {Premium}
   */
  premium;
  /**
   * @type {string}
   */
  register_date;
  /**
   * @type {number}
   */
  days_to_quota_reset;
  /**
   * @type {Usage}
   */
  usage;
  /**
   * @type {Limits}
   */
  limits;
  /**
   * @type {string}
   */
  login;
  /**
   * @type {string}
   */
  firstname;
  /**
   * @type {string}
   */
  lastname;
  /**
   * @type {{createdAt: string, url:string, updatedAt:string}}
   */
  avatar;
  /**
   * @type [string]
   */
  languages;
  /**
   * @type [*]
   */
  notes_predefined_folders;
  /**
   * @type string
   */
  username;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.user_id = 0;
    this.premium = {};
    this.register_date = "";
    this.days_to_quota_reset = 0;
    this.usage = {};
    this.limits = {};
    this.login = "";
    this.firstname = "";
    this.lastname = "";
    this.avatar = null;
    this.languages = [];
    this.notes_predefined_folders = null;
    this.username = "";
    if (data.user_id)
      this.user_id = data.user_id;
    if (data.premium)
      this.premium = new Premium(data.premium);
    if (data.register_date)
      this.register_date = data.register_date;
    if (data.days_to_quota_reset)
      this.days_to_quota_reset = data.days_to_quota_reset;
    if (data.usage)
      this.usage = new Usage(data.usage);
    if (data.limits)
      this.limits = new Limits(data.limits);
    if (data.login)
      this.login = data.login;
    if(data.firstname)
      this.firstname = data.firstname;
    if(data.lastname)
      this.lastname = data.lastname;
    if(data.avatar)
      this.avatar = data.avatar;
    if(data.languages)
      this.languages = data.languages;
    if(data.notes_predefined_folders)
      this.notes_predefined_folders = data.notes_predefined_folders;
    if(data.username)
      this.username = data.username;
  }
}
