import {default as Base_Response} from "../common/Base_Response";
import chalk from "chalk";

export default class NotesUpdateResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {{}|null}
   */
  failedItem;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    this.failedItem = null;
    if (data.body) {
      this.body = new Body(data.body);
    }
    if (data.errorCode != null) {
      this.errorCode = data.errorCode;
    }
    if (data.failedItem) {
      this.failedItem = data.failedItem;
    }
  }
}

class Body {
  /**
   * @type {Usage}
   */
  usage;
  /**
   * @type {number}
   */
  last_update_time;
  /**
   * @type {{}}
   */
  date_updated;
  /**
   * @type {{}}
   */
  savedAttachments;
  /**
   * @type {string}
   */
  _errorDesc;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.usage = {};
    this.last_update_time = 0;
    this.date_updated = {};
    this.savedAttachments = {};
    this._errorDesc = null;

    if (data.usage)
      this.usage = new Usage(data.usage);
    if (data.last_update_time)
      this.last_update_time = data.last_update_time;
    if (data.date_updated)
      this.date_updated = data.date_updated;
    if (data.savedAttachments)
      this.savedAttachments = data.savedAttachments;
    if(data._errorDesc != null)
      this._errorDesc = data._errorDesc;
  }
}

class Usage {
  /**
   * @type {number}
   */
  max;
  /**
   * @type {number}
   */
  current;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.max = 0;
    this.current = 0;

    if (data.max)
      this.max = data.max;
    if (data.current)
      this.current = data.current;
  }
}
