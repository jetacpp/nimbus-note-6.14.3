import {default as Base_Response} from "../common/Base_Response";
import {default as WorkspaceMemberEntity} from "./entities/WorkspaceMemberEntity";
import {default as WorkspaceInviteEntity} from "./entities/WorkspaceInviteEntity";

export default class WorkspaceMemberAddResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = new Body(data);
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {string} (invite|member)
   */
  result;
  /**
   * @type {WorkspaceMemberEntity}
   */
  member;
  /**
   * @type {WorkspaceInviteEntity}
   */
  invite;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.result = '';
    this.member = null;
    this.invite = null;
    if (data.result)
      this.result = data.result;
    if (data.member)
      this.member = data.member;
    if (data.invite)
      this.invite = data.invite;
  }
}

/*
{
  "errorCode": 0,
  "body": {
     "result": "",
     "member": {},
     "invite": {}
  }
}
* */
