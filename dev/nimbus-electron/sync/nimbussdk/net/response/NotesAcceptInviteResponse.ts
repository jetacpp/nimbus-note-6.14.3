import {default as Base_Response} from "./../common/Base_Response";

export default class NotesAcceptInviteResponse extends Base_Response {
/**
       * @type {{}}
       */
body;
/**
       * @type {number}
       */
errorCode;

/**
 * @param {{}} data
 */
  constructor(data) {
super();
      this.errorCode = null;
      this.body = data.body;

      if(data.errorCode != null)
          this.errorCode = data.errorCode;
  }
}

