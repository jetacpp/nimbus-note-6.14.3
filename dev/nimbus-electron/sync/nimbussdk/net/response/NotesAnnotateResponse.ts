import {default as Base_Response} from "../common/Base_Response";
import {default as Annotation} from "./entities/Annotation";

export default class NotesAnnotateResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {Annotation}
   */
  annotation;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.annotation = null;

    if (data.annotation) {
      this.annotation = new Annotation(data.annotation);
    }
  }
}
