import {default as Base_Response} from "../common/Base_Response";

export default class NotesTextTokenUpdateResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body) {
      this.body = new Body(data.body);
    }
    if (data.errorCode != null) {
      this.errorCode = data.errorCode;
    }
  }
}

class Body {
  /**
   * @type {[]}
   */
  tokens;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.tokens = [];

    if (data.tokens && data.tokens.length) {
      for (let token of data.tokens) {
        this.tokens.push(new Token(token));
      }
    }
  }
}

class Token {
  /**
   * @type {string}
   */
  noteGlobalId;
  /**
   * @type {string}
   */
  nodeId;
  /**
   * @type {int}
   */
  expiresAt;
  /**
   * @type {string}
   */
  token;
  /**
   * @type {string}
   */
  url;

  /**
   * @type {number}
   */
  ttl;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.noteGlobalId = '';
    this.nodeId = '';
    this.expiresAt = 0;
    this.token = '';
    this.url = '';
    this.ttl = 0;

    if (data.noteGlobalId)
      this.noteGlobalId = data.noteGlobalId;
    if (data.nodeId)
      this.nodeId = data.nodeId;
    if (data.expiresAt)
      this.expiresAt = data.expiresAt;
    if (data.token)
      this.token = data.token;
    if (data.url)
      this.url = data.url;
    if(data.ttl) {
      this.ttl = data.ttl;
    }
  }
}
