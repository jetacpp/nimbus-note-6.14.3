import {default as Base_Response} from "../common/Base_Response";
import {default as SyncWorkspaceEntity} from "./entities/SyncWorkspaceEntity";

export default class WorkspaceGetResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {SyncWorkspaceEntity}
   */
  workspace;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.workspace = null;
    if (data.workspace)
      this.workspace = data.workspace;
  }
}

/*
{
  "errorCode": 0,
  "body": {
    "workspace": {

    }
  }
}
* */
