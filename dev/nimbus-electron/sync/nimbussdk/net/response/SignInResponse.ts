import chalk from "chalk";
import {default as Base_Response} from "../common/Base_Response";

export default class SignInResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body) {
      this.body = new Body(data.body);
    }
    if (data.errorCode != null) {
      this.errorCode = data.errorCode;
    }
  }
}

class Body {
  /**
   * @type {string}
   */
  sessionId;
  /**
   * @type {int}
   */
  id;
  /**
   * @type Challenge
   */
  challenge;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.sessionId = "";
    this.id = "";
    if (data.sessionId) {
      this.sessionId = data.sessionId;
    }
    if (data.id) {
      this.id = data.id;
    }
    if(data.challenge) {
      this.challenge = data.challenge;
    }
  }
}


