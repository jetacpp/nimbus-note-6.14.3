import {default as Base_Response} from "../common/Base_Response";
import {default as OrgEntity} from "./entities/OrgEntity";

export default class OrganizationsGetResponse extends Base_Response {
    /**
     * @type {Body}
     */
    body;
    /**
     * @type {number}
     */
    errorCode;

    /**
     * @param {{}} data
     */
    constructor(data) {
        super();
        this.body = {};
        this.errorCode = null;
        if (data.body)
            this.body = new Body(data.body);
        if (data.errorCode != null)
            this.errorCode = data.errorCode;
    }
}

class Body {
    /**
     * @type {[OrgEntity]}
     */
    orgs;

    /**
     * @param {{}} data
     */
    constructor(data) {
        this.orgs = [];
        if (data.orgs) {
            this.orgs = data.orgs;
        }
    }
}

/*
{
  "errorCode": 0,
  "body": {
    "orgs": [
      {

      }
    ]
  }
}
* */
