import {default as Base_Response} from "../common/Base_Response";

export default class WorkspaceMemberUpdateResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

/*
{
  "errorCode": 0,
  "body": {

  }
}
* */
