import {default as Base_Response} from "../common/Base_Response";

export default class ProductRecipeUploadResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode !== null)
      this.errorCode = data.err;
  }
}

class Body {
  /**
   * @type {{}} data
   */
  data;

  /**
   * @param {{}} data
   */
  constructor(data) {
    if (data) {
      this.data = data;
    }
  }
}

/*
 * {
 "errorCode": 0,
 "body": {
 "data": {
 }
 }
 }*/
