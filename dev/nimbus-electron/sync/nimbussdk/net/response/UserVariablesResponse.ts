import chalk from "chalk";
import {default as Base_Response} from "../common/Base_Response";
import {default as UserVar} from "./entities/UserVar";

export default class UserVariablesResponse extends Base_Response {
    /**
     * @type {Body}
     */
    body;
    /**
     * @type {number}
     */
    errorCode;

    /**
     * @param {{}} data
     */
    constructor(data) {
        super();
        this.body = {};
        this.errorCode = null;
        if (data.body)
            this.body = new Body(data.body);
        if (data.errorCode != null)
            this.errorCode = data.errorCode;
    }
}

class Body {
    /**
     * @type {[UserVar]}
     */
    vars;

    /**
     * @param {{}} data
     */
    constructor(data) {
        this.vars = [];
        if (data.vars) {
            this.vars = data.vars;
        }
    }
}
