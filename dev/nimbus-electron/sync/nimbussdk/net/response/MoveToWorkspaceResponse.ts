import {default as Base_Response} from "../common/Base_Response";

export default class MoveToWorkspaceResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.errorCode != null) {
      this.errorCode = data.errorCode;
    }
    if(data.body) {
      this.body = new Body(data.body);
    }
  }
}

class Body {
  /**
   * @type {string}
   */
  _errorDesc;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this._errorDesc = '';
    if (data._errorDesc) {
      this._errorDesc = data._errorDesc;
    }
  }
}

/*
{
  "errorCode": 0,
  "body": ""
}
* */
