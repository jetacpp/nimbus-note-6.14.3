import chalk from "chalk";
import {default as Base_Response} from "../common/Base_Response";

export default class UpdateUserVariableResponse extends Base_Response {
    /**
     * @type {{}}
     */
    body;
    /**
     * @type {number}
     */
    errorCode;

    /**
     * @param {{}} data
     */
    constructor(data) {
        super();
        this.body = {};
        this.errorCode = null;
        if (data.body)
            this.body = {};
        if (data.errorCode != null)
            this.errorCode = data.errorCode;
    }
}
