import {default as Base_Response} from "../common/Base_Response";

export default class ChallengeResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;

    if (data.body) {
      this.body = new Body(data.body);
    }
    if (data.errorCode != null) {
      this.errorCode = data.errorCode;
    }
  }
}

class Body {
  /**
   * @type {boolean}
   */
  sessionId;
  /**
   * @type {number}
   */
  id;
  /**
   * @type Challenge
   */
  challenge;

  constructor(data) {
    this.sessionId = '';
    this.id = "";

    if (data.sessionId) {
      this.sessionId = data.sessionId;
    }

    if (data.id) {
      this.id = data.id;
    }

    if(data.challenge) {
      this.challenge = data.challenge;
    }
  }
}

