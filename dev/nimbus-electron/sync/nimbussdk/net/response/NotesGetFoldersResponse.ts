import {default as Base_Response} from "../common/Base_Response";
import {default as Usage} from "./entities/Usage";
import {default as SyncNoteDownloadEntity} from "./entities/SyncNoteDownloadEntity";

export default class NotesGetFoldersResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;

    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {number}
   */
  totalAmount;
  /**
   * @type {SyncNoteDownloadEntity[]}
   */
  notes;
  /**
   * @type {number}
   */
  last_update_time;
  /**
   * @type {Usage}
   */
  usage;

  constructor(data) {
    this.totalAmount = 0;
    this.notes = [];
    this.last_update_time = 0;
    this.usage = {};
    if (data.totalAmount)
      this.totalAmount = data.totalAmount;
    if (data.notes)
      this.notes = data.notes;
    if (data.last_update_time)
      this.last_update_time = data.last_update_time;
    if (data.usage)
      this.usage = new Usage(data.usage);
  }
}

/*
{
  "errorCode": 0,
  "body": {
    "totalAmount": 7,
    "notes": [
      {
        "global_id": "WfH6jfI2A1hPPIoy",
        "parent_id": "root",
        "root_parent_id": "root",
        "date_added": 1484170785,
        "date_added_user": 1484170785,
        "date_updated": 1484284679,
        "date_updated_user": 1484284677,
        "type": "folder",
        "role": "note",
        "title": "New folder",
        "url": "",
        "location_lat": 0,
        "location_lng": 0,
        "shared": 0,
        "favorite": 0,
        "editnote": 1,
        "is_encrypted": 0,
        "color": "",
        "index": 0,
        "text": "<div></div>",
        "text_short": "",
        "reminder": null,
        "todo": [],
        "tags": [],
        "attachements": []
      }
    ],
    "last_update_time": 1484563514,
    "usage": {
      "max": 104857600,
      "current": 513449
    }
  }
}
* */
