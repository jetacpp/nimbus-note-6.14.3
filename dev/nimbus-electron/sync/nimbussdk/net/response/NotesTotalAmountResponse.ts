import {default as Base_Response} from "../common/Base_Response";

export default class NotesTotalAmountResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {number}
   */
  totalAmount;
  /**
   * @type {number}
   */
  last_update_time;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.totalAmount = 0;
    this.last_update_time = 0;
    if (data.totalAmount)
      this.totalAmount = data.totalAmount;
    if (data.last_update_time)
      this.last_update_time = data.last_update_time;
  }
}
