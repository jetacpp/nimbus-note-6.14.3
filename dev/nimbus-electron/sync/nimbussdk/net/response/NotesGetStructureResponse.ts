import {default as Base_Response} from "../common/Base_Response";
import {default as SyncNoteDownloadEntity} from "./entities/SyncNoteDownloadEntity";
import chalk from "chalk";

export default class NotesGetStructureResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {number}
   */
  totalAmount;
  /**
   * @type {SyncNoteDownloadEntity[]}
   */
  notes;
  /**
   * @type {{}}
   */
  fieldsUpdateDates;
  /**
   * @type {number}
   */
  last_update_time;
  /**
   * @type {Usage}
   */
  usage;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.totalAmount = 0;
    this.notes = [];
    this.last_update_time = 0;
    this.usage = {};
    if (data.totalAmount)
      this.totalAmount = data.totalAmount;
    if (data.notes)
      this.notes = data.notes;
    if (data.fieldsUpdateDates)
      this.fieldsUpdateDates = data.fieldsUpdateDates;
    if (data.last_update_time)
      this.last_update_time = data.last_update_time;
    if (data.usage)
      this.usage = new Usage(data.usage);
  }
}

class Usage {
  /**
   * @type {number}
   */
  max;
  /**
   * @type {number}
   */
  current;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.max = 0;
    this.current = 0;

    if (data.max)
      this.max = data.max;
    if (data.current)
      this.current = data.current;
  }
}
