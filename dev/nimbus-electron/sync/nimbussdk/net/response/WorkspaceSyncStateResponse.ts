import {default as Base_Response} from "../common/Base_Response";
import {default as WorkspaceNeedsSyncingEntity} from "./entities/WorkspaceNeedsSyncingEntity";

export default class WorkspaceSyncStateResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {WorkspaceNeedsSyncingEntity[]}
   */
  workspaces;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.workspaces = [];
    if (data.workspaces)
      this.workspaces = data.workspaces;
  }
}

/*
{
  "errorCode": 0,
  "body": {
    "workspaces": [
      {
        "state": {
          "globalId": "",
          "lastUpdateTime": 0
        },
        "reason": ""
      }
    ]
  }
}
* */
