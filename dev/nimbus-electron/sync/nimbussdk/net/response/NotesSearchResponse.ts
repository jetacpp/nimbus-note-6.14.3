import {default as Base_Response} from "../common/Base_Response";
import chalk from "chalk";

export default class NotesSearchResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;
    if (data.body)
      this.body = new Body(data.body);
    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

class Body {
  /**
   * @type {string[]}
   */
  global_ids;
  /**
   * @type {{}}
   */
  matches;
  /**
   * @type {{}}
   */
  excerpts;
  /**
   * @type {{}}
   */
  annotations;
  /**
   * @type {number}
   */
  totalAmount;

  /**
   * @param {{}} data
   */
  constructor(data) {
    this.global_ids = [];
    this.totalAmount = 0;

    if (data.global_ids)
      this.global_ids = data.global_ids;
    if (data.matches)
      this.matches = data.matches;
    if (data.excerpts)
      this.excerpts = data.excerpts;
    if (data.annotations)
      this.annotations = data.annotations;
    if (data.totalAmount)
      this.totalAmount = data.totalAmount;
  }
}
