import {default as Base_Response} from "../common/Base_Response";

/**
 * @param {{}} data
 * @constructor
 */
export default class GetRemovedItemsResponse extends Base_Response {
  /**
   * @type {Body}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  constructor(data) {
    super();
    this.body = {};
    this.errorCode = null;

    if (data.body) {
      this.body = new Body(data.body);
    }

    if (data.errorCode != null) {
      this.errorCode = data.errorCode;
    }
  }
}

class Body {
  /**
   * @type {RemovedItem[]}
   */
  removedItems;
  /**
   * @type {Usage}
   */
  usage;
  /**
   * @type {number}
   */
  last_update_time;

  constructor(data) {
    this.removedItems = [];
    this.usage = {};
    this.last_update_time = 0;
    if (data.removedItems) {
      for (let i in data.removedItems) {
        this.removedItems.push(new RemovedItem(data.removedItems[i]));
      }
    }
    if (data.usage) {
      this.usage = new Usage(data.usage);
    }
    if (data.last_update_time) {
      this.last_update_time = data.last_update_time;
    }
  }
}

class Usage {
  /**
   * @type {number}
   */
  max;
  /**
   * @type {number}
   */
  current;

  constructor(data) {
    this.max = 0;
    this.current = 0;

    if (data.max) {
      this.max = data.max;
    }
    if (data.current) {
      this.current = data.current;
    }
  }
}

class RemovedItem {
  /**
   * @type {number}
   */
  global_id;
  /**
   * @type {string}
   */
  type;
  /**
   * @type {number}
   */
  time;

  constructor(data) {
    this.global_id = 0;
    this.type = "";
    this.time = 0;
    if (data.global_id) {
      this.global_id = data.global_id;
    }
    if (data.type) {
      this.type = data.type;
    }
    if (data.time) {
      this.time = data.time;
    }
  }
}

/*
{
  "errorCode": 0,
  "body": {
    "removedItems": [
      {
        "global_id": "JTwuw9KIIY7aAMXy",
        "type": "note",
        "time": 1484639471
      },
      {
        "global_id": "3QAoUnVmaUY9kcad",
        "type": "note",
        "time": 1484639486
      },
      {
        "global_id": "Hr1jokj5iQKGPYbz",
        "type": "attachement",
        "time": 1484639606
      }
    ],
    "last_update_time": 1484639606,
    "usage": {
      "max": 104857600,
      "current": 643836
    }
  }
}
* */
