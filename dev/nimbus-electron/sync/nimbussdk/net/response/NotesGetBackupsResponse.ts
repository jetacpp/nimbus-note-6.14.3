import {default as Base_Response} from "../common/Base_Response";

export default class NotesGetBackupsResponse extends Base_Response {
  /**
   * @type {{}}
   */
  body;
  /**
   * @type {number}
   */
  errorCode;

  /**
   * @param {{}} data
   */
  constructor(data) {
    super();
    this.errorCode = null;
    this.body = data.body;

    if (data.errorCode != null)
      this.errorCode = data.errorCode;
  }
}

/**
 * {
 * "errorCode": 0,
 * "body": [
 * {
 * "global_id": "46R84y5wNdHf5iWD",
 * "date": 1453159959,
 * "count_notes": 335,
 * "count_folders": 9
 * },
 * {
 * "global_id": "Nr49K3oXb1141atn",
 * "date": 1452010852,
 * "count_notes": 335,
 * "count_folders": 9
 * }
 * ]
 * }
 */
