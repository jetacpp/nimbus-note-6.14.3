// @ts-ignore
const USER_EMAIL = "user_email";
const SESSION_ID = "sessionid";

export default class Session {
  userEmail;
  sessionId;

  /**
   * @param {string} userEmail
   * @param {string} sessionId
   */
  constructor(userEmail, sessionId) {
    this.userEmail = userEmail;
    this.sessionId = sessionId;
  }

  /**
   * @returns {string}
   */
  getUserEmail() {
    return this.userEmail;
  }

  /**
   * @returns {string}
   */
  getSessionId() {
    return this.sessionId;
  }

  /**
   * @returns {string}
   */
  getToken() {
    return '';
  }

  /**
   * @returns {{}}
   */
  getData() {
    let map = {};
    map[USER_EMAIL] = this.userEmail;
    map[SESSION_ID] = this.sessionId;
    return map;
  }
}
