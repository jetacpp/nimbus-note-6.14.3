import {default as socketConnection} from "../../socket/socketFunctions";

let notesPreviewData = {};

export default class PreviewSyncManager {

  /**
   * @param {{workspaceId:string}} inputData
   */
  static init(inputData) {
    const {workspaceId} = inputData;

    if (typeof (notesPreviewData[workspaceId]) === 'undefined') {
      notesPreviewData[workspaceId] = {};
    }

    notesPreviewData[workspaceId] = {
      update: {},
      remove: {}
    };
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static clear(inputData) {
    PreviewSyncManager.init(inputData);
  }

  /**
   * @param {{workspaceId:string, globalId:string, attachmentGlobalId:string}} inputData
   */
  static pushUpdateEvent(inputData) {
    const {workspaceId, globalId, attachmentGlobalId} = inputData;

    if (typeof (notesPreviewData[workspaceId]) === 'undefined') {
      notesPreviewData[workspaceId] = {
        update: {},
        remove: {}
      };
    }

    notesPreviewData[workspaceId].update[globalId] = attachmentGlobalId;
    socketConnection.sendItemPreviewUpdateMessage(inputData);
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static pushRemoveEvent(inputData) {
    const {workspaceId, globalId} = inputData;

    if (typeof (notesPreviewData[workspaceId]) === 'undefined') {
      notesPreviewData[workspaceId] = {
        update: {},
        remove: {}
      };
    }

    notesPreviewData[workspaceId].remove[globalId] = globalId;
    socketConnection.sendItemPreviewRemoveMessage(inputData);
  }

  /**
   * @param {{workspaceId:string, globalId:string, attachmentGlobalId:string}} inputData
   */
  static updateNotePreview(inputData) {
    const {workspaceId, globalId, attachmentGlobalId} = inputData;
    if (typeof(notesPreviewData[workspaceId]) === 'undefined') {
      return;
    }

    if (typeof(notesPreviewData[workspaceId].update) === 'undefined') {
      return;
    }

    if (typeof(notesPreviewData[workspaceId].update[globalId]) === 'undefined') {
      return;
    }

    if (notesPreviewData[workspaceId].update[globalId] == attachmentGlobalId) {
      socketConnection.sendItemPreviewUpdateMessage(inputData);
    }
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @return {{}}
   */
  static getEvents(inputData) {
    const {workspaceId} = inputData;

    if (typeof(notesPreviewData[workspaceId]) === 'undefined') {
      return {
        update: {},
        remove: {}
      };
    }

    return notesPreviewData[workspaceId];
  }
}
