import {default as AccountManager} from "./AccountManager";

/**
 * @type AccountManager
 */
let manager = null;

export default class AccountManagerProvider {
  /**
   * @return AccountManager
   */
  static get() {
    if (!manager) {
      manager = new AccountManager();
    }
    return manager;
  }
}
