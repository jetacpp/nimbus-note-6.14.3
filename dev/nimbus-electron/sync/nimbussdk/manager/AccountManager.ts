import {isNull} from "util";
import chalk from "chalk";
import {default as App} from "../../process/application/App";
import {default as Session} from "./Session";
import {default as SecurityManager} from "./SecurityManager";
import {default as generatorHandler} from "../../../utilities/generatorHandler";
import {default as settings} from "../../../db/models/settings";
import {default as userSettings} from "../../../db/models/userSettings";
import {default as workspace} from "../../../db/models/workspace";
import {default as config} from "../../../../config";

export default class AccountManager {
  static OFFLINE_UNIQUE_USER_NAME = "offline-account";
  static OFFLINE_ACCOUNT = "offline";

  static USER_EMAIL = "user_email";
  static LAST_UPDATE_TIME = "last_update_time";

  static USER_ID = "user_id";
  static USER_LOGIN = "user_login";
  static USER_FIRST_NAME = "user_first_name";
  static USER_LAST_NAME = "user_last_name";
  static USER_AVATAR = "user_avatar";
  static UNIQUE_USER_NAME = "unique_user_name";
  static PREMIUM_ACTIVE = "premium_status";
  static PREMIUM_START_DATE = "premium_start_date";
  static PREMIUM_END_DATE = "premium_end_date";
  static PREMIUM_SOURCE = "premium_source";
  static REGISTER_DATE = "register_date";
  static DAYS_TO_QUOTA_RESET = "days_to_quota_reset";
  static TRAFFIC_MAX = "traffic_max";
  static TRAFFIC_CURRENT = "traffic_current";
  static LIMIT_NOTES_MAX_SIZE = "limit_NOTES_MAX_SIZE";
  static LIMIT_NOTES_MONTH_USAGE_QUOTA = "limit_NOTES_MONTH_USAGE_QUOTA";
  static LIMIT_NOTES_MAX_ATTACHMENT_SIZE = "limit_NOTES_MAX_ATTACHMENT_SIZE";
  static SYNC_TYPE_IS_HEADER = "sync_type_is_header";

  static EMAIL_FOR_SHARE_NOTES = "EMAIL_FOR_SHARE_NOTES";

  static LIMIT_NOTES_MAX_SIZE_DEFAULT = 100000000;
  static LIMIT_NOTES_MAX_ATTACHMENT_SIZE_DEFAULT = 100000000;

  /**
   * @param {Function} callback
   */
  isAuthorized(callback = (err, res) => {
  }) {
    let accountData = [null, null];
    this.getAccountSession((err, session) => {
      if(err || !session) {
        return this.isAuthorizedHandler(null, accountData, callback);
      }

      this.getUserEmail((err, userEmail) => {
        if (userEmail !== null) {
          this.getUniqueUserName((err, uniqueUserName) => {
            if (config.SHOW_WEB_CONSOLE) {
              console.log("is Authorized user: ", userEmail);
            }

            SecurityManager.get().getAccount(userEmail, (err, accountData) => {
              this.isAuthorizedHandler(userEmail, accountData, callback);
            });
          });
        } else {
          this.isAuthorizedHandler(userEmail, accountData, callback);
        }
      });
    });
  }

  /**
   * @param {string} userEmail
   * @param {{}} accountData
   * @param {Function} callback
   */
  isAuthorizedHandler(userEmail, accountData, callback) {
    userEmail = userEmail ? userEmail.trim() : userEmail;
    let sessionId = accountData[0] ? accountData[0].trim() : '';
    let isAuthorized = Boolean(userEmail && sessionId);
    return callback(null, isAuthorized);
  }

  /**
   * @param {string} email
   * @param {Function} callback
   */
  setUserEmail(email, callback = (err, res) => {
  }) {
    this.set(AccountManager.USER_EMAIL, email.toLowerCase(), (err, value) => {
      callback(null, value);
    });
  }

  /**
   * @param {Function} callback
   */
  getUserEmail(callback = (err, res) => {
  }) {
    return this.get(AccountManager.USER_EMAIL, null, callback);
  }

  /**
   * @param {Function} callback
   */
  isOfflineAccount(callback = (err, res) => {
  }) {
    return this.get(AccountManager.OFFLINE_ACCOUNT, false, callback);
  }

  /**
   * @param {Function} callback
   */
  enableOfflineAccount(callback = (err, res) => {
  }) {
    this.set(AccountManager.OFFLINE_ACCOUNT, true);
    this.clearAccountSession();

    this.remove(AccountManager.USER_ID);
    this.remove(AccountManager.USER_LOGIN);
    this.remove(AccountManager.UNIQUE_USER_NAME);
    this.remove(AccountManager.PREMIUM_ACTIVE);
    this.remove(AccountManager.PREMIUM_START_DATE);
    this.remove(AccountManager.PREMIUM_END_DATE);
    this.remove(AccountManager.PREMIUM_SOURCE);
    this.remove(AccountManager.REGISTER_DATE);
    this.remove(AccountManager.DAYS_TO_QUOTA_RESET);
    this.remove(AccountManager.TRAFFIC_MAX);
    this.remove(AccountManager.TRAFFIC_CURRENT);
    this.remove(AccountManager.LIMIT_NOTES_MAX_SIZE);
    this.remove(AccountManager.LIMIT_NOTES_MONTH_USAGE_QUOTA);
    this.remove(AccountManager.LIMIT_NOTES_MAX_ATTACHMENT_SIZE);
    this.remove(AccountManager.EMAIL_FOR_SHARE_NOTES);
    this.remove(AccountManager.LAST_UPDATE_TIME);
  }

  logout() {
    // this.clearAccountSession();
    this.disableOfflineAccount();
  }

  /**
   * @param {Function} callback
   */
  disableOfflineAccount(callback = (err, res) => {
  }) {
    this.set(AccountManager.OFFLINE_ACCOUNT, false);
    return callback(null, true);
  }

  /**
   * @param {Session} session
   * @param {Function} callback
   */
  setAccountSession(session, callback = (err, res) => {
  }) {
    const email = session.getUserEmail();
    if(!email) {
      return callback(new Error('Can not get user session email: setAccountSession'), null);
    }

    const sessionId = session.getSessionId();
    if(!sessionId) {
      return callback(new Error('Can not get user session id: setAccountSession'), null);
    }

    SecurityManager.get().updateAccount(email.toLowerCase(), sessionId, () => {
      this.set(AccountManager.USER_EMAIL, email.toLowerCase(), (err, value) => {
        callback(null, value);
      });
    });
  }

  /**
   * @param {Function} callback
   */
  getAccountSession(callback = (err, res) => {
  }) {
    try {
      this.getUserEmail((err, userEmail) => {
        if(!userEmail) {
          return callback(new Error('Can not get user session email: getAccountSession'), null);
        }

        SecurityManager.get().getAccount(userEmail.toLowerCase(), (err, accountData) => {
          if(err || !accountData) {
            return callback(new Error('Can not get user session id: getAccountSession'), null);
          }
          return callback(err, new Session(userEmail.toLowerCase(), accountData[0]));
        });
      });
    } catch (err) {
      console.log('Problem getAccountSession:', err);
      return callback(new Error('Can not get user session email: getAccountSession'), null);
    }
  }

  /**
   * @param {string} userId
   * @param {Function} callback
   */
  setUserId(userId, callback = (err, res) => {
  }) {
    this.set(AccountManager.USER_ID, userId, callback);
  }

  /**
   * @param {Function} callback
   */
  getUserId(callback = (err, res) => {
  }) {
    return this.get(AccountManager.USER_ID, -1, callback);
  }

  /**
   * @param {string} userLogin
   * @param {Function} callback
   */
  setUserLogin(userLogin, callback = (err, res) => {
  }) {
    this.set(AccountManager.USER_LOGIN, userLogin.toLowerCase(), callback);
  }

  /**
   * @param {Function} callback
   */
  getUserLogin(callback = (err, res) => {
  }) {
    return this.get(AccountManager.USER_LOGIN, null, callback);
  }

  /**
   * @param {string} userFirstName
   * @param {Function} callback
   */
  setUserFirstName(userFirstName, callback = (err, res) => {
  }) {
    this.set(AccountManager.USER_FIRST_NAME, userFirstName, callback);
  }

  /**
   * @param {Function} callback
   */
  getUserFirstName(callback = (err, res) => {
  }) {
    return this.get(AccountManager.USER_FIRST_NAME, null, callback);
  }

  /**
   * @param {string} userLastName
   * @param {Function} callback
   */
  setUserLastName(userLastName, callback = (err, res) => {
  }) {
    this.set(AccountManager.USER_LAST_NAME, userLastName, callback);
  }

  /**
   * @param {Function} callback
   */
  getUserLastName(callback = (err, res) => {
  }) {
    return this.get(AccountManager.USER_LAST_NAME, null, callback);
  }

  /**
   * @param {string} userAvatar
   * @param {Function} callback
   */
  setUserAvatar(userAvatar, callback = (err, res) => {
  }) {
    this.set(AccountManager.USER_AVATAR, userAvatar, callback);
  }

  /**
   * @param {Function} callback
   */
  getUserAvatar(callback = (err, res) => {
  }) {
    return this.get(AccountManager.USER_AVATAR, null, callback);
  }

  /**
   * @param {boolean} isActive
   * @param {Function} callback
   */
  setPremiumActive(isActive, callback = (err, res) => {
  }) {
    this.set(AccountManager.PREMIUM_ACTIVE, isActive, callback);
  }

  /**
   * @param {Function} callback
   */
  isPremiumActive(callback = (err, res) => {
  }) {
    return this.get(AccountManager.PREMIUM_ACTIVE, false, callback);
  }

  /**
   * @return {Promise<boolean>}
   */
  async isPremiumActiveAsync() {
    // @ts-ignore
    return new Promise((resolve) => {
      this.get(AccountManager.PREMIUM_ACTIVE, false, (err, res) => {
        resolve(res);
      });
    });
  }

  /**
   * @param {number} startDate
   * @param {Function} callback
   */
  setPremiumStartDate(startDate, callback = (err, res) => {
  }) {
    this.set(AccountManager.PREMIUM_START_DATE, startDate, callback);
  }

  /**
   * @param {Function} callback
   */
  getPremiumStartDate(callback = (err, res) => {
  }) {
    return this.get(AccountManager.PREMIUM_START_DATE, null, callback);
  }

  /**
   * @param {number} endDate
   * @param {Function} callback
   */
  setPremiumEndDate(endDate, callback = (err, res) => {
  }) {
    this.set(AccountManager.PREMIUM_END_DATE, endDate, callback);
  }

  /**
   * @param {Function} callback
   */
  getPremiumEndDate(callback = (err, res) => {
  }) {
    return this.get(AccountManager.PREMIUM_END_DATE, null, callback);
  }

  /**
   * @param {string} premiumSource
   * @param {Function} callback
   */
  setPremiumSource(premiumSource, callback = (err, res) => {
  }) {
    this.set(AccountManager.PREMIUM_SOURCE, premiumSource, callback);
  }

  /**
   * @param {Function} callback
   */
  getPremiumSource(callback = (err, res) => {
  }) {
    return this.get(AccountManager.PREMIUM_SOURCE, null, callback);
  }

  /**
   * @param {number} registerDate
   * @param {Function} callback
   */
  setRegisterDate(registerDate, callback = (err, res) => {
  }) {
    this.set(AccountManager.REGISTER_DATE, registerDate, callback);
  }

  /**
   * @param {number} callback
   */
  getRegisterDate(callback = (err, res) => {
  }) {
    return this.get(AccountManager.REGISTER_DATE, null, callback);
  }

  /**
   * @param {number} daysToQuotaReset
   * @param {Function} callback
   */
  setDaysToQuotaReset(daysToQuotaReset, callback = (err, res) => {
  }) {
    this.set(AccountManager.DAYS_TO_QUOTA_RESET, daysToQuotaReset, callback);
  }

  /**
   * @param {Function} callback
   */
  getDaysToQuotaReset(callback = (err, res) => {
  }) {
    return this.get(AccountManager.DAYS_TO_QUOTA_RESET, 0, callback);
  }

  /**
   * @param {number} trafficMax
   * @param {Function} callback
   */
  setTrafficMax(trafficMax, callback = (err, res) => {
  }) {
    this.set(AccountManager.TRAFFIC_MAX, trafficMax, callback);
  }

  /**
   * @param {Function} callback
   */
  getTrafficMax(callback = (err, res) => {
  }) {
    return this.get(AccountManager.TRAFFIC_MAX, 0, callback);
  }

  /**
   * @return {Promise<number>}
   */
  async getTrafficMaxAsync() {
    // @ts-ignore
    return new Promise((resolve) => {
      this.get(AccountManager.TRAFFIC_MAX, 0, (err, res) => {
        resolve(res);
      });
    });
  }

  /**
   * @param {number} trafficCurrent
   * @param {Function} callback
   */
  setTrafficCurrent(trafficCurrent, callback = (err, res) => {
  }) {
    this.set(AccountManager.TRAFFIC_CURRENT, trafficCurrent, callback);
  }

  /**
   * @param {Function} callback
   */
  getTrafficCurrent(callback = (err, res) => {
  }) {
    return this.get(AccountManager.TRAFFIC_CURRENT, 0, callback);
  }

  /**
   * @param {number} limitNotesMaxSize
   * @param {Function} callback
   */
  setLimitNotesMaxSize(limitNotesMaxSize, callback = (err, res) => {
  }) {
    this.set(AccountManager.LIMIT_NOTES_MAX_SIZE, limitNotesMaxSize, callback);
  }

  /**
   * @param {Function} callback
   */
  getLimitNotesMaxSize(callback = (err, res) => {
  }) {
    return this.get(AccountManager.LIMIT_NOTES_MAX_SIZE, AccountManager.LIMIT_NOTES_MAX_SIZE_DEFAULT, callback);
  }

  /**
   * @param {number} limitNotesMonthUsageQuota
   * @param {Function} callback
   */
  setLimitNotesMonthUsageQuota(limitNotesMonthUsageQuota, callback = (err, res) => {
  }) {
    this.set(AccountManager.LIMIT_NOTES_MONTH_USAGE_QUOTA, limitNotesMonthUsageQuota, callback);
  }

  /**
   * @param {Function} callback
   */
  getLimitNotesMonthUsageQuota(callback = (err, res) => {
  }) {
    return this.get(AccountManager.LIMIT_NOTES_MONTH_USAGE_QUOTA, 0, callback);
  }

  /**
   * @param {number} limitNotesMaxAttachmentSize
   * @param {Function} callback
   */
  setLimitNotesMaxAttachmentSize(limitNotesMaxAttachmentSize, callback = (err, res) => {
  }) {
    this.set(AccountManager.LIMIT_NOTES_MAX_ATTACHMENT_SIZE, limitNotesMaxAttachmentSize, callback);
  }

  /**
   * @param {Function} callback
   */
  getLimitNotesMaxAttachmentSize(callback = (err, res) => {
  }) {
    return this.get(AccountManager.LIMIT_NOTES_MAX_ATTACHMENT_SIZE, AccountManager.LIMIT_NOTES_MAX_ATTACHMENT_SIZE_DEFAULT, callback);
  }

  /**
   * @param {string} emailForShareNotes
   * @param {Function} callback
   */
  setEmailForShareNotes(emailForShareNotes, callback = (err, res) => {
  }) {
    this.set(AccountManager.EMAIL_FOR_SHARE_NOTES, emailForShareNotes, callback);
  }

  /**
   * @param {Function} callback
   */
  getEmailForShareNotes(callback = (err, res) => {
  }) {
    return this.get(AccountManager.EMAIL_FOR_SHARE_NOTES, null, callback);
  }

  /**
   * @param {boolean} isSyncHeader
   * @param {Function} callback
   */
  setSyncTypeIsHeader(isSyncHeader, callback = (err, res) => {
  }) {
    this.set(AccountManager.SYNC_TYPE_IS_HEADER, isSyncHeader, callback);
  }

  /**
   * @param {Function} callback
   */
  getSyncTypeIsHeader(callback = (err, res) => {
  }) {
    return this.get(AccountManager.SYNC_TYPE_IS_HEADER, true, (err, value) => {
      if (!value) {
        value = App.SYNC_TYPES.NONE;
      }
      callback(err, value);
    });
  }

  /**
   * @param {Function} callback
   */
  getAvailableQuotaTraffic(callback = (err, res) => {
  }) {
    this.getTrafficMax((err, maxLimit) => {
      this.getTrafficCurrent((err, currentTraffic) => {
        let availableQuotaTraffic = maxLimit - currentTraffic;
        callback(err, availableQuotaTraffic);
      });
    });
  }

  /**
   * @param {Function} callback
   */
  clearAccountSession(callback = (err, res) => {
  }) {
    this.getUserEmail((err, userEmail) => {
      if (err || !userEmail) {
        return callback(null, null);
      }
      SecurityManager.get().removeAccount(userEmail);
      return callback(null, true);
    });
  }

  /**
   * @param {{workspaceId:string, lastUpdateTime:int}} inputData
   * @param {Function} callback
   */
  static setLastUpdateTime(inputData, callback = (err, res) => {
  }) {
    let {workspaceId, lastUpdateTime} = inputData;
    if (!workspaceId) {
      workspaceId = workspace.DEFAULT_NAME;
    }
    userSettings.set(`lastUpdateTime:${workspaceId}`, lastUpdateTime, callback);
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @param {Function} callback
   */
  static getLastUpdateTime(inputData, callback = (err, res) => {
  }) {
    let {workspaceId} = inputData;
    if (!workspaceId) {
      workspaceId = workspace.DEFAULT_NAME;
    }

    userSettings.get(`lastUpdateTime:${workspaceId}`, (err, lastUpdateTime) => {
      if (!lastUpdateTime || typeof (lastUpdateTime) === "object") {
        lastUpdateTime = 0;
      }

      callback(err, lastUpdateTime);
    });
  }

  /**
   * @param {string} email
   * @param {Function} callback
   */
  setUniqueUserName(email, callback = (err, res) => {
  }) {
    let uniqueUserName = email.replace("@", "")
      .replace(".", "")
      .toLowerCase();
    this.set(AccountManager.UNIQUE_USER_NAME, uniqueUserName + (generatorHandler.randomString(5)), callback);
  }

  /**
   * @param {Function} callback
   */
  getUniqueUserName(callback = (err, res) => {
  }) {
    this.get(AccountManager.UNIQUE_USER_NAME, AccountManager.OFFLINE_UNIQUE_USER_NAME, callback);
  }

  /**
   * @returns {string}
   */
  getOfflineUniqueUserName() {
    return AccountManager.OFFLINE_UNIQUE_USER_NAME;
  }

  /**
   * @param {string} key
   * @param {*} defValue
   * @param {Function} callback
   */
  get(key, defValue, callback = (err, res) => {
  }) {
    settings.get(key, (err, value) => {
      if (typeof (value) === "undefined") {
        value = null;
      } else if (typeof (value) === "object") {
        if (Object.keys(value).length === 0) {
          value = null;
        }
      }

      if (!value) {
        if (!defValue) {
          if (typeof (defValue) === "number") {
            defValue = 0;
          } else if (typeof (defValue) === "string") {
            defValue = "";
          }
        }

        value = defValue;
      }

      callback(err, value);
    });
  }

  /**
   * @param {string} key
   * @param {*} value
   * @param {Function} callback
   */
  set(key, value, callback = (err, res) => {
  }) {
    if (typeof (value) === "undefined") {
      return callback(null, value);
    }
    settings.set(key, value, (err, response) => {
      callback(err, response);
    });
  }

  /**
   * @param {string} key
   * @param {Function} callback
   */
  remove(key, callback = (err, res) => {
  }) {
    settings.remove(key, callback);
  }

  /**
   * @param {string} key
   * @param {Function} callback
   */
  contains(key, callback = (err, res) => {
  }) {
    return settings.has(key, callback);
  }
}
