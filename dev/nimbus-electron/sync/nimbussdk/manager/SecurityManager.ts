import {default as settings} from "../../../db/models/settings";

/**
 * @type SecurityManager
 */
let INSTANCE;

export default class SecurityManager {
  /**
   * @type string
   */
  context;

  /**
   * @param {string} context
   */
  constructor(context) {
    this.context = context;
  }

  /**
   * @returns {SecurityManager}
   */
  static get() {
    if (!INSTANCE) {
      INSTANCE = new SecurityManager("nimbus-electron");
    }
    return INSTANCE;
  }

  /**
   * @param {string} accountKey
   * @param {string} sessionId
   * @param {Function} callback
   */
  updateAccount(accountKey, sessionId, callback = (err, res) => {
  }) {
    settings.set(accountKey + "_sessionId", accountKey + '_' + sessionId, () => {
      callback(null, true);
    });
  }

  /**
   * @param {string} accountKey
   * @param {Function} callback
   */
  removeAccount(accountKey) {
    settings.remove(accountKey + "_sessionId");
    settings.remove("user_login");
    settings.remove("user_email");
    settings.remove("user_id");
  }

  /**
   * @param {string} accountKey
   * @param {Function} callback
   */
  getAccount(accountKey, callback = (err, res) => {
  }) {
    let values = {};
    values[0] = null;
    settings.get(accountKey + "_sessionId", (err, value) => {
      values[0] = value && typeof (value) === "string" ? value : null;
      if (values[0] !== null) {
        values[0] = values[0].replace(accountKey + "_", "");
      }
      callback(null, values);
    });
  }
}
