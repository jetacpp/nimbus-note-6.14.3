import chalk from "chalk";
import {default as userSettings} from "../../../db/models/userSettings";
import {default as SyncManager} from "../../process/SyncManager";
import SelectedWorkspace from "../../../workspace/SelectedWorkspace";

let syncTimer = {};
let nextQueueSync = {};

/**
 * @constructor
 */
export default class AutoSyncManager {
  /**
   * @param {{workspaceId:string}} inputData
   */
  static initTimer(inputData) {
    let {workspaceId, initState} = inputData;
    if (!initState) {
      initState = false;
    }
    AutoSyncManager.setTimer({workspaceId, initState});
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static updateTimer(inputData) {
    const {workspaceId} = inputData;
    const initState = false;
    AutoSyncManager.setTimer({workspaceId, initState});
  }

  /**
   * @param {{workspaceId:string, initState:boolean}} inputData
   */
  static async setTimer(inputData) {
    // @ts-ignore
    return new Promise(async function (resolve) {
      const {workspaceId, initState} = inputData;
      let syncInfo = <{syncStatus:boolean, syncTimer:number}>await AutoSyncManager.getSyncInfo(inputData);
      if (syncInfo.syncStatus) {
        AutoSyncManager.clearTimer(inputData);
        const syncTimerHandler = async () => {
          if (!workspaceId && !await SelectedWorkspace.getGlobalId()) {
            AutoSyncManager.makeSync(inputData);
          }

          if (workspaceId && (workspaceId === await SelectedWorkspace.getGlobalId())) {
            AutoSyncManager.makeSync(inputData);
          }
        };

        if (initState) {
          setTimeout(syncTimerHandler, 2000);
        }

        if (typeof (syncTimer[workspaceId]) === 'undefined') {
          syncTimer[workspaceId] = setInterval(syncTimerHandler, 60000 * syncInfo.syncTimer);
        }
      }
      return resolve(true);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static clearTimer(inputData) {
    const {workspaceId} = inputData;
    if (typeof (syncTimer[workspaceId]) !== "undefined" && syncTimer[workspaceId]) {
      clearInterval(syncTimer[workspaceId]);
      delete syncTimer[workspaceId];
    }
  }

  static clearTimers() {
    if (syncTimer) {
      for (let syncTimerKey in syncTimer) {
        if (syncTimer.hasOwnProperty(syncTimerKey) && syncTimer[syncTimerKey]) {
          clearInterval(syncTimer[syncTimerKey]);
          delete syncTimer[syncTimerKey];
        }
      }
    }
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static makeSync(inputData) {
    SyncManager.startSyncOnline(inputData);
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static async getSyncInfo(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      let syncInfo = await userSettings.getSyncInfo(inputData);
      resolve(syncInfo);
    });
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static async checkAutoSyncEnable(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      let syncInfo = <{syncStatus:boolean}>await AutoSyncManager.getSyncInfo(inputData);
      resolve(!!syncInfo.syncStatus);
    });
  }

  /**
   * @return {boolean}
   */
  static getQueueSync(inputData) {
    const {workspaceId} = inputData;
    return typeof (nextQueueSync[workspaceId]) !== "undefined" ? nextQueueSync[workspaceId] : false;
  }

  /**
   * @param {{workspaceId:string, state:boolean}} inputData
   */
  static setQueueSync(inputData) {
    let {workspaceId, state} = inputData;
    state = state || false;
    nextQueueSync[workspaceId] = state;
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static clearQueueSync(inputData) {
    const {workspaceId} = inputData;
    const state = false;
    AutoSyncManager.setQueueSync({workspaceId, state});
  }

  /**
   * @param {{workspaceId:string}} inputData
   */
  static makeQueueSync(inputData) {
    if (AutoSyncManager.getQueueSync(inputData)) {
      AutoSyncManager.makeSync(inputData);
    }
  }
}
