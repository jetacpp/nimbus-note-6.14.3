import chalk from "chalk";
import {default as workspace} from "../db/models/workspace";
import {default as item} from "../db/models/item";
import {default as NimbusSDK} from "../sync/nimbussdk/net/NimbusSDK";
import {default as appWindow} from "../window/instance";
import {default as trans} from "../translations/Translations";
import API from "../sync/nimbussdk/net/API";

const NOTE_OF_FOLDER_IS_OFFLINE = 1;
const NOTE_OF_FOLDER_IS_ENCRYPTED = 2;
const NOTE_OF_FOLDER_IS_NOT_SYNC = 3;
const NOTE_OF_FOLDER_WORKSPACE_DOES_NOT_EXIST = 4;

export default class MoveToWorkspace {
  /**
   * @param {{fromWorkspaceInstance:WorkspaceObj, toWorkspaceInstance:WorkspaceObj, itemInstance:{}, removeSourceItem:boolean}} inputData
   */
  static copy(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {fromWorkspaceInstance, toWorkspaceInstance, itemInstance, removeSourceItem} = inputData;
      NimbusSDK.getApi().moveToWorkspace({
        workspaceId: fromWorkspaceInstance.globalId,
        body: {
          workspaceId: fromWorkspaceInstance.globalId,
          globalId: itemInstance.globalId,
          toWorkspaceId: toWorkspaceInstance.globalId,
          remove: removeSourceItem
        },
        skipSyncHandlers: true
      }, (err, res) => {
        let message = ``;
        if (err) {
          if (err === API.ERROR_QUOTA_EXCEED || err === API.ERROR_ACCESS_DENIED) {
            if(res) {
              if (res._errorDesc === 'attachment_too_large') {
                if (itemInstance.type === 'folder') {
                  message += trans.get('toast__can_not_move_to_workspace__quota_exceed_folder_attachments');
                } else {
                  message += trans.get('toast__can_not_move_to_workspace__quota_exceed_note_attachment');
                }

                if (removeSourceItem) {
                  message += ` ${trans.get('toast__can_not_move_to_workspace__quota_exceed_attachments_user')}`;
                } else {
                  message += ` ${trans.get('toast__can_not_move_to_workspace__quota_exceed_attachments_owner')}`;
                }

                if(message) {
                  res._errorDesc = message
                }
              }
            }
          }

          return resolve({
            err,
            errorDesc: res && res._errorDesc ? res._errorDesc : null,
            newGlobalId: null,
            isElectron: !!message
          });
        }

        return resolve({
          err,
          errorDesc: res && res._errorDesc ? res._errorDesc : null,
          newGlobalId: res.newGlobalId,
          remove: removeSourceItem
        });
      });
    });
  }

  /**
   * @param {{fromWorkspaceId:string, toWorkspaceId:string, globalId:string, isMove:boolean}} inputData
   */
  static process(inputData) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {fromWorkspaceId, toWorkspaceId, globalId, isMove} = inputData;
      const fromWorkspaceInstance = fromWorkspaceId ? await workspace.getById(fromWorkspaceId) : await workspace.getDefault();
      const toWorkspaceInstance = toWorkspaceId ? await workspace.getById(toWorkspaceId) : await workspace.getDefault();

      /**
       * @param {{globalId:string, fromWorkspaceInstance:WorkspaceObj}} inputData
       */
      const findUserItem = (inputData) => {
        // @ts-ignore
        return new Promise(async (resolve) => {
          const {globalId, fromWorkspaceInstance} = inputData;
          const workspaceId = fromWorkspaceInstance.globalId ? await workspace.getLocalId(fromWorkspaceInstance.globalId) : null;
          item.find({globalId}, {workspaceId}, (err, itemInstance) => {
            if (err || !itemInstance) {
              return resolve(null);
            }
            return resolve(itemInstance);
          });
        });
      };

      const itemInstance = <any>await findUserItem({globalId, fromWorkspaceInstance});
      if (!itemInstance) {
        return resolve({
          err: NOTE_OF_FOLDER_WORKSPACE_DOES_NOT_EXIST,
          errorDesc: trans.get('toast__note_folder_source_workspace_not_exist'),
          newGlobalId: null,
          isElectron: true,
        });
      }

      if (itemInstance.offlineOnly) {
        return resolve({
          err: NOTE_OF_FOLDER_IS_OFFLINE,
          errorDesc: trans.get('toast__can_not_move_to_workspace__item_offline'),
          newGlobalId: null,
          isElectron: true,
        });
      }

      if (itemInstance.isEncrypted) {
        return resolve({
          err: NOTE_OF_FOLDER_IS_ENCRYPTED,
          errorDesc: 'contains_encrypted_notes',
          newGlobalId: null,
          isElectron: true,
        });
      }

      if (itemInstance.needSync) {
        return resolve({
          err: NOTE_OF_FOLDER_IS_NOT_SYNC,
          errorDesc: trans.get('toast__can_not_move_to_workspace__item_not_sync'),
          newGlobalId: null,
          isElectron: true,
        });
      }

      const result = await MoveToWorkspace.copy({
        fromWorkspaceInstance,
        toWorkspaceInstance,
        itemInstance,
        removeSourceItem: isMove
      });

      return resolve(result);
    });
  }
}
