import chalk from "chalk";
import {default as workspace} from "../db/models/workspace";
import {default as userSettings} from "../db/models/userSettings";
import {default as appWindow} from "../window/instance";
import AutoSyncManager from "../sync/nimbussdk/manager/AutoSyncManager";
import {default as PouchDb} from "../../pdb";
import {default as user} from "../db/models/user";
import SelectedOrganization from "../organization/SelectedOrganization";
import {default as auth} from "../auth/auth";
import {default as errorHandler} from "../utilities/errorHandler";

let selectedWorkspaceId;

export default class SelectedWorkspace {
  static SETTINGS_KEY = "selected_workspace";

  static async getGlobalId() {
    if(typeof (selectedWorkspaceId) === 'undefined') {
      await SelectedWorkspace.get();
    }

    if (!selectedWorkspaceId) {
      return null;
    }

    return selectedWorkspaceId;
  }

  /**
   * @param {string} globalId
   */
  static setGlobalId(globalId) {
    selectedWorkspaceId = globalId;
  }

  static async get() {
    // @ts-ignore
    return new Promise((resolve) => {
      userSettings.get(SelectedWorkspace.SETTINGS_KEY, async (err, res) => {
        if (err || !res) {
          let defaultWorkspaceInstance = await workspace.getDefault();
          if (typeof (selectedWorkspaceId) === 'undefined') {
            selectedWorkspaceId = null;
          }
          return resolve(defaultWorkspaceInstance);
        }

        workspace.find({globalId: res}, {}, async (err, workspaceInstance) => {
          if (err || !workspaceInstance) {
            let defaultWorkspaceInstance = await workspace.getDefault();
            if (typeof (selectedWorkspaceId) === 'undefined') {
              selectedWorkspaceId = null;
            }
            return resolve(defaultWorkspaceInstance);
          }

          if (await workspace.isMainForUser(workspaceInstance)) {
            if (typeof (selectedWorkspaceId) === 'undefined') {
              selectedWorkspaceId = null;
            }
          } else {
            if (typeof (selectedWorkspaceId) === 'undefined') {
              selectedWorkspaceId = workspaceInstance.globalId;
            }
          }

          resolve(workspaceInstance);
        });
      });
    });
  }

  /**
   * @param {string} workspaceGlobalId
   */
  static async set(workspaceGlobalId) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const prevSelectedWorkspaceId = selectedWorkspaceId;

      if (workspaceGlobalId === workspace.DEFAULT_NAME) {
        workspaceGlobalId = null;
      }

      /**
       * @param {{workspaceId:string, prevWorkspaceId:string}} inputData
       */
      const updateWorkspaceStatus = (inputData) => {
        const {workspaceId} = inputData;
        setTimeout(() => {
          if (appWindow.get()) {
            appWindow.get().webContents.send('event:client:update:workspace:sync:status:response', {workspaceId});
          }
        }, 3000);
        updateWorkspaceSyncStatus(inputData);
      };

      /**
       * @param {{workspaceId:string, prevWorkspaceId:string}} inputData
       */
      const updateWorkspaceSyncStatus = (inputData) => {
        const {workspaceId, prevWorkspaceId} = inputData;
        AutoSyncManager.clearTimer({prevWorkspaceId});
        AutoSyncManager.initTimer({
          workspaceId,
          initState: true
        });
      };

      if (!workspaceGlobalId) {
        if (prevSelectedWorkspaceId === workspaceGlobalId) {
          return resolve(prevSelectedWorkspaceId);
        }

        selectedWorkspaceId = null;
        updateWorkspaceStatus({
          prevWorkspaceId: prevSelectedWorkspaceId,
          workspaceId: selectedWorkspaceId
        });
        return resolve(selectedWorkspaceId);
      }

      // @ts-ignore
      workspace.find({globalId: workspaceGlobalId}, {}, async (err, workspaceInstance) => {
        if (err || !workspaceInstance) {
          workspaceGlobalId = null;
        }

        if (await workspace.isMainForUser(workspaceInstance)) {
          workspaceGlobalId = null;
        }

        if (prevSelectedWorkspaceId === workspaceGlobalId) {
          return resolve(prevSelectedWorkspaceId);
        }

        selectedWorkspaceId = workspaceGlobalId;
        if (selectedWorkspaceId) {
          PouchDb.setWorkspaceDbPath(selectedWorkspaceId);
        }

        updateWorkspaceStatus({
          prevWorkspaceId: prevSelectedWorkspaceId,
          workspaceId: selectedWorkspaceId
        });

        // @ts-ignore
        userSettings.set(SelectedWorkspace.SETTINGS_KEY, selectedWorkspaceId, async () => {
          resolve(selectedWorkspaceId);
        });
      });
    });
  }

  static async getUserWorkspaceIdForOrganization(organization, workspaceId = null) {
    let selectedWorkspace;

    const authInfo = <{variables}>await auth.fetchActualUserAsync();
    const userVariables = authInfo && authInfo.variables ? authInfo.variables : user.getDefaultVariables();

    const lastOpenedWorkspaces = userVariables && userVariables.lastOpenedWorkspaces ? userVariables.lastOpenedWorkspaces : {};
    if(organization && organization.globalId) {
      if(typeof (lastOpenedWorkspaces[organization.globalId]) !== 'undefined' && lastOpenedWorkspaces[organization.globalId]) {
        selectedWorkspace = <{globalId, orgId}>await workspace.getById(lastOpenedWorkspaces[organization.globalId]);

      } else if(typeof (lastOpenedWorkspaces['default']) !== 'undefined' && lastOpenedWorkspaces['default']) {
        selectedWorkspace = <{globalId, orgId}>await workspace.getById(lastOpenedWorkspaces['default']);

      } else {
        selectedWorkspace = <{globalId, orgId}>await workspace.findUserWorkspaceByOrgId(organization.globalId);
      }
    } else {
      if(typeof (lastOpenedWorkspaces['default']) !== 'undefined' && lastOpenedWorkspaces['default']) {
        selectedWorkspace = <{globalId, orgId}>await workspace.getById(lastOpenedWorkspaces['default']);
      } else {
        selectedWorkspace = <{globalId, orgId}>await workspace.getWithQuery({isDefault: true});
      }
    }
    return selectedWorkspace ? selectedWorkspace.globalId : null;
  }

  static async setUserWorkspaceForSelectedOrganization(userInfo, workspaceId = null) {
    const accountUserId = await user.getAccountUserId();

    let activeOrganization = await SelectedOrganization.getCurrentOrganization(userInfo);
    if(!activeOrganization) {
      activeOrganization = {globalId: null, userId: accountUserId};
    }

    const activeWorkspaceId = await SelectedWorkspace.getUserWorkspaceIdForOrganization(activeOrganization, workspaceId);
    await SelectedWorkspace.set(activeWorkspaceId);
  }
}
