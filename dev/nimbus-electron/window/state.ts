import {default as config} from "../../config";
import {default as settings} from "../db/models/settings";
import {default as appWindowInstance} from "./instance";

export default class WindowState {
  /**
   * @param callback Function
   */
  static get(callback) {
    settings.get(config.SETTINGS_APP_WINDOW_STATE_NAME, (err, data) => {
      if (err || !data) {
        return callback(null, {});
      }

      return callback(null, data);
    });
  }

  /**
   * @param data Object
   */
  static set(data) {
    settings.set(config.SETTINGS_APP_WINDOW_STATE_NAME, data);
  }

  /**
   * @returns {number}
   */
  static defaultWidth() {
    return config.APP_WINDOW_DEFAULT_WIDTH;
  }

  /**
   * @returns {number}
   */
  static defaultHeight() {
    return config.APP_WINDOW_DEFAULT_HEIGHT;
  }

  static listenChangeInfo() {
    WindowState.get((err, windowState) => {
      if (!windowState) {
        return false;
      }

      let appWindow = appWindowInstance.get();
      if (appWindow) {
        if (windowState.isMaximized) {
          appWindow.maximize();
        }

        if (windowState.bounds) {
          appWindow.setSize(windowState.bounds.width, windowState.bounds.height, true);
          appWindow.setPosition(windowState.bounds.x, windowState.bounds.y, true);
        }
      }
    });

    ['resize', 'move', 'hide'].forEach((event) => {
      let appWindow = appWindowInstance.get();
      if (appWindow) {
        appWindow.on(event, () => {
          try {
            WindowState.get((err, windowState) => {
              if (appWindow) {
                /**
                 * @type {isMaximized: boolean, bounds: {}}
                 */
                let state = {};
                // @ts-ignore
                state.isMaximized = appWindow.isMaximized();
                // @ts-ignore
                state.bounds = appWindow.getBounds();

                WindowState.set(state);
              }
            });
          } catch (error) {
            if (config.SHOW_WEB_CONSOLE) {
              console.log("App close error: ", error);
            }
          }
        });
      }
    });
  }
}
