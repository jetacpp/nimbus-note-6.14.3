let childWindows = {};

export default class ChildWindow {
  static TYPE_PURCHASE = 'purchaise';
  static TYPE_MESSAGE = 'message';
  static TYPE_ATTACH = 'attach';
  static TYPE_UPDATE = 'update';
  static TYPE_EXPORT = 'export';

  /**
   * @param {string} windowType
   * @param {{Window}} child
   */
  static set(windowType, child) {
    childWindows[windowType] = child;
  }

  /**
   * @param {string} windowType
   * @returns {{}|null}
   */
  static get(windowType) {
    return typeof (childWindows[windowType]) !== 'undefined' ? childWindows[windowType] : null;
  }

  /**
   * @param {string} windowType
   */
  static remove(windowType) {
    if (typeof (childWindows[windowType]) === "undefined") {
      return;
    }

    delete childWindows[windowType];
  }
}
