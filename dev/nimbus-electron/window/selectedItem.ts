import chalk from "chalk";
import {default as socketConnection} from "../sync/socket/socketFunctions";
import {default as attach} from "../db/models/attach";

let selectedItems = {};

export default class SelectedItem {
  static TYPE_NOTE = 'note';
  static TYPE_FOLDER = 'folder';
  static TYPE_TAG = 'tag';

  /**
   * @param {{workspaceId:string, itemType:string, item:*}} inputData
   */
  static set(inputData) {
    const {workspaceId, itemType, item} = inputData;

    if (typeof (selectedItems[workspaceId]) === 'undefined') {
      selectedItems[workspaceId] = {};
    }

    selectedItems[workspaceId][itemType] = item;

    if (itemType === SelectedItem.TYPE_NOTE) {
      SelectedItem.updateSelectedNoteAttachments({workspaceId, globalId: item});
    }
  }

  /**
   * @param {{workspaceId:string, itemType:string}} inputData
   * @return {*|null}
   */
  static get(inputData) {
    const {workspaceId, itemType} = inputData;

    if (typeof (selectedItems[workspaceId]) === 'undefined') {
      return null;
    }

    return typeof (selectedItems[workspaceId][itemType]) !== 'undefined' ? selectedItems[workspaceId][itemType] : null;
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static updateSelectedNoteAttachments(inputData) {
    const {workspaceId, globalId} = inputData;
    let queryData = {noteGlobalId: globalId};
    attach.findAll(queryData, {workspaceId}, (err, attachItems) => {
      if (attachItems && attachItems.length) {
        for (let attachItem of attachItems) {
          socketConnection.sendNoteAttachmentsUpdateMessage({
            workspaceId,
            noteGlobalId: attachItem.noteGlobalId,
            globalId: attachItem.globalId
          });
        }
      }
    });
  }
}
