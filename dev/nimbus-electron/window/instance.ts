let appWindow;

export default class WindowInstance {
  /**
   * @param {{}} windowInstance
   */
  static set(windowInstance) {
    appWindow = windowInstance;
  }

  /**
   * @returns {{}|null}
   */
  static get() {
    return appWindow || null;
  }

  /**
   * @param {BrowserWindow} attachWindow
   * @param {string} windowHtml
   */
  static createAttachVideoWindow(attachWindow, windowHtml) {
    attachWindow.once('ready-to-show', () => {
      setTimeout(() => {
        let executeJavaScript = `document.getElementsByTagName('video')[0].offsetWidth`;
        attachWindow.webContents.executeJavaScript(executeJavaScript, (width) => {
          executeJavaScript = `document.getElementsByTagName('video')[0].offsetHeight`;
          attachWindow.webContents.executeJavaScript(executeJavaScript, (height) => {
            if (width && height) {
              height += 30;
              attachWindow.setSize(width, height, false);
            }
            attachWindow.show();
          });
        });
      }, 100);
    });
    let file = 'data:text/html;charset=UTF-8,' + encodeURIComponent(windowHtml);
    attachWindow.loadURL(file);
  }
}
