import chalk from "chalk";
import {ipcMain, powerMonitor} from 'electron';
import {default as SyncManager} from '../sync/process/SyncManager';

let appOnline;

/**
 * @param {boolean} state
 */
function set(state) {
  appOnline = state;
}

/**
 * @return boolean
 */
function get() {
  return appOnline;
}

function listenChangeStatus() {
  ipcMain.on('event:online:changed', (event, arg) => {
    set(arg.online);
    if (!arg.online) {
      SyncManager.stopAllSync();
    }
  });

  powerMonitor.on('suspend', () => {
    SyncManager.stopAllSync();
  });
}

export default {
  set,
  get,
  listenChangeStatus
}
