let purchaseProducts;

export default class PurchaseProduct {
  /**
   * @param {[Product]} products
   */
  static set(products) {
    purchaseProducts = products;
  }

  /**
   * @returns {[Product]}
   */
  static get() {
    return purchaseProducts || [];
  }
}
