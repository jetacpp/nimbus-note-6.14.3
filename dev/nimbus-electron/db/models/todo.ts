import chalk from "chalk";
import {default as date} from "../../utilities/dateHandler";
import {default as generator} from "../../utilities/generatorHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";

let dbName = config.DB_TODO_MODEL_NAME;

export default class ModelTodo {
  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static count(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.count(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.findAll(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.noteGlobalId || !data.label) {
      return callback(error.wrongInput(), null);
    }

    options.dbName = dbName;
    options.prepareModelData = ModelTodo.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.noteGlobalId || !data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (data.globalId || data.noteGlobalId) {
      options.dbName = dbName;
      PouchDb.remove(data, options, callback);
    } else {
      return callback(error.wrongInput(), 0);
    }
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static removeByItemId(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.noteGlobalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicData(doc) {
    let todo = <any>{};
    if (doc) {
      todo.id = doc._id;
      todo.globalId = doc.globalId;
      todo.noteGlobalId = doc.noteGlobalId;
      todo.date = doc.date;
      todo.checked = doc.checked;
      todo.label = doc.label;
      todo.dateUpdated = doc.dateUpdated;

      //TODO: add properties from sync models
      if (doc.parentId)
        todo.parentId = doc.parentId;
      if (doc.dateAdded)
        todo.dateAdded = doc.dateAdded;
      if (doc.uniqueUserName)
        todo.uniqueUserName = doc.uniqueUserName;
      if (doc.syncDate)
        todo.syncDate = doc.syncDate;
      if (doc.needSync)
        todo.needSync = doc.needSync;

      if (doc.orderNumber)
        todo.orderNumber = doc.orderNumber;
    }

    return todo;
  }

  /**
   * @param {{}} data
   * @return {{}|null}
   */
  static getResponseJson(data) {
    if (!data) {
      return null;
    }

    return {
      checked: data.checked,
      date: data.date,
      dateUpdated: data.dateUpdated,
      globalId: data.globalId,
      label: data.label,
      noteGlobalId: data.noteGlobalId
    };
  }

  /**
   * @param {[]} list
   * @return {[]}
   */
  static getResponseListJson(list) {
    return list.map(function (data) {
      return ModelTodo.getResponseJson(data);
    });
  }

  /**
   * @param {{}} data
   * @return {{}}
   */
  static prepareModelData(data) {
    let item = {};

    item = ModelTodo.prepareCommonProperties(item, data);
    item = ModelTodo.prepareSyncOnlyProperties(item, data);

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    let curDate = date.now();

    item.erised = data.erised || false;
    item.globalId = data.globalId || data.global_id || item._id || generator.randomString(16);
    item._id = item.globalId.toString();
    item.noteGlobalId = data.noteGlobalId || data.parentId || "";
    item.date = data.date || date.now();
    item.checked = !!data.checked;
    item.label = data.label;
    item.dateUpdated = data.dateUpdated || curDate || 0;
    if (typeof (data.orderNumber) !== "undefined") {
      item.orderNumber = data.orderNumber;
    }

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareSyncOnlyProperties(item, data) {
    let curDate = date.now();

    item.global_id = data.global_id || data.globalId || item._id;
    item.parentId = data.parentId || item.noteGlobalId || "";
    item.dateAdded = data.dateAdded || curDate || 0;
    item.uniqueUserName = data.uniqueUserName || "";
    item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
    item.syncDate = data.syncDate || 0;
    if (typeof (data.orderNumber) !== "undefined") {
      item.orderNumber = data.orderNumber;
    }

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["globalId", "noteGlobalId", "checked", "label", "orderNumber"];
  }
}
