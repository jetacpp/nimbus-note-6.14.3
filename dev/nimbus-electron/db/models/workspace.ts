import chalk from "chalk";
import {default as date} from "../../utilities/dateHandler";
import {default as generator} from "../../utilities/generatorHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";
import ModelOrgs, {default as orgs} from "./orgs";
import {default as workspaceMember} from "./workspaceMember";
import {default as downloadWorkspaces} from "../../sync/process/rx/handlers/downloadWorkspaces";
import {default as auth} from "../../auth/auth";
import {default as urlParser} from "../../utilities/urlParser";
import fs from "fs-extra";

let dbName = config.DB_WORKSPACE_MODEL_NAME;

let defaultWorkspace;

export default class ModelWorkspace {
  static DEFAULT_NAME = 'default';

  static PRIVILEGE_CAN_READ = 'canRead';
  static PRIVILEGE_CAN_EDIT = 'canEdit';
  static PRIVILEGE_CAN_COMMENT = 'canComment';
  static PRIVILEGE_CAN_ACCESS_ENCRYPTED_NOTES = 'canAccessEncryptedNotes';
  static PRIVILEGE_CAN_ENCRYPT_NOTES = 'canEncryptNotes';
  static PRIVILEGE_CAN_MANAGE_ENCRYPTION_KEYS = 'canManageEncryptionKeys';

  static ERROR_QUOTA_EXCEED = -20;

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static count(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.count(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.findAll(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.title) {
      return callback(error.wrongInput(), null);
    }

    options.dbName = dbName;
    options.prepareModelData = ModelWorkspace.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (data.globalId) {
      options.dbName = dbName;
      PouchDb.remove(data, options, callback);
    } else {
      return callback(error.wrongInput(), 0);
    }
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicData(doc) {
    let workspace = <any>{};
    if (doc) {
      workspace.id = doc._id;
      workspace.globalId = doc.globalId;
      workspace.createdAt = doc.createdAt;
      workspace.updatedAt = doc.updatedAt;

      //TODO: add properties from sync models
      if (doc.title)
        workspace.title = doc.title;
      if (doc.orgId)
        workspace.orgId = doc.orgId;
      if (doc.userId)
        workspace.userId = doc.userId;
      if (doc.isDefault)
        workspace.isDefault = doc.isDefault;
      if (doc.countMembers)
        workspace.countMembers = doc.countMembers;
      if (doc.countInvites)
        workspace.countInvites = doc.countInvites;
      if (doc.access)
        workspace.access = doc.access;
      if (doc.lastUpdateTime) {
        workspace.lastUpdateTime = doc.lastUpdateTime;
      }
      if (doc.reason) {
        workspace.reason = doc.reason;
      }
      if (doc.defaultEncryptionKeyId) {
        workspace.defaultEncryptionKeyId = doc.defaultEncryptionKeyId;
      }

      if (doc.syncDate)
        workspace.syncDate = doc.syncDate;
      if (doc.needSync)
        workspace.needSync = doc.needSync;

      workspace.existOnServer = doc.existOnServer || false;

      if (doc.members)
        workspace.members = doc.members;
      if (doc.invites)
        workspace.invites = doc.invites;

      if(doc.isNotesLimited)
        workspace.isNotesLimited = doc.isNotesLimited;
    }

    return workspace;
  }

  /**
   * @param {{}} data
   * @return {{}|null}
   */
  static getResponseJson(data) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (!data) {
        return resolve(null);
      }

      const userInfo = <any>await auth.getUserAsync();
      const orgInstance = <any>await ModelWorkspace.getWorkspaceOrgData(data);

      const usage = {
        traffic: orgInstance.usage.traffic,
        encryptionKeys: {
          current: 0,
          max: orgInstance.limits.encryptionKeysPerWorkspace
        },
        members: {
          current: data.countMembers || 0,
          max: orgInstance.limits.membersPerWorkspace
        }
      };

      let owner = {
        id: data.user ? data.user.id : 0,
        email: data.user && data.user.email ? data.user.email.toLowerCase() : '',
        username: data.user && data.user.username ? data.user.username : '',
        firstname: data.user && data.user.firstname ? data.user.firstname : '',
        lastname: data.user && data.user.lastname ? data.user.lastname : '',
        displayName: data.user && data.user.username ? data.user.username : '',
        avatar: data.user && data.user.avatar ? data.user.avatar : null,
        defaultEncryptionKeyId: data ? data.defaultEncryptionKeyId : ''
      };

      let isPersonal = false;
      if(data.user && data.user.email && data.user.email.toLowerCase() === userInfo.email) { isPersonal = true; }

      if(!data.user && orgInstance && orgInstance.type === orgs.TYPE_PRIVATE) {
        if(orgInstance.user && orgInstance.user.email) {
          if(orgInstance.user.email.toLowerCase() === userInfo.email) {
            owner = {
              id: orgInstance.user ? orgInstance.user.id : userInfo.id,
              email: userInfo.email ? userInfo.email.toLowerCase() : '',
              username: userInfo.username ? userInfo.username : '',
              firstname: userInfo.firstname ? userInfo.firstname : '',
              lastname: userInfo.lastname ? userInfo.lastname : '',
              displayName: userInfo.username ? userInfo.username : '',
              avatar: userInfo.avatar && userInfo.avatar.url ? userInfo.avatar.url : null,
              defaultEncryptionKeyId: data ? data.defaultEncryptionKeyId : ''
            };
            isPersonal = true;
          } else {
            owner = {
              id: orgInstance.user ? orgInstance.user.id : null,
              email: orgInstance.user && orgInstance.user.email ? orgInstance.user.email.toLowerCase() : '',
              username: orgInstance.user && orgInstance.user.username ? orgInstance.user.username : '',
              firstname: orgInstance.user && orgInstance.user.firstname ? orgInstance.user.firstname : '',
              lastname: orgInstance.user && orgInstance.user.lastname ? orgInstance.user.lastname : '',
              displayName: orgInstance.user && orgInstance.user.username ? orgInstance.user.username : '',
              avatar: orgInstance.user && orgInstance.user.avatar ? orgInstance.user.avatar : null,
              defaultEncryptionKeyId: data ? data.defaultEncryptionKeyId : ''
            };
          }
        }
      }

      let access = null;
      if (data.access && userInfo && userInfo.email && owner && owner.email &&
          userInfo.email.toLowerCase() !== owner.email.toLowerCase()) {
        let privileges = data.access.privileges ? data.access.privileges : [];
        if (data.access.member && data.access.member.encryptRole) {
          switch (data.access.member.encryptRole) {
            case workspaceMember.ENCRYPTION_ROLE_ACCESSOR: {
              privileges.push('canAccessEncryptedNotes');
              break;
            }
            case workspaceMember.ENCRYPTION_ROLE_ENCRYPTOR: {
              privileges.push('canAccessEncryptedNotes');
              privileges.push('canEncryptNotes');
              break;
            }
            case workspaceMember.ENCRYPTION_ROLE_MANGER: {
              privileges.push('canAccessEncryptedNotes');
              privileges.push('canEncryptNotes');
              privileges.push('canManageEncryptionKeys');
              break;
            }
          }
        }

        access = {
          workspaceId: data.globalId,
          access: {
            type: data.access.member ? 'member' : '',
            role: data.access.role ? data.access.role : '',
            encryptRole: data.access.member && data.access.member.encryptRole ? data.access.member.encryptRole : workspaceMember.ENCRYPTION_ROLE_DENY,
            privileges: privileges,
            reduced: false
          }
        };
      }

      const result = {
        id: data.id || null,
        globalId: data.globalId,
        orgId: data.orgId,
        userId: data.userId,
        title: data.title,
        isDefault: data.isDefault || false,
        defaultEncryptionKeyId: data.defaultEncryptionKeyId,
        createdAt: data.createdAt,
        updatedAt: data.updatedAt,
        usage: usage,
        owner: owner,
        access: access,
        isNotesLimited: data.isNotesLimited || false,
        isDefaultForCurrentUser: await ModelWorkspace.isMainForUser(data),
        isPersonal,
        avatar: await ModelWorkspace.getWorkspaceAvatar(data),
        color: data.color ? data.color : null,
      };

      return resolve(result);
    });
  }

  static async getWorkspaceAvatar(data) {
    let response = {storedFileUUID: null};
    if(data.avatar) {
      const {url} = data.avatar;
      if(url) {
        let routeParams = urlParser.getPathParams(url);
        let storedFileUUID = typeof (routeParams[2] !== 'undefined') ? routeParams[2] : null;
        if(storedFileUUID) {
          let targetPath = `${PouchDb.getClientAttachmentPath()}/${storedFileUUID}`;
          // @ts-ignore
          let exists = await fs.exists(targetPath);
          // @ts-ignore
          if (exists) {
            response.storedFileUUID = `avatar/${storedFileUUID}`;
          }
        }
      }
      return response;
    }
  }

  /**
   * @param {{}} data
   * @return {{}}
   */
  static getWorkspaceOrgData(data) {
    return new Promise(async (resolve) => {
      let orgInstance = <any>await orgs.getById(data.orgId);

      if (!orgInstance) {
        const isBusinessOrg = data.orgId && data.orgId.charAt(0) === 'b';
        if(!isBusinessOrg && data.org) {
          orgInstance = data.org;
        }
      }

      if(!orgInstance) {
        orgInstance = await orgs.getDefaultOrg();
      }

      return resolve(orgInstance);
    });
  };

  /**
   * @param {[]} list
   * @return {[]}
   */
  static async getResponseListJson(list) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      let result = [];
      for (let data of list) {
        const jsonData = await ModelWorkspace.getResponseJson(data);
        result.push(jsonData);
      }
      return resolve(result);
    });
  }

  static async getMentionResponseJson(data) {
    const {members} = data;
    const jsonData = <any>await ModelWorkspace.getResponseJson(data);
    if(!jsonData) {
      return null;
    }

    let {globalId, access, userId, updatedAt, createdAt, owner, orgId, isPersonal} = jsonData;
    if(!access) {
      access = data.access;
    }

    if(!access) {
      return null;
    }

    const mentionData = {
      addedByUserId: owner ? owner.id : 0,
      createdAt,
      encryptRole: access.encryptRole,
      globalId,
      orgId,
      privileges: access.privileges,
      role: access.role,
      type: access.type,
      updatedAt,
      userId,
      workspaceId: globalId,
    };

    let membersCount = members ? members.length : 0;
    if(isPersonal && membersCount) {
      membersCount = membersCount - 1;
    }

    let result = [];
    for(let i = 0; i < membersCount; i++) {
      result.push(mentionData);
    }

    return result;
  }

  static getDefaultAccess() {
    return {
      role: 'admin',
      privileges: [
        "canAdmin",
        "canDelete",
        "canEdit",
        "canChangePreview",
        "canChangeColor",
        "canCreateNote",
        "canShare",
        "canChangeReminder",
        "canChangeTodos",
        "canChangeAttachments",
        "canChangeText",
        "canRead"
      ],
      member: 0
    }
  }

  /**
   * @param {{}} data
   * @return {{}}
   */
  static prepareModelData(data) {
    let item = {};

    item = ModelWorkspace.prepareCommonProperties(item, data);
    item = ModelWorkspace.prepareSyncOnlyProperties(item, data);

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    let curDate = date.now();

    item.erised = data.erised || false;
    item.globalId = data.globalId || data.global_id || item._id || generator.randomString(16);
    item.title = data.title || '';
    item._id = item.globalId.toString();
    item.orgId = data.orgId || '';
    item.org = data.org || null;
    item.userId = data.userId || null;
    item.isDefault = data.isDefault || data.default || false;
    item.countMembers = data.countMembers || 0;
    item.countInvites = data.countInvites || 0;
    item.access = data.access || {
      role: '',
      privileges: [],
      memberId: 0
    };
    item.lastUpdateTime = data.lastUpdateTime || 0;
    item.reason = data.reason || '';
    item.defaultEncryptionKeyId = data.defaultEncryptionKeyId || null;
    item.createdAt = data.createdAt || curDate;
    item.updatedAt = data.updatedAt || curDate;
    item.existOnServer = data.existOnServer || false;
    item.members = data.members || [];
    item.invites = data.invites || [];
    item.isNotesLimited = data.isNotesLimited || false;
    item.user = data.user || null;
    item.notesEmail = data.notesEmail || '';
    item.avatar = data.avatar || null;
    item.color = data.color || null;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareSyncOnlyProperties(item, data) {
    item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
    item.syncDate = data.syncDate || 0;

    if (typeof (data.title) !== 'undefined') {
      item.title = data.title;
    }
    if (typeof (data.userId) !== 'undefined') {
      item.userId = data.userId;
    }
    if (typeof (data.org) !== 'undefined') {
      item.org = data.org;
    }
    if (typeof (data.default) !== 'undefined' || typeof (data.isDefault) !== 'undefined') {
      item.isDefault = data.isDefault || data.default;
    }
    if (typeof (data.countMembers) !== 'undefined') {
      item.countMembers = data.countMembers;
    }
    if (typeof (data.countInvites) !== 'undefined') {
      item.countInvites = data.countInvites;
    }
    if (typeof (data.access) !== 'undefined') {
      item.access = data.access;
    }
    if (typeof (data.lastUpdateTime) !== 'undefined') {
      item.lastUpdateTime = data.lastUpdateTime;
    }
    if (typeof (data.reason) !== 'undefined') {
      item.reason = data.reason;
    }
    if (typeof (data.defaultEncryptionKeyId) !== 'undefined') {
      item.defaultEncryptionKeyId = data.defaultEncryptionKeyId;
    }
    if (typeof (data.createdAt) !== 'undefined') {
      item.createdAt = data.createdAt;
    }
    if (typeof (data.updatedAt) !== 'undefined') {
      item.updatedAt = data.updatedAt;
    }
    if (typeof (data.existOnServer) !== 'undefined') {
      item.existOnServer = data.existOnServer;
    }
    if (typeof (data.members) !== 'undefined') {
      item.members = data.members;
    }
    if (typeof (data.invites) !== 'undefined') {
      item.invites = data.invites;
    }
    if(typeof (data.isNotesLimited) !== 'undefined') {
      item.isNotesLimited = data.isNotesLimited;
    }
    if (typeof (data.user) !== 'undefined') {
      item.user = data.user;
    }
    if (typeof (data.notesEmail) !== 'undefined') {
      item.notesEmail = data.notesEmail;
    }
    if (typeof (data.avatar) !== 'undefined') {
      item.avatar = data.avatar;
    }
    if (typeof (data.color) !== 'undefined') {
      item.color = data.color;
    }

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["globalId", "orgId", "role", "disabled", "updatedAt"];
  }

  static async getDefaultWorkspaceData() {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const defaultOrg = await orgs.getDefaultOrg();
      const ogrId = defaultOrg ? defaultOrg.id : '';
      let defaultWorkspaceData = {
        globalId: 'default',
        title: 'Default workspace',
        orgId: ogrId,
        userId: null,
        isDefault: true,
        countMembers: 0,
        countInvites: 0,
        access: {
          role: '',
          privileges: [],
          memberId: 0
        },
        defaultEncryptionKeyId: null,
        lastUpdateTime: 0,
        createdAt: 0,
        updatedAt: 0,
        members: [],
        invites: [],
        isNotesLimited: false,
        avatar: {storedFileUUID: null},
        color: null,
      };

      return resolve(defaultWorkspaceData);
    });
  }

  /**
   * @return {Promise<{}>}
   */
  static async getDefault() {
    // @ts-ignore
    return new Promise(async (resolve) => {
      ModelWorkspace.findAll({isDefault: true}, {}, async (err, workspaceInstances) => {
        if (err || !workspaceInstances) {
          return resolve(await ModelWorkspace.getDefaultWorkspaceData());
        }

        let mainUserWorkspace = null;
        for (let workspaceInstance of workspaceInstances) {
          if (await ModelWorkspace.isMainForUser(workspaceInstance)) {
            mainUserWorkspace = workspaceInstance;
            break;
          }
        }

        if (!mainUserWorkspace) {
          return resolve(await ModelWorkspace.getDefaultWorkspaceData());
        }

        return resolve(mainUserWorkspace);
      });
    });
  }

  /**
   * @return {Promise<{}>}
   */
  static async getWithQuery(query = {}) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      ModelWorkspace.find(query, {}, async (err, workspaceInstance) => {
        if (err || !workspaceInstance) {
          return resolve(null);
        }

        return resolve(workspaceInstance);
      });
    });
  }

  /**
   * @param {string} workspaceId
   */
  static async getLocalId(workspaceId):Promise<string|null> {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (typeof(workspaceId) === 'string' && workspaceId == 'null') {
        return resolve(null);
      }

      if (workspaceId === ModelWorkspace.DEFAULT_NAME) {
        return resolve(null);
      }

      if (!workspaceId) {
        return resolve(null);
      }

      if (!defaultWorkspace) {
        if (typeof (defaultWorkspace) === 'undefined') {
          defaultWorkspace = await ModelWorkspace.getDefault();
        }
      }

      if (!defaultWorkspace) {
        return resolve(null);
      }

      const result = defaultWorkspace.globalId === workspaceId ? null : workspaceId;
      return resolve(result);
    });
  }

  static clearDefaultWorkspace() {
    defaultWorkspace = undefined;
  }

  /**
   * @returns {Promise<[string]>}
   */
  static async getAvailableIdList() {
    // @ts-ignore
    return new Promise(async (resolve) => {
      let result = [null];
      ModelWorkspace.findAll({}, {}, async (err, workspaceList) => {
        if (err || !workspaceList) {
          return resolve(result);
        }

        if (!workspaceList.length) {
          return resolve(result);
        }

        for (let workspaceInstance of workspaceList) {
          if (!await ModelWorkspace.isMainForUser(workspaceInstance)) {
            result.push(workspaceInstance.globalId);
          }
        }

        return resolve(result);
      });
    });
  }

  static getUserWorkspacesCount() {
    // @ts-ignore
    return new Promise((resolve) => {
      ModelWorkspace.findAll({}, {}, (err, workspacesList) => {
        if (err || !workspacesList.length) {
          return resolve(0);
        }
        return resolve(workspacesList.length);
      });
    });
  }

  static syncUserWorkspaces() {
    // @ts-ignore
    return new Promise(async (resolve) => {
      downloadWorkspaces({skipSyncHandlers: true}, () => {
        return resolve();
      });
    });
  }

  /**
   * @param {WorkspaceObj} workspace
   */
  static async isUserWorkspace(workspace) {
    // @ts-ignore
    return new Promise((resolve) => {
      orgs.find({id: workspace.orgId}, {}, async (err, orgInstance) => {
        if (err || !orgInstance) {
          return orgs.count({}, {}, (err, orgsCount) => {
            if (err || !orgsCount) {
              return resolve(workspace.isDefault);
            }

            return resolve(false);
          });
        }

        if (!orgInstance.user) {
          return resolve(false);
        }

        if (!orgInstance.user.email) {
          return resolve(false);
        }

        const userInfo = <{email}>await auth.getUserAsync();
        if(!userInfo) {
          return resolve(false);
        }

        if (userInfo.email.toLowerCase() !== orgInstance.user.email.toLowerCase()) {
          return resolve(false);
        }

        return resolve(true);
      });
    });
  }

  /**
   * @param {WorkspaceObj} workspace
   */
  static async isMainForUser(workspace) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (!workspace) {
        return resolve(false);
      }

      if (!workspace.isDefault) {
        return resolve(false);
      }

      if (workspace.globalId === 'default') {
        return resolve(true);
      }

      const userInfo = <{email}>await auth.getUserAsync();
      if(!userInfo) { return resolve(false); }
      if(!userInfo.email) { return resolve(false); }
      const orgInstance = <any>await ModelWorkspace.getWorkspaceOrgData(workspace);
      let isPersonal = workspace.user && workspace.user.email && workspace.user.email.toLowerCase() === userInfo.email;
      if(!workspace.user && orgInstance && orgInstance.type === orgs.TYPE_PRIVATE) {
        isPersonal = orgInstance.user && orgInstance.user.email && orgInstance.user.email.toLowerCase() === userInfo.email;
      }
      const isBusinessOrg = workspace.orgId && workspace.orgId.charAt(0) === 'b';
      const isMainWorkspaceForUser = workspace.isDefault && isPersonal && !isBusinessOrg;
      return resolve(isMainWorkspaceForUser);
      // const isUserWorkspace = await ModelWorkspace.isUserWorkspace(workspace);
      // return resolve(isUserWorkspace);
    });
  }

  /**
   * @param {string} workspaceId
   */
  static async getById(workspaceId) {
    // @ts-ignore
    return new Promise((resolve) => {
      ModelWorkspace.find({globalId: workspaceId}, {}, (err, workspaceItem) => {
        if (err || !workspaceItem) {
          return resolve(null);
        }
        return resolve(workspaceItem);
      });
    });
  };

  /**
   * @param {string} workspaceId
   */
  static getPublicPremium(workspaceId, originalWorkspaceId) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const orgsInstance = <any>await ModelOrgs.getByWorkspaceId(workspaceId);
      if(orgsInstance && orgsInstance.user) {

        const {premium, id} = orgsInstance.user;
        if(premium) {
          const {type, status, startDate, endDate, isBusiness} = premium;
          const inheritedFromOrg = orgsInstance.type === ModelOrgs.TYPE_BUSINESS ? [orgsInstance.id] : [];
          return resolve({
            inheritedFromOrg,
            type,
            startDate,
            endDate,
            status,
            userId: id,
            isBusiness,
          });
        }
      }

      const workspaceInstance = <any>await ModelWorkspace.getById(originalWorkspaceId);
      if(workspaceInstance) {
        let wsUser = workspaceInstance.org && workspaceInstance.org.user ? workspaceInstance.org.user : null;
        if(!wsUser) { wsUser = workspaceInstance.user ? workspaceInstance.user : null; }

        if(wsUser) {
          const {premium, id} = wsUser;
          if(premium) {
            const {type, status, startDate, endDate, isBusiness} = premium;
            const inheritedFromOrg = workspaceInstance.org && workspaceInstance.org.type === ModelOrgs.TYPE_BUSINESS ? [workspaceInstance.org.id] : [];
            return resolve({
              inheritedFromOrg,
              type,
              startDate,
              endDate,
              status,
              userId: id,
              isBusiness,
            });

          } else {

            if(workspaceInstance.org && workspaceInstance.org.access) {
              if( workspaceInstance.org.type === ModelOrgs.TYPE_BUSINESS &&
                  workspaceInstance.org.access.role &&
                  workspaceInstance.org.access.role !== workspaceMember.ROLE_ADMIN) {
                return resolve({
                  inheritedFromOrg: [workspaceInstance.org.id],
                  type: '',
                  startDate: null,
                  endDate: null,
                  status: 'active',
                  userId: id,
                  isBusiness: true,
                });
              }
            }

          }
        }
      }

      return resolve({
        dateEnd: null,
        status: null
      });
    });
  }

  static findUserWorkspaces() {
    return new Promise((resolve) => {
      ModelWorkspace.findAll({}, {}, async (err, workspacesList) => {
        if (err || !workspacesList.length) {
          const defaultWorkspace = await ModelWorkspace.getDefault();
          workspacesList = [defaultWorkspace];
        }
        return resolve(workspacesList);
      });
    });
  }

  static findUserWorkspaceByOrgId(orgId) {
    return new Promise((resolve) => {
      ModelWorkspace.find({orgId}, {}, async (err, workspaceItem) => {
        const result = workspaceItem || null;
        return resolve(result);
      });
    });
  }

  static countUserWorkspacesByOrgId(orgId) {
    return new Promise((resolve) => {
      ModelWorkspace.count({orgId}, {}, async (err, workspacesCount) => {
        const result = workspacesCount || 0;
        return resolve(result);
      });
    });
  }

}
