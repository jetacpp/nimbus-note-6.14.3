import chalk from "chalk";
import {default as generator} from "../../utilities/generatorHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";
import {default as date} from "../../utilities/dateHandler";
import {default as textHandler} from "../../utilities/textHandler";
import {default as orgs} from "./orgs";
import {default as workspace} from "./workspace";

let dbName = config.DB_ITEM_MODEL_NAME;
let dbTextName = config.DB_TEXT_MODEL_NAME;

export default class ModelItem {
  static ITEM_SCREENSHOT = 'screenshot';
  static ITEM_SCREENCAST = 'screencast';
  static ITEM_WELCOME_GUIDE = 'welcome_guide';
  static ITEM_STREAMING_VIDEO = 'streaming_video';

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static count(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.count(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static async findAll(data, options, callback = (err, res) => {
  }) {
    let findParams = <any>{selector: {}};
    if (options.sortQuery) {
      for (let i in options.sortQuery) {
        if (options.sortQuery.hasOwnProperty(i)) {
          let sortOrder = options.sortQuery[i] < 0 ? "desc" : "asc";

          if (i === "dateUpdated") {
            findParams.sort = [{"dateUpdated": sortOrder}];
          } else if (i === "dateAdded") {
            findParams.sort = [{"dateAdded": sortOrder}];
          } else if (i === "title") {
            findParams.sort = [{"title": sortOrder}];
          }
        }
      }
    }

    if (typeof (options.limitValue) !== "undefined" && options.limitValue > 0) {
      findParams.limit = options.limitValue;
    }

    if (typeof (options.offsetValue) !== "undefined" && options.offsetValue > 0) {
      findParams.skip = options.offsetValue;
    }

    const onlyTitleSearch = data && data.onlyTitleSearch;
    if(data && typeof (data['onlyTitleSearch']) !== 'undefined') {
      delete data['onlyTitleSearch'];
    }

    for (let g in data) {
      if (data.hasOwnProperty(g)) {
        findParams.selector[g] = data[g];
      }
    }

    if (findParams) {
      options.findParams = findParams;
    }

    options.dbName = dbName;

    if (!data.title || onlyTitleSearch) {
      return PouchDb.findAll(data, options, callback);
    }

    let textOptions = <any>{};
    textOptions.dbName = dbTextName;
    textOptions.workspaceId = options.workspaceId;

    const searchByTagIdList = options.searchParams ? options.searchParams.searchByTagIdList : null;
    const searchByStringIdList = options.searchParams ? options.searchParams.searchByStringIdList : null;

    let noteIdList = [];

    if(searchByStringIdList) {
      for (let findId of searchByStringIdList) {
        noteIdList.push(findId);
      }
    } else {
      const textQuery = {text: data.title};
      const docs = <[{noteGlobalId:string}]>await ModelItem.serachByText(textQuery, textOptions);
      for (let i in docs) {
        if (!docs.hasOwnProperty(i)) return;
        noteIdList.push(docs[i].noteGlobalId);
      }
    }

    // @ts-ignore
    let noteData = Object.assign({}, findParams.selector);
    let textData = <any>{globalId: {'$in': noteIdList}};

    if (data.rootId) {
      textData.rootId = data.rootId;
    }

    if (data.parentId) {
      textData.parentId = data.parentId;
    }

    options.findParams.selector = {};

    if(searchByStringIdList && searchByTagIdList) {
      let tagsData = <any>{globalId: {'$in': searchByTagIdList}};
      options.findParams.selector.$and = [{"$or": [noteData, textData]}, tagsData];
    } else {
      options.findParams.selector.$or = [noteData, textData];
    }

    PouchDb.findAll(options.findParams.selector, options, (err, docsByTitle) => {
      return callback(err, docsByTitle);
    });
  }

  static serachByText(textData, textOptions) {
    return new Promise((resolve) => {
      PouchDb.findAll(textData, textOptions, (err, docs) => {
        if(err || !docs) {
          return resolve([]);
        }
        return resolve(docs);
      });
    });
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.parentId || !data.globalId || !data.type || typeof (data.title) === "undefined") {
      return callback(error.wrongInput(), null);
    }

    if (!data.title) {
      data.title = ModelItem.getDefaultTitle();
    }

    options.dbName = dbName;
    options.prepareModelData = ModelItem.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    PouchDb.remove(data, options, callback);
  }

  /**
   * @param {{}} globalId
   * @param {{}} options
   * @param {Function} callback
   */
  static getApiNote(globalId, options, callback = (err, res) => {
  }) {
    if (!globalId) {
      return callback(error.wrongInput(), {});
    }

    let queryData = {globalId: globalId};
    ModelItem.find(queryData, options, (err, itemInstance) => {
      callback(null, ModelItem.getResponseJson(itemInstance));
    });
  }

  /**
   * @param {{}} title
   * @return {string}
   */
  static prepareNoteTitle(title) {
    if (title) {
      title = textHandler.stripTags(title);
    }

    return title ? title : ModelItem.getDefaultTitle();
  }

  /**
   * @return {string}
   */
  static getDefaultTitle() {
    return "Unnamed Note";
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicData(doc) {
    let item = <any>{};

    if (doc) {
      item.id = doc._id;
      item.rootId = doc.rootId;
      item.globalId = doc.globalId;
      item.parentId = doc.parentId;
      item.rootParentId = doc.rootParentId;
      item.createdAt = doc.createdAt;
      item.dateAdded = doc.dateAdded;
      item.dateUpdated = doc.dateUpdated;
      item.updatedAt = doc.updatedAt;
      item.dateUpdatedUser = doc.dateUpdatedUser;
      item.type = doc.type;
      item.role = doc.role;
      item.title = doc.title;
      item.url = doc.url;
      item.locationLat = doc.locationLat;
      item.locationLng = doc.locationLng;
      item.shared = doc.shared;
      item.favorite = doc.favorite;
      item.lastChangeBy = doc.lastChangeBy;
      item.size = doc.size;
      item.editnote = doc.editnote;
      item.isEncrypted = doc.isEncrypted;
      if (doc.offlineOnly)
        item.offlineOnly = doc.offlineOnly;
      if (doc.folderDepth)
        item.folderDepth = doc.folderDepth;
      if (doc.folderList)
        item.folderList = doc.folderList;
      if (doc.titleTree)
        item.titleTree = doc.titleTree;
      if (doc.color)
        item.color = doc.color;
      if(doc.is_imported)
        item.is_imported = doc.is_imported;

      //FOLDER properties
      if (doc.index)
        item.index = doc.index;
      if (doc.existOnServer)
        item.existOnServer = doc.existOnServer;
      if (doc.syncDate)
        item.syncDate = doc.syncDate;
      if (doc.uniqueUserName)
        item.uniqueUserName = doc.uniqueUserName;
      if (doc.needSync)
        item.needSync = doc.needSync;
      if (doc.isMaybeInTrash)
        item.isMaybeInTrash = doc.isMaybeInTrash;
      if (doc.isClicked)
        item.isClicked = doc.isClicked;
      if (doc.subfoldersCount)
        item.subfoldersCount = doc.subfoldersCount;
      if (doc.notesCount)
        item.notesCount = doc.notesCount;
      if (doc.level)
        item.level = doc.level;

      //NOTE properties
      if (doc.isTemp)
        item.isTemp = doc.isTemp;
      if (doc.shortText)
        item.shortText = doc.shortText;
      if (doc.firstImage)
        item.firstImage = doc.firstImage;
      if (doc.isMoreThanLimit)
        item.isMoreThanLimit = doc.isMoreThanLimit;
      if (doc.tags)
        item.tags = doc.tags;
      if (doc.color)
        item.color = doc.color;
      if (doc.textAttachmentGlobalId)
        item.textAttachmentGlobalId = doc.textAttachmentGlobalId;
      if (doc.attachmentsInListCount)
        item.attachmentsInListCount = doc.attachmentsInListCount;
      if (doc.attachmentsInListExist)
        item.attachmentsInListExist = doc.attachmentsInListExist;
      if (doc.reminderExist)
        item.reminderExist = doc.reminderExist;
      if (doc.todoExist)
        item.todoExist = doc.todoExist;
      if (doc.reminderLabel)
        item.reminderLabel = doc.reminderLabel;
      if (doc.todoCount)
        item.todoCount = doc.todoCount;
      if (doc.locationAddress)
        item.locationAddress = doc.locationAddress;
      if (doc.isLocationExist)
        item.isLocationExist = doc.isLocationExist;
      if (doc.isDownloaded)
        item.isDownloaded = doc.isDownloaded;
      if (doc.preview)
        item.preview = doc.preview;
    }

    return item;
  }

  /**
   * @param {{}} data
   * @return {{}|null}
   */
  static getPreviewResponseJson(data) {
    if (!data) {
      return null;
    }

    return {
      "noteGlobalId": data.globalId,
      "attachmentGlobalId": (data.preview ? data.preview['global_id'] : null),
      "createdAt": data.createdAt,
      "updatedAt": data.updatedAt
    };
  }

  /**
   * @param {[]} list
   * @return {[]}
   */
  static getPreviewResponseListJson(list) {
    return list.map((data) => {
      return ModelItem.getPreviewResponseJson(data);
    });
  }

  /**
   * @param {{}} data
   * @param {{}} excerpts
   * @param {{}} matches
   * @param {{}} texts
   * @return {{}|null}
   */
  static getResponseJson(data, excerpts = null, matches = null, texts = null) {
    if (!data) {
      return null;
    }

    if (data.type === "note") {
      return ModelItem.getNotePublicData(data, excerpts, matches, texts);
    } else {
      return ModelItem.getFolderPublicData(data);
    }
  }

  /**
   * @param {[]} list
   * @param {{excerpts:{}, matches: {}}} searchParams
   * @return {[]}
   */
  static getResponseListJson(list, searchParams) {
    const excerpts = searchParams ? searchParams.excerpts : null;
    const matches = searchParams ? searchParams.matches : null;
    const texts = searchParams ? searchParams.texts : null;
    return list.map((data) => {
      return ModelItem.getResponseJson(data, excerpts, matches, texts);
    });
  }

  /**
   * @param {{}} data
   * @param {{}} excerpts
   * @param {{}} matches
   * @param {{}} texts
   * @return {{}}
   */
  static getNotePublicData(data, excerpts, matches, texts) {
    if(excerpts && matches && texts) {
      let matchesValue = [];
      if(typeof (matches[data.globalId]) !== 'undefined') {
        matchesValue = matches[data.globalId];
      }

      let excerptValue = '';
      if(typeof (excerpts[data.globalId]) !== 'undefined') {
        excerptValue = excerpts[data.globalId];
      } else if(typeof (texts[data.globalId]) !== 'undefined') {
        excerptValue = texts[data.globalId];
      }

      return {
        noteGlobalId: data.globalId,
        excerpt: excerptValue,
        matches: matchesValue,
      }
    }

    return {
      rootId: data.rootId,
      rootParentId: data.rootParentId,
      attachmentsCount: data.attachmentsCount,
      childrenNotesCount: data.childrenNotesCount,
      cntNotes: data.cntNotes,
      createdAt: data.createdAt,
      dateAdded: data.dateAdded,
      dateUpdated: data.dateUpdated,
      editnote: !!data.editnote,
      favorite: data.favorite,
      globalId: data.globalId,
      isCompleted: !!data.isCompleted,
      isFullwidth: !!data.isFullwidth,
      isEncrypted: data.isEncrypted,
      lastChangeBy: data.lastChangeBy,
      locationLat: data.locationLat,
      locationLng: data.locationLng,
      parentId: data.parentId,
      role: data.role,
      shared: !!data.shared,
      size: data.size,
      title: data.title,
      todosClosedCount: data.todosClosedCount,
      todosCount: data.todosCount,
      type: data.type,
      updatedAt: data.updatedAt,
      url: data.url,
      isOfflineOnly: data.offlineOnly,
      synced: !data.offlineOnly && data.existOnServer && !!data.syncDate,
      color: data.color ? data.color : '',
      isImported: !!data.is_imported,
      workspaceId: data.workspaceId
    };
  }

  /**
   * @param {{}} data
   * @return {{}}
   */
  static getFolderPublicData(data) {
    return {
      rootId: data.rootId,
      rootParentId: data.rootParentId,
      childrenNotesCount: data.childrenNotesCount,
      cntNotes: data.cntNotes,
      createdAt: data.createdAt,
      dateAdded: data.dateAdded,
      dateUpdated: data.dateUpdated,
      editnote: !!data.editnote,
      favorite: !!data.favorite,
      globalId: data.globalId,
      isCompleted: !!data.isCompleted,
      isEncrypted: !!data.isEncrypted,
      isFullwidth: !!data.isFullwidth,
      lastChangeBy: data.lastChangeBy,
      locationLat: data.locationLat,
      locationLng: data.locationLng,
      parentId: data.parentId,
      role: data.role,
      shared: !!data.shared,
      size: data.size,
      title: data.title,
      todosClosedCount: 0,
      todosCount: 0,
      type: data.type,
      updatedAt: data.updatedAt,
      url: data.url,
      isOfflineOnly: data.offlineOnly,
      color: data.color ? data.color : '',
      isImported: !!data.is_imported,
    };
  }

  /**
   * @param {{}} data
   * @returns {{}}
   */
  static prepareModelData(data) {
    let item = {};

    item = ModelItem.prepareCommonProperties(item, data);
    item = ModelItem.prepareFolderOnlyProperties(item, data);
    item = ModelItem.prepareNoteOnlyProperties(item, data);

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    let cutDate = date.now();

    item.erised = data.erised || false;
    item.rootId = data.rootId || 'root';
    item.globalId = data.globalId || 'default';
    item._id = item.globalId.toString();
    item.parentId = data.parentId || 'root';
    item.rootParentId = data.rootParentId || 'root';
    item.createdAt = data.createdAt || data.dateAdded || cutDate;
    item.dateAdded = data.dateAdded || cutDate;
    item.dateUpdated = data.dateUpdated || cutDate;
    item.updatedAt = data.dateUpdated || data.updatedAt || cutDate;
    item.dateUpdatedUser = data.dateUpdatedUser || item.updatedAt || cutDate;
    item.type = data.type;
    item.role = data.role || 'note';
    item.title = data.title || '';
    item.url = data.url || '';
    item.locationLat = data.locationLat || 0;
    item.locationLng = data.locationLng || 0;
    if (typeof (data.favorite) !== 'undefined') {
      item.favorite = data.favorite;
    }
    if (typeof (data.reminder) !== 'undefined') {
      item.reminder = data.reminder;
    }
    item.lastChangeBy = data.lastChangeBy || 0;
    item.size = data.size || 0;
    if (typeof (data.editnote) !== 'undefined') {
      item.editnote = data.editnote;
    }
    if (typeof (data.isEncrypted) !== 'undefined') {
      item.isEncrypted = !!data.isEncrypted || false;
    }

    item.preview = data.preview || null;
    item.offlineOnly = !!data.offlineOnly;

    if (typeof (data.shared) !== 'undefined') {
      item.shared = data.shared;
    }

    if (!item.shared) {
      item.passwordRequired = false;
      item.shareId = '';
      item.securityKey = '';
    }

    if (typeof (data.text_version) !== 'undefined') {
      item.text_version = data.text_version;
    }

    if (typeof (data.is_imported) !== 'undefined') {
      item.is_imported = data.is_imported;
    }

    if(typeof (data.isFullwidth) !== 'undefined') {
      item.isFullwidth = data.isFullwidth;
    }

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareFolderOnlyProperties(item, data) {
    item.index = data.index || 0;
    item.existOnServer = !!data.existOnServer;
    item.uniqueUserName = data.uniqueUserName || "";
    item.syncDate = data.syncDate || 0;
    item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
    item.isMaybeInTrash = !!data.isMaybeInTrash;
    item.isClicked = !!data.isClicked;
    item.subfoldersCount = data.subfoldersCount || 0;
    item.notesCount = data.notesCount || 0;
    item.level = data.level || 0;
    item.color = data.color ? data.color : "";

    if (typeof (data.shared) !== "undefined") {
      item.shared = !!data.shared;
      if(data.shared_url) {
        item.shared_url = data.shared_url;
      }
    }
    if (!item.shared) {
      item.passwordRequired = false;
      item.shareId = "";
      item.securityKey = "";
      item.shared_url = "";
    }

    if (typeof (data.is_imported) !== "undefined") {
      item.is_imported = data.is_imported;
    }

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareNoteOnlyProperties(item, data) {
    item.syncDate = data.syncDate || 0;
    item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
    item.isTemp = !!data.isTemp;
    item.shortText = data.shortText || "";
    item.firstImage = data.firstImage || "";
    item.isMoreThanLimit = !!data.isMoreThanLimit;
    item.tags = data.tags || "";
    item.color = data.color ? data.color : "";
    item.textAttachmentGlobalId = data.textAttachmentGlobalId || "";
    item.attachmentsInListCount = data.attachmentsInListCount || 0;
    item.attachmentsInListExist = !!data.attachmentsInListExist;
    item.reminderExist = !!data.reminderExist;
    item.todoExist = !!data.todoExist;
    item.reminderLabel = data.reminderLabel || "";
    item.todoCount = data.todoCount || 0;
    item.locationAddress = data.locationAddress || "";
    item.isLocationExist = !!data.isLocationExist;
    item.isDownloaded = !!data.isDownloaded;

    if (typeof (data.shared) !== "undefined") {
      item.shared = !!data.shared;
      if(data.shared_url) {
        item.shared_url = data.shared_url;
      }
    }
    if (!item.shared) {
      item.passwordRequired = false;
      item.shareId = "";
      item.securityKey = "";
      item.shared_url = "";
    }

    if (typeof (data.preview) !== "undefined") {
      item.preview = data.preview || null;
    }

    if (typeof (data.editnote) !== 'undefined') {
      item.editnote = data.editnote || false;
    }

    if (typeof (data.isEncrypted) !== 'undefined') {
      item.isEncrypted = !!data.isEncrypted || false;
    }

    if (typeof (data.url) !== "undefined") {
      item.url = data.url;
    }

    if (typeof (data.color) !== 'undefined') {
      item.color = data.color;
    }

    if (typeof (data.favorite) !== 'undefined') {
      item.favorite = data.favorite;
    }

    if (typeof (data.reminder) !== 'undefined') {
      item.reminder = data.reminder;
    }

    if (typeof (data.text_version) !== 'undefined') {
      item.text_version = data.text_version;
    }

    if (typeof (data.is_imported) !== 'undefined') {
      item.is_imported = data.is_imported;
    }

    if(typeof (data.isFullwidth) !== 'undefined') {
      item.isFullwidth = data.isFullwidth;
    }

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static changeDateUpdate(item, data) {
    if (data.dateUpdated) {
      item.dateUpdated = data.dateUpdated;
    }
    if (data.dateUpdated) {
      item.updatedAt = data.dateUpdated;
    }
    if (data.dateUpdatedUser || item.updatedAt) {
      item.dateUpdatedUser = data.dateUpdatedUser || item.updatedAt;
    }

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @return {{}}
   */
  static getDefaultModel() {
    let item = <any>{};
    let curDate = date.now();

    item.erised = false;
    item._id = generator.randomString(16);

    item.rootId = 'root';
    item.rootIdUpdateTime = curDate;

    item.globalId = 'default';
    item.parentId = 'root';
    item.rootParentId = 'root';
    item.createdAt = curDate;
    item.dateAdded = curDate;
    item.dateUpdated = curDate;
    item.updatedAt = curDate;
    item.dateUpdatedUser = curDate;
    item.type = "folder";
    item.role = "note";
    item.title = "My Notes";
    item.url = "";
    item.locationLat = 0;
    item.locationLng = 0;
    item.shared = false;
    item.favorite = false;
    item.lastChangeBy = 0;
    item.size = 0;
    item.editnote = 1;
    item.isEncrypted = false;
    item.preview = null;
    item.passwordRequired = false;
    item.shareId = "";
    item.securityKey = "";
    item.offlineOnly = false;
    item.color = "";
    item.text_version = 2;
    item.is_imported = 0;

    return item;
  }

  /**
   * @returns {[]}
   */
  static getIndexList() {
    return [
      "rootId", "globalId", "parentId", "rootParentId",
      "createdAt", "dateAdded", "dateUpdated", "updatedAt", "dateUpdatedUser",
      "type", "role", "title", "url", "locationLat", "locationLng", "shared",
      "favorite", "lastChangeBy", "favorite", "editnote", "isEncrypted", "preview",
      "passwordRequired", "shareId", "securityKey", "offlineOnly", "color", "text_version",
      "is_imported"
    ];
  }

  /**
   * @param {string} workspaceId
   */
  static getNotesWithReminders(workspaceId) {
    // @ts-ignore
    return new Promise((resolve) => {
      ModelItem.findAll({type: 'note', reminder: {'$ne': null}}, {workspaceId}, (err, notesList) => {
        if (err || !notesList) {
          return resolve([]);
        }
        return resolve(notesList);
      });
    });
  }

  /**
   * @param {string} globalId
   * @param {string} workspaceId
   */
  static getByGlobalId({globalId, workspaceId}) {
    // @ts-ignore
    return new Promise((resolve) => {
      ModelItem.find({type: 'note', globalId}, {workspaceId}, (err, noteInstance) => {
        if (err || !noteInstance) {
          return resolve(null);
        }
        return resolve(noteInstance);
      });
    });
  }

  /**
   *
   * @param {{workspaceId:string}}
   * @return Promise.<{canCreateNote:bool, currentNotesCount: number, notesPerWorkspace:number, notesPerWorkspacePro:number}>
   */
  static validateNotesCountPerWorkspace({workspaceId}) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const response = {
        canCreateNote: true,
        currentNotesCount: 0,
        notesPerWorkspace: orgs.NOTES_PER_WORKSPACE_MAX,
        notesPerWorkspacePro: orgs.NOTES_PER_WORKSPACE_MAX_PRO,
      };

      const workspaceItem = <any>(workspaceId ? await workspace.getById(workspaceId) : await workspace.getDefault());
      let limits = workspaceItem && workspaceItem.org && workspaceItem.org.limits ? workspaceItem.org.limits : null;
      if(limits && limits.notesPerWorkspace) {
        response.notesPerWorkspace = limits.notesPerWorkspace;
      }

      if(workspaceItem && !workspaceItem.isNotesLimited) {
        return resolve(response);
      }

      const currentNotesCount = <number>await ModelItem.getCountForWorkspace({workspaceId});
      if(typeof (currentNotesCount) === 'undefined') {
        return resolve(response);
      }
      response.currentNotesCount = currentNotesCount;

      if(!limits) {
        limits = <{notesPerWorkspace:number}>await orgs.getLimits(workspaceId);
      }

      if(!limits) {
        return resolve(response);
      }

      const { notesPerWorkspace } = limits;
      if(typeof (notesPerWorkspace) === 'undefined') {
        return resolve(response);
      }
      response.notesPerWorkspace = notesPerWorkspace;

      response.canCreateNote = currentNotesCount < notesPerWorkspace;
      return resolve(response);
    });
  }

  /**
   * @param {{workspaceId:string}}
   * @return Promise.<number>
   */
  static getCountForWorkspace({workspaceId}) {
    // @ts-ignore
    return new Promise((resolve) => {
      ModelItem.count({
        type: 'note',
        role: {
          "$nin": [
            ModelItem.ITEM_SCREENSHOT,
            ModelItem.ITEM_SCREENCAST,
            ModelItem.ITEM_WELCOME_GUIDE,
            ModelItem.ITEM_STREAMING_VIDEO,
          ]
        },
        is_imported: {
          '$ne': 1
        },
      }, {workspaceId}, (err, countNotes) => {
        if (err || !countNotes) {
          return resolve(0);
        }
        return resolve(countNotes);
      });
    });
  }
}
