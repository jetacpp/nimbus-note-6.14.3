import chalk from "chalk";
import {default as textHandler} from "../../utilities/textHandler";
import {default as generator} from "../../utilities/generatorHandler";
import {default as date} from "../../utilities/dateHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";

let dbName = config.DB_TEXT_MODEL_NAME;

export default class ModelText {
  static SHORT_TEXT_LENGTH = 125;
  static SHORT_TEXT_LINE_END = "...";
  static MAX_TEXT_SIZE = 5242880;

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.findAll(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.noteGlobalId) {
      return callback(error.wrongInput(), null);
    }

    options.dbName = dbName;
    options.prepareModelData = ModelText.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.noteGlobalId) {
      return callback(error.wrongInput(), null);
    }
    if (updateFields.text) {
      updateFields.textShort = ModelText.makeShortText(updateFields.text);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.noteGlobalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.noteGlobalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    PouchDb.remove(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {boolean} onlyShort
   * @return {{}|null}
   */
  static getResponseJson(data, onlyShort = false) {
    if (!data) {
      return null;
    }

    let result = {
      dateUpdated: data.dateUpdated,
      length: data.length,
      noteGlobalId: data.noteGlobalId
    };

    if (typeof (data.textShort) === "undefined") {
      result['textShort'] = "";
    } else {
      result['textShort'] = data.textShort;
    }

    if (!onlyShort) {
      result['text'] = data.text === "undefined" ? "" : data.text;
      if(!result['text']) {
        result['text'] = ModelText.getDefaultNoteText();
      }
    }

    if (typeof (data.text_version) !== 'undefined') {
      result['version'] = data.text_version;
    }

    return result;
  }

  /**
   * @param {[]} list
   * @param {boolean} onlyShort
   * @return {[]}
   */
  static getResponseListJson(list, onlyShort) {
    onlyShort = onlyShort || false;
    return list.map((data) => {
      return ModelText.getResponseJson(data, onlyShort);
    });
  }

  /**
   * @param {{}} doc
   * @returns {{}}
   */
  static getPublicData(doc) {
    let text = <any>{};
    if (doc) {
      text.noteGlobalId = doc.noteGlobalId;
      text.dateUpdated = doc.dateUpdated;
      text.text = doc.text;
      text.textShort = doc.textShort;
      text['length'] = doc['length'];

      //TODO: add properties from sync models
      if (doc.dateAdded)
        text.dateAdded = doc.dateAdded;
      if (doc.parentId)
        text.parentId = doc.parentId;
      if (doc.syncDate)
        text.syncDate = doc.syncDate;
      if (doc.needSync)
        text.needSync = doc.needSync;
      if (doc.text_version)
        text.text_version = doc.text_version
    }

    return text;
  }

  /**
   * @param {{}} data
   * @returns {{}}
   */
  static prepareModelData(data) {
    let item = {};

    item = ModelText.prepareCommonProperties(item, data);
    item = ModelText.prepareSyncOnlyProperties(item, data);

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    item.erised = data.erised || false;
    item._id = data._id || generator.randomString(16);
    item.noteGlobalId = data.noteGlobalId || data.parentId;
    item.dateUpdated = data.dateUpdated;
    item.text = data.text || ModelText.getDefaultNoteText();
    item.textShort = data.shortText || textHandler.stripTags(item.text);
    item['length'] = data.text ? data.text.length : 0;
    item.text_version = data.text_version || 1;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareSyncOnlyProperties(item, data) {
    item.shortText = data.shortText || textHandler.stripTags(item.text) || "";
    item.dateAdded = data.dateAdded || "";
    item.parentId = data.parentId || data.noteGlobalId || "";
    item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
    item.syncDate = data.syncDate || 0;
    item.text_version = data.text_version || 1;

    return item;
  }

  /**
   * @return {string}
   */
  static getDefaultNoteText() {
    return "<div></div>";
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @param {string} noteGlobalId
   * @return {{}}
   */
  static getDefaultModel(noteGlobalId) {
    let text = <any>{};
    let curDate = date.now();

    text.erised = false;
    text._id = generator.randomString(16);
    text.noteGlobalId = noteGlobalId;
    text.dateUpdated = curDate;
    text.text = "<div></div>";
    text.textShort = textHandler.stripTags(text.text);
    text['length'] = text.text ? text.text.length : 0;
    text.text_version = text.text_version || 2;

    return text;
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["noteGlobalId", "dateUpdated", "text", "textShort", "length"];
  }

  /**
   * @param {string} text
   */
  static makeShortText(text) {
    let clearedText = textHandler.stripTags(text);
    return clearedText.length > ModelText.SHORT_TEXT_LENGTH ?
      clearedText.substring(0, ModelText.SHORT_TEXT_LENGTH) + ModelText.SHORT_TEXT_LINE_END : clearedText;
  }

  static getTextAsync(noteGlobalId, workspaceId) {
    return new Promise(resolve => {
      ModelText.find({noteGlobalId}, {workspaceId}, (err, textItem) => {
        if(err || !textItem) {
          return '';
        }
        return resolve(textItem.text)
      });
    });
  }
}
