import chalk from "chalk";
import {default as error} from "../../utilities/customError";
import {default as generator} from "../../utilities/generatorHandler";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";

let dbName = config.DB_USER_SETTINGS_MODEL_NAME;

export default class ModelUserSettings {
  static SYNC_STATUS_DEFAULT = 1;
  static SYNC_TIMER_MIN_DEFAULT = 10;

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options = <any>{}, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, (err, item) => {
      callback(err, ModelUserSettings.getPublicData(item));
    });
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static count(data, options = <any>{}, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.count(data, options, callback);
  }

  static create(data = <any>{}, updateFields, callback = (err, res) => {
  }) {
    if (!data.property) {
      return callback(error.wrongInput(), null);
    }

    /**
     * @param {{}} data
     * @param {{}} updateFields
     * @param {Function} callback
     */
    ModelUserSettings.count(data, {}, (err, findCount) => {
      if (err) {
        callback(err, null);
      }

      updateFields.property = data.property;

      let options = <any>{dbName: dbName};
      if (findCount) {
        options.updateFields = updateFields;
        PouchDb.update(data, options, callback);
      } else {
        options.prepareModelData = ModelUserSettings.prepareModelData;
        PouchDb.create(updateFields, options, callback);
      }
    });
  }

  /**
   * @param {{}} data
   * @param {Function} callback
   */
  static delete(data = <any>{}, callback = (err, res) => {
  }) {
    if (!data.property) {
      return callback(error.wrongInput(), 0);
    }

    let options = {dbName: dbName};
    PouchDb.remove(data, options, callback);
  }

  /**
   * @param {string} key
   * @param {Function} callback
   */
  static get(key, callback = (err, res) => {
  }) {
    ModelUserSettings.find({property: key}, {}, callback);
  }

  /**
   * @param {string} key
   * @param {*} value
   * @param {Function} callback
   */
  static set(key, value, callback = (err, res) => {
  }) {
    ModelUserSettings.create({property: key}, {value: value}, callback);
  }

  /**
   * @param {string} key
   * @param {Function} callback
   */
  static has(key, callback = (err, res) => {
  }) {
    ModelUserSettings.count({property: key}, {}, (err, itemCount) => {
      callback(err, !!itemCount);
    });
  }

  /**
   * @param {string} key
   * @param {Function} callback
   */
  static remove(key, callback = (err, res) => {
  }) {
    ModelUserSettings.delete({property: key}, (err, itemCount) => {
      callback(err, !!itemCount);
    });
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicData(doc) {
    let item = {};
    if (doc) {
      return doc.value;
    }

    return item;
  }

  /**
   * @param {{}} data
   * @return {{}}
   */
  static prepareModelData(data) {
    let item = <any>{};
    item._id = data._id || generator.randomString(16);
    item.property = data.property;
    item.value = data.value;
    if (typeof (data.value) !== "undefined") {
      item.value = data.value;
    }

    return item;
  }

  /**
   * @return {int}
   */
  static getDefaultSyncStatus() {
    return ModelUserSettings.SYNC_STATUS_DEFAULT;
  }

  /**
   * @return {int}
   */
  static getDefaultSyncTimer() {
    return ModelUserSettings.SYNC_TIMER_MIN_DEFAULT;
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<{}>}
   */
  static async getSyncInfo(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId} = inputData;
      let syncInfo = <any>{};
      ModelUserSettings.get(`syncStatus:${workspaceId}`, (err, syncStatus) => {
        if (typeof (syncStatus) === "object" && Object.keys(syncStatus).length === 0) {
          syncInfo.syncStatus = ModelUserSettings.getDefaultSyncStatus();
        } else {
          syncInfo.syncStatus = parseInt(syncStatus);
        }

        ModelUserSettings.get(`syncTimer:${workspaceId}`, (err, syncTimer) => {
          if (typeof (syncTimer) === "object" && Object.keys(syncTimer).length === 0) {
            syncInfo.syncTimer = ModelUserSettings.SYNC_TIMER_MIN_DEFAULT;
          } else {
            syncInfo.syncTimer = parseInt(syncTimer);
          }

          resolve(syncInfo);
        });
      });
    });
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["property", "value"];
  }
}
