import chalk from "chalk";
import {default as generator} from "../../utilities/generatorHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";

let dbName = config.DB_TEXT_EDITOR_MODEL_NAME;

export default class ModelTextEditor {
  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.findAll(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), null);
    }

    options.dbName = dbName;
    options.prepareModelData = ModelTextEditor.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), null);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    PouchDb.remove(data, options, callback);
  }

  /**
   * @param {{}} doc
   * @returns {{}}
   */
  static getPublicData(doc) {
    let text = <any>{};
    if (doc) {
      text.globalId = doc.globalId;
      text.noteGlobalId = doc.noteGlobalId;
      text.text = doc.text;
      text.dateUpdated = doc.dateUpdated;
    }

    return text;
  }

  /**
   * @param {{}} data
   * @returns {{}}
   */
  static prepareModelData(data) {
    return ModelTextEditor.prepareCommonProperties({}, data);
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    item._id = data._id || generator.randomString(16);
    item.globalId = data.globalId;
    item.noteGlobalId = data.noteGlobalId;
    item.text = data.text;
    item.dateUpdated = data.dateUpdated;
    return data;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["globalId", "noteGlobalId", "dateUpdated", "text"];
  }
}
