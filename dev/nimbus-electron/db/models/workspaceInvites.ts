import chalk from "chalk";
import {default as date} from "../../utilities/dateHandler";
import {default as generator} from "../../utilities/generatorHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";
import {default as workspaceMember} from "./workspaceMember"

let dbName = config.DB_WORKSPACE_INVITES_MODEL_NAME;

export default class ModelWorkspaceInvite {
  static ERROR_LIMIT = -20;
  static ERROR_UNIQUE = -21;

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static count(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.count(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.findAll(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.workspaceId) {
      return callback(error.wrongInput(), null);
    }

    options.dbName = dbName;
    options.prepareModelData = ModelWorkspaceInvite.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.workspaceId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.id) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (data.id) {
      options.dbName = dbName;
      PouchDb.remove(data, options, callback);
    } else {
      return callback(error.wrongInput(), 0);
    }
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicData(doc) {
    let workspaceInvite = <any>{};
    if (doc) {
      workspaceInvite.id = doc._id;
      workspaceInvite.globalId = doc.globalId;
      workspaceInvite.createdAt = doc.createdAt;
      workspaceInvite.updatedAt = doc.updatedAt;

      //TODO: add properties from sync models
      if (doc.workspaceId)
        workspaceInvite.workspaceId = doc.workspaceId;
      if (doc.role)
        workspaceInvite.role = doc.role;
      if (doc.encryptRole)
        workspaceInvite.encryptRole = doc.encryptRole;
      if (doc.addedByUserId)
        workspaceInvite.addedByUserId = doc.addedByUserId;
      if (doc.email)
        workspaceInvite.email = doc.email ? doc.email.toLowerCase() : '';
      if (doc.used)
        workspaceInvite.used = doc.used;

      if (doc.syncDate)
        workspaceInvite.syncDate = doc.syncDate;
      if (doc.needSync)
        workspaceInvite.needSync = doc.needSync;
    }

    return workspaceInvite;
  }

  /**
   * @param {{}} data
   * @return {{}|null}
   */
  static getResponseJson(data) {
    if (!data) {
      return null;
    }

    return {
      globalId: data.globalId,
      workspaceId: data.workspaceId,
      role: data.role,
      encryptRole: data.encryptRole,
      addedByUserId: data.addedByUserId,
      email: data.email ? data.email.toLowerCase() : '',
      used: data.used,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt
    };
  }

  /**
   * @param {{}} data
   * @return {{}}
   */
  static prepareModelData(data) {
    let item = {};

    item = ModelWorkspaceInvite.prepareCommonProperties(item, data);
    item = ModelWorkspaceInvite.prepareSyncOnlyProperties(item, data);

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    let curDate = date.now();

    item.erised = data.erised || false;
    item.globalId = data.globalId || data.global_id || item._id || generator.randomString(16);
    item._id = item.globalId.toString();
    item.workspaceId = data.workspaceId || '';
    item.role = data.role || workspaceMember.ROLE_READER;
    item.encryptRole = data.encryptRole || workspaceMember.ENCRYPTION_ROLE_DENY;
    item.addedByUserId = data.addedByUserId || 0;
    item.email = data.email ? data.email.toLowerCase() : '';
    item.used = data.used || false;
    item.createdAt = data.createdAt || curDate;
    item.updatedAt = data.updatedAt || curDate;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareSyncOnlyProperties(item, data) {
    item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
    item.syncDate = data.syncDate || 0;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["globalId", "workspaceId", "role", "encryptRole", "addedByUserId", "email", "used", "updatedAt"];
  }
}
