import chalk from "chalk";
import {default as generator} from "../../utilities/generatorHandler";
import {default as date} from "../../utilities/dateHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";
import getTrial from "../../sync/process/rx/handlers/getTrial";
import {default as NimbusSDK} from "../../sync/nimbussdk/net/NimbusSDK";
import {setUpdateProps} from "../../utilities/syncPropsHandler";
import userVariables from "../../sync/process/rx/handlers/userVariables";
import {default as orgs} from "./orgs";
import {default as urlParser} from "../../utilities/urlParser";
import fs from "fs-extra";

let dbName = config.DB_USER_MODEL_NAME;

export default class ModelUser {
  static WARNING_UPLOAD_SIZE = 89128960;
  static MAX_UPLOAD_SIZE = 104857600;
  static MAX_UPLOAD_SIZE_PREMIUM = 5368709120;

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (options && options.useLocalSession) {
      if (!data.localSession) {
        return callback(error.wrongInput(), null);
      }
    } else {
      if (data.login) {
        data.email = data.login ? data.login.toLowerCase() : '';
        delete data.login;
      }
    }

    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static exist(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options.dbName = dbName;
    PouchDb.find(data, options, (err, userItem) => {
      if (err) {
        return callback(err, false);
      }

      callback(err, !!userItem);
    });
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static create(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.login) {
      return callback(error.wrongInput(), null);
    }

    options.dbName = dbName;
    options.prepareModelData = ModelUser.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!(data.userId || data.email)) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.login) {
      return callback(error.wrongInput(), 0);
    }
    data = {email: data.login};
    options.dbName = dbName;
    PouchDb.remove(data, options, callback);
  }

  /**
   * @param {{}} doc
   * @returns {{}}
   */
  static getPublicData(doc) {
    let user = <any>{};
    if (doc) {
      user.id = doc._id;
      user.userId = doc.userId;
      user.language = doc.language;
      user.email = doc.email ? doc.email.toLowerCase() : '';
      user.firstname = doc.firstname;
      user.lastname = doc.lastname;
      user.dateTimeLocale = doc.dateTimeLocale || '';
      user.avatar = doc.avatar;
      user.username = doc.username;
      user.subscribe = doc.subscribe;
      user.languages = doc.languages;
      user.settings = doc.settings || {};
      user.variables = doc.variables || ModelUser.getDefaultVariables();
      user.authProvider = doc.authProvider || '';
    }

    return user;
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicInfo(doc) {
    let info = <any>{};
    if (doc) {
      info.userId = doc.userId;
      info.email = doc.email ? doc.email.toLowerCase() : '';
      info.firstname = doc.firstname;
      info.lastname = doc.lastname;
      info.dateTimeLocale = doc.dateTimeLocale || '';
      info.avatar = doc.avatar;
      info.languages = doc.languages;
      info.notesEmail = doc.notesEmail;

      info.dateNextQuotaReset = doc.dateNextQuotaReset;
      info.quotaResetDate = doc.quotaResetDate;

      info.subscribe = doc.subscribe;
      info.paymentStartDate = doc.paymentStartDate;
      info.paymentEndDate = doc.paymentEndDate;
      info.paymentSystemCode = doc.paymentSystemCode;
      info.settings = doc.settings || {};
      info.variables = doc.variables || ModelUser.getDefaultVariables();
      info.authProvider = doc.authProvider || '';

      //TODO: add properties from sync models
      if (doc.syncDate)
        info.syncDate = doc.syncDate;
      if (doc.needSync)
        info.needSync = doc.needSync;
    }

    return info;
  }

  /**
   * @param {{}} doc
   * @returns {{}}
   */
  static getPublicUsage(doc) {
    let usage = <any>{};
    if (doc) {
      usage.current = doc.usageCurrent;
      usage.max = doc.usageMax;
    }

    return usage;
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicTrial(doc) {
    if (doc.trial) {
      return doc.trial;
    }

    return ModelUser.getDefaultTrial();
  }

  /**
   * @return {{}}
   */
  static getDefaultTrial() {
    return {
      dateStart: 0,
      dateUntil: 0,
      daysRemain: 0,
      requestorService: null,
      state: null
    }
  }

  static async getTrialAsync(userId) {
    return new Promise(resolve => {
      const findQuery = {userId};
      ModelUser.find(findQuery, {}, (err, userItem) => {
        if(err || !userItem) {
          return resolve(ModelUser.getDefaultTrial());
        }

        const apiTrial = ModelUser.getPublicTrial(userItem);
        return resolve(apiTrial);
      });
    });
  }

  static async getProfileAsync(email) {
    return new Promise(resolve => {
      const findQuery = {login: email};
      // @ts-ignore
      ModelUser.find(findQuery, {}, async (err, userItem) => {
        if(err || !userItem) {
          return resolve(null);
        }

        let org = <any>await orgs.getActive();
        let response = <any>{};
        response.email = userItem.email ? userItem.email.toLowerCase() : '';
        response.id = userItem.id || 0;
        response.language = userItem.language || "";
        response.userId = org && org.user ? org.user.id : userItem.userId;
        response.username = userItem.username || "";
        response.firstname = userItem.firstname;
        response.lastname = userItem.lastname;
        response.notesEmail = "";

        if (userItem.notesEmail) {
          response.notesEmail = ModelUser.getNotesEmailPart(userItem.notesEmail);
        }

        if (userItem.dateNextQuotaReset) {
          response.dateNextQuotaReset = userItem.dateNextQuotaReset;
        }

        if (userItem.quotaResetDate) {
          response.quotaResetDate = userItem.quotaResetDate;
        }

        if(userItem.avatar) {
          const {url} = userItem.avatar;
          if(url) {
            let routeParams = urlParser.getPathParams(url);
            let storedFileUUID = typeof (routeParams[2] !== 'undefined') ? routeParams[2] : null;
            if(storedFileUUID) {
              let targetPath = `${PouchDb.getClientAttachmentPath()}/${storedFileUUID}`;
              // @ts-ignore
              let exists = await fs.exists(targetPath);
              // @ts-ignore
              if (exists) {
                response.avatarFileUUID = `avatar/${storedFileUUID}`;
              }
            }
          }
        }

        return resolve(response);
      });
    });
  }

  static syncUserTrial() {
    // @ts-ignore
    return new Promise(async (resolve) => {
      getTrial({saveUserTrial: true, skipSyncHandlers: true}, () => {
        return resolve();
      });
    });
  }

  /**
   * @param {{}} userInfo
   * @param {Function} callback
   */
  static getPublicPremium(userInfo, callback = (err, res) => {
  }) {
    let premium = <any>{message: "premium not found", httpStatus: 404};

    if (!userInfo) {
      return callback(null, premium);
    }

    if(!userInfo.email) {
      return callback(null, premium);
    }

    ModelUser.find({login: userInfo.email.toLowerCase()}, {}, (err, userInstance) => {
      if (err || !userInstance) {
        return callback(null, premium);
      }

      if (!userInstance.subscribe) {
        return callback(null, premium);
      }

      let endDate = "";
      if (userInstance.paymentEndDate) {
        endDate = userInstance.paymentEndDate;
      }

      let paymentSystemCode = "";
      if (userInstance.paymentSystemCode) {
        paymentSystemCode = userInstance.paymentSystemCode;
      }

      let startDate = "";
      if (userInstance.paymentStartDate) {
        startDate = userInstance.paymentStartDate;
      }

      let status = "";
      if (userInstance.subscribe) {
        status = "active";
      }

      premium = {
        httpStatus: 200,
        endDate: endDate,
        paymentSystemCode: paymentSystemCode,
        startDate: startDate,
        status: status,
        userId: userInstance.userId
      };

      callback(null, premium);
    });
  }

  /**
   * @param {string} email
   * @return {string}
   */
  static getNotesEmailPart(email) {
    return (email ? email.split("@", 1)[0] : "");
  }

  /**
   * @param {int} days
   * @return {int}
   */
  static getDateNextQuotaReset(days) {
    // @ts-ignore
    let timestamp = parseInt(new Date().getTime() / 1000);
    if (days) {
      timestamp = timestamp + (days * 86400);
    }
    return timestamp;

  }

  /**
   * @param {{}} data
   * @return {{}}
   */
  static prepareModelData(data) {
    let user = <any>{};
    let userLoginPart = data.login.split('@');
    let userBaseName = typeof (userLoginPart[0]) !== "undefined" ? userLoginPart[0] : data.login;
    let userName = userBaseName + "-" + generator.randomString(6);
    let noteEmail = "";

    user.erised = false;
    user.userId = parseInt(date.now() + generator.randomString(2, '0123456789'));
    user._id = user.userId.toString();
    user.language = "en";
    user.email = data.login ? data.login.toLowerCase() : '';
    user.password = data.password;

    if(typeof (data.firstname) !== 'undefined') {
      user.firstname = data.firstname;
    }
    if(typeof (data.lastname) !== 'undefined') {
      user.lastname = data.lastname;
    }
    if(typeof (data.dateTimeLocale) !== 'undefined') {
      user.dateTimeLocale = data.dateTimeLocale;
    }
    if(typeof (data.avatar) !== 'undefined') {
      user.avatar = data.avatar;
    }
    if(typeof (data.languages) !== 'undefined') {
      user.languages = data.languages;
    }

    user.username = userName;
    user.localSession = '';
    user.notesEmail = noteEmail;

    user.popupLimitNotice = false;
    user.popupLimitError = false;

    user.usageCurrent = 0;
    user.usageMax = ModelUser.MAX_UPLOAD_SIZE;

    user.dateNextQuotaReset = 0;
    user.quotaResetDate = 0;

    user.subscribe = data.subscribe || 0;
    user.paymentStartDate = data.paymentStartDate || "";
    user.paymentEndDate = data.paymentEndDate || "";
    user.paymentSystemCode = data.paymentSystemCode || "";

    //TODO: add properties from sync models
    user.syncDate = data.syncDate || 0;
    user.needSync = data.needSync || true;

    user.settings = data.settings || {};
    user.variables = data.variables || ModelUser.getDefaultVariables();

    user.authProvider = data.authProvider || '';

    return user;
  }

  /**
   * @param {string} email
   * @param {string} id
   * @returns {string|null}
   */
  static prepareName(email, id) {
    let result = null;

    if (email && id) {
      let pos = email.indexOf('@');
      if (pos >= 0) {
        let userName = email.substr(0, pos);
        if (userName) {
          result = userName + '_' + id;
        }
      }
    }

    return result;
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["userId", "language", "email", "password", "firstname",
            "lastname", "dateTimeLocale", "avatar", "languages", "username",
            "subscribe", "localSession", "dateNextQuotaReset",
            "quotaResetDate", "notesEmail", "usageCurrent", "usageMax",
            "settings", "variables", "authProvider"];
  }

  /**
   * @param {user} authInfo
   * @param {string} name
   * @param {string} value
   */
  static saveUserSettings(authInfo, name, value) {
    return new Promise(async (resolve) => {

      if(!authInfo) {
        return resolve(null);
      }

      const curDate = date.now();
      const settings = <{}>await ModelUser.getUserSettings(authInfo);

      const newSettings = {
        ...settings,
        [name]: value
      };

      const queryData = {email: authInfo.email};
      const saveUserData = {settings: newSettings};
      ModelUser.update(queryData, setUpdateProps(saveUserData, curDate), {}, (err, count) => {
        if (err || !count) {
          return resolve(null);
        }
        return resolve(newSettings);
      });
    });
  }

  static getDefaultVariables() {
    return {
      ignoreIntro: false,
      ignoreSharesInfo: false,
      ignoreStartTooltips: false,
      noteAppearance: {
        "style": "normal",
        "size": "normal"
      },
      colorTheme: 0,
      helpTooltipsStartShown: false,
      helpTooltipsNoteShown: false,
      desktopAppsInformerShown: false,
      lastOpenedWorkspaces: {},
      isPinnedLeftBlock: true,
    };
  }

  static getUserSettings(authInfo) {
    return new Promise((resolve) => {
      const queryData = {email: authInfo.email};
      ModelUser.find(queryData, {}, (err, userInstance) => {
        if (err || !userInstance) {
          return resolve({});
        }

        const {settings} = userInstance;

        if(!settings) {
          return resolve({});
        }

        return resolve(settings);
      });
    });
  }

  static async getAccountUserId() {
    return new Promise((resolve) => {
      NimbusSDK.getAccountManager().getUserId((err, userId) => {
        if(!userId) {
          return resolve(null);
        }
        return resolve(userId);
      });
    });
  }

  static syncUserVariables() {
    // @ts-ignore
    return new Promise(async (resolve) => {
      userVariables({saveUserVariables: true, skipSyncHandlers: true}, () => {
        return resolve();
      });
    });
  }
}
