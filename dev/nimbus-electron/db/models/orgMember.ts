import chalk from "chalk";
import {default as date} from "../../utilities/dateHandler";
import {default as generator} from "../../utilities/generatorHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";

let dbName = config.DB_ORG_MEMBERS_MODEL_NAME;

export default class ModelOrgMember {
  static TYPE_ADMIN = 'admin';
  static TYPE_OWNER = 'owner';
  static TYPE_MANAGER = 'manager';
  static TYPE_USER = 'user';

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static count(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.count(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.findAll(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.orgId) {
      return callback(error.wrongInput(), null);
    }

    options.dbName = dbName;
    options.prepareModelData = ModelOrgMember.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.orgId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (data.globalId) {
      options.dbName = dbName;
      PouchDb.remove(data, options, callback);
    } else {
      return callback(error.wrongInput(), 0);
    }
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicData(doc) {
    let orgMember = <any>{};
    if (doc) {
      orgMember.id = doc._id;
      orgMember.globalId = doc.globalId;
      orgMember.createdAt = doc.createdAt;
      orgMember.updatedAt = doc.updatedAt;

      //TODO: add properties from sync models
      if (doc.orgId)
        orgMember.orgId = doc.orgId;
      if (doc.role)
        orgMember.role = doc.role;
      if (doc.disabled)
        orgMember.disabled = doc.disabled;
      if (doc.user)
        orgMember.user = doc.user;

      if (doc.syncDate)
        orgMember.syncDate = doc.syncDate;
      if (doc.needSync)
        orgMember.needSync = doc.needSync;
    }

    return orgMember;
  }

  /**
   * @param {{}} data
   * @return {{}|null}
   */
  static getResponseJson(data) {
    if (!data) {
      return null;
    }

    return {
      globalId: data.globalId,
      orgId: data.orgId,
      role: data.role,
      disabled: data.disabled,
      user: data.user,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt
    };
  }

  /**
   * @param {{}} data
   * @return {{}}
   */
  static prepareModelData(data) {
    let item = {};

    item = ModelOrgMember.prepareCommonProperties(item, data);
    item = ModelOrgMember.prepareSyncOnlyProperties(item, data);

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    let curDate = date.now();

    item.erised = data.erised || false;
    item.globalId = data.globalId || data.global_id || item._id || generator.randomString(16);
    item._id = item.globalId.toString();
    item.orgId = data.orgId || '';
    item.role = data.role || ModelOrgMember.TYPE_USER;
    item.disabled = data.disabled || '';
    item.user = data.user || {
      id: 0,
      email: null,
      username: '',
      firstname: null,
      lastname: null
    };
    item.createdAt = data.createdAt || curDate;
    item.updatedAt = data.updatedAt || curDate;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareSyncOnlyProperties(item, data) {
    item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
    item.syncDate = data.syncDate || 0;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["globalId", "orgId", "role", "disabled", "updatedAt"];
  }
}
