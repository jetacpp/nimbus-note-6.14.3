import chalk from "chalk";
import {default as generator} from "../../utilities/generatorHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";
import {default as tag} from "./tag";

let dbName = config.DB_NOTE_TAGS_MODEL_NAME;

export default class ModelNoteTags {
  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.findAll(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.tagGlobalId || !data.noteGlobalId) {
      return callback(error.wrongInput(), null);
    }

    options.dbName = dbName;
    options.prepareModelData = ModelNoteTags.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.tagGlobalId || !data.noteGlobalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (data.tagGlobalId || data.noteGlobalId) {
      options.dbName = dbName;
      options.updateFields = PouchDb.getErasedUpdateFields();
      PouchDb.update(data, options, callback);
    } else {
      return callback(error.wrongInput(), 0);
    }
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (data.tagGlobalId || data.noteGlobalId) {
      options.dbName = dbName;
      PouchDb.remove(data, options, callback);
    } else {
      return callback(error.wrongInput(), 0);
    }
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static removeByItemId(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.noteGlobalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} doc
   * @returns {{}}
   */
  static getPublicData(doc) {
    let tag = <any>{};
    if (doc) {
      tag.id = doc._id;
      tag.tagGlobalId = doc.tagGlobalId;
      tag.noteGlobalId = doc.noteGlobalId;
      tag.dateUpdated = doc.dateUpdated;

      //TODO: add properties from sync models
      if (doc.dateAdded)
        tag.dateAdded = doc.dateAdded;
      if (doc.parentId)
        tag.parentId = doc.parentId;
      if (doc.syncDate)
        tag.syncDate = doc.syncDate;
      if (doc.needSync)
        tag.needSync = doc.needSync;
    }

    return tag;
  }

  /**
   * @param {{}} data
   * @returns {{}}
   */
  static prepareModelData(data) {
    let item = {};

    item = ModelNoteTags.prepareCommonProperties(item, data);
    item = ModelNoteTags.prepareSyncOnlyProperties(item, data);

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    item.erised = data.erised || false;
    item._id = data._id || generator.randomString(16);
    item.tagGlobalId = data.tagGlobalId;
    item.noteGlobalId = data.noteGlobalId || data.parentId;
    item.dateUpdated = data.dateUpdated;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareSyncOnlyProperties(item, data) {
    item.dateAdded = data.dateAdded || 0;
    item.parentId = data.parentId || data.noteGlobalId || "";
    item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
    item.syncDate = data.syncDate || 0;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["tagGlobalId", "noteGlobalId", "dateUpdated"];
  }

  static getAllNoteTagItems({noteGlobalId, workspaceId}) {
    return new Promise((resolve) => {
      ModelNoteTags.findAll({noteGlobalId}, {workspaceId}, (err, noteTagsItem) => {
        const noteTagsList = noteTagsItem ? noteTagsItem : [];
        if(!noteTagsList.length) {
          return resolve([]);
        }

        const noteTagsItemsIdList = noteTagsList.map(noteTag => noteTag.tagGlobalId);
        tag.findAll({globalId: {"$in": noteTagsItemsIdList}}, {workspaceId}, (err, tagItem) => {
          const tagsList = tagItem ? tagItem : [];
          if(!tagsList.length) {
            return resolve([]);
          }

          return resolve(tagsList);
        });
      });
    });
  }

  static async getAllNoteTagItemsTitle({noteGlobalId, workspaceId}) {
    const tagsList = <[{title}]>await ModelNoteTags.getAllNoteTagItems({noteGlobalId, workspaceId});
    return tagsList.map(tag => tag.title);
  }
}
