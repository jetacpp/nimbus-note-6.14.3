import chalk from "chalk";
import {default as date} from "../../utilities/dateHandler";
import {default as generator} from "../../utilities/generatorHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";

let dbName = config.DB_WORKSPACE_MEMBERS_MODEL_NAME;

export default class ModelWorkspaceMember {
  static TYPE_FULL = 'full';

  static ROLE_MEMBER = 'member';
  static ROLE_READER = 'reader';
  static ROLE_EDITOR = 'editor';
  static ROLE_ADMIN = 'admin';

  static ENCRYPTION_ROLE_ACCESSOR = 'encryptionAccessor';
  static ENCRYPTION_ROLE_ENCRYPTOR = 'encryptor';
  static ENCRYPTION_ROLE_MANGER = 'encryptionManager';
  static ENCRYPTION_ROLE_DENY = 'encryptionDeny';

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static count(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.count(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.findAll(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.workspaceId) {
      return callback(error.wrongInput(), null);
    }

    options.dbName = dbName;
    options.prepareModelData = ModelWorkspaceMember.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.workspaceId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (data.globalId) {
      options.dbName = dbName;
      PouchDb.remove(data, options, callback);
    } else {
      return callback(error.wrongInput(), 0);
    }
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicData(doc) {
    let workspaceMember = <any>{};
    if (doc) {
      workspaceMember.id = doc._id;
      workspaceMember.globalId = doc.globalId;
      workspaceMember.createdAt = doc.createdAt;
      workspaceMember.updatedAt = doc.updatedAt;

      //TODO: add properties from sync models
      if (doc.addedByUserId)
        workspaceMember.addedByUserId = doc.addedByUserId;
      if (doc.user)
        workspaceMember.user = doc.user;
      if (doc.workspaceId)
        workspaceMember.workspaceId = doc.workspaceId;
      if (doc.orgId)
        workspaceMember.orgId = doc.orgId;
      if (doc.type)
        workspaceMember.type = doc.type;
      if (doc.role)
        workspaceMember.role = doc.role;
      if (doc.encryptRole)
        workspaceMember.encryptRole = doc.encryptRole;

      if (doc.syncDate)
        workspaceMember.syncDate = doc.syncDate;
      if (doc.needSync)
        workspaceMember.needSync = doc.needSync;
    }

    return workspaceMember;
  }

  /**
   * @param {{}} data
   * @return {{}|null}
   */
  static getResponseJson(data) {
    if (!data) {
      return null;
    }

    return {
      globalId: data.globalId,
      addedByUserId: data.addedByUserId,
      workspaceId: data.workspaceId,
      orgId: data.orgId,
      type: data.type,
      role: data.role,
      encryptRole: data.encryptRole ? data.encryptRole : '',
      createdAt: data.createdAt,
      updatedAt: data.updatedAt,
      email: data.user && data.user.email ? data.user.email.toLowerCase() : '',
      firstname: data.user ? data.user.firstname : '',
      lastname: data.user ? data.user.lastname : '',
      userId: data.user ? data.user.id : 0,
      username: data.user ? data.user.username : '',
    };
  }

  /**
   * @param {[]} list
   * @return {[]}
   */
  static getResponseListJson(list) {
    let result = [];
    for (let data of list) {
      const jsonData = ModelWorkspaceMember.getResponseJson(data);
      result.push(jsonData);
    }
    return result;
  }

  /**
   * @param {{}} data
   * @return {{}}
   */
  static prepareModelData(data) {
    let item = {};

    item = ModelWorkspaceMember.prepareCommonProperties(item, data);
    item = ModelWorkspaceMember.prepareSyncOnlyProperties(item, data);

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    let curDate = date.now();

    item.erised = data.erised || false;
    item.globalId = data.globalId || data.global_id || item._id || generator.randomString(16);
    item._id = item.globalId.toString();
    item.addedByUserId = data.addedByUserId || 0;
    item.user = data.user || {
      id: 0,
      email: null,
      username: '',
      firstname: null,
      lastname: null
    };
    item.workspaceId = data.workspaceId || '';
    item.orgId = data.orgId || '';
    item.type = data.type || ModelWorkspaceMember.TYPE_FULL;
    item.role = data.role || ModelWorkspaceMember.ROLE_READER;
    item.encryptRole = data.encryptRole || ModelWorkspaceMember.ENCRYPTION_ROLE_DENY;
    item.createdAt = data.createdAt || curDate;
    item.updatedAt = data.updatedAt || curDate;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareSyncOnlyProperties(item, data) {
    item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
    item.syncDate = data.syncDate || 0;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["globalId", "addedByUserId", "workspaceId", "orgId", "type", "role", "encryptRole", "updatedAt"];
  }
}
