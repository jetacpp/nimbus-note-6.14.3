import chalk from "chalk";
import ReminderObj from "../../sync/process/db/ReminderObj";

export default class ModelReminder {
  static INTERVAL_0 = 0;
  static INTERVAL_30_MIN = 1800;
  static INTERVAL_HOUR = 3600;
  static INTERVAL_DAY = 86400;
  static INTERVAL_WEEK = 604800;
  static INTERVAL_MONTH = 2592000;
  static INTERVAL_YEAR = 31536000;

  /**
   * @param {{}} data
   * @return {{}|null}
   */
  static getResponseJson(data) {
    if (!data) {
      return null;
    }

    if (!data.reminder) {
      return null;
    }

    return {
      date: data.reminder.date,
      interval: data.reminder.interval,
      dateUpdated: data.dateUpdated,
      noteGlobalId: data.globalId,
      priority: data.reminder.priority,
      phone: data.reminder.phone
    }
  }

  /**
   * @param {[]} list
   * @return {[]}
   */
  static getResponseListJson(list) {
    return list.map((data) => {
      return ModelReminder.getResponseJson(data);
    });
  }

  /**
   * @param {{}} data
   * @returns {{}}
   */
  static prepareModelData(data) {
    let item = new ReminderObj();
    item = ModelReminder.prepareCommonProperties(item, data);
    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    item.date = data.date || null;
    item.interval = data.interval || null;
    return item;
  }
}