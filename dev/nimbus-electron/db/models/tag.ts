import chalk from "chalk";
import {default as generator} from "../../utilities/generatorHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";

let dbName = config.DB_TAG_MODEL_NAME;

export default class ModelTag {
  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.findAll(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.tag) {
      return callback(error.wrongInput(), null);
    }

    options.dbName = dbName;
    options.prepareModelData = ModelTag.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.tag) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.tag) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (data.tag || data.globalId) {
      options.dbName = dbName;
      PouchDb.remove(data, options, callback);
    } else {
      return callback(error.wrongInput(), 0);
    }
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicData(doc) {
    let tag = <any>{};
    if (doc) {
      tag.id = doc._id;
      tag.globalId = doc.globalId;
      tag.tag = doc.tag;
      tag.dateUpdated = doc.dateUpdated;

      //TODO: add properties from sync models
      if (doc.title || doc.tag)
        tag.title = doc.title || doc.tag || "";
      if (doc.oldTitle)
        tag.oldTitle = doc.oldTitle;
      if (doc.dateAdded)
        tag.dateAdded = doc.dateAdded;
      if (doc.uniqueUserName)
        tag.uniqueUserName = doc.uniqueUserName;
      if (doc.syncDate)
        tag.syncDate = doc.syncDate;
      if (doc.parentId)
        tag.parentId = doc.parentId;
      if (doc.needSync)
        tag.needSync = doc.needSync;
      if (doc.isChecked)
        tag.isChecked = doc.isChecked;
      if (doc.notesCount)
        tag.notesCount = doc.notesCount;
    }

    return tag;
  }

  /**
   * @param {{}} data
   * @returns {{}}
   */
  static prepareModelData(data) {
    let item = {};

    item = ModelTag.prepareCommonProperties(item, data);
    item = ModelTag.prepareSyncOnlyProperties(item, data);

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    item.erised = data.erised || false;
    item._id = data._id || generator.randomString(16);
    item.globalId = item._id.toString();
    item.tag = data.tag || data.title || "";
    item.dateUpdated = data.dateUpdated;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareSyncOnlyProperties(item, data) {
    item.tag = data.title || data.tag || "";
    item.title = data.title || data.tag || "";
    item.oldTitle = data.oldTitle || "";
    item.dateAdded = data.dateAdded || 0;
    item.uniqueUserName = data.uniqueUserName || "";
    item.parentId = data.parentId || "";
    item.needSync = data.needSync || false;
    item.syncDate = data.syncDate || 0;
    item.isChecked = !!data.isChecked;
    item.notesCount = data.notesCount || 0;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["globalId", "tag", "dateUpdated"];
  }
}
