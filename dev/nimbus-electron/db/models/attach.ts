import chalk from "chalk";
import fs = require('fs-extra');
import fileKind = require("file-kind");
import {default as date} from "../../utilities/dateHandler";
import {default as generator} from "../../utilities/generatorHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";
import {default as orgs} from "./orgs";

let dbName = config.DB_ATTACH_MODEL_NAME;

export default class ModelAttachment {
  static MAX_UPLOAD_FILE_SIZE = 10485760;
  static MAX_UPLOAD_FILE_SIZE_PREMIUM = 1073741824;

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static count(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.count(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.findAll(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.noteGlobalId || typeof (data.displayName) === "undefined") {
      return callback(error.wrongInput(), null);
    }

    if (!data.displayName) {
      data.displayName = ModelAttachment.getDefaultDisplayName();
    }

    options.dbName = dbName;
    options.prepareModelData = ModelAttachment.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId || !(updateFields.displayName || updateFields.location)) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.globalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    PouchDb.remove(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static removeByItemId(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.noteGlobalId) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @return {string}
   */
  static generateStoredFileUUID() {
    return generator.generateGUID();
  }

  /**
   * @param {string} mime
   * @return string
   */
  static getTypeByMimeType(mime) {
    let type = fileKind(mime);
    return type ? type : 'file';
  }

  /**
   * @return {string}
   */
  static getDefaultDisplayName() {
    return "Unnamed Attach";
  }

  /**
   * @param {{}} doc
   * @returns {{}}
   */
  static getPublicData(doc) {
    let attach = <any>{};
    if (doc) {
      attach.id = doc._id;
      attach.globalId = doc.globalId;
      attach.noteGlobalId = doc.noteGlobalId;
      attach.dateAdded = doc.dateAdded;
      attach.dateUpdated = doc.dateUpdated;
      attach.displayName = doc.displayName;
      attach.extra = doc.extra;
      attach.inList = doc.inList;
      attach.isEncrypted = doc.isEncrypted;
      attach.isScreenshot = doc.isScreenshot;
      attach.mime = doc.mime;
      attach.role = doc.role;
      attach.size = doc.size;
      attach.storedFileUUID = doc.storedFileUUID;
      attach.type = doc.type;

      //TODO: add properties from sync models
      if (doc.parentId || doc.noteGlobalId) {
        attach.parentId = doc.parentId || doc.noteGlobalId;
      }
      if (doc.syncDate)
        attach.syncDate = doc.syncDate;
      if (doc.uniqueUserName)
        attach.uniqueUserName = doc.uniqueUserName;
      if (doc.needSync)
        attach.needSync = doc.needSync;
      if (doc.location)
        attach.location = doc.location;
      if (doc.tempName)
        attach.tempName = doc.tempName;
      if (doc.typeExtra)
        attach.typeExtra = doc.typeExtra;
      if (doc.extension)
        attach.extension = doc.extension;
      if (doc.oldDisplayName)
        attach.oldDisplayName = doc.oldDisplayName;
      if (doc.isAttachedToNote)
        attach.isAttachedToNote = doc.isAttachedToNote;
      if (doc.fileUUID)
        attach.fileUUID = doc.storedFileUUID;
      if (doc.isDownloaded)
        attach.isDownloaded = doc.isDownloaded;
      if (doc.localPath)
        attach.localPath = doc.localPath;
      if (doc.isDownloadingRunning)
        attach.isDownloadingRunning = doc.isDownloadingRunning;
      if (doc.currentProgress)
        attach.currentProgress = doc.currentProgress;
      if (doc.currentProgress)
        attach.tryDownloadCounter = doc.tryDownloadCounter || 0;
    }

    return attach;
  }

  /**
   * @param {{}} data
   * @return {{}|null}
   */
  static getResponseJson(data) {
    if (!data) {
      return null;
    }

    return {
      dateAdded: data.dateAdded,
      dateUpdated: data.dateUpdated,
      displayName: data.displayName,
      displayUrl: data.location ? ModelAttachment.convertDisplayUrl(data.location) : '',
      extra: data.extra,
      globalId: data.globalId,
      inList: data.inList,
      isEncrypted: data.isEncrypted,
      isScreenshot: data.isScreenshot,
      mime: data.mime,
      noteGlobalId: data.noteGlobalId,
      role: data.role,
      size: data.size,
      storedFileUUID: data.storedFileUUID,
      type: data.type,
      isDownloaded: data.isDownloaded,
      tryDownloadCounter: data.tryDownloadCounter,
      needSync: data.needSync
    };
  }

  /**
   * @param {[]} list
   * @return {[]}
   */
  static getResponseListJson(list) {
    return list.map((data) => {
      return ModelAttachment.getResponseJson(data);
    });
  }

  /**
   * @param {{}} data
   * @returns {{}}
   */
  static prepareModelData(data) {
    let item = {};

    item = ModelAttachment.prepareCommonProperties(item, data);
    item = ModelAttachment.prepareSyncOnlyProperties(item, data);

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    let curDate = date.now();

    item.erised = data.erised || false;
    item.globalId = data.globalId || data.global_id || item._id || generator.randomString(16);
    item._id = item.globalId.toString();
    item.noteGlobalId = data.noteGlobalId || data.parentId || "";
    item.dateAdded = data.dateAdded || data.date_added || curDate;
    item.dateUpdated = data.dateUpdated || data.date_updated || curDate;
    item.displayName = data.display_name || data.displayName || "";
    item.extra = data.extra || {};
    item.inList = !!(data.inList || data.in_list || false);
    item.isEncrypted = !!(data.isEncrypted || data.is_encrypted || false);
    item.isScreenshot = !!data.isScreenshot;
    item.mime = data.mime || "text/plain";
    item.role = data.role || "attachment";
    item.size = data.size || 0;
    if (!item.storedFileUUID) {
      item.storedFileUUID = data.storedFileUUID || data.file_uuid || data.fileUUID || ModelAttachment.generateStoredFileUUID();
    }
    item.type = data.type || ModelAttachment.getTypeByMimeType(data.mime);
    item.hashKey = data.hashKey;
    if (data.tempName) {
      item.tempName = data.tempName || "";
    }
    item.tryDownloadCounter = data.tryDownloadCounter || 0;
    if (typeof (data.isDownloaded) !== "undefined") {
      item.isDownloaded = !!data.isDownloaded;
    }
    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareSyncOnlyProperties(item, data) {
    item.global_id = data.global_id || data.globalId || item._id;
    item.parentId = data.parentId || data.noteGlobalId || "";
    item.displayName = data.display_name || data.displayName || "";
    item.uniqueUserName = data.uniqueUserName || "";
    item.syncDate = data.syncDate || 0;
    item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
    item.location = data.location || "";
    item.tempName = data.tempName || "";
    item.typeExtra = data.typeExtra || "";
    item.extension = data.extension || "";
    item.oldDisplayName = data.oldDisplayName || "";
    item.isAttachedToNote = !!(data.isAttachedToNote || false);
    item.fileUUID = data.file_uuid || data.fileUUID || data.storedFileUUID || ModelAttachment.generateStoredFileUUID();
    if (typeof (data.isDownloaded) !== "undefined") {
      item.isDownloaded = !!data.isDownloaded;
    }
    item.localPath = data.localPath || "";
    item.isDownloadingRunning = !!(data.isDownloadingRunning || false);
    item.currentProgress = data.currentProgress || 0;
    item.tryDownloadCounter = data.tryDownloadCounter || 0;

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @return {[string]}
   */
  static getIndexList() {
    return ["globalId", "noteGlobalId", "dateAdded", "dateUpdated", "displayName", "inList",
      "isEncrypted", "isScreenshot", "mime", "role", "size", "storedFileUUID", "type", "hashKey"];
  }

  /**
   * @param {{workspaceId:string, size:int}} inputData
   * @return {boolean}
   */
  static async checkFileUploadLimit(inputData) {
    const {size} = inputData;
    let maxSize = await ModelAttachment.getFileUploadLimit(inputData);
    return size <= maxSize;
  }

  /**
   * @param {{workspaceId:string}} inputData
   * @return {Promise<int>}
   */
  static async getFileUploadLimit(inputData) {
    const {workspaceId} = inputData;
    let getLimitNotesMaxAttachmentSize = async () => {
      // @ts-ignore
      return new Promise(async (resolve) => {
        const limits = <any>await orgs.getLimits(workspaceId);
        let maxLimit = limits && limits.attachmentSize ? limits.attachmentSize : ModelAttachment.MAX_UPLOAD_FILE_SIZE;
        // TODO: return less value: limits.attachmentSize < limits.textSize on demand
        return resolve(maxLimit);
      });
    };

    return await getLimitNotesMaxAttachmentSize();
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   * @return {Promise<string>}
   */
  static async getAttachmentPath(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      const {workspaceId, globalId} = inputData;
      let queryData = {globalId: globalId};
      ModelAttachment.find(queryData, {workspaceId}, async (err, attachItem) => {
        if (attachItem && Object.keys(attachItem).length) {
          if (!PouchDb.getClientAttachmentPath()) {
            return resolve(null);
          }

          let targetPath = `${PouchDb.getClientAttachmentPath()}/${attachItem.storedFileUUID}`;
          // @ts-ignore
          let exists = await fs.exists(targetPath);
          // @ts-ignore
          if (!exists) {
            return resolve(null);
          }

          return resolve(targetPath);
        } else {
          return resolve(null);
        }
      });
    });
  }

  static convertDisplayUrl(location) {
    if(!location) { return location; }
    return location.replace(config.everhelperAttachmentBoxUrl, config.nimbusAttachmentBoxUrl);
  }
}
