import chalk from "chalk";
import {default as date} from "../../utilities/dateHandler";
import {default as generator} from "../../utilities/generatorHandler";
import {default as error} from "../../utilities/customError";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";
import ModelWorkspace, {default as workspace} from "./workspace";
import ModelUser, {default as user} from "./user";
import {default as attach} from "./attach";
import {default as text} from "./text";
import {default as auth} from "../../auth/auth";
import downloadOrganizations from "../../sync/process/rx/handlers/downloadOrganizations";
import SelectedWorkspace from "../../workspace/SelectedWorkspace";
import {default as urlParser} from "../../utilities/urlParser";
import fs from "fs-extra";

let dbName = config.DB_ORG_MODEL_NAME;

export default class ModelOrgs {
  static TYPE_PRIVATE = 'private';
  static TYPE_BUSINESS = 'business';

  static SERVICE_TYPE_NOTES = 'notes';
  static SERVICE_TYPE_CAPTURES = 'capture';

  static DEFAULT_NAME = 'My Organization';

  static DEFAULT_WORKSPACES_MAX = 2;
  static DEFAULT_ENCRYPTION_KEYS_PER_WORKSPACE = 1;
  static DEFAULT_MEMBERS_PER_WORKSPACE = 3;

  static NOTES_PER_WORKSPACE_MAX = 50;
  static NOTES_PER_WORKSPACE_MAX_PRO = 100000;

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static count(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.count(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static findAll(data, options, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.findAll(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static add(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    if (!data.id) {
      return callback(error.wrongInput(), null);
    }

    options.dbName = dbName;
    options.prepareModelData = ModelOrgs.prepareModelData;
    PouchDb.create(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} updateFields
   * @param {{}} options
   * @param {Function} callback
   */
  static update(data, updateFields, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.id) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = updateFields;
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static remove(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (!data.id) {
      return callback(error.wrongInput(), 0);
    }
    options.dbName = dbName;
    options.updateFields = PouchDb.getErasedUpdateFields();
    PouchDb.update(data, options, callback);
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static erase(data, options, callback = (err, res) => {
  }) {
    data = data || {};
    options = options || {};
    if (data.id) {
      options.dbName = dbName;
      PouchDb.remove(data, options, callback);
    } else {
      return callback(error.wrongInput(), 0);
    }
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicData(doc) {
    let org = <any>{};
    if (doc) {
      org.id = doc._id;
      org.createdAt = doc.createdAt;
      org.updatedAt = doc.updatedAt;

      //TODO: add properties from sync models
      if (doc.type)
        org.type = doc.type;
      if (doc.serviceType)
        org.serviceType = doc.serviceType;
      if (doc.title)
        org.title = doc.title;
      if (doc.usage)
        org.usage = doc.usage;
      if (doc.limits)
        org.limits = doc.limits;
      if (doc.user)
        org.user = doc.user;
      if (doc.user)
        org.user = doc.user;
      if (doc.sub)
        org.sub = doc.sub;
      if (doc.suspended)
        org.suspended = doc.suspended;
      if (doc.suspendedReason)
        org.suspendedReason = doc.suspendedReason;

      if (doc.syncDate)
        org.syncDate = doc.syncDate;
      if (doc.needSync)
        org.needSync = doc.needSync;
    }

    return org;
  }

  /**
   * @param {{}} data
   * @return {{}|null}
   */
  static getResponseJson(data) {
    if (!data) {
      return null;
    }

    return {
      id: data.id,
      // type: data.type,
      type: data.serviceType || ModelOrgs.SERVICE_TYPE_NOTES,
      title: data.title,
      description: data.description,
      usage: data.usage,
      limits: data.limits,
      user: data.user,
      sub: data.sub,
      domain: data.domain,
      suspended: data.suspended,
      suspendedAt: data.suspendedAt,
      suspendedReason: data.suspendedReason,
      access: data.acccess,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt,
    };
  }

  /**
   * @param {{}} data
   * @return {{}}
   */
  static prepareModelData(data) {
    let item = {};

    item = ModelOrgs.prepareCommonProperties(item, data);
    item = ModelOrgs.prepareSyncOnlyProperties(item, data);

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareCommonProperties(item, data) {
    let curDate = date.now();

    item.erised = data.erised || false;
    item.id = data.id || item.id || generator.generateOrgID(data.userId);
    item._id = item.id.toString();
    item.title = data.title || ModelOrgs.DEFAULT_NAME;
    item.type = data.type || ModelOrgs.TYPE_PRIVATE;
    item.serviceType = data.serviceType || ModelOrgs.SERVICE_TYPE_NOTES;
    item.usage = data.usage || {
      current: 0,
      max: 0,
      daysToReset: 0
    };
    item.limits = data.limits || {
      traffic: 0,
      workspaces: 0,
      textSize: 0,
      attachmentSize: 0,
      encryptionKeysPerWorkspace: 0,
      membersPerWorkspace: 0,
      notesPerWorkspace: ModelOrgs.NOTES_PER_WORKSPACE_MAX
    };
    if (item.type === ModelOrgs.TYPE_PRIVATE) {
      item.user = data.user || {
        id: 0,
        email: null,
        username: '',
        firstname: null,
        lastname: null
      };
    }

    item.sub = data.sub || null;
    item.domain = data.domain || null;
    item.description = data.description || null;
    item.suspended = data.suspended || false;
    item.suspendedReason = data.suspendedReason || null;
    item.suspendedAt = data.suspendedAt || null;
    item.smallLogoUrl = data.smallLogoUrl || null;
    item.bigLogoUrl = data.bigLogoUrl || null;
    item.createdAt = data.createdAt || curDate;
    item.updatedAt = data.updatedAt || curDate;
    item.access = data.access || null;
    item.features = data.features || [];

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareSyncOnlyProperties(item, data) {
    item.needSync = typeof (data.needSync) === "undefined" ? true : data.needSync;
    item.syncDate = data.syncDate || 0;
    if (typeof (data.type) !== 'undefined') {
      item.type = data.type;
    }
    if (typeof (data.serviceType) !== 'undefined') {
      item.serviceType = data.serviceType;
    }
    if (typeof (data.title) !== 'undefined') {
      item.title = data.title;
    }
    if (typeof (data.title) !== 'undefined') {
      item.title = data.title;
    }
    if (typeof (data.user) !== 'undefined') {
      item.user = data.user;
    }
    if (typeof (data.usage) !== 'undefined') {
      item.usage = data.usage;
    }
    if (typeof (data.limits) !== 'undefined') {
      item.limits = data.limits;
    }
    if (typeof (data.sub) !== 'undefined') {
      item.sub = data.sub;
    }
    if (typeof (data.domain) !== 'undefined') {
      item.domain = data.domain;
    }
    if (typeof (data.description) !== 'undefined') {
      item.description = data.description;
    }
    if (typeof (data.features) !== 'undefined') {
      item.features = data.features;
    }
    if (typeof (data.access) !== 'undefined') {
      item.access = data.access;
    }
    if (typeof (data.features) !== 'undefined') {
      item.features = data.features;
    }
    if (typeof (data.suspended) !== 'undefined') {
      item.suspended = data.suspended;
    }
    if (typeof (data.suspendedReason) !== 'undefined') {
      item.suspendedReason = data.suspendedReason;
    }
    if (typeof (data.suspendedAt) !== 'undefined') {
      item.suspendedAt = data.suspendedAt;
    }
    if (typeof (data.smallLogoUrl) !== 'undefined') {
      item.smallLogoUrl = data.smallLogoUrl;
    }
    if (typeof (data.bigLogoUrl) !== 'undefined') {
      item.bigLogoUrl = data.bigLogoUrl;
    }

    return item;
  }

  /**
   * @param {{}} item
   * @param {{}} data
   * @return {{}}
   */
  static prepareItemDbProperties(item, data) {
    return PouchDb.prepareItemDbProperties(item, data)
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["id", "type", "serviceType", "title", "sub", "domain", "suspended", "suspendedAt", "updatedAt"];
  }

  static async getDefaultOrg() {
    const getWorkspacesCount = () => {
      // @ts-ignore
      return new Promise((resolve) => {
        workspace.count({}, {}, (err, workspacesCount) => {
          if (err || !workspacesCount) {
            return resolve(0);
          }
          return resolve(workspacesCount);
        });
      });
    };

    const getDefaultOrgData = () => {
      // @ts-ignore
      return new Promise((resolve) => {
        let returnData = {
          traffic: {
            current: 0,
            max: user.MAX_UPLOAD_SIZE,
            daysToReset: 0
          },
          user: {
            id: 0,
            email: '',
            username: '',
            firstname: '',
            lastname: ''
          }
        };
        auth.getUser((err, authInfo) => {
          if (authInfo && Object.keys(authInfo).length) {
            const findQuery = {};
            user.find(findQuery, {}, (err, userItem) => {
              if (userItem && Object.keys(userItem).length) {
                return resolve({
                  traffic: {
                    current: userItem.usageCurrent,
                    max: userItem.usageMax,
                    daysToReset: userItem.quotaResetDate
                  },
                  user: {
                    id: userItem.userId,
                    email: userItem.email ? userItem.email.toLowerCase() : '',
                    username: userItem.username,
                    firstname: '',
                    lastname: ''
                  }
                });
              } else {
                return resolve(returnData);
              }
            });
          } else {
            return resolve(returnData);
          }
        });
      });
    };

    const workspacesCount = await getWorkspacesCount();
    const orgData = <any>await getDefaultOrgData();
    return {
      id: '',
      title: '',
      description: '',
      usage: {
        traffic: orgData.traffic,
        workspaces: {
          current: workspacesCount,
          max: ModelOrgs.DEFAULT_WORKSPACES_MAX
        }
      },
      limits: {
        encryptionKeysPerWorkspace: ModelOrgs.DEFAULT_ENCRYPTION_KEYS_PER_WORKSPACE,
        membersPerWorkspace: ModelOrgs.DEFAULT_MEMBERS_PER_WORKSPACE,
        notesPerWorkspace: ModelOrgs.NOTES_PER_WORKSPACE_MAX
      },
      user: orgData.user
    };
  }

  static async getActive() {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const userInfo = <any>await auth.getUserAsync();
      ModelOrgs.findAll({}, {}, (err, orgsList) => {
        if (err || !orgsList) {
          return resolve(null);
        }

        if (!orgsList.length) {
          return resolve(null);
        }

        let userOrgs = orgsList.filter((orgInstance) => (
          orgInstance.user && orgInstance.user.email &&
          (orgInstance.user.email.toLowerCase() === userInfo.email.toLowerCase()))
        );

        if (!userOrgs.length) {
          return resolve(null);
        }

        return resolve(userOrgs[0]);
      })
    });
  }

  static async getById(id) {
    // @ts-ignore
    return new Promise((resolve) => {
      if (!id) {
        return resolve(null);
      }

      ModelOrgs.find({id}, {}, (err, orgsInstance) => {
        if (err || !orgsInstance) {
          return resolve(null);
        }

        return resolve(orgsInstance);
      })
    });
  }

  static getOrganizationHost(orgInstance) {
    const { domain, sub, type } = orgInstance;
    const { authApiServiceDomain } = config;

    if(type === ModelOrgs.TYPE_PRIVATE) {
      return authApiServiceDomain;
    }

    if(domain) {
      return domain;
    }

    if(sub) {
      return `${sub}.${authApiServiceDomain}`;
    }

    return authApiServiceDomain;
  }

  /**
   * @param {[]} list
   * @return {[]}
   */
  static async getResponseListJson(list) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      let result = [];

      const accountUserId = await user.getAccountUserId();
      for (let data of list) {
        if (!data) {
          continue;
        }

        const { limits, user } = data;
        const userId = user ? user.id : accountUserId;
        const membersPerWorkspace = limits ? limits.membersPerWorkspace : ModelOrgs.DEFAULT_MEMBERS_PER_WORKSPACE;

        const electronHost = ModelOrgs.getOrganizationHost(data);

        result.push({
          globalId: data.id,
          userId,
          createdAt: data.createdAt,
          updatedAt: data.updatedAt,
          title: data.title,
          // type: data.type || '',
          type: data.serviceType || ModelOrgs.SERVICE_TYPE_NOTES,
          sub: data.sub,
          suspended: data.suspended,
          suspendedReason: data.suspendedReason,
          suspendedAt: data.suspendedAt,
          domain: data.domain,
          description: data.description || '',
          peopleToCollaborate: null,
          maxMembers: membersPerWorkspace,
          pricingModel: null,
          access: data.access,
          electronHost,
          smallLogoStoredFileUUID: await ModelOrgs.getOrganizationSmallLogo(data),
        });
      }
      return resolve(result);
    });
  }

  static async getOrganizationSmallLogo(data) {
    let response = null;
    if(data.smallLogoUrl) {
      const {smallLogoUrl} = data;
      if(smallLogoUrl) {
        let routeParams = urlParser.getPathParams(smallLogoUrl);
        let storedFileUUID = typeof (routeParams[2] !== 'undefined') ? routeParams[2] : null;
        if(storedFileUUID) {
          let targetPath = `${PouchDb.getClientAttachmentPath()}/${storedFileUUID}`;
          // @ts-ignore
          let exists = await fs.exists(targetPath);
          // @ts-ignore
          if (exists) {
            response = `avatar/${storedFileUUID}`;
          }
        }
      }
      return response;
    }
  }

  /**
   * @param {{}} query
   */
  static async getAllByQuery(query) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      ModelOrgs.findAll(query, {}, (err, orgsList) => {
        if(err || !orgsList) {
          return resolve([]);
        }
        return resolve(orgsList);
      });
    });
  }

  /**
   * @param {string} workspaceId
   */
  static async getByWorkspaceId(workspaceId) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const workspaceInstance = <{orgId:string}>(workspaceId ? await workspace.getById(workspaceId) : await workspace.getDefault());
      if (workspaceInstance && workspaceInstance.orgId) {
        const orgInstance = await ModelOrgs.getById(workspaceInstance.orgId);
        return resolve(orgInstance);
      } else {
        return resolve(null);
      }
    });
  }

  /**
   * @param {string} workspaceId
   */
  static async getLimits(workspaceId) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const orgsInstance = <any>await ModelOrgs.getByWorkspaceId(workspaceId);
      if(!orgsInstance) {
        return resolve(ModelOrgs.getDefaultLimits());
      }
      const {limits} = orgsInstance;
      if(!limits) {
        return resolve(ModelOrgs.getDefaultLimits());
      }
      return resolve(limits);
    });
  }

  /**
   * @param {string} id
   */
  static async getLimitsByOrgId(id) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const orgsInstance = <{limits:{}}>await ModelOrgs.getById(id);
      if(!orgsInstance) {
        const selectedWorkspace = <{org:{limits:{}}}>await SelectedWorkspace.get();

        if(selectedWorkspace && selectedWorkspace.org) {
          //if(selectedWorkspace.org.id === id) {
            const {limits} = selectedWorkspace.org;
            if(!limits) {
              return resolve(ModelOrgs.getDefaultLimits());
            }
            return resolve(limits);
          //}
        }

        return resolve(ModelOrgs.getDefaultLimits());
      }

      const {limits} = orgsInstance;
      if(!limits) {
        return resolve(ModelOrgs.getDefaultLimits());
      }
      return resolve(limits);
    });
  }

  static getDefaultLimits() {
    return {
      attachmentSize: attach.MAX_UPLOAD_FILE_SIZE,
      encryptionKeysPerWorkspace: ModelOrgs.DEFAULT_ENCRYPTION_KEYS_PER_WORKSPACE,
      membersPerWorkspace: ModelOrgs.DEFAULT_MEMBERS_PER_WORKSPACE,
      textSize: text.MAX_TEXT_SIZE,
      traffic: user.MAX_UPLOAD_SIZE,
      workspaces: ModelOrgs.DEFAULT_WORKSPACES_MAX,
      notesPerWorkspace: ModelOrgs.NOTES_PER_WORKSPACE_MAX
    };
  }

  static syncUserOrganizations() {
    // @ts-ignore
    return new Promise(async (resolve) => {
      downloadOrganizations({skipSyncHandlers: true}, () => {
        return resolve();
      });
    });
  }

  static async syncUserOrganizationsWorkspaces() {
    await ModelUser.syncUserVariables();
    await ModelOrgs.syncUserOrganizations();
    await ModelWorkspace.syncUserWorkspaces();
  }
}
