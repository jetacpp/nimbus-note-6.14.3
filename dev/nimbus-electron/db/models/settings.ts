import chalk from "chalk";
import {default as error} from "../../utilities/customError";
import {default as generator} from "../../utilities/generatorHandler";
import {default as config} from "../../../config.runtime";
import {default as PouchDb} from "../../../pdb";

let dbName = config.DB_SETTINGS_MODEL_NAME;

export default class ModelSettings {
  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static find(data, options = <any>{}, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.find(data, options, (err, item) => {
      callback(err, ModelSettings.getPublicData(item));
    });
  }

  /**
   * @param {{}} data
   * @param {{}} options
   * @param {Function} callback
   */
  static count(data, options = <any>{}, callback = (err, res) => {
  }) {
    options.dbName = dbName;
    PouchDb.count(data, options, callback);
  }

  static create(data = <any>{}, updateFields, callback = (err, res) => {
  }) {
    if (!data.property) {
      return callback(error.wrongInput(), null);
    }

    /**
     * @param {{}} data
     * @param {{}} updateFields
     * @param {Function} callback
     */
    ModelSettings.count(data, {}, (err, findCount) => {
      if (err) {
        callback(err, null);
      }

      updateFields.property = data.property;

      let options = <any>{dbName: dbName};
      if (findCount) {
        options.updateFields = updateFields;
        PouchDb.update(data, options, callback);
      } else {
        options.prepareModelData = ModelSettings.prepareModelData;
        PouchDb.create(updateFields, options, callback);
      }
    });
  }

  /**
   * @param {{}} data
   * @param {Function} callback
   */
  static delete(data = <any>{}, callback = (err, res) => {
  }) {
    if (!data.property) {
      return callback(error.wrongInput(), 0);
    }

    let options = {dbName: dbName};
    PouchDb.remove(data, options, callback);
  }

  /**
   * @param {string} key
   * @param {Function} callback
   */
  static get(key, callback = (err, res) => {
  }) {
    ModelSettings.find({property: key}, {}, callback);
  }

  /**
   * @param {string} key
   * @param {*} value
   * @param {Function} callback
   */
  static set(key, value, callback = (err, res) => {
  }) {
    ModelSettings.create({property: key}, {value: value}, callback);
  }

  /**
   * @param {string} key
   * @param {Function} callback
   */
  static has(key, callback = (err, res) => {
  }) {
    ModelSettings.count({property: key}, {}, (err, itemCount) => {
      callback(err, !!itemCount);
    });
  }

  /**
   * @param {string} key
   * @param {Function} callback
   */
  static remove(key, callback = (err, res) => {
  }) {
    ModelSettings.delete({property: key}, (err, itemCount) => {
      callback(err, !!itemCount);
    });
  }

  /**
   * @param {{}} doc
   * @return {{}}
   */
  static getPublicData(doc) {
    let item = {};
    if (doc) {
      return doc.value;
    }

    return item;
  }

  /**
   * @param {{}} data
   * @return {{}}
   */
  static prepareModelData(data) {
    let item = <any>{};
    item._id = data._id || generator.randomString(16);
    item.property = data.property;
    item.value = data.value;
    if (typeof (data.value) !== "undefined") {
      item.value = data.value;
    }

    return item;
  }

  /**
   * @return {[]}
   */
  static getIndexList() {
    return ["property", "value"];
  }

  static getAppSettings() {
    // @ts-ignore
    return new Promise(async resolve => {
      let appSettings = {}
      appSettings = await ModelSettings.getAutoLoadSetting(appSettings)
      appSettings = await ModelSettings.getSpellCheckerSetting(appSettings)
      return resolve(appSettings)
    });
  }

  static getAutoLoadSetting(appSettings) {
    return new Promise(resolve => {
      ModelSettings.get(`autoLoad`, (err, autoLoad) => {
        if (typeof (autoLoad) === "object" && Object.keys(autoLoad).length === 0) {
          appSettings.autoLoad = true;
        } else {
          appSettings.autoLoad = !!autoLoad;
        }
        return resolve(appSettings);
      });
    })
  }

  static getSpellCheckerSetting(appSettings) {
    return new Promise(resolve => {
      ModelSettings.get(`spellChecker`, (err, spellChecker) => {
        if (typeof (spellChecker) === "object" && Object.keys(spellChecker).length === 0) {
          appSettings.spellChecker = true;
        } else {
          appSettings.spellChecker = !!spellChecker;
        }
        return resolve(appSettings);
      });
    })
  }

  static needSetAutoLoad() {
    return new Promise((resolve) => {
      ModelSettings.get(`autoLoad`, (err, autoLoad) => {
        if(typeof autoLoad === 'undefined') { return resolve(true) }
        const result = typeof (autoLoad) === "object" && Object.keys(autoLoad).length === 0;
        resolve(result);
      });
    });
  }

  static hasAutoLoad() {
    return new Promise((resolve) => {
      ModelSettings.get(`autoLoad`, (err, autoLoad) => {
        if(typeof autoLoad === 'number') { return resolve(!!autoLoad) }
        if(typeof autoLoad === 'boolean') { return resolve(autoLoad) }
        const result = !(typeof (autoLoad) === "object" && Object.keys(autoLoad).length === 0);
        resolve(result);
      });
    });
  }

  static hasSpellChecker() {
    return new Promise((resolve) => {
      ModelSettings.get(`spellChecker`, (err, spellChecker) => {
        if(typeof spellChecker === 'number') { return resolve(!!spellChecker) }
        if(typeof spellChecker === 'boolean') { return resolve(spellChecker) }
        const result = (typeof (spellChecker) === "object" && Object.keys(spellChecker).length === 0);
        resolve(result);
      });
    });
  }

  static setAutoLoad(autoLoad) {
    return new Promise((resolve) => {
      ModelSettings.set(`autoLoad`, autoLoad, () => {
        return resolve(!!autoLoad)
      });
    });
  }

  static setSpellChecker(spellChecker) {
    return new Promise((resolve) => {
      ModelSettings.set(`spellChecker`, spellChecker, () => {
        return resolve(!!spellChecker)
      });
    });
  }
}
