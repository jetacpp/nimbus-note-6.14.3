import chalk from "chalk";
import fs from "fs-extra";
import {default as PouchDb} from "../../pdb";
import {ipcMain} from "electron";
import {default as textEditor} from "./models/textEditor";
import {default as workspace} from "./models/workspace";
import {default as item} from "./models/item";
import {default as date} from "../utilities/dateHandler";
import {default as urlParser} from "../utilities/urlParser";
import {default as appWindow, default as appWindowInstance} from "../window/instance";
import {default as socketConnection} from "../sync/socket/socketFunctions";
import {default as text} from "./models/text";
import {default as attach} from "./models/attach";
import {default as NimbusSDK} from "../sync/nimbussdk/net/NimbusSDK";
import SelectedWorkspace from "../workspace/SelectedWorkspace";
import {default as AttachmentObjRepository} from "../sync/process/repositories/AttachmentObjRepository";
import {default as AttachmentSingleDownloader} from "../sync/downlaoder/AttachmentSingleDownloader";
import {setUpdateProps} from "../utilities/syncPropsHandler";
import {default as textHandler} from "../utilities/textHandler";
import {default as appOnlineState} from "../online/state";

export default class TextEditor {
  /**
   * @param {{}} data
   */
  static put(data) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {valueSerialized, valueShort, valueFull} = data;
      const {globalId, noteGlobalId, workspaceId} = await TextEditor.getNoteData(data);
      const curDate = date.now();
      if(typeof (valueShort) !== 'undefined' || typeof (valueFull) !== 'undefined') {
        let queryData = {noteGlobalId};
        let updateData = {
          textShort: valueShort,
          text: valueFull,
          'length': valueFull.text ? valueFull.text.length : 0
        };
        text.update(queryData, updateData, {workspaceId}, (err, count) => {
          if (count) {
            TextEditor.socketTextMessage({workspaceId, noteGlobalId});
          }
        });
      }
      textEditor.add({
        globalId,
        noteGlobalId,
        text: valueSerialized,
        dateUpdated: curDate
      }, {workspaceId}, (err, res) => {
        if (err) {
          return resolve(null);
        }
        return resolve(res.text);
      });
    });
  }

  static saveSerializedItem(workspaceId, noteGlobalId, globalId, valueSerialized) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const curDate = date.now();
      textEditor.add({
        globalId,
        noteGlobalId,
        text: valueSerialized,
        dateUpdated: curDate
      }, {workspaceId}, (err, res) => {
        if (err) {
          return resolve(null);
        }
        return resolve(res.text);
      });
    });
  }

  /**
   * @param {{}} data
   */
  static putArray(data) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {entries, valueShort, valueFull} = data;
      const {noteGlobalId, workspaceId} = await TextEditor.getNoteData(data);

      for(let entry of entries) {
        await TextEditor.saveSerializedItem(workspaceId, noteGlobalId, entry.key, entry.valueSerialized);
      }

      if(typeof (valueShort) !== 'undefined' || typeof (valueFull) !== 'undefined') {
        let queryData = {noteGlobalId};
        let updateData = {
          textShort: valueShort,
          text: valueFull,
          'length': valueFull.text ? valueFull.text.length : 0
        };
        text.update(queryData, updateData, {workspaceId}, (err, count) => {
          if (count) {
            TextEditor.socketTextMessage({workspaceId, noteGlobalId});
          }
        });
      }

      return resolve(true);
    });
  }

  /**
   * @param {{key:string}} data
   */
  static get(data) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {key} = data;

      if (!key) {
        return resolve(null);
      }

      const {globalId, workspaceId} = await TextEditor.getNoteData(data);
      textEditor.find({globalId}, {workspaceId}, (err, res) => {
        if (err) {
          return resolve(null);
        }
        return resolve(res.text);
      });
    });
  }

  /**
   * @param {{keyFrom:string, keyTo:string}} data
   */
  static getFromRange(data) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {keyFrom, keyTo} = data;

      if (!keyFrom) {
        return resolve([]);
      }

      let {globalId: globalIdFrom, workspaceId} = await TextEditor.getNoteData({key: keyFrom});
      let {globalId: globalIdTo} = await TextEditor.getNoteData({key: keyTo});

      let query = {globalId: {}};
      /*if(!globalIdFrom) {
        return resolve([]);
      } else {
        query['$gte'] = globalIdFrom;
      }

      if(globalIdTo) {
        query['$lte'] = globalIdTo;
      }*/

      const regexp = new RegExp(urlParser.escapeRegExp(globalIdFrom.split('#')[0]));
      query.globalId = {'$regex': regexp};

      textEditor.findAll(query, {workspaceId}, (err, res) => {
        if (err) {
          return resolve([]);
        }
        return resolve(res.filter(textInstance => textInstance.text).map(textInstance => {
          return textInstance.text;
        }));
      });
    });
  }

  /**
   * @param {{keyFrom:string, keyTo:string}} data
   */
  static getKeysFromRange(data) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {keyFrom, keyTo} = data;

      if (!keyFrom) {
        return resolve([]);
      }

      const {globalId: globalIdFrom, workspaceId} = await TextEditor.getNoteData({key: keyFrom});
      const {globalId: globalIdTo} = await TextEditor.getNoteData({key: keyTo});

      let query = {globalId: {}};
      /*if(!globalIdFrom) {
        return resolve([]);
      } else {
        query['$gte'] = globalIdFrom;
      }

      if(globalIdTo) {
        query['$lte'] = globalIdTo;
      }*/

      const regexp = new RegExp(urlParser.escapeRegExp(globalIdFrom.split('#')[0]));
      query.globalId = {'$regex': regexp};

      textEditor.findAll(query, {workspaceId}, (err, res) => {
        if (err) {
          return resolve([]);
        }
        return resolve(res.map(textInstance => textInstance.globalId));
      });
    });
  }

  /**
   * @param {{keys:[string]}} data
   */
  static delete(data) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      const {keys} = data;

      if (!keys) {
        return resolve(0);
      }

      if (!keys.length) {
        return resolve(0);
      }

      const {workspaceId} = await TextEditor.getNoteData(keys[0]);
      textEditor.erase({globalId: {$in: keys}}, {workspaceId}, (err, res) => {
        if (err) {
          return resolve(0);
        }
        return resolve(res);
      });
    });
  }

  /**
   * @param {{key:string, valueSerialized:*}} args
   */
  static async getNoteData(args) {
    const {key} = args;
    let workspaceId, noteGlobalId;
    if (key) {
      const data = key.split('#')[0];
      if (data) {
        const noteData = data.split('_');
        if (noteData.length > 1) {
          workspaceId = noteData[0];
          noteGlobalId = noteData[1];
        }
      }
    }

    workspaceId = workspaceId ? await workspace.getLocalId(workspaceId) : null;

    return {
      globalId: key,
      noteGlobalId,
      workspaceId
    }
  }

  /**
   * @param {{noteId:string}} inputData
   */
  static updateSyncedStatus(inputData) {
    if (appWindowInstance.get()) {
      appWindowInstance.get().webContents.send('event:editor:update:synced-status:request', inputData);
    }
  }

  /**
   * @param {{url:string, room:string, cid: string, note: {synced:boolean}, workspaceId:string, noteId:string}} inputData
   * @return Promise<{result:boolean}>
   */
  static makeBgNoteSync(inputData) {
    // @ts-ignore
    return new Promise((resolve) => {
      if (appWindowInstance.get()) {
        ipcMain.removeAllListeners('event:editor:sync-callback:data:request');

        const timeout = setTimeout(() => {
          ipcMain.removeAllListeners('event:editor:sync-callback:data:request');
          return resolve({
            result: false
          });
        }, 10000);

        ipcMain.once('event:editor:sync-callback:data:request', async (event, args) => {
          if (timeout) {
            clearTimeout(timeout);
          }

          if (!args) {
            return resolve({
              result: false
            });
          }

          return resolve(args);
        });

        appWindowInstance.get().webContents.send('event:editor:bg:note-sync:request', inputData);
      }
    });
  }

  /**
   * @param {{workspaceId:string, globalId:string}} inputData
   */
  static async updateNote(inputData) {
    let {workspaceId, globalId, hasLocalChanges} = inputData;
    if (workspaceId && globalId) {
      workspaceId = await workspace.getLocalId(workspaceId);
      const curDate = date.now();
      const itemQueryData = {globalId};
      let itemSaveData = <any>{
        "dateUpdated": curDate,
        "updatedAt": curDate,
        "dateUpdatedUser": curDate,
      };

      if(hasLocalChanges) {
        itemSaveData["syncDate"] = curDate;
        itemSaveData["needSync"] = true;
      }

      item.update(itemQueryData, itemSaveData, {workspaceId}, () => {
        socketConnection.sendItemUpdateMessage({workspaceId, globalId});
      });
    }
  }

  static async updateNoteText({noteId, workspaceId, textString}) {
    if (workspaceId && noteId) {
      workspaceId = await workspace.getLocalId(workspaceId);
      let curDate = date.now();
      let saveTextData = {
        noteGlobalId: noteId,
        text: textString,
        textShort: textHandler.stripTags(textString),
        'length': textString.text ? textString.text.length : 0
      };

      let queryData = {noteGlobalId: saveTextData.noteGlobalId};
      text.find(queryData, {workspaceId}, (err, textInstance) => {
        if(err) {
          return;
        }

        if(textInstance) {
          let queryUpdateData = {noteGlobalId: saveTextData.noteGlobalId};
          text.update(queryUpdateData, setUpdateProps(saveTextData, curDate), {workspaceId}, () => {});
        } else {
          const saveItemData = text.getDefaultModel(saveTextData.noteGlobalId);
          text.add(setUpdateProps(saveItemData, curDate), {workspaceId}, () => {});
        }
      });
    }
  }

  /**
   * @param {{item:string, options:{}}} inputData
   */
  static async downloadAttach({item, options}) {
    // @ts-ignore
    return new Promise(async (resolve) => {
      if (!appOnlineState.get()) {
        return resolve(null);
      }


      if(!item || !options) {
        return resolve(null);
      }

      const noteId = item;
      const attachmentId = options.attachmentId;
      const workspaceId = await SelectedWorkspace.getGlobalId();

      NimbusSDK.getApi().getStructureNotes({workspaceId, start: 0, amount: 1, globalId: noteId}, async (err, body) => {
        if (err || !body) {
          return resolve(null);
        }

        const { notes } = body;
        if(!notes) {
          return resolve(null);
        }

        if(!notes.length) {
          return resolve(null);
        }

        const note = notes[0];
        if(!note) {
          return resolve(null);
        }

        const { attachements } = note;
        if(!attachements) {
          return resolve(null);
        }

        if(!attachements.length) {
          return resolve(null);
        }

        const attachmentData = attachements.find((attachmentItem) => (attachmentItem.global_id === attachmentId));
        if(!attachmentData) {
          return resolve(null);
        }

        let attachmentObj = <any>attach.prepareModelData(attachmentData);
        attachmentObj.noteGlobalId = noteId;
        attachmentObj.globalId = attachmentId;
        attachmentObj.needSync = false;
        attachmentObj.isDownloaded = false;

        let targetPath = `${PouchDb.getClientAttachmentPath()}/${attachmentObj.storedFileUUID}`;
        // @ts-ignore
        let exists = await fs.exists(targetPath);
        // @ts-ignore
        if(exists) {
          TextEditor.updateTextAttachmentsSchedule(noteId, attachmentId, attachmentObj);
          return resolve(null);
        }

        await AttachmentObjRepository.update({
          workspaceId,
          attachmentObjs: [attachmentObj]
        });

        let attachDownloadResult = <any>await AttachmentSingleDownloader.download({
          workspaceId,
          globalId: attachmentId
        });

        if(!attachDownloadResult) {
          return resolve(null);
        }

        const { attachment, downloaded } = attachDownloadResult.response;
        if(!downloaded || !attachment) {
          return resolve(null);
        }

        TextEditor.updateTextAttachmentsSchedule(noteId, attachmentId, attachment);
      });

    });
  }

  /**
   * @param {string} noteId
   * @param {string} attachmentId
   * @param {{}} attachment
   */
  static updateTextAttachmentsSchedule(noteId, attachmentId, attachment) {
    if (appWindow.get()) {
      appWindow.get().webContents.send('event:client:update:editor:downloaded:attach:response', {
        noteId,
        attachGlobalId: attachmentId,
        serverAttachment: attachment
      });
    }
  }

  /**
   * @param {{workspaceId:string, noteGlobalId:string}} inputData
   */
  static socketTextMessage(inputData) {
    const {workspaceId, noteGlobalId} = inputData;
    socketConnection.sendTextUpdateMessage({
      workspaceId,
      globalId: noteGlobalId,
      typing: true
    });
  }
}
