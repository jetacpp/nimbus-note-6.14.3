const ENV_PROD = 'prod';
const ENV_DEV = 'dev';

module.exports = {
    ENV_PROD,
    ENV_DEV,
    "env": ENV_PROD,
};
