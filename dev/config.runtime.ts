import chalk from "chalk";
import {app} from "electron";
import {default as config} from "./config";

let runtimeConfig = null;

(() => {
  if (!runtimeConfig && config) {
    const appDataPath = app.getPath('userData');
    if (appDataPath) {
      const clientDataPath = appDataPath;
      const dataBasePath = `${clientDataPath}/databases`;
      const nimbusAttachmentPath = `${clientDataPath}/attachments`;
      const nimbusTempAttachmentPath = `${clientDataPath}/temp`;
      const nimbusExportPath = `${clientDataPath}/export-items`;
      runtimeConfig = {
        ...config,
        clientDataPath,
        dataBasePath,
        nimbusAttachmentPath,
        nimbusTempAttachmentPath,
        nimbusExportPath,
      };
    }
  }
  return runtimeConfig;
})();

if (!runtimeConfig) {
  runtimeConfig = config;
}

/**
 * @returns {{}}
 */
export default runtimeConfig;