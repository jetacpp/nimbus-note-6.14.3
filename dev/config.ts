import chalk from "chalk";
import path from "path";
import fs from "fs-extra";
import {app} from "electron";
import electronUtil from "electron-util";

// env

const IS_DEV_MODE = require('electron-is-dev'); // for prod/dev
// const IS_DEV_MODE = false; // for test
const SHOW_WEB_CONSOLE = IS_DEV_MODE;
process.env.NODE_ENV = IS_DEV_MODE ? 'development' : 'production';

// app

const APP_NAME = 'Nimbus Note';
const DEFAULT_CHARSET = 'utf8';
const APPSTORE_APP_ID = '1431085284';
const APPBUNDLE_ID = 'co.nimbusweb.nimbusnoteapp';
const SENTRY_KEY = 'https://7e9d53f6899d4c4288fb48cafb66d5e9:4a8ba3b9a9d54d249b63808ceb012a31@sentry.nimbusweb.co/9';

const APP_DIR = path.resolve(`${__dirname}/..`);
const ASAR_APP_DIR = electronUtil.fixPathForAsarUnpack(APP_DIR);
const SRC_DIR = `${APP_DIR}/src`;

// pages

const PAGE_AUTH_NAME = 'auth';
const PAGE_NOTE_NAME = 'note';

// in-app purchase products

const PURCHASE_PRODUCTS = ['mac.nimbusnote.subs.monthly', 'mac.nimbusnote.subs.yearly'];

// sync

const SYNC_URL_PROD = 'sync.everhelper.me';
const SYNC_URL_DEV = 'sync.develop.nimbustest.com';

const TRIAL_URL_PROD = 'https://trial-api.everhelper.me/v1';
const TRIAL_URL_DEV = 'http://trial-server.develop.nimbustest.com/v1';

const AUTH_URL_PROD = 'nimbusweb.me';
const AUTH_URL_DEV = 'develop.nimbustest.com';

const envFilePath = './config.env.js';
const envFileAbsolutePath = path.join(__dirname, envFilePath);
let envConfig;
try {
  if (fs.existsSync(envFileAbsolutePath)) {
    envConfig = require(envFilePath);
  }
} catch(err) {console.error(err)}

const USE_DEV_SYNC = !(envConfig && envConfig.env && envConfig.env === 'prod');

const syncApiServiceDomain = USE_DEV_SYNC ? SYNC_URL_DEV : SYNC_URL_PROD;
const authApiServiceDomain = USE_DEV_SYNC ? AUTH_URL_DEV : AUTH_URL_PROD;
const trialApiService = USE_DEV_SYNC ? TRIAL_URL_DEV : TRIAL_URL_PROD;
const authApiServicePort = USE_DEV_SYNC ? 80 : 443;

const API_AUTH_LOGIN_PATH = USE_DEV_SYNC ? '/auth_form/api/auth' : '/auth/api/auth';
const API_AUTH_REGISTER_PATH = USE_DEV_SYNC ? '/auth_form/api/register' : '/auth/api/register';
const API_AUTH_CHALLENGE_PATH = USE_DEV_SYNC ? '/auth_form/api/challenge' : '/auth/api/challenge';
const API_AUTH_REMIND_PATH = USE_DEV_SYNC ? '/auth_form/api/remind' : '/auth/api/remind';

const DOWNLOAD_NOTE_COUNT_PER_STEP = 100;

// log report

const logsReportName = 'data6aa6-0c32-a83a-afe9-712daf073565';

// payment url

const nimbusPaymentExternalUrl = 'nimbus.everhelper.me/pricing.php';
const nimbusPaymentUrlPath = '/custom/templates/payment.html';
const nimbusMessageUrlPath = '/custom/templates/message.html';
const nimbusUpdateUrlPath = '/custom/templates/update.html';

// folders

const NIMBUS_AUTH_PROJ = 'nimbus/auth';
const NIMBUS_ANGULAR_PROJ = 'nimbus/webnotes';
const NIMBUS_INJECTED_DIR_NAME = 'nimbus-injected';

const NIMBUS_ANGULAR_WEB_DIR_NAME = 'web';
const NIMBUS_CUSTOM_DIR_NAME = 'custom';
const NIMBUS_ANGULAR_TEMPLATE_STATIC_DIR_NAME = 'static';
const NIMBUS_ANGULAR_TEMPLATE_DIR_NAME = 'templates';
const NIMBUS_AUTH_TEMPLATE_DIR_NAME = 'auth';

// window

const APP_WINDOW_DEFAULT_WIDTH = 1200;
const APP_WINDOW_DEFAULT_HEIGHT = 890;
const SETTINGS_APP_WINDOW_STATE_NAME = 'nimbus-note-window-state';

// db

const DATABASE_SUFFIX = USE_DEV_SYNC ? 'dev' : '';
const DATABASE_CLIENT_VERSION = `3${DATABASE_SUFFIX}`;
const DATABASE_DATA_VERSION = `3${DATABASE_SUFFIX}`;

const DB_USER_MODEL_NAME = `nimbusNoteDbUsers${DATABASE_CLIENT_VERSION}`;
const DB_SETTINGS_MODEL_NAME = `nimbusNoteDbSettings${DATABASE_CLIENT_VERSION}`;
const DB_TODO_MODEL_NAME = `nimbusNoteDbTodo${DATABASE_DATA_VERSION}`;
const DB_ITEM_MODEL_NAME = `nimbusNoteDbItems${DATABASE_DATA_VERSION}`;
const DB_TEXT_MODEL_NAME = `nimbusNoteDbTexts${DATABASE_DATA_VERSION}`;
const DB_TEXT_EDITOR_MODEL_NAME = `nimbusNoteDbTextEditor${DATABASE_DATA_VERSION}`;
const DB_TAG_MODEL_NAME = `nimbusNoteDbTags${DATABASE_DATA_VERSION}`;
const DB_NOTE_TAGS_MODEL_NAME = `nimbusNoteDbNoteTags${DATABASE_DATA_VERSION}`;
const DB_ATTACH_MODEL_NAME = `nimbusNoteDbAttaches${DATABASE_DATA_VERSION}`;
const DB_WORKSPACE_MODEL_NAME = `nimbusNoteDbWorkspaces${DATABASE_DATA_VERSION}`;
const DB_WORKSPACE_MEMBERS_MODEL_NAME = `nimbusNoteDbWorkspaceMembers${DATABASE_DATA_VERSION}`;
const DB_WORKSPACE_INVITES_MODEL_NAME = `nimbusNoteDbWorkspaceInvites${DATABASE_DATA_VERSION}`;
const DB_ORG_MODEL_NAME = `nimbusNoteDbOrgs${DATABASE_DATA_VERSION}`;
const DB_ORG_MEMBERS_MODEL_NAME = `nimbusNoteDbOrgsMembers${DATABASE_DATA_VERSION}`;
const DB_USER_SETTINGS_MODEL_NAME = `nimbusNoteDbUserSettings${DATABASE_DATA_VERSION}`;

// workspaces

const DEFAULT_WORKSPACE_ID = 'default';

// auth

const CLIENT_AUTO_LOGIN = true;
const SETTINGS_APP_AUTH_NAME = 'nimbus-note-user-auth';

// local server

const LOCAL_SERVER_HOST = 'localhost';
const LOCAL_URL_PROTOCOL = 'nimbus-note://';
const LOCAL_SERVER_PORT = 9921;
const LOCAL_SERVER_HTTP = `http://${LOCAL_SERVER_HOST}:${LOCAL_SERVER_PORT}`;
const LOCAL_SERVER_HTTP_HOST = `http://${LOCAL_SERVER_HOST}:${LOCAL_SERVER_PORT}`;
const LOCAL_SERVER_HTTPS_HOST = `https://${LOCAL_SERVER_HOST}:${LOCAL_SERVER_PORT}`;

const WEBNOTES_DOMAIN = USE_DEV_SYNC ? 'develop.nimbustest.com' : 'nimbusweb.me';
const WEBNOTES_PROTOCOL = USE_DEV_SYNC ? 'http://' : 'https://';
const WEBNOTES_BASE_URL = `${WEBNOTES_PROTOCOL}${WEBNOTES_DOMAIN}`;

// host links

const nimbusAttachmentUrl = `${LOCAL_SERVER_HTTP_HOST}/attachment`;
const nimbusPaymentUrl = `${LOCAL_SERVER_HTTP_HOST}/${NIMBUS_ANGULAR_PROJ}${nimbusPaymentUrlPath}`;
const nimbusMessageUrl = `${LOCAL_SERVER_HTTP_HOST}/${NIMBUS_ANGULAR_PROJ}${nimbusMessageUrlPath}`;
const nimbusUpdateUrl = `${LOCAL_SERVER_HTTP_HOST}/${NIMBUS_ANGULAR_PROJ}${nimbusUpdateUrlPath}`;

// path

const nimbusModulesPath = `${APP_DIR}/node_modules`;
const nimbusBuildAssetsPath = `${APP_DIR}/build`;

const platformIconPath = `${nimbusBuildAssetsPath}/${process.platform === 'darwin' ? 'icon.png' : 'icon.ico' }`;
const applicationIconPath = `${nimbusBuildAssetsPath}/${process.platform === 'darwin' ? 'icon.png' : 'icon.window.png' }`;
const trayIconPath = `${nimbusBuildAssetsPath}/${'icon.tray.png' }`;
const aboutIconPath = platformIconPath;

const nimbusTemplatesPath = `${SRC_DIR}/${NIMBUS_INJECTED_DIR_NAME}/template`;
const nimbusAngularTemplatesPath = `${nimbusTemplatesPath}/webnotes/web`;

const nimbusCommonAssetsPath = `${SRC_DIR}/${NIMBUS_INJECTED_DIR_NAME}/assets/nimbus-common/web`;
const nimbusAngularAssetsPath = `${SRC_DIR}/${NIMBUS_INJECTED_DIR_NAME}/assets/webnotes/web`;
const nimbusAuthAssetsPath = `${SRC_DIR}/${NIMBUS_INJECTED_DIR_NAME}/assets/auth/web`;
const nimbusPaymentAssetsPath = `${SRC_DIR}/${NIMBUS_INJECTED_DIR_NAME}/assets/nimbus-payment/web`;
const nimbusUpdateAssetsPath = `${SRC_DIR}/${NIMBUS_INJECTED_DIR_NAME}/assets/nimbus-update/web`;
const nimbusPreloadAssetsFile = `${SRC_DIR}/${NIMBUS_INJECTED_DIR_NAME}/assets/nimbus-electron/preloader/globalModules.js`;

const nimbusAuthIndexFile = `/${NIMBUS_AUTH_PROJ}/${NIMBUS_ANGULAR_WEB_DIR_NAME}/auth/auth.html`;
const nimbusAngularIndexFile = `/${NIMBUS_ANGULAR_PROJ}/${NIMBUS_ANGULAR_WEB_DIR_NAME}/templates/index.html`;

const nimbusAngularFolderPath = `${APP_DIR}/${NIMBUS_ANGULAR_PROJ}/${NIMBUS_ANGULAR_WEB_DIR_NAME}`;
const nimbusAuthFolderPath = `${APP_DIR}/${NIMBUS_AUTH_PROJ}/${NIMBUS_ANGULAR_WEB_DIR_NAME}`;

const nimbusAngularWebFolderPath = `${APP_DIR}/${NIMBUS_ANGULAR_PROJ}/${NIMBUS_ANGULAR_WEB_DIR_NAME}`;
const nimbusAuthWebFolderPath = `${APP_DIR}/${NIMBUS_AUTH_PROJ}/${NIMBUS_ANGULAR_WEB_DIR_NAME}`;
const nimbusCustomFolderPath = `${APP_DIR}/${NIMBUS_ANGULAR_PROJ}/${NIMBUS_CUSTOM_DIR_NAME}`;

const nimbusAngularStaticFolderPath = `${nimbusAngularWebFolderPath}/${NIMBUS_ANGULAR_TEMPLATE_STATIC_DIR_NAME}`;
const nimbusCustomStaticFolderPath = `${nimbusCustomFolderPath}/${NIMBUS_ANGULAR_TEMPLATE_STATIC_DIR_NAME}`;
const nimbusAuthTemplatesFolderPath = `${nimbusAuthWebFolderPath}/${NIMBUS_AUTH_TEMPLATE_DIR_NAME}`;
const nimbusAngularTemplatesFolderPath = `${nimbusAngularWebFolderPath}/${NIMBUS_ANGULAR_TEMPLATE_DIR_NAME}`;
const nimbusCustomTemplatesFolderPath = `${nimbusCustomFolderPath}/${NIMBUS_ANGULAR_TEMPLATE_DIR_NAME}`;

// web app params

const APP_VERSION = app.getVersion();
const WEBNOTES_VERSION = fs.existsSync(`${nimbusAngularWebFolderPath}/package.json`) ? require('../nimbus/webnotes/web/package.json').version : '';

// runtime params

const clientDataBasePath = '';
const clientAttachmentBasePath = '';
const clientAttachmentBaseUrl = '';
const clientDataPath = '';
const dataBasePath = '';
const nimbusAttachmentPath = '';

const everhelperAttachmentBoxUrl = 'https://box.everhelper.me';
const nimbusAttachmentBoxUrl = 'https://nimbusweb.me/box';

// notes params

const NOTE_PREVIEW_SCALE_SIZE = 90;

// oauth params

const CLIENT_ID_GOOGLE = '922895159049-h7lpm45sedlhibpoa78hj0u0j7pksenc.apps.googleusercontent.com';
const CLIENT_SECRET_GOOGLE = '5y0f-dbDrD6eGZw35bU6uH4b';
const CLIENT_URL_GOOGLE = `${LOCAL_SERVER_HTTP_HOST}/ouath/success/google`;

const CLIENT_ID_FACEBOOK = '906231639828983';
const CLIENT_SECRET_FACEBOOK = 'ceeca50b6d7c66b6825e15358ebce25b';
const CLIENT_URL_FACEBOOK = `${LOCAL_SERVER_HTTP_HOST}/ouath/success/facebook`;

const CLIENT_NAME = process.platform === 'win32' ? 'win_notes' : 'mac_notes'

export default {
  APP_NAME,
  DEFAULT_CHARSET,
  APPSTORE_APP_ID,
  APPBUNDLE_ID,
  SENTRY_KEY,
  PAGE_AUTH_NAME,
  PAGE_NOTE_NAME,
  IS_DEV_MODE,
  USE_DEV_SYNC,
  SYNC_URL_PROD,
  SYNC_URL_DEV,
  API_AUTH_LOGIN_PATH,
  API_AUTH_REGISTER_PATH,
  API_AUTH_CHALLENGE_PATH,
  API_AUTH_REMIND_PATH,
  SHOW_WEB_CONSOLE,
  PURCHASE_PRODUCTS,
  DOWNLOAD_NOTE_COUNT_PER_STEP,
  syncApiServiceDomain,
  authApiServiceDomain,
  authApiServicePort,
  trialApiService,
  logsReportName,
  nimbusPaymentExternalUrl,
  nimbusPaymentUrlPath,
  nimbusMessageUrlPath,
  nimbusUpdateUrlPath,
  NIMBUS_AUTH_PROJ,
  NIMBUS_ANGULAR_PROJ,
  NIMBUS_INJECTED_DIR_NAME,
  NIMBUS_ANGULAR_WEB_DIR_NAME,
  NIMBUS_CUSTOM_DIR_NAME,
  NIMBUS_ANGULAR_TEMPLATE_STATIC_DIR_NAME,
  NIMBUS_ANGULAR_TEMPLATE_DIR_NAME,
  NIMBUS_AUTH_TEMPLATE_DIR_NAME,
  APP_WINDOW_DEFAULT_WIDTH,
  APP_WINDOW_DEFAULT_HEIGHT,
  SETTINGS_APP_WINDOW_STATE_NAME,
  DATABASE_SUFFIX,
  DEFAULT_WORKSPACE_ID,
  DATABASE_CLIENT_VERSION,
  DATABASE_DATA_VERSION,
  DB_USER_MODEL_NAME,
  DB_SETTINGS_MODEL_NAME,
  DB_TODO_MODEL_NAME,
  DB_ITEM_MODEL_NAME,
  DB_TEXT_MODEL_NAME,
  DB_TEXT_EDITOR_MODEL_NAME,
  DB_TAG_MODEL_NAME,
  DB_NOTE_TAGS_MODEL_NAME,
  DB_ATTACH_MODEL_NAME,
  DB_WORKSPACE_MODEL_NAME,
  DB_WORKSPACE_MEMBERS_MODEL_NAME,
  DB_WORKSPACE_INVITES_MODEL_NAME,
  DB_ORG_MODEL_NAME,
  DB_ORG_MEMBERS_MODEL_NAME,
  DB_USER_SETTINGS_MODEL_NAME,
  CLIENT_AUTO_LOGIN,
  SETTINGS_APP_AUTH_NAME,
  LOCAL_SERVER_PORT,
  LOCAL_SERVER_HOST,
  LOCAL_URL_PROTOCOL,
  LOCAL_SERVER_HTTP_HOST,
  LOCAL_SERVER_HTTPS_HOST,
  WEBNOTES_DOMAIN,
  WEBNOTES_PROTOCOL,
  WEBNOTES_BASE_URL,
  nimbusAttachmentUrl,
  nimbusPaymentUrl,
  nimbusMessageUrl,
  nimbusUpdateUrl,
  APP_DIR,
  ASAR_APP_DIR,
  SRC_DIR,
  nimbusModulesPath,
  nimbusBuildAssetsPath,
  applicationIconPath,
  trayIconPath,
  aboutIconPath,
  nimbusCommonAssetsPath,
  nimbusTemplatesPath,
  nimbusAngularTemplatesPath,
  nimbusAngularAssetsPath,
  nimbusAuthAssetsPath,
  nimbusPaymentAssetsPath,
  nimbusUpdateAssetsPath,
  nimbusPreloadAssetsFile,
  nimbusAuthIndexFile,
  nimbusAngularIndexFile,
  nimbusAngularFolderPath,
  nimbusAuthFolderPath,
  nimbusAngularWebFolderPath,
  nimbusAuthWebFolderPath,
  nimbusCustomFolderPath,
  nimbusAngularStaticFolderPath,
  nimbusCustomStaticFolderPath,
  nimbusAuthTemplatesFolderPath,
  nimbusAngularTemplatesFolderPath,
  nimbusCustomTemplatesFolderPath,
  APP_VERSION,
  WEBNOTES_VERSION,
  clientDataBasePath,
  clientAttachmentBasePath,
  clientAttachmentBaseUrl,
  clientDataPath,
  dataBasePath,
  nimbusAttachmentPath,
  everhelperAttachmentBoxUrl,
  nimbusAttachmentBoxUrl,
  NOTE_PREVIEW_SCALE_SIZE,
  CLIENT_ID_GOOGLE,
  CLIENT_SECRET_GOOGLE,
  CLIENT_URL_GOOGLE,
  CLIENT_ID_FACEBOOK,
  CLIENT_SECRET_FACEBOOK,
  CLIENT_URL_FACEBOOK,
  CLIENT_NAME,
};
